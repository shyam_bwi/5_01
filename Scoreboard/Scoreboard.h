/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Scoreboard.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : September 11, 2012
* @date Last Modified  : September 11, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __SCOREBOARD_H
#define __SCOREBOARD_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdio.h>
#include <RTL.h>
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_pinsel.h"

#ifdef SCOREBOARD
/*============================================================================
* Public Macro Definitions
*===========================================================================*/

#define COVERTSEC(Time) (Time.RTC_Sec)

/* Defines Section */
#define _SCOREBOARD_UART        LPC_UART3
#define _SCOREBOARD_IRQ          UART3_IRQn
#define _SCOREBOARD_IRQHander    UART3_IRQHandler

#define RATE_SET 0xFF
#define WEIGHT_SET 0xFE
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
{
  U32 Baud_rate;
  U32 Databits;
  U32 Stopbits;
  char Scoreboard_data[20];
	double scoreboard_weight;
	float	 scoreboard_rate;
}SCOREBOARD_PARAM;

extern UART_CFG_Type Scoreboard_ConfigStruct;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void scoreboard_init(void);
extern __task void scoreboard_send_task (void);

#endif /*#ifdef SCOREBOARD*/

#endif /*__SCOREBOARD_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
