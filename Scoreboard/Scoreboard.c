/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Scoreboard.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : September 11, 2012
* @date Last Modified  : September 11, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            : 
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "Scoreboard.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "Sensor_board_data_process.h"

#ifdef SCOREBOARD
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
U8 Flag=0x00;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
  SCOREBOARD_PARAM Scoreboard_struct;
  UART_CFG_Type Scoreboard_ConfigStruct; // UART Configuration structure variable
	static RTCTime Curr_time;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************//**
 * @brief  UART0 interrupt handler sub-routine
 * @param  None
 * @return  None
 **********************************************************************/
void _SCOREBOARD_IRQHander(void)
{
  // Call Standard UART 3 interrupt handler
  /* Determine the interrupt source */
  UART_GetIntId(_SCOREBOARD_UART);
}

/*****************************************************************************
* @note       Function name: void scoreboard_init(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 11th September 2012
* @brief      Description  : Scoreboard initialization with auto baud rate
                             configuration
* @note       Notes        : None
*****************************************************************************/
void scoreboard_init(void)
{
  // UART FIFO configuration Struct variable
  UART_FIFO_CFG_Type UARTFIFOConfigStruct;
  /*
   * Initialize UART3 pin connect
   * P0.2: U3_TXD
   * P0.3: U3_RXD
   */
  PINSEL_ConfigPin(0,2,2);
  PINSEL_ConfigPin(0,3,2);    

  if ( Setup_device_var.Scoreboard_type == Screen4221_str1)
  {
      /* Initialize UART Configuration parameter structure to default state:
       * Baudrate = 9600bps
       * 8 data bit
       * 1 Stop bits
       * None parity
       */
      Scoreboard_ConfigStruct.Baud_rate = 9600;
      Scoreboard_ConfigStruct.Parity = UART_PARITY_NONE;
      set_uart_param(Setup_device_var.Scoreboard_data_bits, set_data_bits, Scoreboard);
      set_uart_param(Setup_device_var.Scoreboard_stop_bits, set_stop_bits, Scoreboard);
      Scoreboard_struct.Baud_rate = Setup_device_var.Scoreboard_baud;
      Scoreboard_struct.Databits = Setup_device_var.Scoreboard_data_bits;
      Scoreboard_struct.Stopbits = Setup_device_var.Scoreboard_stop_bits;
  }
  else
  {
      /* Initialize UART Configuration parameter structure to user defined values*/
      set_uart_param(Setup_device_var.Scoreboard_baud, set_baud_rate, Scoreboard);
      set_uart_param(Setup_device_var.Scoreboard_data_bits, set_data_bits, Scoreboard);
      set_uart_param(Setup_device_var.Scoreboard_stop_bits, set_stop_bits, Scoreboard);
      set_uart_param(0, set_flow_control, Scoreboard);
      Scoreboard_struct.Baud_rate = Setup_device_var.Scoreboard_baud;
      Scoreboard_struct.Databits  = Setup_device_var.Scoreboard_data_bits;
      Scoreboard_struct.Stopbits  = Setup_device_var.Scoreboard_stop_bits;
  }
  
  /* Initialize UART3 peripheral with given to corresponding parameter
   * in this case, don't care the baudrate value UART initialized
   * since this will be determine when running auto baudrate
   */
  UART_Init(_SCOREBOARD_UART, &Scoreboard_ConfigStruct);


  /* Initialize FIFOConfigStruct to default state:
   *         - FIFO_DMAMode = DISABLE
   *         - FIFO_Level = UART_FIFO_TRGLEV0
   *         - FIFO_ResetRxBuf = ENABLE
   *         - FIFO_ResetTxBuf = ENABLE
   *         - FIFO_State = ENABLE
   */
  UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

  // Initialize FIFO for UART0 peripheral
  UART_FIFOConfig(_SCOREBOARD_UART, &UARTFIFOConfigStruct);


  // Enable UART Transmit
  UART_TxCmd(_SCOREBOARD_UART, ENABLE);

  return;
}

/*****************************************************************************
* @note       Function name: __task void scoreboard_data (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 11th September 2012
* @brief      Description  : Send data to the scoreboard
* @note       Notes        : None
*****************************************************************************/
extern int task_number;
__task void scoreboard_send_task (void)
{
  uint16_t size;
//static uint16_t delay = 0;
  static U16 Prev_time;
	
  scoreboard_init();
  while(1)
  {
		
    os_itv_set(SCORE_BOARD_TASK_INTERVAL);
    os_itv_wait();
		LPC_RTC->ALMIN |= (0x01<<2);
    Scoreboard_struct.scoreboard_weight = Calculation_struct.Total_weight;
		Scoreboard_struct.scoreboard_rate 	= Calculation_struct.Rate_for_display;
    //if settings have changed during runtime then re-initialize the scoreboard port
    if ((Scoreboard_struct.Baud_rate != Setup_device_var.Scoreboard_baud) ||
        (Scoreboard_struct.Databits != Setup_device_var.Scoreboard_data_bits) ||
        (Scoreboard_struct.Stopbits != Setup_device_var.Scoreboard_stop_bits))
    {
        scoreboard_init();
    }
    #ifdef CALCULATION
    //display weight
    if(Setup_device_var.Scoreboard_display_data == Screen421_str5)
    {
				if(Setup_device_var.Scoreboard_TMMode == Screen431_str2)
				{
					size = sprintf (Scoreboard_struct.Scoreboard_data, "WEIGHT %0.3f\r\n",Scoreboard_struct.scoreboard_weight);
				}
				else 
				{
					if(Scoreboard_struct.scoreboard_weight < 0)
					{
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "-       ");
						Scoreboard_struct.scoreboard_weight *= -1;
					}
					else
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "        ");
					if(Scoreboard_struct.scoreboard_weight < 10)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[8], "%1.3f\r\n",Scoreboard_struct.scoreboard_weight);
					else if(Scoreboard_struct.scoreboard_weight < 100)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[7], "%2.3f\r\n",Scoreboard_struct.scoreboard_weight);
					else if(Scoreboard_struct.scoreboard_weight < 1000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[6], "%3.3f\r\n",Scoreboard_struct.scoreboard_weight);
					else if(Scoreboard_struct.scoreboard_weight < 10000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[5], "%4.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 100000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[4], "%5.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 1000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[3], "%6.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 10000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[2], "%7.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 100000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[1], "%8.3f\r\n",Scoreboard_struct.scoreboard_weight);							
					size =15;					
				}
    }
    else if(Setup_device_var.Scoreboard_display_data == Screen421_str2) //display rate
    {
				if(Setup_device_var.Scoreboard_TMMode == Screen431_str2)
				{
						size = sprintf (Scoreboard_struct.Scoreboard_data, "RATE   %0.3f\r\n", Scoreboard_struct.scoreboard_rate);
				}
				else 
				{
					if(Scoreboard_struct.scoreboard_rate < 0)
					{
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "-       ");
						Scoreboard_struct.scoreboard_rate *= -1;
					}
					else
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "        ");
					if(Scoreboard_struct.scoreboard_rate < 10)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[8], "%1.3f\r\n",Scoreboard_struct.scoreboard_rate);
					else if(Scoreboard_struct.scoreboard_rate < 100)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[7], "%2.3f\r\n",Scoreboard_struct.scoreboard_rate);
					else if(Scoreboard_struct.scoreboard_rate < 1000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[6], "%3.3f\r\n",Scoreboard_struct.scoreboard_rate);
					else if(Scoreboard_struct.scoreboard_rate < 10000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[5], "%4.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 100000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[4], "%5.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 1000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[3], "%6.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 10000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[2], "%7.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 100000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[1], "%8.3f\r\n",Scoreboard_struct.scoreboard_rate);							
					size =15;					
				}
			
    }
    else //alternate weight and rate
    { 
			if(Flag == 0) // Enters in this block for the first time
			{
				 Flag = 0x01;
				 Curr_time = RTCGetTime();
				 Prev_time = COVERTSEC(Curr_time) + Setup_device_var.Scoreboard_alt_delay; //Setting the delay set in GUI
         if(Prev_time>= 60)
         {
           Prev_time -= 60;
         }
				
			}
			if(Flag == 0x01)  // Displays Weight
			{
				if(Setup_device_var.Scoreboard_TMMode == Screen431_str2)
				{
					size = sprintf (Scoreboard_struct.Scoreboard_data, "WEIGHT %0.3f\r\n",Scoreboard_struct.scoreboard_weight);
				}
				else 
				{
					if(Scoreboard_struct.scoreboard_weight < 0)
					{
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "-       ");
						Scoreboard_struct.scoreboard_weight *= -1;
					}
					else
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "        ");
					if(Scoreboard_struct.scoreboard_weight < 10)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[8], "%1.3f\r\n",Scoreboard_struct.scoreboard_weight);
					else if(Scoreboard_struct.scoreboard_weight < 100)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[7], "%2.3f\r\n",Scoreboard_struct.scoreboard_weight);
					else if(Scoreboard_struct.scoreboard_weight < 1000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[6], "%3.3f\r\n",Scoreboard_struct.scoreboard_weight);
					else if(Scoreboard_struct.scoreboard_weight < 10000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[5], "%4.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 100000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[4], "%5.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 1000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[3], "%6.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 10000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[2], "%7.3f\r\n",Scoreboard_struct.scoreboard_weight);					
					else if(Scoreboard_struct.scoreboard_weight < 100000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[1], "%8.3f\r\n",Scoreboard_struct.scoreboard_weight);															
					
					size =15;
				}
				Curr_time = RTCGetTime();
				if(COVERTSEC(Curr_time) >= Prev_time) //Enters after Time Interval is expired
				{
					 Prev_time = COVERTSEC(Curr_time) + Setup_device_var.Scoreboard_alt_delay; 
					 if(Prev_time>= 60)
					 {
						 Prev_time -= 60;
					 }					
           Flag = 0x02; 		// Updating the flag to display Rate on score board			
				}
			}
			if(Flag  == 0x02)  // Displays Rate
			{ 
				if(Setup_device_var.Scoreboard_TMMode == Screen431_str2)
				{
						size = sprintf (Scoreboard_struct.Scoreboard_data, "RATE   %0.3f\r\n", Scoreboard_struct.scoreboard_rate);
				}
				else 
				{
					if(Scoreboard_struct.scoreboard_rate < 0)
					{
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "-       ");
						Scoreboard_struct.scoreboard_rate *= -1;
					}
					else
						size = sprintf (&Scoreboard_struct.Scoreboard_data[0], "        ");
					if(Scoreboard_struct.scoreboard_rate < 10)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[8], "%1.3f\r\n",Scoreboard_struct.scoreboard_rate);
					else if(Scoreboard_struct.scoreboard_rate < 100)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[7], "%2.3f\r\n",Scoreboard_struct.scoreboard_rate);
					else if(Scoreboard_struct.scoreboard_rate < 1000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[6], "%3.3f\r\n",Scoreboard_struct.scoreboard_rate);
					else if(Scoreboard_struct.scoreboard_rate < 10000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[5], "%4.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 100000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[4], "%5.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 1000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[3], "%6.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 10000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[2], "%7.3f\r\n",Scoreboard_struct.scoreboard_rate);					
					else if(Scoreboard_struct.scoreboard_rate < 100000000)
						size = sprintf (&Scoreboard_struct.Scoreboard_data[1], "%8.3f\r\n",Scoreboard_struct.scoreboard_rate);							
					size =15;					
				}
				Curr_time = RTCGetTime();
				if(COVERTSEC(Curr_time) >= Prev_time) //Enters after Time Interval is expired
				{
					 Prev_time = COVERTSEC(Curr_time) + Setup_device_var.Scoreboard_alt_delay; 
					 if(Prev_time>= 60)
					 {
						 Prev_time -= 60;
					 }				
           Flag = 0x01; 	 // Updating the flag to display Weight on score board	
				}
			}
    }

			UART_Send(_SCOREBOARD_UART, (uint8_t *)Scoreboard_struct.Scoreboard_data, size, BLOCKING);

    #endif //#ifdef CALCULATION
LPC_RTC->ALMIN &= ~(0x01<<2);  
}
}

#endif /*#ifdef SCOREBOARD*/
/*****************************************************************************
* End of file
*****************************************************************************/
