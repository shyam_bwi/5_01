/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    SMTP_UIF.C
 *      Purpose: SMTP Client User Interface Module
 *      Rev.:    V4.22
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2011 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/
#ifdef EMAIL
#include <Net_Config.h>
#include <string.h>
#include <stdio.h>
#include "SMTP_demo.h"
#include "RTOS_main.h"


#define MAIL_FROM       "ctsuser@CTS.local"
#define RCPT_TO         "ctsuser@CTS.local"
#define SMTP_USER       "ctsuser"
#define SMTP_PASSWORD   "ctsrd"
#define MAIL_SUBJECT    "TRIAL EMAIL"

#define MSG_HEADER    "Hello %d!\r\n\r\n"                                                    

#define MSG_FOOTER    "Bye..."

#define MAIL_TEXT			"This is simple mail being sent on an event\r\n"

/* My structure of SMTP U32 storage variable. This variable is private and  */
/* is not altered by SMTP Client. It is only set to zero when smtp_cbfunc() */
/* is called for the first time.  */
typedef struct 
{
  U8  id;
  U16 idx;
} MY_BUF;
#define MYBUF(p)        ((MY_BUF *)p)

/* Net_Config.c */
extern struct sys_cfg   sys_config;
#define lhost_name      sys_config.HostName

/*----------------------------------------------------------------------------
 *      SMTP CallBack Functions
 *---------------------------------------------------------------------------*/

/*--------------------------- smtp_cbfunc -----------------------------------*/

U16 smtp_cbfunc (U8 code, U8 *buf, U16 buflen, U32 *pvar) {
  /* This function is called by the SMTP client to get email parameters and */
  /* data. It returns the number of bytes written to the output buffer.     */
  /* Hi-bit of return value (len is or-ed with 0x8000) is a repeat flag the */
  /* SMTP client. If this bit is set to 1, the system will call this func   */
  /* again with parameter 'pvar' pointing to a 4-byte buffer. This buffer   */
  /* can be used for storing different status variables for this function.  */
  /* It is set to 0 by SMTP client on first call and is not altered by SMTP */
  /* client for repeated calls. This function should NEVER write more than  */
  /* 'buflen' bytes to the buffer.                                          */
  /* Parameters:                                                            */
  /*   code   - function code with following values:                        */
  /*             0 - Username:   - for SMTP authentication if requested     */
  /*             1 - Password:   - for SMTP authentication if requested     */
  /*             2 - 'From   : ' - get email address of the sender          */
  /*             3 - 'To     : ' - get email address of recipient           */
  /*             4 - 'Subject: ' - get subject of email                     */
  /*             5 - 'Data   : ' - get email data in plain ascii format     */
  /*   buf    - SMTP transmit buffer                                        */
  /*   buflen - length of this buffer (500-1400 bytes - depends on MSS)     */
  /*   pvar   - pointer to local storage buffer used for repeated loops     */
  /*            This is a U32 variable - size is 4 bytes. Value is:         */
  /*            - on 1st call = 0                                           */
  /*            - 2nd call    = as set by this function on first call       */
  U32 /*i,*/len = 0;

  switch (code) {
    case 0:
      /* Enter Username for SMTP Server authentication. */
      len = str_copy (buf, SMTP_USER);
      break;

    case 1:
      /* Enter Password for SMTP Server authentication. */
      len = str_copy (buf, SMTP_PASSWORD);
      break;

    case 2:
      /* Enter email address of the sender. */
      len = str_copy (buf, MAIL_FROM);
      break;

    case 3:
      /* Enter email address of the recipient. */
      len = str_copy (buf, RCPT_TO);
      break;

    case 4:
      /* Enter email subject. */
      len = str_copy (buf, MAIL_SUBJECT);
      break;

    case 5:
      /* Enter email data. */
      switch (MYBUF(pvar)->id) 
			{
        case 0:
          /* First call, enter an email header text. */
          len = sprintf ((char *)buf, MSG_HEADER, lhost_name);
          MYBUF(pvar)->id  = 1;
          MYBUF(pvar)->idx = 1;
          goto rep;

        case 1:
          /* Add email message body. */
				  #if 0
          for (len = 0; len < buflen-150; ) 
					{
            /* Let's use as much of the buffer as possible. */
            /* This will produce less packets and speedup the transfer. */
            len += sprintf ((char *)(buf+len), "%d. ",MYBUF(pvar)->idx);
            //for (i = 0; i < 8; i++) 
						{
              len += sprintf ((char *)(buf+len), "AD%d= %d  ",i,AD_in(i));
            }
            len += str_copy (buf+len, "\r\n");
            if (++MYBUF(pvar)->idx > 100) 
						{
              MYBUF(pvar)->id = 2;
              break;
            }
          }
					#endif
					len = str_copy (buf, MAIL_TEXT);
					MYBUF(pvar)->id = 2;
          /* Request a repeated call, bit 15 is a repeat flag. */
rep:      len |= 0x8000;
          break;

        case 2:
          /* Last one, add a footer text to this email. */
          len = str_copy (buf, MSG_FOOTER);
					printf("\n email sent");
					//counter = 2;
//					counter1 = 0;
          break;
      }
  }
  return ((U16)len);
}


/*--------------------------- smtp_accept_auth ------------------------------*/
#if 0
BOOL smtp_accept_auth (U8 *srv_ip) {
  /* SMTP server with address 'srv_ip' is asking for user authentication. */
  /* Return value: __TRUE  = use the authentication                       */
  /*               __FALSE = do not use the authentication                */

  /* Accept the authentication. */
  return (__TRUE);
}
#endif

BOOL smtp_accept_auth (U8 *srv_ip) 
{

  if (srv_ip[0] == 192  &&
      srv_ip[1] == 168  &&
      srv_ip[2] == 1    &&
      srv_ip[3] == 200) {
    /* Our local SMTP server, no authentication. */
    return (__FALSE);
  }
  /* Log on to SMTP Server. */
  return (__TRUE);
}

#endif //#ifdef EMAIL
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
