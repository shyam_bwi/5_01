/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : USB_main.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 16, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __USB_MAIN_H
#define __USB_MAIN_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include <rl_usb.h>
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifdef USB
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern __task void usb_task (void);
extern int init_msd (char *letter);
void Backup_var_into_usb(void);
 int deinit_msd(char *letter);
int reinit_usb(void);
//extern __task void periodic_log_write (void);
#endif /*USB*/

#endif /*__USB_MAIN_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
