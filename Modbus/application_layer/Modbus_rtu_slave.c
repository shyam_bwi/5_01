/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Sensor_board_data_process.c
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 12, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     :
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
/* ----------------------- Platform includes --------------------------------*/
#include "Modbus_rtu_slave.h"
#include "RTOS_main.h"
#include "Modbus_uart_low_level.h"
#include "Screen_global_ex.h"
#include "Modbus_rtu_master.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mbs.h"
#include "internal/mbsiframe.h"
#include "common/mbtypes.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"
#include "internal/mbsi.h"

//Added by DK on 29April 2016 if communication link between Blending master & slave is broken
//then Calculation_struct.Rate_from_int_board will be set to zero
#include "Rate_blending_load_ctrl_calc.h"
#include "Screen_data_enum.h"

#ifdef MODBUS_RTU_INTEGRATOR
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
U8 Blending_Rate_Cntrl_Coil_status = 0; // Coil status of Blending and Rate Control, it is turned ON
// if blending or rate control runmode screen is seleted.
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
U32 Master_Slave_LinkCounter =0;

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
STATIC eMBException rtu_read_input_register(UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs);
STATIC eMBException rtu_read_write_register(UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs, eMBSRegisterMode eRegMode);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : __task void modbus_integrator_slave_rtu (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 24th August 2012
* @brief      Description    : Modbus rtu slave task, to initialize the modbus rtu slave stack and
*                            : respond to requests from the integrator board, usually every 300 ms.
*                            : 0x04 - read input register
* @note       Notes          : None
*****************************************************************************/
extern int 	task_number ;
__task void modbus_integrator_slave_rtu (void)
{
    eMBErrorCode    eStatus;
    xMBSHandle      xMBSHdl;

    Flags_struct.Error_flags &= ~SCALE_COMM_ERR;
    Flags_struct.Warning_flags &= ~SCALE_COMM_WARNING;
    Modbus_rtu_var.master_slave_stack_reset = NORMAL_MODE;
    do
    {
      // LPC_RTC->ALDOW &= ~(0x03<<0);
			//					 LPC_RTC->ALDOW |= (0x01<<0);
        Modbus_rtu_var.sensor_integrator_port = INTEGRATOR_BOARD;
        if( MB_ENOERR != ( eStatus = eMBSSerialInit( &xMBSHdl, MB_RTU, Setup_device_var.Network_Addr,
                                     MB_SERIAL_PORT_INT, MB_SERIAL_BAUDRATE_INT, MB_PARITY)))
        {
            ( void )eMBSClose( xMBSHdl );
        }
        else if( MB_ENOERR != ( eStatus = eMBSRegisterInputCB( xMBSHdl, rtu_read_input_register ) ) )
        {
            ( void )eMBSClose( xMBSHdl );
        }
        else if(MB_ENOERR != ( eStatus = eMBSRegisterHoldingCB( xMBSHdl, rtu_read_write_register ) ))
        {
            ( void )eMBSClose( xMBSHdl );
        }
        else
        {
            Modbus_rtu_var.current_slave_addr = Setup_device_var.Network_Addr;
            do
            {
                os_itv_set(INT_TO_INT_BOARD_MODBUS_SLAVE_RTU_COMM_TASK_INTERVAL);
                os_itv_wait();
// 								 LPC_RTC->ALDOW &= ~(0x03<<0);
// 								 LPC_RTC->ALDOW |= (0x01<<0);				
                if (Modbus_rtu_var.user_semaphore == SEMAPHORE_RELEASED)
                {
							//	LPC_RTC->ALDOW &= ~(0x03<<0);
							//	LPC_RTC->ALDOW |= (0x02<<0);			//SECTION 2
                    Modbus_rtu_var.user_semaphore = SEMAPHORE_ACQUIRED;
                    Modbus_rtu_var.sensor_integrator_port = INTEGRATOR_BOARD;
                    /* Poll the communication stack. */
                    eMBSPoll( xMBSHdl );
                    os_tsk_pass();
                    //slave address is changed so close the current slave instance and reinitialize the stack
                    //by breaking out of the loop
                    if (Modbus_rtu_var.master_slave_stack_reset == SLAVE_ADDR_CHANGE)
                    {
                        Modbus_rtu_var.master_slave_stack_reset = NORMAL_MODE;
                        break;
                    }
                    else if (Modbus_rtu_var.master_slave_stack_reset == SLAVE_TO_MASTER_STACK)
                    {
                        break;
                    }
                    Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;
                }
								
                //Added by DK on 29April 2016 if communication link between Blending master & slave is broken
                //then Calculation_struct.Rate_from_int_board will be set to zero
                if(++Master_Slave_LinkCounter > 300 )
                {
                    Master_Slave_LinkCounter =0;
                    Calculation_struct.Rate_from_int_board =0;
                    if ( Setup_device_var.PID_Setpoint_Type == Screen4513_str4)
                        Rate_blend_load.Set_rate =0;
                }

            }
            while(MB_ENOERR == eStatus);
            Modbus_rtu_var.sensor_integrator_port = INTEGRATOR_BOARD;
// 						LPC_RTC->ALDOW &= ~(0x03<<0);
// 								LPC_RTC->ALDOW |= (0x03<<0);					//SECTION 3
            (void)eMBSClose(xMBSHdl);
            if (Modbus_rtu_var.master_slave_stack_reset == SLAVE_TO_MASTER_STACK)
            {
                Modbus_rtu_var.master_slave_stack_reset = NORMAL_MODE;
                t_modbus_integrator_master_rtu = os_tsk_create (modbus_integrator_master_rtu,
                                                 MODBUS_INT_TASK_PRIORITY_MASTER);
                t_modbus_integrator_slave_rtu = 0;
                NVIC_DisableIRQ(_INTEGRATOR_UART_IRQ);
                Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;
                os_tsk_delete_self();
            }
            Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;
        }
//       	LPC_RTC->ALDOW &= ~(0x03<<0);
    }
    while( TRUE );
}

/*****************************************************************************
* @note       Function name  : eMBException rtu_read_input_register(UBYTE * pubRegBuffer,
*                            : USHORT usAddress, USHORT usNRegs)
* @returns    returns        : MB_PDU_EX_NONE - if address & value is correct, write is successful
*                            : MB_PDU_EX_ILLEGAL_DATA_ADDRESS - if address is incorrect
* @param      arg1           : Pointer to the buffer in which the read data is to be stored
*             arg2           : Virtual address of the modbus register
*             arg3           : Number of registers to be read
* @author                    : Anagha Basole
* @date       date created   : 24th June 2013
* @brief      Description    : Read the current rate and store it in the transmission buffer
* @note       Notes          : None
*****************************************************************************/
eMBException rtu_read_input_register(UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    *pubRegBuffer++ = 0x00;
    *pubRegBuffer++ = Blending_Rate_Cntrl_Coil_status;
    eException = MB_PDU_EX_NONE;
    return eException;
}
eMBException rtu_read_write_register(UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs, eMBSRegisterMode eRegMode)
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    union float_union rate;
    if(eRegMode == MBS_REGISTER_WRITE )
    {
        rate.f_array[3] = *pubRegBuffer++;
        rate.f_array[2] = *pubRegBuffer++;
        rate.f_array[1] = *pubRegBuffer++;
        rate.f_array[0] = *pubRegBuffer++;
        Calculation_struct.Rate_from_int_board = rate.float_val;
        //Added by DK on 29April 2016 if communication link between Blending master & slave is broken
        //then Calculation_struct.Rate_from_int_board will be set to zero
        Master_Slave_LinkCounter =0;
    }
    eException = MB_PDU_EX_NONE;
    return eException;
}

#endif //#ifdef MODBUS_RTU_INTEGRATOR

