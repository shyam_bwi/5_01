/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename        : Sensor_board_data_process.c
* @brief               : Controller Board
*
* @author               : Anagha Basole
*
* @date Created         : July Thursday, 2012  <July 12, 2012>
* @date Last Modified   : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
/* ----------------------- Platform includes --------------------------------*/
//#include "Global_ex.h"
#include "Modbus_rtu_master.h"
#include "RTOS_main.h"
#include "Modbus_uart_low_level.h"
#include "Sensor_board_data_process.h"
#include "Screen_global_ex.h"
#include "Calibration.h"
#include "Screen_data_enum.h"
#include "Conversion_functions.h"

/* ----------------------- Modbus includes ----------------------------------*/
//#include "common/mbtypes.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

#define MBM_SERIAL_PORT_SEN           (MB_UART_0)
#define MBM_SERIAL_BAUDRATE_SEN       (921600)

#define THREE_SEC_TIMEOUT             3000 //corresponds to 3 seconds

#define SEN_BRD_RCV_BUFFER_SIZE       0x66
#define INT_BRD_RCV_BUFFER_SIZE       0x04
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
  USHORT          rcv_buffer[210];
  USHORT          rcv_buffer1[10];
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
//xMBHandle xMBMMaster[2];
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
#ifdef MODBUS_RTU_INTEGRATOR
/*****************************************************************************
* @note       Function name  : void close_integrator_modbus_master_stack (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 24th June 2013
* @brief      Description    : Close the Modbus RTU integrator (master) stack
* @note       Notes          : None
*****************************************************************************/
void close_integrator_modbus_master_stack (void)
{
  	//eMBErrorCode    eStatus;

	  if (sensor_integrator_port == INTEGRATOR_BOARD)
		{
				//if (MB_ENOERR != /*(eStatus = */eMBMClose(xMBMMaster[INTEGRATOR_BOARD]))//)
				{
						MBP_ASSERT( 0 );
				}
				/* Return a token back to a semaphore */
        os_sem_send (semaphore1);
		}
		return;
}

/*****************************************************************************
* @note       Function name  : __task void modbus_integrator_rtu (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 3rd August 2012
* @brief      Description    : Modbus rtu master task, to initialize the modbus rtu master stack and
*                            : send requests to the integrator board every 300 ms.
*                            : 0x04 - read input register
* @note       Notes          : None
*****************************************************************************/
__task void modbus_integrator_master_rtu (void)
{
    eMBErrorCode    eStatus, eStatus2;
    static int com_error = 0;
	  xMBHandle xMBMMaster[2];

    while(1)
    {
        os_itv_set(INT_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL);
        os_itv_wait();

			  /* Wait indefinitely for a free semaphore */
        //ret = os_sem_wait (semaphore1, 0xFFFF);
        sensor_integrator_port = INTEGRATOR_BOARD;
        if( MB_ENOERR ==
           ( eStatus = eMBMSerialInit( &xMBMMaster[INTEGRATOR_BOARD], MB_RTU, MB_SERIAL_PORT_INT,
                                        				MB_SERIAL_BAUDRATE_INT, MB_PARITY ) ) )
        {
             eStatus = MB_ENOERR;
             /* Read the input registers from address 1
              * xMBHandle xHdl, UCHAR ucSlaveAddress, USHORT usRegStartAddress, UBYTE ubNRegs, USHORT arusBufferOut[]
             */
             if(MB_ENOERR != (eStatus2 = eMBMReadInputRegisters(xMBMMaster[INTEGRATOR_BOARD], 
							                            Setup_device_var.Network_Addr, 1, INT_BRD_RCV_BUFFER_SIZE, rcv_buffer1)))
             {
                 eStatus = eStatus2;
							   com_error += (INT_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL * 10);
                 if (!(Flags_struct.Warning_flags & SCALE_COMM_WARNING))
                 {
                    Flags_struct.Warning_flags |= SCALE_COMM_WARNING;
                    Flags_struct.Err_Wrn_log_written |= SCALE_COMM_WRN_LOGGED;
                 }
								 if(com_error >= THREE_SEC_TIMEOUT) //count if communication has failed for past 3 seconds
                 {
                    Flags_struct.Warning_flags &= ~SCALE_COMM_WARNING;
                    if (!(Flags_struct.Error_flags & SCALE_COMM_ERR))
                    {
                       Flags_struct.Error_flags |= SCALE_COMM_ERR;
                       Flags_struct.Err_Wrn_log_written |= SCALE_COMM_ERR_LOGGED;
                    }
								 }
								 Rprts_diag_var.RS485_2_Status = Strings[Port_string2].Text;
             }
						switch ( eStatus )
             {
                case MB_ENOERR:
                    Flags_struct.Error_flags &= ~SCALE_COMM_ERR;
                    Flags_struct.Warning_flags &= ~SCALE_COMM_WARNING;
								    GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_ERR_TIME] = 0;
								    GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_WRN_TIME] = 0;
								    Rprts_diag_var.RS485_2_Status = Strings[Port_string1].Text;
								    memcpy(&Calculation_struct.Rate_from_int_board, &rcv_buffer1[0],
                          					sizeof(Calculation_struct.Rate_from_int_board));
								    break;

                default:
                    ;
                    break;
             }
             //close_integrator_modbus_master_stack();
						 if (MB_ENOERR != (eStatus = eMBMClose(xMBMMaster[INTEGRATOR_BOARD])))
 						 {
								 MBP_ASSERT( 0 );
						 }
						 /* Return a token back to a semaphore */
             //os_sem_send (semaphore1);
        }//if( MB_ENOERR ==..)
    }//while(1)

    //return;
}
#endif //#ifdef MODBUS_RTU_INTEGRATOR

#ifdef MODBUS_SENSOR_RTU_MASTER
/*****************************************************************************
* @note       Function name  : __task void modbus_sensor_rtu (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 3rd August 2012
* @brief      Description    : Modbus RTU master task, to initialize the modbus rtu master stack and
*                            : send requests to the sensor board every 50 ms.
*                            : 0x04 - read input register
* @note       Notes          : None
*****************************************************************************/
__task void modbus_sensor_rtu (void)
{

    eMBErrorCode    eStatus, eStatus2;
    USHORT data_bytes;
    UBYTE i;
    static int com_error = 0;
	  xMBHandle xMBMMaster[2];

  #ifdef CALCULATION
    Weight_unit_conv_func();
    Distance_unit_conversion();
    //Calculation_struct.Total_weight = 0.0;
	  //Calculation_struct.Total_weight_accum = 0.0;
	  Calculation_struct.Total_weight = Calculation_struct.Total_weight_accum * Calculation_struct.Weight_conv_factor;
    Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
    Calculation_struct.Zero_weight = Calibration_var.New_zero_value / \
                                      Calculation_struct.Zero_value_conv_factor;
    Calculation_struct.Belt_length_calc = Calibration_var.New_belt_length * \
                                             Calculation_struct.Belt_length_conv_factor;
  #endif
    Calib_struct.Set_progress_bar = PROGRESS_BAR_RESET;
    Calib_struct.Start_flag = __FALSE;
    Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
    Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;

    while(1)
    {
        os_itv_set(SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL);
        os_itv_wait();

			  if (t_modbus_integrator_master_rtu != 0)
        {
			      /* Wait indefinitely for a free semaphore */
            ret = os_sem_wait (semaphore1, 0xFFFF);
				}
        sensor_integrator_port = SENSOR_BOARD;
        if( MB_ENOERR ==
           ( eStatus = eMBMSerialInit( &xMBMMaster[SENSOR_BOARD], MB_RTU, MBM_SERIAL_PORT_SEN,
                                          				MBM_SERIAL_BAUDRATE_SEN, MB_PARITY ) ) )
        {
              eStatus = MB_ENOERR;
              /* Read the input registers from address 1 - 66
               *xMBHandle xHdl, UCHAR ucSlaveAddress, USHORT usRegStartAddress, UBYTE ubNRegs, USHORT arusBufferOut[]
               */
              if( MB_ENOERR != ( eStatus2 = eMBMReadInputRegisters( xMBMMaster[SENSOR_BOARD], 1, 1, SEN_BRD_RCV_BUFFER_SIZE, rcv_buffer ) ) )
              {
                  eStatus = eStatus2;
                  com_error += (SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL * 10);
                  if (!(Flags_struct.Warning_flags & SEN_BRD_COMM_WARNING))
                  {
                    Flags_struct.Warning_flags |= SEN_BRD_COMM_WARNING;
                    Flags_struct.Err_Wrn_log_written |= SEN_BRD_COMM_WRN_LOGGED;
                  }
                  if(com_error >= THREE_SEC_TIMEOUT) //count if communication has failed for past 3 seconds
                  {
                    Flags_struct.Warning_flags &= ~SEN_BRD_COMM_WARNING;
                    if (!(Flags_struct.Error_flags & SEN_BRD_COMM_ERR))
                    {
                      Flags_struct.Err_Wrn_log_written |= SEN_BRD_COMM_ERR_LOGGED;
                      Flags_struct.Error_flags |= SEN_BRD_COMM_ERR;
                    }
                    Sens_brd_param.Sensor_Board_Status = FALSE;
                  }
                  Rprts_diag_var.RS485_1_Status = Strings[Port_string2].Text;
              }
						  
              switch ( eStatus )
              {
                case MB_ENOERR:
                    Rprts_diag_var.RS485_1_Status = Strings[Port_string1].Text;
                    Flags_struct.Warning_flags &= ~SEN_BRD_COMM_WARNING;
                    Flags_struct.Error_flags &= ~SEN_BRD_COMM_ERR;
                    Sens_brd_param.Sensor_Board_Status = TRUE;
                    com_error = 0;
                    for(i=0; i<SEN_BRD_RCV_BUFFER_SIZE; i++)
                    {
                      data_bytes =  rcv_buffer[i];
                      rcv_buffer[i] = ((data_bytes & 0xff) << 8) | ((data_bytes & 0xff00) >> 8);
                    }
                    Parse_rcvd_data((U8 *)&rcv_buffer[0]);
										/*printf("\n %lf, %d, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf",
                          										Sens_brd_param.Time,
                          										Sens_brd_param.No_of_pulses,
                           										Sens_brd_param.Accum_LC_Perc[0],
										                          Sens_brd_param.Accum_LC_Perc[1],
										                          Sens_brd_param.Accum_LC_Perc[2],
										                          Sens_brd_param.Accum_LC_Perc[3],
										                          Sens_brd_param.Accum_LC_Perc[4],
										                          Sens_brd_param.Accum_LC_Perc[5],
										                          Sens_brd_param.Accum_LC_Perc[6],
										                          Sens_brd_param.Accum_LC_Perc[7]); */
                    Calculation();
                    os_evt_set (MONITOR_WEIGHT_EVENT_FLAG, t_weight_monitor);
                    //For calibration
                    if(Calib_struct.Cal_status_flag == START_TEST_WEIGHT_CAL)
                    {
                        test_weight_cal();
                    }
                    else if(Calib_struct.Cal_status_flag == START_MATERIAL_CAL)
                    {
                        material_test_cal();
                    }
                    else if(Calib_struct.Cal_status_flag == START_DIGITAL_CAL)
                    {
                        digital_cal();
                    }
                    else if(Calib_struct.Cal_status_flag == START_STATICZERO_CAL)
                    {
                        static_zero_cal();
                    }
                    else if(Calib_struct.Cal_status_flag == START_DYNAMICZERO_CAL)
                    {
                        dynamic_zero_cal();
                    }
                    else if(Calib_struct.Cal_status_flag == START_BELT_CAL)
                    {
                        AutoBeltLength();
                    }
                    else if(Calib_struct.Cal_status_flag == START_LENZERO_CAL)
                    {
                        LengthAndZero();
                    }
                    break;

                default:
                   ;
                   break;
              }
              if( MB_ENOERR != ( eStatus = eMBMClose( xMBMMaster[SENSOR_BOARD] ) ) )
              {
                  MBP_ASSERT( 0 );
              }
							if (t_modbus_integrator_master_rtu != 0)
              {
			             /* Return a token back to a semaphore */
                   os_sem_send (semaphore1);
							}
        }//if( MB_ENOERR ==..)
    }//while(1)
}

#endif //#ifdef MODBUS_SENSOR_RTU_MASTER
/*****************************************************************************
* End of file
*****************************************************************************/
