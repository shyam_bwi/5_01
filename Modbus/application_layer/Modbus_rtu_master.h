/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Modbus_rtu_master.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : September 13th 2012
* @date Last Modified	 : September 13th 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __MODBUS_RTU_MASTER_H
#define __MODBUS_RTU_MASTER_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "mbport.h"
#include "mbm.h"
#include "common/mbtypes.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifdef MODBUS_SENSOR_RTU_MASTER
/*============================================================================
* Public Data Types
*===========================================================================*/
/**/
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern xMBHandle xMBMMaster[2];
/*============================================================================
* Public Function Declarations
*===========================================================================*/
#ifdef DEBUG_DATA_SEND
extern __task void Send_Data_On_Debug_Port_task(void);
#endif

extern __task void modbus_sensor_rtu (void);
extern __task void modbus_integrator_master_rtu(void);

#endif //#ifdef MODBUS_SENSOR_RTU_MASTER

#endif /*__MODBUS_RTU_MASTER_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
