/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project         : Beltscale Weighing Product - Integrator Board
* @detail Customer        : Beltway
*
* @file Filename          : Modbus_tcp_common.c
* @brief                  : Controller Board
*
* @author                 : Anagha Basole
*
* @date Created           : November, 2012
* @date Last Modified     : November, 2012
*
* @internal Change Log    : <YYYY-MM-DD>
* @internal               :
* @internal               :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdlib.h>
#include "Modbus_tcp_common.h"
#include "Screen_global_data.h"
#include "File_update.h"
#include "Plant_connect.h"

#ifdef MODBUS_TCP
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
__align(4) MODBUS_TCP_SIZE_STRUCT Size_structure __attribute__ ((section ("MODBUS_REG_DATA_RAM")));

__align(4) MODBUS_TCP_VAR_STRUCT Scale_runtime_tcp_var[] __attribute__ ((section ("MODBUS_REG_DATA_RAM"))) = {
    //-----------------------Scale runtime variables----------------------------------------
    /*1  */{40001, 1, &Calculation_struct.Total_weight,   5,  bit64_dbl}, //Total weight - 32 bit flt
    /*2  */{40005, 1, &Calculation_struct.Rate_for_display,7, bit32_flt}, //Rate - 32 bit flt
    /*3  */{40007, 1, &Calculation_struct.Belt_speed,     7,  bit32_flt}, //Belt_speed - 32 bit flt
    /*4  */{40009, 1, &Calculation_struct.Load_cell_perc, 7,  bit32_flt}, //Load_percentage - 32 bit flt
    /*5  */{40011, 1, &Calculation_struct.Angle,          7,  bit32_flt}, //Angle - 32 bit flt
    //------------------------------Scale Totals---------------------------------------
    /*6  */{40050, 1, &Rprts_diag_var.daily_Total_weight, 5,  bit64_dbl}, //daily total weight - 32 bit flt
    /*7  */{40054, 1, &Rprts_diag_var.weekly_Total_weight,5,  bit64_dbl}, //weekly total weight - 32 bit flt
    /*8  */{40058, 1, &Rprts_diag_var.monthly_Total_weight,5,  bit64_dbl}, //monthly total weight - 32 bit flt
    /*9  */{40062, 1, &Rprts_diag_var.yearly_Total_weight,5,  bit64_dbl}, //yearly total weight - 32 bit flt
    /*10 */{40066, 1, &Rprts_diag_var.Job_Total,          5,  bit64_dbl}, //master total weight - 32 bit flt
    /*11 */{40070, 1, &Rprts_diag_var.Master_total, 			5,  bit64_dbl}, //daily total - 32 bit int
    /*12 *///{40074, 1, &Rprts_diag_var.weekly_Total_weight,5,  bit64_dbl}, //weekly total - 32 bit int
    /*13 *///{40078, 1, &Rprts_diag_var.monthly_Total_weight,5,  bit64_dbl}, //monthly total - 32 bit int
    /*14 *///{40082, 1, &Rprts_diag_var.yearly_Total_weight,5,  bit64_dbl}, //yearly total - 32 bit int
    /*15 *///{40086, 1, &Rprts_diag_var.Master_total,       5,  bit64_dbl}, //master total - 32 bit int
};

__align(4) MODBUS_TCP_VAR_STRUCT Scale_setup_calib_tcp_var[] __attribute__ ((section ("MODBUS_REG_DATA_RAM"))) = {
    //-----------------------Scale setup----------------------------------------
    /*1  */{41001, GUI_COUNTOF(Org_210000_data), (void *)Org_210000_data,                3,  bit16_int}, //Run mode - 16 bit int
    /*2  */{41002, GUI_COUNTOF(Org_220000_data), (void *)Org_220000_data,                4,  bit16_int}, //Number of idlers - 16 bit int
    /*3  */{41003, GUI_COUNTOF(Org_230000_data), (void *)Org_230000_data,                3,  bit16_int}, //Load cell size - 16 bit int
    /*4  */{41004, 1,                            &Scale_setup_var.Custom_LC_capacity,    5,  bit16_int}, //Custom load cell capacity - 16 bit int
    /*5  */{41005, GUI_COUNTOF(Org_231210_data), (void *)Org_231210_data,                3,  bit16_int}, //Custom load cell unit - 16 bit int
    /*6  */{41006, 1,                            &Scale_setup_var.Custom_LC_output,      5,  bit16_int}, //Custom load cell mV/V - 32 bit float
    /*7  */{41007, GUI_COUNTOF(Org_240000_data), (void *)Org_240000_data,                3,  bit16_int}, //Distance Unit - 16 bit int
    /*8  */{41008, GUI_COUNTOF(Org_250000_data), (void *)Org_250000_data,                3,  bit16_int}, //Weight Unit - 16 bit int
    /*9  */{41009, GUI_COUNTOF(Org_260000_data), (void *)Org_260000_data,                3,  bit16_int}, //Rate Unit - 16 bit int
    /*10 */{41010, 1,                            &Scale_setup_var.Conveyor_angle,        5,  bit32_flt}, //Conveyor Angle - 32 bit float
    /*11 */{41012, 1,                            &Scale_setup_var.Idler_distanceA,       5,  bit32_flt}, //Idler Distance A - 32 bit int
    /*12 */{41014, 1,                            &Scale_setup_var.Idler_distanceB,       5,  bit32_flt}, //Idler Distance B - 32 bit int
    /*13 */{41016, 1,                            &Scale_setup_var.Idler_distanceC,       5,  bit32_flt}, //Idler Distance C - 32 bit int
    /*14 */{41018, 1,                            &Scale_setup_var.Idler_distanceD,       5,  bit32_flt}, //Idler Distance D - 32 bit int
    /*15 */{41020, 1,                            &Scale_setup_var.Idler_distanceE,       5,  bit32_flt}, //Idler Distance E - 32 bit int

		/*16 */{41022, 1,                            &Scale_setup_var.Wheel_diameter,        5,  bit32_flt}, //Wheel Diameter - 32 bit float
    /*17 */{41024, 1,                            &Scale_setup_var.Pulses_Per_Revolution, 5,  bit32_flt}, //Pulses per revolution - 32 bit float
    /*18 */{41026, 1,                            &Scale_setup_var.User_Belt_Speed,       5,  bit32_flt}, //User belt speed - 32 bit float
    //-----------------------------Calibration-------------------------------
    /*19 */{41028, 1,                            &Calibration_var.Test_weight,           5,  bit32_flt}, //Test Weight Value - 32 bit float
    /*20 */{41030, 1,                            &Calibration_var.Belt_scale_weight,     5,  bit32_flt}, //Belt scale weight value - 32 bit float
    /*21 */{41032, 1,                            &Calibration_var.Cert_scale_weight,     5,  bit32_flt}, //Certified scale weight value - 32 bit float
    /*22 */{41034, GUI_COUNTOF(Org_323100_data), (void *)Org_323100_data,                3,  bit16_int}, //Belt scale unit - 16 bit int
    /*23 */{41035, GUI_COUNTOF(Org_323100_data), (void *)Org_323100_data,                3,  bit16_int}, //Certified scale unit - 16 bit int
    /*24 */{41036, 1,                            &Calibration_var.old_span_value,        10, bit32_flt}, //Old Span value - 32 bit float
    /*25 */{41038, 1,                            &Calibration_var.new_span_value,        10, bit32_flt}, //New Span Value - 32 bit float
    /*26 */{41040, 1,                            &Calibration_var.span_diff,             10, bit32_flt}, //Span Difference - 32 bit float
    /*27 */{41042, 1,                            &Calibration_var.Old_zero_value,        10, bit32_flt}, //Old zero value - 32 bit float
    /*28 */{41044, 1,                            &Calibration_var.New_zero_value,        10, bit32_flt}, //New zero value - 32 bit float
    /*29 */{41046, 1,                            &Calibration_var.New_belt_length,       10, bit32_flt}, //Belt length - 32 bit float
};

__align(4) MODBUS_TCP_VAR_STRUCT Setup_devices_tcp_var[] __attribute__ ((section ("MODBUS_REG_DATA_RAM"))) = {
    //---------------------------Setup Devices-------------------------------
    /*1  */{42001, GUI_COUNTOF(Org_411000_data), (void *)Org_411000_data,                    3, bit16_int}, //Printer � enable printing - 16 bit int
    /*2  */{42002, GUI_COUNTOF(Org_412000_data), (void *)Org_412000_data,                    3, bit16_int}, //Printer � Beltway/Custom - 16 bit int
    /*3  */{42003, GUI_COUNTOF(Org_412210_data), (void *)Org_412210_data,                    3, bit16_int}, //Printer � baud rate - 16 bit int
    /*4  */{42004, GUI_COUNTOF(Org_412220_data), (void *)Org_412220_data,                    3, bit16_int}, //Printer � data bits - 16 bit int
    /*5  */{42005, GUI_COUNTOF(Org_412230_data), (void *)Org_412230_data,                    3, bit16_int}, //Printer � stop bits - 16 bit int
    /*6  */{42006, GUI_COUNTOF(Org_412240_data), (void *)Org_412240_data,                    3, bit16_int}, //Printer � flow control - 16 bit int
    /*7  */{42007, GUI_COUNTOF(Org_421000_data), (void *)Org_421000_data,                    3, bit16_int}, //Scoreboard � display data - 16 bit int
    /*8  */{42008, 1,                            &Setup_device_var.Scoreboard_alt_delay,     5, bit16_int}, //Scoreboard � time delay - 16 bit int
    /*9  */{42009, GUI_COUNTOF(Org_422000_data), (void *)Org_422000_data,                    3, bit16_int}, //Scoreboard �  display type - 16 bit int
    /*10 */{42010, GUI_COUNTOF(Org_422211_data), (void *)Org_422211_data,                    3, bit16_int}, //Scoreboard � baud rate - 16 bit int
    /*11 */{42011, GUI_COUNTOF(Org_422212_data), (void *)Org_422212_data,                    3, bit16_int}, //Scoreboard � data bits - 16 bit int
    /*12 */{42012, GUI_COUNTOF(Org_422213_data), (void *)Org_422213_data,                    3, bit16_int}, //Scoreboard � stop bits - 16 bit int
    /*13 */{42013, GUI_COUNTOF(Org_431000_data), (void *)Org_431000_data,                    3, bit16_int}, //USB � calibration log - 16 bit int
    /*14 */{42014, GUI_COUNTOF(Org_432000_data), (void *)Org_432000_data,                    3, bit16_int}, //USB � periodic log - 16 bit int
    /*15 */{42015, 1,                            &Setup_device_var.Periodic_log_interval,    5, bit16_int}, //USB � periodic time interval - 16 bit int
    /*16 */{42016, GUI_COUNTOF(Org_433000_data), (void *)Org_433000_data,                    3, bit16_int}, //USB � error log - 16 bit int
    /*17 */{42017, GUI_COUNTOF(Org_434000_data), (void *)Org_434000_data,                    3, bit16_int}, //USB � zero calibration log - 16 bit int
    /*18 */{42018, GUI_COUNTOF(Org_435000_data), (void *)Org_435000_data,                    3, bit16_int}, //USB � clear weight log - 16 bit int
    /*19 */{42019, GUI_COUNTOF(Org_441100_data), (void *)Org_441100_data,                    3, bit16_int}, //Assign input 1 - 16 bit int
    /*20 */{42020, GUI_COUNTOF(Org_441200_data), (void *)Org_441200_data,                    3, bit16_int}, //Assign input 2 - 16 bit int
    /*21 */{42021, GUI_COUNTOF(Org_441300_data), (void *)Org_441300_data,                    3, bit16_int}, //Assign input 3 - 16 bit int
    /*22 */{42022, GUI_COUNTOF(Org_441400_data), (void *)Org_441400_data,                    3, bit16_int}, //Assign input 4 - 16 bit int
    /*23 */{42023, GUI_COUNTOF(Org_442100_data), (void *)Org_442100_data,                    3, bit16_int}, //Assign output 1 - 16 bit int
    /*24 */{42024, GUI_COUNTOF(Org_442111_data), (void *)Org_442111_data,                    3, bit16_int}, //Pulse output 1 - 16 bit int
    /*25 */{42025, 1,                            &Setup_device_var.Pulse_on_time_1,          5, bit16_int}, //Pulse on time 1 - 16 bit int
    /*26 */{42026, GUI_COUNTOF(Org_442130_data), (void *)Org_442130_data,                    3, bit16_int}, //Error alarm 1 - 16 bit int
    /*27 */{42027, 1,                            &Setup_device_var.MinMax_Speed_Setpoint_1,  5, bit32_flt}, //Speed set-point 1 - 32 bit float
    /*28 */{42029, GUI_COUNTOF(Org_442142_data), (void *)Org_442142_data,                    3, bit16_int}, //Speed output type 1 - 16 bit int
    /*29 */{42030, 1,                            &Setup_device_var.MinMax_Rate_Setpoint_1,   5, bit32_flt}, //Rate set-point 1 - 32 bit float
    /*30 */{42032, GUI_COUNTOF(Org_442152_data), (void *)Org_442152_data,                    3, bit16_int}, //Rate output type 1 - 16 bit int
    /*31 */{42033, GUI_COUNTOF(Org_442200_data), (void *)Org_442200_data,                    3, bit16_int}, //Assign output 2 - 16 bit int
    /*32 */{42034, GUI_COUNTOF(Org_442211_data), (void *)Org_442211_data,                    3, bit16_int}, //Pulse output 2 - 16 bit int
    /*33 */{42035, 1,                            &Setup_device_var.Pulse_on_time_2,          5, bit16_int}, //Pulse on time 2 - 16 bit int
    /*34 */{42036, GUI_COUNTOF(Org_442230_data), (void *)Org_442230_data,                    3, bit16_int}, //Error alarm 2 - 16 bit int
    /*35 */{42037, 1,                            &Setup_device_var.MinMax_Speed_Setpoint_2,  5, bit32_flt}, //Speed set-point 2 - 32 bit float
    /*36 */{42039, GUI_COUNTOF(Org_442242_data), (void *)Org_442242_data,                    3, bit16_int}, //Speed output type 2 - 16 bit int
    /*37 */{42040, 1,                            &Setup_device_var.MinMax_Rate_Setpoint_2,   5, bit32_flt}, //Rate set-point 2 - 32 bit float
    /*38 */{42042, GUI_COUNTOF(Org_442252_data), (void *)Org_442252_data,                    3, bit16_int}, //Rate output type 2 - 16 bit int
    /*39 */{42043, GUI_COUNTOF(Org_442300_data), (void *)Org_442300_data,                    3, bit16_int}, //Assign output 3 - 16 bit int
    /*40 */{42044, GUI_COUNTOF(Org_442311_data), (void *)Org_442311_data,                    3, bit16_int}, //Pulse output 3 - 16 bit int
    /*41 */{42045, 1,                            &Setup_device_var.Pulse_on_time_3,          5, bit16_int}, //Pulse on time 3 - 16 bit int
    /*42 */{42046, GUI_COUNTOF(Org_442330_data), (void *)Org_442330_data,                    3, bit16_int}, //Error alarm 3 - 16 bit int
    /*43 */{42047, 1,                            &Setup_device_var.MinMax_Speed_Setpoint_3,  5, bit32_flt}, //Speed set-point 3 - 32 bit float
    /*44 */{42049, GUI_COUNTOF(Org_442342_data), (void *)Org_442342_data,                    3, bit16_int}, //Speed output type 3 - 16 bit int
    /*45 */{42050, 1,                            &Setup_device_var.MinMax_Rate_Setpoint_3,   5, bit32_flt}, //Rate set-point 3 - 32 bit float
    /*46 */{42052, GUI_COUNTOF(Org_442352_data), (void *)Org_442352_data,                    3, bit16_int}, //Rate output type 3 - 16 bit int
    /*47 */{42053, GUI_COUNTOF(Org_443100_data), (void *)Org_443100_data,                    3, bit16_int}, //Assign relay1 - 16 bit int
    /*48 */{42054, GUI_COUNTOF(Org_443110_data), (void *)Org_443110_data,                    3, bit16_int}, //Error alarm 4 - 16 bit int
    /*49 */{42055, 1,                            &Setup_device_var.MinMax_Speed_Setpoint_4,  5, bit32_flt}, //Speed set-point 4 - 32 bit float
    /*50 */{42057, GUI_COUNTOF(Org_443122_data), (void *)Org_443122_data,                    3, bit16_int}, //Speed output type 4 - 16 bit int
    /*51 */{42058, 1,                            &Setup_device_var.MinMax_Rate_Setpoint_4,   5, bit32_flt}, //Rate set-point 4 - 32 bit float
    /*52 */{42060, GUI_COUNTOF(Org_443132_data), (void *)Org_443132_data,                    3, bit16_int}, //Rate output type 4 - 16 bit int
    /*53 */{42061, GUI_COUNTOF(Org_443200_data), (void *)Org_443200_data,                    3, bit16_int}, //Assign relay2 - 16 bit int
    /*54 */{42062, GUI_COUNTOF(Org_443210_data), (void *)Org_443210_data,                    3, bit16_int}, //Error alarm 5 - 16 bit int
    /*55 */{42063, 1,                            &Setup_device_var.MinMax_Speed_Setpoint_5,  5, bit32_flt}, //Speed set-point 5 - 32 bit float
    /*56 */{42065, GUI_COUNTOF(Org_443222_data), (void *)Org_443222_data,                    3, bit16_int}, //Speed output type 5 - 16 bit int
    /*57 */{42066, 1,                            &Setup_device_var.MinMax_Rate_Setpoint_5,   5, bit32_flt}, //Rate set-point 5 - 32 bit float
    /*58 */{42068, GUI_COUNTOF(Org_443232_data), (void *)Org_443232_data,                    3, bit16_int}, //Rate output type 5 - 16 bit int
    /*59 */{42069, GUI_COUNTOF(Org_443300_data), (void *)Org_443300_data,                    3, bit16_int}, //Assign relay3 - 16 bit int
    /*60 */{42070, GUI_COUNTOF(Org_443310_data), (void *)Org_443310_data,                    3, bit16_int}, //Error alarm 6 - 16 bit int
    /*61 */{42071, 1,                            &Setup_device_var.MinMax_Speed_Setpoint_6,  5, bit32_flt}, //Speed set-point 6 - 32 bit float
    /*62 */{42073, GUI_COUNTOF(Org_443322_data), (void *)Org_443322_data,                    3, bit16_int}, //Speed output type 6 - 16 bit int
    /*63 */{42074, 1,                            &Setup_device_var.MinMax_Rate_Setpoint_6,   5, bit32_flt}, //Rate set-point 6 - 32 bit float
    /*64 */{42076, GUI_COUNTOF(Org_443332_data), (void *)Org_443332_data,                    3, bit16_int}, //Rate output type 6 - 16 bit int
    /*65 */{42077, GUI_COUNTOF(Org_444110_data), (void *)Org_444110_data,                    3, bit16_int}, //Analog 1 type - 16 bit int
    /*66 */{42078, 1,                            &Setup_device_var.Analog_Output_1_Setpoint, 5, bit32_flt}, //Analog 1 set-point - 16 bit int
    /*67 */{42080, 1,                            &Setup_device_var.Analog_Output_1_Maxrate,  5, bit32_flt}, //Analog 1 max-rate - 16 bit int
    /*68 */{42082, 1,                            &Setup_device_var.Analog_Output_1_ZeroCal,  5, bit32_flt}, //Analog 1 calibration num - 16 bit int
    /*69 */{42084, GUI_COUNTOF(Org_444210_data), (void *)Org_444210_data,                    3, bit16_int}, //Analog 2 type - 16 bit int
    /*70 */{42085, 1,                            &Setup_device_var.Analog_Output_2_Setpoint, 5, bit32_flt}, //Analog 2 set-point - 16 bit int
    /*71 */{42087, 1,                            &Setup_device_var.Analog_Output_2_Maxrate,  5, bit32_flt}, //Analog 2 max-rate - 16 bit int
    /*72 */{42089, 1,                            &Setup_device_var.Analog_Output_2_ZeroCal,  5, bit32_flt}, //Analog 2 calibration num - 16 bit int
    /*73 */{42091, 1,                            &Setup_device_var.PID_Channel,              5, bit16_int}, //PID control channel number - 16 bit int
    /*74 */{42092, GUI_COUNTOF(Org_451200_data), (void *)Org_451200_data,                    3, bit16_int}, //PID action - 16 bit int
    /*75 */{42093, GUI_COUNTOF(Org_451300_data), (void *)Org_451300_data,                    3, bit16_int}, //PID set-point - 16 bit int
    /*76 */{42094, 1,                            &Setup_device_var.PID_Local_Setpoint,       5, bit16_int}, //Local set-point - 16 bit int
    /*77 */{42095, 1,                            &Setup_device_var.PID_P_Term,               5, bit16_int}, //P term - 16 bit int
    /*78 */{42096, 1,                            &Setup_device_var.PID_I_Term,               5, bit16_int}, //I term - 16 bit int
    /*79 */{42097, 1,                            &Setup_device_var.PID_D_Term,               5, bit16_int}, //D term - 16 bit int
    /*80 */{42098, 1,                            &Setup_device_var.Load_weight_1,            5, bit32_flt}, //Target weight 1 - 32 bit float
    /*81 */{42100, 1,                            &Setup_device_var.Cutoff_1,                 5, bit32_flt}, //Cutoff weight 1 - 32 bit float
    /*82 */{42102, 1,                            &Setup_device_var.Load_weight_2,            5, bit32_flt}, //Target weight 2 - 32 bit float
    /*83 */{42104, 1,                            &Setup_device_var.Cutoff_2,                 5, bit32_flt}, //Cutoff weight 2 - 32 bit float
    /*84 */{42106, 1,                            &Setup_device_var.Load_weight_3,            5, bit32_flt}, //Target weight 3 - 32 bit float
    /*85 */{42108, 1,                            &Setup_device_var.Cutoff_3,                 5, bit32_flt}, //Cutoff weight 3 - 32 bit float
    /*86 */{42110, 1,                            &Setup_device_var.Load_weight_4,            5, bit32_flt}, //Target weight 4 - 32 bit float
    /*87 */{42112, 1,                            &Setup_device_var.Cutoff_4,                 5, bit32_flt}, //Cutoff weight 4 - 32 bit float
    /*88 */{42114, 1,                            &Setup_device_var.Load_weight_5,            5, bit32_flt}, //Target weight 5 - 32 bit float
    /*89 */{42116, 1,                            &Setup_device_var.Cutoff_5,                 5, bit32_flt}, //Cutoff weight 5 - 32 bit float
    /*90 */{42118, 1,                            &Setup_device_var.Load_weight_6,            5, bit32_flt}, //Target weight 6 - 32 bit float
    /*91 */{42120, 1,                            &Setup_device_var.Cutoff_6,                 5, bit32_flt}, //Cutoff weight 6 - 32 bit float
    /*92 */{42122, 1,                            &Setup_device_var.Load_weight_7,            5, bit32_flt}, //Target weight 7 - 32 bit float
    /*93 */{42124, 1,                            &Setup_device_var.Cutoff_7,                 5, bit32_flt}, //Cutoff weight 7 - 32 bit float
    /*94 */{42126, 1,                            &Setup_device_var.Load_weight_8,            5, bit32_flt}, //Target weight 8 - 32 bit float
    /*95 */{42128, 1,                            &Setup_device_var.Cutoff_8,                 5, bit32_flt}, //Cutoff weight 8 - 32 bit float
    /*96 */{42130, 1,                            &Setup_device_var.Network_Addr,             5, bit16_int}, //Network address - 16 bit int
    /*97 */{42131, 1,                            &Setup_device_var.Network_Addr,         		 5, bit16_int}, //Number of Slaves - 16 bit int
    /*98 */{42132, 1,                            &Setup_device_var.Percent_Ingredient,       5, bit32_flt}, //Ingredient % - 16 bit int
    /*99 */{42134, 1,                            &Setup_device_var.Feed_Delay,               5, bit16_int}, //Feed delay - 16 bit int
    /*100*/{42135, 1,                            &Setup_device_var.Preload_Delay,            5, bit16_int}, //Preload Delay - 16 bit int
    /*101*/{42136, 1,                            &Setup_device_var.Feeder_Max_Rate,          5, bit32_flt}, //Feed max rate - 32 bit float
    /*102*/{42138, 1,                            &Setup_device_var.Target_rate,              5, bit32_flt}, //Target rate - 32 bit float
    /*103*/{42140, 1,                            &Setup_device_var.Target_Load,              5, bit32_flt}, //Target load - 32 bit float
    /*104*/{42142, 1,                            &Setup_device_var.Min_Belt_Speed,           5, bit32_flt}, //Minimum belt speed - 32 bit float
    /*105*/{42144, 1,                            &Setup_device_var.Preload_Distance,         5, bit32_flt}, //Preload distance - 32 bit float
    /*106*/{42146, 1,                            &Setup_device_var.Empty_Belt_Speed,         5, bit32_flt}, //Empty belt speed - 32 bit float
};

__align(4) MODBUS_TCP_VAR_STRUCT Reports_diag_tcp_var[] __attribute__ ((section ("MODBUS_REG_DATA_RAM"))) = {
    //---------------Reports and Diagnostics---------------------------
    /*1  */{43001, 1, &Rprts_diag_var.Load_Cell_1_Output,         2, bit32_flt}, //Load cell 1 - 32 bit float
    /*2  */{43003, 1, &Rprts_diag_var.Load_Cell_2_Output,         2, bit32_flt}, //Load cell 2 - 32 bit float
    /*3  */{43005, 1, &Rprts_diag_var.Load_Cell_3_Output,         2, bit32_flt}, //Load cell 3 - 32 bit float
    /*4  */{43007, 1, &Rprts_diag_var.Load_Cell_4_Output,         2, bit32_flt}, //Load cell 4 - 32 bit float
    /*5  */{43009, 1, &Rprts_diag_var.Load_Cell_5_Output,         2, bit32_flt}, //Load cell 5 - 32 bit float
    /*6  */{43011, 1, &Rprts_diag_var.Load_Cell_6_Output,         2, bit32_flt}, //Load cell 6 - 32 bit float
    /*7  */{43013, 1, &Rprts_diag_var.Load_Cell_7_Output,         2, bit32_flt}, //Load cell 7 - 32 bit float
    /*8  */{43015, 1, &Rprts_diag_var.Load_Cell_8_Output,         2, bit32_flt}, //Load cell 8 - 32 bit float
    /*9  */{43017, 1, &Rprts_diag_var.Speed_Sensor,               2, bit32_flt}, //Speed sensor - 32 bit float
    /*10 */{43019, 1, &Rprts_diag_var.Angle_Sensor,               2, bit32_flt}, //Angle sensor - 32 bit float
    /*11 */{43021, 1, &Setup_device_var.Analog_Output_1_Setpoint, 2, bit32_flt}, //Analog_Output_1_Setpoint - 32 bit float
    /*12 */{43023, 1, &Setup_device_var.Analog_Output_2_Setpoint, 2, bit32_flt}, //Analog_Output_2_Setpoint - 32 bit float
    /*13 */{43025, 1, &Rprts_diag_var.Load_cell_supply,           2, bit32_flt}, //Load cell supply - 32 bit float
    /*14 */{43027, 1, &Rprts_diag_var.Five_volt_supply,           2, bit32_flt}, //+5V supply - 32 bit float
    /*15 */{2001,  1, &Rprts_diag_var.RS485_1_Status,             2, discrete},  //RS485 port 1 - On / Off
    /*16 */{2002,  1, &Rprts_diag_var.RS485_2_Status,             2, discrete},  //RS485 port 2 - On / Off
    /*17 */{2003,  1, &Rprts_diag_var.RS232_1_Status,             2, discrete},  //RS232 port 1 - On / Off
    /*18 */{2004,  1, &Rprts_diag_var.RS232_2_Status,             2, discrete},  //RS232 port 2 - On / Off
    /*19 */{2005,  1, &Rprts_diag_var.Ethernet_Status,            2, discrete},  //Ethernet - On / Off
    /*20 */{2006,  1, &Rprts_diag_var.SPI_Status,                 2, discrete},  //SPI - On / Off
    /*21 */{2007,  1, &Rprts_diag_var.Digital_Input_1_Status,     2, discrete},  //Digital_Input_1_Status - On / Off
    /*22 */{2008,  1, &Rprts_diag_var.Digital_Input_2_Status,     2, discrete},  //Digital_Input_2_Status - On / Off
    /*23 */{2009,  1, &Rprts_diag_var.Digital_Input_3_Status,     2, discrete},  //Digital_Input_3_Status - On / Off
    /*24 */{2010,  1, &Rprts_diag_var.Digital_Input_4_Status,     2, discrete},  //Digital_Input_4_Status - On / Off
    /*25 */{2011,  1, &Rprts_diag_var.Digital_Output_1_Status,    2, discrete},  //Digital_Output_1_Status - On / Off
    /*26 */{2012,  1, &Rprts_diag_var.Digital_Output_2_Status,    2, discrete},  //Digital_Output_2_Status - On / Off
    /*27 */{2013,  1, &Rprts_diag_var.Digital_Output_3_Status,    2, discrete},  //Digital_Output_3_Status - On / Off
    /*28 */{2014,  1, &Rprts_diag_var.Relay_Output_1_Status,      2, discrete},  //Relay_Output_1_Status - On / Off
    /*29 */{2015,  1, &Rprts_diag_var.Relay_Output_2_Status,      2, discrete},  //Relay_Output_2_Status - On / Off
    /*30 */{2016,  1, &Rprts_diag_var.Relay_Output_3_Status,      2, discrete},  //Relay_Output_3_Status - On / Off
};

__align(4) MODBUS_TCP_VAR_STRUCT Admin_tcp_var[] __attribute__ ((section ("MODBUS_REG_DATA_RAM"))) = {
    //-------------------Admin-----------------------------------------
    /*1  */{44001, 1,                            &Admin_var.Config_Date,         5, bit32_int}, //Date entry - 32 bit int
    /*2  */{44003, GUI_COUNTOF(Org_614200_data), (void *)Org_614200_data,        3, bit16_int}, //Date format - 16 bit int
    /*3  */{44004, GUI_COUNTOF(Org_614300_data), (void *)Org_614300_data,        3, bit16_int}, //Day of week - 16 bit int
    /*4  */{44005, 1,                            &Admin_var.Config_Time,         5, bit32_int}, //Time entry - 32 bit int
    /*5  */{44007, GUI_COUNTOF(Org_615200_data), (void *)Org_615200_data,        3, bit16_int}, //AM_PM - 16 bit int
    /*6  */{44008, GUI_COUNTOF(Org_615300_data), (void *)Org_615300_data,        3, bit16_int}, //Time format - 16 bit int
    /*7  */{44009, 1,                            &Admin_var.Auto_zero_tolerance, 5, bit32_flt}, //Auto zero tolerance - 32 bit float
    /*8  */{44011, GUI_COUNTOF(Org_617100_data), (void *)Org_617100_data,        3, bit16_int}, //Zero rate enable - 16 bit int
    /*9  */{44012, 1,                            &Admin_var.Zero_rate_limit,     5, bit32_flt}, //Zero rate limit - 32 bit float
    /*10 */{44014, 1,                            &Admin_var.Negative_rate_limit, 5, bit32_flt}, //Negative rate limit - 32 bit float
    /*11 */{44016, 1,                            &Admin_var.Negative_rate_time,  5, bit32_flt}, //Negative rate time - 32 bit float
    /*12 */{44018, GUI_COUNTOF(Org_631000_data), (void *)Org_631000_data,        3, bit16_int}, //Secure setup wizard - 16 bit int
    /*13 */{44019, GUI_COUNTOF(Org_632000_data), (void *)Org_632000_data,        3, bit16_int}, //Secure calibration - 16 bit int
    /*14 */{44020, GUI_COUNTOF(Org_633000_data), (void *)Org_633000_data,        3, bit16_int}, //Secure zero calibration - 16 bit int
    /*15 */{44021, GUI_COUNTOF(Org_634000_data), (void *)Org_634000_data,        3, bit16_int}, //Secure setup devices - 16 bit int
    /*16 */{44022, GUI_COUNTOF(Org_635000_data), (void *)Org_635000_data,        3, bit16_int}, //Secure admin - 16 bit int
    /*17 */{44023, GUI_COUNTOF(Org_636000_data), (void *)Org_636000_data,        3, bit16_int}, //Secure clear weight - 16 bit int
    /*18 */{44024, GUI_COUNTOF(Org_661000_data), (void *)Org_661000_data,        3, bit16_int}, //DHCP client - 16 bit int
    /*19 */{44025, 1,                            &Admin_var.IP_Addr,             5, bit32_int}, //IP address - 32 bit int
    /*20 */{44027, 1,                            &Admin_var.Subnet_Mask,         5, bit32_int}, //Subnet mask - 32 bit int
    /*21 */{44029, 1,                            &Admin_var.Gateway,             5, bit32_int}, //Gateway - 32 bit int
		/*22 */{44031, 1,                            &Admin_var.MBS_TCP_slid,				 5, bit16_int},	//Slave Id - 16 bit int
								//44031 slaveid
};

__align(4) MODBUS_TCP_VAR_STRUCT Plant_connect_tcp_var[] __attribute__ ((section ("MODBUS_REG_DATA_RAM"))) = {
    //-------------------Plant connect-----------------------------------------
    /*1  */{45001, 1, &Plant_connect_var.Record_type,                   7, bit16_int}, //Record type - 16 bit int
    /*2  */{45002, 1, &Plant_connect_var.Unique_id,                     7, bit32_int}, //Unique ID - 32 bit int
    /*3  */{45004, 1, &Plant_connect_var.Date,                          7, bit32_int}, //Date - 32 bit int
    /*4  */{45006, 1, &Plant_connect_var.Time,                          7, bit32_int}, //Time - 32 bit int
           //-----------------------data log---------------------------------------
    /*5  */{45010, 1, &Plant_connect_var.Accumulated_weight,            7, bit64_dbl}, //Accumulated weight - 32 bit float
    /*6  */{45014, 1, &Plant_connect_var.Weight_unit,                   7, bit16_int}, //Weight unit - 16 bit int
    /*7  */{45015, 1, &Plant_connect_var.Tons_per_hour,                 7, bit32_flt}, //Tons per hour - 32 bit float
    /*8  */{45017, 1, &Plant_connect_var.Rate_unit,                     7, bit16_int}, //Rate unit - 16 bit int
    /*9  */{45018, 1, &Plant_connect_var.Load_percentage,               7, bit32_flt}, //load percentage - 32 bit float
    /*10 */{45020, 1, &Plant_connect_var.Angle,                         7, bit32_flt}, //Angle - 32 bit float
    /*11 */{45022, 1, &Plant_connect_var.Speed,                         7, bit32_flt}, //Speed - 32 bit float
    /*12 */{45024, 1, &Plant_connect_var.Speed_unit,                    7, bit16_int}, //Speed unit - 16 bit int
           //----------------------set zero log-------------------------------------
    /*13 */{45050, 1, &Plant_connect_var.Old_zero_value,                7, bit32_flt}, //Previous zero - 32 bit float
    /*14 */{45052, 1, &Plant_connect_var.New_zero_value,                7, bit32_flt}, //New zero - 32 bit float
    /*15 */{45054, 1, &Plant_connect_var.Nos_of_pulses,                 7, bit32_int}, //Number of Pulses - 32 bit int
    /*16 */{45056, 1, &Plant_connect_var.Time_duration,                 7, bit32_flt}, //Time Duration - 32 bit float
           //-----------------------------clear log---------------------------------
    /*17 */{45100, 1, &Plant_connect_var.Accumulated_weight_before_clr, 7, bit64_dbl}, //Accumulated weight before clear - 32 bit float
		/*18 */{45104, 1, &Plant_connect_var.Clear_Date , 									7, bit32_int}, //Date at which Accumulated weight was cleared - 32 bit float
		/*19 */{45106, 1, &Plant_connect_var.Clear_Time, 										7, bit32_int}, //Accumulated weight before clear - 32 bit float
		/*20 */{45108, 1, &Plant_connect_var.Clear_Speed, 									7, bit32_flt}, //Accumulated weight before clear - 32 bit float
           //-----------------------------calibration log---------------------------
    /*20 */{45110, 1, &Plant_connect_var.Calibration_type,              7, bit16_int}, //Calibration type - 16 bit int
    /*21 */{45111, 1, &Plant_connect_var.Load_cell_capacity,            7, bit16_int}, //Load cell capacity - 16 bit int
    /*22 */{45112, 1, &Plant_connect_var.Load_cell_unit,                7, bit16_int}, //Load cell unit - 16 bit int
    /*23 */{45113, 1, &Plant_connect_var.Distance_unit,                 7, bit16_int}, //Distance unit - 16 bit int
    
// 		/*22 */{45114, 1, &Plant_connect_var.Idler_distanceA,               7, bit16_int}, //Idler distance A - 16 bit int
//     /*23 */{45115, 1, &Plant_connect_var.Idler_distanceB,               7, bit16_int}, //Idler distance B - 16 bit int
//     /*24 */{45116, 1, &Plant_connect_var.Idler_distanceC,               7, bit16_int}, //Idler distance C - 16 bit int
//     /*25 */{45117, 1, &Plant_connect_var.Idler_distanceD,               7, bit16_int}, //Idler distance D - 16 bit int
//     /*26 */{45118, 1, &Plant_connect_var.Idler_distanceE,               7, bit16_int}, //Idler distance E - 16 bit int
  
		/*24 */{45114, 1, &Plant_connect_var.Idler_distanceA,               7, bit32_flt}, //Idler distance A - 32 bit int
    /*25 */{45116, 1, &Plant_connect_var.Idler_distanceB,               7, bit32_flt}, //Idler distance B - 32 bit int
    /*26 */{45118, 1, &Plant_connect_var.Idler_distanceC,               7, bit32_flt}, //Idler distance C - 32 bit int
    /*27 */{45120, 1, &Plant_connect_var.Idler_distanceD,               7, bit32_flt}, //Idler distance D - 32 bit int
    /*26 */{45122, 1, &Plant_connect_var.Idler_distanceE,               7, bit32_flt}, //Idler distance E - 32 bit int
    /*28 */{45124, 1, &Plant_connect_var.Conveyor_angle,                7, bit32_flt}, //Conveyor angle - 32 bit float
    /*29 */{45126, 1, &Plant_connect_var.Test_weight_value,             7, bit32_flt}, //Test weight - 32 bit float
    /*30 */{45128, 1, &Plant_connect_var.Accumulated_weight_during_cal, 7, bit64_dbl}, //Accumulated weight during calibration - 32 bit float
    /*31 */{45132, 1, &Plant_connect_var.Certified_weight,              7, bit32_flt}, //Certified weight - 32 bit float
    /*32 */{45134, 1, &Plant_connect_var.Prev_cal_factor,               7, bit32_flt}, //Previous cal factor - 32 bit float
    /*33 */{45136, 1, &Plant_connect_var.New_cal_factor,                7, bit32_flt}, //New cal factor - 32 bit float
		       //------------------------ error codes---------------------------------
    /*34 */{45150, 1, &Plant_connect_var.Error_code1,                   7, bit16_int}, //Error code1 - 16 bit int
    /*35 */{45151, 1, &Plant_connect_var.Error_code2,                   7, bit16_int}, //Error code2 - 16 bit int
    /*36 */{45152, 1, &Plant_connect_var.Error_code3,                   7, bit16_int}, //Error code3 - 16 bit int
		       //-------------------old request variable-------------------------------
    /*37 */{45200, 1, &Plant_connect_var.Old_record_request,            5, bit32_int}, //Old record request - 32 bit int
		/*37 */{45202, 1, &Plant_connect_var.FWVer_Int,					            7, bit16_int}, //FW_Ver
		
		/*38 */{46001, 1, &Admin_var.MBS_TCP_slid,			            									  7, bit16_int}, //Scale Id
		/*39 */{46002, 1, &periodic_data_send_mb.Function_code,  					          		7, bit16_int}, //Data Type
		/*40 */{46003, 1, &periodic_data_send_mb.Periodic_hdr.Id,            						7, bit16_int}, //Record Id
		/*41 */{46004, 1, &periodic_data_send_mb.Periodic_hdr.Date,            					7, bit32_int}, //Date
		/*42 */{46006, 1, &periodic_data_send_mb.Periodic_hdr.Time,            					7, bit32_int}, //Time
		/*43 */{46008, 1, &periodic_data_send_mb.Accum_weight,            							7, bit64_dbl}, //Accumulated whole
		/*44 */{46012, 1, &periodic_data_send_mb.Accum_weight_unit_val,            			7, bit16_int}, //Accumulated Weight Unit
		/*45 */{46013, 1, &periodic_data_send_mb.Rate,		            									7, bit32_flt}, //Rate
		/*46 */{46015, 1, &periodic_data_send_mb.Rate_unit_val,            							7, bit16_int}, //Rate Unit
		/*47 */{46016, 1, &periodic_data_send_mb.Inst_load_pcnt,            						7, bit32_flt}, //Instantaneous Load Percentage
		/*48 */{46018, 1, &periodic_data_send_mb.Inst_conv_angle,            						7, bit32_flt}, //Instantaneous Conversion Angle
		/*49 */{46020, 1, &periodic_data_send_mb.Inst_conv_speed,            						7, bit32_flt}, //Instantaneous Belt Speed
		/*50 */{46022, 1, &periodic_data_send_mb.Speed_unit_val,            						7, bit16_int}, //Speed Unit
		/*51 */{46023, 1, &periodic_data_send_mb.daily_weight,            							7, bit64_dbl}, //daily Weight
		/*52 */{46027, 1, &periodic_data_send_mb.Accum_weight_unit_val,            			7, bit16_int}, //daily Weight unit
//SKS 10/10/2017 -->	REF EMAIL Tue 10-10-2017 10:00
	/*53 */{46028, 1, &periodic_data_send_mb.master_total_pound,            				7, bit64_dbl}, //
		//BS-280 SKS->
		/*54 */{46032, 1, &periodic_data_send_mb.daily_total_pound,            				7, bit64_dbl}, 
		/*55 */{46036, 1, &periodic_data_send_mb.zero_number_pound,            				7, bit32_int}, 
		/*56 */{46038, 1, &periodic_data_send_mb.Speed_Feet_Min,            				7, bit32_int}, 
		/*57 */{46040, 1, &periodic_data_send_mb.Inst_Rate_Pounds_Hour,            				7, bit64_dbl}, 
		/*58 */{46044, 1, &USB_err_no,            				7, bit16_int}, //
	//	/*59 */{46044, 1, &USB_err_no,            				7, bit16_int}, //TODAYS IDT
		
		//<-
		//<--
};

#if 0
__align(4) MODBUS_TCP_VAR_STRUCT Plant_connect_tcp_var_old_data[] __attribute__ ((section ("MODBUS_REG_DATA_RAM"))) = {
    //-------------------Plant connect-----------------------------------------
    /*1  */{45001, 1, &Plant_connect_var_read.Record_type,                   7, bit16_int}, //Record type - 16 bit int
    /*2  */{45002, 1, &Plant_connect_var_read.Unique_id,                     7, bit32_int}, //Unique ID - 32 bit int
    /*3  */{45004, 1, &Plant_connect_var_read.Date,                          7, bit32_int}, //Date - 32 bit int
    /*4  */{45006, 1, &Plant_connect_var_read.Time,                          7, bit32_int}, //Time - 32 bit int
           //-----------------------data log---------------------------------------
    /*5  */{45010, 1, &Plant_connect_var_read.Accumulated_weight,            7, bit64_dbl}, //Accumulated weight - 32 bit float
    /*6  */{45014, 1, &Plant_connect_var_read.Weight_unit,                   7, bit16_int}, //Weight unit - 16 bit int
    /*7  */{45015, 1, &Plant_connect_var_read.Tons_per_hour,                 7, bit32_flt}, //Tons per hour - 32 bit float
    /*8  */{45017, 1, &Plant_connect_var_read.Rate_unit,                     7, bit16_int}, //Rate unit - 16 bit int
    /*9  */{45018, 1, &Plant_connect_var_read.Load_percentage,               7, bit32_flt}, //load percentage - 32 bit float
    /*10 */{45020, 1, &Plant_connect_var_read.Angle,                         7, bit32_flt}, //Angle - 32 bit float
    /*11 */{45022, 1, &Plant_connect_var_read.Speed,                         7, bit32_flt}, //Speed - 32 bit float
    /*12 */{45024, 1, &Plant_connect_var_read.Speed_unit,                    7, bit16_int}, //Speed unit - 16 bit int
           //----------------------set zero log-------------------------------------
    /*13 */{45050, 1, &Plant_connect_var_read.Old_zero_value,                7, bit32_flt}, //Previous zero - 32 bit float
    /*14 */{45052, 1, &Plant_connect_var_read.New_zero_value,                7, bit32_flt}, //New zero - 32 bit float
    /*15 */{45054, 1, &Plant_connect_var_read.Nos_of_pulses,                 7, bit32_int}, //Number of Pulses - 32 bit int
    /*16 */{45056, 1, &Plant_connect_var_read.Time_duration,                 7, bit32_flt}, //Time Duration - 32 bit float
           //-----------------------------clear log---------------------------------
    /*17 */{45100, 1, &Plant_connect_var_read.Accumulated_weight_before_clr, 7, bit64_dbl}, //Accumulated weight before clear - 32 bit float
           //-----------------------------calibration log---------------------------
    /*18 */{45110, 1, &Plant_connect_var_read.Calibration_type,              7, bit16_int}, //Calibration type - 16 bit int
    /*19 */{45111, 1, &Plant_connect_var_read.Load_cell_capacity,            7, bit16_int}, //Load cell capacity - 16 bit int
    /*20 */{45112, 1, &Plant_connect_var_read.Load_cell_unit,                7, bit16_int}, //Load cell unit - 16 bit int
    /*21 */{45113, 1, &Plant_connect_var_read.Distance_unit,                 7, bit16_int}, //Distance unit - 16 bit int
//     /*22 */{45114, 1, &Plant_connect_var_read.Idler_distanceA,               7, bit16_int}, //Idler distance A - 16 bit int
//     /*23 */{45115, 1, &Plant_connect_var_read.Idler_distanceB,               7, bit16_int}, //Idler distance B - 16 bit int
//     /*24 */{45116, 1, &Plant_connect_var_read.Idler_distanceC,               7, bit16_int}, //Idler distance C - 16 bit int
//     /*25 */{45117, 1, &Plant_connect_var_read.Idler_distanceD,               7, bit16_int}, //Idler distance D - 16 bit int
//     /*26 */{45118, 1, &Plant_connect_var_read.Idler_distanceE,               7, bit16_int}, //Idler distance E - 16 bit int
 
    /*22 */{45114, 1, &Plant_connect_var_read.Idler_distanceA,               7, bit32_flt}, //Idler distance A - 32 bit int
    /*23 */{45116, 1, &Plant_connect_var_read.Idler_distanceB,               7, bit32_flt}, //Idler distance B - 32 bit int
    /*24 */{45118, 1, &Plant_connect_var_read.Idler_distanceC,               7, bit32_flt}, //Idler distance C - 32 bit int
    /*25 */{45120, 1, &Plant_connect_var_read.Idler_distanceD,               7, bit32_flt}, //Idler distance D - 32 bit int
    /*26 */{45122, 1, &Plant_connect_var_read.Idler_distanceE,               7, bit32_flt}, //Idler distance E - 32 bit int
  

    /*27 */{45124, 1, &Plant_connect_var_read.Conveyor_angle,                7, bit32_flt}, //Conveyor angle - 32 bit float
    /*28 */{45126, 1, &Plant_connect_var_read.Test_weight_value,             7, bit32_flt}, //Test weight - 32 bit float
    /*29 */{45128, 1, &Plant_connect_var_read.Accumulated_weight_during_cal, 7, bit64_dbl}, //Accumulated weight during calibration - 32 bit float
    /*30 */{45132, 1, &Plant_connect_var_read.Certified_weight,              7, bit32_flt}, //Certified weight - 32 bit float
    /*31 */{45134, 1, &Plant_connect_var_read.Prev_cal_factor,               7, bit32_flt}, //Previous cal factor - 32 bit float
    /*32 */{45136, 1, &Plant_connect_var_read.New_cal_factor,                7, bit32_flt}, //New cal factor - 32 bit float
		       //------------------------ error codes---------------------------------
    /*33 */{45150, 1, &Plant_connect_var_read.Error_code1,                   7, bit16_int}, //Error code1 - 16 bit int
    /*34 */{45151, 1, &Plant_connect_var_read.Error_code2,                   7, bit16_int}, //Error code2 - 16 bit int
    /*35 */{45152, 1, &Plant_connect_var_read.Error_code3,                   7, bit16_int}, //Error code3 - 16 bit int
		       //-------------------old request variable-------------------------------
    /*36 */{45200, 1, &Plant_connect_var_read.Old_record_request,            5, bit32_int}, //Old record request - 32 bit int
					 //------------------- Plant Connect variables continued--------------------------
		/*37 */{45200, 1, &Plant_connect_var_read.Old_record_request,            5, bit32_int}, //Old record request - 32 bit int

		
};
#endif
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : void calculate_size (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 29th May 2013
* @brief      Description    : Calculate the size of the modbus data structures and
*                            : store the sizes for later use in a size structure
* @note       Notes          : None
*****************************************************************************/
void calculate_size (void)
{
	  Size_structure.sizeof_scale_runtime_var = GUI_COUNTOF(Scale_runtime_tcp_var);
    Size_structure.sizeof_admin_var = GUI_COUNTOF(Admin_tcp_var);
    Size_structure.sizeof_reports_diag_var = GUI_COUNTOF(Reports_diag_tcp_var);
    Size_structure.sizeof_scale_setup_cal_var = GUI_COUNTOF(Scale_setup_calib_tcp_var);
    Size_structure.sizeof_setup_device_var = GUI_COUNTOF(Setup_devices_tcp_var);
	  Size_structure.sizeof_plantconnect_var = GUI_COUNTOF(Plant_connect_tcp_var);
	  
    return;
}


/*****************************************************************************
* @note       Function name  : int point_to_modbus_register (MODBUS_TCP_VAR_STRUCT * data,
*                                                             unsigned int start_address,
*                                                             unsigned int size)
* @returns    returns        : None
* @param      arg1           : pointer to the structure in which the element is being searched
*             arg2           : address to be found
*             arg3           : size of the structure 
*                            :
* @author                    : Anagha Basole
* @date       date created   : 29th May 2013
* @brief      Description    : point to the correct element of the structure depending
*                            : on the starting address.
* @note       Notes          : None
*****************************************************************************/
int point_to_modbus_register (MODBUS_TCP_VAR_STRUCT * data,
                                        unsigned int start_address, unsigned int size)
{
    unsigned int i;
	  int index = 0;

    for(i=0; i<size; i++)
    {
       if (data[i].modbus_reg_addr == start_address)
       {
         index = i;
         break;
       }
    }
		//if the start address is not found within the structure then return -1 
		if ((i >= size) && (index == 0))
		{
			 index = -1;
		}
    return(index);
}

/*****************************************************************************
* @note       Function name  : unsigned int screen3_variable_read_write (MODBUS_TCP_VAR_STRUCT * data,
*                                  unsigned int index, unsigned char read_write, unsigned int value)
* @returns    returns        : The value of the variable read, if read is specified.
*                            : The value written to the variable if write is specified.
* @param      arg1           : pointer to the structure, which pts to the variable which has to be read/modified
*             arg2           : refers to the specific index with the structure
*             arg3           : 1 - variable should be read from the structure
*                            : 2 - variable should be modified according to the input
*             arg4           : value to be written to the variable in write mode, else 0
* @author                    : Anagha Basole
* @date       date created   : 29th May 2013
* @brief      Description    : The variable pointed to by the structure is read/modified using the
*                            : the index and the read_write variable
* @note       Notes          : None
*****************************************************************************/
unsigned int screen3_variable_read_write (MODBUS_TCP_VAR_STRUCT * data, unsigned int index,
                                  unsigned char read_write, unsigned int value)
{
    SCREEN_TYPE3_DATA * screen_data;
    char * double_ptr;
    char char_data[ARRAY_SIZE];
    unsigned int i, read_value = 0;

    screen_data = data[index].screen_var_data;
    if (read_write == READ_REG) //read
    {
        switch(screen_data[1].data_type)
        {
            case unsigned_char_type:
              //since the structure stores the address of a character pointer, use a double char pointer
              //to access the variable value
              double_ptr = Strings[*screen_data[1].Variable1].Text;
              break;

            default:
              custom_sprintf(screen_data[1].data_type, screen_data[1].Variable1, char_data, sizeof(char_data));
              break;
        }
        //Traverse the screen data structure, for the size of the structure/listbox to read
        //the variable value
        for(i=1; i<data[index].size_of_screen_var_data; i++)
        {
           //compare with the main text index and verify which listbox entry matches with
           //the current variable
            if(screen_data[1].data_type == unsigned_char_type)
            {
                if (strcmp(double_ptr, Strings[screen_data[i].Main_text_index].Text) == 0)
                 {
                    //return the listbox selection
                    read_value = i;
                    break;
                }
            }
            else
            {
                if (strcmp(char_data, Strings[screen_data[i].Main_text_index].Text) == 0)
                 {
                    //return the listbox selcetion
                    read_value = i;
                    break;
                }
            }
        }
    }
    else //write
    {
			  if ((value < data[index].size_of_screen_var_data) && (value != 0))
				{
						//write the required data to the variable, refering to the type of the variable
						//custom_sscanf(screen_data[1].data_type, screen_data[1].Variable1,
														//Strings[screen_data[value].Main_text_index].Text);
					custom_sscanf(screen_data[1].data_type, screen_data[1].Variable1,
														(char *)screen_data[value].Main_text_index);
						//return the value that was written
						read_value = value;
				}
				else
				{
					  read_value = 0;
				}
    }

    return (read_value);
}

/*****************************************************************************
* @note       Function name  : unsigned int screen4_variable_read_write (MODBUS_TCP_VAR_STRUCT * data,
*                                  unsigned int index, unsigned char read_write, unsigned int value)
* @returns    returns        : The value of the variable read, if read is specified.
*                            : The value written to the variable if write is specified.
* @param      arg1           : pointer to the structure, which pts to the variable which has to be read/modified
*             arg2           : refers to the specific index with the structure
*             arg3           : 1 - variable should be read from the structure
*                            : 2 - variable should be modified according to the input
*             arg4           : value to be written to the variable in write mode, else 0
* @author                    : Anagha Basole
* @date       date created   : 29th May 2013
* @brief      Description    : The variable pointed to by the structure is read/modified using the
*                            : the index and the read_write variable
* @note       Notes          : None
*****************************************************************************/
unsigned int screen4_variable_read_write (MODBUS_TCP_VAR_STRUCT * data, unsigned int index,
                                  unsigned char read_write, unsigned int value)
{
    SCREEN_TYPE4_DATA * screen_data;
    char char_data[10];
    unsigned int i, read_value = 0;

    screen_data = data[index].screen_var_data;
    if (read_write == READ_REG) //read
    {
        //Since the number of idlers is an integer, convert it to string format for comparisn
        //with listbox entries
        custom_sprintf(screen_data[1].data_type1, screen_data[1].Variable1, char_data, sizeof(char_data));
        //Traverse the screen data structure, for the size of the structure/listbox to read
        //the variable value
        for(i=1; i<data[index].size_of_screen_var_data; i++)
        {
           //compare with the main text index and verify which listbox entry matches with
           //the current variable
           if (strcmp(char_data, Strings[screen_data[i].Main_text_index].Text) == 0)
           {
              //return the listbox selection
              read_value = i;
              break;
           }
        }
    }
    else //write
    {
       if ((value < data[index].size_of_screen_var_data) && (value != 0))
			 {
						//write the required data to the variable, refering to the type of the variable
            custom_sscanf(screen_data[1].data_type1, screen_data[1].Variable1,
                        (char *)screen_data[value].Main_text_index);
            //return the value that was written
            read_value = value;
				}
				else
				{
					  read_value = 0;
				}
    }

    return (read_value);
}

#endif //#ifdef MODBUS_TCP
/*****************************************************************************
* End of file
*****************************************************************************/
