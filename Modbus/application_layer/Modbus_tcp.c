/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Modbus_tcp.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 12, 2012>
* @date Last Modified  : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/

/* ----------------------- Platform includes --------------------------------*/
#include "math.h"
#include "Global_ex.h"
#include "Screen_data_enum.h"
#include "Calibration.h"
#include "Conversion_functions.h"
#include "Modbus_tcp.h"
#include "RTOS_main.h"
#include "Plant_connect.h"
#include "File_update.h"
#include "IOProcessing.h"
//#include "Firmware_upgrade.h"
#include "Log_report_data_calculate.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mbport.h"
#include "mbs.h"
#include "common/mbtypes.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"
#include "Rate_blending_load_ctrl_calc.h"
#include "Ext_flash_high_level.h"
#include "Firmware_upgrade.h"
#include "File_update.h"
#include "EMAC_LPC177x_8x.h"
#include "I2C_driver.h"
#include "rl_usb.h"
#ifdef MODBUS_TCP
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

#define MBS_LISTEN_ADDRESS              "0.0.0.0"       /* Bind on all addresses */
#define MBS_LISTEN_PORT                 502
#define MAX_TIMEOUT_IN_MS 							2500//(5 * MODBUS_TCP_PKT_TASK_INTERVAL)

#define COIL_ON                         1
#define COIL_OFF                        0

#define DAILY_WEIGHT_LBS_KG_INT_REG     40070
#define WEEKLY_WEIGHT_LBS_KG_INT_REG    40074
#define MONTHLY_WEIGHT_LBS_KG_INT_REG   40078
#define RUN_MODE_SCREEN_REGISTER				41001
#define DISTANCE_UNIT_REGISTER          41007
#define WEIGHT_UNIT_REGISTER            41008
#define RATE_UNIT_REGISTER              41009

#define PRINTER_TYPE_REGISTER           42002
#define SCOREBOARD_TYPE_REGISTER        42009

#define DATE_ENTRY_REGISTER             44001
#define DATE_FORMAT_REGISTER            44003
#define DAY_OF_THE_WEEK_REGISTER        44004
#define TIME_ENTRY_REGISTER             44005
#define AM_PM_ENTRY_REGISTER            44007
#define TIME_FORMAT_REGISTER            44008
#define DHCP_ENABLE_DISABLE_REGISTER    44024
#define IP_ADDRESS_REGISTER             44025
#define SUBNET_MASK_REGISTER            44027
#define GATEWAY_ADDRESS_REGISTER        44029

#define PLNT_CNCT_RECORD_TYPE_REGISTER  45001
#define PLNT_CNCT_WEIGHT_UNIT_REGISTER  45014
#define PLNT_CNCT_TON_PER_HR_REGISTER   45015
#define PLNT_CNCT_RATE_UNIT_REGISTER    45017
#define PLNT_CNCT_SPEED_UNIT_REGISTER   45024
#define PLNT_CNCT_CAL_TYPE_REGISTER     45110
#define PLNT_CNCT_LDCELL_CAP_REGISTER   45111
#define PLNT_CNCT_LDCELL_UNIT_REGISTER  45112
#define PLNT_CNCT_DIST_UNIT_REGISTER    45113
#define PLNT_CNCT_ERR_CODE1_REGISTER    45150
#define PLNT_CNCT_ERR_CODE2_REGISTER    45151
#define PLNT_CNCT_ERR_CODE3_REGISTER    45152
#define PLNT_CNCT_OLD_REQ_ID_REGISTER   45200
#define PLNT_CONNECT_VERSION						45202
#define START_TEST_WEIGHT_CAL_FLAG      0x01
#define START_MATERIAL_CAL_FLAG         0x02
#define START_DIGITAL_CAL_FLAG          0x04
#define START_LENZERO_CAL_FLAG          0x08
#define LENZERO_CAL_DONE                0x10
#define LEN_ZER_LEN_CAL_ACPT_FLAG       0x20
#define START_DYNAMICZERO_CAL_FLAG      0x40
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL Modbus_Data_Write_Cmd_recd = 0;
BOOL Cal_Reject_Cmd_recd = 0;

/* Character variables section */
U8 socket_num, flag;
U8 dhcp_func_modified, ip_gateway_mask_addr_rst;
U8 g_coil_buff[NO_OF_COILS] __attribute__ ((section ("GUI_DATA_RAM"))) = {0};

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
STATIC eMBException tcp_read_write_holding_reg(UBYTE * pubRegBuffer, USHORT usAddress,
        USHORT usNRegs, eMBSRegisterMode eRegMode);

STATIC eMBException tcp_read_input_reg(UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs);

STATIC eMBException tcp_read_write_coil(UBYTE * pubRegBuffer, USHORT usAddress,
                                        USHORT usNRegs, eMBSRegisterMode eRegMode);
// Below function is added for testing purpose by venkat on 7/9/2013
void coil_read(UBYTE *pubRegBuffer, unsigned int sREG, USHORT coilcount, UBYTE *coilflags);
void Double_to_Ints(double *value);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : __task void modbus_tcp_packet (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 3rd August 2012
* @brief      Description    : Modbus TCP task, to initialize the modbus tcp stack and
*                            : route the incoming requests according to the defined commands
*                            : 0x01 - read coils
*                            : 0x03 - read holding registers
*                            : 0x04 - read input register
*                            : 0x05 - write single coil
*                            : 0x10 - write multiple holding register
* @note       Notes          : None
*****************************************************************************/
extern int task_number;
void __task modbus_tcp_packet (void)
{
    eMBErrorCode    eStatus;
    STATIC xMBSHandle xMBSHdl = NULL;
    U8 timeout = 0, first_time = 0;
    U8 FirstTime_Flag1 = 1, FirstTime_Flag2 = 1;
    U8 Current_Status1 = 0, Prev_Status1 = 0,Current_Status2 = 0, Prev_Status2 = 0;


    //U16	waiting_for_fin_ack_timeout = 0;

    //calculate the size of the modbus TCP structures
    calculate_size();
    dhcp_func_modified = ip_gateway_mask_addr_rst = 0;

    memset(g_coil_buff,0,sizeof(g_coil_buff)); //Added by PK on 19Feb2016

    while(1)
    {

        if(NULL == xMBSHdl)
        {
            LPC_RTC->ALHOUR |= (0x01<<4);
            //initialize the modbus TCP slave stack
            if(MB_ENOERR != (eStatus = eMBSTCPInit(&xMBSHdl, MBS_LISTEN_ADDRESS, MBS_LISTEN_PORT)))
            {
                //if initialization is not successful then set the ethernet status to 'Not working' and
                //reset the ethernet connection flag
                Flags_struct.Connection_flags &= ~ETH_CON_FLAG;
                Rprts_diag_var.Ethernet_Status = Port_string2;
                Flags_struct.Connection_flags &= ~ETH_CON_FLAG2;

            }
            //if the request is to read/write holding register forward the request to tcp_read_write_holding_reg function
            else if(MB_ENOERR != (eStatus = eMBSRegisterHoldingCB(xMBSHdl, tcp_read_write_holding_reg)))
            {
                ( void )eMBSClose( xMBSHdl );
            }
            //if the request is to read input register forward the request to tcp_read_input_reg
            else if(MB_ENOERR != (eStatus = eMBSRegisterInputCB(xMBSHdl, tcp_read_input_reg)))
            {
                ( void )eMBSClose( xMBSHdl );
            }
            //if the request is to read/write coils forward the request to tcp_write_coil
            else if(MB_ENOERR != (eStatus = eMBSRegisterCoilCB(xMBSHdl, tcp_read_write_coil)))
            {
                ( void )eMBSClose( xMBSHdl );
            }
            else
            {
                eStatus = MB_ENOERR;
            }
            if( MB_ENOERR != eStatus )
            {
                if( NULL != xMBSHdl )
                {
                    eMBSClose( xMBSHdl );
                }
                xMBSHdl = NULL;
            }
        }
        else
        {
            //set the flag for ethernet connection up and set the status to 'working'
            //do
            {
                os_itv_set(MODBUS_TCP_PKT_TASK_INTERVAL);
                os_itv_wait();
                LPC_RTC->ALHOUR |= (0x01<<4);
                //Added on 27 April 2016
                /*	while(Flash_Write_In_progress == 1)
                  {
                    os_dly_wait(1);
                  }	*/

                if(Flash_Write_In_progress == 0)  //Added on 28 April 2016
                {
                    vMBTCPServerPoll(  );
                    /* Poll the communication stack. */
                    eStatus = eMBSPoll( xMBSHdl );
                }

                os_tsk_pass();
// 						flag = tcp_get_state(socket_num);
// 						if (flag == TCP_STATE_CLOSED)
// 						{
// 							( void )eMBSClose( xMBSHdl );
// 							//tcp_close(socket_num);
// 							break;
// 						}
// 						if (flag == TCP_STATE_LAST_ACK)
// 						{
// 							//The firmware stack has sent a reset signal, and will wait for 5 * MODBUS_TCP_PKT_TASK_INTERVAL
// 							//for an acknowledgement before resetting the connection
// 							if (waiting_for_fin_ack_timeout < MAX_TIMEOUT_IN_MS)
// 							{
// 								waiting_for_fin_ack_timeout += MODBUS_TCP_PKT_TASK_INTERVAL;
// 							}
// 							else
// 							{
// 								( void )eMBSClose( xMBSHdl );
// 								tcp_close(socket_num);
// 							//	tcp_close(socket_num);
// 							//	waiting_for_fin_ack_timeout = 0;
// 						 		break;
// 							}
// 						}
// 						if(flag == TCP_STATE_FINW1 || flag == TCP_STATE_FINW2)
// 						{
// 								tcp_close(socket_num);
// 						}
// 						if(flag ==  TCP_STATE_CLOSING)
// 						{

// 							( void )eMBSClose( xMBSHdl );
// 							tcp_close(socket_num);
// 						}

// 						if (flag == TCP_STATE_LISTEN) //not yet connected, only the port is initialized
// 						{
// 							 Flags_struct.Connection_flags &= ~ETH_CON_FLAG;
// 							 Rprts_diag_var.Ethernet_Status = Port_string2; //off
// 						}
// 						else if (flag == TCP_STATE_CONNECT) //
// 						{
// 								//set the flag for ethernet connection up and set the status to 'working'
// 							 Flags_struct.Connection_flags |= ETH_CON_FLAG;
// 							 Rprts_diag_var.Ethernet_Status = Port_string1; //on
// 						}
                if (((ip_gateway_mask_addr_rst == 1) || (dhcp_func_modified == 1)) && (timeout == 0) && (first_time == 0))
                {
                    timeout = 10;
                    first_time = 1;
                }
                else if (((ip_gateway_mask_addr_rst == 1) || (dhcp_func_modified == 1)) && (timeout > 0))
                {
                    timeout--;
                }
                else if ((dhcp_func_modified == 1) && (timeout == 0))
                {
#ifdef ETHERNET
                    dhcp_chk_enable_disable();
#endif
                    dhcp_func_modified = 0;
                    timeout = 0;
                    first_time = 0;
                    (void)eMBSClose( xMBSHdl );
                    tcp_close(socket_num);
                    //break; //Commented by DK on 19 Feb 2016
                }
                else if ((ip_gateway_mask_addr_rst == 1) && (timeout == 0))
                {
#ifdef ETHERNET
                    change_ip_address();
#endif
                    ip_gateway_mask_addr_rst = 0;
                    timeout = 0;
                    first_time = 0;
                    (void)eMBSClose( xMBSHdl );
                    tcp_close(socket_num);
                    //break;  //Commented by DK on 19 Feb 2016
                }
            }
            //while( MB_ENOERR == eStatus );
            if( MB_ENOERR != eStatus )
            {
                ( void )eMBSClose( xMBSHdl );
                xMBSHdl = NULL;
            }
        }//end of else

        //Added by PVK 31 Mar 2016
        Current_Status1 = (Flags_struct.Connection_flags & ETH_CON_FLAG);

        if((Current_Status1) && ((Prev_Status1 != Current_Status1)||(FirstTime_Flag1 == 1)))
        {
            Prev_Status1 = Current_Status1;
            //Log error into USB
//				Write_Debug_Logs_into_file(TCP_MODBUS_SYSLOG_EVENT1);
            FirstTime_Flag1 = 0;
        }
        else if((!(Current_Status1)) &&((Prev_Status1 != (!(Current_Status1)))||(FirstTime_Flag1 == 1)))
        {
            Prev_Status1 = (!(Current_Status1));
            //Log error into USB
            //			Write_Debug_Logs_into_file(TCP_MODBUS_SYSLOG_EVENT2);
            FirstTime_Flag1 = 0;
        }

        Current_Status2 = (Flags_struct.Connection_flags & ETH_CON_FLAG2);

        if((Current_Status2) && ((Prev_Status2 != Current_Status2)||(FirstTime_Flag2 == 1)))
        {
            Prev_Status2 = Current_Status2;
            //Log error into USB
//				Write_Debug_Logs_into_file(TCP_MODBUS_SYSLOG_EVENT3);
            FirstTime_Flag2 = 0;
        }
        else if((!(Current_Status2)) &&((Prev_Status2 != (!(Current_Status2)))||(FirstTime_Flag2 == 1)))
        {
            Prev_Status2 = (!(Current_Status2));
            //Log error into USB
//				Write_Debug_Logs_into_file(TCP_MODBUS_SYSLOG_EVENT4);
            FirstTime_Flag2 = 0;
        }
        LPC_RTC->ALHOUR &= ~(0x01<<4);
    }//While end
}//Task end
/*****************************************************************************
* @note       Function name  : eMBException tcp_read_write_holding_reg(UBYTE * pubRegBuffer, USHORT usAddress,
                                                   USHORT usNRegs, eMBSRegisterMode eRegMode)
* @returns    returns        : MB_PDU_EX_NONE - if address & value is correct, write is successful
*                            : MB_PDU_EX_ILLEGAL_DATA_ADDRESS - if address is incorrect
*                            : MB_PDU_EX_ILLEGAL_DATA_VALUE - if the data value is incorrect
* @param      arg1           : Pointer to the buffer which contains the data to be written/the data just read
*             arg2           : Virtual address of the modbus holding register
*             arg3           : Number of registers to be read/modified
*             arg4           : Whether to read/modify the holding register
* @author                    : Anagha Basole
* @date       date created   : 3rd July 2013
* @brief      Description    : Read or modify the GUI variables according to the virtual register
*                            : address and number of registers.
*                            :
* @note       Notes          : If the MODBUS slave stack needs to now the value of holding registers it
*                              executes this callback with \c eRegisterMode = MBS_REGISTER_READ. The
*                              callback function should then store the current values of the registers
*                              starting at \c usAddress up to <tt>usAddress + usNRegs</tt> (not
*                              including the last one) into the buffer \c pubRegBuffer. The 16 bit
*                              registers values must be stored in big endian format.<br>
*                              If the values should be updated the function is called with \c eRegisterMode
*                              set to MBS_REGISTER_WRITE. The register values are passed in the buffer \c
*                              pubRegBuffer and it is the applications responsibility to use these values.
*                              The first register is stored in <tt>pubRegBuffer[0] - pubRegBuffer[1]</tt>
*                              where the high byte comes first (big endian).
*
*                       \param pubRegBuffer A pointer to an internal buffer. If registers are read
*                              then exactly <tt>2 * usNRegs</tt> bytes must be written to this buffer.
*                              If registers are written the application can read up to
*                              <tt>2 * usNRegs</tt> from this buffer.
*                       \param usAddress The address of the first register which should be returned.
*                              Registers start at zero.
*                       \param usNRegs The number of registers to read.
*                       \param eRegMode If the registers are read or written.
*
*                      \return If the callback returns eMBException::MB_PDU_EX_NONE a response is
*                              sent back to the master. Otherwise an appropriate exception frame is
*                              generated.
*****************************************************************************/
eMBException tcp_read_write_holding_reg(UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs,
                                        eMBSRegisterMode eRegMode)
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    MODBUS_TCP_VAR_STRUCT * data;
    unsigned int usRegStart = (usAddress + MODBUS_REGISTER_BASE_ADDR); //add the base address to the received address
    unsigned int usRegEnd = (usRegStart + usNRegs - 1); //calculate the end address
    unsigned int size = 0, index = 0,  curr_address= 0, address_valid = 0, curr_address_old = 0;
    int curr_index = 0;
    unsigned char read_write, error_flag = 0;
    union uint_union register_int16bit;
    union ulong_union register_int32bit;
    union float_union register_float32bit;
    union double_union register_double64bit;
    unsigned int prev_distance_unit, prev_weight_unit, prev_rate_unit;
    unsigned char i = 0;

    /*#ifdef DEBUG_DATA_SEND_1
    char chData_buf[50];
    #endif
    */

    //static float rate_conv_factor;

    //40001 to 40068 - Scale runtime variables
    if ((SCALE_RUNTIME_TCP_REG_START_ADDR <= usRegStart) &&
            (usRegStart <= SCALE_RUNTIME_TCP_REG_END_ADDR))
    {
        data = Scale_runtime_tcp_var;
        //store the maximum array index for navigation limits
        size = Size_structure.sizeof_scale_runtime_var;
        address_valid = 1;
    }
    //41001 to 41035 - Scale setup and calibration variables
    else if ((SCALE_SETUP_CAL_TCP_REG_START_ADDR <= usRegStart) &&
             (usRegStart <= CAL_VAR_READ_ONLY_TCP_REG_END_ADDR))
    {
        data = Scale_setup_calib_tcp_var;
        //store the maximum array index for navigation limits
        size = Size_structure.sizeof_scale_setup_cal_var;
        address_valid = 1;
    }
    //42001 to 42145 - Setup device variables
    else if ((SETUP_DEVICE_TCP_REG_START_ADDR <= usRegStart) &&
             (usRegStart <= SETUP_DEVICE_TCP_REG_END_ADDR))
    {
        data = Setup_devices_tcp_var;
        //store the maximum array index for navigation limits
        size = Size_structure.sizeof_setup_device_var;
        address_valid = 1;
    }
    //44001 to 44029 - Admin variables
    else if ((ADMIN_TCP_REG_START_ADDR <= usRegStart) &&
             (usRegStart <= ADMIN_TCP_REG_END_ADDR))
    {
        data = Admin_tcp_var;
        //store the maximum array index for navigation limits
        size = Size_structure.sizeof_admin_var;
        address_valid = 1;
    }
    //45001 to 45200 - Plant connect variables
    else if ((PLANT_CONNECT_TCP_REG_START_ADDR <= usRegStart) &&
             (usRegStart <= PLANT_CONNECT_TCP_REG_END_ADDR))
    {
        data = Plant_connect_tcp_var;
        //store the maximum array index for navigation limits
        size = Size_structure.sizeof_plantconnect_var;
        address_valid = 1;
    }

    //set the read_write flag to 1 if the command is read holding registers and set it to 2
    //if the command is write holding registers
    switch ( eRegMode )
    {
    case MBS_REGISTER_READ:
        read_write = READ_REG;
        break;

    case MBS_REGISTER_WRITE:
        read_write = WRITE_REG;
        //Added on 25March 2016
        if (!((SCALE_RUNTIME_TCP_REG_START_ADDR <= usRegStart) &&
                (usRegStart <= SCALE_RUNTIME_TCP_REG_END_ADDR)))
        {
            //GUI_data_nav.GUI_structure_backup = 1; //Set the flag to store the modifications in the EEPROM
            //Added by PVK on 2 May 2016
            Modbus_Data_Write_Cmd_recd = 1;
        }
        break;
    }

    if (address_valid == 1)
    {
        //calculate the index w.r.t the starting address in the request, -1 will be returned if  address is not found
        curr_index = point_to_modbus_register (data, usRegStart, size);

        //if requested address is found within specified struture, then access the structure to perform the required action
        if (curr_index != -1)
        {
            //read/modify all the required variables from the start address to the end address
            for(index = (unsigned int)curr_index, curr_address = usRegStart; curr_address <= usRegEnd; index++)
            {
                //check the index and verify that the index is within array size range and the current requested address
                //is present at that index
                if ((index < size) && (curr_address == data[index].modbus_reg_addr))
                {
                    //initialize all the variables
                    register_int16bit.uint_val = 0;
                    register_int32bit.ulong_val = 0;
                    register_float32bit.float_val = 0;
                    register_double64bit.double_val = 0;
                    if (read_write == WRITE_REG)
                    {
                        curr_address_old = curr_address;
                        //store the received data according to the type of variable being accessed into the
                        //internal buffer to be copied into the variable
                        switch (data[index].data_type)
                        {
                        case bit16_int:
                            //since data is 16 bit long, read 2 bytes from the input buffer
                            register_int16bit.uint_array[1] = *pubRegBuffer++;
                            register_int16bit.uint_array[0] = *pubRegBuffer++;
                            curr_address += 1;
                            break;

                        case bit32_int:
                            //since data is 32 bit long, read 4 bytes from the input buffer
                            register_int32bit.ulong_array[3] = *pubRegBuffer++;
                            register_int32bit.ulong_array[2] = *pubRegBuffer++;
                            register_int32bit.ulong_array[1] = *pubRegBuffer++;
                            register_int32bit.ulong_array[0] = *pubRegBuffer++;
                            curr_address += 2;
                            break;

                        case bit32_flt:
                            //since data is 32 bits long/float, read 4 bytes from the input buffer
                            register_float32bit.f_array[3] = *pubRegBuffer++;
                            register_float32bit.f_array[2] = *pubRegBuffer++;
                            register_float32bit.f_array[1] = *pubRegBuffer++;
                            register_float32bit.f_array[0] = *pubRegBuffer++;
                            curr_address += 2;
                            break;

                        case bit64_dbl:
                            //since data is 64 bit long/double, read 8 bytes from the input buffer
                            register_double64bit.d_array[7] = *pubRegBuffer++;
                            register_double64bit.d_array[6] = *pubRegBuffer++;
                            register_double64bit.d_array[5] = *pubRegBuffer++;
                            register_double64bit.d_array[4] = *pubRegBuffer++;
                            register_double64bit.d_array[3] = *pubRegBuffer++;
                            register_double64bit.d_array[2] = *pubRegBuffer++;
                            register_double64bit.d_array[1] = *pubRegBuffer++;
                            register_double64bit.d_array[0] = *pubRegBuffer++;
                            curr_address += 4;
                            break;
                        }
                    }
                    //Read/modify the variable, w.r.t the type of screen structure
                    switch (data[index].Screen_type)
                    {
                    case 3:
                        if ((curr_address == DISTANCE_UNIT_REGISTER) ||
                                (curr_address == WEIGHT_UNIT_REGISTER) ||
                                (curr_address == RATE_UNIT_REGISTER))
                        {
                            //store the previously selected unit for conversion of the defined parameters
                            prev_distance_unit = Scale_setup_var.Distance_unit;
                            prev_weight_unit = Scale_setup_var.Weight_unit;
                            prev_rate_unit = Scale_setup_var.Rate_time_unit;
                        }

                        if ((register_int16bit.uint_val > 3 && Scale_setup_var.Distance_unit == Screen24_str2 && curr_address_old == WEIGHT_UNIT_REGISTER)
                                || (register_int16bit.uint_val > 2 && Scale_setup_var.Distance_unit == Screen24_str5 && curr_address_old == WEIGHT_UNIT_REGISTER))
                        {
                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                            error_flag = 1;
                            break;
                        }

                        register_int16bit.uint_val = screen3_variable_read_write(data, index, read_write, register_int16bit.uint_val);

                        if(curr_address == WEIGHT_UNIT_REGISTER)
                        {
                            if(register_int16bit.uint_val > 3)
                            {
                                if(Scale_setup_var.Distance_unit == Screen24_str2)
                                {
                                    eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                    error_flag = 1;
                                    break;
                                }
                                else
                                {
                                    register_int16bit.uint_val-=3;
                                }
                            }
                        }
                        if (register_int16bit.uint_val == 0)
                        {
                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                            error_flag = 1;
                            break;
                        }
                        if(read_write == WRITE_REG)
                        {
                            switch(curr_address_old)
                            {
                            //distance unit is to be modified
                            case DISTANCE_UNIT_REGISTER:
                                //set the default length, speed and weight units depending on English/Metric
                                //selection of distance units
                                set_default_eng_metric_units(prev_distance_unit);
                                //convert the current length or speed values if the measurement unit has changed
                                Convert_user_values(prev_distance_unit);
                                //convert the current weight values if the measurement unit has changed
                                Convert_user_weight_values(prev_weight_unit);
                                break;

                            case WEIGHT_UNIT_REGISTER:
                                //Set the weight unit depending on the listbox selection and if distance unit
                                //is English/Metric
                                if ((register_int16bit.uint_val > 3 && Scale_setup_var.Distance_unit == Screen24_str2)
                                        || (register_int16bit.uint_val > 2 && Scale_setup_var.Distance_unit == Screen24_str5))
                                {
                                    eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                    error_flag = 1;
                                    break;
                                }
                                else
                                {
                                    prev_weight_unit = Scale_setup_var.Weight_unit;
                                    set_weight_unit(register_int16bit.uint_val);
                                    //convert the current weight values if the measurement unit has changed
                                    Convert_user_weight_values(prev_weight_unit);
                                }
                                break;

                            case RATE_UNIT_REGISTER:
                                Convert_rate_user_values(prev_rate_unit);
                                break;

                            case PRINTER_TYPE_REGISTER:
                                //if printer type is beltway printer then set the default parameters
                                if(register_int16bit.uint_val == 1)
                                {
                                    set_beltway_printer_param();
                                }
                                break;

                            case SCOREBOARD_TYPE_REGISTER:
                                //if printer type is beltway printer then set the default parameters
                                if(register_int16bit.uint_val == 1)
                                {
                                    set_beltway_scoreboard_param();
                                }
                                break;

                            case DHCP_ENABLE_DISABLE_REGISTER:
                                dhcp_func_modified = 1;
                                break;

                            case RUN_MODE_SCREEN_REGISTER:
// 																		 switch (register_int16bit.uint_val)
// 																		 {
// 																					case 1:
// 																							 Create_Weight_Run_mode_screen(CREATE_SCREEN);
// 																							 break;
//
// 																					case 2:
// 																							 Create_loadout_screen(CREATE_SCREEN);
// 																							 break;

// 																					case 3:
// 																							 Create_blending_load_rate_ctrl_screen(CREATE_SCREEN);
// 																							 //set the flag
// 																							 Rate_blend_load.Calculation_flags |= BLEND_CONTROL_SCREEN_FLAG;
// 																							 break;
//
// 																					case 4:
// 																							 Create_blending_load_rate_ctrl_screen(CREATE_SCREEN);
// 																							 //set the flag
// 																							 Rate_blend_load.Calculation_flags |= RATE_CONTROL_SCREEN_FLAG;
// 																							 break;
// 																					default:
// 																							 Create_loadout_screen(CREATE_SCREEN);
// 																							 break;
//
// 																			}
                                break;
                            case DAY_OF_THE_WEEK_REGISTER:
                                Set_rtc_dow(Admin_var.Day_of_week);
                                break;
                                /*case DATE_FORMAT_REGISTER:
                                	break;

                                case AM_PM_ENTRY_REGISTER:
                                	break;

                                case TIME_FORMAT_REGISTER:
                                	break;*/
                            }


                        }
                        break;

                    case 4:
                        register_int16bit.uint_val = screen4_variable_read_write(data, index, read_write, register_int16bit.uint_val);
                        if (register_int16bit.uint_val == 0)
                        {
                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                            error_flag = 1;
                            break;
                        }
                        break;

                    case 5:
                        if(read_write == READ_REG) //read
                        {
                            switch (data[index].data_type)
                            {
                            case bit16_int:
                                register_int16bit.uint_val = *(unsigned int *)data[index].screen_var_data;
                                break;

                            case bit32_int:
                                register_int32bit.ulong_val = *(unsigned long *)data[index].screen_var_data;
                                break;

                            case bit32_flt:
                                register_float32bit.float_val = *(float *)data[index].screen_var_data;
                                break;

                            case bit64_dbl:
                                register_double64bit.double_val = *(double *)data[index].screen_var_data;
                                break;
                            }
                        }
                        else //write
                        {
                            switch (data[index].data_type)
                            {
                            case bit16_int:



                                *(unsigned int *)data[index].screen_var_data = register_int16bit.uint_val;
// 																 if(curr_address_old == DAY_OF_THE_WEEK_REGISTER)		//MOVED TO SCREEN TYPE 3 RRG 4Dec2014
// 																 {
// 																		 Set_rtc_dow(Admin_var.Day_of_week);
// 																 }
                                break;

                            case bit32_int:
                                *(unsigned long *)data[index].screen_var_data = register_int32bit.ulong_val;
                                switch(curr_address_old)
                                {

                                case DATE_ENTRY_REGISTER:
                                    //changing date via Modbus

                                    Set_rtc_date(Admin_var.Config_Date);
                                    Write_Debug_Logs_into_file(MB_DATE_CHANGE_EVENT);
                                    break;

                                case TIME_ENTRY_REGISTER:
                                    //changing time via modbus

                                    Set_rtc_time(Admin_var.Config_Time);
                                    Write_Debug_Logs_into_file(MB_TIME_CHANGE_EVENT);
                                    break;

                                case IP_ADDRESS_REGISTER:
                                case SUBNET_MASK_REGISTER:
                                case GATEWAY_ADDRESS_REGISTER:
                                    ip_gateway_mask_addr_rst = 1;
                                    break;

                                case PLNT_CNCT_OLD_REQ_ID_REGISTER:
                                    Flags_struct.Plant_connect_record_flag |= REGISTER_ADDR_ENTERED_FLAG;
                                    break;
                                }
                                break;

                            case bit32_flt:
                                /* Added on 30th Nov 2015 for BS-111 fix */
                                if(usRegStart == 41010)
                                {
                                    //If angle sensor input from Modbus is negative then consider its abs value
                                    if(register_float32bit.float_val < 0)
                                    {
                                        register_float32bit.float_val = fabsf(register_float32bit.float_val);
                                    }
                                    //If angle sensor input from Modbus is gretaer than 60 then restore old value
                                    if(register_float32bit.float_val > 60)
                                    {
                                        register_float32bit.float_val = Scale_setup_var.Conveyor_angle;
                                    }
                                }
                                *(float *)data[index].screen_var_data = register_float32bit.float_val;
                                break;

                            case bit64_dbl:
                                if(data[index].modbus_reg_addr == TOTAL_WEIGHT)
                                {


                                    for(i=0; i<8; i++)
                                    {
                                        if(register_double64bit.d_array[i] == 0)
                                        {
                                            if(i==7)
                                            {
                                                //Calculation_struct.Total_weight_accum = 0.0;
                                                Plant_connect_var.Clear_Date.Year = Current_time.RTC_Year;
                                                Plant_connect_var.Clear_Date.Mon 	= Current_time.RTC_Mon;
                                                Plant_connect_var.Clear_Date.Day 	= Current_time.RTC_Mday;
                                                Plant_connect_var.Clear_Time.Hr 	= Current_time.RTC_Hour;
                                                Plant_connect_var.Clear_Time.Min 	= Current_time.RTC_Min;
                                                Plant_connect_var.Clear_Time.Sec 	= Current_time.RTC_Sec;
                                                Plant_connect_var.Clear_Speed 		= Calculation_struct.Belt_speed;

                                                if(clr_wt_cmd_recd == 0 ) //Added on 27 April 2016
                                                {
                                                    clr_wt_cmd_recd =1;  //Added on 27 April 2016
                                                    Flash_Write_In_progress = 1; //Added on 27 April 2016
                                                    //LPC_EMAC->IntEnable &= ~INT_RX_DONE; //Added on 28 April 2016  to disable EMAC rx int
                                                    //LPC_EMAC->IntEnable &= ~INT_TX_DONE; //Added on 28 April 2016  to disable EMAC tx int

                                                    /*
                                                    LPC_EMAC->Command &= (~CR_RX_EN); //Added on 28 April 2016  to disable EMAC receiption
                                                    LPC_EMAC->Command &= (~CR_TX_EN); //Added on 28 April 2016  to disable EMAC Transmission
                                                    // Reset all interrupts
                                                    LPC_EMAC->IntClear  = 0xFFFF;
                                                    */

                                                    IO_board_param.Clear_weight_input_set = __TRUE;
                                                }
                                                else //Added on 27 April 2016
                                                {

                                                    eException = MB_PDU_EX_SLAVE_BUSY;
                                                    error_flag = 1;
                                                    break;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                            error_flag = 1;
                                            break;
                                        }
                                    }

                                }
                                else if(data[index].modbus_reg_addr == DAILY_WEIGHT && register_double64bit.double_val == 0.0)
                                {

                                    for(i=0; i<8; i++)
                                    {
                                        if(register_double64bit.d_array[i] == 0)
                                        {
                                            if(i==7)
                                            {
                                                //Megha updated this for BS-155 and -120 on 25-Nov-2015 to clear daily
                                                //totals from modbus and logged DCW-CLR event in periodic log file
                                                Totals_var.daily_Total_weight = 0.0;
                                                Clr_totals_key_press_flag = 1;
                                                IO_board_param.Clear_weight_input_set = __TRUE;
                                            }
                                        }
                                        else
                                        {
                                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                            error_flag = 1;
                                            break;
                                        }
                                    }

                                }
                                else if(data[index].modbus_reg_addr == MONTHLY_WEIGHT && register_double64bit.double_val == 0.0)
                                {
                                    for(i=0; i<8; i++)
                                    {
                                        if(register_double64bit.d_array[i] == 0)
                                        {
                                            Totals_var.monthly_Total_weight = 0.0;
                                            Rprts_diag_var.monthly_Total_weight = 0.0;
                                        }
                                        else
                                        {
                                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                            error_flag = 1;
                                            break;
                                        }
                                    }
                                }
                                else if(data[index].modbus_reg_addr == WEEKLY_WEIGHT && register_double64bit.double_val == 0.0)
                                {
                                    for(i=0; i<8; i++)
                                    {
                                        if(register_double64bit.d_array[i] == 0)
                                        {
                                            Totals_var.weekly_Total_weight = 0.0;
                                            Rprts_diag_var.weekly_Total_weight = 0.0;
                                        }
                                        else
                                        {
                                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                            error_flag = 1;
                                            break;
                                        }
                                    }
                                }
                                else if(data[index].modbus_reg_addr == YEARLY_WEIGHT && register_double64bit.double_val == 0.0)
                                {
                                    for(i=0; i<8; i++)
                                    {
                                        if(register_double64bit.d_array[i] == 0)
                                        {
                                            Totals_var.yearly_Total_weight = 0.0;
                                            Rprts_diag_var.yearly_Total_weight = 0.0;
                                        }
                                        else
                                        {
                                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                            error_flag = 1;
                                            break;
                                        }
                                    }
                                }
                                else if(data[index].modbus_reg_addr == JOB_TOTAL && register_double64bit.double_val == 0.0)
                                {
                                    for(i=0; i<8; i++)
                                    {
                                        if(register_double64bit.d_array[i] == 0)
                                        {
                                            Totals_var.Job_Total = 0.0;
                                            Rprts_diag_var.Job_Total = 0.0;
                                        }
                                        else
                                        {
                                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                            error_flag = 1;
                                            break;
                                        }
                                    }
                                }
                                else if(data[index].modbus_reg_addr == MASTER_TOTAL && register_double64bit.double_val == 0.0)
                                {
                                    for(i=0; i<8; i++)
                                    {
                                        if(register_double64bit.d_array[i] == 0)
                                        {
                                            Totals_var.Master_total = 0.0;
                                            Rprts_diag_var.Master_total = 0.0;
                                        }
                                        else
                                        {
                                            eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                            error_flag = 1;
                                            break;
                                        }
                                    }
                                }

                                else
                                {
                                    eException = MB_PDU_EX_ILLEGAL_DATA_VALUE;
                                    error_flag = 1;
                                    break;
                                }
                                //Make all the double weight varibles to zero
                                //if external utility tries to write non zero value
                                /* if(register_double64bit.double_val == 0.00000)
                                 {
                                	 register_double64bit.double_val = 0.0;
                                 }
                                 if(((data[index].modbus_reg_addr != DAILY_WEIGHT) || (data[index].modbus_reg_addr != WEEKLY_WEIGHT) ||
                                	  (data[index].modbus_reg_addr != MONTHLY_WEIGHT)) && register_double64bit.double_val == 0.0)
                                 {
                                        *(double *)data[index].screen_var_data = register_double64bit.double_val;
                                 }*/
                                break;
                            }
                        }
                        break;

                    case 7: //read only holding register screen type-variables not actually on any screen(dummy screen type)
                        if(read_write == READ_REG) //read
                        {
                            switch (data[index].data_type)
                            {
                            case bit16_int:
                                switch(curr_address)
                                {
                                /*case PLNT_CNCT_RECORD_TYPE_REGISTER:
                                 break;*/

                                case PLNT_CNCT_WEIGHT_UNIT_REGISTER:
                                    weight_unit_plant_connect();
                                    break;

                                case PLNT_CNCT_RATE_UNIT_REGISTER:
                                    rate_unit_plant_connect();
                                    break;

                                case PLNT_CNCT_DIST_UNIT_REGISTER:
                                case PLNT_CNCT_SPEED_UNIT_REGISTER:
                                    speed_distance_plant_connect_unit();
                                    break;

                                /*case PLNT_CNCT_CAL_TYPE_REGISTER:
                                 break;*/

                                case PLNT_CNCT_LDCELL_UNIT_REGISTER:
                                case PLNT_CNCT_LDCELL_CAP_REGISTER:
                                    load_cell_size_unit_plant_connect();
                                    break;
                                case PLNT_CONNECT_VERSION:
                                    Plant_connect_var.FWVer_Int = (FIRMWARE_VER_MAJOR<<8)|FIRMWARE_VER;
                                    break;

                                    /*case PLNT_CNCT_ERR_CODE1_REGISTER:
                                     break;

                                    case PLNT_CNCT_ERR_CODE2_REGISTER:
                                     break;

                                    case PLNT_CNCT_ERR_CODE3_REGISTER:
                                     break;*/
                                }
                                register_int16bit.uint_val = *(unsigned int *)data[index].screen_var_data;
                                break;

                            case bit32_int:
                                switch(curr_address)
                                {
                                case DAILY_WEIGHT_LBS_KG_INT_REG:
                                case WEEKLY_WEIGHT_LBS_KG_INT_REG:
                                case MONTHLY_WEIGHT_LBS_KG_INT_REG:
                                    if ((Scale_setup_var.Weight_unit == LBS) ||
                                            (Scale_setup_var.Weight_unit == KG))
                                    {
                                        register_int32bit.ulong_val = *(unsigned long *)data[index].screen_var_data;
                                    }
                                    else if (Scale_setup_var.Weight_unit == TONNE) //metric ton or tonne
                                    {
                                        register_int32bit.ulong_val = (*(unsigned long *)data[index].screen_var_data \
                                                                       / KG_TO_TONNE_CONV_FACTOR);
                                    }
                                    else if (Scale_setup_var.Weight_unit == LONG_TON) //metric ton or tonne
                                    {
                                        register_int32bit.ulong_val = (*(unsigned long *)data[index].screen_var_data \
                                                                       / LBS_TO_LONG_TON_CONV_FACTOR);
                                    }
                                    else if ( Scale_setup_var.Weight_unit == TONS) //metric ton or tonne
                                    {
                                        register_int32bit.ulong_val = (*(unsigned long *)data[index].screen_var_data \
                                                                       / LBS_TO_SHORT_TON_CONV_FACTOR);
                                    }
                                    break;

                                default:
                                    register_int32bit.ulong_val = *(unsigned long *)data[index].screen_var_data;
                                    break;
                                }
                                break;

                            case bit32_flt:
                                register_float32bit.float_val = *(float *)data[index].screen_var_data;
                                break;

                            case bit64_dbl:
                                register_double64bit.double_val = *(double *)data[index].screen_var_data;
                                break;
                            }
                            if(curr_address==PLANT_CONNECT_TCP_REG_END_ADDR)
                            {
                                if(U8GlobalMBCount>0)
                                {
                                    //added circular buffer logic
                                    g_Rec_idx_mb = rec_Q_get_item_mb();
                                    memcpy(&periodic_data_send_mb,&periodic_data_mb[g_Rec_idx_mb],sizeof(PERIODIC_LOG_STRUCT));
                                    last_sent_mb_record.idx_no = g_Rec_idx_mb;
                                    last_sent_mb_record.rec_no = periodic_data_mb[g_Rec_idx_mb].Periodic_hdr.Id;

                                    if(U8GlobalMBCount > 0)
                                        U8GlobalMBCount--;
                                    else
                                        U8GlobalMBCount = 0;
                                }//SKS : 31/10/2017 retain the last modbus record
                                else if(U8GlobalMBCount == 0)
                                {
                                    memcpy(&periodic_data_send_mb,&periodic_data_mb[g_Rec_idx_mb],sizeof(PERIODIC_LOG_STRUCT));
                                    last_sent_mb_record.rec_no = periodic_data_mb[g_Rec_idx_mb].Periodic_hdr.Id;

                                }
                            }
                        }
                        else //write
                        {
                            /*switch (data[index].data_type)
                            {
                            	 case bit16_int:
                            		 *(unsigned int *)data[index].screen_var_data = register_int16bit.uint_val;
                            		 break;

                            	 case bit32_int:
                            		 *(unsigned long *)data[index].screen_var_data = register_int32bit.ulong_val;
                            		 break;

                            	 case bit32_flt:
                            		 *(float *)data[index].screen_var_data = register_float32bit.float_val;
                            		 break;

                            	 case bit64_dbl:
                            		 *(double *)data[index].screen_var_data = register_double64bit.double_val;
                            		 break;
                            }*/
                            error_flag = 1; //write not allowed for screen type 7
                        }
                        break;

                    case 10: //read only holding registers
                        switch (data[index].data_type)
                        {
                        case bit16_int:
                            register_int16bit.uint_val = *(unsigned int *)data[index].screen_var_data;
                            break;

                        case bit32_int:
                            register_int32bit.ulong_val = *(unsigned long *)data[index].screen_var_data;
                            break;

                        case bit32_flt:
                            register_float32bit.float_val = *(float *)data[index].screen_var_data;
                            break;

                        case bit64_dbl:
                            register_double64bit.double_val = *(double *)data[index].screen_var_data;
                            break;
                        }
                        break;
                    }
                    if (read_write == READ_REG)
                    {
                        //store the data from the GUI parameter in the destination buffer according
                        //to the data type and in byte format
                        switch (data[index].data_type)
                        {
                        case bit16_int:
                            //since data is 16 bit long, 2 bytes are required to be sent
                            *pubRegBuffer++ = (UBYTE) register_int16bit.uint_array[1];
                            *pubRegBuffer++ = (UBYTE) register_int16bit.uint_array[0];
                            curr_address += 1;
                            break;

                        case bit32_int:
                            //since data is 32 bit long, 4 bytes are required to be sent
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[3];
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[2];
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[1];
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[0];
                            curr_address += 2;
                            break;

                        case bit32_flt:
                            //since data is 32 bit/float, 4 bytes are required to be sent
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[3];
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[2];
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[1];
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[0];
                            curr_address += 2;
                            break;

                        case bit64_dbl:
                            //since data is 64 bit/double, 8 bytes are required to be sent
                            //	if(data[index].modbus_reg_addr == TOTAL_WEIGHT)
                            if (curr_address<46028)
                            {
                                Double_to_Ints(&register_double64bit.double_val);
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[7];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[6];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[5];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[4];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[3];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[2];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[1];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[0];
                            }
                            else
                            {
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[1];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[0];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[3];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[2];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[5];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[4];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[7];
                                *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[6];

                            }

                            register_double64bit.double_val = 1;
                            curr_address += 4;
                            break;
                        }
                    }
                    if(error_flag != 1)
                    {
                        eException = MB_PDU_EX_NONE;
                    }
                    else if(error_flag == 1)
                    {
                        break;
                    }
                }
                else
                {
                    //generate an illegal address error if the index exceeds the maximum array index or if the current address
                    //does not correspond to the virtual address in the structure for that variable
                    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
                    break;
                }
            }//for loop
        } //if (curr_index != -1)
        else
        {
            //generate an illegal address error if the requested address is not found within the specified structure
            eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
        }
    } //if (address_valid == 1)


    if (usRegStart ==USB_HEALTH_STATUS)
    {
        if (eRegMode ==MBS_REGISTER_READ)
        {
            // USB_err_no = usbh_msc_get_last_error(0,0);
            if (con ==1) register_int16bit.uint_val	=0;
            else
                register_int16bit.uint_val	= USB_err_no;
            *pubRegBuffer++=register_int16bit.uint_array[1];
            *pubRegBuffer++=register_int16bit.uint_array[0];
        }
//         if (eRegMode ==MBS_REGISTER_WRITE)
//         {
//             register_int32bit.ulong_array[3] = *pubRegBuffer++;
//             register_int32bit.ulong_array[2] = *pubRegBuffer++;
//             register_int32bit.ulong_array[1] = *pubRegBuffer++;
//             register_int32bit.ulong_array[0] = *pubRegBuffer++;
//             curr_address += 2;
//             LPC_RTC->CALIBRATION =register_int32bit.ulong_val;
//         }
        eException = MB_PDU_EX_NONE;
    }
// 		if (usRegStart ==MASTER_TOTAL_POUND)
//     {
//         if (eRegMode ==MBS_REGISTER_READ)
//         {
// 							register_double64bit.double_val = Totals_var.Master_total;
// 							*pubRegBuffer++=register_double64bit.d_array[7];
// 							*pubRegBuffer++=register_double64bit.d_array[6];
// 							*pubRegBuffer++=register_double64bit.d_array[5];
// 							*pubRegBuffer++=register_double64bit.d_array[4];
// 							*pubRegBuffer++=register_double64bit.d_array[3];
// 							*pubRegBuffer++=register_double64bit.d_array[2];
// 							*pubRegBuffer++=register_double64bit.d_array[1];
// 							*pubRegBuffer++=register_double64bit.d_array[0];

//         }
// //         if (eRegMode ==MBS_REGISTER_WRITE)
// //         {
// //             register_int32bit.ulong_array[3] = *pubRegBuffer++;
// //             register_int32bit.ulong_array[2] = *pubRegBuffer++;
// //             register_int32bit.ulong_array[1] = *pubRegBuffer++;
// //             register_int32bit.ulong_array[0] = *pubRegBuffer++;
// //             curr_address += 2;
// //             LPC_RTC->CALIBRATION =register_int32bit.ulong_val;
// //         }
// eException = MB_PDU_EX_NONE;
//     }
    if (usRegStart ==RTC_CALIB_VAL_ADDR)
    {
        if (eRegMode ==MBS_REGISTER_READ)
        {

            register_int32bit.ulong_val	= LPC_RTC->CALIBRATION;
            *pubRegBuffer++=register_int32bit.ulong_array[3];
            *pubRegBuffer++=register_int32bit.ulong_array[2];
            *pubRegBuffer++=register_int32bit.ulong_array[1];
            *pubRegBuffer++=register_int32bit.ulong_array[0];
        }
        if (eRegMode ==MBS_REGISTER_WRITE)
        {
            register_int32bit.ulong_array[3] = *pubRegBuffer++;
            register_int32bit.ulong_array[2] = *pubRegBuffer++;
            register_int32bit.ulong_array[1] = *pubRegBuffer++;
            register_int32bit.ulong_array[0] = *pubRegBuffer++;
            curr_address += 2;
            LPC_RTC->CALIBRATION =register_int32bit.ulong_val;
        }
        eException = MB_PDU_EX_NONE;
    }

    /*	#ifdef DEBUG_DATA_SEND_1
      sprintf(chData_buf,"\r\nHolding Reg R/W: %d,%d,%d,%d",eRegMode, usNRegs,usAddress,eException);
    	Uart_i2c_data_Tx_1((uint8_t *)chData_buf, strlen(chData_buf));
    	#endif
    	*/

    return eException;
}

/*****************************************************************************
* @note       Function name  : eMBException tcp_read_input_reg( UBYTE * pubRegBuffer,
*                                                        USHORT usAddress, USHORT usNRegs)
* @returns    returns        : MB_PDU_EX_NONE - if address & value is correct, write is successful
*                            : MB_PDU_EX_ILLEGAL_DATA_ADDRESS - if address is incorrect
* @param      arg1           : Pointer to the buffer in which the read data is to be stored
*             arg2           : Virtual address of the modbus register
*             arg3           : Number of registers to be read
* @author                    : Anagha Basole
* @date       date created   : 3rd July 2013
* @brief      Description    : Read the input registers.(Calibration value registers &
*                            : diagnostic value registers)
*                            :
* @note       Notes          : If the MODBUS slave stack needs to now the values of input registers this
*                              callback function must store the current value of the registers starting
*                              at\c usAddress to <tt>usAddress + usNRegs</tt> (not including the last one)
*                              into the buffer \c pubRegBuffer. The 16 bit registers values must be stored
*                              in big endian format.
*
*                      \param pubRegBuffer A pointer to an internal buffer. Exactly
*                             <tt>2 * usNRegs</tt> bytes must be written to this buffer.
*                      \param usAddress The address of the first register which should be returned.
*                             Registers start at zero.
*                      \param usNRegs The number of registers to read.
*
*                      \return If the callback returns eMBException::MB_PDU_EX_NONE a response is
*                              sent back to the master. Otherwise an appropriate exception frame is
*                              generated.
*****************************************************************************/
eMBException tcp_read_input_reg( UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    MODBUS_TCP_VAR_STRUCT * data;
    unsigned int usRegStart = (usAddress + MODBUS_REGISTER_BASE_ADDR); //add the base address to the received address
    unsigned int usRegEnd = (usRegStart + usNRegs - 1); //calculate the end address
    unsigned int size = 0, index = 0, curr_address = 0, address_valid = 0;
    int curr_index = 0;
    union uint_union register_int16bit;
    union ulong_union register_int32bit;
    union float_union register_float32bit;
    union double_union register_double64bit;

    //41036 to 41046 - calibration variables
    if ((CAL_VAR_READ_ONLY_TCP_REG_START_ADDR <= usRegStart) &&
            (usRegStart <= CAL_VAR_READ_ONLY_TCP_REG_END_ADDR))
    {
        data = Scale_setup_calib_tcp_var;
        //store the maximum array index for navigation limits
        size = Size_structure.sizeof_scale_setup_cal_var;
        address_valid = 1;
    }
    //43001 to 43027 - reports and diagnostic variables
    else if ((REPORTS_DIAG_TCP_REG_START_ADDR <= usRegStart) &&
             (usRegStart <= REPORTS_DIAG_TCP_REG_END_ADDR))
    {
        data = Reports_diag_tcp_var;
        //store the maximum array index for navigation limits
        size = Size_structure.sizeof_reports_diag_var;
        address_valid = 1;
    }

    if(address_valid == 1)
    {
        //calculate the index w.r.t the starting address in the request, -1 will be returned if  address is not found
        curr_index = point_to_modbus_register (data, usRegStart, size);

        //if requested address is found within specified struture, then access the structure to perform the required action
        if (curr_index != -1)
        {
            //read all the required variables from the start address to the end address
            for(index = (unsigned int)curr_index, curr_address = usRegStart; curr_address <= usRegEnd; index++)
            {
                //check the index and verify that the index is within array size range and the current requested address
                //is present at that index
                if ((index < size) && (curr_address == data[index].modbus_reg_addr))
                {
                    //initialize all the variables
                    register_int16bit.uint_val = 0;
                    register_int32bit.ulong_val = 0;
                    register_float32bit.float_val = 0;
                    register_double64bit.double_val = 0;
                    //Read the variable, w.r.t the type of screen structure and variable type
                    switch (data[index].Screen_type)
                    {
                    case 2:
                    case 10:
                        //store the data from the GUI parameter in the destination buffer according
                        //to the data type and in byte format
                        switch (data[index].data_type)
                        {
                        case bit16_int:
                            register_int16bit.uint_val = *(unsigned int *)data[index].screen_var_data;
                            //since data is 16 bit long, 2 bytes is required to be sent
                            *pubRegBuffer++ = (UBYTE) register_int16bit.uint_array[1];
                            *pubRegBuffer++ = (UBYTE) register_int16bit.uint_array[0];
                            curr_address += 1;
                            break;

                        case bit32_int:
                            register_int32bit.ulong_val = *(unsigned long *)data[index].screen_var_data;
                            //since data is 32 bit long, 4 bytes are required to be sent
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[3];
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[2];
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[1];
                            *pubRegBuffer++ = (UBYTE) register_int32bit.ulong_array[0];
                            curr_address += 2;
                            break;

                        case bit32_flt:
                            register_float32bit.float_val = *(float *)data[index].screen_var_data;
                            //since data is 32 bit/float, 4 bytes are required to be sent
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[3];
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[2];
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[1];
                            *pubRegBuffer++ = (UBYTE) register_float32bit.f_array[0];
                            curr_address += 2;
                            break;

                        case bit64_dbl:
                            register_double64bit.double_val = *(double *)data[index].screen_var_data;
                            //since data is 64 bit/double, 8 bytes are required to be sent
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[7];
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[6];
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[5];
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[4];
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[3];
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[2];
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[1];
                            *pubRegBuffer++ = (UBYTE) register_double64bit.d_array[0];
                            curr_address += 4;
                            break;
                        }
                        break;
                    }
                    eException = MB_PDU_EX_NONE;
                }
                else
                {
                    //generate an illegal address error if the index exceeds the maximum array index or if the current address
                    //does not correspond to the virtual address in the structure for that variable
                    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
                    break;
                }
            }//for loop
        } //if (curr_index != -1)
        else
        {
            //generate an illegal address error if the requested address is not found within the specified structure
            eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
        }
    }

    return eException;
}

/*****************************************************************************
* @note       Function name  : eMBException tcp_read_write_coil(UBYTE * pubRegBuffer, USHORT usAddress,
                                                   USHORT usNRegs, eMBSRegisterMode eRegMode)
* @returns    returns        : MB_PDU_EX_NONE - if address & value is correct, write is successful
*                            : MB_PDU_EX_ILLEGAL_DATA_ADDRESS - if address is incorrect
*                            : MB_PDU_EX_ILLEGAL_FUNCTION - if an illegal write is done to a coil
* @param      arg1           : Pointer to the buffer which contains the data to be written
*             arg2           : Virtual address of the modbus coil
*             arg3           : Number of coils to be modified
*             arg4           : Whether to read/modify the coil
* @author                    : Anagha Basole
* @date       date created   : 3rd July 2013
* @brief      Description    : Write to the specific coils will trigger the corresponding
*                            : calibration. Also accept the calibration or stop calibration
*                            : depending on coil address. Also read the calibration status
*                            : to find out if the calibration is in progress or over.
*                            : Read the input-output status variables.
*                            :
* @note       Notes          : The callback may read or write up to 2000 coils where the first coil
*                              is address by the parameter \c usAddress. If the coils are read by
*                              the protocol stack the first coil should be written to the buffer
*                              \c pubRegBuffer where the first 8 coils should be written to
*                              pubRegBuffer[0], the second 8 coils to pubRegBuffer[1], ... If the
*                              total amount is not a multiple of 8 the missing coils should be
*                              set to zero.<br>
*                              If the coils are written by the protocol stack, which is indicated
*                              by the argument \c eRegMode set to eMBSRegisterMode::MBS_REGISTER_WRITE
*                              ,then the callback should update its coils with the values supplied
*                              in \c pubRegBuffer. The enconding is the same as above.
*
*                       \param pubRegBuffer If the values are read by the stack the callback
*                              should update the buffer. Otherwise the callback can read the new
*                              status of the coils from this buffer.
*                       \param usAddress Address of first coil.
*                       \param usNRegs Number of coils.
*                       \param eRegMode If set to eMBSRegisterMode::MBS_REGISTER_READ the
*                              coils are read by the protocol stack. In case of
*                              eMBSRegisterMode::MBS_REGISTER_WRITE the protocol stack needs to
*                              know the current value of the coil register.
*                      \return If the callback returns eMBException::MB_PDU_EX_NONE a response is
*                              sent back to the master. Otherwise an appropriate exception frame is
*                              generated.
*****************************************************************************/
extern int az_cal_on;	//SKS auto zero isable/enable flag
eMBException tcp_read_write_coil(UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs, eMBSRegisterMode eRegMode)
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    MODBUS_TCP_VAR_STRUCT * data;
    unsigned int usRegAddr = (usAddress + 1001); //add the base address to the virtual coil address
    unsigned int usRegEnd = (usRegAddr + usNRegs - 1); //calculate the end address
    unsigned int index = 0, curr_address = 0, coil_index = 0, coil_index_old = 0;
    int curr_index = 0;
    char ** dchar_data;
    static unsigned char cal_started = CLEAR_CAL_FLAG;

    /*#ifdef DEBUG_DATA_SEND_1
    char chData_buf[50];
    #endif
     */
    //write coil functionality
    if(eRegMode == MBS_REGISTER_WRITE)
    {
        //switch the coils/calibration depending on the address
        switch(usRegAddr)
        {
        case DYN_ZERO_COIL_STATUS_RD_CAL_ADDR:
            eException = MB_PDU_EX_NONE;
            break;
        //PVK Added 27 Apr 2016
        case DYN_ZERO_COIL_ACCEPT_STATUS_CAL_ADDR:
            eException = MB_PDU_EX_NONE;
            break;
        //PVK Added 27 Apr 2016
        case DYN_ZERO_COIL_REJECT_STATUS_CAL_ADDR:
            eException = MB_PDU_EX_NONE;
            break;

        case TEST_WEIGHT_COIL_START_CAL_ADDR:  //Test Weight - Start Calibration - COIL(OFF to ON)
            //start the test weight calibration if no other calibration is going on and if the coil is
            //turned on
            // Firmware upgrade testing purpose
// 						   strcpy(Admin_var.Int_update_file_name,"Beltway_Integrator.hex");
// 						   FwUpgrade(Admin_var.Int_update_file_name, sizeof(Admin_var.Int_update_file_name));
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == CLEAR_CAL_FLAG))
            {
                //set the flag to begin test weight calibration and send no exception response
                Calib_struct.Cal_status_flag = START_TEST_WEIGHT_CAL;
                eException = MB_PDU_EX_NONE;
                cal_started = START_TEST_WEIGHT_CAL_FLAG;
                g_coil_buff[TEST_WEIGHT_COIL_START_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case TEST_WEIGHT_COIL_ACCEPT_CAL_ADDR:  //Test Weight - Accept Calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_TEST_WEIGHT_CAL_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                cal_started = CLEAR_CAL_FLAG;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case TEST_WEIGHT_COIL_ACC_RPT_CAL_ADDR:  //Test Weight - Accept and repeat calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_TEST_WEIGHT_CAL_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                //set the flag to restart test weight calibration
                Calib_struct.Cal_status_flag = START_TEST_WEIGHT_CAL;
                g_coil_buff[TEST_WEIGHT_COIL_ACC_RPT_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case TEST_WEIGHT_COIL_REJ_RPT_CAL_ADDR:  //Test Weight - Reject and repeat calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_TEST_WEIGHT_CAL_FLAG))
            {
                //set the flag to restart test weight calibration and send no exception response
                eException = MB_PDU_EX_NONE;
                Calib_struct.Cal_status_flag = START_TEST_WEIGHT_CAL;
                g_coil_buff[TEST_WEIGHT_COIL_REJ_RPT_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case MATERIAL_TEST_COIL_START_CAL_ADDR:  //Material Test - Start Calibration - COIL(OFF to ON)
            //start the material test calibration if no other calibration is going on and if the coil is
            //turned on
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == CLEAR_CAL_FLAG))
            {
                //set the flag to begin material test calibration and send no exception response
                Calib_struct.Cal_status_flag = START_MATERIAL_CAL;
                eException = MB_PDU_EX_NONE;
                cal_started = START_MATERIAL_CAL_FLAG;
                g_coil_buff[MATERIAL_TEST_COIL_START_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case MATERIAL_TEST_COIL_ACCEPT_CAL_ADDR:  //Material Test - Accept calibration - COIL(ON to OFF)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_MATERIAL_CAL_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                cal_started = CLEAR_CAL_FLAG;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case MATERIAL_TEST_COIL_ACC_RPT_CAL_ADDR:  //Material Test - Accept and repeat calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_MATERIAL_CAL_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                //set the flag to begin material test calibration
                Calib_struct.Cal_status_flag = START_MATERIAL_CAL;
                g_coil_buff[MATERIAL_TEST_COIL_ACC_RPT_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case MATERIAL_TEST_COIL_REJ_RPT_CAL_ADDR:  //Material Test - Reject and repeat calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_MATERIAL_CAL_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //set the flag to begin material test calibration
                Calib_struct.Cal_status_flag = START_MATERIAL_CAL;
                g_coil_buff[MATERIAL_TEST_COIL_REJ_RPT_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case DIGITAL_COIL_START_CAL_ADDR:  //Digital Calibration - Start calibration - COIL(OFF to ON)
            //start the digital calibration if no other calibration is going on and if the coil is turned on
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == CLEAR_CAL_FLAG))
            {
                //set the flag to begin digital calibration and send no exception response
                Calib_struct.Cal_status_flag = START_DIGITAL_CAL;
                eException = MB_PDU_EX_NONE;
                cal_started = START_DIGITAL_CAL_FLAG;
                g_coil_buff[DIGITAL_COIL_START_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case DIGITAL_COIL_ACCEPT_CAL_ADDR:  //Digital Calibration - Accept calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_DIGITAL_CAL_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                cal_started = CLEAR_CAL_FLAG;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_START_CAL_ADDR:  //Length & zero cal - Start calibration - COIL(OFF to ON)
            //start the length and zero calibration if no other calibration is going on and if the coil is
            //turned on
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == CLEAR_CAL_FLAG))
            {
                //set the flag to begin length and zero calibration and send no exception response
                Calib_struct.Cal_status_flag = START_LENZERO_CAL;
                eException = MB_PDU_EX_NONE;
                cal_started = START_LENZERO_CAL_FLAG;
                g_coil_buff[LEN_ZERO_COIL_START_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_STOP_CAL_ADDR: //Length & zero cal - Stop length calibration - COIL(OFF to ON)
            //stop the length and zero calibration if it is in progress and if the coil is turned on
            if ((Calib_struct.Cal_status_flag == START_LENZERO_CAL) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_LENZERO_CAL_FLAG))
            {
                //set the flag to stop length and zero calibration and send no exception response
                Calib_struct.BeltLenCal_Comp = 1;
                eException = MB_PDU_EX_NONE;
                cal_started = LENZERO_CAL_DONE;
            }
            else
            {
                //send an illegal function if calibration is not yet started or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_ACCEPT_LEN_CAL_ADDR:  //Length & zero cal - Accept length calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == LENZERO_CAL_DONE))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Belt_length_calc = Calibration_var.New_belt_length * \
                                                      Calculation_struct.Belt_length_conv_factor; //in inches
                cal_started = LEN_ZER_LEN_CAL_ACPT_FLAG;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_ACC_RPT_LEN_CAL_ADDR:  //Length & zero cal - Accept & repeat length calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == LENZERO_CAL_DONE))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Belt_length_calc = Calibration_var.New_belt_length * \
                                                      Calculation_struct.Belt_length_conv_factor; //in inches
                //set the flag to begin length and zero calibration
                Calib_struct.Cal_status_flag = START_LENZERO_CAL;
                cal_started = START_LENZERO_CAL_FLAG;
                g_coil_buff[LEN_ZERO_COIL_ACC_RPT_LEN_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_REJ_RPT_LEN_CAL_ADDR: //Length & zero cal - Reject & repeat length calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == LENZERO_CAL_DONE))
            {
                //set the flag to restart length and zero calibration and send no exception response
                eException = MB_PDU_EX_NONE;
                Calib_struct.Cal_status_flag = START_LENZERO_CAL;
                cal_started = START_LENZERO_CAL_FLAG;
                g_coil_buff[LEN_ZERO_COIL_REJ_RPT_LEN_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_ACCEPT_ZERO_CAL_ADDR: //Length & zero cal - Accept zero calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == LEN_ZER_LEN_CAL_ACPT_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
                                                 / Calculation_struct.Zero_value_conv_factor;
                cal_started = CLEAR_CAL_FLAG;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_ACC_RPT_ZERO_LEN_CAL_ADDR: //Length & zero cal - Accept & repeat zero calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == LEN_ZER_LEN_CAL_ACPT_FLAG))
            {
                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;
                //GUI_data_nav.GUI_structure_backup = 1;
                //Added by PVK on 2 May 2016
                Modbus_Data_Write_Cmd_recd = 1;
                Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
                                                 / Calculation_struct.Zero_value_conv_factor;
                //set the flag to begin length and zero calibration
                Calib_struct.Cal_status_flag = START_LENZERO_CAL;
                cal_started = START_LENZERO_CAL_FLAG;
                g_coil_buff[LEN_ZERO_COIL_ACC_RPT_ZERO_LEN_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case LEN_ZERO_COIL_REJ_RPT_ZERO_CAL_ADDR: //Length & zero cal - Reject & repeat zero calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == LEN_ZER_LEN_CAL_ACPT_FLAG))
            {
                //set the flag to restart length and zero calibration and send no exception response
                eException = MB_PDU_EX_NONE;
                Calib_struct.Cal_status_flag = START_LENZERO_CAL;
                cal_started = START_LENZERO_CAL_FLAG;
                g_coil_buff[LEN_ZERO_COIL_REJ_RPT_ZERO_CAL_ADDR - COIL_BASE_ADDRESS] = COIL_ON;
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case DYN_ZERO_COIL_START_CAL_ADDR:  //Dynamic Zero cal - Start zero calibration - COIL(OFF to ON)
            //start the dynamic zero calibration if no other calibration is going on and if the coil is
            //turned on
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == CLEAR_CAL_FLAG))
                /*if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && \
                				  ((g_coil_buff[DYN_ZERO_COIL_START_CAL_ADDR-COIL_BASE_ADDRESS]) == COIL_OFF) &&
                    (cal_started == CLEAR_CAL_FLAG))*/
            {
                //PVK added 27 Apr 2016 clear the status of coils used for accept dynamic zero cal
                g_coil_buff[DYN_ZERO_COIL_ACCEPT_STATUS_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_OFF; //26
                g_coil_buff[DYN_ZERO_COIL_REJECT_STATUS_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_OFF; //27

                //set the flag to begin dynamic zero calibration and send no exception response
                Calib_struct.Cal_status_flag = START_DYNAMICZERO_CAL;
                eException = MB_PDU_EX_NONE;
                cal_started = START_DYNAMICZERO_CAL_FLAG;
                g_coil_buff[DYN_ZERO_COIL_START_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_ON;			//23
                g_coil_buff[DYN_ZERO_COIL_STATUS_RD_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_ON;	//25
            }
            else
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                //eException = MB_PDU_EX_ILLEGAL_FUNCTION;
                //eException = MB_PDU_EX_SLAVE_DEVICE_FAILURE;
                eException = MB_PDU_EX_NONE;
            }
            break;

        case DYN_ZERO_COIL_ACCEPT_CAL_ADDR:  //Dynamic Zero cal - Accept zero calibration - COIL(OFF to ON)
            if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_ON) &&
                    (cal_started == START_DYNAMICZERO_CAL_FLAG))
                //if (Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG)
            {
                // if(cal_started == START_DYNAMICZERO_CAL_FLAG)
                {
                    //GUI_data_nav.GUI_structure_backup = 1;
                    Modbus_Data_Write_Cmd_recd = 1;
                    Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
                                                     / Calculation_struct.Zero_value_conv_factor;
                    cal_started = CLEAR_CAL_FLAG;
                }

                g_coil_buff[DYN_ZERO_COIL_ACCEPT_STATUS_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_ON; //26

                //set the flag to store calibration results and send no exception response
                eException = MB_PDU_EX_NONE;

                //g_coil_buff[DYN_ZERO_COIL_START_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_ON;
                // g_coil_buff[DYN_ZERO_COIL_STATUS_RD_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_OFF;
            }
            else if ((Calib_struct.Cal_status_flag == START_DYNAMICZERO_CAL) && (((*pubRegBuffer) == COIL_ON) ||((*pubRegBuffer) == COIL_OFF))
                     && (cal_started == START_DYNAMICZERO_CAL_FLAG))
            {
                //send an illegal function if calibration is not yet complete or if wrong status
                //is written to the coil
                //eException = MB_PDU_EX_ILLEGAL_FUNCTION;
                //eException = MB_PDU_EX_SLAVE_DEVICE_FAILURE;
                //eException = MB_PDU_EX_NONE;
                eException = MB_PDU_EX_SLAVE_BUSY;
            }
            //Recevied reject command and not accepted the previous cal command
            else if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && ((*pubRegBuffer) == COIL_OFF) &&
                     (cal_started == START_DYNAMICZERO_CAL_FLAG))
            {
                Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
                g_coil_buff[DYN_ZERO_COIL_REJECT_STATUS_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_ON; //27
                cal_started = CLEAR_CAL_FLAG;

                //BS-195 Periodic Log duplicate record numbers, bad data , missing entry
                //Added by PVK on 2 May 2016 populate the cancel calibartion log into period log
                Cal_Reject_Cmd_recd = 1 ;

                eException = MB_PDU_EX_NONE;
            }
            else
            {
                eException = MB_PDU_EX_NONE;
            }
            az_cal_on =0;
            break;

        case TEST_WEIGHT_COIL_REJ_CNCL_CAL_ADDR: //Test Weight - Reject and cancel calibration - COIL(OFF to ON)
        case MATERIAL_TEST_COIL_REJ_CNCL_CAL_ADDR: //Material Test - Reject and cancel calibration - COIL(OFF to ON)
        case LEN_ZERO_COIL_REJ_CNCL_LEN_CAL_ADDR: //Length & zero cal - Reject & cancel length calibration - COIL(OFF to ON)
        case LEN_ZERO_COIL_REJ_CNCL_ZERO_CAL_ADDR: //Length & zero cal - Reject & cancel zero calibration - COIL(OFF to ON)
            if (((*pubRegBuffer) == COIL_ON) &&
                    ((cal_started == START_TEST_WEIGHT_CAL_FLAG) ||
                     (cal_started == START_MATERIAL_CAL_FLAG) ||
                     (cal_started == START_DIGITAL_CAL_FLAG) ||
                     (cal_started == START_LENZERO_CAL_FLAG) ||
                     (cal_started == LENZERO_CAL_DONE) ||
                     (cal_started == LEN_ZER_LEN_CAL_ACPT_FLAG) ||
                     (cal_started == START_DYNAMICZERO_CAL_FLAG)))
            {
                //clear the flag to stop calibration and send no exception response
                eException = MB_PDU_EX_NONE;
                Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
                Calib_struct.Start_flag = __FALSE;

                //BS-195 Periodic Log duplicate record numbers, bad data , missing entry
                //Added by PVK on 2 May 2016 populate the cancel calibartion log into period log
                Cal_Reject_Cmd_recd = 1;
                az_cal_on =0;
                cal_started = CLEAR_CAL_FLAG;
                switch(usRegAddr)
                {
                case TEST_WEIGHT_COIL_REJ_CNCL_CAL_ADDR:
                    g_coil_buff[TEST_WEIGHT_COIL_START_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_OFF;
                    break;
                case MATERIAL_TEST_COIL_REJ_CNCL_CAL_ADDR:
                    g_coil_buff[MATERIAL_TEST_COIL_START_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_OFF;
                    break;
                case LEN_ZERO_COIL_REJ_CNCL_LEN_CAL_ADDR:
                    g_coil_buff[LEN_ZERO_COIL_START_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_OFF;
                    break;
                case LEN_ZERO_COIL_REJ_CNCL_ZERO_CAL_ADDR:
                    g_coil_buff[LEN_ZERO_COIL_START_CAL_ADDR-COIL_BASE_ADDRESS] = COIL_OFF;
                    break;
                default:
                    break;
                }
            }
            else
            {
                //send an illegal function if wrong status is written to the coil
                eException = MB_PDU_EX_ILLEGAL_FUNCTION;
            }
            break;

        case PLANT_CONNECT_NEW_RECORD_DATA:
            if ((*pubRegBuffer) == COIL_OFF)
            {
                //clear the flag for the new record available
                Flags_struct.Plant_connect_record_flag &= ~NEW_PLANT_CONNECT_RECORD_FLAG;
                //clear the flag to stop calibration and send no exception response
                eException = MB_PDU_EX_NONE;
            }
            break;

        case PLANT_CONNECT_OLD_RECORD_DATA:
            if ((*pubRegBuffer) == COIL_OFF)
            {
                //clear the flag for the new record available
                Flags_struct.Plant_connect_record_flag &= ~OLD_PLANT_CONNECT_RECORD_FLAG;
                //clear the flag to stop calibration and send no exception response
                eException = MB_PDU_EX_NONE;
            }
            else if ((*pubRegBuffer) == COIL_ON)
            {
                //set the flag for the new record available
                Flags_struct.Plant_connect_record_flag |= OLD_PLANT_CONNECT_RECORD_FLAG;
                //clear the flag to stop calibration and send no exception response
                eException = MB_PDU_EX_NONE;
            }
            break;
        } //switch case
    }//if(eRegMode == MBS_REGISTER_WRITE)
    else if(eRegMode == MBS_REGISTER_READ)
    {
        if ((usRegAddr >= TEST_WEIGHT_COIL_START_CAL_ADDR) && (usRegAddr <= COILS_END_ADDR))
            //	(usRegAddr <= DYN_ZERO_COIL_STATUS_RD_CAL_ADDR))
            //(usRegAddr <= DYN_ZERO_COIL_ACCEPT_CAL_ADDR))
        {
            eException = MB_PDU_EX_NONE;
            coil_index = coil_index_old = 0;
            pubRegBuffer[coil_index] = 0;
            for(curr_address = usRegAddr; ( (curr_address >= TEST_WEIGHT_COIL_START_CAL_ADDR) && (curr_address <= usRegEnd) && (curr_address <= /*DYN_ZERO_COIL_STATUS_RD_CAL_ADDR*/COILS_END_ADDR)); index++, coil_index_old++)
            {
                if (coil_index_old == 8)
                {
                    coil_index++;
                    coil_index_old = 0;
                    pubRegBuffer[coil_index] = 0;
                }



                if ((Calib_struct.Cal_status_flag == START_TEST_WEIGHT_CAL) ||
                        (Calib_struct.Cal_status_flag == START_MATERIAL_CAL) ||
                        (Calib_struct.Cal_status_flag == START_DIGITAL_CAL) ||
                        (Calib_struct.Cal_status_flag == START_LENZERO_CAL) ||
                        (Calib_struct.Cal_status_flag == START_DYNAMICZERO_CAL))
                {

                    pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | ((g_coil_buff[curr_address-COIL_BASE_ADDRESS]) << ( coil_index_old)));	//(coil_index+1) *
                }
                else if (Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG)
                {
                    //if calibration has not started or calibration has finished then send COIL_OFF
                    //Added by PVK 27 Apr 2016 do not clear the coil status of dynamic zero cal
                    if(!((curr_address == DYN_ZERO_COIL_ACCEPT_STATUS_CAL_ADDR) || (curr_address == DYN_ZERO_COIL_REJECT_STATUS_CAL_ADDR)))
                    {
                        g_coil_buff[curr_address-COIL_BASE_ADDRESS] = COIL_OFF;
                    }
                    pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | ((g_coil_buff[curr_address-COIL_BASE_ADDRESS]) << ( coil_index_old)));	//(coil_index+1) *
                }
                else
                {
                    //send an illegal function if wrong status is written to the coil
                    // eException = MB_PDU_EX_ILLEGAL_FUNCTION;
                    eException = MB_PDU_EX_NONE;
                    // break;
                }
                curr_address += 1;
            }//for
        } //if
        else if ((usRegAddr >= REPORTS_DIAG_TCP_STATUS_START_ADDR) &&
                 (usRegAddr <= REPORTS_DIAG_TCP_STATUS_END_ADDR))
        {
            data = Reports_diag_tcp_var;

            //calculate the index w.r.t the starting address in the request
            curr_index = point_to_modbus_register (data, usRegAddr, Size_structure.sizeof_reports_diag_var);

            //if requested address is found within specified struture, then access the structure to perform the required action
            if (curr_index != -1)
            {
                coil_index = coil_index_old = 0;
                pubRegBuffer[coil_index] = 0;
                //read all the required variables from the start address to the end address
                for(index = (unsigned int)curr_index, curr_address = usRegAddr; curr_address <= usRegEnd; index++, coil_index_old++)
                {
                    //check the index and verify that the index is within array size range and the current requested address
                    //is present at that index
                    if ((index < Size_structure.sizeof_reports_diag_var) && (curr_address == data[index].modbus_reg_addr))
                    {
                        if (coil_index_old == 8)
                        {
                            coil_index++;
                            coil_index_old = 0;
                            pubRegBuffer[coil_index] = 0;
                        }
                        dchar_data = data[index].screen_var_data;
                        //If the status of the port is 'off' send 0 else if 'on' send 1
                        if (strcmp(*dchar_data, Strings[Port_string1].Text) == 0) //on
                        {
                            pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | (COIL_ON << coil_index_old));
                        }
                        else
                        {
                            pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | (COIL_OFF << coil_index_old));
                        }
                        curr_address += 1;
                        eException = MB_PDU_EX_NONE;
                    }
                }
            }//if (curr_index != -1)
        }
        else if ((usRegAddr >= PLANT_CONNECT_STATUS_START_ADDRESS) &&
                 (usRegAddr <= PLANT_CONNECT_STATUS_END_ADDRESS))
        {
            //Currently status for only two coils can be requested
            if (usRegEnd <= PLANT_CONNECT_STATUS_END_ADDRESS)
            {
                eException = MB_PDU_EX_NONE;
//                 coil_index = coil_index_old = 0;
                pubRegBuffer[coil_index] = 0;
                coil_read(pubRegBuffer, usRegAddr, usNRegs, &Flags_struct.Plant_connect_record_flag);
                /*   coil_index = coil_index_old = 0;  //commented out for testing on 07/09/2013
                	 for(curr_address = usRegAddr; curr_address <= usRegEnd; index++, coil_index_old++)
                	 {
                			if (coil_index_old == 8)
                			{
                				 coil_index++;
                				 coil_index_old = 0;
                				 pubRegBuffer[coil_index] = 0;
                			}
                			switch(curr_address)
                			{
                				 case PLANT_CONNECT_NEW_RECORD_DATA:
                					 if ((Flags_struct.Plant_connect_record_flag & NEW_PLANT_CONNECT_RECORD_FLAG) == NEW_PLANT_CONNECT_RECORD_FLAG)
                					 {
                						  //if calibration has started then send COIL_ON
                						  pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | (COIL_ON << coil_index_old));
                					 }
                					 else
                					 {
                						  //if calibration has not started or calibration has finished then send COIL_OFF
                						  pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | (COIL_OFF << coil_index_old));
                					 }
                					 break;

                				 case PLANT_CONNECT_OLD_RECORD_DATA:
                					 if ((Flags_struct.Plant_connect_record_flag & OLD_PLANT_CONNECT_RECORD_FLAG) == OLD_PLANT_CONNECT_RECORD_FLAG)
                					 {
                						  //if calibration has started then send COIL_ON
                						  pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | (COIL_ON << coil_index_old));
                					 }
                					 else
                					 {
                						  //if calibration has not started or calibration has finished then send COIL_OFF
                						  pubRegBuffer[coil_index] = (pubRegBuffer[coil_index] | (COIL_OFF << coil_index_old));
                					 }
                				   break;

                					 default:
                						 curr_address = usRegEnd;
                						 break;
                			}
                			curr_address += 1;
                	} //for
                	*/
            }//if(usRegEnd)
            else
            {
                eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
            }
        } //else if
    }//else if register_read


    /*#ifdef DEBUG_DATA_SEND_1
    sprintf(chData_buf,"\r\nCoil R/W: %d,%d,%d,%d",eRegMode, usNRegs,usAddress,eException);
    Uart_i2c_data_Tx_1((uint8_t *)chData_buf, strlen(chData_buf));
    #endif
    */
    return eException;
}
//  Below function is added for testing purpose by venkat on 7/9/2013
/*****************************************************************************
* @note       Function name  : coil_read(UBYTE * pubRegBuffer, unsigned int sREG, USHORT coilcount,UBYTE *coilflags)
* @returns    returns        :
* @param      arg1           : Pointer to the buffer which contains the data to be written
*             arg2           : Virtual address of the modbus coil
*             arg3           : Number of coils to be modified
*             arg4           : Whether to read/modify the coil
* @author                    : Venkata Krishna Rao Racharla
* @date       date created   : 7th Sept 2013
* @brief      Description    :
*                            :
* @note       Notes          :
*****************************************************************************/
void coil_read(UBYTE * pubRegBuffer, unsigned int sREG, USHORT coilcount,UBYTE *coilflags)
{
    unsigned char nbyte=0;   // Number of bytes in response message
    unsigned char rindex=0;  // Index of Register flag Array
    unsigned char bit=0;     // Bit Position of Register flag byte
    unsigned char nbits=8;   // Number of bits in the Flag byte
    unsigned char bindex=0;  // Response Message Buffer Index
    unsigned char shift=0;   // Shift required if starting coil
    // address is not equal to starting
    // value of range
    unsigned char bbit;      // Buffer mesage byte index
    if(sREG >= 5001 && sREG <= 5002)	 //To find the starting index of flag register
        rindex = (sREG - 5001)/8;
    if(coilcount%8 == 0)         //Number of bytes in response message
    {
        nbyte = coilcount/8;
    }
    else
    {
        nbyte = (coilcount/8) + 1;
    }
    nbyte += rindex;  // Update the Number of bytes in respone message based on starting index
    shift = (sREG - 5001)%8;
    bit = shift;      //To find starting bit position in regflag
    for(; rindex<nbyte; bindex++)
    {
        if(rindex == nbyte-1 && coilcount%8 != 0)   //If it is last byte of buff
        {
            nbits = coilcount%8;  //Number of bits in last byte of message
        }
        // This loop copies byte from regflag array to buffer message
        for(bbit=0; bbit<nbits; bit++,bbit++)
        {
            //copy bits from shift bit to last bit
            if(bit >= shift && bit < nbits+shift)
            {
                pubRegBuffer[bindex] |= (coilflags[rindex] & (1<<bit)) >> shift;
            }
            //this block executes when last bit of the regflag byte is reached
            if(shift != 0 && bit == nbits-1)
            {
                rindex++;  // Next byte of register flag array
                bit = 0;
            }
            // last bit to shift bit of register flag
            if(bit < shift)
            {
                pubRegBuffer[bindex] |= (coilflags[rindex] & (1<<bit)) << (nbits - shift);
            }
        }
        if(shift == 0)
        {
            rindex++;  // Next byte of register flag array
            bit = 0;
        }
    }
}

/*****************************************************************************
* @note       Function name  : Double_to_Ints(double *value)
* @returns    returns        :
* @param      arg1           : Pointer to the double variable which contains the register value argument
															 should be element of Union type "double_union"
*
* @author                    : Venkata Krishna Rao Racharla
* @date       date created   : 31st Oct 2013
							Requirment		 : When Modbus Client puts a Read Holding or Input register request for a double variable like
                               weight, rate etc we need to send response modbus packet with 8 bytes output(i.e 4 registers)
                               higher four bytes (2 registers) response packet's data field should represent value after
                               deicmal point and lower four bytes (2 registers) of response packet's data field should
															 represent value before decimal point.
* @brief      Description    : This function converts double value to two integer values,
*                              one integer represents value before decimal point and another integer
*															 represents value after decipoint. Please see below example
                               Double_value = 1111111.22222
                               after convertion we will have following two integer values
                               Integer_value1 = 1111111
															 Integer_value2 = 22222
                               Once we get two 32 bit integer values we will put these value in a 8 byte temp array
                               Lower 4 bytes of temp array represent Integer_value1
                               Higher 4 bytes of temp array represent Integer_value2
*                            :
* @note       Notes          : Modbus always follows Bigendian format, So check byte ordering in client side accordingly.
*****************************************************************************/

void Double_to_Ints(double *value)
{
    unsigned char i=0,j;  // Loop counters
    unsigned char temp[8]; // Temporary 8 byte array represents 8 bytes data field in response packet.
    //By DK on 5May2016 to fix Kinder Morgan issue of not getting -ve Total
    INT32_DATA temp_int1; // signed Integer Union Variable
    //UINT32_DATA temp_int2; // Unsigned Integer Union Variable
    char str[25],substr[15];

    if(*value > UINT32_MAX) //When Significant value is greater than maximum value of unsigned int range then value is intialized to Max.
    {
        *value = UINT32_MAX;
    }
    /*else if(*value < 0.0)     //When Significant value is less than min value of unsigned int range then value is intialized to Min.
    {
     *value = 0.0;
    }*/
    else if(*value < (-INT32_MAX))     //When Significant value is less than min value of unsigned int range then value is intialized to Min.
    {
        *value = (-INT32_MAX);
    }

    sprintf(str,"%11.6lf",*value);
    while(str[i] != '.')   // This loop will store digits before decimal point in an array
    {
        substr[i] = str[i];
        i++;
    }
    substr[i] = '\0';

    //temp_int.value = strtoul(substr,NULL,10); // Converting the string into unsigned integer
    //temp_int.value = strtoull(substr,NULL,10); // Converting the string into unsigned integer
    temp_int1.value = strtoll(substr,NULL,10); // Converting the string into unsigned integer


    /* if(temp_int1.value >= 0.0)
     {
    	 temp_int2.value = strtoull(substr,NULL,10); // Converting the string into unsigned integer
    }*/

    for(j=0; j<4; j++)
    {
        /* if(temp_int1.value >= 0.0)
         temp[j+4] =  temp_int2.int_array[j];
         else
         temp[j+4] =  temp_int1.int_array[j];	*/
        temp[j+4] =  temp_int1.int_array[j];
    }
    memset(substr,0,sizeof(substr));
    j=++i;
    while(str[i] != '\0') // This loop will store digits after decimal point in an array
    {
        substr[i-j] = str[i];
        i++;
    }

    //temp_int.value=strtoul(substr,NULL,10);  // Converting the string into unsigned integer
    //temp_int2.value=strtoull(substr,NULL,10);  // Converting the string into unsigned integer
    temp_int1.value=strtoll(substr,NULL,10);  // Converting the string into unsigned integer

    if(*value < 0.0)
    {
        temp_int1.value = temp_int1.value * (-1.0);
    }

    for(j=0; j<4; j++)
    {
        //	temp[j] =  temp_int2.int_array[j];
        temp[j] =  temp_int1.int_array[j];
    }
    memcpy(value,temp,sizeof(temp));   // 8 bytes of data is copied to double variable.
}
#endif //#ifdef MODBUS_TCP
/*****************************************************************************
* End of file
*****************************************************************************/
