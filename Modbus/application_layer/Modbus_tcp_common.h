/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Modbus_tcp_common.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef  MODBUS_TCP_COMMON_H
#define  MODBUS_TCP_COMMON_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"

#ifdef MODBUS_TCP
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define MODBUS_REGISTER_BASE_ADDR                40001

#define SCALE_RUNTIME_TCP_REG_START_ADDR         40001
#define SCALE_RUNTIME_TCP_REG_END_ADDR           40086

#define TOTAL_WEIGHT														 40001
#define DAILY_WEIGHT	  												 40050
#define WEEKLY_WEIGHT	  												 40054
#define MONTHLY_WEIGHT	  											 40058
#define YEARLY_WEIGHT													   40062
#define JOB_TOTAL																 40066
#define MASTER_TOTAL														 40070


#define MASTER_TOTAL_POUND										 	47004
#define USB_HEALTH_STATUS										 		47008

#define DAILY_TOTAL_POUND												47010
#define ZERO_NUMBER_POUND												47014				
#define SPEED_FEET_MIN													47018
#define INST_RATE_POUNDS_HOUR										47019


//SKS -> RTC CAlibration over MODBUS TCP
#define RTC_CALIB_VAL_ADDR											47001
#define RTC_CALIB_DIR_ADDR											47002

//<-

#define SCALE_SETUP_CAL_TCP_REG_START_ADDR       41001
#define SCALE_SETUP_CAL_TCP_REG_END_ADDR         41035

#define CAL_VAR_READ_ONLY_TCP_REG_START_ADDR     41036
#define CAL_VAR_READ_ONLY_TCP_REG_END_ADDR       41046

#define SETUP_DEVICE_TCP_REG_START_ADDR          42001
#define SETUP_DEVICE_TCP_REG_END_ADDR            42145

#define REPORTS_DIAG_TCP_REG_START_ADDR          43001
#define REPORTS_DIAG_TCP_REG_END_ADDR            43027
#define REPORTS_DIAG_TCP_STATUS_START_ADDR       2001
#define REPORTS_DIAG_TCP_STATUS_END_ADDR         2016

#define ADMIN_TCP_REG_START_ADDR                 44001
#define ADMIN_TCP_REG_END_ADDR                   44029

#define PLANT_CONNECT_TCP_REG_START_ADDR         45001
#define PLANT_CONNECT_TCP_REG_END_ADDR           46044

#define PLANT_CONNECT_STATUS_START_ADDRESS       5001
#define PLANT_CONNECT_STATUS_END_ADDRESS         5002

//test calibration
#define COIL_BASE_ADDRESS												 1001	//Virtual coil base address

#define TEST_WEIGHT_COIL_START_CAL_ADDR          1001 //Test Weight - Start Calibration - COIL(OFF to ON)
#define TEST_WEIGHT_COIL_ACCEPT_CAL_ADDR         1002 //Test Weight - Accept Calibration - COIL(OFF to ON)
#define TEST_WEIGHT_COIL_ACC_RPT_CAL_ADDR        1003 //Test Weight - Accept and repeat calibration - COIL(OFF to ON)
#define TEST_WEIGHT_COIL_REJ_RPT_CAL_ADDR        1004 //Test Weight - Reject and repeat calibration - COIL(OFF to ON)
#define TEST_WEIGHT_COIL_REJ_CNCL_CAL_ADDR       1005 //Test Weight - Reject and cancel calibration - COIL(OFF to ON)

//material test calibration
#define MATERIAL_TEST_COIL_START_CAL_ADDR        1006 //Material Test - Start Calibration - COIL(OFF to ON)
#define MATERIAL_TEST_COIL_ACCEPT_CAL_ADDR       1007 //Material Test - Accept calibration - COIL(OFF to ON)
#define MATERIAL_TEST_COIL_ACC_RPT_CAL_ADDR      1008 //Material Test - Accept and repeat calibration - COIL(OFF to ON)
#define MATERIAL_TEST_COIL_REJ_RPT_CAL_ADDR      1009 //Material Test - Reject and repeat calibration - COIL(OFF to ON)
#define MATERIAL_TEST_COIL_REJ_CNCL_CAL_ADDR     1010 //Material Test - Reject and cancel calibration - COIL(OFF to ON)

//digital calibration
#define DIGITAL_COIL_START_CAL_ADDR              1011 //Digital Calibration - Start calibration - COIL(OFF to ON)
#define DIGITAL_COIL_ACCEPT_CAL_ADDR             1012 //Digital Calibration - Accept calibration - COIL(OFF to ON)

//length and zero calibration
#define LEN_ZERO_COIL_START_CAL_ADDR             1013 //Length & zero cal - Start calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_STOP_CAL_ADDR              1014 //Length & zero cal - Stop length calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_ACCEPT_LEN_CAL_ADDR        1015 //Length & zero cal - Accept length calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_ACC_RPT_LEN_CAL_ADDR       1016 //Length & zero cal - Accept & repeat length calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_REJ_RPT_LEN_CAL_ADDR       1017 //Length & zero cal - Reject & repeat length calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_REJ_CNCL_LEN_CAL_ADDR      1018 //Length & zero cal - Reject & cancel length calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_ACCEPT_ZERO_CAL_ADDR       1019 //Length & zero cal - Accept zero calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_ACC_RPT_ZERO_LEN_CAL_ADDR  1020 //Length & zero cal - Accept & repeat zero calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_REJ_RPT_ZERO_CAL_ADDR      1021 //Length & zero cal - Reject & repeat zero calibration - COIL(OFF to ON)
#define LEN_ZERO_COIL_REJ_CNCL_ZERO_CAL_ADDR     1022 //Length & zero cal - Reject & cancel zero calibration - COIL(OFF to ON)

//dynamic zero calibration
#define DYN_ZERO_COIL_START_CAL_ADDR             1023 //Dynamic Zero cal - Start zero calibration - COIL(OFF to ON)
#define DYN_ZERO_COIL_ACCEPT_CAL_ADDR            1024 //Dynamic Zero cal - Accept zero calibration - COIL(OFF to ON)
#define DYN_ZERO_COIL_STATUS_RD_CAL_ADDR         1025	

#define DYN_ZERO_COIL_ACCEPT_STATUS_CAL_ADDR     1026
#define DYN_ZERO_COIL_REJECT_STATUS_CAL_ADDR     1027

#define COILS_END_ADDR         									 DYN_ZERO_COIL_REJECT_STATUS_CAL_ADDR
#define NO_OF_COILS															 32

#define PLANT_CONNECT_NEW_RECORD_DATA            5001
#define PLANT_CONNECT_OLD_RECORD_DATA            5002

#define READ_REG                                 1
#define WRITE_REG                                2

/*============================================================================
* Public Data Types
*===========================================================================*/
typedef struct
             {
                unsigned int modbus_reg_addr;         //virtual address of the modbus register
                unsigned int size_of_screen_var_data; //size of the screen structure or 1 if points to variable
                void * screen_var_data;               //pointer to the screen structure or variable
                unsigned char Screen_type;            //Screen type
                unsigned char data_type;              //type of variable
             } MODBUS_TCP_VAR_STRUCT;

typedef struct
             {
							 unsigned int sizeof_scale_runtime_var;
               unsigned int sizeof_scale_setup_cal_var;
               unsigned int sizeof_setup_device_var;
               unsigned int sizeof_reports_diag_var;
               unsigned int sizeof_admin_var;
							 unsigned int sizeof_plantconnect_var;
             }MODBUS_TCP_SIZE_STRUCT;

typedef enum
            {
                bit16_int = 1,
                bit32_int,
                bit32_flt,
                bit64_dbl,
                discrete,
            } VARIABLE_TYPE;

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern MODBUS_TCP_SIZE_STRUCT Size_structure;

extern MODBUS_TCP_VAR_STRUCT Scale_runtime_tcp_var[];
extern MODBUS_TCP_VAR_STRUCT Scale_setup_calib_tcp_var[];
extern MODBUS_TCP_VAR_STRUCT Setup_devices_tcp_var[];
extern MODBUS_TCP_VAR_STRUCT Reports_diag_tcp_var[];
extern MODBUS_TCP_VAR_STRUCT Admin_tcp_var[];
extern MODBUS_TCP_VAR_STRUCT Plant_connect_tcp_var[];

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void calculate_size (void);
extern int point_to_modbus_register (MODBUS_TCP_VAR_STRUCT * data, unsigned int start_address, unsigned int size);
extern unsigned int screen3_variable_read_write (MODBUS_TCP_VAR_STRUCT * data, unsigned int index,
                                            unsigned char read_write, unsigned int value);
extern unsigned int screen4_variable_read_write (MODBUS_TCP_VAR_STRUCT * data, unsigned int index,
                                  unsigned char read_write, unsigned int value);
#endif //#ifdef MODBUS_TCP

#endif //MODBUS_TCP_COMMON
/*****************************************************************************
* End of file
*****************************************************************************/
