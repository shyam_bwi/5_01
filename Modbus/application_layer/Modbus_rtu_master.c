/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename 		   : Modbus_rtu_master.c
 * @brief			         : Controller Board

 * @author			         : Anagha Basole
 *
 * @date Created		     : July Thursday, 2012  <July 12, 2012>
 * @date Last Modified	 : July Thursday, 2012  <July 12, 2012>
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal 			     :
 * @internal 			     :
 *
 *****************************************************************************/

/*============================================================================
 * Include Header Files
 *===========================================================================*/
#include "Global_ex.h"
#include "Modbus_rtu_master.h"
#include "RTOS_main.h"
#include "Modbus_uart_low_level.h"
#include "Sensor_board_data_process.h"
#include "Screen_global_ex.h"
#include "Calibration.h"
#include "Screen_data_enum.h"
#include "Conversion_functions.h"
#include "Modbus_rtu_slave.h"
#include "Rate_blending_load_ctrl_calc.h"
#include "Log_report_data_calculate.h"
#include "Plant_connect.h"
#include "Global_ex.h"
#include "I2C_driver.h"
/* ----------------------- Modbus includes ----------------------------------*/
//#include "common/mbtypes.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"

/*============================================================================
 * Private Macro Definitions
 *===========================================================================*/
/* Defines Section */

#define MBM_SERIAL_PORT_INT           ( MB_UART_1 )
#define MBM_SERIAL_BAUDRATE_INT       ( 921600 )
#define MBM_PARITY                		( MB_PAR_NONE )
#define MBM_SERIAL_PORT_SEN           ( MB_UART_0 )
#define MBM_SERIAL_BAUDRATE_SEN       ( 921600 )

#define THREE_SEC_TIMEOUT             3000 //corresponds to 3 seconds
#define SEN_BRD_RCV_BUFFER_SIZE       0x68
#define INT_BRD_RCV_BUFFER_SIZE       0x04
#define SENSOR_BOARD_ADDR             1
/*============================================================================
 * Private Data Types
 *===========================================================================*/

/*============================================================================
 *Public Variables
 *===========================================================================*/
/* Constants section */
extern int task_number;
/* Boolean variables section */

/* Character variables section */
USHORT rcv_buffer[214];
USHORT rcv_buffer1[20];
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
unsigned char Onlyonce;
//  xMBHandle       xMBMMaster[2];
/*============================================================================
 * Private Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern int Uart_i2c_data_send(uint8_t * i2cBuf, uint8_t size);
/*============================================================================
 * Private Function Prototypes Declarations
 *===========================================================================*/

/*============================================================================
 * Function Implementation Section
 *===========================================================================*/
#ifdef DEBUG_DATA_SEND

/*****************************************************************************
 * @note       Function name  : __task void Send_Data_On_Debug_Port_task(void)
 * @returns    returns        : None
 * @param      arg1           : None
 * @author                    : Oaces Team
 * @date       date created   : 21 Apr 2016
 * @brief      Description    : Send Debug data on serial debug port
 * @note       Notes          : None
 *****************************************************************************/
//Added by PVK on 21 Apr 2016
extern int task_number;
__task void Send_Data_On_Debug_Port_task(void)
{
    OS_RESULT result;
    char chData_buf[200];

#ifdef DEBUG_DATA_SEND
    sprintf(chData_buf, "%s",
            "\r\nTotal_gross_weight, Belt_load, Dist_travelled, Net_total_del_wt,");
    Uart_i2c_data_Tx((uint8_t *) chData_buf, strlen(chData_buf));
    sprintf(chData_buf, "%s",
            "Total_weight_accum,Belt_speed,Rate,Rate_for_display,No_of_pulses,Time between two requeset");
    Uart_i2c_data_Tx((uint8_t *) chData_buf, strlen(chData_buf));
#endif

    while (1)
    {
        //os_itv_set(DEBUG_TASK_INTERVAL);
        //os_itv_wait();

        result = os_evt_wait_or(DEBUG_DATA_SEND_EVENT, 0xFFFF);
        LPC_RTC->ALSEC |= (0x01<<5);
        if (result == OS_R_EVT)
        {
#ifdef DEBUG_DATA_SEND
            if (debugConfig == 1)
            {
                sprintf(chData_buf,
                        "\r\n%0.2f,%0.2f,%0.2f,%0.2lf,%0.2lf,%0.2f,%0.2f,%0.2f,",
                        Calculation_struct.Total_gross_weight,
                        Calculation_struct.Belt_load,
                        Calculation_struct.Dist_travelled,
                        Calculation_struct.Net_total_del_wt,
                        Calculation_struct.Total_weight,
                        Calculation_struct.Belt_speed, Calculation_struct.Rate,
                        Calculation_struct.Rate_for_display);
                Uart_i2c_data_Tx((uint8_t *) chData_buf, strlen(chData_buf));

                sprintf(chData_buf, "%u,%0.2f", Sens_brd_param.No_of_pulses,
                        Sens_brd_param.Time);
                Uart_i2c_data_Tx((uint8_t *) chData_buf, strlen(chData_buf));
            }
            /*//Sens_brd_param.Time, Calculation_struct.Dist_travelled
             sprintf(chData_buf,"\r\n%0.6f,%0.6lf,%0.6f,%d",Calculation_struct.Rate,daily_rprt.Total_weight,\
             Sens_brd_param.Time, Sens_brd_param.No_of_pulses);
             Uart_i2c_data_Tx((uint8_t *)chData_buf, strlen(chData_buf));*/

#endif
            os_evt_clr(DEBUG_DATA_SEND_EVENT, t_modbus_sensor_rtu);
        }
        LPC_RTC->ALSEC &= ~(0x01<<5);
    }
}
#endif

__task void modbus_integrator_master_rtu(void)
{
    eMBErrorCode eStatus;
    static int com_error = 0;
    union float_union rate;
    xMBHandle xMBMMaster[2];
    Modbus_rtu_var.master_slave_stack_reset = NORMAL_MODE;
    while (1)
    {
       
        os_itv_set(INT_TO_INT_BOARD_MODBUS_MASTER_RTU_COMM_TASK_INTERVAL);
        os_itv_wait();
				 LPC_RTC->ALDOM &= ~(0x03<<3);
				 LPC_RTC->ALDOM |= (0x01<<3);//SECTION 1
        if (Modbus_rtu_var.user_semaphore == SEMAPHORE_RELEASED)
        {
            Modbus_rtu_var.user_semaphore = SEMAPHORE_ACQUIRED;
            Modbus_rtu_var.sensor_integrator_port = INTEGRATOR_BOARD;
            Modbus_rtu_var.current_slave_addr = 0;
            // Initialization of Modbus Master Stack
            if (MB_ENOERR
                    == (eStatus = eMBMSerialInit(&xMBMMaster[1], MB_RTU,
                                                 MB_SERIAL_PORT_INT, MB_SERIAL_BAUDRATE_INT,
                                                 MBM_PARITY)))
            {
                // Read Slaves's Blending Coil from address 1000
                //*xMBHandle xHdl, UCHAR ucSlaveAddress,USHORT usRegStartAddress, UBYTE ubNRegs, USHORT arusBufferOut[] */
                //By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152
                //	if((Admin_var.Zero_rate_status == Screen6171_str2) && (Calculation_struct.Rate < Admin_var.Zero_rate_limit))
                if ((Admin_var.Zero_rate_status == Screen6171_str2)
                        && (Calculation_struct.Rate_for_display
                            < Admin_var.Zero_rate_limit))
                    rate.float_val = 0.00000;
                else
                    //	rate.float_val = Calculation_struct.Rate;
                    rate.float_val = Calculation_struct.Rate_for_display;
                rcv_buffer1[1] = rate.f_array[1];
                rcv_buffer1[1] <<= 8;
                rcv_buffer1[1] |= rate.f_array[0];
                rcv_buffer1[0] = rate.f_array[3];
                rcv_buffer1[0] <<= 8;
                rcv_buffer1[0] |= rate.f_array[2];
                // Write Rate value to Correspoding Slave through WSR modbus function command.
								LPC_RTC->ALDOM &= ~(0x03<<3);
								LPC_RTC->ALDOM |= (0x02<<3);	//SECTION2
                eStatus = eMBMWriteMultipleRegisters(xMBMMaster[1], 0, 1, 2,
                                                     rcv_buffer1);
                // Error Handling.
                switch (eStatus)
                {
                case MB_ENOERR:
                    com_error = 0; // On No Error
                    Flags_struct.Error_flags &= ~SCALE_COMM_ERR;
                    Flags_struct.Warning_flags &= ~SCALE_COMM_WARNING;
                    GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_ERR_TIME] = 0;
                    GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_WRN_TIME] = 0;
                    Rprts_diag_var.RS485_2_Status = Port_string1;
                    break;
                default:        // On Any Error
                    com_error +=
                        (INT_TO_INT_BOARD_MODBUS_MASTER_RTU_COMM_TASK_INTERVAL
                         * 10);
                    if ((!(Flags_struct.Warning_flags & SCALE_COMM_WARNING))
                            && (com_error
                                == (40
                                    * INT_TO_INT_BOARD_MODBUS_MASTER_RTU_COMM_TASK_INTERVAL)))
                    {
                        Flags_struct.Warning_flags |= SCALE_COMM_WARNING;
                        Flags_struct.Err_Wrn_log_written |=
                            SCALE_COMM_WRN_LOGGED;
                    }
                    if (com_error >= THREE_SEC_TIMEOUT) //count if communication has failed for past 3 seconds
                    {
                        Flags_struct.Warning_flags &= ~SCALE_COMM_WARNING;
                        if (!(Flags_struct.Error_flags & SCALE_COMM_ERR))
                        {
                            Flags_struct.Error_flags |= SCALE_COMM_ERR;
                            Flags_struct.Err_Wrn_log_written |=
                                SCALE_COMM_ERR_LOGGED;
                        }
                    }
                    Rprts_diag_var.RS485_2_Status = Port_string2;
                    break;
                }
                Modbus_rtu_var.sensor_integrator_port = INTEGRATOR_BOARD;
                if (MB_ENOERR != (eStatus = eMBMClose(xMBMMaster[1])))
                {
                    MBP_ASSERT(0);
                }
                //switch from master to slave mode so, create the slave task and delete the master stack instance
								LPC_RTC->ALDOM &= ~(0x03<<3);
								LPC_RTC->ALDOM |= (0x03<<3);	//SECTION3
                if (Modbus_rtu_var.master_slave_stack_reset
                        == MASTER_TO_SLAVE_STACK)
                {
                    Modbus_rtu_var.master_slave_stack_reset = NORMAL_MODE;
                    t_modbus_integrator_slave_rtu = os_tsk_create(
                                                        modbus_integrator_slave_rtu,
                                                        MODBUS_INT_TASK_PRIORITY_SLAVE);
                    t_modbus_integrator_master_rtu = 0;
                    Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;
                    os_tsk_delete_self();
                }
                Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;
            } //if( MB_ENOERR ==..)
            //} //End For
            Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;
        }
        LPC_RTC->ALDOM &= ~(0x03<<3);
    }					//while(1)
}

/*****************************************************************************
 * @note       Function name  : __task void modbus_sensor_rtu (void)
 * @returns    returns        : None
 * @param      arg1           : None
 * @author                    : Anagha Basole
 * @date       date created   : 3rd August 2012
 * @brief      Description    : Modbus RTU master task, to initialize the modbus rtu master stack and
 *                            : send requests to the sensor board every 50 ms.
 *                            : 0x04 - read input register
 * @note       Notes          : None
 *****************************************************************************/
int log_len_once;
extern int task_number;
__task void modbus_sensor_rtu(void)
{

    eMBErrorCode eStatus, eStatus2;
    USHORT data_bytes;
    UBYTE i;
    static int com_error = 0;
    xMBHandle xMBMMaster[2];
//		signed int nooftimes = 0;
#ifdef CALCULATION
    Weight_unit_conv_func();
    Distance_unit_conversion();
    //Calculation_struct.Total_weight = Calculation_struct.Total_weight_accum * Calculation_struct.Weight_conv_factor;
    Calculation_struct.Total_weight = ConvWeightUnit(LBS,
                                      Scale_setup_var.Weight_unit, Calculation_struct.Total_weight_accum);
    Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
    Calculation_struct.Zero_weight = Calibration_var.New_zero_value
                                     / Calculation_struct.Zero_value_conv_factor;
    Calculation_struct.Belt_length_calc = Calibration_var.New_belt_length
                                          * Calculation_struct.Belt_length_conv_factor;
#endif

    Calib_struct.Set_progress_bar = PROGRESS_BAR_RESET;
    Calib_struct.Start_flag = __FALSE;
    Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
    Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
    Sens_brd_param.Rate_input_4_20ma = 0;
#ifdef RATE_BLEND_LOAD_CTRL_CALC
    Rate_blend_load.Bias = 0;
    Rate_blend_load.DAC_count = 0;
    Rate_blend_load.D_term = 0;
    Rate_blend_load.Feed_delay_timer = 0;
    Rate_blend_load.Calculation_flags = 0;
    Rate_blend_load.I_sum = 0;
    Rate_blend_load.I_term = 0;
    Rate_blend_load.PID_error = 0;
    Rate_blend_load.Preload_delay_timer = 0;
    Rate_blend_load.P_term = 0;
    Rate_blend_load.Set_rate = 0;
    Rate_blend_load.actual_weight = 0;
    Rate_blend_load.packet_cnt = 0;
    Rate_blend_load.PD_Timer_flag = 0;
    Rate_blend_load.FD_Timer_flag = 0;
    Rate_blend_load.Delta_time = 0.0;

    //Added on 31/03/2016
    Rate_blend_load.P_term = Setup_device_var.PID_P_Term;
    Rate_blend_load.I_term = Setup_device_var.PID_I_Term;
    Rate_blend_load.D_term = Setup_device_var.PID_D_Term;
    Rate_blend_load.Set_rate = Setup_device_var.Target_rate;

    // Initialization of Rate Display FIFO structure
    Init_FIFO(Rate_disp_FIFO);
#endif

    while (1)
    {

        os_itv_set(SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL);
        os_itv_wait();
				LPC_RTC->ALSEC &= ~(0x03<<3);	
        LPC_RTC->ALSEC |= (0x01<<3);				//SECTION 1
        Current_time = RTCGetTime();

        if ((Current_time.RTC_Hour == 0 && Current_time.RTC_Min == 0
                && Current_time.RTC_Sec == 0
                && (Admin_var.Current_Time_Format == TWENTYFOUR_HR))
                || ((Current_time.RTC_Hour == 12 && Current_time.RTC_Min == 0
                     && Current_time.RTC_Sec == 0)
                    && (Admin_var.Current_Time_Format == TWELVE_HR)
                    && (Admin_var.AM_PM == AM_TIME_FORMAT)))
        {
            if (Onlyonce == 1)
            {
                Totals_var.daily_Total_weight = 0;
                Rprts_diag_var.daily_Total_weight = 0;
                daily_rprt.Total_weight = 0;
                Onlyonce = 1;
            }
        }
        else
        {
            Onlyonce = 0;
        }
        if (Modbus_rtu_var.user_semaphore == SEMAPHORE_RELEASED)
        {

            LPC_RTC->ALSEC &= ~(0x03<<3);				//SECTION 2
            LPC_RTC->ALSEC |= (0x02<<3);				//SECTION 2
            Modbus_rtu_var.user_semaphore = SEMAPHORE_ACQUIRED;
            Modbus_rtu_var.sensor_integrator_port = SENSOR_BOARD;
            if (MB_ENOERR
                    == (eStatus = eMBMSerialInit(&xMBMMaster[SENSOR_BOARD],
                                                 MB_RTU, MBM_SERIAL_PORT_SEN,
                                                 MBM_SERIAL_BAUDRATE_SEN, MB_PARITY)))
            {
                eStatus = MB_ENOERR;
                if (MB_ENOERR
                        != (eStatus2 = eMBMReadInputRegisters(
                                           xMBMMaster[SENSOR_BOARD], SENSOR_BOARD_ADDR, 1,
                                           SEN_BRD_RCV_BUFFER_SIZE, rcv_buffer)))
                {
                    eStatus = eStatus2;
                    //Calculation_struct.Belt_speed = Calculation_struct.Rate = 0;
                    com_error +=
                        (SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL
                         * 10);
                    if (!(Flags_struct.Warning_flags & SEN_BRD_COMM_WARNING))
                    {
                        Flags_struct.Warning_flags |= SEN_BRD_COMM_WARNING;
                        Flags_struct.Err_Wrn_log_written |=
                            SEN_BRD_COMM_WRN_LOGGED;
                    }
                    if (com_error >= THREE_SEC_TIMEOUT) //count if communication has failed for past 3 seconds
                    {
                        Flags_struct.Warning_flags &= ~SEN_BRD_COMM_WARNING;
                        if (!(Flags_struct.Error_flags & SEN_BRD_COMM_ERR))
                        {
                            Flags_struct.Err_Wrn_log_written |=
                                SEN_BRD_COMM_ERR_LOGGED;
                            Flags_struct.Error_flags |= SEN_BRD_COMM_ERR;
                        }
                        //Sens_brd_param.Sensor_Board_Status = FALSE;
                    }
                    Rprts_diag_var.RS485_1_Status = Port_string2;
                }

                switch (eStatus)
                {
                    LPC_RTC->ALSEC &= ~(0x03<<3);				//SECTION 3
                    LPC_RTC->ALSEC |= (0x03<<3);				//SECTION 3
                case MB_ENOERR:
                    Rprts_diag_var.RS485_1_Status = Port_string1;
                    Flags_struct.Warning_flags &= ~SEN_BRD_COMM_WARNING;
                    Flags_struct.Error_flags &= ~SEN_BRD_COMM_ERR;
                    //Sens_brd_param.Sensor_Board_Status = TRUE;
                    com_error = 0;
                    for (i = 0; i < SEN_BRD_RCV_BUFFER_SIZE; i++)
                    {
                        data_bytes = rcv_buffer[i];
                        rcv_buffer[i] = ((data_bytes & 0xff) << 8)
                                        | ((data_bytes & 0xff00) >> 8);
                    }
                    Parse_rcvd_data((U8 *) &rcv_buffer[0]);
                    /*printf("\n %lf, %d, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf",
                     Sens_brd_param.Time,
                     Sens_brd_param.No_of_pulses,
                     Sens_brd_param.Accum_LC_Perc[0],
                     Sens_brd_param.Accum_LC_Perc[1],
                     Sens_brd_param.Accum_LC_Perc[2],
                     Sens_brd_param.Accum_LC_Perc[3],
                     Sens_brd_param.Accum_LC_Perc[4],
                     Sens_brd_param.Accum_LC_Perc[5],
                     Sens_brd_param.Accum_LC_Perc[6],
                     Sens_brd_param.Accum_LC_Perc[7]); */
                    Calculation();

                    //os_evt_set (MONITOR_WEIGHT_EVENT_FLAG, t_weight_monitor);
                    //For calibration
                    if (Calib_struct.Cal_status_flag == START_TEST_WEIGHT_CAL)
                    {
                        test_weight_cal();
                    }
                    else if (Calib_struct.Cal_status_flag
                             == START_MATERIAL_CAL)
                    {
                        log_data_populate(periodic_mat_log);
                        material_test_cal();
                    }
                    else if (Calib_struct.Cal_status_flag == START_DIGITAL_CAL)
                    {
                        log_data_populate(periodic_dig_log);
                        digital_cal();
                    }
                    else if (Calib_struct.Cal_status_flag
                             == START_STATICZERO_CAL)
                    {
                        static_zero_cal();
                    }
                    else if (Calib_struct.Cal_status_flag
                             == START_DYNAMICZERO_CAL)
                    {
                        dynamic_zero_cal();
                    }
                    else if (Calib_struct.Cal_status_flag == START_BELT_CAL)
                    {
                        if (log_len_once ==0)
                        {
                            log_data_populate(periodic_len_log); 			//multiple LOGS here
                            log_len_once =1;
                        }
                        AutoBeltLength();
                    }
                    else if (Calib_struct.Cal_status_flag == START_LENZERO_CAL)
                    {
                        LengthAndZero();
                    }
                    break;
                default:
                    ;
                    break;
                }
                // START OF PID CONTROL LOGIC
#ifdef RATE_BLEND_LOAD_CTRL_CALC
                if (Rate_blend_load.packet_cnt >= 4)   // If beltspeed == 0 Sensorboard.time value is zero so we are directly assigning Detalat time to 4 * packet duration value
                {
                    //storing of previous Set rate
                    Rate_blend_load.Last_Set_rate = Rate_blend_load.Set_rate;
                    // Set Rate is defined based on the Setpoint Type (LOCAL or INPUT or REMOTE )
                    if (Setup_device_var.PID_Setpoint_Type == Screen4513_str1)
                    {
                        Rate_blend_load.Set_rate =
                            Setup_device_var.PID_Local_Setpoint;
                    } //if rate is from Remote Master scale
                    else if (Setup_device_var.PID_Setpoint_Type
                             == Screen4513_str4)   /*Rate from Master integrator board is Master rate*/
                    {
                        Rate_blend_load.Set_rate =
                            (Calculation_struct.Rate_from_int_board)
                            * ((float) Setup_device_var.Percent_Ingredient
                               / 100.00);
                    }
                    else   //if Rate is from Analog Input
                    {
                        Rate_blend_load.Set_rate =
                            (((Sens_brd_param.Rate_input_4_20ma - 4) / 16)
                             * Setup_device_var.Analog_Input_Maxrate)
                            * ((float) Setup_device_var.Percent_Ingredient
                               / 100.00);
                    }
                    // Select the Control function according to Run mode screen selected
                    if ((Rate_blend_load.Calculation_flags
                            & BLEND_CONTROL_SCREEN_FLAG)
                            == BLEND_CONTROL_SCREEN_FLAG)
                    {
                        blend_control_flow();
                    }
                    else if ((Rate_blend_load.Calculation_flags
                              & RATE_CONTROL_SCREEN_FLAG)
                             == RATE_CONTROL_SCREEN_FLAG)
                    {
                        rate_control_flow();
                    }
                    else if ((Rate_blend_load.Calculation_flags
                              & LOAD_CONTROL_SCREEN_FLAG)
                             == LOAD_CONTROL_SCREEN_FLAG)
                    {
                        // Needs to be implemented in future..
                    }
                }
#endif
                os_evt_set(IOBOARD_EVENT_FLAG, t_ioboard);

#ifdef DEBUG_DATA_SEND
                os_evt_set(DEBUG_DATA_SEND_EVENT, t_Send_Data_On_Debug_Port);
#endif

                //END of PID CONTROL LOGIC
                Modbus_rtu_var.sensor_integrator_port = SENSOR_BOARD;
                if (MB_ENOERR
                        != (eStatus = eMBMClose(xMBMMaster[SENSOR_BOARD])))
                {
                    MBP_ASSERT(0);
                }
            } //if( MB_ENOERR ==..)
            /* Disable Interrupt for UART0 channel */
            //NVIC_DisableIRQ(_SENSOR_UART_IRQ);
            Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;
        } //if semaphore
        LPC_RTC->ALSEC &= ~(0x03<<3);				//EXIT SECTION
    } //while(1)
}

