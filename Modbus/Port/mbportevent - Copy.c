/* 
 * MODBUS Library: LPC2388/lwIP  port
 * Copyright (c) 2011 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbportevent.c,v 1.1 2011/06/13 19:17:54 embedded-solutions.cwalter Exp $
 */

#if 1
/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"
#include "Rtos_main.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define MAX_EVENT_HDLS          ( 3 )
#define IDX_INVALID             ( 255 )
#define EV_NONE                 ( 0 )

#define HDL_RESET( x ) do { \
    ( x )->ubIdx = IDX_INVALID; \
		( x )->xType = EV_NONE; \
} while( 0 )
//#define MAILBOX_DEF
//    ( x )->xType = EV_NONE; \

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
		#ifdef MAILBOX_DEF
    //xMBPEventType   xType;
		//OS_ID				 pxMailBox;
		//os_mbx_declare(pxMailBox, 16);
		#else
			xMBPEventType   xType;
		#endif
} xEventInternalHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC BOOL     bIsInitialized = FALSE;
STATIC xEventInternalHandle arxEventHdls[MAX_EVENT_HDLS];
/******************************************************************/
#ifdef MAILBOX_DEF
	os_mbx_declare(pxMailBox, 16);           /* Declare an RTX mailbox              */
	_declare_box (mpool, sizeof(U32/*xEventInternalHandle*/), 16);/* Dynamic memory pool                */
	//OS_TID create, post, get;
	extern OS_MBX pxMailBox;
#endif
/******************************************************************/

/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Start implementation -----------------------------*/
#if 0
eMBErrorCode
eMBPEventCreate( xMBPEventHandle * pxEventHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    UBYTE           i;

    if( NULL != pxEventHdl )
    {
        MBP_ENTER_CRITICAL_SECTION(  );
        if( !bIsInitialized )
        {
            for( i = 0; i < MAX_EVENT_HDLS; i++ )
            {
                HDL_RESET( &arxEventHdls[i] );
            }
            bIsInitialized = TRUE;
        }
        for( i = 0; i < MAX_EVENT_HDLS; i++ )
        {
            if( IDX_INVALID == arxEventHdls[i].ubIdx )
            {
                arxEventHdls[i].ubIdx = i;
                arxEventHdls[i].xType = EV_NONE;
                *pxEventHdl = &arxEventHdls[i];
                eStatus = MB_ENOERR;
                break;
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
    return eStatus;
}
#endif
eMBErrorCode
eMBPEventCreate( xMBPEventHandle * pxEventHdl )
{
    /* Thread: 
     *  - Multiple MODBUS core threads from different handles
     * Protection: Full 
     */
    eMBErrorCode    eStatus = MB_EINVAL;
    //xQueueHandle    xQueueHdl;
    UBYTE           i;

		
		/******************************************************************/
		//t_modbus_sensor_tsk  = os_tsk_self ();          /* identify task for EventCreate   */
		/******************************************************************/
    if( NULL != pxEventHdl )
    {	
        MBP_ENTER_CRITICAL_SECTION(  );
        if( !bIsInitialized )
        {
            for( i = 0; i < MAX_EVENT_HDLS; i++ )
            {
                HDL_RESET( &arxEventHdls[i] );								
            }						
						/******************************************************************/
						#ifdef MAILBOX_DEF
								_init_box (mpool, sizeof(mpool), sizeof(U32/*xEventInternalHandle*/));
								os_mbx_init (/*mailbox1, sizeof(mailbox1)*//*arxEventHdls[i].*/pxMailBox, sizeof(/*arxEventHdls[i].*/pxMailBox));
								os_dly_wait (5);						
						#endif
								/******************************************************************/
            bIsInitialized = TRUE;
        }
        for( i = 0; i < MAX_EVENT_HDLS; i++ )
        {
            if( IDX_INVALID == arxEventHdls[i].ubIdx )
            {                
								arxEventHdls[i].ubIdx = i;      
								#ifndef MAILBOX_DEF
								arxEventHdls[i].xType = EV_NONE;
								#endif
								/******************************************************************/
								 //arxEventHdls[i].pxMailBox = (OS_ID) &mailbox1;
								/******************************************************************/
								 *pxEventHdl = &arxEventHdls[i];
                eStatus = MB_ENOERR;
                break;
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
    return eStatus;
}


eMBErrorCode
eMBPEventPost( const xMBPEventHandle xEventHdl, xMBPEventType xEvent )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xEventInternalHandle *pxEventHdl = xEventHdl;
		/******************************************************************/
		#ifdef MAILBOX_DEF
		OS_RESULT result;
		U32 *mptr;
		//t_modbus_sensor_tsk = os_tsk_self ();          /* identify task for Event Post   */
		#endif
		/******************************************************************/
    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {
				/******************************************************************/
				#ifdef MAILBOX_DEF
				mptr = _alloc_box (mpool);          /* Allocate a memory for the message   */
				//mptr->xType = xEvent;
				mptr[0] = (U32)xEvent;
				//pxEventHdl->xType = xEvent;
				//result = os_mbx_check (/*pxEventHdl->*/pxMailBox);
				os_mbx_send (/*pxEventHdl->*/pxMailBox/*mailbox1*/, (void *)/*(&xEvent)*/mptr, 0xFFFF);
				#else
				 pxEventHdl->xType = xEvent;
				#endif
				/******************************************************************/        
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

BOOL
bMBPEventGet( const xMBPEventHandle xEventHdl, xMBPEventType * pxEvent )
{
    BOOL            bEventInQueue = FALSE;
    xEventInternalHandle *pxEventHdl = xEventHdl;

		/******************************************************************/
		#ifdef MAILBOX_DEF
		U32 Event;
		U32 *rptr;
		//t_modbus_sensor_tsk  = os_tsk_self ();          /* identify task for Event gets   */
		#endif
		/******************************************************************/
    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {	
				#ifdef MAILBOX_DEF
				/******************************************************************/
				os_mbx_wait (/*pxEventHdl->*/pxMailBox/*mailbox1*/, /*(void *)(&pxEvent)*/(void **)&rptr, 0xFFFF);
				/*pxEventHdl->xType*/Event = /*rptr->xType*/*rptr;
				/******************************************************************/
       // if( EV_NONE != pxEventHdl->xType )
				 if( EV_NONE != Event )
        {
            bEventInQueue = TRUE;
            *pxEvent = /*pxEventHdl->xType*/Event;
            /*pxEventHdl->xType*/Event = EV_NONE;
        }
				/******************************************************************/
				_free_box (mpool, rptr);           /* free memory allocated for message  */
				/******************************************************************/
				#else
				 if( EV_NONE != pxEventHdl->xType )
				 {
					 bEventInQueue = TRUE;
            *pxEvent = pxEventHdl->xType;
           pxEventHdl->xType = EV_NONE;
				 }
				#endif
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return bEventInQueue;
}

void
vMBPEventDelete( xMBPEventHandle xEventHdl )
{
    xEventInternalHandle *pxEventIntHdl = xEventHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxEventIntHdl, arxEventHdls ) )
    {
        HDL_RESET( pxEventIntHdl );
    }
    MBP_EXIT_CRITICAL_SECTION(  );
}
#endif //commented on 14th sept for a file from christian

#if 0
/*************File from Christian*************************/
/* 
 * MODBUS Library: LPC2388/lwIP  port
 * Copyright (c) 2011 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbportevent.c,v 1.1 2011/06/13 19:17:54 embedded-solutions.cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <rtl.h>

/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define MAX_EVENT_HDLS          ( 2 )
#define IDX_INVALID             ( 255 )
#define EV_NONE                 ( 0 )

#define HDL_RESET( x ) do { \
    ( x )->ubIdx = IDX_INVALID; \
} while( 0 )

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
    os_mbx_declare(xMsgBox, 1);    
} xEventInternalHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC BOOL     bIsInitialized = FALSE;
STATIC BOOL     bInISR = FALSE;
STATIC xEventInternalHandle arxEventHdls[MAX_EVENT_HDLS];

/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBPEventCreate( xMBPEventHandle * pxEventHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    UBYTE           i;

    if( NULL != pxEventHdl )
    {
        MBP_ENTER_CRITICAL_SECTION(  );
        if( !bIsInitialized )
        {
            for( i = 0; i < MAX_EVENT_HDLS; i++ )
            {
                HDL_RESET( &arxEventHdls[i] );
                os_mbx_init (arxEventHdls[i].xMsgBox, sizeof(arxEventHdls[i].xMsgBox));
            }
            bIsInitialized = TRUE;
        }
        for( i = 0; i < MAX_EVENT_HDLS; i++ )
        {
            if( IDX_INVALID == arxEventHdls[i].ubIdx )
            {
                arxEventHdls[i].ubIdx = i;
                *pxEventHdl = &arxEventHdls[i];
                eStatus = MB_ENOERR;
                break;
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
    return eStatus;
}

void
vMBPEventSetIsINISR( BOOL bInISRArg )
{
    MBP_ENTER_CRITICAL_SECTION(  );
    bInISR = bInISRArg;
    MBP_EXIT_CRITICAL_SECTION(  );
}

eMBErrorCode
eMBPEventPost( const xMBPEventHandle xEventHdl, xMBPEventType xEvent )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xEventInternalHandle *pxEventHdl = xEventHdl;
    OS_RESULT xRes;
    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {
        if( bInISR )
        {
             MBP_EXIT_CRITICAL_SECTION(  );
             isr_mbx_send (pxEventHdl->xMsgBox, (void *)xEvent);
        }
        else
        {
             MBP_EXIT_CRITICAL_SECTION(  );
             xRes = os_mbx_send (pxEventHdl->xMsgBox, (void *)xEvent, 0xffff);
             MBP_ASSERT( OS_R_OK == xRes );
        }
        eStatus = MB_ENOERR;
    }
    else
    {
         MBP_EXIT_CRITICAL_SECTION(  );
    }
   
    return eStatus;
}

BOOL
bMBPEventGet( const xMBPEventHandle xEventHdl, xMBPEventType * pxEvent )
{
    BOOL            bEventInQueue = FALSE;
    xEventInternalHandle *pxEventHdl = xEventHdl;
    void *ptr;
    OS_RESULT xRes;
    if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {
         xRes = os_mbx_wait ( pxEventHdl->xMsgBox, &ptr, 100 );
        if( OS_R_TMO != xRes )
        {
            bEventInQueue = TRUE;
            *pxEvent = ( xMBPEventType )ptr;
        }
    }
    return bEventInQueue;
}

void
vMBPEventDelete( xMBPEventHandle xEventHdl )
{
    xEventInternalHandle *pxEventIntHdl = xEventHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxEventIntHdl, arxEventHdls ) )
    {
        HDL_RESET( pxEventIntHdl );
    }
    MBP_EXIT_CRITICAL_SECTION(  );
}
#endif
