/*
 * MODBUS Library: Bare port
 * Copyright (c) 2011 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbporttcp.c,v 1.1 2011/06/13 19:17:54 embedded-solutions.cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <RTL.h>
#include "SMTP_demo.h"
#include "RTOS_main.h"
#include <stdio.h>
/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbframe.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"

/* ----------------------- Defines ------------------------------------------*/
#define IDX_INVALID                         ( 255 )
#define MAX_NUMBER_OF_CLIENTS								( 2 )
#ifndef MBP_TCP_DEBUG
#define MBP_TCP_DEBUG                       ( 0 )
#endif

/* ----------------------- Type definitions ---------------------------------*/

typedef struct
{
    int             iSocket;
    BOOL            bDelete;
} xMBPTCPIntClientHandle;

typedef struct
{
    UBYTE           ubIdx; 
    xMBHandle       xMBHdl; 
    peMBPTCPClientNewDataCB eMBPTCPClientNewDataFN; 
    peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFN;
} xMBPTCPIntCommonHandle;

typedef struct
{
    UBYTE           ubIdx;
    xMBHandle       xMBHdl;
    U8              xServerSoc;
    xMBPTCPIntClientHandle xClientCon[2];
    BYTE            arbClientRcvBuffer[260];
    USHORT          usClientRcvBufferPos;
    USHORT          usClientRcvBufferLen;
    peMBPTCPClientConnectedCB eMBPTCPClientConnectedFN;
    peMBPTCPClientNewDataCB eMBPTCPClientNewDataFN;
    peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFN;
} xMBPTCPIntSlaveHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC xMBPTCPIntSlaveHandle xMBTCPSlaveHdls[2];
STATIC BOOL     bIsInitalized = FALSE;

#ifdef MODBUS_TCP
extern U8 socket_num;
#endif
/* ----------------------- Static functions ---------------------------------*/
U16 prvxMBTCPPortTCPListenCallback (U8 soc, U8 event, U8 *ptr, U16 par) ;

/* ----------------------- Start implementation -----------------------------*/

/************************************************************************/
STATIC void
prvxMBTCPIntClientHandleReset( xMBPTCPIntSlaveHandle *pxIntHdl, BOOL bClose )
{
		U8 ubIdx = 0;
		ubIdx = pxIntHdl->ubIdx;
    pxIntHdl->eMBPTCPClientConnectedFN = NULL;
    pxIntHdl->eMBPTCPClientDisconnectedFN = NULL;
    pxIntHdl->eMBPTCPClientNewDataFN = NULL;
    if( bClose && ( 0 != pxIntHdl->xClientCon[ubIdx].iSocket ) )
    {
        tcp_abort( pxIntHdl->xClientCon[ubIdx].iSocket );
        tcp_release_socket( pxIntHdl->xClientCon[ubIdx].iSocket );
    }
    pxIntHdl->xClientCon[ubIdx].iSocket = 0;
}

/************************************************************************/
STATIC void
prvxMBTCPIntHandleReset( xMBPTCPIntSlaveHandle *pxIntHdl, BOOL bClose )
{
    pxIntHdl->ubIdx = IDX_INVALID;
    pxIntHdl->xMBHdl = MB_HDL_INVALID;
    if( bClose && ( 0 != pxIntHdl->xServerSoc ) )
    {
        tcp_abort( pxIntHdl->xServerSoc );
        tcp_release_socket( pxIntHdl->xServerSoc );
    }
    pxIntHdl->xServerSoc = 0;
}


/*! \brief Create a new listening server on address \c usTCPPort which
 *   accepts connection for the addresses specificed in \c pcBindAddress.
 *
 * The exact meaning of \c pcBindAddress is application and platform
 * dependent and therefore not further specified.
 *
 * \param pxTCPHdl If a new listening server has been created this handle
 *   should point to the server.
 * \param pcBindAddress The address to bind to. The exact meaning is port
 *   dependent.
 * \param usTCPPort The TCP port.
 * \param xMBHdl A handle which is used for the callback functions to
 *   identify the stack.
 * \param eMBPTCPClientNewDataFN Callback function if new client data
 *    is available.
 * \param eMBPTCPClientDisconnectedFN Callback function if a client
 *    connection is broken.
 * \param eMBPTCPClientConnectedFN Callback function if a client
 *    connects to the server.
 * \return eMBErrorCode::MB_ENOERR if a new server was created. In case
 *   of invalid arguments it should return eMBErrorCode::MB_EINVAL. Otherwise
 *   eMBErrorCode::MB_EPORTERR.
 */
eMBErrorCode
eMBPTCPServerInit( xMBPTCPHandle * pxTCPHdl, CHAR * pcBindAddress,
                   USHORT usTCPPort,
                   xMBHandle xMBHdlArg,
                   peMBPTCPClientNewDataCB eMBPTCPClientNewDataFNArg,
                   peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFNArg,
                   peMBPTCPClientConnectedCB eMBPTCPClientConnectedFNArg )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    U8              xServerSoc;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = NULL;
		U8 ubIdx = 0;
    if( NULL != pxTCPHdl )
    {

        MBP_ENTER_CRITICAL_SECTION(  );
				for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls ); ubIdx++ )
				{			
					if( !bIsInitalized )
					{
							prvxMBTCPIntHandleReset( &xMBTCPSlaveHdls[0], FALSE );
							prvxMBTCPIntClientHandleReset( &xMBTCPSlaveHdls[0], FALSE );
					}
				}
        MBP_EXIT_CRITICAL_SECTION(  );
				for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls->xClientCon ); ubIdx++ )
				{		
					if( IDX_INVALID == xMBTCPSlaveHdls[0].ubIdx )
					{
							pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[0];
							if(0 == ( xServerSoc = tcp_get_socket( (TCP_TYPE_SERVER | TCP_TYPE_KEEP_ALIVE) , 0, 60, prvxMBTCPPortTCPListenCallback ) )  )
							{
									eStatus = MB_ENORES;
							}
// 							if(ubIdx ==1 && 0 == ( xServerSoc = tcp_get_socket( (TCP_TYPE_SERVER | TCP_TYPE_KEEP_ALIVE) , 0, 60, prvxMBTCPPortTCPListen2Callback ) )  )
// 							{
// 									eStatus = MB_ENORES;
// 							}							
							else if( !tcp_listen( xServerSoc, usTCPPort ) )
							{
									eStatus = MB_EPORTERR;
							}
							else
							{
									pxTCPIntSlaveHdl->ubIdx = ubIdx;
									pxTCPIntSlaveHdl->xServerSoc = xServerSoc;
									pxTCPIntSlaveHdl->eMBPTCPClientNewDataFN = eMBPTCPClientNewDataFNArg;
									pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN = eMBPTCPClientDisconnectedFNArg;
									pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN = eMBPTCPClientConnectedFNArg;
									pxTCPIntSlaveHdl->xMBHdl = xMBHdlArg;
									*pxTCPHdl = pxTCPIntSlaveHdl;
									eStatus = MB_ENOERR;
									#ifdef MODBUS_TCP
									socket_num = xServerSoc;
									#endif
							}
							if( MB_ENOERR != eStatus )
							{
									prvxMBTCPIntHandleReset( pxTCPIntSlaveHdl, TRUE );
									prvxMBTCPIntClientHandleReset( pxTCPIntSlaveHdl, TRUE );
							}
					}
					else
					{
							eStatus = MB_ENORES;
					}
				}				
    }
    return eStatus;
}

/************************************************************************/
U16 prvxMBTCPPortTCPListenCallback (U8 soc, U8 event, U8 *ptr, U16 par)
{
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[0];
    eMBErrorCode eStatus;
    U16 retval = 0;
		U8 ubIdx = 0;
    ( void )eStatus;
// 		if(xMBTCPSlaveHdls[0].xServerSoc == soc)
// 			ubIdx = 0;
// 		else if(xMBTCPSlaveHdls[1].xServerSoc == soc)
// 			ubIdx = 1;
// 		else
// 			ubIdx = 0;
		
//		pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[ubIdx];
		
    switch (event)
    {
    case TCP_EVT_CONREQ:
        if( MB_ENOERR != ( eStatus = pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCon[ubIdx].iSocket ) ) ) )
        {
            retval = 0;
        }
        else
        {
            pxTCPIntSlaveHdl->xClientCon[ubIdx].iSocket = soc;
            retval = 1;
        }
        break;

    case TCP_EVT_ABORT:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCon[ubIdx].iSocket ) );
      break;

    case TCP_EVT_CONNECT:
      retval = 1;
      break;

    case TCP_EVT_CLOSE:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCon[ubIdx].iSocket ) );
      break;

    case TCP_EVT_ACK:
			retval = 1;
      break;

    case TCP_EVT_DATA:
      if( par < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls[0].arbClientRcvBuffer ) )
      {
          memcpy( xMBTCPSlaveHdls[0].arbClientRcvBuffer, ptr, par );
          xMBTCPSlaveHdls[0].usClientRcvBufferLen = par;
          xMBTCPSlaveHdls[0].usClientRcvBufferPos = 0;
      }
// 		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
//       break;

  }
  return retval;
}

/************************************************************************/
/*! \brief Closes a server instance.
 *
 * \param xTCPHdl A handle for a TCP server.
 * \return eMBErrorCode::MB_ENOERR if the server has shut down (and closed
 *   all client connections). eMBErrorCode::MB_EINVAL if the handle was
 *   not valid. All other errors should be mapped to eMBErrorCode::MB_EPORTERR.
 */
eMBErrorCode
eMBTCPServerClose( xMBPTCPHandle xTCPHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = xTCPHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTCPIntSlaveHdl, xMBTCPSlaveHdls )  )
    {
        prvxMBTCPIntClientHandleReset( pxTCPIntSlaveHdl, TRUE );
        prvxMBTCPIntHandleReset( pxTCPIntSlaveHdl, TRUE );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );

    return eStatus;
}

/************************************************************************/
/*! \brief Close a TCP client connection.
 *
 * Called by the stack when a TCP client connection should be closed.
 *
 * \param xTCPHdl A handle to a TCP server instance.
 * \param xTCPClientHdl A handle for a TCP client which should be closed.
 * \return The function should return eMBErrorCode::MB_ENOERR if the client
 *   connection has been closed. If any of the handles are invalid it should
 *   return eMBErrorCode::MB_EINVAL. All other errors should be mapped to
 *   eMBErrorCode::MB_EPORTERR.
 */
eMBErrorCode
eMBPTCPConClose( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = xTCPHdl;

    if( NULL != xTCPClientHdl )
    {
        MBP_ENTER_CRITICAL_SECTION(  );
        prvxMBTCPIntClientHandleReset( pxTCPIntSlaveHdl, TRUE );
        MBP_EXIT_CRITICAL_SECTION(  );
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

/************************************************************************/
void
vMBPTCPServerPoll( void )
{
	U8 ubIdx = 0;
	
		for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls->xClientCon ); ubIdx++ )
		{	
			if( xMBTCPSlaveHdls[0].usClientRcvBufferLen  &&  NULL != xMBTCPSlaveHdls[0].xClientCon[ubIdx].iSocket )
			{
					( void )xMBTCPSlaveHdls[0].eMBPTCPClientNewDataFN( xMBTCPSlaveHdls[0].xMBHdl, &( xMBTCPSlaveHdls[0].xClientCon[ubIdx].iSocket ) );
			}
		}
}

/************************************************************************/
/*! \brief This function is called by the MODBUS stack when new data should
 *    be read from a client.
 *
 * This function must not block and should read up to \c usBufferMax bytes and
 * store them into the buffer \c pubBuffer. The value of \c pusBufferLen should
 * be set to the number of bytes read where 0 is used when there is no data
 * available.
 *
 * \param xTCPHdl A handle to a TCP server instance.
 * \param xTCPClientHdl A handle for a TCP client.
 * \param pubBuffer A buffer of at least \c usBufferMax bytes where the read
 *   bytes from the client should be stored.
 * \param pusBufferLen On return the value should hold the number of bytes
 *   written into the buffer.
 * \param usBufferMax Maximum number of bytes which can be stored in the
 *   buffer.
 * \return The function should return eMBErrorCode::MB_ENOERR if zero or more
 *   bytes have been stored in the buffer and no error occurred. In case of a
 *   client error it should return eMBErrorCode::MB_EIO. In case of an invalid
 *   handle is should return eMBErrorCode::MB_EINVAL. Other errors should be
 *   mapped to eMBErrorCode::MB_EPORTERR signaling the stack that the porting
 *   layer is no longer functional.
 */
eMBErrorCode
eMBPTCPConRead( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, UBYTE * pubBuffer,
                USHORT * pusBufferLen, USHORT usBufferMax )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;
		xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
    USHORT usToCopy;
		U8 ubIdx = 0;
	
		ubIdx = (pxTCPIntCommonHdl -> ubIdx) -1;
	
    if( NULL != xTCPClientHdl )
    {
        if( usBufferMax > xMBTCPSlaveHdls[0].usClientRcvBufferLen )
        {
            usToCopy = xMBTCPSlaveHdls[0].usClientRcvBufferLen;
        }
        else
        {
            usToCopy = usBufferMax;
        }
        memcpy( pubBuffer, &( xMBTCPSlaveHdls[0].arbClientRcvBuffer[xMBTCPSlaveHdls[0].usClientRcvBufferPos] ), usToCopy );
        *pusBufferLen = usToCopy;
        xMBTCPSlaveHdls[ubIdx].usClientRcvBufferPos += usToCopy;
        xMBTCPSlaveHdls[ubIdx].usClientRcvBufferLen -= usToCopy;
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

/************************************************************************/
/*! \brief This function is called by the MODBUS stack when new data should
 *    be sent over a client connection.
 *
 * This function should not block and should transmit \c usBufferLen bytes over
 * the client connection.
 *
 * \param xTCPHdl A handle to a TCP server instance.
 * \param xTCPClientHdl A handle for a TCP client.
 * \param pubBuffer A buffer of \c usBufferLen bytes which should be transmitted.
 * \param usBufferLen Number of bytes to transmit.
 *
 * \return The function should return eMBErrorCode::MB_ENOERR if all bytes have
 *   been written. In case of an I/O error it should return eMBErrorCode::MB_EIO.
 *   In case of an invalid handle or invalid arguments it should return
 *   eMBErrorCode::MB_EINVAL. All other errors should be mapped to eMBErrorCode::MB_EPORTERR.
 */
eMBErrorCode
eMBPTCPConWrite( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, const UBYTE * pubBuffer, USHORT usBufferLen )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;

    U8 *sendbuf;
    U16 maxlen;
    BOOL bWriteError = FALSE;
	
	
    if( NULL != xTCPClientHdl )
    {
        if (!tcp_check_send (pxTCPIntClientHdl->iSocket))
        {
            bWriteError = TRUE;
        }
        else
        {
            /* The socket is ready to send the data. */
            maxlen = tcp_max_dsize (pxTCPIntClientHdl->iSocket);
            if( maxlen < usBufferLen )
            {
                bWriteError = TRUE;
            }
            else
            {
                sendbuf = tcp_get_buf (usBufferLen);
                memcpy (sendbuf, pubBuffer, usBufferLen);
                tcp_send (pxTCPIntClientHdl->iSocket, sendbuf, usBufferLen);
            }
        }

        if( bWriteError )
        {
            prvxMBTCPIntClientHandleReset( &xMBTCPSlaveHdls[0], TRUE );
            ( void )xMBTCPSlaveHdls[0].eMBPTCPClientDisconnectedFN( xMBTCPSlaveHdls[0].xMBHdl, xTCPClientHdl );
        }
        eStatus = MB_ENOERR;
    }
    return eStatus;
}
