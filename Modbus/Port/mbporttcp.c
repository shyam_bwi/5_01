/*****************************************************************************
* @copyright Copyright (c) 2014-2015 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : mbporttcp.c
* @brief               : Porting layer for Modbus Slave
*
* @author              : Rajeev Gadgil
*
* @date Created        : 19 September Friday, 2014  <September 19, 2014>
* @date Last Modified  : 19 September Friday, 2014  <September 19, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/


/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <RTL.h>
#include "RTOS_main.h"
#include <stdio.h>
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"

/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbframe.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"

/* ----------------------- Defines ------------------------------------------*/
#define MAX_SLAVE_HDLS                      ( 2 )       /* Should be equal to MBS_TCP_MAX_INSTANCES. In addition the TCPIP-Config.h must allow enough handles */
#define MAX_SLAVE_CLIENT_HDLS               ( 2 )       /* Should be equal to MBS_TCP_MAX_CLIENTS. In addition the TCPIP-Config.h must allow enough handles */
#define IDX_INVALID                         ( 255 )

#ifndef MBP_TCP_DEBUG
#define MBP_TCP_DEBUG                       ( 0 )
#endif

#define INVALID_SOCKET 											( 0 )
typedef unsigned char TCP_SOCKET;
#define MBP_TCP_HDL_COMMON \
    UBYTE           ubIdx; \
    xMBTCPIntHandleType eType; \
    xMBHandle       xMBHdl; \
    peMBPTCPClientNewDataCB eMBPTCPClientNewDataFN; \
    peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFN;

/* ----------------------- Type definitions ---------------------------------*/

typedef enum
{
    CLIENTHDL_INIT_STATE,
    CLIENTHDL_DISCONNECT_SLAVE,
    CLIENTHDL_CLOSE_SLAVE,
    CLIENTHDL_CLOSE_MASTER
} eClientHandleResetMode;

typedef struct
{
    TCP_SOCKET      xSocket;
    BYTE            arbClientRcvBuffer[260];
    USHORT          usClientRcvBufferPos;
    USHORT          usClientRcvBufferLen;		
    enum
    { SOCK_LISTENING, SOCK_CONNECTED } eState;
} xMBPTCPIntClientHandle;

typedef enum
{
    TCP_MODBUS_UNKNOWN,
    TCP_MODBUS_MASTER,
    TCP_MODBUS_SLAVE
} xMBTCPIntHandleType;

typedef struct
{
    MBP_TCP_HDL_COMMON
} xMBPTCPIntCommonHandle;

typedef struct
{
    MBP_TCP_HDL_COMMON
    xMBPTCPIntClientHandle xClientCon;
} xMBPTCPIntMasterHandle;

typedef struct
{
    MBP_TCP_HDL_COMMON
    xMBPTCPIntClientHandle xClientCons[MAX_SLAVE_CLIENT_HDLS];
    peMBPTCPClientConnectedCB eMBPTCPClientConnectedFN;
} xMBPTCPIntSlaveHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC xMBPTCPIntSlaveHandle xMBTCPSlaveHdls[MAX_SLAVE_HDLS];
STATIC BOOL     bIsInitalized = FALSE;

/* ----------------------- Static functions ---------------------------------*/
STATIC void     vMBPTCPInit( void );
STATIC void     vMBTCPCommonHandleReset( xMBPTCPIntCommonHandle * pxTCPHdl );
STATIC void     vMBTCPClientHandleReset( xMBPTCPIntClientHandle * pxClientHdl, eClientHandleResetMode eMode );
STATIC void     vMBTCPSlaveHandleReset( xMBPTCPIntSlaveHandle * pxTCPSlaveHdl, eClientHandleResetMode eMode );
//STATIC void     prvvMBTCPPortTCPAccept( xMBPTCPIntSlaveHandle * pxTCPIntSlaveHdl, TCP_SOCKET xSocket );

#if defined( MBP_ENABLE_DEBUG_FACILITY ) && ( MBP_ENABLE_DEBUG_FACILITY == 1 )
STATIC const char *pszMBTCPHdlType2Str( xMBTCPIntHandleType );
#endif


/* ----------------------- Start implementation -----------------------------*/

U16 prvxMBTCPPortTCPListenCallback1 (U8 soc, U8 event, U8 *ptr, U16 par)
{
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[0];
    eMBErrorCode eStatus;
    U16 retval = 0;
		U8 ubIdx = 0;
    ( void )eStatus;
	
		for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( pxTCPIntSlaveHdl->xClientCons ); ubIdx++ )
		{
			if(pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket == soc)
			{
				break;
			}
		}							

    switch (event)
    {
    case TCP_EVT_CONREQ:
				retval = 1;
        break;

    case TCP_EVT_ABORT:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket ) );
      vMBTCPClientHandleReset( &( pxTCPIntSlaveHdl->xClientCons[ubIdx] ),CLIENTHDL_DISCONNECT_SLAVE );
      tcp_abort( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );	
			//tcp_release_socket(pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );
			if(ubIdx == 0)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG;
				Rprts_diag_var.Ethernet_Status = Port_string2; 		
			}
			else if(ubIdx ==1)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG2;
				Rprts_diag_var.Ethernet_Status = Port_string2; 						
			}
      break;

    case TCP_EVT_CONNECT:
			if( MB_ENOERR != ( eStatus = pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket ) ) ) )
			{
					vMBTCPClientHandleReset( &( pxTCPIntSlaveHdl->xClientCons[ubIdx] ),CLIENTHDL_DISCONNECT_SLAVE );
					tcp_abort( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );					
					retval = 0;
			}
			else
			{
				if(ubIdx == 0)
				{
					Flags_struct.Connection_flags |= ETH_CON_FLAG;
					Rprts_diag_var.Ethernet_Status = Port_string1; //on				
				}
				else if(ubIdx == 1)
				{
					Flags_struct.Connection_flags |= ETH_CON_FLAG2;
					Rprts_diag_var.Ethernet_Status = Port_string1; //on								
				}
				pxTCPIntSlaveHdl->xClientCons[ubIdx].eState = SOCK_CONNECTED;
				retval = 1;
			}
      break;

    case TCP_EVT_CLOSE:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket ) );
      vMBTCPClientHandleReset( &( pxTCPIntSlaveHdl->xClientCons[ubIdx] ),CLIENTHDL_DISCONNECT_SLAVE );
      tcp_abort( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );	
			//tcp_release_socket (pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );

			if(ubIdx == 0)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG;
				Rprts_diag_var.Ethernet_Status = Port_string2; 		
			}
			else if(ubIdx ==1)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG2;
				Rprts_diag_var.Ethernet_Status = Port_string2; 						
			}
      break;

    case TCP_EVT_ACK:
			retval = 1;
      break;

    case TCP_EVT_DATA:
      if( par < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls[0].xClientCons[ubIdx].arbClientRcvBuffer ) )
      {
          memcpy( xMBTCPSlaveHdls[0].xClientCons[ubIdx].arbClientRcvBuffer, ptr, par );
          xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferLen = par;
          xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferPos = 0;
      }
// 		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
//       break;

  }
  return retval;
}


U16 prvxMBTCPPortTCPListenCallback2 (U8 soc, U8 event, U8 *ptr, U16 par)
{
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[0];
    eMBErrorCode eStatus;
    U16 retval = 0;
		U8 ubIdx = 0;
    ( void )eStatus;
	
		for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( pxTCPIntSlaveHdl->xClientCons ); ubIdx++ )
		{
			if(pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket == soc)
			{
				break;
			}
		}							

    switch (event)
    {
    case TCP_EVT_CONREQ:
				retval = 1;
        break;

    case TCP_EVT_ABORT:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket ) );
      vMBTCPClientHandleReset( &( pxTCPIntSlaveHdl->xClientCons[ubIdx] ),CLIENTHDL_DISCONNECT_SLAVE );
      tcp_abort( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );	
			//tcp_release_socket(pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );
			if(ubIdx == 0)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG;
				Rprts_diag_var.Ethernet_Status = Port_string2; 		
			}
			else if(ubIdx ==1)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG2;
				Rprts_diag_var.Ethernet_Status = Port_string2; 						
			}
      break;

    case TCP_EVT_CONNECT:
			if( MB_ENOERR != ( eStatus = pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket ) ) ) )
			{
					vMBTCPClientHandleReset( &( pxTCPIntSlaveHdl->xClientCons[ubIdx] ),CLIENTHDL_DISCONNECT_SLAVE );
					tcp_abort( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );					
					retval = 0;
			}
			else
			{
				if(ubIdx == 0)
				{
					Flags_struct.Connection_flags |= ETH_CON_FLAG;
					Rprts_diag_var.Ethernet_Status = Port_string1; //on				
				}
				else if(ubIdx == 1)
				{
					Flags_struct.Connection_flags |= ETH_CON_FLAG2;
					Rprts_diag_var.Ethernet_Status = Port_string1; //on								
				}
				pxTCPIntSlaveHdl->xClientCons[ubIdx].eState = SOCK_CONNECTED;
				retval = 1;
			}
      break;

    case TCP_EVT_CLOSE:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket ) );
      vMBTCPClientHandleReset( &( pxTCPIntSlaveHdl->xClientCons[ubIdx] ),CLIENTHDL_DISCONNECT_SLAVE );
      tcp_abort( pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );	
			//tcp_release_socket (pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket );

			if(ubIdx == 0)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG;
				Rprts_diag_var.Ethernet_Status = Port_string2; 		
			}
			else if(ubIdx ==1)
			{
				Flags_struct.Connection_flags &= ~ETH_CON_FLAG2;
				Rprts_diag_var.Ethernet_Status = Port_string2; 						
			}
      break;

    case TCP_EVT_ACK:
			retval = 1;
      break;

    case TCP_EVT_DATA:
      if( par < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls[0].xClientCons[ubIdx].arbClientRcvBuffer ) )
      {
          memcpy( xMBTCPSlaveHdls[0].xClientCons[ubIdx].arbClientRcvBuffer, ptr, par );
          xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferLen = par;
          xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferPos = 0;
      }
// 		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
//       break;

  }
  return retval;
}

typedef U16 (*ptMBSTCP_Callback) (U8 soc, U8 event, U8 *ptr, U16 par);
ptMBSTCP_Callback arTCPSlaveCallback[MAX_SLAVE_CLIENT_HDLS]=
{
&prvxMBTCPPortTCPListenCallback1,
&prvxMBTCPPortTCPListenCallback2,
};

eMBErrorCode
eMBPTCPServerInit( xMBPTCPHandle * pxTCPHdl, CHAR * pcBindAddress,
                   USHORT usTCPPort,
                   xMBHandle xMBHdlArg,
                   peMBPTCPClientNewDataCB eMBPTCPClientNewDataFNArg,
                   peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFNArg,
                   peMBPTCPClientConnectedCB eMBPTCPClientConnectedFNArg )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = NULL;
    UBYTE           ubIdx;
//    int             iResult;
    TCP_SOCKET      xSock;
    vMBPTCPInit(  );
    if( NULL != pxTCPHdl )
    {
        MBP_ENTER_CRITICAL_SECTION(  );
        for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( xMBTCPSlaveHdls ); ubIdx++ )
        {
            if( IDX_INVALID == xMBTCPSlaveHdls[ubIdx].ubIdx )
            {
                pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[ubIdx];
                vMBTCPSlaveHandleReset( pxTCPIntSlaveHdl, CLIENTHDL_INIT_STATE );
                pxTCPIntSlaveHdl->ubIdx = ubIdx;
                break;
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
        if( NULL != pxTCPIntSlaveHdl )
        {

            eStatus = MB_ENOERR;
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( pxTCPIntSlaveHdl->xClientCons ); ubIdx++ )
            {

							if(ubIdx >= MAX_SLAVE_CLIENT_HDLS)
							break;
							
							//if(0 == ( xSock = tcp_get_socket( (TCP_TYPE_SERVER | TCP_TYPE_KEEP_ALIVE) , 0, 60, prvxMBTCPPortTCPListenCallback ) )  )
							if(0 == ( xSock = tcp_get_socket( (TCP_TYPE_SERVER | TCP_TYPE_KEEP_ALIVE) , 0, 60, arTCPSlaveCallback[ubIdx] ) )  )
							{
									eStatus = MB_ENORES;
							}
							else if( !tcp_listen( xSock, usTCPPort ) )
							{
									eStatus = MB_EPORTERR;
							}
							else
							{
								
									pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket = xSock;
									pxTCPIntSlaveHdl->xClientCons[ubIdx].eState = SOCK_LISTENING;					
							}
						
             }
					
            if( MB_ENOERR != eStatus )
            {
                vMBTCPSlaveHandleReset( pxTCPIntSlaveHdl, CLIENTHDL_CLOSE_SLAVE );
            }
            else
            {
                pxTCPIntSlaveHdl->eMBPTCPClientNewDataFN = eMBPTCPClientNewDataFNArg;
                pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN = eMBPTCPClientDisconnectedFNArg;
                pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN = eMBPTCPClientConnectedFNArg;
                pxTCPIntSlaveHdl->xMBHdl = xMBHdlArg;
                *pxTCPHdl = pxTCPIntSlaveHdl;
                eStatus = MB_ENOERR;
            }
				}
        else
        {
            eStatus = MB_ENORES;
        }
    }
    return eStatus;
}

STATIC void
vMBTCPSlaveHandleReset( xMBPTCPIntSlaveHandle * pxTCPSlaveHdl, eClientHandleResetMode eMode )
{
    UBYTE           ubIdx;

    MBP_ASSERT( NULL != pxTCPSlaveHdl );
    for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( pxTCPSlaveHdl->xClientCons ); ubIdx++ )
    {
        vMBTCPClientHandleReset( &( pxTCPSlaveHdl->xClientCons[ubIdx] ), eMode );

    }
    vMBTCPCommonHandleReset( ( xMBPTCPIntCommonHandle * ) pxTCPSlaveHdl );
    pxTCPSlaveHdl->eMBPTCPClientConnectedFN = NULL;
    pxTCPSlaveHdl->eType = TCP_MODBUS_SLAVE;
}

eMBErrorCode
eMBTCPServerClose( xMBPTCPHandle xTCPHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = xTCPHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTCPIntSlaveHdl, xMBTCPSlaveHdls ) && ( pxTCPIntSlaveHdl->eType == TCP_MODBUS_SLAVE ) )
    {
        vMBTCPSlaveHandleReset( pxTCPIntSlaveHdl, CLIENTHDL_CLOSE_SLAVE );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );

    return eStatus;
}

void
vMBTCPServerPoll( void )
{
    UBYTE           ubIdx, ubIdx2;
    eMBErrorCode    eStatus;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl;

    for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls ); ubIdx++ )
    {
        if( IDX_INVALID != xMBTCPSlaveHdls[ubIdx].ubIdx )
        {
            pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[ubIdx];
            /* Check for pending connections. */
            for( ubIdx2 = 0; ubIdx2 < MB_UTILS_NARRSIZE( pxTCPIntSlaveHdl->xClientCons ); ubIdx2++ )
            {
                /* Check for new data */
                if( ( SOCK_CONNECTED == pxTCPIntSlaveHdl->xClientCons[ubIdx2].eState )
                    && (tcp_get_state( pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket ) == TCP_STATE_CONNECT)
										&&  xMBTCPSlaveHdls[ubIdx].xClientCons[ubIdx2].usClientRcvBufferLen)
                {
                    eStatus =
                        xMBTCPSlaveHdls[ubIdx].eMBPTCPClientNewDataFN( xMBTCPSlaveHdls[ubIdx].xMBHdl,
                                                                       &pxTCPIntSlaveHdl->xClientCons[ubIdx2] );
									
                    MBP_ASSERT( MB_ENOERR == eStatus );
                }
            }
        }
    }
}

/* --------------------------------------------------------------------------*/
/* ----------------------- COMMON CODE --------------------------------------*/
/* --------------------------------------------------------------------------*/

STATIC void
vMBPTCPInit(  )
{
    UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( !bIsInitalized )
    {
        for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( xMBTCPSlaveHdls ); ubIdx++ )
        {
            vMBTCPSlaveHandleReset( &xMBTCPSlaveHdls[ubIdx], FALSE );
        }
        bIsInitalized = TRUE;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
}

eMBErrorCode
eMBPTCPConClose( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;

    ( void )pxTCPIntCommonHdl;

    if( NULL != xTCPClientHdl )
    {
#if defined( MBP_ENABLE_DEBUG_FACILITY ) && ( MBP_ENABLE_DEBUG_FACILITY == 1 )
        if( bMBPPortLogIsEnabled( MB_LOG_DEBUG, MB_LOG_PORT_TCP ) )
        {
            vMBPPortLog( MB_LOG_DEBUG, MB_LOG_PORT_TCP,
                         "[%s=%hu] closing client connection %hu\n",
                         pszMBTCPHdlType2Str( pxTCPIntCommonHdl->eType ), ( USHORT ) pxTCPIntCommonHdl->ubIdx,
                         pxTCPIntClientHdl->ubIdx );
        }
#endif
        MBP_ENTER_CRITICAL_SECTION(  );
        if( TCP_MODBUS_SLAVE == pxTCPIntCommonHdl->eType )
        {
            vMBTCPClientHandleReset( pxTCPIntClientHdl, CLIENTHDL_DISCONNECT_SLAVE );
        }
        else
        {
            vMBTCPClientHandleReset( pxTCPIntClientHdl, CLIENTHDL_CLOSE_MASTER );
        }
        MBP_EXIT_CRITICAL_SECTION(  );
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

void
 vMBTCPClientHandleReset( xMBPTCPIntClientHandle * pxClientHdl, eClientHandleResetMode eMode )
{
		pxClientHdl->usClientRcvBufferLen = 0;
		pxClientHdl->usClientRcvBufferPos = 0;
		memset(pxClientHdl->arbClientRcvBuffer,0,sizeof(pxClientHdl->arbClientRcvBuffer));
		if(pxClientHdl->xSocket == xMBTCPSlaveHdls[0].xClientCons[0].xSocket)
		{
			Flags_struct.Connection_flags &= ~ETH_CON_FLAG;
			Rprts_diag_var.Ethernet_Status = Port_string2; 		
		}
		else if(pxClientHdl->xSocket == xMBTCPSlaveHdls[0].xClientCons[1].xSocket)
		{
			Flags_struct.Connection_flags &= ~ETH_CON_FLAG2;
			Rprts_diag_var.Ethernet_Status = Port_string2; 						
		}	
    switch ( eMode )
    {
    case CLIENTHDL_INIT_STATE:
        pxClientHdl->eState = SOCK_LISTENING;
        pxClientHdl->xSocket = INVALID_SOCKET;
				pxClientHdl->usClientRcvBufferLen = 0;
        break;
    case CLIENTHDL_DISCONNECT_SLAVE:
        tcp_abort( pxClientHdl->xSocket );
        pxClientHdl->eState = SOCK_LISTENING;
        break;
    case CLIENTHDL_CLOSE_SLAVE:
        tcp_close( pxClientHdl->xSocket );
        pxClientHdl->eState = SOCK_LISTENING;
        pxClientHdl->xSocket = INVALID_SOCKET;
        break;
    case CLIENTHDL_CLOSE_MASTER:
        tcp_close( pxClientHdl->xSocket );
        pxClientHdl->eState = SOCK_LISTENING;
        pxClientHdl->xSocket = INVALID_SOCKET;
        break;
    }
}

STATIC void
vMBTCPCommonHandleReset( xMBPTCPIntCommonHandle * pxTCPCommonHdl )
{
    pxTCPCommonHdl->ubIdx = IDX_INVALID;
    pxTCPCommonHdl->eType = TCP_MODBUS_UNKNOWN;
    pxTCPCommonHdl->xMBHdl = MB_HDL_INVALID;
    pxTCPCommonHdl->eMBPTCPClientDisconnectedFN = NULL;
    pxTCPCommonHdl->eMBPTCPClientNewDataFN = NULL;
}

eMBErrorCode
eMBPTCPConRead( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, UBYTE * pubBuffer,
                USHORT * pusBufferLen, USHORT usBufferMax )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;
		xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
    USHORT usToCopy;
		U8 ubIdx = 0;

			for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls[0].xClientCons ); ubIdx++ )
		{
			if(xMBTCPSlaveHdls[0].xClientCons[ubIdx].xSocket == pxTCPIntClientHdl->xSocket)
			{
				break;
			}
		}							

	( void )pxTCPIntCommonHdl;
	
    if( NULL != xTCPClientHdl )
    {
        if( usBufferMax > xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferLen )
        {
            usToCopy = xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferLen;
        }
        else
        {
            usToCopy = usBufferMax;
        }
        memcpy( pubBuffer, &( xMBTCPSlaveHdls[0].xClientCons[ubIdx].arbClientRcvBuffer[xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferPos] ), usToCopy );
        *pusBufferLen = usToCopy;
        xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferPos += usToCopy;
        xMBTCPSlaveHdls[0].xClientCons[ubIdx].usClientRcvBufferLen -= usToCopy;
        eStatus = MB_ENOERR;
    }
    return eStatus;	
}

eMBErrorCode
eMBPTCPConWrite( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, const UBYTE * pubBuffer, USHORT usBufferLen )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;

    U8 *sendbuf;
    U16 maxlen;
    //BOOL bWriteError = 0;
	
		( void )pxTCPIntCommonHdl;
    if( NULL != xTCPClientHdl )
    {
        if (!tcp_check_send (pxTCPIntClientHdl->xSocket))
        {
            ;
        }
        else
        {
            /* The socket is ready to send the data. */
            maxlen = tcp_max_dsize (pxTCPIntClientHdl->xSocket);
            if( maxlen < usBufferLen )
            {
                ;
            }
            else
            {
                sendbuf = tcp_get_buf (usBufferLen);
                memcpy (sendbuf, pubBuffer, usBufferLen);
                tcp_send (pxTCPIntClientHdl->xSocket, sendbuf, usBufferLen);
            }
        }

       eStatus = MB_ENOERR;
    }
    return eStatus;	
}

#if defined( MBP_ENABLE_DEBUG_FACILITY ) && ( MBP_ENABLE_DEBUG_FACILITY == 1 )
const char     *
pszMBTCPHdlType2Str( xMBTCPIntHandleType eType )
{
    const char     *pszRetVal;

    switch ( eType )
    {
    case TCP_MODBUS_UNKNOWN:
        pszRetVal = "?";
        break;
    case TCP_MODBUS_MASTER:
        pszRetVal = "MBM";
        break;
    case TCP_MODBUS_SLAVE:
        pszRetVal = "MBS";
        break;
    }
    return pszRetVal;
}
#endif
