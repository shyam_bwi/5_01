/*
 * MODBUS Library: PCI32/TCPIP  port
 * Copyright (c) 2011 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbporttcp.c,v 1.1 2011/05/01 18:06:42 embedded-solutions.cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <RTL.h>
#include "RTOS_main.h"
#include <stdio.h>

/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbframe.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"

/* ----------------------- Defines ------------------------------------------*/
#define MAX_SLAVE_HDLS                      ( 2 )       /* Should be equal to MBS_TCP_MAX_INSTANCES. In addition the TCPIP-Config.h must allow enough handles */
#define MAX_SLAVE_CLIENT_HDLS               ( 2 )       /* Should be equal to MBS_TCP_MAX_CLIENTS. In addition the TCPIP-Config.h must allow enough handles */
#define IDX_INVALID                         ( 255 )
#define INVALID_SOCKET											( 0xFE )
#ifndef MBP_TCP_DEBUG
#define MBP_TCP_DEBUG                       ( 1 )
#endif

#define MBP_TCP_HDL_COMMON \
    UBYTE           ubIdx; \
    xMBTCPIntHandleType eType; \
    xMBHandle       xMBHdl; \
    peMBPTCPClientNewDataCB eMBPTCPClientNewDataFN; \
    peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFN;

/* ----------------------- Type definitions ---------------------------------*/
typedef unsigned char TCP_SOCKET;

typedef enum
{
    CLIENTHDL_INIT_STATE,
    CLIENTHDL_DISCONNECT_SLAVE,
    CLIENTHDL_CLOSE_SLAVE,
    CLIENTHDL_CLOSE_MASTER
} eClientHandleResetMode;

typedef struct
{
    TCP_SOCKET      xSocket;
    enum
    { SOCK_LISTENING, SOCK_CONNECTED } eState;
} xMBPTCPIntClientHandle;

typedef enum
{
    TCP_MODBUS_UNKNOWN,
    TCP_MODBUS_MASTER,
    TCP_MODBUS_SLAVE
} xMBTCPIntHandleType;

typedef struct
{
    MBP_TCP_HDL_COMMON;
} xMBPTCPIntCommonHandle;

typedef struct
{
    MBP_TCP_HDL_COMMON;
    xMBPTCPIntClientHandle xClientCon;
} xMBPTCPIntMasterHandle;

typedef struct
{
    MBP_TCP_HDL_COMMON;
    xMBPTCPIntClientHandle xClientCons[MAX_SLAVE_CLIENT_HDLS];
    peMBPTCPClientConnectedCB eMBPTCPClientConnectedFN;
} xMBPTCPIntSlaveHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC xMBPTCPIntSlaveHandle xMBTCPSlaveHdls[MAX_SLAVE_HDLS];
STATIC BOOL     bIsInitalized = FALSE;

/* ----------------------- Static functions ---------------------------------*/
STATIC void     vMBPTCPInit( void );
STATIC void     vMBTCPCommonHandleReset( xMBPTCPIntCommonHandle * pxTCPHdl );
STATIC void     vMBTCPClientHandleReset( xMBPTCPIntClientHandle * pxClientHdl, eClientHandleResetMode eMode );
STATIC void     vMBTCPSlaveHandleReset( xMBPTCPIntSlaveHandle * pxTCPSlaveHdl, eClientHandleResetMode eMode );
//STATIC void     prvvMBTCPPortTCPAccept( xMBPTCPIntSlaveHandle * pxTCPIntSlaveHdl, TCP_SOCKET xSocket );
U16 prvxMBTCPPortTCPListenCallback (U8 soc, U8 event, U8 *ptr, U16 par) ;

#if defined( MBP_ENABLE_DEBUG_FACILITY ) && ( MBP_ENABLE_DEBUG_FACILITY == 1 )
STATIC const char *pszMBTCPHdlType2Str( xMBTCPIntHandleType );
#endif


/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBPTCPServerInit( xMBPTCPHandle * pxTCPHdl, CHAR * pcBindAddress,
                   USHORT usTCPPort,
                   xMBHandle xMBHdlArg,
                   peMBPTCPClientNewDataCB eMBPTCPClientNewDataFNArg,
                   peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFNArg,
                   peMBPTCPClientConnectedCB eMBPTCPClientConnectedFNArg )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = NULL;
    UBYTE           ubIdx;
    int             iResult;
    TCP_SOCKET      xSock;
    vMBPTCPInit(  );
    if( NULL != pxTCPHdl )
    {

       if( IDX_INVALID == xMBTCPSlaveHdls[0].ubIdx )
        {
            pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[0];
            if( 0 == ( xSock = tcp_get_socket( (TCP_TYPE_SERVER | TCP_TYPE_KEEP_ALIVE) , 0, 60, prvxMBTCPPortTCPListenCallback ) ) )
            {
                eStatus = MB_ENORES;
            }
            else if( !tcp_listen( xSock, usTCPPort ) )
            {
                eStatus = MB_EPORTERR;
            }
            else
            {
                //pxTCPIntSlaveHdl->ubIdx = 0;
                pxTCPIntSlaveHdl->xClientCons->xSocket = xSock;
                pxTCPIntSlaveHdl->eMBPTCPClientNewDataFN = eMBPTCPClientNewDataFNArg;
                pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN = eMBPTCPClientDisconnectedFNArg;
                pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN = eMBPTCPClientConnectedFNArg;
                pxTCPIntSlaveHdl->xMBHdl = xMBHdlArg;
                *pxTCPHdl = pxTCPIntSlaveHdl;
                eStatus = MB_ENOERR;
							  #ifdef MODBUS_TCP
							  //socket_num = xServerSoc;
							  #endif
            }
        }
        else
        {
            eStatus = MB_ENORES;
        }
    }
    return eStatus;			
//         MBP_ENTER_CRITICAL_SECTION(  );
//         for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( xMBTCPSlaveHdls ); ubIdx++ )
//         {
//             if( IDX_INVALID == xMBTCPSlaveHdls[ubIdx].ubIdx )
//             {
//                 pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[ubIdx];
//                 vMBTCPSlaveHandleReset( pxTCPIntSlaveHdl, CLIENTHDL_INIT_STATE );
//                 pxTCPIntSlaveHdl->ubIdx = ubIdx;
//                 break;
//             }
//         }
//         MBP_EXIT_CRITICAL_SECTION(  );
//         if( NULL != pxTCPIntSlaveHdl )
//         {

//             eStatus = MB_ENOERR;
//             for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( pxTCPIntSlaveHdl->xClientCons ); ubIdx++ )
//             {
// 								if( 0 == ( xSock = tcp_get_socket( TCP_TYPE_SERVER , 0, 60, prvxMBTCPPortTCPListenCallback ) ) )
// 								{
// 										eStatus = MB_ENORES;
// 								}
// 								if( !tcp_listen( xSock, usTCPPort ) )
// 								{
// 										eStatus = MB_EPORTERR;
// 								}							
//                 else
//                 {
//                     /* Clear first time reset flag */
//                     pxTCPIntSlaveHdl->xClientCons[ubIdx].xSocket = xSock;
//                     pxTCPIntSlaveHdl->xClientCons[ubIdx].eState = SOCK_LISTENING;
//                 }
//             }

//             if( MB_ENOERR != eStatus )
//             {
//                 vMBTCPSlaveHandleReset( pxTCPIntSlaveHdl, CLIENTHDL_CLOSE_SLAVE );
//             }
//             else
//             {
//                 pxTCPIntSlaveHdl->eMBPTCPClientNewDataFN = eMBPTCPClientNewDataFNArg;
//                 pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN = eMBPTCPClientDisconnectedFNArg;
//                 pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN = eMBPTCPClientConnectedFNArg;
//                 pxTCPIntSlaveHdl->xMBHdl = xMBHdlArg;
//                 *pxTCPHdl = pxTCPIntSlaveHdl;
//                 eStatus = MB_ENOERR;
//             }
//         }
//         else
//         {
//             eStatus = MB_ENORES;
//         }
//     }
//     return eStatus;
}

STATIC void
vMBTCPSlaveHandleReset( xMBPTCPIntSlaveHandle * pxTCPSlaveHdl, eClientHandleResetMode eMode )
{
    UBYTE           ubIdx;

    MBP_ASSERT( NULL != pxTCPSlaveHdl );
    for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( pxTCPSlaveHdl->xClientCons ); ubIdx++ )
    {
        vMBTCPClientHandleReset( &( pxTCPSlaveHdl->xClientCons[ubIdx] ), eMode );

    }
    vMBTCPCommonHandleReset( ( xMBPTCPIntCommonHandle * ) pxTCPSlaveHdl );
    pxTCPSlaveHdl->eMBPTCPClientConnectedFN = NULL;
    pxTCPSlaveHdl->eType = TCP_MODBUS_SLAVE;
}

eMBErrorCode
eMBTCPServerClose( xMBPTCPHandle xTCPHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl = xTCPHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTCPIntSlaveHdl, xMBTCPSlaveHdls ) && ( pxTCPIntSlaveHdl->eType == TCP_MODBUS_SLAVE ) )
    {
        vMBTCPSlaveHandleReset( pxTCPIntSlaveHdl, CLIENTHDL_CLOSE_SLAVE );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );

    return eStatus;
}

void
vMBTCPServerPoll( void )
{
    UBYTE           ubIdx, ubIdx2;
    eMBErrorCode    eStatus;
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl;

    for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xMBTCPSlaveHdls ); ubIdx++ )
    {
        if( IDX_INVALID != xMBTCPSlaveHdls[ubIdx].ubIdx )
        {
            pxTCPIntSlaveHdl = &xMBTCPSlaveHdls[ubIdx];
            /* Check for pending connections. */
            for( ubIdx2 = 0; ubIdx2 < MB_UTILS_NARRSIZE( pxTCPIntSlaveHdl->xClientCons ); ubIdx2++ )
            {
                /* Check if a previously disconnected socket now has a client connection pending */
                if( ( SOCK_LISTENING == pxTCPIntSlaveHdl->xClientCons[ubIdx2].eState )
										&& (tcp_get_state (pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket) == TCP_STATE_LISTEN))
                    //&& TCPIsConnected( pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket ) )
                {
                    //( void )TCPWasReset( pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket );
										
                    if( MB_ENOERR !=
                        ( eStatus =
                          pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN( pxTCPIntSlaveHdl->xMBHdl,
                                                                      &( pxTCPIntSlaveHdl->xClientCons[ubIdx2] ) ) ) )
                    {
                        vMBTCPClientHandleReset( &( pxTCPIntSlaveHdl->xClientCons[ubIdx] ),
                                                 CLIENTHDL_DISCONNECT_SLAVE );
                        tcp_abort ( pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket );
                    }
                    else
                    {
                        pxTCPIntSlaveHdl->xClientCons[ubIdx2].eState = SOCK_CONNECTED;
                    }
                }

                /* Check if a previous connected socket now is disconnected */
                if( ( SOCK_CONNECTED == pxTCPIntSlaveHdl->xClientCons[ubIdx2].eState )
									&& (tcp_get_state (pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket) == TCP_STATE_CLOSED))
                    //&& TCPWasReset( pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket ) )
                {
                    eStatus =
                        xMBTCPSlaveHdls[ubIdx].eMBPTCPClientDisconnectedFN( xMBTCPSlaveHdls[ubIdx].xMBHdl,
                                                                            &pxTCPIntSlaveHdl->xClientCons[ubIdx2] );
                    MBP_ASSERT( MB_ENOERR == eStatus );
                }

                /* Check for new data */
                if( ( SOCK_CONNECTED == pxTCPIntSlaveHdl->xClientCons[ubIdx2].eState )
									&& (tcp_get_state (pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket) == TCP_STATE_CONNECT))
                    //&& TCPIsGetReady( pxTCPIntSlaveHdl->xClientCons[ubIdx2].xSocket ) )
                {
                    eStatus =
                        xMBTCPSlaveHdls[ubIdx].eMBPTCPClientNewDataFN( xMBTCPSlaveHdls[ubIdx].xMBHdl,
                                                                       &pxTCPIntSlaveHdl->xClientCons[ubIdx2] );
                    MBP_ASSERT( MB_ENOERR == eStatus );
                }
            }
        }
    }
}

/* --------------------------------------------------------------------------*/
/* ----------------------- COMMON CODE --------------------------------------*/
/* --------------------------------------------------------------------------*/

STATIC void
vMBPTCPInit(  )
{
    UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( !bIsInitalized )
    {
        for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( xMBTCPSlaveHdls ); ubIdx++ )
        {
            vMBTCPSlaveHandleReset( &xMBTCPSlaveHdls[ubIdx], 0 );
        }
        bIsInitalized = TRUE;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
}

eMBErrorCode
eMBPTCPConClose( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;

    ( void )pxTCPIntCommonHdl;

    if( NULL != xTCPClientHdl )
    {
#if defined( MBP_ENABLE_DEBUG_FACILITY ) && ( MBP_ENABLE_DEBUG_FACILITY == 1 )
        if( bMBPPortLogIsEnabled( MB_LOG_DEBUG, MB_LOG_PORT_TCP ) )
        {
            vMBPPortLog( MB_LOG_DEBUG, MB_LOG_PORT_TCP,
                         "[%s=%hu] closing client connection %hu\n",
                         pszMBTCPHdlType2Str( pxTCPIntCommonHdl->eType ), ( USHORT ) pxTCPIntCommonHdl->ubIdx,
                         pxTCPIntClientHdl->ubIdx );
        }
#endif
        MBP_ENTER_CRITICAL_SECTION(  );
        if( TCP_MODBUS_SLAVE == pxTCPIntCommonHdl->eType )
        {
            vMBTCPClientHandleReset( pxTCPIntClientHdl, CLIENTHDL_DISCONNECT_SLAVE );
        }
        else
        {
            vMBTCPClientHandleReset( pxTCPIntClientHdl, CLIENTHDL_CLOSE_MASTER );
        }
        MBP_EXIT_CRITICAL_SECTION(  );
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

void
vMBTCPClientHandleReset( xMBPTCPIntClientHandle * pxClientHdl, eClientHandleResetMode eMode )
{
    switch ( eMode )
    {
    case CLIENTHDL_INIT_STATE:
        pxClientHdl->eState = SOCK_LISTENING;
        pxClientHdl->xSocket = INVALID_SOCKET;
        break;
    case CLIENTHDL_DISCONNECT_SLAVE:
        tcp_abort( pxClientHdl->xSocket );
        pxClientHdl->eState = SOCK_LISTENING;
        break;
    case CLIENTHDL_CLOSE_SLAVE:
        tcp_close( pxClientHdl->xSocket );
        pxClientHdl->eState = SOCK_LISTENING;
        pxClientHdl->xSocket = INVALID_SOCKET;
        break;
    case CLIENTHDL_CLOSE_MASTER:
        tcp_close( pxClientHdl->xSocket );
        pxClientHdl->eState = SOCK_LISTENING;
        pxClientHdl->xSocket = INVALID_SOCKET;
        break;
    }
}

STATIC void
vMBTCPCommonHandleReset( xMBPTCPIntCommonHandle * pxTCPCommonHdl )
{
    pxTCPCommonHdl->ubIdx = IDX_INVALID;
    pxTCPCommonHdl->eType = TCP_MODBUS_UNKNOWN;
    pxTCPCommonHdl->xMBHdl = MB_HDL_INVALID;
    pxTCPCommonHdl->eMBPTCPClientDisconnectedFN = NULL;
    pxTCPCommonHdl->eMBPTCPClientNewDataFN = NULL;
}

eMBErrorCode
eMBPTCPConRead( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, UBYTE * pubBuffer,
                USHORT * pusBufferLen, USHORT usBufferMax )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;
    int             iBytesReceived;
	( void )pxTCPIntCommonHdl;

    if( NULL != xTCPClientHdl )
    {
        //iBytesReceived = TCPGetArray( pxTCPIntClientHdl->xSocket, ( BYTE * ) pubBuffer, ( WORD ) usBufferMax );
				//iBytesReceived = recv( pxTCPIntClientHdl->xSocket, ( BYTE * ) pubBuffer, ( U16 ) usBufferMax ,0);
        if( 0 == iBytesReceived )
        {
            *pusBufferLen = 0;
            eStatus = MB_EIO;
        }
        else
        {
            *pusBufferLen = ( USHORT ) iBytesReceived;
            eStatus = MB_ENOERR;
        }
    }
    return eStatus;
}

eMBErrorCode
eMBPTCPConWrite( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, const UBYTE * pubBuffer, USHORT usBufferLen )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
    xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;
    //int             iBytesWritten;
		USHORT maxlen = 0;
		UBYTE * sendbuf;
    ( void )pxTCPIntCommonHdl;
    if( NULL != xTCPClientHdl )
    {
				if(!tcp_check_send (pxTCPIntClientHdl->xSocket))
        {
            eStatus = MB_EIO;
        }
        else
        {
						maxlen = tcp_max_dsize (pxTCPIntClientHdl->xSocket);
						sendbuf = tcp_get_buf (maxlen);
						memcpy (sendbuf, ( BYTE * ) pubBuffer, maxlen);
						tcp_send (pxTCPIntClientHdl->xSocket, sendbuf, maxlen);					
            eStatus = MB_ENOERR;
        }
    }
    return eStatus;
}

#if defined( MBP_ENABLE_DEBUG_FACILITY ) && ( MBP_ENABLE_DEBUG_FACILITY == 1 )
const char     *
pszMBTCPHdlType2Str( xMBTCPIntHandleType eType )
{
    const char     *pszRetVal;

    switch ( eType )
    {
    case TCP_MODBUS_UNKNOWN:
        pszRetVal = "?";
        break;
    case TCP_MODBUS_MASTER:
        pszRetVal = "MBM";
        break;
    case TCP_MODBUS_SLAVE:
        pszRetVal = "MBS";
        break;
    }
    return pszRetVal;
}
#endif

U16 prvxMBTCPPortTCPListenCallback (U8 soc, U8 event, U8 *ptr, U16 par)
{
    xMBPTCPIntSlaveHandle *pxTCPIntSlaveHdl;
    eMBErrorCode eStatus;
    U16 retval = 0;
	
    ( void )eStatus;
    switch (event)
    {
    case TCP_EVT_CONREQ:
        if( MB_ENOERR != ( eStatus = pxTCPIntSlaveHdl->eMBPTCPClientConnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons->xSocket ) ) ) )
        {
            retval = 0;
        }
        else
        {
            pxTCPIntSlaveHdl->xClientCons->xSocket = soc;
            retval = 1;
        }
        break;

    case TCP_EVT_ABORT:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons->xSocket ) );
      break;

    case TCP_EVT_CONNECT:
      retval = 1;
      break;

    case TCP_EVT_CLOSE:
      eStatus = pxTCPIntSlaveHdl->eMBPTCPClientDisconnectedFN( pxTCPIntSlaveHdl->xMBHdl, &( pxTCPIntSlaveHdl->xClientCons->xSocket ) );
      break;

    case TCP_EVT_ACK:
			retval = 1;
      break;

     case TCP_EVT_DATA:
			 retval = 1;
// 				memcpy( xMBTCPSlaveHdls[0].arbClientRcvBuffer, ptr, par );
// 				xMBTCPSlaveHdls[0].usClientRcvBufferLen = par;
// 				xMBTCPSlaveHdls[0].usClientRcvBufferPos = 0;
			break;
// 		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
//       break;

  }
  return retval;
}
