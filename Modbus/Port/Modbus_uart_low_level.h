/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename        : Modbus_uart_low_level.h
* @brief               : Controller Board
*
* @author               : Anagha Basole
*
* @date Created         : September 13th 2012
* @date Last Modified   : September 13th 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __MODBUS_UART_LOW_LEVEL_H
#define __MODBUS_UART_LOW_LEVEL_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_pinsel.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define SENSOR_BOARD                 0
#define INTEGRATOR_BOARD             1

#define MB_SERIAL_PORT_INT           ( MB_UART_1 )
#define MB_SERIAL_BAUDRATE_INT       ( 921600 )
#define MB_PARITY                    ( MB_PAR_NONE )

#define _SENSOR_UART                 LPC_UART2
#define _SENSOR_UART_IRQ             UART2_IRQn
#define _SENSOR_UART_IRQHandler      UART2_IRQHandler

#define _INTEGRATOR_UART             (LPC_UART_TypeDef *) LPC_UART4
#define _INTEGRATOR_UART_IRQ         UART4_IRQn
#define _INTEGRATOR_UART_IRQHandler  UART4_IRQHandler

#define NORMAL_MODE                  0x00
#define SLAVE_ADDR_CHANGE            0x01
#define SLAVE_TO_MASTER_STACK        0x02
#define MASTER_TO_SLAVE_STACK        0x04

#define SEMAPHORE_RELEASED           0x00
#define SEMAPHORE_ACQUIRED           0x01
/*============================================================================
* Public Data Types
*===========================================================================*/
/*Modbus Variable Structure*/
typedef struct
             {
							  uint8_t sensor_integrator_port;
							  uint8_t master_slave_stack_reset;
							  uint8_t current_slave_addr;
							  uint8_t user_semaphore;
						 }MODBUS_RTU_VAR_STRUCT;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
//extern uint8_t sensor_integrator_port;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern MODBUS_RTU_VAR_STRUCT Modbus_rtu_var;
/*============================================================================
* Public Function Declarations
*===========================================================================*/

/* Interrupt service routines */
extern void _SENSOR_UART_IRQHandler(void);
extern void _INTEGRATOR_UART_IRQHandler(void);
extern int  modbus_uart_init(int uart2_4);

extern uint32_t UARTReceive(LPC_UART_TypeDef *UARTPort, uint8_t *rxbuf, uint32_t buflen);
extern uint32_t UARTSend(LPC_UART_TypeDef *UARTPort, uint8_t *txbuf, uint32_t buflen);

#endif /*__MODBUS_UART_LOW_LEVEL_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
