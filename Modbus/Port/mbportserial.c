/* 
 * MODBUS Library: Skeleton port
 * Copyright (c) 2008 Christian Walter <wolti@sil.at>
 * All rights reserved.
 *
 * $Id: mbportserial.c,v 1.1 2008/04/06 07:47:26 cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>

/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"
#include "LPC177x_8x.h"
#include "Modbus_uart_low_level.h"
#include "Rtos_main.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define IDX_INVALID					( 255 )
#define UART_BAUDRATE_MIN		( 300 )
#define UART_BAUDRATE_MAX		( 921600 )

#define HDL_RESET( x ) do { \
	( x )->ubIdx = IDX_INVALID; \
	( x )->pbMBPTransmitterEmptyFN = NULL; \
	( x )->pvMBPReceiveFN = NULL; \
	( x )->xMBMHdl = MB_HDL_INVALID; \
} while( 0 );

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
    pbMBPSerialTransmitterEmptyAPIV1CB pbMBPTransmitterEmptyFN;
    pvMBPSerialReceiverAPIV1CB pvMBPReceiveFN;
    xMBHandle       xMBMHdl;
} xSerialHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC xSerialHandle xSerialHdls[2];
STATIC BOOL     bIsInitalized = FALSE;
/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBPSerialInit( xMBPSerialHandle * pxSerialHdl, UCHAR ucPort, ULONG ulBaudRate,
                UCHAR ucDataBits, eMBSerialParity eParity, UCHAR ucStopBits, xMBHandle xMBMHdl )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    //UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( !bIsInitalized )
    {
        /*for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( xSerialHdls ); ubIdx++ )
        {
            HDL_RESET( &xSerialHdls[ubIdx] );
        }*/
			  if (Modbus_rtu_var.sensor_integrator_port == SENSOR_BOARD)
				{
					  HDL_RESET( &xSerialHdls[SENSOR_BOARD] );
				}
				else if (Modbus_rtu_var.sensor_integrator_port == INTEGRATOR_BOARD)
				{
					  HDL_RESET( &xSerialHdls[INTEGRATOR_BOARD] );
				}
    }

    if( NULL == pxSerialHdl )
    {
        eStatus = MB_EINVAL;
    }
    else if(( SENSOR_BOARD == ucPort) && (IDX_INVALID == xSerialHdls[SENSOR_BOARD].ubIdx))
    {
        *pxSerialHdl = NULL;

        if((ulBaudRate >= UART_BAUDRATE_MIN) && (ulBaudRate <= UART_BAUDRATE_MAX) && (MB_HDL_INVALID != xMBMHdl))
        {
            /* Initialize the serial port here. */
					  modbus_uart_init(Modbus_rtu_var.sensor_integrator_port);
						xSerialHdls[SENSOR_BOARD].ubIdx = SENSOR_BOARD;
						xSerialHdls[SENSOR_BOARD].xMBMHdl = xMBMHdl;
						*pxSerialHdl = &xSerialHdls[SENSOR_BOARD];
						eStatus = MB_ENOERR;
        }
        else
        {
            eStatus = MB_EINVAL;
        }
    }
		else if((INTEGRATOR_BOARD == ucPort) && (IDX_INVALID == xSerialHdls[INTEGRATOR_BOARD].ubIdx))
    {
        *pxSerialHdl = NULL;

        if((ulBaudRate >= UART_BAUDRATE_MIN) && (ulBaudRate <= UART_BAUDRATE_MAX) && (MB_HDL_INVALID != xMBMHdl))
        {
            /* Initialize the serial port here. */
					  modbus_uart_init(Modbus_rtu_var.sensor_integrator_port);
						xSerialHdls[INTEGRATOR_BOARD].ubIdx = INTEGRATOR_BOARD;
						xSerialHdls[INTEGRATOR_BOARD].xMBMHdl = xMBMHdl;
						*pxSerialHdl = &xSerialHdls[INTEGRATOR_BOARD];
						eStatus = MB_ENOERR;
        }
        else
        {
            eStatus = MB_EINVAL;
        }
    }
    else
    {
        eStatus = MB_ENORES;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eMBPSerialClose( xMBPSerialHandle xSerialHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xSerialHandle  *pxSerialIntHdl = xSerialHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxSerialIntHdl, xSerialHdls ) )
    {
        if (pxSerialIntHdl->pbMBPTransmitterEmptyFN == NULL )
        {
            /* Close the serial port here. */
						if (Modbus_rtu_var.sensor_integrator_port == SENSOR_BOARD)
						{
							UART_DeInit(_SENSOR_UART);
						}
						else if (Modbus_rtu_var.sensor_integrator_port == INTEGRATOR_BOARD)
						{
							UART_DeInit(_INTEGRATOR_UART);
						}							
            eStatus = MB_ENOERR;
        }
        else
        {
            eStatus = MB_EAGAIN;
        }
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eMBPSerialTxEnable( xMBPSerialHandle xSerialHdl, pbMBPSerialTransmitterEmptyCB pbMBPTransmitterEmptyFN )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xSerialHandle  *pxSerialIntHdl = xSerialHdl;
    UBYTE           ubTxByte;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxSerialIntHdl, xSerialHdls ) )
    {
        eStatus = MB_ENOERR;
        if( NULL != pbMBPTransmitterEmptyFN )
        {
            MBP_ASSERT( NULL == pxSerialIntHdl->pbMBPTransmitterEmptyFN );
            pxSerialIntHdl->pbMBPTransmitterEmptyFN = pbMBPTransmitterEmptyFN;
                        
						if (Modbus_rtu_var.sensor_integrator_port == SENSOR_BOARD)
						{
							  /*Enable the transmitter. */
								/* Enable Interrupt for UART channel */
								if( NULL != xSerialHdls[SENSOR_BOARD].pbMBPTransmitterEmptyFN )
								{
										xSerialHdls[SENSOR_BOARD].pbMBPTransmitterEmptyFN( xSerialHdls[SENSOR_BOARD].xMBMHdl, &ubTxByte );
								}
								while (UART_CheckBusy(_SENSOR_UART));
								_SENSOR_UART->THR = ubTxByte;								
						}
						else if (Modbus_rtu_var.sensor_integrator_port == INTEGRATOR_BOARD)
						{
								/*Enable the transmitter. */
								/* Enable Interrupt for UART channel */
								if( NULL != xSerialHdls[INTEGRATOR_BOARD].pbMBPTransmitterEmptyFN )
								{
										xSerialHdls[INTEGRATOR_BOARD].pbMBPTransmitterEmptyFN( xSerialHdls[INTEGRATOR_BOARD].xMBMHdl, &ubTxByte );
								}

								while (UART_CheckBusy(_INTEGRATOR_UART));
								LPC_UART4->THR = ubTxByte;								
						}						
        }
        else
        {
            pxSerialIntHdl->pbMBPTransmitterEmptyFN = NULL;
            
            /* Disable the transmitter. Make sure that all characters have been
             * transmitted in case you do any buffering internally.
             */
						if (Modbus_rtu_var.sensor_integrator_port == SENSOR_BOARD)
						{
								while (UART_CheckBusy(_SENSOR_UART));
								UART_IntConfig(_SENSOR_UART, UART_INTCFG_THRE, DISABLE);
						}
						else if (Modbus_rtu_var.sensor_integrator_port == INTEGRATOR_BOARD)
						{
								while (UART_CheckBusy(_INTEGRATOR_UART));								
								UART_IntConfig(_INTEGRATOR_UART, UART_INTCFG_THRE, DISABLE);
						}
        }
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}


eMBErrorCode
eMBPSerialRxEnable( xMBPSerialHandle xSerialHdl, pvMBPSerialReceiverCB pvMBPReceiveFN )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xSerialHandle  *pxSerialIntHdl = xSerialHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxSerialIntHdl, xSerialHdls ) )
    {
        eStatus = MB_ENOERR;
        if( NULL != pvMBPReceiveFN )
        {
            MBP_ASSERT( NULL == pxSerialIntHdl->pvMBPReceiveFN );
            pxSerialIntHdl->pvMBPReceiveFN = pvMBPReceiveFN;

						if (Modbus_rtu_var.sensor_integrator_port == SENSOR_BOARD)
						{
								/* Enable UART Rx interrupt */
								UART_IntConfig(_SENSOR_UART, UART_INTCFG_RBR, ENABLE);
						}
						else if (Modbus_rtu_var.sensor_integrator_port == INTEGRATOR_BOARD)
						{
								/* Enable UART Rx interrupt */
								UART_IntConfig(_INTEGRATOR_UART, UART_INTCFG_RBR, ENABLE);								
						}									
        }
        else
        {
            pxSerialIntHdl->pvMBPReceiveFN = NULL;
            /* Disable the receiver. */
						if (Modbus_rtu_var.sensor_integrator_port == SENSOR_BOARD)
						{
								/* Disable UART Rx interrupt */
								UART_IntConfig(_SENSOR_UART, UART_INTCFG_RBR, DISABLE);
						}
						else if (Modbus_rtu_var.sensor_integrator_port == INTEGRATOR_BOARD)
						{
								/* Disable UART Rx interrupt */
								UART_IntConfig(_INTEGRATOR_UART, UART_INTCFG_RBR, DISABLE);
						}
        }
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

void _SENSOR_UART_IRQHandler( void )
{
    BOOL            bHasMoreData;
    UBYTE           ubTxByte;
		uint32_t intsrc, tmp, tmp1;

		// Determine the interrupt source 
		intsrc = UART_GetIntId(_SENSOR_UART);
		tmp = intsrc & UART_IIR_INTID_MASK;

	  if (tmp == UART_IIR_INTID_RLS)
		{
			 tmp1 = LPC_UART2->LSR;
			 // Check line status
			 tmp1 = UART_GetLineStatus(_SENSOR_UART);
			 //There are errors or break interrupt, read LSR will clear the interrupt.
			 tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE);
			 // If any error exist
			 if (tmp1) 
			 {
					tmp1 = LPC_UART2->RBR; // Dummy read on RX to clear interrupt, then bail out
			 }
			 return;
		}
		// Receive Data Available or Character time-out
		if ((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI))
		{
				ubTxByte = UART_ReceiveByte(LPC_UART2);
		
        if( NULL != xSerialHdls[SENSOR_BOARD].pvMBPReceiveFN )
        {
            xSerialHdls[SENSOR_BOARD].pvMBPReceiveFN( xSerialHdls[SENSOR_BOARD].xMBMHdl, ubTxByte );
        }
    }
		// Transmit Holding Empty
		if (tmp == UART_IIR_INTID_THRE)
		{
			/* call transmitter callback function if transmitter is ready */
			if( NULL != xSerialHdls[SENSOR_BOARD].pbMBPTransmitterEmptyFN )
			{
					bHasMoreData = xSerialHdls[SENSOR_BOARD].pbMBPTransmitterEmptyFN( xSerialHdls[SENSOR_BOARD].xMBMHdl, &ubTxByte );
			}
			if( !bHasMoreData)
			{
					/* wait till all characters are transmitted. make sure that the
					 * uart transmit fifo is empty before changing the RS485 
					 * direction.
					 */
					xSerialHdls[SENSOR_BOARD].pbMBPTransmitterEmptyFN = NULL;
			}
			else
			{
					/* write character in ubTxByte to USART */
					UART_SendByte(LPC_UART2, ubTxByte);
			}
    }
}

void _INTEGRATOR_UART_IRQHandler( void )
{
    BOOL            bHasMoreData;
    UBYTE           ubTxByte;
		uint32_t intsrc, tmp, tmp1;

		// Determine the interrupt source 
		intsrc = UART_GetIntId(_INTEGRATOR_UART);
		tmp = intsrc & UART_IIR_INTID_MASK;

	  if (tmp == UART_IIR_INTID_RLS)
		{
			 tmp1 = LPC_UART4->LSR;
			 // Check line status
			 tmp1 = UART_GetLineStatus(_INTEGRATOR_UART);
			 //There are errors or break interrupt, read LSR will clear the interrupt.
			 tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE);
			 // If any error exist
			 if (tmp1) 
			 {
					tmp1 = LPC_UART4->RBR; // Dummy read on RX to clear interrupt, then bail out
			 }
			 return;
		}
		// Receive Data Available or Character time-out
		if ((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI))
		{
				/* call receive callback function is character is received */
				/* read character into ubTxByte */
			  ubTxByte = UART_ReceiveByte((LPC_UART_TypeDef *)LPC_UART4);

				if( NULL != xSerialHdls[INTEGRATOR_BOARD].pvMBPReceiveFN )
				{
						xSerialHdls[INTEGRATOR_BOARD].pvMBPReceiveFN( xSerialHdls[INTEGRATOR_BOARD].xMBMHdl, ubTxByte );
				}
    }
		
		// Transmit Holding Empty
		if (tmp == UART_IIR_INTID_THRE)
		{
			/* call transmitter callback function if transmitter is ready */
			if( NULL != xSerialHdls[INTEGRATOR_BOARD].pbMBPTransmitterEmptyFN )
			{
					bHasMoreData = xSerialHdls[INTEGRATOR_BOARD].pbMBPTransmitterEmptyFN( xSerialHdls[INTEGRATOR_BOARD].xMBMHdl, &ubTxByte );
			}
			if( !bHasMoreData)
			{
					/* wait till all characters are transmitted. make sure that the
					 * uart transmit fifo is empty before changing the RS485 
					 * direction.
					 */
					xSerialHdls[INTEGRATOR_BOARD].pbMBPTransmitterEmptyFN = NULL;
			}
			else
			{
					/* write character in ubTxByte to USART */
					UART_SendByte((LPC_UART_TypeDef *)LPC_UART4, ubTxByte);
			}
    }
}
