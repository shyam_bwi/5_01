/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : File_update.c
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 26, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 26, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     :
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "File_update.h"
#include <RTL.h>
#include <float.h>
#include <string.h>
#include "Screen_global_ex.h"
#include "EEPROM_high_level.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "Log_report_data_calculate.h"
#include "Ext_flash_high_level.h"
//#include "Screen_global_data.h"
#ifdef FILE_SYS
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
char Logfile_Get_Line_Buff[1000] __attribute__((section ("COMMON_DATA_RAM"),zero_init));
/* unsigned integer variables section */
U16 ferror_cnt = 0;

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

__align(4) FILE_HEADER_STRUCT 		fheader;
__align(4) SET_ZERO_LOG_STRUCT 		set_zero_data_log;
__align(4) CLR_DATA_LOG_STRUCT 		clear_data_log;
__align(4) CAL_DATA_LOG_STRUCT 		calib_data_log;
__align(4) LENGTH_DATA_LOG_STRUCT length_cal_data_log;
__align(4) ERR_DATA_LOG_STRUCT 		error_data_log __attribute__ ((section ("GUI_DATA_RAM"))); //Added this variable to SDRAM by DK on 27 Jan 2016
__align(4) FREQUENT_REPORT_STRUCT daily_rprt, weekly_rprt, monthly_rprt;
__align(4) ERR_REPORT_STRUCT 			error_rprt;
__align(4) CAL_REPORT_STRUCT 			calibration_rprt;
__align(4) Q_REC_INFO							last_sent_mb_record;

__align(4) PERIODIC_LOG_STRUCT 		periodic_data,periodic_data_mb[30] __attribute__ ((section ("GUI_DATA_RAM")));
__align(4) PERIODIC_LOG_STRUCT 		periodic_data_send_mb __attribute__ ((section ("GUI_DATA_RAM")));

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: U8 log_file_init(void)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 27/7/12
* @brief      Description	 : Creates the files and write the headers to the file
* @note       Notes		     : None
*****************************************************************************/
U8 log_file_init(char file_name[FILE_NAME_SIZE])
{
    FILE *fp;
    U8 err = 0;
    U8 u8File_exists = 0, Error = 0;
    unsigned int LastRecord_No = 0;
    char *Ptr;
    static char Buff1[10] = ",";



    /*Create and initialize the header of the periodic log file*/
    //PVK - 1 Apr 2016 Commented
    //fp = fopen (file_name, "w");

    //PVK - 1 Apr 2016 Added
    fp = fopen (file_name, "r");
    if (fp!= NULL)
    {
        u8File_exists = 1;
        fclose(fp);
    }
    else
    {
        u8File_exists = 0;
        //fclose(fp);
    }

    fflush(fp);

    if(u8File_exists == 0)
    {
        fp = fopen (file_name, "w");
    }
    else
    {
        //Open file in read mode to get record number
        fp = fopen (file_name, "r");
    }

    if(fp == NULL)
    {
        err = 1;
        return 0;
    }
    else
    {
        if(u8File_exists == 0)
        {
            fprintf(fp, "%s,",fheader.File_hdr.Data_type);
            fprintf(fp, " %d,", fheader.File_hdr.Id);
            fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
                    fheader.File_hdr.Date.Mon,
                    fheader.File_hdr.Date.Day);
            fprintf(fp, " %02d:%02d:%02d,", fheader.File_hdr.Time.Hr,
                    fheader.File_hdr.Time.Min,
                    fheader.File_hdr.Time.Sec);
            fprintf(fp, " %s,",Admin_var.Scale_Name);
            fprintf(fp, " %s,",Admin_var.Plant_Name);
            fprintf(fp, " %s,",Admin_var.Product_Name);
            fprintf(fp, " %s\r\n",Admin_var.Int_firmware_version);
            if(strcmp("calib_log.txt",file_name) == 0)
            {
                fprintf(fp,"Type,ID#, Date, Time, Scale name, Plant name, Product name, Cal pulse count,Cal duration (Min), Belt Speed, Belt Speed Unit, Prev. zero value, New zero value, Angle,Accumulated Cal Weight, Accumulated weight unit, Test Weight, Test Weight unit,Certified scale weight, Certified Scale Unit, Belt Scale Weight, Belt Scale weight Unit,Previous TRIM factor, New Trim factor, Idler Distance1, Idler Distance2,Unit");
                calib_data_log.Cal_data_hdr.Id = 0;
            }
            else if(strcmp("System_Log.txt",file_name) == 0)
            {
                ;
            }
            else
            {
               fprintf(fp,"Function, Record #, Date, Time, Accumulated Weight, Weight Unit, Rate, Rate Unit, Load Percent, Angle, Belt speed, Speed unit,Daily weight, Daily Weight Unit,Master Total,Daily Total Pound,Zero Number Pound,Speed (ft_per_min),Inst Rate (Pounds_per_Hour)\r\n");
             periodic_data.Function_code = PER_HDR;
                //periodic_data.Periodic_hdr.Id = 0;	//SKS : The record number should never roll over
               // periodic_data.Periodic_hdr.Id = LPC_RTC->GPREG2;
            }
        }
        //Added By PVK 4 Apr 2016
        else
        {
            //Go to the end of file and skip \n and \r character of last line
            while(1)
            {
                if(fgets(Logfile_Get_Line_Buff,sizeof(Logfile_Get_Line_Buff),fp) == NULL)
                {
                    break;
                }
            }

            Ptr = strtok(Logfile_Get_Line_Buff,"\n");
            if(Ptr != NULL)
            {
                Ptr = strtok(Ptr, Buff1);
                if(Ptr != NULL)
                {
                    Ptr = strtok(NULL, Buff1);
                    Error = 1;
                }
                else
                {
                    Error = 0;
                }
            }
            else
            {
                Error = 0;
            }

            //check for file without record only header information
            if(Error == 1)
            {
                if((strcmp(Ptr, "Record #") != 0) ||(strcmp(Ptr, "ID#") != 0))
                {
                    LastRecord_No =  atoi(Ptr);
                    Error = 1;
                }
                else
                {
                    Error = 0;
                }
            }
            //get record number
            //if((Error == 0) || (FileSize  <= 0) || ( FileSize > Total_FileSize))
            if(Error == 0)
            {
                if(strcmp("calib_log.txt",file_name) == 0)
                {
                    calib_data_log.Cal_data_hdr.Id = 0;
                }
                else
                {
                    periodic_data.Periodic_hdr.Id = 0;
                    clear_data_log.Clr_data_hdr.Id = 0;
                }
            }
            else
            {
                if(strcmp("calib_log.txt",file_name) == 0)
                {
                    calib_data_log.Cal_data_hdr.Id = LastRecord_No;
                }
                else
                {
                    periodic_data.Periodic_hdr.Id = LastRecord_No;
                    clear_data_log.Clr_data_hdr.Id = LastRecord_No;
                }
            }
        }//end of else

        if(ferror(fp))
        {
//			printf("\n File write error-%s", file_name);
            return 0;
        }
    }

    if(0 != fclose(fp))
    {
//		printf ("\n %s file could not be closed!", file_name);
        return 0;
    }
    fflush (stdout);
    return (err);
}


/*****************************************************************************
* @note       Function name: U8 debug_log_write(const U8 *buf)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : pointer to the data to be written to the file
* @author			             : Anagha Basole
* @date       date created : 15/2/13
* @brief      Description	 : Creates the file if not present and writes the
*														 debug messages to it
* @note       Notes		     : None
*****************************************************************************/
U8 debug_log_write(const U8 *buf)
{
    FILE *fp;
    static U8 file_error = 0;

    if (Flags_struct.Connection_flags & USB_CON_FLAG)
        //if(con)
    {
        fp = fopen ("debug_log.txt", "a");
        if(fp == NULL)
        {
            if (file_error == 0)
            {
//				printf("\n Error in opening debug file");
            }
            file_error = 1;
        }
        else
        {
            file_error = 0;
            //fwrite(buf, 1, sizeof(buf), fp);
            fprintf(fp,"%s",buf);
            if(ferror(fp))
            {
//				printf("\n Debug file write error");
            }
        }
        if(0 != fclose(fp))
        {
//			printf ("\nDebug file could not be closed!\n");
            file_error = 1;
        }
        fflush (stdout);
    }
    return (file_error);
}

/*****************************************************************************
* @note       Function name: U8 log_file_write(U8 log_type)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : type of data log to be written
* @author			             : Anagha Basole
* @date       date created : 26/7/12
* @brief      Description	 : Writes the current data log in the file
* @note       Notes		     : None
*****************************************************************************/

U8 log_file_write(U8 log_type)
{

    U8 err = 0;
    U8 u8File_exists = 0;
    FILE *fp,*fpw;
    char file_name[FILE_NAME_SIZE]= {0};
    //write the structure in the file
    switch(log_type)
    {
    /*if the log is a periodic data log*/
    case periodic_log_stop:
    case periodic_log_start:
    case periodic_log:
    case periodic_log_stop_at_start:
    case total_clr_wt_log:
    case calibration_finished:
    case calibration_cancel:
    case periodic_tst_wt_log:
    case periodic_dig_log:
    case periodic_mat_log:
    case periodic_zer_log:
    case periodic_len_log:
    case daily_wt_clr_log:	/*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/
        sprintf(&file_name[0],"%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",periodic_data.Periodic_hdr.Date.Year,periodic_data.Periodic_hdr.Date.Mon,
                periodic_data.Periodic_hdr.Date.Year, periodic_data.Periodic_hdr.Date.Mon,periodic_data.Periodic_hdr.Date.Day);
        fp = fopen (file_name, "r");
        if (fp != NULL)
        {
            u8File_exists = 1;
            fclose(fp);
        }
        else
            u8File_exists = 0;
//fclose(fp);
        fflush(fp);
        if(u8File_exists == 0)
        {
            fp = fopen (file_name, "w");
        }
        else
        {
            fp = fopen (file_name, "a");
        }

//fp = fopen (file_name, "a");
        if(fp == NULL)
        {
//				printf("\n Error in opening periodic log file");
            ferror_cnt++;
            err = 1;
            return 0;
        }
        else
        {
            if(u8File_exists == 0)
            {
                fprintf(fp, "%s,",fheader.File_hdr.Data_type);
                fprintf(fp, " %d,", fheader.File_hdr.Id);
                fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
                        fheader.File_hdr.Date.Mon,
                        fheader.File_hdr.Date.Day);
                fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
                        fheader.File_hdr.Time.Min,
                        fheader.File_hdr.Time.Sec);
                fprintf(fp, " %s,",Admin_var.Scale_Name);
                fprintf(fp, " %s,",Admin_var.Plant_Name);
                fprintf(fp, " %s,",Admin_var.Product_Name);
                fprintf(fp, " %s\r\n",Admin_var.Int_firmware_version);
          fprintf(fp,"Function, Record #, Date, Time, Accumulated Weight, Weight Unit, Rate, Rate Unit, Load Percent, Angle, Belt speed, Speed unit,Daily weight, Daily Weight Unit,Master Total,Daily Total Pound,Zero Number Pound,Speed (ft_per_min),Inst Rate (Pounds_per_Hour)\r\n");
            }
            fprintf(fp, " %s,", periodic_data.Periodic_hdr.Data_type);
            fprintf(fp, " %d,", periodic_data.Periodic_hdr.Id);
            fprintf(fp, " %d/%d/%d,", periodic_data.Periodic_hdr.Date.Year,
                    periodic_data.Periodic_hdr.Date.Mon,
                    periodic_data.Periodic_hdr.Date.Day);
            fprintf(fp, " %02d:%02d:%02d,", periodic_data.Periodic_hdr.Time.Hr,
                    periodic_data.Periodic_hdr.Time.Min,
                    periodic_data.Periodic_hdr.Time.Sec);
            fprintf(fp, " %11.6lf,", periodic_data.Accum_weight);
            fprintf(fp, " %s,", periodic_data.Accum_weight_unit);
            fprintf(fp, " %f,",periodic_data.Rate);
            fprintf(fp, " %s/%s,",periodic_data.Accum_weight_unit,periodic_data.Rate_unit);
            fprintf(fp, " %f,", periodic_data.Inst_load_pcnt);
            fprintf(fp, " %f,", periodic_data.Inst_conv_angle);
            fprintf(fp, " %f,", periodic_data.Inst_conv_speed);
            fprintf(fp, " %s,", Strings[Units_var.Unit_speed].Text);
            fprintf(fp, " %11.6lf,", periodic_data.daily_weight);
            fprintf(fp, " %s,", periodic_data.Accum_weight_unit);
            fprintf(fp, " %lld,", periodic_data.master_total_pound);
						fprintf(fp, " %lld,", periodic_data.daily_total_pound);
						fprintf(fp, " %d,", periodic_data.zero_number_pound);
						fprintf(fp, " %d,", periodic_data.Speed_Feet_Min);
						fprintf(fp, " %lld\r\n", periodic_data.Inst_Rate_Pounds_Hour);
            if(ferror(fp))
            {
                ferror_cnt++;
//					printf("\n Periodic log file write error");
            }
            fclose(fp);
            flash_para_record_flag(SET_FLAG);
        }
        break;

    /*if the log is a set zero function data log*/
    case set_zero_log:
    case calib_log:
    case length_log:
    case digital_log:
    case mat_test_log:
    case tst_wt_log:
        sprintf(&file_name[0],"calib_log.txt");
        fp = fopen (file_name, "r");
        if (fp != NULL)
        {
            u8File_exists = 1;
            fclose(fp);
        }
        else
            u8File_exists = 0;

        fflush(fp);
        if(u8File_exists == 0)
        {
            fp = fopen (file_name, "w");
        }
        else
        {
            fp = fopen (file_name, "a");
        }

        //fp = fopen ("calib_log.txt", "a");
        //fp = fopen ("set_zero_log.txt", "a");
        if(fp == NULL)
        {
//				printf("\n Error in opening calib log file");
            //printf("\n Error in opening set zero file");
            err = 1;
            return 0;
        }
        else
        {
            if(u8File_exists == 0)
            {
                fprintf(fp, "%s,",fheader.File_hdr.Data_type);
                fprintf(fp, " %d,", fheader.File_hdr.Id);
                fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
                        fheader.File_hdr.Date.Mon,
                        fheader.File_hdr.Date.Day);
                fprintf(fp, " %02d:%02d:%02d,", fheader.File_hdr.Time.Hr,
                        fheader.File_hdr.Time.Min,
                        fheader.File_hdr.Time.Sec);
                fprintf(fp, " %s,",Admin_var.Scale_Name);
                fprintf(fp, " %s,",Admin_var.Plant_Name);
                fprintf(fp, " %s,",Admin_var.Product_Name);
                fprintf(fp, " %s\r\n",Admin_var.Int_firmware_version);
                fprintf(fp,"Type,ID#, Date, Time, Scale name, Plant name, Product name, Cal pulse count,Cal duration (Min), Belt Speed, Belt Speed Unit, Prev. zero value, New zero value, Angle,Accumulated Cal Weight, Accumulated weight unit, Test Weight, Test Weight unit,Certified scale weight, Certified Scale Unit, Belt Scale Weight, Belt Scale weight Unit,Previous TRIM factor, New Trim factor, Idler Distance1, Idler Distance2,Unit");
            }
            if(log_type == set_zero_log)
            {
                fprintf(fp, "\r\n ZER,");
                //fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
                fprintf(fp, " %d,", calib_data_log.Cal_data_hdr.Id);
            }
            else if(log_type== length_log)
            {
                fprintf(fp, "\r\nLEN,");
                //fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
                fprintf(fp, " %d,", length_cal_data_log.Len_data_hdr.Id);
            }
            else if(log_type == calib_log)
            {
                fprintf(fp, "\r\nDIG,");
                //fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
                fprintf(fp, " %d,", calib_data_log.Cal_data_hdr.Id);
            }
            else if(log_type == digital_log)
            {
                fprintf(fp, "\r\n DIG,");
                //fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
                fprintf(fp, " %d,", calib_data_log.Cal_data_hdr.Id);
            }
            else if(log_type == mat_test_log)
            {
                fprintf(fp, "\r\n MAT,");
                //fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
                fprintf(fp, " %d,", calib_data_log.Cal_data_hdr.Id);
            }
            else if(log_type == tst_wt_log)
            {
                fprintf(fp, "\r\n TST,");
                //fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
                fprintf(fp, " %d,", calib_data_log.Cal_data_hdr.Id);
            }

            fprintf(fp, "%d/%d/%d,", calibration_rprt.Cal_rprt_header.Date.Year,
                    calibration_rprt.Cal_rprt_header.Date.Mon,
                    calibration_rprt.Cal_rprt_header.Date.Day);
            fprintf(fp, "%02d:%02d:%02d,", calibration_rprt.Cal_rprt_header.Time.Hr,
                    calibration_rprt.Cal_rprt_header.Time.Min,
                    calibration_rprt.Cal_rprt_header.Time.Sec);
            fprintf(fp, "%s,",Admin_var.Scale_Name);
            fprintf(fp, "%s,",Admin_var.Plant_Name);
            fprintf(fp, "%s,",Admin_var.Product_Name);

            fprintf(fp, "%d,", length_cal_data_log.New_nos_pulses);
            fprintf(fp, "%f,", length_cal_data_log.New_time_duration);

            fprintf(fp, "%f,", Calculation_struct.Belt_speed);
            fprintf(fp, "%s/min,", Strings[Scale_setup_var.Speed_unit].Text);
            if(log_type == set_zero_test_report)
            {
                fprintf(fp, "%f,", set_zero_data_log.Prev_set_zero_val);
                fprintf(fp, "%f,", set_zero_data_log.New_set_zero_val);
                fprintf(fp, "%f,", set_zero_data_log.Set_zero_accum_weight);
                fprintf(fp, "%s,", Strings[Scale_setup_var.Weight_unit].Text);
            }
            else
            {
                fprintf(fp, "NA,");
                fprintf(fp, "NA,");
                fprintf(fp, "NA,");
                fprintf(fp, "NA,");
            }
            fprintf(fp, "%f,", set_zero_data_log.Inst_conv_angle);

            fprintf(fp, "%11.6lf,", calib_data_log.Accum_weight);
            fprintf(fp, "%s,", Strings[Scale_setup_var.Weight_unit].Text);
            if(log_type == set_zero_log)
            {
                fprintf(fp, "%f,", Calibration_var.Test_weight);
                fprintf(fp, "%s,", Strings[Calibration_var.Test_zero_weight_unit].Text);
            }
            else
            {
                fprintf(fp, "NA,");
                fprintf(fp, "NA,");
            }
            if(log_type == mat_test_log)
            {
                fprintf(fp, "%f,", calib_data_log.Cert_weight);
                fprintf(fp, "%s,", Strings[Calibration_var.Cert_scale_unit].Text);
                fprintf(fp, "%f,", Calibration_var.Belt_scale_weight);
                fprintf(fp, "%s,", Strings[Calibration_var.Belt_scale_unit].Text);
            }
            else
            {
                fprintf(fp, "NA,");
                fprintf(fp, "NA,");
                fprintf(fp, "NA,");
                fprintf(fp, "NA,");
            }
            fprintf(fp, "%f,", calib_data_log.Prev_span);
            fprintf(fp, "%f,", calib_data_log.New_span);
            fprintf(fp, "%f,", calib_data_log.Idler_spacing1);
            fprintf(fp, "%f,", calib_data_log.Idler_spacing2);
            fprintf(fp, "%f,", calib_data_log.Idler_spacing3);

// 				fprintf(fp, "\r\n%s,", set_zero_data_log.Set_zero_hdr.Data_type);
// 				fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
// 				fprintf(fp, " %d/%d/%d,", set_zero_data_log.Set_zero_hdr.Date.Year,
// 																					set_zero_data_log.Set_zero_hdr.Date.Mon,
// 																					set_zero_data_log.Set_zero_hdr.Date.Day);
// 				fprintf(fp, " %02d:%02d:%02d,", set_zero_data_log.Set_zero_hdr.Time.Hr,
// 																					set_zero_data_log.Set_zero_hdr.Time.Min,
// 																					set_zero_data_log.Set_zero_hdr.Time.Sec);
// 				fprintf(fp, " %f,", set_zero_data_log.Prev_set_zero_val);
// 				fprintf(fp, " %f,", set_zero_data_log.New_set_zero_val);
// 				fprintf(fp, " %11.1f,", set_zero_data_log.belt_length);
// 				fprintf(fp, " %s,", set_zero_data_log.belt_length_unit);
// 				fprintf(fp, " %f", set_zero_data_log.Inst_conv_angle);
            if(ferror(fp))
            {
//					printf("\n Set zero write error");
            }
        }
        break;

    /*if the log is a clear data function data log*/
    case clear_log:
        sprintf(&file_name[0],"%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",periodic_data.Periodic_hdr.Date.Year,periodic_data.Periodic_hdr.Date.Mon,
                periodic_data.Periodic_hdr.Date.Year, periodic_data.Periodic_hdr.Date.Mon,periodic_data.Periodic_hdr.Date.Day);

        fp = fopen (file_name, "r");
        if (fp != NULL)
        {
            u8File_exists = 1;
            fclose(fp);
        }
        else
            u8File_exists = 0;

        fflush(fp);
        if(u8File_exists == 0)
        {
            fp = fopen (file_name, "w");
        }
        else
        {
            fp = fopen (file_name, "a");
        }

        //fp = fopen (file_name, "a");
        //fp = fopen ("clr_data_log.txt", "a");
        if(fp == NULL)
        {
//				printf("\n Error in opening periodic log file");
            //printf("\n Error in opening clear data log file");
            err = 1;
            return 0;
        }
        else
        {
            if(u8File_exists == 0)
            {
                fprintf(fp, "%s,",fheader.File_hdr.Data_type);
                fprintf(fp, " %d,", fheader.File_hdr.Id);
                fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
                        fheader.File_hdr.Date.Mon,
                        fheader.File_hdr.Date.Day);
                fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
                        fheader.File_hdr.Time.Min,
                        fheader.File_hdr.Time.Sec);
                fprintf(fp, " %s,",Admin_var.Scale_Name);
                fprintf(fp, " %s,",Admin_var.Plant_Name);
                fprintf(fp, " %s,",Admin_var.Product_Name);
                fprintf(fp, " %s\r\n",Admin_var.Int_firmware_version);
               fprintf(fp,"Function, Record #, Date, Time, Accumulated Weight, Weight Unit, Rate, Rate Unit, Load Percent, Angle, Belt speed, Speed unit,Daily weight, Daily Weight Unit,Master Total,Daily Total Pound,Zero Number Pound,Speed (ft_per_min),Inst Rate (Pounds_per_Hour)\r\n");
            }
            fprintf(fp, " %s,", clear_data_log.Clr_data_hdr.Data_type);
            fprintf(fp, " %d,", clear_data_log.Clr_data_hdr.Id);
            fprintf(fp, " %d/%d/%d,", periodic_data.Periodic_hdr.Date.Year,
                    periodic_data.Periodic_hdr.Date.Mon,periodic_data.Periodic_hdr.Date.Day);
            fprintf(fp, " %02d:%02d:%02d,", periodic_data.Periodic_hdr.Time.Hr,
                    periodic_data.Periodic_hdr.Time.Min,
                    periodic_data.Periodic_hdr.Time.Sec);
            /*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/
            fprintf(fp, " %11.6lf,",periodic_data.Accum_weight);
            /*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/
            fprintf(fp, " %s,", periodic_data.Accum_weight_unit);
            fprintf(fp, " %11.6f,",periodic_data.Rate);
            fprintf(fp, " %s/%s,",periodic_data.Accum_weight_unit,periodic_data.Rate_unit);
            fprintf(fp, " %f,", periodic_data.Inst_load_pcnt);
            fprintf(fp, " %f,", periodic_data.Inst_conv_angle);
            fprintf(fp, " %11.6f,", periodic_data.Inst_conv_speed);
            fprintf(fp, " %s,", Strings[Units_var.Unit_speed].Text);
            fprintf(fp, " %11.6lf,", periodic_data.daily_weight);
            fprintf(fp, " %s", periodic_data.Accum_weight_unit);
            fprintf(fp, " %lld,", periodic_data.master_total_pound);
												fprintf(fp, " %lld,", periodic_data.daily_total_pound);
						fprintf(fp, " %d,", periodic_data.zero_number_pound);
						fprintf(fp, " %d,", periodic_data.Speed_Feet_Min);
						fprintf(fp, " %lld\r\n", periodic_data.Inst_Rate_Pounds_Hour);
            if(ferror(fp))
            {
//					printf("\n Clear data write error");
            }

            flash_para_record_flag(SET_FLAG);
        }
        break;

    /*if the log is a calibration function data log*/
// 		case calib_log:
// 			fp = fopen ("calib_log.txt", "a");
// 			if(fp == NULL)
// 			{
// //				printf("\n Error in opening calib log file");
// 				err = 1;
// 			}
// 			else
// 			{
// 				fprintf(fp, "\r\n%s,", calib_data_log.Cal_data_hdr.Data_type);
// 				fprintf(fp, " %d,", calib_data_log.Cal_data_hdr.Id);
// 				fprintf(fp, " %d/%d/%d,", calib_data_log.Cal_data_hdr.Date.Year,
// 																					calib_data_log.Cal_data_hdr.Date.Mon,
// 																					calib_data_log.Cal_data_hdr.Date.Day);
// 				fprintf(fp, " %d:%d:%d,", calib_data_log.Cal_data_hdr.Time.Hr,
// 																					calib_data_log.Cal_data_hdr.Time.Min,
// 																					calib_data_log.Cal_data_hdr.Time.Sec);
// 				  //Calculation of Accumilated weight during Calibration
// 				calib_data_log.Accum_weight = Calculation_struct.Total_weight - calib_data_log.Accum_weight;
// 				fprintf(fp, " %11.1f,", calib_data_log.Accum_weight);
// 				fprintf(fp, " %s,", calib_data_log.Accum_weight_unit);
// 				fprintf(fp, " %f,", calib_data_log.Cert_weight);
// 				fprintf(fp, " %s,", calib_data_log.Cert_weight_unit);
// 				fprintf(fp, " %f,", calib_data_log.Prev_span);
// 				fprintf(fp, " %f,", calib_data_log.New_span);
// 				fprintf(fp, " %f,", calib_data_log.Idler_spacing1);
// 				fprintf(fp, " %f,", calib_data_log.Idler_spacing2);
// 				fprintf(fp, " %f,", calib_data_log.Idler_spacing3);
// 				fprintf(fp, " %d", calib_data_log.Cal_type);
// 				if(ferror(fp))
// 				{
// //					printf("\n Calib log write error");
// 				}
// 			}
// 			break;

    /*if the log is a error function data log*/
    case error_log:
        sprintf(&file_name[0],"System_Log.txt");
        fp = fopen (file_name, "r");
        if (fp != NULL)
        {
            u8File_exists = 1;
            fclose(fp);
        }
        else
            u8File_exists = 0;

        fflush(fp);
        if(u8File_exists == 0)
        {
            fp = fopen (file_name, "w");
        }
        else
        {
            fp = fopen (file_name, "a");
        }

        //fp = fopen ("System_Log.txt", "a");
        if(fp == NULL)
        {
//				printf("\n Error in opening error log file");
            err = 1;
            return 0;
        }
        else
        {
            if(u8File_exists == 0)
            {
                //	fprintf(fp, "%s\t",fheader.File_hdr.Data_type);
                //	fprintf(fp, " %d\t", fheader.File_hdr.Id);
                //Added by DK on 2 Feb 2016
                error_data_log.Err_data_hdr.Id = 1;
                fprintf(fp,"Record #\t Date\t Time\t  \tType\t \tMessage\t\r\n");
                //fprintf(fp, " %c\t", '0');
                fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                fprintf(fp, " %04d/%02d/%02d\t", fheader.File_hdr.Date.Year,
                        fheader.File_hdr.Date.Mon,
                        fheader.File_hdr.Date.Day);
                fprintf(fp, " %02d:%02d:%02d\t", fheader.File_hdr.Time.Hr,
                        fheader.File_hdr.Time.Min,
                        fheader.File_hdr.Time.Sec);
                fprintf(fp, " %s\t","Scale Info");
                fprintf(fp, "Scale name =%c%s%c, Plant name =%c%s%c, Product name =%c%s%c, Firmware Version %s\t\r\n",\
                        '"',Admin_var.Scale_Name,'"',\
                        '"',Admin_var.Plant_Name,'"',\
                        '"',Admin_var.Product_Name,'"',\
                        Admin_var.Int_firmware_version);
                /*fprintf(fp, " %s\t",Admin_var.Scale_Name);
                fprintf(fp, " %s\t",Admin_var.Plant_Name);
                fprintf(fp, " %s\t",Admin_var.Product_Name);
                fprintf(fp, " %s\t\r\n",Admin_var.Int_firmware_version);*/
                //Added by DK on 28 January 2016
                //		fprintf(fp,"Record #\t Date\t Time\t  \tType\t \tMessage\t\r\n");
                error_data_log.Err_data_hdr.Id++;
            }
            //fprintf(fp, "\r\n%s\t", error_data_log.Err_data_hdr.Data_type);
            fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
            fprintf(fp, " %04d/%02d/%02d\t", error_data_log.Err_data_hdr.Date.Year,
                    error_data_log.Err_data_hdr.Date.Mon,
                    error_data_log.Err_data_hdr.Date.Day);
            fprintf(fp, " %02d:%02d:%02d\t", error_data_log.Err_data_hdr.Time.Hr,
                    error_data_log.Err_data_hdr.Time.Min,
                    error_data_log.Err_data_hdr.Time.Sec);
            fprintf(fp, " %s\t",error_data_log.Error_code);
            fprintf(fp, " %s\r\n",error_data_log.Error_desc);
            if(ferror(fp))
            {
//					printf("\n Error log file write error");
            }
        }
        break;

    default:
        break;
    }
    if(0 != fclose(fp))
    {
//		printf ("\nFile could not be closed!\n");
        return 0;
    }
    fflush(fp);
    return (err);
}

/*****************************************************************************
* @note       Function name: U8 report_file_write (U8 report_type)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : type of report to be written to be written
* @author			             : Anagha Basole
* @date       date created : 26/7/12
* @brief      Description	 : Writes the current report to the file
* @note       Notes		     : None
*****************************************************************************/
U8 report_file_write (U8 report_type)
{
    FILE *fp;
    U8 err = 0;
//	char file_name[FILE_NAME_SIZE]={0};
    //write the structure in the file
    switch(report_type)
    {
    /*if the report is calibration report*/
    case calib_report:
    case material_test_report:
    case digital_test_report:
    case set_zero_test_report:
    case tst_wt_report:
    case length_cal_report:
        fp = fopen ("calib_report.txt", "a");
        if(fp == NULL)
        {
//				printf("\n Error in opening Cal report file");
            err = 1;
            return 0;
        }
        else
        {
            //fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", calibration_rprt.Cal_rprt_header.Data_type);
            fprintf(fp, "Report Type\t\t\t");
            if(report_type == calib_report)
                fprintf(fp, "DIG\r\n");
            else if(report_type == material_test_report)
                fprintf(fp, "MAT\r\n");
            else if(report_type == digital_test_report)
                fprintf(fp, "DIG\r\n");
            else if(report_type == set_zero_test_report)
                fprintf(fp, "ZER\r\n");
            else if(report_type == length_cal_report)
                fprintf(fp, "LEN\r\n");
            else if(report_type == tst_wt_report)
                fprintf(fp, "TST\r\n");
            fprintf(fp, "Date\t\t\t\t%d/%d/%d\r\n", calibration_rprt.Cal_rprt_header.Date.Year,
                    calibration_rprt.Cal_rprt_header.Date.Mon,
                    calibration_rprt.Cal_rprt_header.Date.Day);
            fprintf(fp, "Time\t\t\t\t%02d:%02d:%02d\r\n", calibration_rprt.Cal_rprt_header.Time.Hr,
                    calibration_rprt.Cal_rprt_header.Time.Min,
                    calibration_rprt.Cal_rprt_header.Time.Sec);
            fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
            fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
            fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);
            fprintf(fp, "Firmware Version\t\t%s\r\n",Admin_var.Int_firmware_version);

            //fprintf(fp, "NO of pulses in 1 revolution\t\t%d\r\n", length_cal_data_log.New_nos_pulses);
            fprintf(fp, "Time for 1 revolution\t\t%f\r\n", length_cal_data_log.New_time_duration);

            fprintf(fp, "Belt Length\t\t\t%11.1f\r\n", set_zero_data_log.belt_length);
            fprintf(fp, "Belt Length Unit\t\t\t%s\r\n", set_zero_data_log.belt_length_unit);

            fprintf(fp, "Belt Speed\t\t%f\r\n", Calculation_struct.Belt_speed);
            fprintf(fp, "Belt Speed Unit\t\t%s/min\r\n", Strings[Scale_setup_var.Speed_unit].Text);

            fprintf(fp, "Conveyor Angle(Degrees)\t\t\t%f\r\n", set_zero_data_log.Inst_conv_angle);

            fprintf(fp, "No of scale idlers\t\t\t%d\r\n",Scale_setup_var.Number_of_idlers);
            if(Scale_setup_var.Number_of_idlers>=1)
                fprintf(fp, "Idler_spacing1\t\t%f\r\n", calib_data_log.Idler_spacing1);
            else
                fprintf(fp, "Idler_spacing1\t\tNA\r\n");
            if(Scale_setup_var.Number_of_idlers>=1)
                fprintf(fp, "Idler_spacing2\t\t%f\r\n", calib_data_log.Idler_spacing2);
            else
                fprintf(fp, "Idler_spacing2\t\tNA\r\n");
            if(Scale_setup_var.Number_of_idlers>=2)
                fprintf(fp, "Idler_spacing3\t\t%f\r\n", calib_data_log.Idler_spacing3);
            else
                fprintf(fp, "Idler_spacing3\t\tNA\r\n");
            if(Scale_setup_var.Number_of_idlers>=3)
                fprintf(fp, "Idler_spacing4\t\t%f\r\n", calib_data_log.Idler_spacing2);
            else
                fprintf(fp, "Idler_spacing4\t\tNA\r\n");
            if(Scale_setup_var.Number_of_idlers>=4)
                fprintf(fp, "Idler_spacing5\t\t%f\r\n", calib_data_log.Idler_spacing3);
            else
                fprintf(fp, "Idler_spacing5\t\tNA\r\n");

            if(report_type == set_zero_test_report)
            {
                fprintf(fp, "Prev set zero value\t\t%f\r\n", set_zero_data_log.Prev_set_zero_val);
                fprintf(fp, "New set zero value\t\t%f\r\n", set_zero_data_log.New_set_zero_val);
                fprintf(fp, "Accumulated Weight\t%f\r\n", set_zero_data_log.Set_zero_accum_weight);
                fprintf(fp, "Accumulated weight unit %s\r\n\r\n", Strings[Scale_setup_var.Weight_unit].Text);
            }

            if(report_type == tst_wt_report)
            {
                fprintf(fp, "Total Test Weight \t\t%f\r\n", Calibration_var.Test_weight);
                fprintf(fp, "Total Test Weight Unit %s\r\n", Strings[Calibration_var.Test_zero_weight_unit].Text);
                fprintf(fp, "Previous Calibration TRIM factor\t%f\r\n", calib_data_log.Prev_span);
                fprintf(fp, "New Calibration TRIM factor\t\t%f\r\n", calib_data_log.New_span);
            }
            if(report_type == material_test_report)
            {
                fprintf(fp, "Certified Scale weight\t\t%f\r\n", calib_data_log.Cert_weight);
                fprintf(fp, "Certified Scale weight unit %s\r\n", Strings[Calibration_var.Cert_scale_unit].Text);
                fprintf(fp, "Belt Scale weight\t\t%f\r\n", Calibration_var.Belt_scale_weight);
                fprintf(fp, "Belt Scale weight unit %s\r\n", Strings[Calibration_var.Belt_scale_unit].Text);
                fprintf(fp, "Previous Calibration TRIM factor\t%f\r\n", calib_data_log.Prev_span);
                fprintf(fp, "New Calibration TRIM factor\t\t%f\r\n", calib_data_log.New_span);
                fprintf(fp, "TRIM Difference\t\t%f\r\n\r\n",Calibration_var.span_diff);
            }
            if(report_type == digital_test_report || report_type == calib_report)
            {
                fprintf(fp, "Previous Calibration TRIM factor\t%f\r\n", calib_data_log.Prev_span);
                fprintf(fp, "New Calibration TRIM factor\t\t%f\r\n", calib_data_log.New_span);
                fprintf(fp, "TRIM Difference\t\t%f\r\n\r\n",Calibration_var.span_diff);
            }

            if(ferror(fp))
            {
//					printf("\n Cal report file write error");
                return 0;
            }
        }
        break;

    /*if the report is error report*/
    case 2:
        fp = fopen ("error_report.txt", "a");
        if(fp == NULL)
        {
//				printf("\n Error in opening error report file");
            err = 1;
            return 0;
        }
        else
        {
            fprintf(fp, "\r\n\nData Type\t%s\r\n", error_rprt.Err_rprt_header.Data_type);
            fprintf(fp, "Report Type\t%s\r\n", error_rprt.Err_rprt_header.Report_type);
            fprintf(fp, "Date\t\t%d/%d/%d\r\n", error_rprt.Err_rprt_header.Date.Year,
                    error_rprt.Err_rprt_header.Date.Mon,
                    error_rprt.Err_rprt_header.Date.Day);
            fprintf(fp, "Time\t\t%d:%d:%d\r\n", error_rprt.Err_rprt_header.Time.Hr,
                    error_rprt.Err_rprt_header.Time.Min,
                    error_rprt.Err_rprt_header.Time.Sec);
            fprintf(fp, "Scale name\t%s\r\n",Admin_var.Scale_Name);
            fprintf(fp, "Plant name\t%s\r\n",Admin_var.Plant_Name);
            fprintf(fp, "Product name\t%s\r\n",Admin_var.Product_Name);
            fprintf(fp, "Firmware Version\t%s\r\n",Admin_var.Int_firmware_version);
            fprintf(fp, "Error code\t%s\r\n",error_data_log.Error_code);
            fprintf(fp, "Error Desc:\t%s\r\n",error_data_log.Error_desc);
            if(ferror(fp))
            {
//					printf("\n Error report file write error");
                return 0;
            }
        }
        break;

#ifdef SHOW_REPORTS
    /*if the report is daily report*/
    case 3:
        sprintf(&file_name[0],"%04d\\%02d\\%04d-%02d-%02d_daily_report.txt",daily_rprt.freq_rprt_header.Date.Year,daily_rprt.freq_rprt_header.Date.Mon,
                daily_rprt.freq_rprt_header.Date.Year,daily_rprt.freq_rprt_header.Date.Mon,daily_rprt.freq_rprt_header.Date.Day);
        fp = fopen (file_name, "w");
        if(fp == NULL)
        {
//				printf("\n Error in opening daily report file");
            err = 1;
            return 0;
        }
        else
        {
            fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", daily_rprt.freq_rprt_header.Data_type);
            fprintf(fp, "Report Type\t\t\t%s\r\n", daily_rprt.freq_rprt_header.Report_type);
            fprintf(fp, "Report Date\t\t\t\t%d/%d/%d\r\n", daily_rprt.freq_rprt_header.Date.Year,
                    daily_rprt.freq_rprt_header.Date.Mon,
                    daily_rprt.freq_rprt_header.Date.Day);
            fprintf(fp, "Power ON Time\t\t\t\t%02d:%02d:%02d\r\n", daily_rprt.freq_rprt_header.Time.Hr,
                    daily_rprt.freq_rprt_header.Time.Min,
                    daily_rprt.freq_rprt_header.Time.Sec);
            fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
            fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
            fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);
            fprintf(fp, "Firmware Version\t\t%s\r\n",Admin_var.Int_firmware_version);
            if(daily_rprt.Start_time.Hr == 24)
            {
                fprintf(fp, "Belt Start Time\t\t\tNo Start Detected\r\n");
            }
            else
            {
                fprintf(fp, "Belt Start Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.Start_time.Hr,
                        daily_rprt.Start_time.Min,
                        daily_rprt.Start_time.Sec);
            }
            if(daily_rprt.Start_load_time.Hr == 24)
            {
                fprintf(fp, "Load Start Time\t\t\tNo Start Load Detected\r\n");
            }
            else
            {
                fprintf(fp, "Load Start Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.Start_load_time.Hr,
                        daily_rprt.Start_load_time.Min,
                        daily_rprt.Start_load_time.Sec);
            }
            if(daily_rprt.End_load_time.Hr == 24)
            {
                fprintf(fp, "Load Stop Time\t\t\tNo End Load Detected\r\n");
            }
            else
            {
                fprintf(fp, "Load Stop Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.End_load_time.Hr,
                        daily_rprt.End_load_time.Min,
                        daily_rprt.End_load_time.Sec);
            }
            if(daily_rprt.End_time.Hr == 24)
            {
                fprintf(fp, "Belt Stop Time\t\t\tNo End Detected\r\n");
            }
            else
            {
                fprintf(fp, "Belt Stop Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.End_time.Hr,
                        daily_rprt.End_time.Min,
                        daily_rprt.End_time.Sec);
            }
            fprintf(fp, "Loaded Run Time(Hours)\t\t\t%4.3f\r\n",(float)(daily_rprt.Run_time/60.0));
            fprintf(fp, "Running Empty + Stopped Time(Hours)\t\t\t%4.3f\r\n",(float)(daily_rprt.Down_time/60.0));
            fprintf(fp, "Total Report Time(Hours)\t\t\t%4.3f\r\n",(float)(daily_rprt.Total_time/60.0));
            fprintf(fp, "Number of Belt Stops\t\t%d\r\n",daily_rprt.Cnt_zero_speed);
            fprintf(fp, "Report Period Average.Production Rate\t\t\t%11.2f\r\n",daily_rprt.Average_rate);
            fprintf(fp, "Rate Unit\t\t\t%s/%s\r\n", daily_rprt.Total_weight_unit,daily_rprt.Total_rate_unit);
            fprintf(fp, "Total Weight\t\t\t%11.2f\r\n",periodic_data.daily_weight);
            fprintf(fp, "Weight Unit\t\t\t%s\r\n", daily_rprt.Total_weight_unit);
            if(ferror(fp))
            {
//					printf("\n Daily report file write error");
                return 0;
            }
            //reset the values after writing them on the USB drive
            daily_rprt.Run_time = daily_rprt.Down_time = daily_rprt.Total_time = daily_rprt.Cnt_zero_speed = 0;
            daily_rprt.Start_time.Hr = daily_rprt.Start_time.Min = daily_rprt.Start_time.Sec = 0;
            daily_rprt.Start_load_time.Hr = daily_rprt.Start_load_time.Min = daily_rprt.Start_load_time.Sec = 0;
            daily_rprt.End_load_time.Hr = daily_rprt.End_load_time.Min = daily_rprt.End_load_time.Sec = 0;
            daily_rprt.End_time.Hr = daily_rprt.End_time.Min = daily_rprt.End_time.Sec = 0;
        }
        break;

    /*if the report is weekly report*/
    case 4:
        sprintf(&file_name[0],"%04d\\weekly_report.txt",weekly_rprt.freq_rprt_header.Date.Year);
        fp = fopen (file_name, "a");
        if(fp == NULL)
        {
//				printf("\nError in opening weekly report file");
            err = 1;
            return 0;
        }
        else
        {
            fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", weekly_rprt.freq_rprt_header.Data_type);
            fprintf(fp, "Report Type\t\t\t%s(Sun-Sat)\r\n", weekly_rprt.freq_rprt_header.Report_type);
            fprintf(fp, "Report Date\t\t\t\t%d/%d/%d\r\n", weekly_rprt.freq_rprt_header.Date.Year,
                    weekly_rprt.freq_rprt_header.Date.Mon,
                    weekly_rprt.freq_rprt_header.Date.Day);
            fprintf(fp, "Power ON Time\t\t\t\t%02d:%02d:%02d\r\n", weekly_rprt.freq_rprt_header.Time.Hr,
                    weekly_rprt.freq_rprt_header.Time.Min,
                    weekly_rprt.freq_rprt_header.Time.Sec);
            fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
            fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
            fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);
            fprintf(fp, "Firmware Version\t\t%s\r\n",Admin_var.Int_firmware_version);
            fprintf(fp, "Loaded Run Time(Hours)\t\t\t%4.3f\r\n",(float)(weekly_rprt.Run_time/60.0));
            fprintf(fp, "Running Empty + Stopped Time(Hours)\t\t\t%4.3f\r\n",(float)(weekly_rprt.Down_time/60.0));
            fprintf(fp, "Total Report Time(Hours)\t\t\t%4.3f\r\n",(float)(weekly_rprt.Total_time/60.0));
            fprintf(fp, "Number of belt stops\t\t%d\r\n",weekly_rprt.Cnt_zero_speed);
            fprintf(fp, "Report Period Avg.Production Rate\t\t\t%11.2f\r\n",weekly_rprt.Average_rate);
            fprintf(fp, "Rate Unit\t\t\t%s/%s\r\n", weekly_rprt.Total_weight_unit,weekly_rprt.Total_rate_unit);
            fprintf(fp, "Total Weight\t\t\t%11.2f\r\n",weekly_rprt.Total_weight);
            fprintf(fp, "Weight Unit\t\t\t%s\r\n", weekly_rprt.Total_weight_unit);
            if(ferror(fp))
            {
//					printf("\n Weekly report file write error");
                return 0;
            }
            //reset the values after writing them on the USB drive
            weekly_rprt.Run_time = weekly_rprt.Down_time = weekly_rprt.Total_time = weekly_rprt.Cnt_zero_speed = 0;
        }
        break;

    /*if the report is monthly report*/
    case 5:
        sprintf(&file_name[0],"%04d\\monthly_report.txt",monthly_rprt.freq_rprt_header.Date.Year);
        fp = fopen (file_name, "a");
        if(fp == NULL)
        {
//				printf("\nError in opening monthly report file");
            err = 1;
            return 0;
        }
        else
        {
            fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", monthly_rprt.freq_rprt_header.Data_type);
            fprintf(fp, "Report Type\t\t\t%s\r\n", monthly_rprt.freq_rprt_header.Report_type);
            fprintf(fp, "Report Date\t\t\t\t%d/%d/%d\r\n", monthly_rprt.freq_rprt_header.Date.Year,
                    monthly_rprt.freq_rprt_header.Date.Mon,
                    monthly_rprt.freq_rprt_header.Date.Day);
            fprintf(fp, "Power ON Time\t\t\t\t%02d:%02d:%02d\r\n", monthly_rprt.freq_rprt_header.Time.Hr,
                    monthly_rprt.freq_rprt_header.Time.Min,
                    monthly_rprt.freq_rprt_header.Time.Sec);
            fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
            fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
            fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);
            fprintf(fp, "Firmware Version\t\t%s\r\n",Admin_var.Int_firmware_version);
            fprintf(fp, "Loaded Run Time(Hours)\t\t\t%4.3f\r\n",(float)(monthly_rprt.Run_time/60.0));
            fprintf(fp, "Running Empty + Stopped Time(Hours)\t\t\t%4.3f\r\n",(float)(monthly_rprt.Down_time/60.0));
            fprintf(fp, "Total Report Time(Hours)\t\t\t%4.3f\r\n",(float)(monthly_rprt.Total_time/60.0));
            fprintf(fp, "Number of Belt stops\t\t%d\r\n",monthly_rprt.Cnt_zero_speed);
            fprintf(fp, "Report Period Average.Production Rate\t\t\t%11.2f\r\n",monthly_rprt.Average_rate);
            fprintf(fp, "Rate Unit\t\t\t%s/%s\r\n", monthly_rprt.Total_weight_unit,monthly_rprt.Total_rate_unit);
            fprintf(fp, "Total Weight\t\t\t%11.2f\r\n",monthly_rprt.Total_weight);
            fprintf(fp, "Weight Unit\t\t\t%s\r\n", monthly_rprt.Total_weight_unit);
            if(ferror(fp))
            {
//					printf("\n Monthly report file write error");
                return 0;
            }
            //reset the values after writing them on the USB drive
            monthly_rprt.Run_time = monthly_rprt.Down_time = monthly_rprt.Total_time = monthly_rprt.Cnt_zero_speed = 0;
        }
        break;
#endif

    default:
        break;
    }
    if(0 != fclose(fp))
    {
//		printf ("\nFile could not be closed!\n");
        return 0;
    }
    fflush (stdout);
    return (err);
}

/*****************************************************************************
* @note       Function name: U8 config_backup_write (char * stored_file_name)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : pointer to the variable where file name is to be stored
* @author			             : Anagha Basole
* @date       date created : 26/7/12
* @brief      Description	 : Writes the current report to the file
* @note       Notes		     : None
*****************************************************************************/
U8 config_backup_write (char * stored_file_name)
{
    FILE *fp;
    U8 err = 0;
    U8 i;
    char file_name[FILE_NAME_SIZE];

    fheader.File_hdr.Date.Day = (U8) Current_time.RTC_Mday;
    fheader.File_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
    fheader.File_hdr.Date.Year = (U16) Current_time.RTC_Year;
    //fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
    if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
        fheader.File_hdr.Time.Hr   = (U8) Current_time.RTC_Hour;
    else if(Admin_var.Current_Time_Format == TWELVE_HR)
    {
        if(Admin_var.AM_PM == AM_TIME_FORMAT)
        {
            if((U8) Current_time.RTC_Hour ==12)
                fheader.File_hdr.Time.Hr   = 0;
            else
                fheader.File_hdr.Time.Hr   = (U8) Current_time.RTC_Hour;
        }
        else
        {
            fheader.File_hdr.Time.Hr   = (U8) Current_time.RTC_Hour + 12;
        }
    }
    fheader.File_hdr.Time.Min = (U8) Current_time.RTC_Min;

    sprintf(&file_name[0], "Backups\\");
    for(i=0; Admin_var.Scale_Name[i] != '\0'; i++)
    {
        file_name[i+8] = Admin_var.Scale_Name[i];
    }
    i=i+8;
    sprintf(&file_name[i], "%d", fheader.File_hdr.Date.Year);
    i=i+4;
    sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Date.Mon);
    i+=2;

    sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Date.Day);
    i+=2;

    sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Time.Hr);
    i+=2;

    sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Time.Min);
    i+=2;

    strcat(&file_name[i], ".bat");

    strcpy(stored_file_name, file_name);
    //fp = fopen ("config_backup.bat", "w");
    fp = fopen (file_name, "wb");
    if(fp == NULL)
    {
//			printf("\n error in opening backup file");
        err = 1;
        return 0;
    }
    else
    {
        /*write the LCD structure to a file*/
        fwrite((U16 *)&Scale_setup_var, 1, sizeof(Scale_setup_var), fp);

        fwrite((U16 *)&Calibration_var, 1, sizeof(Calibration_var), fp);

        fwrite((U16 *)&Setup_device_var, 1, sizeof(Setup_device_var), fp);

        fwrite((U16 *)&Rprts_diag_var, 1, sizeof(Rprts_diag_var), fp);

        fwrite((U16 *)&Admin_var, 1, sizeof(Admin_var), fp);
        if(ferror(fp))
        {
//				printf("\n Backup file write error");
        }
    }
    if(0 != fclose(fp))
    {
//			printf ("\nBackup file could not be closed!\n");
        return 0;
    }

    fflush (stdout);
    return (err);
}

/*****************************************************************************
* @note       Function name: U8 config_backup_read (char filename[16])
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : file to be read back
* @author			             : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description	 : Reads and loads the current configuration from the file
* @note       Notes		     : None
*****************************************************************************/
U8 config_backup_read (char file_name[25])
{
    FILE *fp;
    U8 err = 0;
    char updated_file_name[FILE_NAME_SIZE];

    /*Megha updated file path on 24-Nov-2015*/
    sprintf(&updated_file_name[0], "Backups\\");
    strcat(&updated_file_name[8], file_name);
    fp = fopen (updated_file_name, "rb");
    if(fp == NULL)
    {
//		printf("\n error in opening file");
        err = 1;
    }
    else
    {
        /*read the LCD structure from the file*/
        fread((U16 *)&Scale_setup_var, 1, sizeof(Scale_setup_var), fp);

        fread((U16 *)&Calibration_var, 1, sizeof(Calibration_var), fp);

        fread((U16 *)&Setup_device_var, 1, sizeof(Setup_device_var), fp);

        fread((U16 *)&Rprts_diag_var, 1, sizeof(Rprts_diag_var), fp);

        fread((U16 *)&Admin_var, 1, sizeof(Admin_var), fp);
        eeprom_struct_backup();
        if(ferror(fp))
        {
//			printf("\n Backup file read error");
        }
        if(0 != fclose(fp))
        {
//				printf ("\nBackup file could not be closed!\n");
        }
    }

    fflush (stdout);
    return (err);
}

/*
void _WriteByte2File(U8 Data, void * p)
{
	int WR_Count = 0;
	int Wait_Count =5000;
	//WR_Count = fwrite(&Data, sizeof(Data), 1, p);
	WR_Count = fputc(Data, p);
	while(Wait_Count)
	{
		Wait_Count = Wait_Count -1;
	}


	 if(WR_Count != Data)
	 {
		WR_Count = WR_Count;
	 }
}
*/
#endif /*#ifdef FILE_SYS*/
/*****************************************************************************
* End of file
*****************************************************************************/
