/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Printer_low_level.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : September 20, 2012
* @date Last Modified  : September 20, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef __PRINTER_LOW_LEVEL_H
#define __PRINTER_LOW_LEVEL_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "RTOS_main.h"
/* ==========================================================================
 * Public Macro Definitions
 * ========================================================================== */
#ifdef PRINTER
#define  USART_XON                (0x11)
#define  USART_XOFF               (0x13)
#define USART_RX_BUFFER_SIZE      (8)
#define USART_TX_BUFFER_SIZE      (225)  //(8)  //Changed from 8 bytes to 225 bytes by DK on 17May2016

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
  typedef unsigned char bufferElement_t;
  extern char Set_XOFF_flag ;
/* unsigned integer variables section */
  typedef unsigned int bufferIndex_t;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
  typedef struct bufferFlags_tag
  {
    unsigned int empty        :1;
    unsigned int full         :1;
    unsigned int flowCtlState :2;
  } bufferFlags_t;

  typedef struct USARTBuffer_tag
  {
    bufferIndex_t  head;
    bufferIndex_t  tail;
    bufferIndex_t  size;
    bufferFlags_t  flags;
    bufferElement_t  *data;
  } USARTBuffer_t;

  typedef struct UARTRxFlowCtlParams_tag
  {
    bufferIndex_t  XONThreshold;
    bufferIndex_t  XOFFThreshold;
  } UARTRxFlowCtlParams_t;

  typedef struct BufferedUARTDesc_tag
  {
    USARTBuffer_t      RxBuffer;
    USARTBuffer_t      TxBuffer;
    UARTRxFlowCtlParams_t  RxFlowCtl;
  } BufferedUARTDesc_t;
  extern BufferedUARTDesc_t  BU0;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
void uart_buffer_init(uint8_t *rxBufferData, uint32_t rxBufferSize,
                                        uint8_t *txBufferData,
                                        uint32_t txBufferSize);

void uart_flow_control_thresholds_set(uint32_t XON, uint32_t XOFF);

int usart0_getch(void);
S16 usart0_putch(char ch);
BOOL usart0_chrdy(void);

//extern S16 buffered_putch(BufferedUARTDesc_t *BU, int c);	
//extern BOOL buffer_empty(USARTBuffer_t *buffer);	
extern bufferElement_t buffer_remove(USARTBuffer_t *buffer);

#endif /*#ifdef PRINTER*/
#endif /* __PRINTER_LOW_LEVEL_H */
/*****************************************************************************
* End of file
*****************************************************************************/
