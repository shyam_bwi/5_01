/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Printer_low_level.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : September 20, 2012
* @date Last Modified  : September 20, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/

#include "Printer_low_level.h"
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "Printer_high_level.h"
#include "lpc177x_8x_uart.h"
#include "Global_ex.h"
#include "Screen_global_ex.h"
#include "mbport.h"
#ifdef PRINTER
/* ==========================================================================
 * Private Macro Definitions
 * ========================================================================== */
#define  FLOW_CTL_XON_SENT        (0)
#define  FLOW_CTL_XON_INITIATED   (1u)
#define  FLOW_CTL_XOFF_INITIATED  (2u)
#define  FLOW_CTL_XOFF_SENT       (3u)

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
char Set_XOFF_flag = 0;

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

BufferedUARTDesc_t  BU0;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static S16 buffered_putch(BufferedUARTDesc_t *BU, int c);
static int buffered_getch(BufferedUARTDesc_t *BU);
static void handle_rx_xon(BufferedUARTDesc_t * const BU);
static void buffer_init(USARTBuffer_t *buffer, bufferElement_t *data, bufferIndex_t size);
static BOOL buffer_empty(USARTBuffer_t *buffer);
static BOOL buffer_full(USARTBuffer_t *buffer);
static void buffer_insert(USARTBuffer_t *buffer, bufferElement_t element);
static bufferIndex_t buffer_length(const USARTBuffer_t *buffer);
static void handle_rx_xoff_in_isr(BufferedUARTDesc_t * const BU);

/*============================================================================
* Function Implementation Section
*===========================================================================*/
/* ==========================================================================
* Private Global Variable Definitions
* ========================================================================== */

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*****************************************************************************
* @note       Function name: void _PRINTER_IRQHander(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Printer interrupt service routine for receiving data,
                             transmitting data and also auto baud setting
* @note       Notes        : None
*****************************************************************************/
void _PRINTER_IRQHander(void)
{
  char c;
  // Call Standard UART 3 interrupt handler
  uint32_t intsrc, tmp;

  /* Determine the interrupt source */
  intsrc = UART_GetIntId(_PRINTER_UART);
  tmp = intsrc & UART_IIR_INTID_MASK;

  /*******************If USART has a received char ready:*****************/
  if ((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI))
  {
    /* Read a character from the UART.*/
    c = UART_ReceiveByte(_PRINTER_UART);


    if (c == USART_XOFF)
    {
      if(Printer_struct.flow_control == 2)
      {
        UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, DISABLE);
        Printer_struct.xoff_rcvd = 1;
      }
    }
    else if (c == USART_XON)
    {
      if(Printer_struct.flow_control == 2)
      {
        UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, ENABLE);
        Printer_struct.xoff_rcvd = 0;
      }
    }
		else if((c =='c') || (c =='d') || (c =='k'))
		{
			  //buffer_insert(&BU0.RxBuffer, c);
				send_debug = 1;
				send_debug_char = c;
				/*sprintf (Printer_struct.Printer_data, "%s,%s,%s,%8.3f,%6.3f,%d:%d:%d,%d/%d/%d \r\n",
							Admin_var.Scale_Name,
							Admin_var.Plant_Name,
							Admin_var.Product_Name,
							Calculation_struct.Total_weight,
							Calculation_struct.Rate_for_display,
							Current_time.RTC_Hour, Current_time.RTC_Min, Current_time.RTC_Sec,
							Current_time.RTC_Mon, Current_time.RTC_Mday, Current_time.RTC_Year
							);		*/	
          /*while (i <= 225)
          {
						vMBPEnterCritical();
              UART_SendByte(_PRINTER_UART, (uint8_t)Printer_struct.Printer_data[i]);
						vMBPExitCritical();
							i++;
					}*/
		}
    else
    {
      /* If buffer has more space:*/
      if (!buffer_full(&BU0.RxBuffer))
      {
        /* Insert the character into receive buffer*/
        buffer_insert(&BU0.RxBuffer, c);
      }

      if ((FLOW_CTL_XOFF_SENT != BU0.RxBuffer.flags.flowCtlState) && (Printer_struct.flow_control == 2))
      {
        /* Check whether Rx buffer is getting full.*/
        handle_rx_xoff_in_isr(&BU0);
      }
    }
  }
  /*****************If USART transmitter is empty:********************/
  else if (tmp == UART_IIR_INTID_THRE)
  {
    /* If XOFF state happened and XOFF has not been sent:*/
    if ((FLOW_CTL_XOFF_INITIATED == BU0.RxBuffer.flags.flowCtlState) && (Printer_struct.flow_control == 2))
    {
      /* Send XOFF character.*/
      UART_SendByte (_PRINTER_UART, (uint8_t)USART_XOFF);

      /* Change state so XOFF is not sent again.*/
      BU0.RxBuffer.flags.flowCtlState = FLOW_CTL_XOFF_SENT;
      Set_XOFF_flag = 1;
    }
    /* If Tx buffer is not empty:*/
    else if (!buffer_empty(&BU0.TxBuffer))
    {
      /* Remove a character from the transmit buffer.*/
      c = buffer_remove(&BU0.TxBuffer);
      /* Send byte from Tx buffer to UART.*/
      //LPC_UART2->THR = (uint8_t)c & UART_THR_MASKBIT;
      UART_SendByte(_PRINTER_UART, (uint8_t)c);
    }
    /* If Tx buffer is empty:*/
    else
    {
      /*Disable USART transmit interrupt.*/
      UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, DISABLE);
    }
  }
}


/*****************************************************************************
* @note       Function name: void uart_buffer_init(uint8_t *rxBufferData,
*                                                  uint32_t rxBufferSize,
*                                                  uint8_t *txBufferData,
*                                                  uint32_t txBufferSize)
* @returns    returns      : None
* @param      arg1         : rxBufferData[in]  pointer to an array to use as an RxBuffer
* @param      arg2         : rxBufferSize[in]  size of array to use as RxBuffer
* @param      arg3         : txBufferData[in]  pointer to an array to use as a TxBuffer
* @param      arg4         : txBufferSize[in]  size of array to use as TxBuffer
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Initialize the UART data buffers and receive
                             threshold value
* @note       Notes        : None
*****************************************************************************/
void uart_buffer_init(uint8_t *rxBufferData,
                        uint32_t rxBufferSize,
                        uint8_t *txBufferData,
                        uint32_t txBufferSize)
{
  bufferIndex_t threshold;

  threshold = rxBufferSize / 4u;
  if (threshold == 0)
  {
    threshold = 1u;
  }
  buffer_init(&BU0.RxBuffer, rxBufferData, rxBufferSize);
  buffer_init(&BU0.TxBuffer, txBufferData, txBufferSize);
  BU0.RxFlowCtl.XONThreshold = threshold;
  BU0.RxFlowCtl.XOFFThreshold = rxBufferSize - threshold;
}

/*****************************************************************************
* @note       Function name: void uart_flow_control_thresholds_set(uint32_t XON,
*                                                                  uint32_t XOFF)
* @returns    returns      : None
* @param      arg1         : Xon - threshold value
* @param      arg2         : XOFF -  Xoff thershold value
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Set the Xon and Xoff thresholds
* @note       Notes        : None
*****************************************************************************/
void uart_flow_control_thresholds_set(uint32_t XON, uint32_t XOFF)
{

  if (XON)
  {
    BU0.RxFlowCtl.XONThreshold = XON;
  }
  if (XOFF)
  {
    BU0.RxFlowCtl.XOFFThreshold = XOFF;
  }
}

/*****************************************************************************
* @note       Function name: int usart0_getch(void)
* @returns    returns      : Returns the received character
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Retrieve the character from the buffer
* @note       Notes        : None
*****************************************************************************/
int usart0_getch(void)
{
  int c;

  c = buffered_getch(&BU0);

  if (FLOW_CTL_XON_SENT != BU0.RxBuffer.flags.flowCtlState)
  {
    handle_rx_xon(&BU0);
  }

  return c;
}

/*****************************************************************************
* @note       Function name: S16 usart0_putch(char ch)
* @returns    returns      : retval - status of the insertion of character in the
*                             buffer
* @param      arg1         : ch - the character to be sent through the UART port
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Stores the character to be sent in the buffer and
                             enables the transmit interrupt if buffer is empty
* @note       Notes        : None
*****************************************************************************/
S16 usart0_putch(char ch)
{
  S16 retval;
  BOOL turnOnTxEmpty;

  /*Determine whether the Tx buffer is empty before inserting the character.*/
  turnOnTxEmpty = buffer_empty(&BU0.TxBuffer);

  /*Put the character into the transmit buffer for USART0.*/
  retval = buffered_putch(&BU0, ch);

  /*If the Tx buffer is empty:*/
  if (turnOnTxEmpty)
  {
    /*Enable the USART tx empty interrupt.*/
    UART_TxCmd(_PRINTER_UART, ENABLE);
    UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, ENABLE);
  }

  return retval;
}

/* ==========================================================================
 * Private Function Definitions
 * ========================================================================== */
/*****************************************************************************
* @note       Function name: static S16 buffered_putch(BufferedUARTDesc_t *BU, int c)
* @returns    returns      : -1 - if the buffer is full
                           : c - the stored character is returned
* @param      arg1         : buffer[in]  pointer to buffer header
*             arg2         : c - character to be stored
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Puts the data in the buffer
* @note       Notes        : None
*****************************************************************************/
static  S16 buffered_putch(BufferedUARTDesc_t *BU, int c)
{
  /* Handle the buffer is full:*/
  if (buffer_full(&BU->TxBuffer))
  {
    return -1;
  }
  /* Handle the buffer has room:*/
  else
  {
    /* Insert character into buffer.*/
    buffer_insert(&BU->TxBuffer, (bufferElement_t)c);
    return c;
  }
}

/*****************************************************************************
* @note       Function name: static int buffered_getch(BufferedUARTDesc_t *BU)
* @returns    returns      : -1 - if the buffer is empty
                           : c - the character read is returned
* @param      arg1         : buffer[in]  pointer to buffer header
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Character is retrieved from the buffer
* @note       Notes        : None
*****************************************************************************/
static int buffered_getch(BufferedUARTDesc_t *BU)
{
  int c;

  /* Handle the buffer is empty:*/
  if (buffer_empty(&BU->RxBuffer))
  {
    c = -1;
  }
  /* Handle the buffer has some data:*/
  else
  {
    /* Remove character from buffer.*/
    c = (int)buffer_remove(&BU->RxBuffer);
  }
  return c;
}

/*****************************************************************************
* @note       Function name: static void handle_rx_xon(BufferedUARTDesc_t * const BU)
* @returns    returns      : None
* @param      arg1         : buffer[in]  pointer to buffer header
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Checks if flow control thershold was reached and
                             sends Xon to allow incoming data
* @note       Notes        : None
*****************************************************************************/
static void handle_rx_xon(BufferedUARTDesc_t * const BU)
{
  /* Check whether flow control threshold was reached.*/
  if (buffer_length(&BU->RxBuffer) <= BU->RxFlowCtl.XONThreshold)
  {
    /* Turn off flow control "brake" to allow incoming data.*/
    BU->RxBuffer.flags.flowCtlState = FLOW_CTL_XON_INITIATED;
    /* Send XON.*/
    if (buffered_putch(BU, USART_XON) >= 0)
    {
      /* Set state to XON to indicate XON has been sent.*/
      BU->RxBuffer.flags.flowCtlState = FLOW_CTL_XON_SENT;
    }

    /* Turn on UART Tx Empty interrupt in case it is off.*/
    UART_TxCmd(_PRINTER_UART, ENABLE);
    UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, ENABLE);
  }
}

/*****************************************************************************
* @note       Function name: static void buffer_init(USARTBuffer_t *buffer,
*                                                    bufferElement_t *data,
*                                                      bufferIndex_t size)
* @returns    returns      : None
* @param      arg1         : buffer[in]  pointer to buffer header to init
* @param      arg2         : data[in] pointer to array of buffer elements.
* @param      arg3         : size[in] size of array of buffer elements.
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Initialize USART buffer and buffer header.
* @note       Notes        : None
*****************************************************************************/
static void buffer_init(USARTBuffer_t *buffer,
            bufferElement_t *data,
            bufferIndex_t size)
{
  buffer->flags.empty = __TRUE;
  buffer->flags.full = __FALSE;
  buffer->head = 0;
  buffer->tail = 0;
  buffer->size = size;
  buffer->data = data;
}

/*****************************************************************************
* @note       Function name: static BOOL buffer_empty(USARTBuffer_t *buffer)
* @returns    returns      : Returns buffer status, full/empty
* @param      arg1         : buffer[in]  pointer to buffer header
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Checks if buffer is empty
* @note       Notes        : None
*****************************************************************************/
/*static*/ BOOL buffer_empty(USARTBuffer_t *buffer)
{
  return buffer->flags.empty;
}

/*****************************************************************************
* @note       Function name: static BOOL buffer_full(USARTBuffer_t *buffer)
* @returns    returns      : Returns buffer status, full/empty
* @param      arg1         : buffer[in]  pointer to buffer header
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : The status of the buffer is checked if full
* @note       Notes        : None
*****************************************************************************/
static BOOL buffer_full(USARTBuffer_t *buffer)
{
  return buffer->flags.full;
}

/*****************************************************************************
* @note       Function name: static void buffer_insert(USARTBuffer_t *buffer,
*                                                    bufferElement_t element)
* @returns    returns      : None
* @param      arg1         : buffer[in]  pointer to buffer header
* @param      arg2         : data[in]    data to insert into the buffer
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Insert an element into the buffer at its head
* @note       Notes        : None
*****************************************************************************/
static void buffer_insert(USARTBuffer_t *buffer,
              bufferElement_t element)
{

  /* Insert an element at the head of the buffer.*/
  buffer->data[buffer->head] = element;
  /* If element was inserted at the end of the buffer:*/
  if (++buffer->head >= buffer->size)
  {
    /* Roll the buffer head index back to the beginning.*/
    buffer->head = 0;
  }
  /* We know the buffer is not empty.*/
  buffer->flags.empty = __FALSE;

  /* If the buffer head index now matches the tail index:*/
  if (buffer->head == buffer->tail)
  {
    /* Then the buffer has become full.*/
    buffer->flags.full = __TRUE;
  }

}

/*****************************************************************************
* @note       Function name: bufferElement_t buffer_remove(USARTBuffer_t *buffer)
* @returns    returns      : oldest data element in the buffer
* @param      arg1         : buffer[in]  pointer to buffer header to init
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Remove and return an element from the buffer at its tail
* @note       Notes        : None
*****************************************************************************/
static bufferElement_t buffer_remove(USARTBuffer_t *buffer)
{
  bufferElement_t element;

  /* Retrieve an element at the tail of the buffer.*/
  element = buffer->data[buffer->tail];
  /* If element was located at the end of the buffer:*/
  if (++buffer->tail >= buffer->size)
  {
    /* Roll the buffer tail index back to the beginning.*/
    buffer->tail = 0;
  }

  /* If the buffer head index now matches the tail index:*/
  if (buffer->tail == buffer->head)
  {
    /* The buffer has become empty.*/
    buffer->flags.empty = __TRUE;
  }

  /* We know the buffer is not full.*/
  buffer->flags.full = __FALSE;

  return element;
}

/*****************************************************************************
* @note       Function name: static bufferIndex_t buffer_length(const USARTBuffer_t *buffer)
* @returns    returns      : the buffer length
* @param      arg1         : buffer[in]  pointer to buffer header to init
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Calculates the current buffer length according
                             to the data present in the buffer
* @note       Notes        : None
*****************************************************************************/
static bufferIndex_t buffer_length(const USARTBuffer_t *buffer)
{

  bufferIndex_t length;

  if (buffer->flags.empty)
  {
    length = 0;
  }
  else if (buffer->flags.full)
  {
    length = buffer->size;
  }
  else
  {
    if (buffer->head > buffer->tail)
    {
      length = buffer->head - buffer->tail;
    }
    else
    {
      length = ( buffer->size + buffer->head ) - buffer->tail;
    }
  }

  return length;
}

/*****************************************************************************
* @note       Function name: static void handle_rx_xoff_in_isr(BufferedUARTDesc_t * const BU)
* @returns    returns      : None
* @param      arg1         : buffer[in]  pointer to buffer header
* @author                  : Anagha Basole
* @date       date created : 20th September 2012
* @brief      Description  : Send Xoff to stop incoming data if threshold has
                             been reached.
* @note       Notes        : None
*****************************************************************************/
__attribute__((always_inline))
static void handle_rx_xoff_in_isr(BufferedUARTDesc_t * const BU)
{
  /* Check whether flow control threshold was reached.*/
  if (buffer_length(&BU->RxBuffer) >= BU->RxFlowCtl.XOFFThreshold)
  {
    /* Engage flow control "brake" to stop incoming data.*/
    BU->RxBuffer.flags.flowCtlState = FLOW_CTL_XOFF_INITIATED;

    /* Make sure Tx Empty interrupt is on so XOFF can be sent.*/
    UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, ENABLE);
  }
}

#endif /*#ifdef PRINTER*/
