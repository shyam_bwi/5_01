/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Printer_high_level.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : September 11, 2012
* @date Last Modified  : September 11, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __PRINTER_HIGH_LEVEL_H
#define __PRINTER_HIGH_LEVEL_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_pinsel.h"
#include "Printer_low_level.h"

#ifdef PRINTER
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define _PRINTER_UART          (LPC_UART_TypeDef *) LPC_UART1
#define _PRINTER_IRQ          UART1_IRQn
#define _PRINTER_IRQHander    UART1_IRQHandler
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
{
  U8 flow_control;
  U8 xoff_rcvd;
  U8 Scale_num;
  U32 Baud_rate;
  U32 Databits;
  U32 Stopbits;
  char Printer_data[USART_TX_BUFFER_SIZE]; //Replaced from 225 bytes to USART_TX_BUFFER_SIZE by DK on 17May2016
  unsigned char Tx0BufferData[USART_TX_BUFFER_SIZE];
  unsigned char Rx0BufferData[USART_RX_BUFFER_SIZE];
}PRINTER_PARAM;

extern UART_CFG_Type PRINTER_ConfigStruct;
extern PRINTER_PARAM Printer_struct;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void printer_init(void);
extern __task void printer_send_task (void);

#endif /*#ifdef PRINTER*/

#endif /*__PRINTER_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
