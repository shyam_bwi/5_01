/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Printer_high_level.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : September 11, 2012
* @date Last Modified  : September 11, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "Printer_high_level.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "RTOS_main.h"
#include "Sensor_board_data_process.h"
#include "math.h"
#include "ctype.h"

#ifdef PRINTER
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/
char send_debug;
char send_debug_char;

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
// UART Configuration structure variable
UART_CFG_Type PRINTER_ConfigStruct;
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
PRINTER_PARAM Printer_struct;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void printer_data_sprintf(void);
void check_string(char * string, U8 index);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: void printer_init(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 11th September 2012
* @brief      Description  : Printer initialization with auto baud rate
                             configuration
* @note       Notes        : None
*****************************************************************************/
void printer_init(void)
{
    // UART FIFO configuration Struct variable
    UART_FIFO_CFG_Type UARTFIFOConfigStruct;

    /*
     * Initialize UART1 pin connect
     * P0.15: U1_TXD
     * P0.16: U1_RXD
     * P0.17: U1_CTS
     * P3.20: U1_DSR
     * P3.19: U1_DCD
     */
    PINSEL_ConfigPin(0,15,1);
    PINSEL_ConfigPin(0,16,1);
    PINSEL_ConfigPin(0,17,1);
    PINSEL_ConfigPin(3,20,3);
    PINSEL_ConfigPin(3,19,3);

    /* Initialize UART Configuration parameter structure to default state
       if the the printer is the default beltway printer:
     * Baudrate = 9600bps
     * 8 data bit
     * 1 Stop bits
     * None parity
     */
    if (Setup_device_var.Printer_type == Screen4121_str1)
    {
        PRINTER_ConfigStruct.Baud_rate = 9600;
        set_uart_param(Setup_device_var.Printer_data_bits, set_data_bits, Printer);
        set_uart_param(Setup_device_var.Printer_stop_bits, set_stop_bits, Printer);
        PRINTER_ConfigStruct.Parity = UART_PARITY_NONE;
        Printer_struct.Baud_rate = Setup_device_var.Printer_baud;
        Printer_struct.Databits = Setup_device_var.Printer_data_bits;
        Printer_struct.Stopbits = Setup_device_var.Printer_stop_bits;
    }
    else
    {
        set_uart_param(Setup_device_var.Printer_baud, set_baud_rate, Printer);
        set_uart_param(Setup_device_var.Printer_data_bits, set_data_bits, Printer);
        set_uart_param(Setup_device_var.Printer_stop_bits, set_stop_bits, Printer);
        //set_uart_param(0, set_flow_control, Printer);
        Printer_struct.Baud_rate = Setup_device_var.Printer_baud;
        Printer_struct.Databits = Setup_device_var.Printer_data_bits;
        Printer_struct.Stopbits = Setup_device_var.Printer_stop_bits;
        if (Setup_device_var.Printer_flow_control == Screen412214_str2)
        {
            Printer_struct.flow_control = 0;//no flow control
            Printer_struct.xoff_rcvd = 0;
        }
        else if ( Setup_device_var.Printer_flow_control == Screen412214_str4)
        {
            Printer_struct.flow_control = 1;//hardware flow control
            /* Determine current state of CTS pin to enable Tx activity  */
            if (UART_FullModemGetStatus(LPC_UART1) & UART1_MODEM_STAT_CTS)
            {
                // Enable UART Transmit
                UART_TxCmd(_PRINTER_UART, ENABLE);
            }
            UART_FullModemConfigMode(LPC_UART1, UART1_MODEM_MODE_AUTO_CTS, ENABLE);
        }
        else if ( Setup_device_var.Printer_flow_control == Screen412214_str7)
        {
            Printer_struct.flow_control = 2; //xon/xoff flow control
        }
    }

    /* Initialize UART3 peripheral with given to corresponding parameter
     * in this case, don't care the baudrate value UART initialized
     * since this will be determine when running auto baudrate
     */
    UART_Init(_PRINTER_UART, &PRINTER_ConfigStruct);


    /* Initialize FIFOConfigStruct to default state:
     *         - FIFO_DMAMode = DISABLE
     *         - FIFO_Level = UART_FIFO_TRGLEV0
     *         - FIFO_ResetRxBuf = ENABLE
     *         - FIFO_ResetTxBuf = ENABLE
     *         - FIFO_State = ENABLE
     */
    UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

    // Initialize FIFO for UART0 peripheral
    UART_FIFOConfig(_PRINTER_UART, &UARTFIFOConfigStruct);


    // Enable UART Transmit
    UART_TxCmd(_PRINTER_UART, ENABLE);

    /* Enable UART Rx interrupt */
    UART_IntConfig(_PRINTER_UART, UART_INTCFG_RBR, ENABLE);
    /* Enable UART line status interrupt */
    //UART_IntConfig(_PRINTER_UART, UART_INTCFG_RLS, ENABLE);
    /*
     * Do not enable transmit interrupt here, since it is handled by
     * UART_Send() function, just to reset Tx Interrupt state for the
     * first time
     */
    UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, ENABLE);

    /* preemption = 1, sub-priority = 1 */
    NVIC_SetPriority(_PRINTER_IRQ, 0x21);

    /* Enable Interrupt for UART0 channel */
    NVIC_EnableIRQ(_PRINTER_IRQ);

    return;
}

/*****************************************************************************
* @note       Function name: __task void printer_data (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 11th September 2012
* @brief      Description  : Send data to the printer. Reinitialize the port
*                          : if the printer settings have changed
* @note       Notes        : None
*****************************************************************************/
extern int task_number;
__task void printer_send_task (void)
{
    uint8_t reinit = 0;
    uint16_t i;
    double dbl_disp_value = 0.0;		//Suvrat
    float disp_value = 0.0;
    int disp_unit = 0;
    char buf[4] = {0};
    int nullfound =0;
    printer_init();
    uart_buffer_init(Printer_struct.Rx0BufferData, USART_RX_BUFFER_SIZE, Printer_struct.Tx0BufferData, USART_TX_BUFFER_SIZE);
    if (Printer_struct.flow_control == 2)
    {
        uart_flow_control_thresholds_set(0,(USART_RX_BUFFER_SIZE - 2u));
    }
    else
    {
        uart_flow_control_thresholds_set(0,0);
    }
    Printer_struct.Scale_num = 1;
    Printer_struct.xoff_rcvd = 0;

    while(1)
    {

        os_evt_wait_or(PRINTER_EVENT_FLAG, 0xFFFF);
				LPC_RTC->ALMIN&= ~(0x03<<0);
        LPC_RTC->ALMIN|= (0x01<<0);				//SECTION1
        //if settings have changed during runtime then re-initialize the printer port
        if ((Printer_struct.Baud_rate != Setup_device_var.Printer_baud) ||
                (Printer_struct.Databits != Setup_device_var.Printer_data_bits) ||
                (Printer_struct.Stopbits != Setup_device_var.Printer_stop_bits))
        {
            printer_init();
        }

        if (Setup_device_var.Printer_flow_control == Screen412214_str2)
        {
            Printer_struct.flow_control = 0;//no flow control
            Printer_struct.xoff_rcvd = 0;
            reinit = 0;
        }
        else if ( Setup_device_var.Printer_flow_control == Screen412214_str4)
        {
            Printer_struct.flow_control = 1;//hardware flow control
            if (reinit == 0)
            {
                /* Determine current state of CTS pin to enable Tx activity  */
                if (UART_FullModemGetStatus(LPC_UART1) & UART1_MODEM_STAT_CTS)
                {
                    // Enable UART Transmit
                    UART_TxCmd(_PRINTER_UART, ENABLE);
                }
                UART_FullModemConfigMode(LPC_UART1, UART1_MODEM_MODE_AUTO_CTS, ENABLE);
                reinit = 1;
            }
        }
        else if ( Setup_device_var.Printer_flow_control == Screen412214_str7)
        {
            Printer_struct.flow_control = 2; //xon/xoff flow control
            reinit = 0;
        }

        uart_buffer_init(Printer_struct.Rx0BufferData, USART_RX_BUFFER_SIZE, Printer_struct.Tx0BufferData, USART_TX_BUFFER_SIZE);
        if (Printer_struct.flow_control == 2)
        {
            uart_flow_control_thresholds_set(0,(USART_RX_BUFFER_SIZE - 2u));
        }
        else
        {
            uart_flow_control_thresholds_set(0,0);
        }

#ifdef CALCULATION
        LPC_RTC->ALMIN&= ~(0x03<<0);
        LPC_RTC->ALMIN|= (0x02<<0);						//SECTION 2
        if(Printer_struct.flow_control != 1) //hardware flow control disabled
        {
            if (Printer_struct.xoff_rcvd == 0)
            {
                if((send_debug == 1) && (send_debug_char == 'c'))
                {
                    memset(Printer_struct.Printer_data,' ',sizeof(Printer_struct.Printer_data));

                    check_string(Admin_var.Scale_Name, 0);
                    Printer_struct.Printer_data[3] = ',';
                    check_string(Admin_var.Plant_Name, 4);
                    Printer_struct.Printer_data[7] = ',';
                    check_string(Admin_var.Product_Name, 8);
                    Printer_struct.Printer_data[11] = ',';

                    if(Calculation_struct.Total_weight < 0)
                    {
                        sprintf (&Printer_struct.Printer_data[12], "-        ");
                    }
                    else
                        sprintf (&Printer_struct.Printer_data[12], "         ");


                    //disp_value = fabsf(Calculation_struct.Total_weight);
                    dbl_disp_value = fabs(Calculation_struct.Total_weight);
                    if(dbl_disp_value < 10)
                        sprintf (&Printer_struct.Printer_data[21], "%1.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100)
                        sprintf (&Printer_struct.Printer_data[20], "%2.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000)
                        sprintf (&Printer_struct.Printer_data[19], "%3.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 10000)
                        sprintf (&Printer_struct.Printer_data[18], "%4.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100000)
                        sprintf (&Printer_struct.Printer_data[17], "%5.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000000)
                        sprintf (&Printer_struct.Printer_data[16], "%6.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 10000000)
                        sprintf (&Printer_struct.Printer_data[15], "%7.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100000000)
                        sprintf (&Printer_struct.Printer_data[14], "%8.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000000000)
                        sprintf (&Printer_struct.Printer_data[13], "%9.3lf\r\n",dbl_disp_value);
                    else
                        sprintf (&Printer_struct.Printer_data[13], "%9.3lf\r\n",999999999.999);
                    Printer_struct.Printer_data[26] = ',';


                    if(Calculation_struct.Rate_for_display<0)
                    {
                        sprintf (&Printer_struct.Printer_data[27], "-");
                    }
                    else
                        sprintf (&Printer_struct.Printer_data[27], " ");
                    Printer_struct.Printer_data[28] = ' ';

                    disp_value = fabsf(Calculation_struct.Rate_for_display);
                    if(disp_value < 10)
                        sprintf (&Printer_struct.Printer_data[33], "%1.3f\r\n",disp_value);
                    else if(disp_value < 100)
                        sprintf (&Printer_struct.Printer_data[32], "%2.3f\r\n",disp_value);
                    else if(disp_value < 1000)
                        sprintf (&Printer_struct.Printer_data[31], "%3.3f\r\n",disp_value);
                    else if(disp_value < 10000)
                        sprintf (&Printer_struct.Printer_data[30], "%4.3f\r\n",disp_value);
                    else if(disp_value < 100000)
                        sprintf (&Printer_struct.Printer_data[29], "%5.3f\r\n",disp_value);
                    else if(disp_value < 1000000)
                        sprintf (&Printer_struct.Printer_data[28], "%6.3f\r\n",disp_value);
                    else
                        sprintf (&Printer_struct.Printer_data[28], "%6.3f\r\n",999999.999);
                    Printer_struct.Printer_data[38] = ',';

                    sprintf (&Printer_struct.Printer_data[39], "%02d:%02d:%02d,%02d/%02d/\r\n",
                             Current_time.RTC_Hour, Current_time.RTC_Min, Current_time.RTC_Sec,
                             Current_time.RTC_Mon, Current_time.RTC_Mday);
                    sprintf(&buf[0], "%d", Current_time.RTC_Year);
                    Printer_struct.Printer_data[54] = buf[2];
                    Printer_struct.Printer_data[55] = buf[3];
                    Printer_struct.Printer_data[56] = '\r';
                    Printer_struct.Printer_data[57] = '\n';
                    Printer_struct.Printer_data[58] = '\0';
                    send_debug = 0;
                }
                else if((send_debug == 1) && (send_debug_char == 'd'))
                {
                    memset(Printer_struct.Printer_data,' ',sizeof(Printer_struct.Printer_data));

                    if(Calculation_struct.Total_weight < 0)
                    {
                        memcpy(&Printer_struct.Printer_data[0],"\r\nWEIGHT-",9);
                    }
                    else
                        memcpy(&Printer_struct.Printer_data[0], "\r\nWEIGHT ",9);

                    //disp_value = fabsf(Calculation_struct.Total_weight);
                    dbl_disp_value = fabs(Calculation_struct.Total_weight);
                    if(dbl_disp_value < 10)
                        sprintf (&Printer_struct.Printer_data[17], "%1.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100)
                        sprintf (&Printer_struct.Printer_data[16], "%2.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000)
                        sprintf (&Printer_struct.Printer_data[15], "%3.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 10000)
                        sprintf (&Printer_struct.Printer_data[14], "%4.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100000)
                        sprintf (&Printer_struct.Printer_data[13], "%5.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000000)
                        sprintf (&Printer_struct.Printer_data[12], "%6.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 10000000)
                        sprintf (&Printer_struct.Printer_data[11], "%7.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100000000)
                        sprintf (&Printer_struct.Printer_data[10], "%8.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000000000)
                        sprintf (&Printer_struct.Printer_data[9], "%9.3lf\r\n",dbl_disp_value);
                    else
                        sprintf (&Printer_struct.Printer_data[9], "%9.3lf\r\n",999999999.999);

                    disp_unit = toupper(Strings[Scale_setup_var.Weight_unit].Text[0]);
                    if(Calculation_struct.Rate_for_display<0)
                    {
                        sprintf (&Printer_struct.Printer_data[22], "\r\nRATE %cP%c- ",disp_unit,Strings[Scale_setup_var.Rate_time_unit].Text[0]);
                    }
                    else
                        sprintf (&Printer_struct.Printer_data[22], "\r\nRATE %cP%c  ",disp_unit,Strings[Scale_setup_var.Rate_time_unit].Text[0]);
                    Printer_struct.Printer_data[34] = ' ';

                    disp_value = fabsf(Calculation_struct.Rate_for_display);
                    if(disp_value < 10)
                        sprintf (&Printer_struct.Printer_data[39], "%1.3f\r\n",disp_value);
                    else if(disp_value < 100)
                        sprintf (&Printer_struct.Printer_data[38], "%2.3f\r\n",disp_value);
                    else if(disp_value < 1000)
                        sprintf (&Printer_struct.Printer_data[37], "%3.3f\r\n",disp_value);
                    else if(disp_value < 10000)
                        sprintf (&Printer_struct.Printer_data[36], "%4.3f\r\n",disp_value);
                    else if(disp_value < 100000)
                        sprintf (&Printer_struct.Printer_data[35], "%5.3f\r\n",disp_value);
                    else if(disp_value < 1000000)
                        sprintf (&Printer_struct.Printer_data[34], "%6.3f\r\n",disp_value);
                    else
                        sprintf (&Printer_struct.Printer_data[34], "%6.3f\r\n",999999.999);

                    sprintf (&Printer_struct.Printer_data[44], "\r\nSPEED       ");
                    Printer_struct.Printer_data[58] = ' ';
                    if(Calculation_struct.Belt_speed < 10)
                        sprintf (&Printer_struct.Printer_data[61], "%1.3f\r\n",Calculation_struct.Belt_speed);
                    else if(Calculation_struct.Belt_speed < 100)
                        sprintf (&Printer_struct.Printer_data[60], "%2.3f\r\n",Calculation_struct.Belt_speed);
                    else if(Calculation_struct.Belt_speed < 1000)
                        sprintf (&Printer_struct.Printer_data[59], "%3.3f\r\n",Calculation_struct.Belt_speed);
                    else if(Calculation_struct.Belt_speed < 10000)
                        sprintf (&Printer_struct.Printer_data[58], "%4.3f\r\n",Calculation_struct.Belt_speed);

                    memcpy(&Printer_struct.Printer_data[66],"\r\nNODE ",7);
                    //memcpy(&Printer_struct.Printer_data[71],Admin_var.Scale_Name,3);
                    nullfound=0;
                    for(i=73; i<76; i++)
                    {
                        if((Admin_var.Scale_Name[i-73]=='\0')||(nullfound==1))
                        {
                            Printer_struct.Printer_data[i] = ' ';
                            nullfound=1;
                        }
                        else Printer_struct.Printer_data[i] = Admin_var.Scale_Name[i-73];

                    }

                    sprintf (&Printer_struct.Printer_data[77], "LOAD ");
                    Printer_struct.Printer_data[82]=' ';
                    if(Sens_brd_param.Inst_LC_Perc[0] < 10)
                        sprintf (&Printer_struct.Printer_data[84], "%1.1f",Sens_brd_param.Inst_LC_Perc[0]);
                    else if(Sens_brd_param.Inst_LC_Perc[0] < 100)
                        sprintf (&Printer_struct.Printer_data[83], "%2.1f",Sens_brd_param.Inst_LC_Perc[0]);
                    else if(Sens_brd_param.Inst_LC_Perc[0] < 1000)
                        sprintf (&Printer_struct.Printer_data[82], "%3.1f",Sens_brd_param.Inst_LC_Perc[0]);

                    Printer_struct.Printer_data[87] = '%';
                    Printer_struct.Printer_data[88] = '\r';
                    Printer_struct.Printer_data[89] = '\n';
                    Printer_struct.Printer_data[90] = '\0';
                    send_debug = 0;
                }
                else if((send_debug == 1) && (send_debug_char == 'k'))
                {
                    memset(Printer_struct.Printer_data,' ',sizeof(Printer_struct.Printer_data));

                    memcpy(&Printer_struct.Printer_data[0],"\r\nMASTER TOTAL IS     ",22);
                    Printer_struct.Printer_data[22] = '\r';
                    Printer_struct.Printer_data[23] = '\n';
                    if(Rprts_diag_var.Master_total < 0)
                    {
                        sprintf (&Printer_struct.Printer_data[24], "-         ");
                    }
                    else
                        sprintf (&Printer_struct.Printer_data[24], "          ");

                    //disp_value = fabsf(Rprts_diag_var.Master_total);		//Suvrat
                    dbl_disp_value = fabs(Rprts_diag_var.Master_total);
                    if(dbl_disp_value < 10)
                        sprintf (&Printer_struct.Printer_data[33], "%1.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100)
                        sprintf (&Printer_struct.Printer_data[32], "%2.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000)
                        sprintf (&Printer_struct.Printer_data[31], "%3.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 10000)
                        sprintf (&Printer_struct.Printer_data[30], "%4.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100000)
                        sprintf (&Printer_struct.Printer_data[29], "%5.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000000)
                        sprintf (&Printer_struct.Printer_data[28], "%6.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 10000000)
                        sprintf (&Printer_struct.Printer_data[27], "%7.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 100000000)
                        sprintf (&Printer_struct.Printer_data[26], "%8.3lf\r\n",dbl_disp_value);
                    else if(dbl_disp_value < 1000000000)
                        sprintf (&Printer_struct.Printer_data[25], "%9.3lf\r\n",dbl_disp_value);
                    else
                        sprintf (&Printer_struct.Printer_data[25], "%9.3lf\r\n",999999999.999);

                    sprintf (&Printer_struct.Printer_data[38], "      ");
                    memcpy(&Printer_struct.Printer_data[44],"\r\nTIME ",7);
                    sprintf (&Printer_struct.Printer_data[51], "%02d:%02d:%02d       \r\n",
                             Current_time.RTC_Hour, Current_time.RTC_Min, Current_time.RTC_Sec);

                    memcpy(&Printer_struct.Printer_data[66],"\r\nDATE ",7);
                    sprintf (&Printer_struct.Printer_data[73], "%02d/%02d/\r\n",
                             Current_time.RTC_Mon, Current_time.RTC_Mday);
                    sprintf(&buf[0], "%d", Current_time.RTC_Year);
                    Printer_struct.Printer_data[79] = buf[2];
                    Printer_struct.Printer_data[80] = buf[3];
                    sprintf (&Printer_struct.Printer_data[81], "       ");
                    Printer_struct.Printer_data[88] = '\r';
                    Printer_struct.Printer_data[89] = '\n';
                    Printer_struct.Printer_data[90] = '\0';
                    send_debug = 0;
                }
                else if(send_debug == 2)
                {
                    memset(Printer_struct.Printer_data,' ',sizeof(Printer_struct.Printer_data));
                    sprintf (Printer_struct.Printer_data, " WEIGHT     %0.3lf\r\n DATE       %d/%d/%d\r\n TIME       %d:%d:%d\r\n TICKET #   %d\r\n SCALE #    %d\r\n PHONE:     %s\r\n %s\r\n %s\r\n %s\r\n %s\r\n",// %s\r\n",/* \r\n \r\n \r\n \r\n \r\n \r\n",*/
                             Calculation_struct.Total_weight,
                             Current_time.RTC_Mon, Current_time.RTC_Mday, Current_time.RTC_Year,
                             Current_time.RTC_Hour, Current_time.RTC_Min, Current_time.RTC_Sec,
                             Setup_device_var.Printer_ticket_num,
                             Printer_struct.Scale_num,
                             Setup_device_var.Phone,
                             Setup_device_var.Addr3,
                             Setup_device_var.Addr2,
                             Setup_device_var.Addr1,
                             Setup_device_var.Company_name);
                    send_debug = 0;
                    Setup_device_var.Printer_ticket_num++;
                    GUI_data_nav.GUI_structure_backup = 1;
                }

                i = 0;
                UART_SendByte(_PRINTER_UART, (uint8_t)Printer_struct.Printer_data[0]);
                UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, ENABLE);
                i++;
            }
            LPC_RTC->ALMIN&= ~(0x03<<0);
            LPC_RTC->ALMIN|= (0x03<<0);						//SECTION 3

            os_tsk_prio_self(MODBUS_SEN_TASK_PRIORITY+1);
            //UART_SendByte(_PRINTER_UART, '\r');
            //UART_SendByte(_PRINTER_UART, '\n');
            while (Printer_struct.Printer_data[i] != '\0')
            {
                if (usart0_putch(Printer_struct.Printer_data[i]) != (-1))
                {
                    i++;
                }
                if (Printer_struct.xoff_rcvd == 1)
                {
                    break;
                }
            }
            os_tsk_prio_self(PRINTER_TASK_PRIORITY);
        }
        else if(Printer_struct.flow_control == 1) //hardware flow control enabled
        {
            //if (((_PRINTER_UART->MSR) && UART1_MSR_CTS) == UART1_MSR_CTS)
            if (UART_FullModemGetStatus(LPC_UART1) & UART1_MODEM_STAT_CTS)
            {
                sprintf (Printer_struct.Printer_data," WEIGHT     %0.3f\r\n DATE       %d/%d/%d\r\n TIME       %d:%d:%d\r\n TICKET #   %d\r\n SCALE #    %d\r\n PHONE:     %s\r\n %s\r\n %s\r\n %s\r\n %s\r\n",/* \r\n \r\n \r\n \r\n \r\n \r\n",*/
                         (Calculation_struct.Net_weight * Calculation_struct.Weight_conv_factor),
                         Current_time.RTC_Mon, Current_time.RTC_Mday, Current_time.RTC_Year,
                         Current_time.RTC_Hour, Current_time.RTC_Min, Current_time.RTC_Sec,
                         Setup_device_var.Printer_ticket_num,
                         Printer_struct.Scale_num,
                         Setup_device_var.Phone,
                         Setup_device_var.Addr3,
                         Setup_device_var.Addr2,
                         Setup_device_var.Addr1,
                         Setup_device_var.Company_name);
                i = 0;
                Setup_device_var.Printer_ticket_num++;
                GUI_data_nav.GUI_structure_backup = 1;
                UART_SendByte(_PRINTER_UART, (uint8_t)Printer_struct.Printer_data[0]);
                UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, ENABLE);
                i++;
                //os_tsk_prio_self(7);
                os_tsk_prio_self(MODBUS_SEN_TASK_PRIORITY+1);
                while (Printer_struct.Printer_data[i] != '\0')
                {
                    if (usart0_putch(Printer_struct.Printer_data[i]) != (-1))
                    {
                        i++;
                    }
                    if (!(UART_FullModemGetStatus(LPC_UART1) & UART1_MODEM_STAT_CTS))
                    {
                        break;
                    }
                }
                //os_tsk_prio_self(3);
                os_tsk_prio_self(PRINTER_TASK_PRIORITY);
            }
        }
#endif //#ifdef CALCULATION
        LPC_RTC->ALMIN&= ~(0x03<<0); 			//SECTION EXIT
    }//while(1)
}

/*****************************************************************************
* @note       Function name: void check_string(char * string, U8 index)
* @returns    returns      : *string - Pointer to string
														 index - String index
* @param      arg1         : None
* @author                  : Megha Gaikwad
* @date       date created : 25th Aug 2015
* @brief      Description  : Check string and fill Printer_data for command response
* @note       Notes        : None
*****************************************************************************/
void check_string(char * string, U8 index)
{
    U8 cnt = 0, i = 0;

    for(i=0; i<3; i++)
    {
        if(string[i]!='\0')
            cnt++;
        else
            break;
    }
    if(cnt == 0)
    {
        Printer_struct.Printer_data[index++] = ' ';
        Printer_struct.Printer_data[index++] = ' ';
        Printer_struct.Printer_data[index++] = ' ';
    }
    if(cnt == 1)
    {
        Printer_struct.Printer_data[index++] = string[0];
        Printer_struct.Printer_data[index++] = ' ';
        Printer_struct.Printer_data[index] =  ' ';
    }
    else if(cnt == 2)
    {
        Printer_struct.Printer_data[index++] = string[0];
        Printer_struct.Printer_data[index++] = string[1];
        Printer_struct.Printer_data[index] = ' ';
    }
    else
    {
        Printer_struct.Printer_data[index++] = string[0];
        Printer_struct.Printer_data[index++] = string[1];
        Printer_struct.Printer_data[index] = string[2];

    }

}
#endif /*#ifdef PRINTER*/
/*****************************************************************************
* End of file
*****************************************************************************/
