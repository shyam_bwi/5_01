/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : EEPROM_high_level.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : April Thursday, 2012  <April 11, 2012>
* @date Last Modified	 : April Thursday, 2012  <April 11, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __EEPROM_HIGH_LEVEL_H
#define __EEPROM_HIGH_LEVEL_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "lpc177x_8x_eeprom.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

#ifdef EEPROM
#define SCALE_SETUP_NO_OF_PAGES      3
#define CALIBRATION_NO_OF_PAGES      3
#define SETUP_DEVICE_NO_OF_PAGES     9
#define ADMIN_NO_OF_PAGES            6					//MEGHA : admin pages now (30-09-2016) = 6 (prev 5)
#define MAC_NO_OF_PAGES								1
#define MISC_NO_OF_PAGES							1				//+1 bcause after mac id page,e2p backup fg is written on next page
//#define RPRTS_DIAG_NO_OF_PAGES      4

#define EEPROM_BACKUP_FLAG           0xAF
#define PAGE_NO_LOCATION             22
#define OFFSET_FOR_FLAG              0
#define OFFSET_FOR_PAGE_NO           1  //corresponds to 1st location
#define OFFSET_FOR_COUNTER           2  //corresponds to 8 bits variable since pg number is 8 bit var
#define ENDURANCE_CYCLES             99990
#define OFFSET_FOR_ENDURANCE_CTR     8  //corresponds to 64 bits/8 byte var since wt is a double
#define EEPROM_PAGE_NO_WT_BACKUP     40

#define EEPROM_WRITE_ERR_FLAG        0x01
#define EEPROM_WRITE_ERR_PAGE_NO     62

#define SCALE_SETUP_START_PAGE_NO    0
#define CALIBRATION_START_PAGE_NO	   (SCALE_SETUP_START_PAGE_NO + SCALE_SETUP_NO_OF_PAGES) //3
#define SETUP_DEVICE_START_PAGE_NO	 (CALIBRATION_START_PAGE_NO + CALIBRATION_NO_OF_PAGES) //6
#define ADMIN_START_PAGE_NO	 			   (SETUP_DEVICE_START_PAGE_NO + SETUP_DEVICE_NO_OF_PAGES) //15
#define MAC_PAGE_NO							 (ADMIN_START_PAGE_NO + ADMIN_NO_OF_PAGES )	//		 (ADMIN_START_PAGE_NO + ADMIN_NO_OF_PAGES + 1)	
//#define RPRTS_DIAG_START_PAGE_NO		 (ADMIN_START_PAGE_NO + ADMIN_NO_OF_PAGES)//20
#define MISC_START_PAGE_NO				(MAC_PAGE_NO + MAC_NO_OF_PAGES + 1)		
#define OUI1         0x00
#define OUI2         0x11
#define OUI3         0x27

/*============================================================================
* Public Data Types
*===========================================================================*/
typedef struct
              {
								 uint8_t Page_number;
								 uint8_t Offset;
								 double Endurance_counter;
							}EEPROM_WT_BACKUP_STRUCT;
							
typedef union {
   unsigned int  uint_val;
   unsigned char uint_array[4];
}UINT_TYPE;
typedef struct {
	               UINT_TYPE oui; // Organization Unique ID
	               UINT_TYPE next_MAC; // Next MAC address
	               UINT_TYPE available_MACs; // Available MAC ID's
	               UINT_TYPE checksum;  // Check Sum for the file
}MAC_FILE_DATA;
// typedef union
// 							{
// 								 U8 oui[3];
// 								 U8 mac[6];
// 							}MAC_ID_STRUCT;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern unsigned char Struct_backup_fg;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */
extern float g_old_fw_ver;
extern float g_current_fw_ver;
/* Double variables section */

/* Structure or Union variables section */
extern EEPROM_WT_BACKUP_STRUCT EEPROM_wt_backup;
extern unsigned char Backup_individual_param_fg;
extern U8 own_hw_adr[];
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void eeprom_first_time_write(void);
extern void eeprom_weight_backup(void);
extern void eeprom_weight_reload(void);
extern void eeprom_struct_backup(void);
extern void eeprom_struct_reload(void);
extern U8 eeprom_program_MAC_id(void);
extern void Read_FW_Ver_From_EEPOM(void);
void EEP_load_good_data (void);
#endif /*#ifdef EEPROM*/

#endif /*__EEPROM_HIGH_LEVEL_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
