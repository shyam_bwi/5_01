/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : IOProcessing.h
* @brief               : All IO processing functions depending on IO status
*
* @author              : Shweta Pimple
*
* @date Created        : January 2, 2013
* @date Last Modified  : January 2, 2013
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __IOPROCESSING_H
#define __IOPROCESSING_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include <RTL.h>
#include "lpc_types.h"
#ifdef IOBOARD
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
  {
    float Load_from_strt_stop_stn;         /*Load weight from start stop station*/
    //double Target_wt;
    //double Cutoff_wt;
    float Weight_per_pulse;
    double Delivered_weight_OP1;
		double Delivered_weight_OP2;
		double Delivered_weight_OP3;
		double Delivered_weight_Quad_OP;
		unsigned char Load_in_percentage;	  
    char Quadrature_Wave_Setting;
    char IO_Board_Status;
    char Batching_Flag;
    char Inst_Zero_Cal_Flag;
    char Cal_started_flag;
    char Error_ack_flag;
    char Clear_weight_input_set;
    char Send_event_to_IO_board;
		char Pulsed_Output_Done_Flag;
    uint8_t Status_displayed_flag;
  }IO_BOARD_PARAM;

extern IO_BOARD_PARAM IO_board_param;

enum
{
  LOAD_CELL_ERR = 1,
  //ANGLE_SENSOR_ERR ,
  COMM_ERR,
  ANY_ERR
};

enum
{
    BATCH_START = 1,
    BATCH_COMPLETE,
};
/*============================================================================
* Public Function Declarations
*===========================================================================*/

extern void Check_for_Quadrature_Wave_Setting(void);
extern uint16_t Update_DO_RLY_Outputs (char Output_number,unsigned int Output_Func, unsigned int Speed_Output_Type, 
																				uint16_t Speed_MinMax_Setpoint,unsigned int Rate_Output_Type, 
																				uint16_t Rate_MinMax_Setpoint,unsigned int Error_Alarm_Type);
extern uint16_t Get_RawCount_For_AO (uint8_t AO_Ch);
extern void Process_DigitalInputs (unsigned Input_Function);
//__task void monitor_weight_task (void);


#endif /*#ifdef IOBOARD*/
#endif /*__IOPROCESSING_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
