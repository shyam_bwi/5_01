/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : IOboard_Drv.c
* @brief               : IO Board driver layer (SPI interface)
*
* @author              : Shweta Pimple
*
* @date Created        : December 19, 2012
* @date Last Modified  : December 19, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "lpc177x_8x_ssp.h"
#include "IOboard_Drv.h"
#include "IOboard_App.h"
#include <string.h>
#include "RTOS_main.h"
#ifdef IOBOARD
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
/* Status Flag indicates current SSP transmission complete or not */
  uint8_t complete = 0;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */


// SSP data structure variable
static SSP_DATA_SETUP_Type SSP_DataStruct;

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/

/*********************************************************************//**
 * @brief    SSP0 Interrupt used for reading and writing handler
 * @param    None
 * @return   None
 ***********************************************************************/
void SSP_IRQHandler(void)
{
  uint16_t tmp;

  /* check if RX FIFO contains data */
  //while (SSP_GetStatus(LPC_SSP, SSP_STAT_RXFIFO_NOTEMPTY) == SET)
  while ((SSP_GetRawIntStatus(LPC_SSP, SSP_INTSTAT_RAW_RT) == SET) ||
					(SSP_GetStatus(LPC_SSP, SSP_STAT_RXFIFO_NOTEMPTY) == SET))
  {
    tmp = SSP_ReceiveData(LPC_SSP);
    // Store data to destination
    if (SSP_DataStruct.rx_data != NULL)
    {
        *(uint8_t *)((uint32_t)SSP_DataStruct.rx_data + SSP_DataStruct.rx_cnt) = (uint8_t) tmp;
    }
    // Increase counter
      SSP_DataStruct.rx_cnt++;
  }

  /* There is no data to receive */
  if (SSP_DataStruct.rx_cnt >= SSP_DataStruct.length)
  {
    //SSP_IntConfig(LPC_SSP, SSP_INTCFG_RT, DISABLE);
		 SSP_IntConfig(LPC_SSP,(SSP_INTCFG_RT | SSP_INTCFG_RX), DISABLE);

    // Save status
    SSP_DataStruct.status = SSP_STAT_DONE;
    // Set Complete Flag
    complete = DONE;

    //send event to ioboard task
    isr_evt_set (IOBOARD_EVENT_FLAG, t_ioboard);
  }

  /* Clear all interrupt */
  SSP_ClearIntPending(LPC_SSP, SSP_INTCLR_RT);

}

/*-------------------------PRIVATE FUNCTIONS------------------------------*/

/*********************************************************************//**
* @note       Function name: void Init_SSP_Buffer(uint8_t* pu8SSPTxBuf, uint8_t* pu8SSPRxBuf)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Shweta Pimple
* @date       date created : 21, December 2012
* @brief      Description  : Initialize SSP driver buffer
************************************************************************/
void Init_SSP_Buffer(uint8_t* pu8SSPTxBuf, uint8_t* pu8SSPRxBuf)
{
    SSP_DataStruct.tx_data = pu8SSPTxBuf;
    SSP_DataStruct.rx_data = pu8SSPRxBuf;
    SSP_DataStruct.length  = 0;
    SSP_DataStruct.tx_cnt  = 0;
    SSP_DataStruct.rx_cnt  = 0;
}

/*****************************************************************************
* @note       Function name: void Config_SSP_Intrpt(uint8_t u8cfg)
* @returns    returns      : None
* @param      arg1         : Configuration
*                              ENABLE or DISABLE
* @author                  : Shweta Pimple
* @date       date created : 20, December 2012
* @brief      Description  : Configuration of SSP interrupt
* @note       Notes        : None
*****************************************************************************/
void Config_SSP_Intrpt(uint8_t u8cfg)
{
  if(u8cfg == ENABLE)
  {
      //Enable receive interrupt
      SSP_IntConfig(LPC_SSP,(SSP_INTCFG_RT | SSP_INTCFG_RX), ENABLE);
      /* preemption = 1, sub-priority = 1 */
      NVIC_SetPriority(SSP_IRQn, 0x21);
      /* Enable SSP0 interrupt */
      NVIC_EnableIRQ(SSP_IRQn);
  }
  else
  {
    //Disable all interrupt
    SSP_IntConfig(LPC_SSP,(SSP_INTCFG_RT | SSP_INTCFG_RX), DISABLE);
  }
}

/*****************************************************************************
* @note       Function name: void Init_IOboard_GPIO (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Shweta Pimple
* @date       date created : 20, December 2012
* @brief      Description  : IOboard initialization
* @note       Notes        : None
*****************************************************************************/
void Init_IOboard_GPIO (void)
{
  /*
   * Initialize SSP pin connect
   * P2.22 - SCK;
   * P2.23 - SSEL
   * P2.26 - MISO
   * P2.27 - MOSI
   */
  PINSEL_ConfigPin(SSP_PORT, SSP_SCK_GPIO, FUNC_SSP);
  PINSEL_ConfigPin(SSP_PORT, SSP_SSEL_GPIO, FUNC_SSP);
  PINSEL_ConfigPin(SSP_PORT, SSP_MISO_GPIO, FUNC_SSP);
  PINSEL_ConfigPin(SSP_PORT, SSP_MOSI_GPIO, FUNC_SSP);

  PINSEL_ConfigPin(2, 22, 2);
  PINSEL_ConfigPin(2, 23, 2);
  PINSEL_ConfigPin(2, 26, 2);
  PINSEL_ConfigPin(2, 27, 2);
}

/***********************PUBLIC FUNCTION **********************************/
/*****************************************************************************
* @note       Function name: void Init_IOboard(uint8_t SSP_State, uint8_t *pu8SspTxBuf,
*                                                uint8_t *pu8SspRxBuf, uint8 u8DataLength)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Shweta Pimple
* @date       date created : 20, December 2012
* @brief      Description  : SSP initialization
* @note       Notes        : None
*****************************************************************************/
// SSP Configuration structure variable
  SSP_CFG_Type SSP_ConfigStruct;

void Init_IOboard(uint8_t SSP_State, uint8_t* pu8SspTxBuf,
                  uint8_t* pu8SspRxBuf, uint8_t u8DataLength)
{
  uint8_t u8IntCfg;


  //Disable the SSP peripheral
  SSP_Cmd(LPC_SSP, DISABLE);

  //Reset the SSPx peripheral registers
  SSP_DeInit(LPC_SSP);

  complete = CLR;
  u8IntCfg = DISABLE;

  /* Initialize SPI Buffer */
  Init_SSP_Buffer(pu8SspTxBuf, pu8SspRxBuf);

  //Assign the no of bytes to transmit/receive
  SSP_DataStruct.length = u8DataLength;

  switch (SSP_State)
  {
    case INIT:
            Init_IOboard_GPIO();
            /* Initialize SPI Buffer */
            //Init_SSP_Buffer(pu8SspTxBuf, pu8SspRxBuf);
            // initialize SSP configuration structure to default
            SSP_ConfigStructInit(&SSP_ConfigStruct);
            break;
    case MASTER:
            SSP_ConfigStruct.Mode = SSP_MASTER_MODE;
            //Init the driver layer receive variables
            //SSP_DataStruct.tx_cnt  = 0;
            u8IntCfg = DISABLE;
            break;
    case SLAVE:
            SSP_ConfigStruct.Mode = SSP_SLAVE_MODE;
            u8IntCfg = ENABLE;
            //Init the driver layer receive variables
            //SSP_DataStruct.rx_cnt  = 0;
            break;
    default:
            break;
  }

  // Initialize SSP peripheral with parameter given in structure above
  SSP_Init(LPC_SSP, &SSP_ConfigStruct);

  // Enable SSP peripheral
  SSP_Cmd(LPC_SSP, ENABLE);

  //Enable/Disable the interrupt according to SSP mode
  Config_SSP_Intrpt(u8IntCfg);

 // wait for current SSP activity complete
  while (SSP_GetStatus(LPC_SSP, SSP_STAT_BUSY))
  {
    SSP_ReceiveData(LPC_SSP);
  }

  /* Clear all remaining data in RX FIFO */
  while (SSP_GetStatus(LPC_SSP, SSP_STAT_RXFIFO_NOTEMPTY))
  {
    SSP_ReceiveData(LPC_SSP);
  }
   return;
}

/*****************************************************************************
* @note       Function name: int8_t ioboard_send_frame (uint8_t u8DatLen)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Shweta Pimple
* @date       date created : 20, December 2012
* @brief      Description  : IOboard initialization
* @note       Notes        : None
*****************************************************************************/
int8_t Ioboard_send_frame (uint8_t u8DatLen)
{
  int8_t Tx_status;

  //Reset the data transfer complete flag
  complete = CLR;

  //Define the no of bytes to send
  SSP_DataStruct.length = u8DatLen;

  //Init the driver layer receive variables
  SSP_DataStruct.tx_cnt  = 0;

  //send request frame
  Tx_status = SSP_ReadWrite(LPC_SSP, &SSP_DataStruct, SSP_TRANSFER_POLLING);

  return Tx_status;
}


#endif /*#ifdef IOBOARD*/
/*****************************************************************************
* End of file
*****************************************************************************/
