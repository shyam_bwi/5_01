/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : IOProcessing.c
* @brief               : All IO processing functions depending on IO status
*
* @author              : Shweta Pimple
*
* @date Created        : January 2, 2013
* @date Last Modified  : Feb 12, 2014
* @Modified by				 : Venkata Krishna rao
* @internal Change Log : <YYYY-MM-DD>
* @internal            : Changes made related to Digital Inputs and Analog Outputs Diaganostic Variables.
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include <math.h>
#include "Screen_global_ex.h"
#include "Sensor_board_data_process.h"
#include "IOProcessing.h"
#include "RTOS_main.h"
#include "IOboard_App.h"
#include "Screen_data_enum.h"
#include "Calibration.h"
#include "Printer_high_level.h"
#include "Rate_blending_load_ctrl_calc.h"
//int PulseHigh1,PulseHigh2,PulseHigh3,ZeroedHighPulse=0;
#ifdef IOBOARD
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/
/* Character variables section */


/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
IO_BOARD_PARAM IO_board_param;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void Print_Ticket(void);
void Print_Then_Clear(void);
void Print_Then_Clear(void);
void Enter_Load(void);
void PID_Rate_Zero(void);
void Process_DigitalInputs (unsigned int Input_Function);
// void Get_Weight_Per_Pulse(void);
// void Find_Weight_Per_Pulse(char Output_no, unsigned int Digital_Output_Func, 
//                              unsigned int Digital_Output_Units_Per_Pulse, double *pOutput_Delivered_wt);

// void Get_Batch_Complete_Status(void);
// void Check_for_Quadrature_Wave_Setting(void);
uint16_t Update_DO_RLY_Outputs (char Output_number,unsigned int Output_Func, unsigned int Speed_Output_Type, uint16_t Speed_MinMax_Setpoint,
                               unsigned int Rate_Output_Type, uint16_t Rate_MinMax_Setpoint, unsigned int Error_Alarm_Type);

uint16_t Get_RawCount_For_AO (uint8_t AO_Ch);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name : void Print_Ticket(void)
* @returns    returns       : None
* @param      arg1          : None
* @param      arg2          : None
* @author                   : Shweta Pimple
* @date       date  Created : 12 Jan 2013
* @brief      Description   : Give command to printer
* @note       Notes         : None
*****************************************************************************/
void Print_Ticket(void)
{
    //printf("Print ticket function");
    //send event to print task
	send_debug = 2;
    os_evt_set (PRINTER_EVENT_FLAG, t_printer_data);
}

/*****************************************************************************
* @note       Function name : void Print_Then_Clear(void)
* @returns    returns       : None
* @param      arg1          : None
* @param      arg2          : None
* @author                   : Shweta Pimple
* @date       date  Created : 12 Jan 2013
* @brief      Description   : Give commnd to printer and then clear the  total weight variable
* @note       Notes         : None
*****************************************************************************/
void Print_Then_Clear(void)
{
    //send event to print task
	send_debug = 2;
    os_evt_set (PRINTER_EVENT_FLAG, t_printer_data);

    if(Admin_var.Clear_Weight_Locked == LOCK_ENABLE_STR)
    {
       GUI_data_nav.Clr_wt_pw_to_be_entered = CLR_WT_PW_SCREEN;
    }
    else
    {
       GUI_data_nav.Clr_wt_pw_to_be_entered = CLEAR_THE_WEIGHT;
    }
    //Set a flag to clear the variable of accumulated weight
    IO_board_param.Clear_weight_input_set = __TRUE;
}
/*****************************************************************************
* @note       Function name : void   Enter_Load(void)
* @returns    returns       : None
* @param      arg1          : None
* @param      arg2          : None
* @author                   : Shweta Pimple
* @date       date  Created : 12 Jan 2013
* @brief      Description   :
* @note       Notes         : None
*****************************************************************************/
void   Enter_Load(void)
{
  //float Angle_In_v;

  //read angle sensor input and select the load weight based on input voltage
  //0.25V = LOAD 1
  //0.75V = LOAD 2
  //1.25V = LOAD 3
  //1.75V = LOAD 4
  //2.25V = LOAD 5
  //2.75V = LOAD 6
  //3.25V = LOAD 7
  //3.75V = LOAD 8
//    Angle_In_v = (Sens_brd_param.Angle_mv / 1000.0);

    if( Sens_brd_param.Angle_mv >=0 && Sens_brd_param.Angle_mv<339)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_1;
    }
    else if( Sens_brd_param.Angle_mv>= 339 && Sens_brd_param.Angle_mv<668)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_2;
    }
    else if( Sens_brd_param.Angle_mv>=668 && Sens_brd_param.Angle_mv<979)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_3;
    }
    else if(Sens_brd_param.Angle_mv>=979 && Sens_brd_param.Angle_mv<1283)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_4;
    }
    else if(Sens_brd_param.Angle_mv>=1283 && Sens_brd_param.Angle_mv<1588)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_5;
    }
    else if(Sens_brd_param.Angle_mv>=1588 && Sens_brd_param.Angle_mv<1904)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_6;
    }
    else if(Sens_brd_param.Angle_mv>=1904 && Sens_brd_param.Angle_mv<2238)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_7;
    }
    else if( Sens_brd_param.Angle_mv>=2272 && Sens_brd_param.Angle_mv<2500)   //considering hysterisis of 0.5V
    {
        IO_board_param.Load_from_strt_stop_stn = Setup_device_var.Load_weight_8;
    }
		Calculation_struct.Target_weight = IO_board_param.Load_from_strt_stop_stn;
}

/*****************************************************************************
* @note       Function name : void PID_Rate_Zero(void)
* @returns    returns       : None
* @param      arg1          : None
* @param      arg2          : None
* @author                   : Shweta Pimple
* @date       date  Created : 12 Jan 2013
* @brief      Description   : Reset the PID rate variable
* @note       Notes         : None
*****************************************************************************/
void PID_Rate_Zero(void)
{
	 if(Setup_device_var.PID_Local_Setpoint != 0)
	 {
	    PID_Old_Local_Setpoint = Setup_device_var.PID_Local_Setpoint;
      Setup_device_var.PID_Local_Setpoint = 0;
	 }
}


/*-------------------------PUBLIC FUNCTIONS------------------------------*/

/*****************************************************************************
* @note       Function name : void Process_Inputs (void)
* @returns    returns       : None
* @param      arg1          : None
* @param      arg2          : None
* @author                   : Shweta Pimple
* @date       date  Created : 12 Jan 2013
* @brief      Description   : Calculate CRC
* @note       Notes         : None
*****************************************************************************/
void Process_DigitalInputs (unsigned int Input_Function)
{
    /*"Clear Weight"*/
    if(Input_Function == CLEAR_WEIGHT_STR)
    {
        if( Admin_var.Clear_Weight_Locked == LOCK_ENABLE_STR)
        {
           GUI_data_nav.Clr_wt_pw_to_be_entered = CLR_WT_PW_SCREEN;
        }
        else
        {
           GUI_data_nav.Clr_wt_pw_to_be_entered = CLEAR_THE_WEIGHT;
        }
        IO_board_param.Clear_weight_input_set = __TRUE;/*
        //Clear the weight to enter the new load for next batch
        IO_board_param.Batch_Start_Flag = BATCH_START;*/
    }
    /*"Print Ticket"*/
    else if(Input_Function == PRINT_TICKET_STR)
    {
        Print_Ticket();
    }
    /*"Print then Clear"*/
    else if(Input_Function == PRINT_AND_CLEAR_STR)
    {
        Print_Then_Clear();
    }
    /*"Enter Load"*/
    else if(Input_Function == ENTER_LOAD_STR )
    {
        Enter_Load();
    }
    /*"Zero Calibration"*/
    else if(Input_Function == ZERO_CAL_STR)
    {
      Calib_struct.Cal_status_flag = START_DYNAMICZERO_CAL;
      IO_board_param.Cal_started_flag = __TRUE;
    }
    /*"Error Acknowledge"*/
//     else if(Input_Function == ERROR_ACK_STR)
//     {
//       IO_board_param.Error_ack_flag = __TRUE;
//     }
}
// /*****************************************************************************
// * @note       Function name : void Get_Weight_Per_Pulse(void)
// * @returns    returns       : None
// * @param      arg1          : None
// * @author                   :
// * @date       date created  : 25 Apr 2013
// * @brief      Description   : Get the computed value of Weight_Per_Pulse
// * @note       Notes         : None
// *****************************************************************************/
// void Get_Weight_Per_Pulse(void)
// {
// 	Find_Weight_Per_Pulse(1,Setup_device_var.Digital_Output_1_Function, Setup_device_var.Units_Per_Pulse_1, 
// 												&IO_board_param.Delivered_weight_OP1);
// 	
//   Find_Weight_Per_Pulse(2,Setup_device_var.Digital_Output_2_Function, Setup_device_var.Units_Per_Pulse_2,
// 	                       &IO_board_param.Delivered_weight_OP2);
//   
// 	Find_Weight_Per_Pulse(3,Setup_device_var.Digital_Output_3_Function, Setup_device_var.Units_Per_Pulse_3,
// 													&IO_board_param.Delivered_weight_OP3);
// }

// /*****************************************************************************
// * @note       Function name : void Process_Pulse_Output(char Output_no, float New_Wt_Per_Pulse, float *pOutput_Delivered_wt)
// * @returns    returns       : None
// * @param      arg1          : None
// * @author                   :
// * @date       date created  : 20 Mar 2014
// * @brief      Description   : 
// * @note       Notes         : None
// *****************************************************************************/
// void Process_Pulse_Output(char Output_no, float New_Wt_Per_Pulse, double *pOutput_Delivered_wt)
// {
// 	double Delivered_wt = 0;
// 	
// 		IO_board_param.Pulsed_Output_Done_Flag &= ~(1 << (Output_no - 1));
// 	
// 			Delivered_wt = (*pOutput_Delivered_wt) * Calculation_struct.Weight_conv_factor;
// 	
// 	//	*pOutput_Delivered_wt = *pOutput_Delivered_wt + IO_board_param.Delivered_weight_Net;
// 	
// 		if(Delivered_wt >= New_Wt_Per_Pulse)
// 		{
// 			
// 			 *pOutput_Delivered_wt = *pOutput_Delivered_wt - (New_Wt_Per_Pulse / Calculation_struct.Weight_conv_factor);
// 			
// 				if(Output_no == 1)
// 				{
// 					PulseHigh1++;
// 				}
// 				else if(Output_no == 2)
// 				{
// 					PulseHigh2++;					
// 				}
// 				else if(Output_no == 3)
// 				{
// 					PulseHigh3++;
// 				}
// 			 //check for pulse output set flag
// 				IO_board_param.Pulsed_Output_Done_Flag |= (1 << (Output_no - 1));	 
// 			 //send an event to IO board
// 			 IO_board_param.Send_event_to_IO_board = 1;
// 		}
// }

// /*****************************************************************************
// * @note       Function name : float Find_Weight_Per_Pulse(char Output_no, char* Digital_Output_Func, char* Digital_Output_Units_Per_Pulse)
// * @returns    returns       : None
// * @param      arg1          : char* to Digital_Output_Func,Digital_Output_Units_Per_Pulse
// * @author                   :
// * @date       date created  : 25 Apr 2013
// * @brief      Description   : find the value of Weight_Per_Pulse using LC capacity
// * @note       Notes         : None
// *****************************************************************************/
// void Find_Weight_Per_Pulse(char Output_no, unsigned int Digital_Output_Func,
// 														unsigned int Digital_Output_Units_Per_Pulse, double *pOutput_Delivered_wt)
// {
// 	float New_Wt_Per_Pulse = 0.1;
//   double TempTotal_weight_Perc = 0;
// 		
//   //Pulsed Output and  Quadrature Wave
//   if((Digital_Output_Func == PULSED_OUTPUT_STR) ||  (Digital_Output_Func == QUADRATURE_WAVE_STR))
//   {
//     //If the output conf. for pulse
//     if(Digital_Output_Func == PULSED_OUTPUT_STR)
//     {
// 			if( Digital_Output_Units_Per_Pulse == FIFTH_WT_UNIT_PER_PULSE_STR)
// 			{
//         New_Wt_Per_Pulse = (0.01);				
// 			}
//       //0.1 Weight Units
//       else if( Digital_Output_Units_Per_Pulse == FIRST_WT_UNIT_PER_PULSE_STR)
//       {
//         New_Wt_Per_Pulse = (0.1);
//       }
//       //1.0 Weight Units
//       else if( Digital_Output_Units_Per_Pulse == SECOND_WT_UNIT_PER_PULSE_STR)
//       {
//         New_Wt_Per_Pulse = (1.0);
//       }
//       //10 Weight Units
//       else if( Digital_Output_Units_Per_Pulse == THIRD_WT_UNIT_PER_PULSE_STR)
//       {
//         New_Wt_Per_Pulse = (10.0);
//       }
//       //100 Weight Units
//       else if( Digital_Output_Units_Per_Pulse == FOURTH_WT_UNIT_PER_PULSE_STR)
//       {
//         New_Wt_Per_Pulse = (100.0);
//       }
//       // default 0.1 Weight Units
//       else
//       {
//         New_Wt_Per_Pulse = IO_board_param.Weight_per_pulse;
//       }
//     }
//     else  //Output config  for quad take last pulse output - Weight Per Pulse value
//     {
// 				New_Wt_Per_Pulse = IO_board_param.Weight_per_pulse;
//     }

//     //Assign values
//     IO_board_param.Weight_per_pulse = New_Wt_Per_Pulse;

// 		if(Digital_Output_Func == PULSED_OUTPUT_STR)
// 		{
// 			if(IO_board_param.Quadrature_Wave_Setting == 11)
// 			{
// 				Process_Pulse_Output(Output_no, New_Wt_Per_Pulse, pOutput_Delivered_wt);
// 				return;
// 			}
// 		}
// 		//else
// 		{
// 				TempTotal_weight_Perc = ( (IO_board_param.Delivered_weight_Quad_OP * Calculation_struct.Weight_conv_factor) / New_Wt_Per_Pulse ) * 100;
// 			
// 				if((TempTotal_weight_Perc >= 25)&& (TempTotal_weight_Perc < 50))
// 				{
// 					  if(IO_board_param.Load_in_percentage != 25)
// 						{
// 							IO_board_param.Load_in_percentage = 25;
// 							//send an event to IO board to enable the 1st channel of quadrature output
// 							IO_board_param.Send_event_to_IO_board = 1 ;
// 						}
// 				}
// 				else if((TempTotal_weight_Perc >= 50) && (TempTotal_weight_Perc < 75))
// 				{
// 						if(IO_board_param.Load_in_percentage != 50)
// 						{
// 							IO_board_param.Load_in_percentage = 50;
// 							//send an event to IO board to enable the 2nd channel of quadrature output
// 							IO_board_param.Send_event_to_IO_board = 1 ;
// 						}
// 				}
// 				else if((TempTotal_weight_Perc >= 75) && (TempTotal_weight_Perc < 100))
// 				{
// 					 if(IO_board_param.Load_in_percentage != 75)
// 					 {
// 				     //check if weight is reached to multiples of weight per pulse
// 							IO_board_param.Load_in_percentage = 75;
// 							IO_board_param.Send_event_to_IO_board = 1 ;
// 					 }
// 				}
// 				else if(TempTotal_weight_Perc >= 100)
// 				{
// 						//set load to 100%
// 						IO_board_param.Load_in_percentage = 100;
// 						//send an event to IO board to enable the 2nd channel of quadrature output
// 						IO_board_param.Send_event_to_IO_board = 1 ;					
// 						IO_board_param.Delivered_weight_Quad_OP = IO_board_param.Delivered_weight_Quad_OP - (New_Wt_Per_Pulse / Calculation_struct.Weight_conv_factor);
// 				}
// 				else
// 				{
// 						IO_board_param.Load_in_percentage = 0;
// 				}
// 		}
// 	}
// }

// /*****************************************************************************
// * @note       Function name : void Get_Batch_Complete_Status(void)
// * @returns    returns       : None
// * @param      arg1          : None
// * @author                   :
// * @date       date created  : 25 Apr 2013
// * @brief      Description   : Get batch complete flag
// * @note       Notes         : None
// *****************************************************************************/
// void Get_Batch_Complete_Status(void)
// {

//     //check weight for Batching/Loadout functionality
// 	 if((Setup_device_var.Digital_Output_1_Function == BATCHING_LOADOUT_STR) ||
// 		  (Setup_device_var.Digital_Output_2_Function == BATCHING_LOADOUT_STR) ||
// 	    (Setup_device_var.Digital_Output_3_Function == BATCHING_LOADOUT_STR) ||
// 	    (Setup_device_var.Relay_Output_1_Function == BATCHING_LOADOUT_STR) ||
// 	    (Setup_device_var.Relay_Output_2_Function == BATCHING_LOADOUT_STR) ||
// 	    (Setup_device_var.Relay_Output_3_Function == BATCHING_LOADOUT_STR))
// 	 {
// 		 if(Calculation_struct.Total_weight >= (Calculation_struct.Target_weight - Calculation_struct.Cutoff_weight))
//      {
//           IO_board_param.Batching_Flag = BATCH_COMPLETE;
// 			    //send an event to IO board to turn of the batching output to stop the feeder
//           IO_board_param.Send_event_to_IO_board = 1 ;
//      }
// 	 }
// }
/*****************************************************************************
* @note       Function name : __task void monitor_weight_task (void)
* @returns    returns       : None
* @param      arg1          : None
* @author                   : Shweta Pimple
* @date       date created  : 12 Jan 2013
* @brief      Description   : Task to monitor weight continously and gives signal to other tasks
* @note       Notes         : None
*****************************************************************************/
// __task void monitor_weight_task (void)
// {

//   while(1)
//   {
//     os_evt_wait_or (MONITOR_WEIGHT_EVENT_FLAG, 0xffff);   /* wait for an event flag 0x0001*/
//     IO_board_param.Send_event_to_IO_board = 0;
// 		//Check for Quadrature output setting
// 		/*Check_for_Quadrature_Wave_Setting();
//     Get_Batch_Complete_Status();
//     Get_Weight_Per_Pulse();*/

//     /*if(IO_board_param.Send_event_to_IO_board == 1)
//     {
//       tsk_lock();
//       //send an event to IO board to turn of the batching output to stop the feeder
//       os_evt_set (IOBOARD_EVENT_FLAG, t_ioboard);
//       IO_board_param.Send_event_to_IO_board = 0;
//       tsk_unlock();
//     }*/
//  }
// }

/*****************************************************************************
* @note       Function name : void Check_for_Quadrature_Wave_Setting(void)
* @returns    returns       : None
* @param      arg1          : None
* @author                   :
* @date       date created  : 25 Apr 2013
* @brief      Description   : Check for quad settings and set the lags
* @note       Notes         : None
*****************************************************************************/
void Check_for_Quadrature_Wave_Setting(void)
{
  //Quadrature Wave
  if(Setup_device_var.Digital_Output_1_Function == QUADRATURE_WAVE_STR)
  {
    //check for another output selected for quad
    if((Setup_device_var.Digital_Output_2_Function == QUADRATURE_WAVE_STR)||
      (Setup_device_var.Digital_Output_3_Function == QUADRATURE_WAVE_STR))
    {
      IO_board_param.Quadrature_Wave_Setting = 0;
    }
    else
    {
      //Pulsed output
      if(Setup_device_var.Digital_Output_2_Function == PULSED_OUTPUT_STR)
      {
        IO_board_param.Quadrature_Wave_Setting = 12;
      }
      //Pulsed output
      else if(Setup_device_var.Digital_Output_3_Function == PULSED_OUTPUT_STR)
      {
        IO_board_param.Quadrature_Wave_Setting = 13;
      }
      else
      {
       IO_board_param.Quadrature_Wave_Setting = 0;
      }
    }
  }
  //Quadrature Wave
  else if(Setup_device_var.Digital_Output_2_Function == QUADRATURE_WAVE_STR)
  {
    //check for another output selected for quad
    if((Setup_device_var.Digital_Output_1_Function == QUADRATURE_WAVE_STR)||
      (Setup_device_var.Digital_Output_3_Function == QUADRATURE_WAVE_STR))
    {
      IO_board_param.Quadrature_Wave_Setting = 0;
    }
    else
    {
      //Pulsed output
      if(Setup_device_var.Digital_Output_1_Function == PULSED_OUTPUT_STR)
      {
        IO_board_param.Quadrature_Wave_Setting = 21;
      }
      //Pulsed output
      else if(Setup_device_var.Digital_Output_3_Function == PULSED_OUTPUT_STR)
      {
          IO_board_param.Quadrature_Wave_Setting = 23;
      }
      else
      {
        IO_board_param.Quadrature_Wave_Setting = 0;
      }
    }
  }
  //Quadrature Wave
  else if(Setup_device_var.Digital_Output_3_Function == QUADRATURE_WAVE_STR)
  {
    //check for another output selected for quad
    if((Setup_device_var.Digital_Output_1_Function == QUADRATURE_WAVE_STR)||
      (Setup_device_var.Digital_Output_2_Function == QUADRATURE_WAVE_STR))
    {
      IO_board_param.Quadrature_Wave_Setting = 0;
    }
    else
    {
      //Pulsed output
      if(Setup_device_var.Digital_Output_1_Function == PULSED_OUTPUT_STR)
      {
        IO_board_param.Quadrature_Wave_Setting = 31;
      }
      //Pulsed output
      else if(Setup_device_var.Digital_Output_2_Function == PULSED_OUTPUT_STR)
      {
          IO_board_param.Quadrature_Wave_Setting = 32;
      }
      else
      {
        IO_board_param.Quadrature_Wave_Setting = 0;
      }
    }
  }
  else
  {
    IO_board_param.Quadrature_Wave_Setting = 11;
  }

}
/*****************************************************************************
* @note       Function name   : uint16_t Check_for_Pluse_output_Wt(float Load_in_percentage_wt)
* @returns    returns         : None
* @param      arg1            : None
* @author                     :
* @date       date created    : 25 Apr 2013
* @brief      Description     : Check for Pluse_output_Wt
* @note       Notes           : None
*****************************************************************************/
uint16_t Check_for_Pluse_output_Wt(float Load_in_percentage_wt)
{
  uint16_t Temp_Status = 0;

  if((Flags_struct.Error_flags & NEGATIVE_RATE_ERR))
  {
    if((Load_in_percentage_wt == 0)||(Load_in_percentage_wt == 25) || (Load_in_percentage_wt == 100))
    {
      Temp_Status = 1;
    }
  }
  else if((Load_in_percentage_wt == 50)||(Load_in_percentage_wt == 75))
  {
      Temp_Status = 1;
  }
  return(Temp_Status);
}

/*****************************************************************************
* @note       Function name: uint16_t Update_DO_RLY_Outputs (char Output_number,char* Output_Func, char* Speed_Output_Type, uint16_t Speed_MinMax_Setpoint,
                                char* Rate_Output_Type, uint16_t Rate_MinMax_Setpoint, float Load_in_percentage, char* Error_Alarm_Type)
* @returns    returns      : Output status (ON or OFF)
*                              //3 bits are assign. Truth Table
*                              bit 2 1 0
*                                  0 0 0   Normal digital output off
*                                  0 0 1   Normal digital output on
*                                  0 1 1   Pulsed output on
*                                  1 0 1   Quadrature output for load 50% and 100%
*                                  1 1 1   Quadrature Output for load 25% and 75%
*
* @param      arg1         : Output functionality assigned by user
* @param      arg2         : If OP is speed, Speed type (Min or Max)
* @param      arg3         : Speed set point
* @param      arg4         : If output is Rate, Rate typr (Min or Max)
* @param      arg5         : Rate set point
* @author                    : Shweta Pimple
* @date       date created : Jan 7,2013
* @brief      Description  : Update the outputs status (Digital and Relay)
* @note       Notes        : None
*****************************************************************************/
uint16_t Update_DO_RLY_Outputs (char Output_number,unsigned int Output_Func, unsigned int Speed_Output_Type, uint16_t Speed_MinMax_Setpoint,
                               unsigned int Rate_Output_Type, uint16_t Rate_MinMax_Setpoint, unsigned int Error_Alarm_Type)
{
  uint16_t Status = 0;

  if(Output_Func == PULSED_OUTPUT_STR)    /*"Pulsed Output"*/
  {
      if (Output_number == 1)
      {
        //Pulsed Output
        if(IO_board_param.Quadrature_Wave_Setting == 11)
        {
            Status = 2;
            //check the load modulus of weight per pulse
            if(IO_board_param.Pulsed_Output_Done_Flag & 0x01)
            {
						  IO_board_param.Pulsed_Output_Done_Flag &= ~(0x01);
							//ZeroedHighPulse++;
							Status = 3;
            }
        }
        //Output-1 for Pulse - CH-A and output -2 for Quadrature Wave-CH-B
        else if(IO_board_param.Quadrature_Wave_Setting == 21)
        {
          Status = Check_for_Pluse_output_Wt(IO_board_param.Load_in_percentage);
        }
        //Output-1 for Pulse - CH-A and output -3 for Quadrature Wave-CH-B
        else if(IO_board_param.Quadrature_Wave_Setting == 31)
        {
          Status = Check_for_Pluse_output_Wt(IO_board_param.Load_in_percentage);
        }
      }
      //check the load modulus of weight per pulse
      else if (Output_number == 2)
      {
        //Pulsed Output
        if(IO_board_param.Quadrature_Wave_Setting == 11)
        {
           Status = 2;
           //check the load modulus of weight per pulse
           if(IO_board_param.Pulsed_Output_Done_Flag & 0x02)
           {
						 IO_board_param.Pulsed_Output_Done_Flag &= ~(0x02);
						 Status = 3;
           }
        }
        //Output-2 for Pulse - CH-A and output -1 for Quadrature Wave-CH-B
        else if(IO_board_param.Quadrature_Wave_Setting == 12)
        {
            Status = Check_for_Pluse_output_Wt(IO_board_param.Load_in_percentage);
        }
        //Output-2 for Pulse - CH-A and output -3 for Quadrature Wave-CH-B
        else if(IO_board_param.Quadrature_Wave_Setting == 32)
        {
          Status = Check_for_Pluse_output_Wt(IO_board_param.Load_in_percentage);
        }
      }
      //check the load modulus of weight per pulse
      else if (Output_number == 3)
      {
        //Pulsed Output
        if(IO_board_param.Quadrature_Wave_Setting == 11)
        {
           Status = 2;
           //check the load modulus of weight per pulse
           if(IO_board_param.Pulsed_Output_Done_Flag & 0x04)
           {
						 IO_board_param.Pulsed_Output_Done_Flag &= ~(0x04);
						 Status = 3;
           }
        }
        //Output-3 for Pulse - CH-A and output -1 for Quadrature Wave-CH-B
        else if(IO_board_param.Quadrature_Wave_Setting == 13)
        {
          Status = Check_for_Pluse_output_Wt(IO_board_param.Load_in_percentage);
        }
        //Output-3 for Pulse - CH-A and output -2 for Quadrature Wave-CH-B
        else if(IO_board_param.Quadrature_Wave_Setting == 23)
        {
          Status = Check_for_Pluse_output_Wt(IO_board_param.Load_in_percentage);
        }

      }
  }
  else if(Output_Func == QUADRATURE_WAVE_STR)  /*"Quadrature Wave"*/
  {
    //Genrate CH - B if IO_board_param.Quadrature_Wave_Setting = 0 means
    //any two output config for Quadrature Wave then invalid - ignore i
    if(IO_board_param.Quadrature_Wave_Setting != 0)
    {
       //settings will be same for negative and positive rate for chB
       if((IO_board_param.Load_in_percentage == 0)||(IO_board_param.Load_in_percentage == 75)||(IO_board_param.Load_in_percentage == 100))
       {
         Status = 1;
       } 
    }
  }
  else if( Output_Func == EEROR_ALARM_STR)  /*"Error Alarm"*/
  {
		if(IO_board_param.Error_ack_flag == __FALSE)
		{
      //check for which output is configured as error alarm output
      if(Error_Alarm_Type == LOAD_CELL_ERR_STR)  /*"Load cell"*/
      {
            if(Flags_struct.Error_flags & LOAD_CELL_DATA_ERR)
            {
                Status = 1;
            }
      }
      else if( Error_Alarm_Type == ANGLE_SENSOR_ERR_STR)    /*"Angle sensor"*/
      {
            if(Flags_struct.Error_flags & ANGLE_SENSOR_ERR)
            {
                Status = 1;
            }
      }
      else if(Error_Alarm_Type == COMMUNICATION_ERR_STR) /*"Communications"*/
      {
            if((Flags_struct.Error_flags & SEN_BRD_COMM_ERR) ||
               (Flags_struct.Error_flags & SCALE_COMM_ERR))
            {
                Status = 1;
            }
      }
      else if(Error_Alarm_Type == NEGATIVE_RATE_ERR_STR)  /*"Negative Rate"*/
      {
            if(Flags_struct.Error_flags & NEGATIVE_RATE_ERR)
            {
                Status = 1;
            }
      }
      else if( Error_Alarm_Type == ANY_ERROR_STR)  /*"Any Error"*/
      {
            if((Flags_struct.Error_flags & LOAD_CELL_DATA_ERR) ||
               (Flags_struct.Error_flags & ANGLE_SENSOR_ERR) ||
               (Flags_struct.Error_flags & SEN_BRD_COMM_ERR) ||
               (Flags_struct.Error_flags & SCALE_COMM_ERR) ||
               (Flags_struct.Error_flags & NEGATIVE_RATE_ERR))
            {
                Status = 1;
            }
      }
		}
		else
		{
			Status = 0;
		}
			 
  }
  else if( Output_Func == MIN_MAX_SPEED_STR)    /*"Min/Max Speed"*/
  {
      if( Speed_Output_Type == MIN_SPEED_OUTPUT_STR)     /*"Min Speed Output"*/
      {
          if(Calculation_struct.Belt_speed <= Speed_MinMax_Setpoint)
          {
              Status = 1;
          }
      }
      else if( Speed_Output_Type == MAX_SPEED_OUTPUT_STR)    /*"Max Speed Output"*/
      {
          if(Calculation_struct.Belt_speed >= Speed_MinMax_Setpoint)
          {
              Status = 1;
          }
      }
  }
  else if( Output_Func == MIN_MAX_RATE_STR)                /*"Min/Max Rate"*/
  {
      if(Rate_Output_Type == MIN_RATE_OUTPUT_STR)            /*"Min Rate Output"*/
      {
        //By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152  
				//if(Calculation_struct.Rate <= Rate_MinMax_Setpoint)
				  if(Calculation_struct.Rate_for_display <= Rate_MinMax_Setpoint)
          {
              Status = 1;
          }
      }
      else if(Rate_Output_Type == MAX_RATE_OUTPUT_STR)        /*"Max Rate Output"*/
      {
         // if(Calculation_struct.Rate >= Rate_MinMax_Setpoint)
				   if(Calculation_struct.Rate_for_display >= Rate_MinMax_Setpoint)
          {
              Status = 1;
          }
      }
  }
  else if(Output_Func == BATCHING_LOADOUT_STR)                  /*"Batching/Loadout"*/
  {
      if(IO_board_param.Batching_Flag == BATCH_COMPLETE)
      {
          //Turn off the output and stops the feeder
          Status = 0;					
      }
      else if(IO_board_param.Batching_Flag == BATCH_START)
      {
          Status = 1; //start the feeder for the batch
      }
			else
			{
				  Status = 0;
			}
  }
  else if(Output_Func == Screen4421_str20)      /*"Zero Calibration"*/
  {
      IO_board_param.Inst_Zero_Cal_Flag = Calib_struct.Cal_status_flag;
      if((IO_board_param.Inst_Zero_Cal_Flag == START_STATICZERO_CAL)
         ||(IO_board_param.Inst_Zero_Cal_Flag == START_DYNAMICZERO_CAL))

       {
         Status = 1;
       }
  }
  return(Status);
}

/*****************************************************************************
* @note       Function name: uint16_t Get_RawCount_For_AO (void)
* @returns    returns      : Raw count for Analog output
* @param      arg1         : Channel (1 or 2)
* @author                  : Shweta Pimple
* @date       date created : Jan 7,2013
* @brief      Description  : Get raw count for DAC
* @note       Notes        : None
*****************************************************************************/
uint16_t Get_RawCount_For_AO (uint8_t AO_Ch)
{
  uint16_t DAC_Count = 0;
  float Processing_var;
  float  Iout = 0;
  //Processing_var = Calculation_struct.Rate;
	Processing_var = Calculation_struct.Rate_for_display;

  switch(AO_Ch)
  {
      case 1:
          //Output 0-20mA
			    if(Setup_device_var.Analog_Output_1_Function == Port_string3)
					{
						 DAC_Count =0;
						 // Theoritical Analog Current output for Diagnosis Purpose
						 Setup_device_var.Analog_Output1 = 0; 
						 //Analog ch-0, set 13thbit for 0-20mA and reset it for 4-20mA
             DAC_Count = (DAC_Count | (1 << 12));
					}
          else if(Setup_device_var.Analog_Output_1_Function == Screen44411_str2)                                                                                      /*"0-20mA"*/
          {
              //Iout = (20mA / 4096)* count
              //first calculate the scaling factor.
              //setpoint % of maxrate will be the full scale value for analog output
              /*max_rate = (Setup_device_var.Analog_Output_1_Maxrate * Setup_device_var. Analog_Output_1_Setpoint) / 100;

              Scaling_Factor = max_rate / 20.0; //

              Iout = (Processing_var / Scaling_Factor);

              DAC_Count = (uint16_t)((Iout * DAC_FULL_SCALE_COUNT)/ 20.0);*/
              #ifdef RATE_BLEND_LOAD_CTRL_CALC
              if ((((Rate_blend_load.Calculation_flags & RATE_CONTROL_SCREEN_FLAG) == RATE_CONTROL_SCREEN_FLAG) ||
                  ((Rate_blend_load.Calculation_flags & BLEND_CONTROL_SCREEN_FLAG) == BLEND_CONTROL_SCREEN_FLAG)) &&
							     (Setup_device_var.PID_Channel == 1))
              {
									 //DAC_Count =  (((uint16_t)Rate_blend_load.DAC_count)/Setup_device_var.Analog_Output_1_Maxrate) * (DAC_FULL_SCALE_COUNT+1) ;
									if(Setup_device_var.PID_Action == Screen4512_str1) // Reverse Action
									{
										DAC_Count =  ((uint16_t)Rate_blend_load.DAC_count) - (Setup_device_var.Analog_Output_1_ZeroCal/SCALING_FACTOR_0_20MA);
									}
									else // Forward Action
									{
										DAC_Count =  (DAC_FULL_SCALE_COUNT) - ((uint16_t)Rate_blend_load.DAC_count -(Setup_device_var.Analog_Output_1_ZeroCal/SCALING_FACTOR_0_20MA));
									}
              }
              else
              {
							   Iout = ((Processing_var * Setup_device_var. Analog_Output_1_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_1_Maxrate));
							
							   DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT) -(Setup_device_var.Analog_Output_1_ZeroCal/SCALING_FACTOR_0_20MA) ;
              }
              #else
							Iout = (Processing_var * Setup_device_var. Analog_Output_1_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_1_Maxrate);
							
							DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT);
              #endif

							Rprts_diag_var.Analog_Output_1_Status = Port_string1; //on
              if(DAC_Count > DAC_FULL_SCALE_COUNT)
              {
                DAC_Count = DAC_FULL_SCALE_COUNT;
              }
							// Theoritical Analog Current output for Diagnosis Purpose
							Setup_device_var.Analog_Output1 = SCALING_FACTOR_0_20MA*DAC_Count + Setup_device_var.Analog_Output_1_ZeroCal; 
              //Analog ch-0, set 13thbit for 0-20mA and reset it for 4-20mA
              DAC_Count = (DAC_Count | (1 << 12));
							
          }
          //Output 4-20mA
          else if(Setup_device_var.Analog_Output_1_Function == Screen44411_str5)                                                                                      /*"4-20mA"*/
          {
              //Iout = (16mA/ 4096) * count +  4mA
              //first calculate the scaling factor.
              //setpoint % of maxrate will be the full scale value for analog output
              /*max_rate = (Setup_device_var.Analog_Output_1_Maxrate * Setup_device_var. Analog_Output_1_Setpoint) / 100;

              Scaling_Factor = (max_rate / 16.0); //

              Iout = (Processing_var / Scaling_Factor);

              DAC_Count =  (uint16_t)(Iout * DAC_FULL_SCALE_COUNT / 16.0);*/
              #ifdef RATE_BLEND_LOAD_CTRL_CALC
              if ((((Rate_blend_load.Calculation_flags & RATE_CONTROL_SCREEN_FLAG) == RATE_CONTROL_SCREEN_FLAG) ||
                  ((Rate_blend_load.Calculation_flags & BLEND_CONTROL_SCREEN_FLAG) == BLEND_CONTROL_SCREEN_FLAG)) &&
							    (Setup_device_var.PID_Channel == 1))
              {
										//DAC_Count =  (((uint16_t)Rate_blend_load.DAC_count)/Setup_device_var.Analog_Output_1_Maxrate) * (DAC_FULL_SCALE_COUNT+1) ;
									if(Setup_device_var.PID_Action == Screen4512_str1) // Reverse Action
									{
										DAC_Count =  ((uint16_t)Rate_blend_load.DAC_count) - (Setup_device_var.Analog_Output_1_ZeroCal/SCALING_FACTOR_4_20MA);
									}
									else // Forward Action
									{
										DAC_Count =  (DAC_FULL_SCALE_COUNT) - ((uint16_t)Rate_blend_load.DAC_count - (Setup_device_var.Analog_Output_1_ZeroCal/SCALING_FACTOR_4_20MA));
									}
              }
              else
              {

							   Iout = ((Processing_var * Setup_device_var. Analog_Output_1_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_1_Maxrate));
							
							   DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT) - (Setup_device_var.Analog_Output_1_ZeroCal/SCALING_FACTOR_4_20MA);
              }
              #else
							 Iout = (Processing_var * Setup_device_var. Analog_Output_1_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_1_Maxrate);
							
							 DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT);
              #endif

              Rprts_diag_var.Analog_Output_1_Status = Port_string1; //on
              if(DAC_Count > DAC_FULL_SCALE_COUNT)
              {
                DAC_Count = DAC_FULL_SCALE_COUNT;
              }
							// Theoritical Analog Current output for Diagnosis Purpose
							Setup_device_var.Analog_Output1 = SCALING_FACTOR_4_20MA*DAC_Count + 4 + Setup_device_var.Analog_Output_1_ZeroCal; 
          }
      break;

      case 2:
				  if(Setup_device_var.Analog_Output_2_Function == Port_string3)
					{
						 DAC_Count =0;
						 // Theoritical Analog Current output for Diagnosis Purpose
						 Setup_device_var.Analog_Output2 = 0; 
						 //Analog ch-0, set 12thbit for 0-20mA and reset it for 4-20mA
             DAC_Count = (DAC_Count | (1 << 12));
					}
          else if(Setup_device_var.Analog_Output_2_Function == Screen44421_str2)                                                                                      /*"0-20mA"*/
          {
              //Iout = (20mA / 4096)* count
              /*max_rate = (Setup_device_var.Analog_Output_2_Maxrate * Setup_device_var. Analog_Output_2_Setpoint) / 100;
              Scaling_Factor = (max_rate / 20.0); //

              Iout = (Processing_var / Scaling_Factor);

              DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT / 20.0);*/
              #ifdef RATE_BLEND_LOAD_CTRL_CALC
              if ((((Rate_blend_load.Calculation_flags & RATE_CONTROL_SCREEN_FLAG) == RATE_CONTROL_SCREEN_FLAG) ||
                  ((Rate_blend_load.Calculation_flags & BLEND_CONTROL_SCREEN_FLAG) == BLEND_CONTROL_SCREEN_FLAG)) &&
							     (Setup_device_var.PID_Channel == 2))
              {
										 //DAC_Count =  (((uint16_t)Rate_blend_load.DAC_count)/Setup_device_var.Analog_Output_1_Maxrate) * (DAC_FULL_SCALE_COUNT+1) ;
									if(Setup_device_var.PID_Action == Screen4512_str1) // Reverse Action
									{
										DAC_Count =  ((uint16_t)Rate_blend_load.DAC_count) - (Setup_device_var.Analog_Output_2_ZeroCal/SCALING_FACTOR_0_20MA);
									}
									else // Forward Action
									{
										DAC_Count =  (DAC_FULL_SCALE_COUNT) - ((uint16_t)Rate_blend_load.DAC_count - (Setup_device_var.Analog_Output_2_ZeroCal/SCALING_FACTOR_0_20MA));
									}
							}
              else
              {
							   Iout = ((Processing_var * Setup_device_var. Analog_Output_2_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_2_Maxrate));
							
							   DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT) - (Setup_device_var.Analog_Output_2_ZeroCal/SCALING_FACTOR_0_20MA);
              }
              #else
              /*DAC_Count = (uint16_t)(Processing_var * Setup_device_var. Analog_Output_2_Setpoint * \
								                       DAC_FULL_SCALE_COUNT) / \
                                    (100 * Setup_device_var.Analog_Output_2_Maxrate);*/
							   Iout = (Processing_var * Setup_device_var. Analog_Output_2_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_2_Maxrate);
							
							   DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT);
              #endif

              Rprts_diag_var.Analog_Output_2_Status = Port_string1; //on
              if(DAC_Count > DAC_FULL_SCALE_COUNT)
              {
                DAC_Count = DAC_FULL_SCALE_COUNT;
              }
							// Theoritical Analog Current output for Diagnosis Purpose
							Setup_device_var.Analog_Output2 = SCALING_FACTOR_0_20MA*DAC_Count + Setup_device_var.Analog_Output_2_ZeroCal; 
              //Analog ch-0, set 13th-bit for 0-20mA and reset it for 4-20mA
              DAC_Count = (DAC_Count | (1 << 12));
							
          }
          else if(Setup_device_var.Analog_Output_2_Function == Screen44421_str5)                                                                                      /*"4-20mA"*/
          {
              //Iout = (16mA/ 4096) * count +  4mA
              /*max_rate = (Setup_device_var.Analog_Output_2_Maxrate * Setup_device_var. Analog_Output_2_Setpoint) / 100;
              Scaling_Factor = (max_rate / 16.0); //

              Iout = (Processing_var / Scaling_Factor);

              DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT / 16.0);*/
              #ifdef RATE_BLEND_LOAD_CTRL_CALC
              if ((((Rate_blend_load.Calculation_flags & RATE_CONTROL_SCREEN_FLAG) == RATE_CONTROL_SCREEN_FLAG) ||
                  ((Rate_blend_load.Calculation_flags & BLEND_CONTROL_SCREEN_FLAG) == BLEND_CONTROL_SCREEN_FLAG)) &&
							     (Setup_device_var.PID_Channel == 2))
              {
										 //DAC_Count =  (((uint16_t)Rate_blend_load.DAC_count)/Setup_device_var.Analog_Output_1_Maxrate) * (DAC_FULL_SCALE_COUNT+1) ;
									if(Setup_device_var.PID_Action == Screen4512_str1) // Reverse Action
									{
										DAC_Count =  ((uint16_t)Rate_blend_load.DAC_count) - (Setup_device_var.Analog_Output_2_ZeroCal/SCALING_FACTOR_4_20MA);
									}
									else // Forward Action
									{
										DAC_Count =  (DAC_FULL_SCALE_COUNT) - ((uint16_t)Rate_blend_load.DAC_count - (Setup_device_var.Analog_Output_2_ZeroCal/SCALING_FACTOR_4_20MA));
									}
              }
              else
              {
							   Iout = ((Processing_var * Setup_device_var. Analog_Output_2_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_2_Maxrate));
							
							   DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT) - (Setup_device_var.Analog_Output_2_ZeroCal/SCALING_FACTOR_4_20MA);
              }
              #else
							   Iout = (Processing_var * Setup_device_var. Analog_Output_2_Setpoint) / \
                                    (100 * Setup_device_var.Analog_Output_2_Maxrate);
							
							   DAC_Count = (uint16_t)(Iout * DAC_FULL_SCALE_COUNT);
              #endif

              Rprts_diag_var.Analog_Output_2_Status = Port_string1; //on
              if(DAC_Count > DAC_FULL_SCALE_COUNT)
              {
                DAC_Count = DAC_FULL_SCALE_COUNT;
              }
							// Theoritical Analog Current output for Diagnosis Purpose
							Setup_device_var.Analog_Output2 = SCALING_FACTOR_4_20MA*DAC_Count + 4 + Setup_device_var.Analog_Output_2_ZeroCal; 
          }
      break;
  }
  return DAC_Count;
}

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
