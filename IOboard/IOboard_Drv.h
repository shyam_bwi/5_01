/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : IOboard.h
* @brief               : IO Board
*
* @author              : Shweta Pimple
*
* @date Created        : December 19, 2012
* @date Last Modified  : December 19, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __IOBOARD_DRV_H
#define __IOBOARD_DRV_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

#ifdef IOBOARD
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

#define _SSP_NO_USING       0

#if (_SSP_NO_USING == 0)
#define LPC_SSP         (LPC_SSP0)
#define SSP_IRQn        SSP0_IRQn
#define SSP_IRQHandler  SSP0_IRQHandler
#endif

#define SSP_PORT                      2
#define SSP_SCK_GPIO                  22
#define SSP_SSEL_GPIO                 23
#define SSP_MISO_GPIO                 26
#define SSP_MOSI_GPIO                 27
#define FUNC_SSP                      2


/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern uint8_t complete;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

enum SPI_MODE {
   INIT = 0,
   MASTER,
   SLAVE
};

/*============================================================================
* Public Function Declarations
*===========================================================================*/
void Init_IOboard(uint8_t SSP_State, uint8_t *pu8SspTxBuf, uint8_t *pu8SspRxBuf, uint8_t u8DataLength);
int8_t Ioboard_send_frame (uint8_t u8DatLen);

#endif /*#ifdef IOBOARD*/

#endif /*__IOBOARD_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
