/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : IOboard_App.h
* @brief               : IO Board application layer for communication protocol
*
* @author              : Shweta Pimple
*
* @date Created        : December 21, 2012
* @date Last Modified  : December 21, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*
*****************************************************************************/
#ifndef __IOBOARDAPP_H
#define __IOBOARDAPP_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#ifdef IOBOARD
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define SOF                            0x5AA5
#define DEVICE_ID                      1

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct RequestPara {
   uint16_t Sof; //Start of Frame
   uint8_t DeviceId;
   uint16_t PulseOnTime1;
   uint16_t PulseOnTime2;
   uint16_t PulseOnTime3;
   uint16_t AnalogCh1;
   uint16_t AnalogCh2;
   uint16_t DigitalOP;
   uint8_t RelayOP;
   uint16_t checksum;
	 //uint16_t PulseCount;
} REQUEST_PARA;

typedef struct ResponsePara {
   uint16_t Sof; //Start of Frame
   uint8_t DeviceId;
   uint8_t DigitalIP;
   int16_t ResponseCode;
	 short Firm_ver[2];
   uint16_t checksum;
} RESPONSE_PARA;

typedef struct Output_struct {
   char* Output_Func;
   uint8_t Quad_OP_1;
   uint8_t Quad_OP_2;
   char* error_alarm_type;
   char* Speed_Output_Type;
   uint16_t Speed_MinMax_Setpoint;
   char* Rate_Output_Type;
   uint16_t Rate_MinMax_Setpoint;
} IOBOARD_OUTPUT;


typedef struct Bitfield{
  uint8_t bit0 : 1;
  uint8_t bit1 : 1;
  uint8_t bit2 : 1;
  uint8_t bit3 : 1;
  uint8_t bit4 : 1;
  uint8_t bit5 : 1;
  uint8_t bit6 : 1;
  uint8_t bit7 : 1;
} BIT_FIELD;

enum SSP_STATUS {
   CLR = 0,
   DONE,
   ERR
};

enum IOBOARD_STATUS {

  IO_GN_ERROR = -2,
  CRC_ERR = -1,
  TIMEOUT_ERR = 0,
  SOF_ERR,
  DEV_ID_ERR,
  HEALTHY
};

extern RESPONSE_PARA IoResponse_Para;
extern REQUEST_PARA IoRequest_Para;
extern U8 Previous_DigitalIP;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
__task void ioboard_task (void);

#endif /*#ifdef IOBOARD*/

#endif /*__IOBOARD_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
