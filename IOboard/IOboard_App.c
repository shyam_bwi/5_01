/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : IOboard_App.c
* @brief               : IO Board application layer for communication protocol
*
* @author              : Shweta Pimple
*
* @date Created        : December 21, 2012
* @date Last Modified  : Feb 12, 2014
* @Modified by				 : Venkata Krishna rao
* @internal Change Log : <YYYY-MM-DD>
* @internal            : Changes made related to Digital Inputs and Analog Outputs Diaganostic Variables.
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

#include "IOboard_App.h"
#include "IOboard_Drv.h"
#include "Screen_global_ex.h"
#include "IOProcessing.h"
#include "Sensor_board_data_process.h"
#include "Calibration.h"
#include "Log_report_data_calculate.h"
#include "Screen_data_enum.h"
#include "Rate_blending_load_ctrl_calc.h"
#include <RTL.h>
#ifdef IOBOARD
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define CHANNEL1        1
#define CHANNEL2        2

#define PIN2_19_STATUS  0x00080000
#define PIN2_19_CONFIG  0x00000008
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
U8 Previous_DigitalIP = 0,u8Resend = 0;
char IOboard_fw_ver[6];
int SentPulseCount = 0,ReceivedPkts=0;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
RESPONSE_PARA IoResponse_Para;
REQUEST_PARA IoRequest_Para;
int PulseHigh1,PulseHigh2,PulseHigh3,ZeroedHighPulse=0;

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void Set_IO_board_input_output_relay_status(unsigned int, unsigned int* status, uint16_t on_off);
void Get_Weight_Per_Pulse(void);
void Find_Weight_Per_Pulse(char Output_no, unsigned int Digital_Output_Func,
                           unsigned int Digital_Output_Units_Per_Pulse, double *pOutput_Delivered_wt);

void Get_Batch_Complete_Status(void);
void Check_for_Quadrature_Wave_Setting(void);

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*-------------------------PRIVATE FUNCTIONS------------------------------*/
/*****************************************************************************
* @note       Function name : Set_IO_board_input_output_relay_status
* @returns    returns       : None
* @param      arg1          : Output, Input or relay function assignment
* @param      arg2          : status bit of the respective function
*             arg3          : status if on or off
* @author                   : Anagha Basole
* @date       date created  : 12/07/2012
* @brief      Description   : Sets the status flag for the input/output/relay functionality
* @note       Notes         : None
*****************************************************************************/
void Set_IO_board_input_output_relay_status(unsigned int function_name, unsigned int* status, uint16_t on_off)
{
    //check if the output/input/relay is unassigned. If it is unassigned then status is off
    //otherwise status is as per the input/output/relay status.
    if (function_name == Port_string3) //Unassigned
    {
        *status = Port_string2; //"Off"
    }
    else
    {
        if(IO_board_param.Status_displayed_flag == __TRUE)
        {
            if (on_off == 1)
            {
                *status = Port_string1; //"On"
            }
            else
            {
                *status = Port_string2; //"Off"
            }
        }
    }
    return;
}

/*****************************************************************************
* @note       Function name : uint16_t CalculateCRC(uint8_t *StartBuffer, uint16_t Length)
* @returns    returns       : Calculated CRC
* @param      arg1          : Buffer start address
* @param      arg2          : length of the data
* @author                   : Viren Dobariya
* @date       date  Created : 12/07/2012
* @brief      Description   : Calculate CRC
* @note       Notes         : None
*****************************************************************************/
uint16_t CalculateCRC(uint8_t *StartBuffer, uint16_t Length)
{
    /*Optimized CRC-16 calculation.*/

    /*Polynomial: x^16 + x^15 + x^2 + 1 (0xa001)*/
    /*Initial value: 0xffff*/
    /* This CRC is normally used in disk-drive controllers.*/

    uint8_t test_cnt;
    uint16_t CntIndex;
    uint16_t uiCRC_Byte;

    uiCRC_Byte = 0xFFFF;

    for(CntIndex = 0; CntIndex < Length; CntIndex++)
    {
        //EXOR word constant with CRC_test
        uiCRC_Byte = uiCRC_Byte  ^ *StartBuffer;

        // Test for all 16 - bits
        for(test_cnt=16; test_cnt !=0; test_cnt--)
        {
            if( uiCRC_Byte & 0x0001)
            {
                uiCRC_Byte ^= 0xA001;  //CRC_polynomial ; //A001
            }
            uiCRC_Byte = (uiCRC_Byte>>1);
        }

        StartBuffer++;
    }
    return(uiCRC_Byte);
}

/*****************************************************************************
* @note       Function name : void Prepare_Request_frame (void)
* @returns    returns       : None
* @param      arg1          : None
* @param      arg2          : None
* @author                   : Shweta Pimple
* @date       date  Created : 01/02/2013
* @brief      Description   : Build the request frame for IO board
* @note       Notes         : None
*****************************************************************************/
void Prepare_Request_frame (void)
{
    IoRequest_Para.Sof = SOF;
    IoRequest_Para.DeviceId = DEVICE_ID;

    //Update analog output
    IoRequest_Para.AnalogCh1 = Get_RawCount_For_AO(CHANNEL1);
    IoRequest_Para.AnalogCh2 = Get_RawCount_For_AO(CHANNEL2);
    if(Rate_blend_load.packet_cnt >= 4)
    {
        Rate_blend_load.packet_cnt = 0;
        Rate_blend_load.actual_weight = 0;
        Rate_blend_load.Delta_time = 0;
    }


    //Update Digital Output1
    //Update Digital Output1
    if(u8Resend == 0)
    {
        IoRequest_Para.DigitalOP = Update_DO_RLY_Outputs (1, Setup_device_var.Digital_Output_1_Function, \
                                   Setup_device_var.Speed_Output_Type_1, \
                                   Setup_device_var.MinMax_Speed_Setpoint_1, \
                                   Setup_device_var.Rate_Output_Type_1, \
                                   Setup_device_var.MinMax_Rate_Setpoint_1, \
                                   Setup_device_var.error_alarm_1);
    }
    Set_IO_board_input_output_relay_status(Setup_device_var.Digital_Output_1_Function, \
                                           &Rprts_diag_var.Digital_Output_1_Status, \
                                           (IoRequest_Para.DigitalOP & 0x0001));
    SentPulseCount++;
    //check for pulsed output
    if(IoRequest_Para.DigitalOP == 3)
    {
        //update the pulse on time
        IoRequest_Para.PulseOnTime1 = Setup_device_var.Pulse_on_time_1;
    }

    //Update Digital Output2
    if(u8Resend == 0)
    {
        IoRequest_Para.DigitalOP |= ((Update_DO_RLY_Outputs (2, Setup_device_var.Digital_Output_2_Function,  \
                                      Setup_device_var.Speed_Output_Type_2, \
                                      Setup_device_var.MinMax_Speed_Setpoint_2, \
                                      Setup_device_var.Rate_Output_Type_2, \
                                      Setup_device_var.MinMax_Rate_Setpoint_2, \
                                      Setup_device_var.error_alarm_2)) << 4);
    }
    Set_IO_board_input_output_relay_status(Setup_device_var.Digital_Output_2_Function, \
                                           &Rprts_diag_var.Digital_Output_2_Status, \
                                           ((IoRequest_Para.DigitalOP & 0x0010)>>4));

    //check for pulsed output
    if((IoRequest_Para.DigitalOP & 0x30) == 0x30)
    {
        //update the pulse on time
        IoRequest_Para.PulseOnTime2 = Setup_device_var.Pulse_on_time_2;
    }

    if(u8Resend == 0)
    {
        //Update Digital Output3
        IoRequest_Para.DigitalOP |= ((Update_DO_RLY_Outputs (3, Setup_device_var.Digital_Output_3_Function,  \
                                      Setup_device_var.Speed_Output_Type_3, \
                                      Setup_device_var.MinMax_Speed_Setpoint_3, \
                                      Setup_device_var.Rate_Output_Type_3, \
                                      Setup_device_var.MinMax_Rate_Setpoint_3, \
                                      Setup_device_var.error_alarm_3)) << 8);
    }
    Set_IO_board_input_output_relay_status(Setup_device_var.Digital_Output_3_Function, \
                                           &Rprts_diag_var.Digital_Output_3_Status, \
                                           ((IoRequest_Para.DigitalOP & 0x0100)>>8));

    //check for pulsed output
    if((IoRequest_Para.DigitalOP & 0x300) == 0x300)
    {
        //update the pulse on time
        IoRequest_Para.PulseOnTime3 = Setup_device_var.Pulse_on_time_3;
    }

    //Update Relay Output1
    IoRequest_Para.RelayOP = Update_DO_RLY_Outputs (4, Setup_device_var.Relay_Output_1_Function, \
                             Setup_device_var.Speed_Output_Type_4, \
                             Setup_device_var.MinMax_Speed_Setpoint_4, \
                             Setup_device_var.Rate_Output_Type_4, \
                             Setup_device_var.MinMax_Rate_Setpoint_4, \
                             Setup_device_var.error_alarm_4);

    Set_IO_board_input_output_relay_status(Setup_device_var.Relay_Output_1_Function, \
                                           &Rprts_diag_var.Relay_Output_1_Status, \
                                           (IoRequest_Para.RelayOP & 0x0001));

    //Update Relay Output2
    IoRequest_Para.RelayOP |= ((Update_DO_RLY_Outputs (5, Setup_device_var.Relay_Output_2_Function,  \
                                Setup_device_var.Speed_Output_Type_5, \
                                Setup_device_var.MinMax_Speed_Setpoint_5, \
                                Setup_device_var.Rate_Output_Type_5, \
                                Setup_device_var.MinMax_Rate_Setpoint_5, \
                                Setup_device_var.error_alarm_5)) << 1);

    Set_IO_board_input_output_relay_status(Setup_device_var.Relay_Output_2_Function, \
                                           &Rprts_diag_var.Relay_Output_2_Status, \
                                           ((IoRequest_Para.RelayOP & 0x0002)>>1));

    //Update Relay Output3
    IoRequest_Para.RelayOP |= ((Update_DO_RLY_Outputs (6, Setup_device_var.Relay_Output_3_Function,  \
                                Setup_device_var.Speed_Output_Type_6, \
                                Setup_device_var.MinMax_Speed_Setpoint_6, \
                                Setup_device_var.Rate_Output_Type_6, \
                                Setup_device_var.MinMax_Rate_Setpoint_6, \
                                Setup_device_var.error_alarm_6)) << 2);

    Set_IO_board_input_output_relay_status(Setup_device_var.Relay_Output_3_Function, \
                                           &Rprts_diag_var.Relay_Output_3_Status, \
                                           ((IoRequest_Para.RelayOP & 0x0004)>>2));

    IoRequest_Para.checksum = CalculateCRC((uint8_t *)&(IoRequest_Para), sizeof(IoRequest_Para)-2);

    return;
}

/*****************************************************************************
* @note       Function name : void Init_IOboard_struct(void)
* @returns    returns       : None
* @param      arg1          : None
* @author                   : Shweta Pimple
* @date       date created  : 20 December 2012
* @brief      Description   : Initialize the IO board application layer structure
* @note       Notes         : None
*****************************************************************************/
void Init_IOboard_struct(void)
{
    memset((uint8_t*)&IoRequest_Para, 0,(sizeof(IoRequest_Para)));
    memset((uint8_t*)&IoResponse_Para, 0, (sizeof(IoResponse_Para)));
    memset((uint8_t*)&IO_board_param, 0, (sizeof(IO_board_param)));
    IO_board_param.Quadrature_Wave_Setting = 0xFF;
    return;
}
/*****************************************************************************
* @note       Function name : void Get_Weight_Per_Pulse(void)
* @returns    returns       : None
* @param      arg1          : None
* @author                   :
* @date       date created  : 25 Apr 2013
* @brief      Description   : Get the computed value of Weight_Per_Pulse
* @note       Notes         : None
*****************************************************************************/
void Get_Weight_Per_Pulse(void)
{
    Find_Weight_Per_Pulse(1,Setup_device_var.Digital_Output_1_Function, Setup_device_var.Units_Per_Pulse_1,
                          &IO_board_param.Delivered_weight_OP1);

    Find_Weight_Per_Pulse(2,Setup_device_var.Digital_Output_2_Function, Setup_device_var.Units_Per_Pulse_2,
                          &IO_board_param.Delivered_weight_OP2);

    Find_Weight_Per_Pulse(3,Setup_device_var.Digital_Output_3_Function, Setup_device_var.Units_Per_Pulse_3,
                          &IO_board_param.Delivered_weight_OP3);
}

/*****************************************************************************
* @note       Function name : void Process_Pulse_Output(char Output_no, float New_Wt_Per_Pulse, float *pOutput_Delivered_wt)
* @returns    returns       : None
* @param      arg1          : None
* @author                   :
* @date       date created  : 20 Mar 2014
* @brief      Description   :
* @note       Notes         : None
*****************************************************************************/
void Process_Pulse_Output(char Output_no, float New_Wt_Per_Pulse, double *pOutput_Delivered_wt)
{
    double Delivered_wt = 0;

    IO_board_param.Pulsed_Output_Done_Flag &= ~(1 << (Output_no - 1));

    Delivered_wt = (*pOutput_Delivered_wt) * Calculation_struct.Weight_conv_factor;

    //	*pOutput_Delivered_wt = *pOutput_Delivered_wt + IO_board_param.Delivered_weight_Net;

    if(Delivered_wt >= New_Wt_Per_Pulse)
    {

        *pOutput_Delivered_wt = *pOutput_Delivered_wt - (New_Wt_Per_Pulse / Calculation_struct.Weight_conv_factor);

        if(Output_no == 1)
        {
            PulseHigh1++;
        }
        else if(Output_no == 2)
        {
            PulseHigh2++;
        }
        else if(Output_no == 3)
        {
            PulseHigh3++;
        }
        //check for pulse output set flag
        IO_board_param.Pulsed_Output_Done_Flag |= (1 << (Output_no - 1));
        //send an event to IO board
        IO_board_param.Send_event_to_IO_board = 1;
    }
}
/*****************************************************************************
* @note       Function name : uint8_t Process_Response_Frame(void)
* @returns    returns       : None
* @param      arg1          : None
* @author                   : Shweta Pimple
* @date       date created  : 4 Jan, 2012
* @brief      Description   : Process the response frame
* @note       Notes         : None
*****************************************************************************/
int8_t Process_Response_Frame(void)
{
    uint16_t Calculated_Chksum;
    BIT_FIELD *ptrBitfield,*ptrBitfield1;
    ptrBitfield = (BIT_FIELD *)&IoResponse_Para.DigitalIP;
    ptrBitfield1 = (BIT_FIELD *)&Previous_DigitalIP;
    Set_IO_board_input_output_relay_status(Setup_device_var.Digital_Input_1_Function, \
                                           &Rprts_diag_var.Digital_Input_1_Status, \
                                           ptrBitfield->bit0);
    Set_IO_board_input_output_relay_status(Setup_device_var.Digital_Input_2_Function, \
                                           &Rprts_diag_var.Digital_Input_2_Status, \
                                           ptrBitfield->bit1);
    Set_IO_board_input_output_relay_status(Setup_device_var.Digital_Input_3_Function, \
                                           &Rprts_diag_var.Digital_Input_3_Status, \
                                           ptrBitfield->bit2);
    Set_IO_board_input_output_relay_status(Setup_device_var.Digital_Input_4_Function, \
                                           &Rprts_diag_var.Digital_Input_4_Status, \
                                           ptrBitfield->bit3);

    //Calculate the checksum
    Calculated_Chksum = CalculateCRC((uint8_t*)&(IoResponse_Para), (sizeof(IoResponse_Para)-2));
    IoResponse_Para.DigitalIP = (~IoResponse_Para.DigitalIP);
    if(Calculated_Chksum == IoResponse_Para.checksum)
    {
        if(IoResponse_Para.Sof == IoRequest_Para.Sof)
        {
            if(IoResponse_Para.DeviceId == IoRequest_Para.DeviceId)
            {
                sprintf(IOboard_fw_ver,"%02x.%02x",IoResponse_Para.Firm_ver[0],IoResponse_Para.Firm_ver[1]);
                //check the response code
                if(IoResponse_Para.ResponseCode == 0)
                {
                    // Digital Input 1
                    if(ptrBitfield->bit0)    // if current state of the pin is on
                    {
                        if(ptrBitfield1->bit0 == 0) // if Previous state of the pin is off
                        {
                            Process_DigitalInputs(Setup_device_var.Digital_Input_1_Function);
                        } /*"PID Rate = Zero"*/
                        if((Setup_device_var.Digital_Input_1_Function == PID_RATE_ZERO_STR) &&
                                (Setup_device_var.PID_Local_Setpoint != 0))
                        {
                            PID_Old_Local_Setpoint = Setup_device_var.PID_Local_Setpoint;
                            Setup_device_var.PID_Local_Setpoint = 0;
                        }
                        else if(Setup_device_var.Digital_Input_1_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __TRUE;
                        }
                    }
                    else
                    {
                        if((Setup_device_var.Digital_Input_1_Function == PID_RATE_ZERO_STR) &&
                                (PID_Old_Local_Setpoint  != -1))
                        {
                            Setup_device_var.PID_Local_Setpoint = PID_Old_Local_Setpoint;
                            PID_Old_Local_Setpoint  = -1;
                        }
                        else if(Setup_device_var.Digital_Input_1_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __FALSE;
                        }
                    }
                    // Digital Input 2
                    if(ptrBitfield->bit1)
                    {
                        if(ptrBitfield1->bit1 == 0) // if Previous state of the pin is off
                        {
                            Process_DigitalInputs(Setup_device_var.Digital_Input_2_Function);
                        } /*"PID Rate = Zero"*/
                        if((Setup_device_var.Digital_Input_2_Function == PID_RATE_ZERO_STR) &&
                                (Setup_device_var.PID_Local_Setpoint != 0))
                        {
                            PID_Old_Local_Setpoint = Setup_device_var.PID_Local_Setpoint;
                            Setup_device_var.PID_Local_Setpoint = 0;
                        }
                        else if(Setup_device_var.Digital_Input_2_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __TRUE;
                        }
                    }
                    else
                    {
                        if((Setup_device_var.Digital_Input_2_Function == PID_RATE_ZERO_STR) &&
                                (PID_Old_Local_Setpoint  != -1))
                        {
                            Setup_device_var.PID_Local_Setpoint = PID_Old_Local_Setpoint;
                            PID_Old_Local_Setpoint  = -1;
                        }
                        else if(Setup_device_var.Digital_Input_2_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __FALSE;
                        }
                    }
                    // Digital Input 3
                    if(ptrBitfield->bit2)
                    {
                        if(ptrBitfield1->bit2 == 0) // if Previous state of the pin is off
                        {
                            Process_DigitalInputs(Setup_device_var.Digital_Input_3_Function);
                        } /*"PID Rate = Zero"*/
                        if((Setup_device_var.Digital_Input_3_Function == PID_RATE_ZERO_STR) &&
                                (Setup_device_var.PID_Local_Setpoint != 0))
                        {
                            PID_Old_Local_Setpoint = Setup_device_var.PID_Local_Setpoint;
                            Setup_device_var.PID_Local_Setpoint = 0;
                        }
                        else if(Setup_device_var.Digital_Input_3_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __TRUE;
                        }
                    }
                    else
                    {
                        if((Setup_device_var.Digital_Input_3_Function == PID_RATE_ZERO_STR) &&
                                (PID_Old_Local_Setpoint  != -1))
                        {
                            Setup_device_var.PID_Local_Setpoint = PID_Old_Local_Setpoint;
                            PID_Old_Local_Setpoint  = -1;
                        }
                        else if(Setup_device_var.Digital_Input_3_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __FALSE;
                        }
                    }
                    // Digital Input 4
                    if(ptrBitfield->bit3)
                    {
                        if(ptrBitfield1->bit3 == 0) // if Previous state of the pin is off
                        {
                            Process_DigitalInputs(Setup_device_var.Digital_Input_4_Function);
                        } /*"PID Rate = Zero"*/
                        if((Setup_device_var.Digital_Input_4_Function == PID_RATE_ZERO_STR) &&
                                (Setup_device_var.PID_Local_Setpoint != 0))
                        {
                            PID_Old_Local_Setpoint = Setup_device_var.PID_Local_Setpoint;
                            Setup_device_var.PID_Local_Setpoint = 0;
                        }
                        else if(Setup_device_var.Digital_Input_4_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __TRUE;
                        }
                    }
                    else
                    {
                        if((Setup_device_var.Digital_Input_4_Function == PID_RATE_ZERO_STR) &&
                                (PID_Old_Local_Setpoint  != -1))
                        {
                            Setup_device_var.PID_Local_Setpoint = PID_Old_Local_Setpoint;
                            PID_Old_Local_Setpoint  = -1;
                        }
                        else if(Setup_device_var.Digital_Input_4_Function  == ERROR_ACK_STR)
                        {
                            IO_board_param.Error_ack_flag = __FALSE;
                        }
                    }
                }
            }
            else
            {
                return IO_GN_ERROR;
            }
        }
        else
        {
            return IO_GN_ERROR;
        }
    }
    else
    {
        return CRC_ERR;
    }
    return IoResponse_Para.ResponseCode;
}

/*-------------------------PUBLIC FUNCTIONS------------------------------*/
/*****************************************************************************
* @note       Function name : float Find_Weight_Per_Pulse(char Output_no, char* Digital_Output_Func, char* Digital_Output_Units_Per_Pulse)
* @returns    returns       : None
* @param      arg1          : char* to Digital_Output_Func,Digital_Output_Units_Per_Pulse
* @author                   :
* @date       date created  : 25 Apr 2013
* @brief      Description   : find the value of Weight_Per_Pulse using LC capacity
* @note       Notes         : None
*****************************************************************************/
void Find_Weight_Per_Pulse(char Output_no, unsigned int Digital_Output_Func,
                           unsigned int Digital_Output_Units_Per_Pulse, double *pOutput_Delivered_wt)
{
    float New_Wt_Per_Pulse = 0.1;
    double TempTotal_weight_Perc = 0;

    //Pulsed Output and  Quadrature Wave
    if((Digital_Output_Func == PULSED_OUTPUT_STR) ||  (Digital_Output_Func == QUADRATURE_WAVE_STR))
    {
        //If the output conf. for pulse
        if(Digital_Output_Func == PULSED_OUTPUT_STR)
        {
            if( Digital_Output_Units_Per_Pulse == FIFTH_WT_UNIT_PER_PULSE_STR)
            {
                New_Wt_Per_Pulse = (0.01);
            }
            //0.1 Weight Units
            else if( Digital_Output_Units_Per_Pulse == FIRST_WT_UNIT_PER_PULSE_STR)
            {
                New_Wt_Per_Pulse = (0.1);
            }
            //1.0 Weight Units
            else if( Digital_Output_Units_Per_Pulse == SECOND_WT_UNIT_PER_PULSE_STR)
            {
                New_Wt_Per_Pulse = (1.0);
            }
            //10 Weight Units
            else if( Digital_Output_Units_Per_Pulse == THIRD_WT_UNIT_PER_PULSE_STR)
            {
                New_Wt_Per_Pulse = (10.0);
            }
            //100 Weight Units
            else if( Digital_Output_Units_Per_Pulse == FOURTH_WT_UNIT_PER_PULSE_STR)
            {
                New_Wt_Per_Pulse = (100.0);
            }
            // default 0.1 Weight Units
            else
            {
                New_Wt_Per_Pulse = IO_board_param.Weight_per_pulse;
            }
        }
        else  //Output config  for quad take last pulse output - Weight Per Pulse value
        {
            New_Wt_Per_Pulse = IO_board_param.Weight_per_pulse;
        }

        //Assign values
        IO_board_param.Weight_per_pulse = New_Wt_Per_Pulse;

        if(Digital_Output_Func == PULSED_OUTPUT_STR)
        {
            if(IO_board_param.Quadrature_Wave_Setting == 11)
            {
                Process_Pulse_Output(Output_no, New_Wt_Per_Pulse, pOutput_Delivered_wt);
                return;
            }
        }
        //else
        {
            TempTotal_weight_Perc = ( (IO_board_param.Delivered_weight_Quad_OP * Calculation_struct.Weight_conv_factor) / New_Wt_Per_Pulse ) * 100;

            if((TempTotal_weight_Perc >= 25)&& (TempTotal_weight_Perc < 50))
            {
                if(IO_board_param.Load_in_percentage != 25)
                {
                    IO_board_param.Load_in_percentage = 25;
                    //send an event to IO board to enable the 1st channel of quadrature output
                    IO_board_param.Send_event_to_IO_board = 1 ;
                }
            }
            else if((TempTotal_weight_Perc >= 50) && (TempTotal_weight_Perc < 75))
            {
                if(IO_board_param.Load_in_percentage != 50)
                {
                    IO_board_param.Load_in_percentage = 50;
                    //send an event to IO board to enable the 2nd channel of quadrature output
                    IO_board_param.Send_event_to_IO_board = 1 ;
                }
            }
            else if((TempTotal_weight_Perc >= 75) && (TempTotal_weight_Perc < 100))
            {
                if(IO_board_param.Load_in_percentage != 75)
                {
                    //check if weight is reached to multiples of weight per pulse
                    IO_board_param.Load_in_percentage = 75;
                    IO_board_param.Send_event_to_IO_board = 1 ;
                }
            }
            else if(TempTotal_weight_Perc >= 100)
            {
                //set load to 100%
                IO_board_param.Load_in_percentage = 100;
                //send an event to IO board to enable the 2nd channel of quadrature output
                IO_board_param.Send_event_to_IO_board = 1 ;
                IO_board_param.Delivered_weight_Quad_OP = IO_board_param.Delivered_weight_Quad_OP - (New_Wt_Per_Pulse / Calculation_struct.Weight_conv_factor);
            }
            else
            {
                IO_board_param.Load_in_percentage = 0;
            }
        }
    }
}

/*****************************************************************************
* @note       Function name : void Get_Batch_Complete_Status(void)
* @returns    returns       : None
* @param      arg1          : None
* @author                   :
* @date       date created  : 25 Apr 2013
* @brief      Description   : Get batch complete flag
* @note       Notes         : None
*****************************************************************************/
void Get_Batch_Complete_Status(void)
{

    //check weight for Batching/Loadout functionality
    if((Setup_device_var.Digital_Output_1_Function == BATCHING_LOADOUT_STR) ||
            (Setup_device_var.Digital_Output_2_Function == BATCHING_LOADOUT_STR) ||
            (Setup_device_var.Digital_Output_3_Function == BATCHING_LOADOUT_STR) ||
            (Setup_device_var.Relay_Output_1_Function == BATCHING_LOADOUT_STR) ||
            (Setup_device_var.Relay_Output_2_Function == BATCHING_LOADOUT_STR) ||
            (Setup_device_var.Relay_Output_3_Function == BATCHING_LOADOUT_STR))
    {
        if((Calculation_struct.Total_weight >= (Calculation_struct.Target_weight - Calculation_struct.Cutoff_weight)) && (IO_board_param.Batching_Flag == BATCH_START))
        {
            IO_board_param.Batching_Flag = BATCH_COMPLETE;
            //send an event to IO board to turn of the batching output to stop the feeder
            IO_board_param.Send_event_to_IO_board = 1 ;
        }
    }
}
/*****************************************************************************
* @note       Function name : __task void ioboard_task (void)
* @returns    returns       : None
* @param      arg1          : None
* @author                   : Shweta Pimple
* @date       date created  : 20 December 2012
* @brief      Description   : IO board task
* @note       Notes         : None
*****************************************************************************/
extern int task_number;
__task void ioboard_task (void)
{
    uint8_t u8state, WaitForResponseCntr, flash_write_status = 0;

    /*Initialize the SSP port */
    Init_IOboard(INIT,(uint8_t*)&IoRequest_Para, (uint8_t*)&IoResponse_Para, (sizeof(IoRequest_Para)));

    /*Initialize the IO board protocol structure to default values */
    Init_IOboard_struct();
    //Initialize variables
    u8state = 0;
    WaitForResponseCntr = 0;
    IO_board_param.Error_ack_flag = __FALSE;
    IO_board_param.Clear_weight_input_set = __FALSE;
    IO_board_param.Send_event_to_IO_board = 0;
    IO_board_param.Weight_per_pulse = 0.1;
    IO_board_param.Status_displayed_flag = __TRUE;
    //initialize P2.19 as I/O with pulldown register
    LPC_IOCON->P2_19 = PIN2_19_CONFIG;
    //set P2.19 as input
    LPC_GPIO2->DIR &= ~PIN2_19_STATUS;
    //Intialization of Analog Outputs after reset.
    Prepare_Request_frame();
    IoRequest_Para.AnalogCh1 = 0x1000;
    IoRequest_Para.AnalogCh2 = 0x1000;
    Ioboard_send_frame(sizeof(IoRequest_Para));
    //Loop forever
    while(1)
    {

        //timeout of 500 msec or wait for an event
        os_evt_wait_or(IOBOARD_EVENT_FLAG, IOBOARD_TASK_INTERVAL);
        LPC_RTC->ALHOUR |= (0x01<<0);
        switch (u8state)
        {
        case 0:
        {
            REQUEST_PARA Temp_IoRequest_Para;
            if(u8Resend == 0)
            {
                Check_for_Quadrature_Wave_Setting();
                Get_Batch_Complete_Status();
                Get_Weight_Per_Pulse();
                //Initialize the SSP port to master
                /*Construct the frame send to IO board*/
                //if(!Resend_Packet)
            }
            {
                Prepare_Request_frame();
            }
            Init_IOboard(MASTER,(uint8_t*)&IoRequest_Para, (uint8_t*)&Temp_IoRequest_Para, (sizeof(IoRequest_Para)));
            /*Send request to io board*/
            Ioboard_send_frame(sizeof(IoRequest_Para));
            Previous_DigitalIP = IoResponse_Para.DigitalIP;
            //Clear the response buffer
            memset((uint8_t *)&IoResponse_Para, 0, sizeof(IoResponse_Para));
            u8state = 1;
        }
        case 1:
        {
            RESPONSE_PARA Temp_IoResponse_Para;
            //Configure SPI to Slave mode
            Init_IOboard(SLAVE,(uint8_t*)&Temp_IoResponse_Para, (uint8_t*)&IoResponse_Para, (sizeof(IoResponse_Para)));
            u8state = 2;
        }
        break;
        case 2:
            if(complete == DONE)
            {
                ReceivedPkts++;
                IO_board_param.IO_Board_Status = __TRUE;
                //reset the flag
                Flags_struct.Error_flags &= ~IO_BRD_COMM_ERR;
                GUI_data_nav.Error_wrn_msg_flag[IO_BRD_COMM_ERR_TIME] = 0;
                Rprts_diag_var.SPI_Status = Port_string1; //on
                //Reset the data transfer complete flag
                complete = CLR;
                /*Process repsonse frame*/
                Process_Response_Frame();
                IO_board_param.Status_displayed_flag = __FALSE;
                u8state = 0;
                WaitForResponseCntr = 0;
                u8Resend = 0;
            }
            else
            {
                u8Resend = 1;
                WaitForResponseCntr++;
                //Retry for 50 times only 50*30 ms = 1.5 secs
                if(WaitForResponseCntr > 10)
                {
                    WaitForResponseCntr = 0;
                    IO_board_param.IO_Board_Status = __FALSE;
                    Rprts_diag_var.SPI_Status = Port_string2; //off
                    //set the error flag
                    if (!(Flags_struct.Error_flags & IO_BRD_COMM_ERR) && Scale_setup_var.IO_board_install == INSTALLED)
                    {
                        Flags_struct.Error_flags |= IO_BRD_COMM_ERR;
                        Flags_struct.Err_Wrn_log_written |= IO_BRD_COMM_ERR_LOGGED;
                    }
                }
                u8state = 0;
            }

            break;
        default:
            break;
        }
        if ((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && (IO_board_param.Cal_started_flag == __TRUE))
        {
            //write the calib log and report after the calibration is complete
            if(Calib_struct.Calib_log_flag == SET_ZERO_DATA_LOG_WRITE)
            {
                //if logging is enabled
                if (Setup_device_var.Log_zero_cal_data == Screen431_str2)
                {
                    /*populate the set zero cal data log and update on the USB drive or flash*/
                    flash_write_status = log_data_populate(set_zero_log);
                    report_data_populate(set_zero_test_report);
                    if (flash_write_status == __TRUE)
                    {
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    }
                }
            }
            //BS-195 Periodic Log duplicate record numbers, bad data , missing entry
            //Added by PVK 02 May 2016 finshed log entery into periodic log file
            log_data_populate(calibration_finished);
            //PVK - Added on 27/4/2016 to reset the Digital input started flag
            IO_board_param.Cal_started_flag = __FALSE;
        }
        LPC_RTC->ALHOUR &= ~(0x01<<0);
    }
}
#endif
/*****************************************************************************
* End of file
*****************************************************************************/
