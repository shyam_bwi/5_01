/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : rtc.c
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 16, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "rtc.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/
#ifdef RTC
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
	volatile uint32_t alarm_on = 0;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

RTCTime Current_time;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void RTC_IRQHandler (void);
void RTCInit( void );
void RTCStart( void );
void RTCStop( void );
void RTC_CTCReset( void );
void RTCSetTime( RTCTime );
RTCTime RTCGetTime( void );
void RTCSetAlarm( RTCTime );
void RTCSetAlarmMask( U32 AlarmMask );

void Set_rtc_date(DATE_STRUCT date);
void Set_rtc_time(TIME_STRUCT time);
void Set_rtc_dow(unsigned int dow);
U16 Get_Dow_String_Enum(U16 dow);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: void RTC_IRQHandler (void) 
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : RTC interrupt handler, it executes based on the
*								 						 the alarm setting
* @note       Notes		     : None
*****************************************************************************/
void RTC_IRQHandler (void) 
{  
  LPC_RTC->ILR |= ILR_RTCCIF;		/* clear interrupt flag */
 // alarm_on = 1;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCInit( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Initialize RTC timer
* @note       Notes		     : None
*****************************************************************************/
void RTCInit( void )
{
  alarm_on = 0;

  /* Enable CLOCK into RTC */
  LPC_SC->PCONP |= (1 << 9);

  /* If RTC is stopped, clear STOP bit. */
  if ( LPC_RTC->RTC_AUX & (0x1<<4) )
  {
	LPC_RTC->RTC_AUX |= (0x1<<4);	
  }
  
  /*--- Initialize registers ---*/    
 // LPC_RTC->AMR = 0;
	LPC_RTC->AMR = 0XFF;		//SKS --> 03-11-2016 need to use alarm registers to 
													//store prev date time values for comparison <--
  LPC_RTC->CIIR = 0;
  LPC_RTC->CCR = 0;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCStart( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Start RTC timer
* @note       Notes		     : None
*****************************************************************************/
void RTCStart( void ) 
{
  /*--- Start RTC counters ---*/
  LPC_RTC->CCR |= CCR_CLKEN;
  LPC_RTC->ILR = ILR_RTCCIF;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCStop( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Stop RTC timer
* @note       Notes		     : None
*****************************************************************************/
void RTCStop( void )
{   
  /*--- Stop RTC counters ---*/
  LPC_RTC->CCR &= ~CCR_CLKEN;
  return;
} 

/*****************************************************************************
* @note       Function name: void RTC_CTCReset( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Reset RTC clock tick counter
* @note       Notes		     : None
*****************************************************************************/
void RTC_CTCReset( void )
{   
  /*--- Reset CTC ---*/
  LPC_RTC->CCR |= CCR_CTCRST;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCSetTime( RTCTime Time ) 
* @returns    returns		   : None
* @param      arg1			   : Time - time to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC timer value
* @note       Notes		     : None
*****************************************************************************/
void RTCSetTime( RTCTime Time ) 
{
  LPC_RTC->SEC = Time.RTC_Sec;
  LPC_RTC->MIN = Time.RTC_Min;
  LPC_RTC->HOUR = Time.RTC_Hour;
  LPC_RTC->DOM = Time.RTC_Mday;
  LPC_RTC->DOW = Time.RTC_Wday;
  LPC_RTC->DOY = Time.RTC_Yday;
  LPC_RTC->MONTH = Time.RTC_Mon;
  LPC_RTC->YEAR = Time.RTC_Year;    
  return;
}

/*****************************************************************************
* @note       Function name: void Set_rtc_date(DATE_STRUCT date) 
* @returns    returns		   : None
* @param      arg1			   : date to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC date
* @note       Notes		     : None
*****************************************************************************/
void Set_rtc_date(DATE_STRUCT date) 
{
  LPC_RTC->DOM = date.Day;
  LPC_RTC->MONTH = date.Mon;
  LPC_RTC->YEAR = date.Year;    
  return;
}

/*****************************************************************************
* @note       Function name: void Set_rtc_time(TIME_STRUCT time)
* @returns    returns		   : None
* @param      arg1			   : time to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC hour min sec value
* @note       Notes		     : Venkata Modified this function on 9thNov2013, 
                             included AM and PM related logic
														 Megha modified this logic on 21st Oct 2015 for correct 
														 time entry in 12/24 hr mode. 
														 Do not change this logic.
*****************************************************************************/
void Set_rtc_time(TIME_STRUCT time) 
{
 	if(Admin_var.Current_Time_Format == TWELVE_HR)
   {
 	   if(Admin_var.AM_PM == AM_TIME_FORMAT)
 		 { 
 			  if(time.Hr == 12)
 			      LPC_RTC->HOUR = 0;
 			  else
 				    LPC_RTC->HOUR = (U32)time.Hr;
 		 }
 	   else
 		 {
 			  if(time.Hr == 12)
 			      LPC_RTC->HOUR = 12 ;
 				else
 				    LPC_RTC->HOUR = (U32)time.Hr + 12;
 		 }
 	}
 	else 
	{	
    LPC_RTC->HOUR = (U32)time.Hr;
	}
	LPC_RTC->SEC = (U32)time.Sec;
  LPC_RTC->MIN = (U32)time.Min;
  return;
}

/*****************************************************************************
* @note       Function name: void Set_rtc_dow(unsigned int dow) 
* @returns    returns		   : None
* @param      arg1			   : Day of the week
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Set the rtc day of the week
* @note       Notes		     : None
*****************************************************************************/
void Set_rtc_dow(unsigned int dow) 
{
	if (dow == SUNDAY) //Sunday
	{
		 LPC_RTC->DOW = 0;
	}
	else if (dow == MONDAY) //Monday
	{
		 LPC_RTC->DOW = 1;
	}
	else if (dow == TUESDAY) //Tuesday
	{
		 LPC_RTC->DOW = 2;
	}
	else if ( dow == WEDNESDAY) //Wednesday
	{
		 LPC_RTC->DOW = 3;
	}
	else if ( dow == THURSDAY ) //Thursday
	{
		 LPC_RTC->DOW = 4;
	}
	else if ( dow == FRIDAY) //Friday
	{
		 LPC_RTC->DOW = 5;
	}
	else if ( dow == SATURDAY) //Saturday
	{
		 LPC_RTC->DOW = 6;
	}
  return;
}
/*****************************************************************************
* @note       Function name: U16 Get_Dow_String_Enum(U16 dow) 
* @returns    returns		   : enum corresponding to day of week string(sunday,saturday,etc) or 0 on invalid argument.
* @param      arg1			   : Day of the week
* @author			             : R Venkata Krishna Rao
* @date       date created : 
* @brief      Description	 : gets the enum of day of week string
* @note       Notes		     : None
*****************************************************************************/
U16 Get_Dow_String_Enum(U16 dow) 
{
   switch(dow)
	 {
		  case 0:  return SUNDAY;
			case 1:  return MONDAY;
			case 2:  return TUESDAY;
			case 3:  return WEDNESDAY;
			case 4:  return THURSDAY;
			case 5:  return FRIDAY;
			case 6:  return SATURDAY;
			default: return 0;
	 }
}
/*****************************************************************************
* @note       Function name: void RTCSetTime( RTCTime Time ) 
* @returns    returns		   : None
* @param      arg1			   : Time - time to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC timer value
* @note       Notes		     : None
*****************************************************************************/
/*void RTCSetTime( RTCTime Time ) 
{
  LPC_RTC->SEC = Time.RTC_Sec;
  LPC_RTC->MIN = Time.RTC_Min;
  LPC_RTC->HOUR = Time.RTC_Hour;
  LPC_RTC->DOM = Time.RTC_Mday;
  LPC_RTC->DOW = Time.RTC_Wday;
  LPC_RTC->DOY = Time.RTC_Yday;
  LPC_RTC->MONTH = Time.RTC_Mon;
  LPC_RTC->YEAR = Time.RTC_Year;    
  return;
}
*/
/*****************************************************************************
* @note       Function name: void RTCSetAlarm( RTCTime Alarm )
* @returns    returns		   : None
* @param      arg1			   : Alarm - Time at which alarm is to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Inittialize the alarm time
														 SysTick interrupt happens every 10 ms
* @note       Notes		     : None
*****************************************************************************/
/*void RTCSetAlarm( RTCTime Alarm ) 
{   
  LPC_RTC->ALSEC = Alarm.RTC_Sec;
  LPC_RTC->ALMIN = Alarm.RTC_Min;
  LPC_RTC->ALHOUR = Alarm.RTC_Hour;
  LPC_RTC->ALDOM = Alarm.RTC_Mday;
  LPC_RTC->ALDOW = Alarm.RTC_Wday;
  LPC_RTC->ALDOY = Alarm.RTC_Yday;
  LPC_RTC->ALMON = Alarm.RTC_Mon;
  LPC_RTC->ALYEAR = Alarm.RTC_Year;    
  return;
}*/

/*****************************************************************************
* @note       Function name: RTCTime RTCGetTime( void )
* @returns    returns		   : The current time is returned
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Get RTC timer value
* @note       Notes		     : Venkata Modified this function on 9thNov2013, 
                             included AM and PM related logic
*****************************************************************************/
RTCTime RTCGetTime( void ) 
{
  RTCTime LocalTime;
	
	//Added by PVK on 20 May 2016 to set default time format as 24 hour fixed as
	//there is no option for selection
	Admin_var.Current_Time_Format = TWENTYFOUR_HR;
	
  if( Admin_var.Current_Time_Format == TWELVE_HR)
    {
        if(LPC_RTC->HOUR <12)  // 0hrs to 11hrs i.e AM time period
				{
				   Admin_var.AM_PM = AM_TIME_FORMAT; // AM
           if(LPC_RTC->HOUR == 0)
					 {			 
  					  LocalTime.RTC_Hour = 12; 
					 }
					 else
					 {
						  LocalTime.RTC_Hour = LPC_RTC->HOUR;
					 }
				}
				else   // 12hrs to 23hrs i.e PM time period
        {
				   Admin_var.AM_PM = PM_TIME_FORMAT; // PM
					  if(LPC_RTC->HOUR == 12)
						{
						  LocalTime.RTC_Hour = 12; 
						}
					  else
						{
              LocalTime.RTC_Hour = LPC_RTC->HOUR - 12;
						}
        }
	}
  else
  {
		LocalTime.RTC_Hour = LPC_RTC->HOUR;
	}

  LocalTime.RTC_Sec = LPC_RTC->SEC;
  LocalTime.RTC_Min = LPC_RTC->MIN;
  LocalTime.RTC_Mday = LPC_RTC->DOM;
  LocalTime.RTC_Wday = LPC_RTC->DOW;
  LocalTime.RTC_Yday = LPC_RTC->DOY;
  LocalTime.RTC_Mon = LPC_RTC->MONTH;
  LocalTime.RTC_Year = LPC_RTC->YEAR;
  return ( LocalTime );    
}

/*****************************************************************************
* @note       Function name: void RTCSetAlarmMask( uint32_t AlarmMask )
* @returns    returns		   : None
* @param      arg1			   : Alarm mask needs to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Set RTC timer alarm mask
* @note       Notes		     : None
*****************************************************************************/
/*void RTCSetAlarmMask( uint32_t AlarmMask ) 
{
  //--- Set alarm mask ---
  LPC_RTC->AMR = AlarmMask;
  return;
}
*/

/*****************************************************************************
**                            End Of File
******************************************************************************/
#endif
