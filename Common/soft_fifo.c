/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : soft_fifo.c
* @brief               : Controller Board
*
* @author              : R. Venkata Krishna Rao
*
* @date Created        : 20th  November wed, 2013  <Nov 20, 2013>
* @date Last Modified  : 20th  November wed, 2013  <Nov 20, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "soft_fifo.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifdef _SOFT_FIFO_H_


/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: Push_FIFO
* @returns    returns      : 0 if Push operation is a success
                             1 if Push operation is a failure
* @param      arg1         : a pointer to Soft_FIFO Structure
* @param      arg2         : a float value which is to be pushed in FIFO (Queue)                             
* @author                  : R. Venkata Krishna Rao
* @date       date created : Nov 20, 2013
* @brief      Description  : It pushes one value of type float into FIFO(Queue)
* @note       Notes        : None
*****************************************************************************/

int Push_FIFO(Soft_FIFO *t_FIFO,float element )
{  
	if( t_FIFO->front == SOFT_FIFO_SIZE)
		t_FIFO->front = 0;
  if(!(Is_FIFO_Full(t_FIFO))) // If FIFO is not FULL
  {
	  if((t_FIFO->count + t_FIFO->front) < SOFT_FIFO_SIZE )
	  {
       t_FIFO->contents[t_FIFO->count + t_FIFO->front] = element;
	  }
	  else
	  {
		   t_FIFO->contents[(t_FIFO->count + t_FIFO->front)-SOFT_FIFO_SIZE] = element;
	  }
    (t_FIFO->count)++;
	  return 0;
  }
  else 
	  return -1;
}
/*****************************************************************************
* @note       Function name: Pop_FIFO
* @returns    returns      : 1 if Pop operation is a success
													   0 if Pop operation is a failure
* @param      arg1         : a pointer to Soft_FIFO Structure                          
* @author                  : R. Venkata Krishna Rao
* @date       date created : Nov 20, 2013
* @brief      Description  : It Pop's one value of type float out of FIFO(Queue)
* @note       Notes        : None
*****************************************************************************/
float Pop_FIFO(Soft_FIFO *t_FIFO ,float *dest )
{
	if(!(Is_FIFO_Empty(t_FIFO))) // // If FIFO is not Empty
	{
		 if(t_FIFO->front == SOFT_FIFO_SIZE) // if front pointer reaches the end of the circular buffer then set it to beginning
		 {
       t_FIFO->front = 0;
     }
		 *dest = t_FIFO->contents[t_FIFO->front];
		 //Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
		 //t_FIFO->contents[t_FIFO->front] = 0; 
     t_FIFO->front++;
	   t_FIFO->count--;
	   return 1;
	}
	else 
	   return 0;
	
}
#endif

