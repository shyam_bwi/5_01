/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename DEBUG_PORT      : Global_ex.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : March 21st Thursday, 2013
* @date Last Modified  : March 21st Thursday, 2013
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __GLOBAL_EX_H
#define __GLOBAL_EX_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <RTL.h>
#include "File_Config.h"
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "lpc177x_8x_i2c.h"
#include "lpc177x_8x_clkpwr.h"
#include "lpc177x_8x_pinsel.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

#define MATRIX_ARB      (*(volatile U32*)(0x400FC188))
#define ARRAY_SIZE			30
#define FILE_NAME_SIZE	40
#define SET_FLAG				1
#define GET_FLAG				0

//Enable / Disable macros to activate Functions

//Added by DK on 4 May2016 for Rev3.xx & rev4.xx merge
//#define REV3_BOARD //Comment this for Rev4 board & use Beltway_Integrator_Rev4.sct. else use Beltway_Integrator_Rev3.sct

#define RTOS
#define RTC
#define IOBOARD
#define CALCULATION
#define KEYPAD
#define PRINTER
#define SCOREBOARD
#define GUI
#define EXT_FLASH
#define MODBUS_SENSOR_RTU_MASTER
#define FILE_SYS
#define USB
#define EEPROM
#define MODBUS_TCP
#define ETHERNET
#define MODBUS_RTU_INTEGRATOR
#define RATE_BLEND_LOAD_CTRL_CALC
#define DEBUG_PORT
//#define EXPANSION_SLOT
#define DEBUG_DATA_SEND    
#define DEBUG_DATA_SEND_1

#define SCREENSHOT
#define PID_CONTROL
//#define LOAD_CONTROL
#define MVT_TABLE
#define LCD_TURNED_ON_INIT                                    0
#define LCD_ON_TIMEOUT_START                                  1
#define LCD_TURNED_OFF_AFTER_TIMEOUT                          2

//CPU CLK
#define __CPUCLK                                               120000000

//Task Time - All task intervals are in 10mSec resolution
#define FREE_RUN_TIMER_TASK_INTERVAL                           5
#define KEYPAD_TASK_INTERVAL                                   15
#define LCD_TASK_INTERVAL                                      20
#define IOBOARD_TASK_INTERVAL                                  50
#define SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL        5
#define INT_TO_INT_BOARD_MODBUS_MASTER_RTU_COMM_TASK_INTERVAL  50
#define INT_TO_INT_BOARD_MODBUS_SLAVE_RTU_COMM_TASK_INTERVAL   1
#define USB_CON_TASK_INTERVAL                                  2
#define SCORE_BOARD_TASK_INTERVAL                              35
#define RTC_GET_TIME_TASK_INTERVAL                             5
#define ETHERNET_MAIN_TASK_INTERVAL                            1
#define ETHERNET_SERVER_TASK_INTERVAL                          10

#define ETHERNET_TCP_TICK_TASK_INTERVAL                        1
#define MODBUS_TCP_PKT_TASK_INTERVAL                           1
#define EMAIL_TASK_INTERVAL                                    50

//SKS : REMOVE THE BELOW AFTER TEST OF FLASH API IS DONE. 31/10/2017 -->
#define DEBUG_FLASH_TASK_INTERVAL 														100
//<<-

#define WDT_FEED_TASK_INTERVAL                        10 

#define LOAD_MASTER_MODBUS_STACK_TO_SCALE_MASTER_ADD_ZERO				1
#define BLEND_RATE_STATUS_ADDRESS																1000

//Event Flags
//#define GUI_X_WaitEvent 0001 //reserved for GUI lib
#define KEYPAD_EVENT_FLAG                               0x0002
#define PRINTER_EVENT_FLAG                              0x0004
#define IOBOARD_EVENT_FLAG                              0x0008
#define MONITOR_WEIGHT_EVENT_FLAG                       0x0010
//#define FLASH_LOG_STORE_EVENT_FLAG                    0x0020

#ifdef DEBUG_DATA_SEND
#define DEBUG_DATA_SEND_EVENT                            0x0001
#endif 

//Time outs
#define KEPAD_TIMEOUT_COUNT                             10000 //60Sec
#define KBD_TIMEOUT                                     (KEPAD_TIMEOUT_COUNT / FREE_RUN_TIMER_TASK_INTERVAL)

/*Connection flags bit definitions*/
#define USB_CON_FLAG                                    0x01
#define USB_CON_EX_FLAG                                 0x02
#define ETH_CON_FLAG                                    0x04
#define ETH_CON_FLAG2																		0x08

/*Board communication flags bit definitions*/
#define INT_BRD_COM_FLAG                                0x01
#define SEN_BRD_COM_FLAG                                0x02
#define IO_BRD_COM_FLAG                                 0x04

/*Log flags bit definitions*/
#define PERIODIC_LOG_WR_FLAG                            0x01
#define SET_ZERO_LOG_WR_FLAG                            0x02
#define CLEAR_LOG_WR_FLAG                               0x04
#define CALIB_LOG_WR_FLAG                               0x08
#define ERROR_LOG_WR_FLAG                               0x10
#define LENGTH_LOG_WR_FLAG                              0x20
#define LOGS_WR_FLAG                                    0x40

/*Report flags bit definitions*/
#define CAL_RPRT_WR_FLAG                                0x01
#define ERR_RPRT_WR_FLAG                                0x02
#define DLY_RPRT_WR_FLAG                                0x04
#define WKLY_RPRT_WR_FLAG                               0x08
#define MTHLY_RPRT_WR_FLAG                              0x10

/*Calibration flags bit definitions*/
#define TEST_WT_CAL_FLAG                                0x01
#define MAT_TEST_CAL_FLAG                               0x02
#define DIG_CAL_FLAG                                    0x04
#define BELT_LEN_CAL_FLAG                               0x08
#define ZERO_CAL_FLAG                                   0x10

/*Error flags bit definitions*/
#define ZERO_BELT_SPEED_ERR                             0x01
#define LOAD_CELL_DATA_ERR                              0x02
#define ANGLE_SENSOR_ERR                                0x04
#define SEN_BRD_COMM_ERR                                0x08
#define SCALE_COMM_ERR                                  0x10
#define NEGATIVE_RATE_ERR                               0x20
#define IO_BRD_COMM_ERR                                 0x40
#define EEPROM_WRITE_ERR                                0x80

/*Warning flags bit definitions*/
#define LOW_SPEED_WARNING                               0x01
#define HIGH_SPEED_WARNING                              0x02
#define ZERO_CAL_CHANGE_WARNING                         0x04
#define SEN_BRD_COMM_WARNING                            0x08
#define SCALE_COMM_WARNING                              0x10
#define LOW_RATE_WARNING                                0x20
#define HIGH_RATE_WARNING                               0x40

/*Error and warning logs written bit definitions*/
#define ZERO_BELT_SPEED_ERR_LOGGED                      0x0001
#define LOAD_CELL_DATA_ERR_LOGGED                       0x0002
#define ANGLE_SENSOR_ERR_LOGGED                         0x0004
#define SEN_BRD_COMM_ERR_LOGGED                         0x0008
#define SCALE_COMM_ERR_LOGGED                           0x0010
#define NEGATIVE_RATE_ERR_LOGGED                        0x0020
#define IO_BRD_COMM_ERR_LOGGED                          0x0040
#define LOW_SPEED_WRN_LOGGED                            0x0080
#define HIGH_SPEED_WRN_LOGGED                           0x0100
#define ZERO_CAL_CHANGE_WRN_LOGGED                      0x0200
#define SEN_BRD_COMM_WRN_LOGGED                         0x0400
#define SCALE_COMM_WRN_LOGGED                           0x0800
#define LOW_RATE_WRN_LOGGED                             0x1000
#define HIGH_RATE_WRN_LOGGED                            0x2000

/*Plant connect flag definitions*/
#define NEW_PLANT_CONNECT_RECORD_FLAG                   0x01
#define OLD_PLANT_CONNECT_RECORD_FLAG                   0x02
#define REGISTER_ADDR_ENTERED_FLAG                      0x04

/*Run Screen Flag Definations*/

#define BLENDING_CONTROL_RUN_SCREEN_FLAG			          0x01
#define LOAD_CONTROL_RUN_SCREEN_FLAG				            0x02
#define RATE_CONTROL_RUN_SCREEN_FLAG				            0x03



/*Error and warning logs display array definitions*/
#define ZERO_BELT_SPEED_ERR_TIME                        0
#define LOAD_CELL_DATA_ERR_TIME                         1
#define ANGLE_SENSOR_ERR_TIME                           2
#define SEN_BRD_COMM_ERR_TIME                           3
#define SCALE_COMM_ERR_TIME                             4
#define NEGATIVE_RATE_ERR_TIME                          5
#define IO_BRD_COMM_ERR_TIME                            6
#define LOW_SPEED_WRN_TIME                              7
#define HIGH_SPEED_WRN_TIME                             8
#define ZERO_CAL_CHANGE_WRN_TIME                        9
#define SEN_BRD_COMM_WRN_TIME                           10
#define SCALE_COMM_WRN_TIME                             11
#define LOW_RATE_WRN_TIME                               12
#define HIGH_RATE_WRN_TIME                              13
#define ERROR_ACK_TIMER_CYCLE														(10*(5)) // value 5 is the factor based on Display task time interval and 10 is the number of seconds

// Testing GPIO Pin posistion
#define TEST_GPIO_P1_05                     0x00000020
#define EXT_INT_PIN													1<<29
#undef TEST_FIRMWARE

/*============================================================================
* Public Data Types
*===========================================================================*/
typedef struct
  {
    U8 Connection_flags;           /*Bit 0-USB,
                                    Bit 1-USB_con_ex,
                                    Bit 2-Modbus 1,
																		Bit 3-Modbus 2*/
    U8 Communication_flags;       /*Bit 0-Integrator board,
                                    Bit 1-Sensor board,
                                    Bit 2-IO board*/
    U8 Report_flags;              /*Flags for report to be written to USBdrive
                                    Bit 0-Calib report,
                                    Bit 1-Error report,
                                    Bit 2-Daily report,
                                    Bit 3-Weekly report,
                                    Bit 4-Monthly report*/
    U8 Log_flags;                 /*Flags for report to be written to USBdrive
                                    Bit 0-Periodic log,
                                    Bit 1-Set Zero log,
                                    Bit 2-Clear log,
                                    Bit 3-Calib log,
                                    Bit 4-Error log,
                                    Bit 5-Length log
                                    Bit 6-Logs in flash to be written to the USB*/
    U8 Calibration_flags;         /*Flags set if calibration needs to be performed
                                    Bit 0-Test weight cal,
                                    Bit 1-Material test cal
                                    Bit 2-Digital cal,
                                    Bit 3-Belt length cal,
                                    Bit 4-Zero cal*/
    U8 Error_flags;                /*Bit 0- Belt speed is zero,
                                    Bit 1 - Load cell error,
                                    Bit 2 - Angle sensor error,
                                    Bit 3 - Sensor board communication error,
                                    Bit 4 - Scale communication error,
                                    Bit 5 - Rate negative for x seconds,
                                    Bit 6 - IO Board communciation error,
															      Bit 7 - EEPROM writes finished, so generate an error*/
    U8 Warning_flags;              /*Bit 0-Low speed warning,
                                    Bit 1-High speed warning,
                                    Bit 2-Zero cal changed significantly warning,
                                    Bit 3-Sensor board 1st missed communication warning,
                                    Bit 4-1st missed communication with scale warning,
                                    Bit 5-Low rate warning,
                                    Bit 6-High rate warning*/
    U16 Err_Wrn_log_written;       /*Bit 0-Belt speed is zero,
                                    Bit 1 - Load cell error,
                                    Bit 2 - Angle sensor error,
                                    Bit 3 - Sensor board communication error,
                                    Bit 4 - Scale communication error,
                                    Bit 5 - Rate negative for x seconds,
                                    Bit 6 - IO Board communciation error
                                    Bit 7 - Low speed warning,
                                    Bit 8 - High speed warning,
                                    Bit 9 - Zero cal changed significantly warning,
                                    Bit 10- Sensor board 1st missed communication warning,
                                    Bit 11- 1st missed communication with scale warning,
                                    Bit 12- Low rate warning,
                                    Bit 13- High rate warning*/
		U8 Plant_connect_record_flag; /* Flag to indicate Plant connect record flag
		                                Bit 0 - Flag for new record available
																		Bit 1 - Flag for old record to be read
																		Bit 2 - Flag to indicate that the old record request has been entered*/
		U8 MAC_Programming_Error;      
		                                   //0x01 ----  NO Error
                                       //0x02 ----  MAC ERROR:MACfile.txt file not found
                                       //0x03 ----  MAC ERROR:USB Key not found
																	     //0x04 ----  MAC ERROR:Data in file is invalid
                                       //0x05 ----  MAC ERROR:Verification Failed */
  }FLAGS_STRUCT_DEF;
	

typedef struct {
	int Max_count_int ;
	int Max_size_int ;
	int Def_size_int ;
}prop_file_ini;
//SKS <- created the following structure to 
//read config values from config.ini in USB
//and clamp the Integrator Term and SUM values accordingly

typedef struct {
	float I_term ;
	float I_sum;
}PID_integrator_struct;


typedef enum
            {
              Printer = 1,
              Scoreboard,
            }UART_CONN;

typedef enum
            {
              set_baud_rate = 1,
              set_data_bits,
              set_stop_bits,
              set_flow_control,
            }PARAM_TYPE;
/*============================================================================
*Public Variables
*===========================================================================*/
/* OS Related!	*/
extern OS_MUT USB_Mutex,Debug_uart_Mutex;
						
						
/* Constants section */

/* Boolean variables section */
extern unsigned char usb_connect_done_fg,one_time_flash_restore_fg,usb_on_fg;
extern unsigned char flash_restore_to_usb_fg,flash_restore_to_usb_skip_fg;	
extern unsigned char Power_on_auto_zero_cal_fg,Auto_zero_Start_flag;
extern BOOL Dynamic_zero_cal_done_fg ;						
/* Character variables section */
	extern U8 U8GlobalMBCount;
	extern U16 nooftimes,nooftimestemp,Record_no;
	extern double daily_weight_at_1;			
  extern const char FirmwareVer[6];
  extern U8 con, con_ex;
	extern U8 Run_Screen_Flags;		
  extern U8 Zero_key_press_flag;		// zero key press flag
	extern U8 Clr_totals_key_press_flag;	// Clr key press flag for totals
	extern U8 Blending_Rate_Cntrl_Coil_status; // Coil status of Blending and Rate Control, it is turned ON
                                             // if blending or rate control runmode screen is seleted.			
extern U8 Task_section; // testing pupose						
extern char send_debug;
extern char send_debug_char;
extern int debugConfig;
#ifdef DEBUG_PORT
extern char g_buf[];
#endif

  //extern U8 GUI_structure_backup;
/* unsigned integer variables section */
extern unsigned int USB_err_no;
extern unsigned int HDR_Written;
extern char USB_ERR[5][35];
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */
extern float Ref_Zero_weight,Old_auto_zero_offset,New_auto_zero_offset,Zero_wt_tolerance_low,Zero_wt_tolerance_high;
extern float New_zero_wt,Old_zero_wt,Idler_spacing,offset1,Calc_zero_wt;
extern float LenPulses1, CalPulses1,ZeroWeight1;

/* Double variables section */

/* Structure or Union variables section */

 extern FLAGS_STRUCT_DEF Flags_struct;
 union uint_union  {
   unsigned int  uint_val;
   unsigned char uint_array[2];
 };

 union ulong_union  {
   unsigned long ulong_val;
   unsigned char ulong_array[4];
};

 union float_union  {
   float         float_val;
   unsigned char f_array[4];
 };
 
 union double_union  {
   double         double_val;
   unsigned char  d_array[8];
 };
typedef union 
{
	  unsigned int value;
    unsigned char int_array[4];
}UINT32_DATA;

//By DK on 5May2016 to fix Kinder Morgan issue of not getting -ve Total
typedef union 
{
	  int value;
    unsigned char int_array[4];
}INT32_DATA;


/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void set_uart_param(unsigned int value, U8 parameter, U8 printer_scoreboard);
extern void Write_Debug_Logs_into_file(int MsgNo);
extern void delete_widgets_mem_free (void);
//extern static BOOL ReadInfo (U8  , Media_INFO *info);
extern void key_scan(void);
extern void Power_on_auto_zero_cal (void);
extern void Auto_zero_cycle(void);

#endif /*__GLOBAL_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
