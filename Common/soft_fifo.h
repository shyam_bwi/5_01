/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : soft_fifo.h
* @brief               : Controller Board
*
* @author              : R. Venkata Krishna Rao
*
* @date Created        : 20th  November wed, 2013  <Nov 20, 2013>
* @date Last Modified  : 20th  November wed, 2013  <Nov 20, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef _SOFT_FIFO_H_
#define _SOFT_FIFO_H_
/*============================================================================
* Include Header Files
*===========================================================================*/


/*============================================================================
* Public Macro Definitions
*===========================================================================*/
#define Init_FIFO(t_FIFO) memset(&t_FIFO,0,sizeof(t_FIFO));
#define Is_FIFO_Empty(t_FIFO) (((t_FIFO)->count==0)?1:0)
#define Is_FIFO_Full(t_FIFO)  (((t_FIFO)->count==SOFT_FIFO_SIZE)?1:0)
/* Defines Section */
#define SOFT_FIFO_SIZE 120
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
{
	float contents[SOFT_FIFO_SIZE];
	unsigned char count;
	unsigned char front;
}Soft_FIFO;
extern Soft_FIFO blend_FIFO;

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern int Push_FIFO(Soft_FIFO*,float);
extern float Pop_FIFO(Soft_FIFO*,float* );
#endif
