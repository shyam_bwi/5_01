/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Belt-Way Scales Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Belt-Way Scales Inc 
* @copyright in this software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Belt-Way Scales Inc.
*
* @detail Project         : Belt Scale Weighing Product
* @detail Customer        : Belt-Way Scales Inc
*
* @file Filename 		   		: Version.h
* @brief			       			: 
*
* @author			       			: Anagha Basole
* 
* @date Created		       	: Month Day, Year  <Feb 8, 2012>
* @date Last Modified	   	: Month Day, Year  <Feb 8, 2012>
*
* @internal Change Log	  :
* @internal 			   			: <YYYY-MM-DD>
* @internal 			   			:
*
* Version History:
* Version 01.69   In this firmware we have resolved PID control issue.
* Version 01.68   In this firmware we have resolved issues BS23, BS24(added these options to setup  wizard )
* Version 01.67   In this firmware we have resolved issues BS22, BS25. Considering the inconvenience of our customers to update firmware version 1.66 using two hex files we have come up with a single hex file upgrade solution. Now we can upgrade the scale with older firmware versions directly with this single hex file. 
* Version 01.66   In this firmware�s we have resolved following issues BS21, BS23, BS24, BS25, BS26, BS28, BS29, BS30, this firmware version is compatible with IO Board firmware version.
* Version 01.65   In this version we have performed one minor change relating to PID control, now experiment 5 is working fine
* Version 01.64   In this version we have done minor modification related to pulsed output. now editing Target rate ro set point variable will affect both the variables. we are liminting Iterm accumulation for negative values aswell
* Version 01.63   In this version we have provided fix for Pulsed output and Quadrature outputs, implemented moving average rate buffering
* Version 01.62   In this version we have provided fix for line 158 (Negative Rate error), also did changes to PID control logic 
* Version 01.61   In this version we have provided fix for Output drop issue for rate control and issues in row 157, 159
* Version 01.60   We have performed changes related to PID controls, Out is getting dropped for rate control periodically
* Version 01.59   We have performed changes related to PID controls
* Version 01.58   We have enabled blending and rate control, did changes related to pulsed output logic of IO
* Version 01.57   We have implemented fix for ethernet issue related plant connect, changes made for PID control logic 
* Version 01.56   We have implemented check sum filed for MAC ID programming for data integrity.
* Version 01.55   We have implmented ERROR handling for MAC ID programming, changes made in brownout detection logic.
* Version 01.54   We have Implemented MAC ID programming in Scale during production option.
* Version 01.53   We have Implemented some changes related PID logic for blending and rate control
* Version 01.52   Enabled Ethernet which was earlier Disabled
* Version 01.51		Resolved Issues mentioned in row 147, 148, 156 of Excel sheet.
* Version 01.50		Resolved Issues mentioned in row 141(batching/loadout) of Excel sheet.
* Version 01.49		Resolved Issues mentioned in row 138-145 of Excel sheet and Corrected Daily, Weekly, Monthy Totals Implementation.
* Version 01.48   In this version we have resolved issues of line 131,133,134,135,136 of feedback response excel document.
* Version 01.47   From this version, version number will be format 01.xx to indicate production release version of firmware,
*                 In this version we have provided fix for issues in line 124,126 and  two issues found during  our in-house testing.                   
* Version 00.46   Resolved Issues mentioned in row 125-128  of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status 
* Version 00.45		Resolved Issues mentioned in row 121-124  of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status 
* Version 00.44		Added New feature which is Backdoor Password as per beltway Request.
* Version 00.43   Resolved Issues mentioned in row 118  of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status 
* Version 00.42		Resolved issues or modifications mentioned in row 109-116 of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status 
* Version 00.41		Resolved issues or modifications mentioned in row 95,100,105,106,107 of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status 
* Version 00.40		Performed minar modification related to issue in row 104 of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status 
* Version 00.39   Resolved issues or or modifications mentioned in row 104,94,82,81,100,67-72,55-58,101 of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status 
* Version 00.38   Resolved issues or modifications mentioned in row 22,50,89-91 of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
* Version 00.37   Fixed one minor bug related to Blending and Rate control, 
*                 Resolved issues mentioned in row 82,86-93 of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
* Version 00.36   Fixed one minor bug related to Blending and Rate control, 
*                 Resolved issues mentioned in row 83,84,85 of Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
* Version 00.35   Changes done by Venkata Krishna Rao a
*                 reimplemented Screenshot and Blending, Rate Control logic algorithms
* Version 00.34   done changes by pandurang at beltway on 18th - 22th Nov 2013
*                 Resolved All the issues raised by customer mentioned in Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
*									Row-59,63,66,74,75,78,79,80 and New issues need to resolved
* Version 00.33   done changes by pandurang at beltway on 11th -15th Nov 2013
*                 Resolved All the issues raised by customer mentioned in Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
*									Row-37,52,53,62,New issues need to resolved
* Version 00.32   done changes by pandurang at beltway on 4th -8th Nov 2013
*                 Resolved All the issues raised by customer mentioned in Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
*									New issues need to resolved
* Version 00.31   Sent on 30th Oct 2013
*                 Resolved All the issues raised by customer mentioned in Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
*									New issues need to resolved
* Version 00.30   Sent on 
*                 Resolved All the issues raised by customer mentioned in Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status

* Version 00.29   Sent on 27th September 2013
*                 Resolved All the issues raised by customer mentioned in Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
* Version 00.28   Sent on 20th September 2013
*                 Resolved some of issues raised by customer mentioned in Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
* Version 00.27   Sent on 17th September 2013
*                 Added feature of version number in HEX file, Resolved some of issues raised by customer mentioned in 
*                 Excel sheet: 2013-09-10 Integrator Firmware Feedback_Response_status
* Version 00.26   Sent on 12th September 2013
*                 Modified the Connection flag and communication flag names for ethernet and USB connection detection.
*                 Also modified all the double variables to float as per beltway's suggestion.
*                 Plant connect changes added. Also modified the Modbus TCP structures according to the latest changes.
*
* Version 00.25   Sent on 14th August 2013
*                 Disabled the DEBUG_PORT #define
*                 Modified the DAC calculations by addition of a float variable due to rounding down error.
*                 Initialized all report variables on bootup, since the report structure is not reloaded from
*                 EEPROM, so can have garbage values.
*                 Also added code for external interrupt, but is disabled in this version, since h/w is not ready.
*                 Modified the status variable for analog output once enabled.
*
* Version 00.24   Sent on 1st August 2013
*                 Enabled the boot loader and made the required changes in scatter file, screen11.c and startup file
*                 Increased the size of the version array to 6 since there was no space to store null.
*                 Rate control and blending control calculation implementation.
*                 Implemented the changes required to meet customer modifications for ver 00.23
*
* Version 00.23   Sent on 18th July 2013
*                 Modified the logic for dynamic ip changes through modbus tcp.
*                 Changed the maximum handles for events in mbportevent.c to 5 from 3.(since modbus tcp is also added)
*                 Modified the test weight calibration percent calculation formula, by shifting 100 to numerator.
*
* Version 00.22   Sent on 17th July 2013
*                 Made the keypad and number pad change to see the selected button properly.
*                 Modified the PHY read and write timeouts.
*                 Modified the keypad functionality not to send the button pressed to the UI if the 
*                 screen is blacked out.
*                 Implemented dynamic TCP configuration functionality.
*
* Version 00.21   Sent on 16th July 2013
*                 Modbus TCP functionality compiled.
*                 Modification made to ensure that the port is released when the master disconnects
*                 Made the changes for comments on version 00.20
*                 Integrator to integrator communication functionality added.
*
* Version 00.20   Sent on 4th July 2013
*                 Modbus TCP implementation.
*                 Modifications made for all the comments given by beltway for ver.00.18 firmware
*                 Function creation for common functions between modbus TCP and screens.
*                 Priority reset for all tasks(Macro creation).
*                 Calculation optimization changes and file split for conversion functions.
*                 Variable optimisation according to the comments provided by Beltway.
*                 Made printer string modifications.
*
* Version 00.19   Sent on 28th June 2013
*                 Modifications made in angle calculation
*                 Expansion slot driver layer written(NOT TESTED).
*
* Version 00.18   Sent on 14th June 2013
*                 Priority for modbus sensor task changed to highest
*
* Version 00.17   Sent on 31st May 2013
*                 Disable the clearing of the accumulated weight after calibration
*
* Version 00.16   Sent on 31st May 2013
*                 Made test weight calibration changes.
*                 Also added the user_belt_speed scale setup var to the conversion function
*
* Version 00.15   Sent on 28th May 2013
*                 Made the Run mode screen font bigger(rate and weight screens)
*                 Modified the rate formula (reverted back the ver 00.14 ver formula and added
*                 the weight conversion factor)
*                 Implemented the weight unit screen changes as per suggestion by beltway
*                 Also added the conversion functions for variables to accomodate run-time
*                 unit changes
*
* Version 00.14   Sent on 23rd May 2013
*                 String modifications made as per beltway suggestion
*                 Change in rate calculation
*
* Version 00.13   Sent on 22nd May 2013
*                 Belt Length calibration changes
*
* Version 00.12   Sent on 21st May 2013
*                 Distance unit selection issue fixed
*
* Version 00.11   Sent on 17th May 2013
*                 Implemented calculation changes as discussed in call.
*                 Implemented printer and scoreboard changes as suggested.
*                 Implemented all the new run mode screens.
*
* Version 00.10   Sent on 13th May 2013
*
* Version 00.09   Sent on 3rd May 2013
*                 Added few changes from screen variable document 12.
*                 Compiled IO board code changes.
*                 Added the zero rate and negative rate changes.
*                 Optimized the screen strings array.
*                 Optimized the screen layout and added the display of the secondary
*                 functionality.
*
* Version 00.08   Sent with the board on 29th April 2013
*                 Firmware Upgrade file display screen added. Added the conversion functions.
*                 Added, tested and commented the boatloader functionality.
*                 Modified the report writing functions and also added the error logging function.
*                 
* Version 00.07   Sent on 19th April 2013
*                 Added the Multiedit text box size changes according to the data.
*                 Modified the printer and scoreboard tasks. Also added the calibration
*                 routines to the modbus task and moved the logging to the lcd task.
*
* Version 00.06   Sent on 08th April 2013
*	                Linking of Password screen
*	                Border for all screens(Optimization)
*	                LCD refresh rate
*	                Calibration functions
*
* Version 00.05   sent on 22nd march 2013
*
* Version 00.04   sent on 15th march 2013
*
* Version 00.03	- Added the 30 sec and 15 sec timeout for the keyboard, in case of
*									no button press.
*									Implemented the data logging to the flash routines.
*									Resolved the USB run time initialization issue.
*									Added the error countdown for the sensor board timeout.
*									Tested the Ethernet(Modbus over TCP, DHCP and SMTP protocols) basic
*							    functionality as tested on the eval board successfully.
*									Also added the global flag structure.
*									Released on 4th March 2013-not sent to beltway
*
* Version 00.02 - Adjusted the formula for rate and speed.
*									Integrated IO_board variables.
*									Integrated and tested Flash with SRAM.
*									Defined GUI for conditionally compiling LCD_GUI functionality.
*									Added the screen configuration backup functionality from and to flash.
*									Modified the GUI code to reflect the changes made in the VC code.
*									Modified the report files according to customer comments.
*									Added user stacks to some tasks.
*									Modified the USB functions to calculate the the data to be logged
*									Added a line to ignore the first ever packet received from the 
*									sensor board, since garbage data was being sent.
*									Released on 18th Feb 2013
*
* Version 00.01 - First release version to Beltway.(8th Feb 2013)
*
*****************************************************************************/
#ifndef __VERSION_H 
#define __VERSION_H
/*============================================================================
* Include Header Files
*===========================================================================*/
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */
const char FirmwareVer[6] = "01.71";
/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
#endif 
/*****************************************************************************
**                            End Of File
******************************************************************************/
