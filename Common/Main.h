/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Main.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 12, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __MAIN_H
#define __MAIN_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "rtc.h"
#include "Screen_structure.h"
#include <Net_Config.h>
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

extern RTCTime Shutdown_time;

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
extern BOOL OS_Init_Done_Flag ;
extern BOOL Power_Down_Set_Flag ;
/* Character variables section */
extern U8 Power_On_flag;
extern char Syslog_Get_Line_Buff[1000];
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */
#ifdef ETHERNET
  extern U32  dhcp_tout;
#endif
/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
#ifdef ETHERNET
 extern LOCALM localm[];
#endif
/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif /*__MAIN_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
