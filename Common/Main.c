/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename       : Main.c
 * @brief               : Controller Board
 *
 * @author              : Anagha Basole
 *
 * @date Created        : July Thursday, 2012  <July 12, 2012>
 * @date Last Modified  : July Thursday, 2012  <July 12, 2012>
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal            :
 * @internal            :
 *
 *****************************************************************************/

/*============================================================================
 * Include Header Files
 *===========================================================================*/
#include <stdio.h>
#include "Global_ex.h"
#include "Display_task.h"
#include "RTOS_main.h"
#include "Main.h"
#include "USB_main.h"
#include "SMTP_demo.h"
#include "Keypad.h"
#include "I2C_driver.h"
#include "Emc_config.h"
#include "Ext_flash_low_level.h"
#include "Ext_flash_high_level.h"
#include "File_update.h"
#include "Sensor_board_data_process.h"
#include "Screen_global_ex.h"
#include "Ext_Data_flag.h"
#include "EEPROM_high_level.h"
#include "lpc177x_8x_bod.h"
#include "LCDConf.h"
#include "soft_fifo.h"
#include "Rate_blending_load_ctrl_calc.h"
#include "Log_report_data_calculate.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "float.h"
#include "Conversion_functions.h"
#include "MVT_Totals.h"
#include "MVT.h"
#include "Firmware_upgrade.h"
#include "wdt.h"
#include "lpc177x_8x_iap.h"
//#include "lcd_driver.h"

/*============================================================================
 * Private Macro Definitions
 *===========================================================================*/
#define NVIC_VTOR           (*(volatile U32*)(0xE000ED08))

#define EXT_INT

#ifdef EXT_INT
#if 1
#define TEST_GPIO_P2_31                     0x80000000
#endif

//#define DEBUG_PORT
//#define DEBUG_DATA_SEND

//#define EXT_FLASH_LAST_SECTOR_START_ADDR     0x807F0000		//moved to Ext_flash_high_level.h
//#define EXT_FLASH_LAST_SECTOR_END_ADDR       0x807FFFFF		//moved to Ext_flash_high_level.h
U16 * read_flash;
union ulong_union buffer_long;
#endif
RTCTime Shutdown_time;
/*============================================================================
 * Private Data Types
 *===========================================================================*/

/*============================================================================
 *Public Variables
 *===========================================================================*/
/* Constants section */
#ifdef DEBUG_PORT
char g_buf[50];
#endif
/* Boolean variables section */

/* Character variables section */
char Syslog_Get_Line_Buff[1000] __attribute__((section ("COMMON_DATA_RAM"),zero_init));
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
 * Private Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL OS_Init_Done_Flag = 0;
BOOL Power_Down_Set_Flag = 0;

/* Character variables section */
U8 con, con_ex;
U8 Task_section = 0;
U8 Error_Task = 0;
U8 Ready_Write = 0;
U8 Power_On_flag = 1;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */
#ifdef ETHERNET
U32 dhcp_tout;
#endif
/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */
extern void Weight_unit_conv_func(void);
/*============================================================================
 * Private Function Prototypes Declarations
 *===========================================================================*/
//By DK on 16 Feb 2016
void Read_ErrorLog_RecId(void);
void Create_Errorlog_file_and_Log_events(void);
/*============================================================================
 * Function Implementation Section
 *===========================================================================*/
/*****************************************************************************
 * @note       Function name: void EINT0_IRQHandler (void)
 * @returns    returns      : None
 * @param      arg1         : None
 * @author                  : Anagha Basole
 * @date       date created : July 25, 2013
 * @brief      Description  : Brownout Interrupt Handler
 * @note       Notes        : None
 *****************************************************************************/
#ifdef EXT_INT
void EINT0_IRQHandler(void)
{
    U16 * read_flash_addr;
    union double_union buffer;
    union float_union buffer_float;
    U16 * u16Var, Block_Length, Calculated_CRC = 0;
    int i;
    U32 addr;

    LPC_SC->EXTINT = 0x00000001; /* clear interrupt */
		LPC_GPIO1->DIR |=0x01<<5;
 		LPC_GPIO1->SET |=0x01<<5;
		LPC_GPIO1->DIR |=0x01<<5;
 		LPC_GPIO1->CLR |=0x01<<5;
    Power_Down_Set_Flag = 1; //SKS -> resolving issue of totals losing on power cycle
    if (Ready_Write == 1)
    {
        //Turn off the backlight
        LPC_GPIO2->CLR |= (1 << LCD_BACKLIGHT_BIT); // Set backlight to off
        __disable_irq();
        //NVIC_DisableIRQ(EINT0_IRQn);
        MATRIX_ARB |= 0x00010000;

        OS_Init_Done_Flag = 0; //By DK on 4 May2016 for resolving issue of totals losing on power cycle

        //PVK
        addr = EXT_FLASH_LAST_SECTOR_START_ADDR;

        //LPC_GPIO2->SET |= 0x80000000;
        buffer.double_val = Calculation_struct.Total_weight_accum;
        //Store the accumulated weight into flash at address of flag + 1
        read_flash_addr = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 4);
        u16Var = (U16*) &buffer;
        //LPC_GPIO2->SET |= TEST_GPIO_P2_31;
        for (i = 0; i < FLASH_SIZEOF(buffer); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //Backup of Job Total
        buffer.double_val = Rprts_diag_var.Job_Total;
        read_flash_addr = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 12);
        u16Var = (U16*) &buffer;
        for (i = 0; i < FLASH_SIZEOF(buffer); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //Backup of Master Total
        buffer.double_val = Rprts_diag_var.Master_total;
        read_flash_addr = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 20);
        u16Var = (U16*) &buffer;
        for (i = 0; i < FLASH_SIZEOF(buffer); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //Backup of Daily total
        buffer.double_val = Rprts_diag_var.daily_Total_weight;
        read_flash_addr = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 28);
        u16Var = (U16*) &buffer;
        for (i = 0; i < FLASH_SIZEOF(buffer); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //Backup of weekly total
        buffer.double_val = Rprts_diag_var.weekly_Total_weight;
        read_flash_addr = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 36);
        u16Var = (U16*) &buffer;
        for (i = 0; i < FLASH_SIZEOF(buffer); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //Backup of monthly total
        buffer.double_val = Rprts_diag_var.monthly_Total_weight;
        read_flash_addr = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 44);
        u16Var = (U16*) &buffer;
        for (i = 0; i < FLASH_SIZEOF(buffer); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //Backup of Yearly total
        buffer.double_val = Rprts_diag_var.yearly_Total_weight;
        read_flash_addr = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 52);
        u16Var = (U16*) &buffer;
        for (i = 0; i < FLASH_SIZEOF(buffer); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //LPC_GPIO2->CLR |= 0x80000000;
        /********************************************************************/
        //store the local set point
        u16Var = (U16*) &PID_Old_Local_Setpoint;
        for (i = 0; i < FLASH_SIZEOF(PID_Old_Local_Setpoint); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //store the current time
        u16Var = (U16*) &Current_time;
        for (i = 0; i < FLASH_SIZEOF(Current_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        if (!(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR)
                && ((daily_rprt.Dummy_var1 & 0x0002) == 0x0002))
        {
            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
            {
                daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
                daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
                daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
            }
            else if (Admin_var.Current_Time_Format == TWELVE_HR)
            {
                if (Admin_var.AM_PM == AM_TIME_FORMAT)
                {
                    if ((U8) Current_time.RTC_Hour == 12)
                        daily_rprt.End_time.Hr = 0;
                    else
                        daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
                    daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
                    daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
                }
                else
                {
                    daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour + 12;
                    daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
                    daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
                }
            }
            daily_rprt.Cnt_zero_speed++;
            weekly_rprt.Cnt_zero_speed++;
            monthly_rprt.Cnt_zero_speed++;
            daily_rprt.Dummy_var1 &= 0xFF00;
            daily_rprt.Dummy_var1 |= 0x0004;
        }
        u16Var = (U16*) &daily_rprt.Cnt_zero_speed;
        for (i = 0; i < FLASH_SIZEOF(daily_rprt.Cnt_zero_speed); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //store the start time for zero speed
        u16Var = (U16*) &daily_rprt.Start_time;
        for (i = 0; i < sizeof(daily_rprt.Start_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //store the end time for zero speed
        u16Var = (U16*) &daily_rprt.End_time;
        for (i = 0; i < sizeof(daily_rprt.End_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //store the downtime
        u16Var = (U16*) &daily_rprt.Down_time;
        for (i = 0; i < FLASH_SIZEOF(daily_rprt.Down_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //store the start load time
        u16Var = (U16*) &daily_rprt.Start_load_time;
        for (i = 0; i < sizeof(daily_rprt.Start_load_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        /*if((Calculation_struct.Rate > Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
         || (Calculation_struct.Rate > 0 && Admin_var.Zero_rate_status == Screen6171_str5))*/
        //By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152
        if ((Admin_var.Zero_rate_status == Screen6171_str2)
                || (Calculation_struct.Rate_for_display > 0
                    && Admin_var.Zero_rate_status == Screen6171_str5)) //BS-152 fixed by DK on 27 April 2016
        {
            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
            {
                daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour;
                daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
                daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
            }
            else if (Admin_var.Current_Time_Format == TWELVE_HR)
            {
                if (Admin_var.AM_PM == AM_TIME_FORMAT)
                {
                    if ((U8) Current_time.RTC_Hour == 12)
                        daily_rprt.End_load_time.Hr = 0;
                    else
                        daily_rprt.End_load_time.Hr =
                            (U8) Current_time.RTC_Hour;
                    daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
                    daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
                }
                else
                {
                    daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour
                                                  + 12;
                    daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
                    daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
                }
            }
            daily_rprt.Dummy_var1 &= 0x0000;
            daily_rprt.Dummy_var1 |= (0x0304);
        }
        u16Var = (U16*) &daily_rprt.End_load_time;
        for (i = 0; i < sizeof(daily_rprt.End_load_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        u16Var = (U16*) &daily_rprt.Run_time;
        for (i = 0; i < FLASH_SIZEOF(daily_rprt.Run_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //---------------weekly report------------------------
        //weekly report zero speed counter
        u16Var = (U16*) &weekly_rprt.Cnt_zero_speed;
        for (i = 0; i < FLASH_SIZEOF(weekly_rprt.Cnt_zero_speed); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //weekly report down time
        u16Var = (U16*) &weekly_rprt.Down_time;
        for (i = 0; i < FLASH_SIZEOF(weekly_rprt.Down_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //store weekly report run time
        u16Var = (U16*) &weekly_rprt.Run_time;
        for (i = 0; i < FLASH_SIZEOF(weekly_rprt.Run_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //----------------------monthly report---------------------------
        //monthly report count zero speed
        u16Var = (U16*) &monthly_rprt.Cnt_zero_speed;
        for (i = 0; i < FLASH_SIZEOF(monthly_rprt.Cnt_zero_speed); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //monthly report down time
        u16Var = (U16*) &monthly_rprt.Down_time;
        for (i = 0; i < FLASH_SIZEOF(monthly_rprt.Down_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //store monthly report run time
        u16Var = (U16*) &monthly_rprt.Run_time;
        for (i = 0; i < FLASH_SIZEOF(monthly_rprt.Run_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //extra write for writing the special id
        u16Var = (U16*) &monthly_rprt.Run_time;
        for (i = 0; i < FLASH_SIZEOF(monthly_rprt.Run_time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //------------------------Report Header Daily Date and Time-----------------
        u16Var = (U16*) &daily_rprt.freq_rprt_header.Date;
        for (i = 0; i < FLASH_SIZEOF(daily_rprt.freq_rprt_header.Date); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        u16Var = (U16*) &weekly_rprt.freq_rprt_header.Date;
        for (i = 0; i < FLASH_SIZEOF(weekly_rprt.freq_rprt_header.Date); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        u16Var = (U16*) &monthly_rprt.freq_rprt_header.Date;
        for (i = 0; i < FLASH_SIZEOF(monthly_rprt.freq_rprt_header.Date); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        u16Var = (U16*) &daily_rprt.freq_rprt_header.Time;
        for (i = 0; i < FLASH_SIZEOF(daily_rprt.freq_rprt_header.Time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //------------------------Report Header Weekly Date and Time-----------------

        u16Var = (U16*) &weekly_rprt.freq_rprt_header.Time;
        for (i = 0; i < FLASH_SIZEOF(weekly_rprt.freq_rprt_header.Time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //------------------------Report Header Monthly Date and Time-----------------

        u16Var = (U16*) &monthly_rprt.freq_rprt_header.Time;
        for (i = 0; i < FLASH_SIZEOF(monthly_rprt.freq_rprt_header.Time); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        daily_rprt.Dummy_var1 |= 0x8000;
        u16Var = (U16*) &daily_rprt.Dummy_var1;
        ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
        read_flash_addr++;
        /********************************************************************/
        u16Var = (U16*) &periodic_data.Periodic_hdr.Id;
        ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
        read_flash_addr++;

        u16Var = (U16*) &set_zero_data_log.Set_zero_hdr.Id;
        ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
        read_flash_addr++;

        u16Var = (U16*) &clear_data_log.Clr_data_hdr.Id;
        ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
        read_flash_addr++;

        u16Var = (U16*) &calib_data_log.Cal_data_hdr.Id;
        ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
        read_flash_addr++;

        u16Var = (U16*) &length_cal_data_log.Len_data_hdr.Id;
        ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
        read_flash_addr++;

        u16Var = (U16*) &error_data_log.Err_data_hdr.Id;
        ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
        read_flash_addr++;

        buffer_float.float_val = (float) Calculation_struct.run_time;
        u16Var = (U16*) &buffer_float;
        for (i = 0; i < FLASH_SIZEOF(buffer_float); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }

        //Scale setup weight unit
        u16Var = (U16*) &Scale_setup_var.Weight_unit;
        for (i = 0; i < FLASH_SIZEOF(Scale_setup_var.Weight_unit); i++)
        {
            ExtFlash_writeWord((uint32_t) read_flash_addr, *u16Var++);
            read_flash_addr++;
        }
        //PVK
        addr = (uint32_t) read_flash_addr;

        read_flash_addr = (U16 *) EXT_FLASH_LAST_SECTOR_START_ADDR;
        ExtFlash_writeWord((uint32_t) read_flash_addr, BACKUP_PRESENT_FLAG);

        //PVK
        Block_Length = (addr - EXT_FLASH_LAST_SECTOR_START_ADDR);
        CalcCRC((unsigned char*) EXT_FLASH_LAST_SECTOR_START_ADDR,
                (unsigned char*) &Calculated_CRC, Block_Length);
        read_flash_addr = (U16 *) addr;
        //Write CRC
        ExtFlash_writeWord((uint32_t) read_flash_addr, Calculated_CRC);
//	 Write_Debug_Logs_into_file(POWER_OFF_SYSLOG_EVENT);
    }

    //LPC_GPIO2->CLR |= 0x80000000;
    //LPC_GPIO4->SET	|= 0x01000000;
		funinit("U0:");
		LPC_GPIO1->DIR |=0x01<<5;
 		LPC_GPIO1->SET |=0x01<<5;
    while (1)
    {
        // if normal condition is restored then reset the controller
        if ((LPC_GPIO0->PIN & EXT_INT_PIN) != 0)
        {
            //Resets the Controller through Watchdog timer method
            NVIC_SystemReset();
            break;
        }
    }
    __enable_irq();
}
#endif

void HardFault_Handler(void)
{

    char buf[30];
    OS_TID err_task;
    memset(buf, 0, sizeof(buf));
    err_task = isr_tsk_get();
    Error_Task = err_task;
    sprintf(buf, "\r\nHardfault,TSK_ID:%d, TSK_SEC:%d\n", err_task,
            Task_section);

#ifdef DEBUG_PORT
    Uart_i2c_data_send((uint8_t *) buf, strlen(buf));
#endif
    NVIC_SystemReset();
    while (1)
        ;
}

/*****************************************************************************
 * @note       Function name: int main (void)
 * @returns    returns      : None
 * @param      arg1         : None
 * @author                  : Anagha Basole
 * @date       date created : July 12, 2012
 * @brief      Description  :
 * @note       Notes        : None
 *****************************************************************************/
extern int checkUSB;
int WDT_EN =0;
int badethernet_flag;
void peripheral_reset(void)
{
    //  LPC_GPIO3->DIR |=0x01<<30;
    //  LPC_GPIO3->SET |=0x01<<30;
    LPC_IOCON->P0_23 &=(~0x3<<3) ;
    LPC_IOCON->P0_23 |=0x01<<3 ;
    LPC_IOCON->P3_30 &=(~0x3<<3) ;
    LPC_IOCON->P3_30 |=0x01<<3 ;
		
		//SKS 16/08/2017
    LPC_RTC->CALIBRATION =0;
    LPC_RTC->CCR&=~(0x01<<4);
    if (((LPC_GPIO0->PIN) &(0x01<<23))==(0x01<<23))
    {
      //  WDT_EN =1;
     //   WDTInit();
			WDT_EN =0;
        //	LPC_RTC->ALDOY &=(~0x01);
    }
    else
    {
        WDT_EN =0;
        return;
    }
    LPC_GPIO3->DIR |=0x01<<30;
    LPC_GPIO3->SET |=0x01<<30;
    DelayMs(100);
    LPC_GPIO3->CLR |=0x01<<30;
    DelayMs(100);
    LPC_GPIO3->SET |=0x01<<30;

		
		LPC_GPIO1->DIR |=0x01<<5;
 		LPC_GPIO1->SET |=0x01<<5;
}
unsigned int HDR_Written;
extern int corrupt_usb_Flag;
int main (void)
{
    U16 Data_valid;
    U8 Last_Admin_AM_PM = 0;
    U16 Block_Length,Calculated_CRC = 0;
    U32 addr;
    //Only for testing by DK on 27 Jan 2016
//	double test_var;

#ifdef EXT_INT
    U16 i=0,j=0, read_buffer[30];
    union double_union buffer;
    union ulong_union buffer_long;
    union float_union buffer_float;
    FILE *fp;
//    char file_name[FILE_NAME_SIZE];
    U32 addr_flash = 0;
    U16 record_flag = 0;
#else
    U16 * read_flash;
    IoRequest_Para.PulseCount = 0;
    Ready_Write = 0;
    daily_rprt.Dummy_var1 = 0x0101;
#endif
//    Screen_org_data * current_screen_org_no;
 //   char disp[100],screen_no[6],screen_type,in_line[20];
    /*
    #ifdef DEBUG_PORT
    	unsigned char buf[30];
    #endif
    */
    HDR_Written =0;
    checkUSB =1;
    MATRIX_ARB = 0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
                 | (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
                 | (2 <<  2)  // PRI_DCODE : D-Code bus priority.
                 | (0 <<  4)  // PRI_SYS   : System bus priority.
                 | (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
                 | (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
                 | (3 << 10)  // PRI_LCD   : LCD DMA priority.
                 | (0 << 12)  // PRI_USB   : USB DMA priority.
                 | (1 << 16)  // ROM_LAT   : ROM latency select.
                 ;

    /**************************************************************/
// Software Stack Initilization
    Init_FIFO((Rate_blend_load.blend_FIFO));  // Initilization of Blending FIFO

    OS_Init_Done_Flag = 0;
    peripheral_reset();
//   WDTFeed();
#ifdef EXT_INT
//  keypad testing
//  PINSEL_ConfigPin (1, 5, 0);
// 	LPC_GPIO2->DIR |= TEST_GPIO_P1_05;
//  LPC_GPIO2->CLR |= TEST_GPIO_P1_05;
    LPC_SC->EXTINT = 0x00000001;					/*Clear interrupt*/
    PINSEL_ConfigPin (0, 29, 2);
    LPC_GPIOINT->IO0IntEnF = 0x20000000;	/* Port0.29 is falling edge. */
    LPC_SC->EXTMODE = 0x00000001;		      /* INT0 edge trigger */
    LPC_SC->EXTPOLAR = 0;				          /* INT0 is falling edge by default */
    NVIC_SetPriority(ENET_IRQn, 0x01);
    NVIC_EnableIRQ(EINT0_IRQn);
    LPC_SC->EXTINT = 0x00000001;		/* clear interrupt */
#endif
    /**************************************************************/
    //LPC_IOCON->P2_31 = 0x21;
// 	PINSEL_ConfigPin (2, 31, 0);
// 	LPC_GPIO2->DIR |= 0x80000000;
//   LPC_GPIO2->CLR |= 0x80000000;
//LPC_RTC->GPREG4 =0; //ethernet fail count

//WDTInit();

    for(addr=0; addr < 100000; addr++);
    for(addr=0; addr < 100000; addr++);
    for(addr=0; addr < 100000; addr++);
    for(addr=0; addr < 100000; addr++);
    for(addr=0; addr < 100000; addr++);
    for(addr=0; addr < 100000; addr++);
#ifdef DEBUG_PORT
    I2C_high_level_init(100000);
    WDTFeed();
#endif
#ifdef DEBUG_PORT
    sprintf(g_buf,"\r\nReboot\n");
    Uart_i2c_data_send((uint8_t *)g_buf, strlen(g_buf));
    WDTFeed();
#endif
#ifdef USB


//Added by DK on 3 March 2016


    if(funinit("U0:") ==0)
    {
#ifdef DEBUG_PORT
        sprintf(g_buf,"\r\n uninitializes the Flash File System");
        Uart_i2c_data_send((uint8_t *)g_buf, strlen(g_buf));
#endif
    }
    WDTFeed();
    for(addr=0; addr < 100000; addr++); //Added by DK on 3 March 2016
    usbh_engine_all(); //Added by DK on 3 March 2016
    WDTFeed();
    for(addr=0; addr < 100000; addr++); //Added by DK on 3 March 2016
    if (((LPC_RTC->ALDOY &(0x01<<2))==(0x01<<2))&&((LPC_SC->RSID & 0x04) == 0x04))
    {
        con =0;
        corrupt_usb_Flag =1;
				USB_err_no = 0x1E;
    }
    else
    {
        corrupt_usb_Flag =0;
        LPC_RTC->ALDOY |=0x01<<2;
        finit("U0:");
        LPC_RTC->ALDOY &=~(0x01<<2);

        WDTFeed();
        usbh_engine_all();
        //	 usbh_engine_all();
        con = usbh_msc_status(0, 0);
        USB_err_no = usbh_msc_get_last_error(0,0);
// con = init_msd ("U0:");
//   if ((LPC_RTC->ALDOY &(0x01<<1))==(0x01<<1))con =0;
        //  else
        if ((con==1)&&(USB_err_no ==0))
        {
            con =1;
            con_ex = con | 0x80;
        }
        else
        {
            con =0;
            corrupt_usb_Flag =0;
        }
    }
    //	}
    //	if ((con==1)&&(USB_err_no >0))
    //	;//corrupt pendrive
    if (con == 1)
    {

        Flags_struct.Connection_flags/*.Communication_flags*/|= USB_CON_FLAG;


        //Added by DK on 3 March 2016
#ifdef DEBUG_PORT
        sprintf(g_buf, "\r\n initializes the Flash File System");
        Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

    }
    else
    {
        Flags_struct.Connection_flags/*.Communication_flags*/&= ~USB_CON_FLAG;
    }
    WDTFeed();
#endif /*#ifdef USB*/

#ifdef KEYPAD
    keypad_init();
    WDTFeed();
#endif

#ifdef RTC
    // Initialize RTC module
    RTCInit();
    RTCStart();
    Current_time = RTCGetTime();
    WDTFeed();
    nooftimes = Current_time.RTC_Sec * 20; /* Match the nooftimes counter with the Current_time Counter */
    daily_rprt.freq_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
    daily_rprt.freq_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
    daily_rprt.freq_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
        daily_rprt.freq_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
    else if (Admin_var.Current_Time_Format == TWELVE_HR)
    {
        if (Admin_var.AM_PM == AM_TIME_FORMAT)
        {
            if ((U8) Current_time.RTC_Hour == 12)
                daily_rprt.freq_rprt_header.Time.Hr = 0;
            else
                daily_rprt.freq_rprt_header.Time.Hr =
                    (U8) Current_time.RTC_Hour;
            daily_rprt.End_time.AM_PM = AM;	//AM = 0		//Added on 21-Oct-2015 for storing last AM_PM setting
        }
        else
        {
            daily_rprt.freq_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour
                                                  + 12;
            daily_rprt.End_time.AM_PM = PM;	//PM = 1		//Added on 21-Oct-2015 for storing last AM_PM setting
        }
    }
    daily_rprt.freq_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
    daily_rprt.freq_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;

    weekly_rprt.freq_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
    weekly_rprt.freq_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
    weekly_rprt.freq_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
        weekly_rprt.freq_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
    else if (Admin_var.Current_Time_Format == TWELVE_HR)
    {
        if (Admin_var.AM_PM == AM_TIME_FORMAT)
        {
            if ((U8) Current_time.RTC_Hour == 12)
                weekly_rprt.freq_rprt_header.Time.Hr = 0;
            else
                weekly_rprt.freq_rprt_header.Time.Hr =
                    (U8) Current_time.RTC_Hour;
        }
        else
        {
            weekly_rprt.freq_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour
                                                   + 12;
        }
    }
    weekly_rprt.freq_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
    weekly_rprt.freq_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;

    monthly_rprt.freq_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
    monthly_rprt.freq_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
    monthly_rprt.freq_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
        monthly_rprt.freq_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
    else if (Admin_var.Current_Time_Format == TWELVE_HR)
    {
        if (Admin_var.AM_PM == AM_TIME_FORMAT)
        {
            if ((U8) Current_time.RTC_Hour == 12)
                monthly_rprt.freq_rprt_header.Time.Hr = 0;
            else
                monthly_rprt.freq_rprt_header.Time.Hr =
                    (U8) Current_time.RTC_Hour;
        }
        else
        {
            monthly_rprt.freq_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour
                                                    + 12;
        }
    }
    monthly_rprt.freq_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
    monthly_rprt.freq_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;

#endif
    if ((Calculation_struct.Total_weight_accum > DBL_MAX)
            && (Calculation_struct.Total_weight_accum < -(DBL_MAX)))
        Calculation_struct.Total_weight_accum = 0;
#ifdef GUI
#ifdef EEPROM
    //Initialize On chip EEPROM
    EEPROM_Init();
    WDTFeed();
    Flags_struct.MAC_Programming_Error = eeprom_program_MAC_id();

    if (Flags_struct.MAC_Programming_Error <= 1)
    {
        // Copy the MAC ID into String Array for display
        for (i = 0, j = 0; i < ETH_ADRLEN; i++, j += 3)
        {
            sprintf(MAC_String + j, "%0.2x", own_hw_adr[i]);
            if (j != 0)
                MAC_String[j - 1] = ':';
        }
    }

    //Read FW version
    Read_FW_Ver_From_EEPOM();
    WDTFeed();
    //PVK-#- Mar-2016
    EEPROM_Read (0/*ADMIN_START_PAGE_NO*/, ADMIN_START_PAGE_NO, &Admin_var, MODE_8_BIT, sizeof(Admin_var));
    EEPROM_Read(0/*ADMIN_START_PAGE_NO*/, MISC_START_PAGE_NO, &Misc_var,
                MODE_8_BIT, sizeof(Misc_var));
    if ((Misc_var.Language_select != Screen61B1_str0)
            && (Misc_var.Language_select != Screen61B2_str0))
    {
        Misc_var.Language_select = Screen61B1_str0;
        EEPROM_Write(0/*ADMIN_START_PAGE_NO*/, MISC_START_PAGE_NO,
                     &Misc_var.Language_select, MODE_8_BIT,
                     sizeof(Misc_var.Language_select));
    }
    strcpy(&Admin_var.Int_firmware_version[0], FirmwareVer);
    strcpy(&Admin_var.Int_update_version[0], FirmwareVer);

    error_data_log.Err_data_hdr.Id = 0;

    Shutdown_time = Current_time;

    switch (Misc_var.Language_select)
    {
    case Screen61B1_str0:
        Strings = Strings_english;
        break;
    case Screen61B2_str0:
        Strings = Strings_polish;
        break;

    }

//					Strings = Strings_english;
    if (__TRUE == ExtFlash_Init())
    {
        WDTFeed();
        read_flash = (U16 *) FLASH_BASE_ADDRESS;
        Data_valid = *read_flash;

        //read_flash_1 = (U16 *)EXT_FLASH_LAST_SECTOR_START_ADDR;
        //Data_valid_1 = *read_flash_1;
        /*If the flash contents are 0xFF, then initialize the screen variables with default values,
         else if the flash contains user defined configuration the read the sector and restore the
         screen variable values*/
        if (Data_valid != BACKUP_PRESENT_FLAG)// && Data_valid_1!= BACKUP_PRESENT_FLAG)
        {
            /*
             //Added by PVK 1 Apr 2016  read record number from file
             fp = fopen ("Miscellaneous.txt", "r");
             if(fp != NULL)
             {
             fscanf(fp,"%hi,%hi\r\n",&periodic_data.Periodic_hdr.Id, &error_data_log.Err_data_hdr.Id);
             fclose(fp);
             }
             */
            //PVK-03-Mar-2016
            //To read error_data_log.Err_data_hdr.Id from NVRAM
            //Read_ErrorLog_RecId();
            if (con==1)
                Create_Errorlog_file_and_Log_events();

            screen_variables_init(); /*Initialize with default values*/
            rec_Q_init();		//Initialize circular queue related parameters
            Calculation_struct.Total_weight_accum = 0;
            //eeprom_struct_backup();
            //Set_rtc_date(Admin_var.Config_Date);
            //Set_rtc_time(Admin_var.Config_Time);
            //Set_rtc_dow(Admin_var.Day_of_week);
// 			periodic_data.Periodic_hdr.Id = 0;
// 			set_zero_data_log.Set_zero_hdr.Id = 0;
// 			clear_data_log.Clr_data_hdr.Id = 0;
// 			calib_data_log.Cal_data_hdr.Id = 0;
// 			//error_data_log.Err_data_hdr.Id = 0;// //commented by DK on 16 Feb 2016
// 			length_cal_data_log.Len_data_hdr.Id = 0;
// 			strcpy((char *) fheader.File_hdr.Data_type, "HDR");
// 			fheader.File_hdr.Id = 0;
// 			fheader.File_hdr.Date.Year = Current_time.RTC_Year;
// 			fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
// 			fheader.File_hdr.Date.Day = Current_time.RTC_Mday;
// 			if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
// 				fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
// 			else if (Admin_var.Current_Time_Format == TWELVE_HR) {
// 				if (Admin_var.AM_PM == AM_TIME_FORMAT) {
// 					if ((U8) Current_time.RTC_Hour == 12)
// 						fheader.File_hdr.Time.Hr = 0;
// 					else
// 						fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
// 				} else {
// 					fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour + 12;
// 				}
// 			}
// 			//fheader.File_hdr.Time.Hr = Admin_var.Config_Time.Hr;
// 			fheader.File_hdr.Time.Min = Current_time.RTC_Min;
// 			fheader.File_hdr.Time.Sec = Current_time.RTC_Sec;
// 			if (Flags_struct.Connection_flags & USB_CON_FLAG)
// 			//if (con == 1)
// 			{
// 				//Check for folder structure
// 				if ((g_old_fw_ver >= (float) 1.00)
// 						&& (g_old_fw_ver <= (float) 4.09)) {
// 					sprintf(&file_name[0], "%04d-%02d-%02d_periodic_log.txt",
// 							fheader.File_hdr.Date.Year,
// 							fheader.File_hdr.Date.Mon,
// 							fheader.File_hdr.Date.Day);
// 				} else {
// 					sprintf(&file_name[0],
// 							"%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",
// 							fheader.File_hdr.Date.Year,
// 							fheader.File_hdr.Date.Mon,
// 							fheader.File_hdr.Date.Year,
// 							fheader.File_hdr.Date.Mon,
// 							fheader.File_hdr.Date.Day);
// 				}

// 				log_file_init(file_name);
//sks --> COMMENTING OUT THE BELOW LINES 20/07/2017
// WILL HANDLE INIT IF FILE NOT FOUND WHILE WRITING
//<--
            if (con ==1)
                log_file_init("calib_log.txt");  //sks : DO WE NEED TO DO IT HERE???
            //log_file_init("System_Log.txt"); //Commented by DK on 27 January 2016
//			}

            eeprom_first_time_write();

            //PVK - Commented on 7 Jan 2016
            //ExtFlash_writeWord_before_task((uint32_t)read_flash, BACKUP_PRESENT_FLAG);

            //PVK - Commented on 30-12-15 and put in function eeprom_first_time_write()
            /*
             #ifdef EXT_INT
             //erase the flash
             ExtFlash_eraseBlock(EXT_FLASH_LAST_SECTOR_START_ADDR);
             ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_START_ADDR);
             //PVK
             ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_COPY_START_ADDR);
             #endif
             */

            //Added by PVK 1 Apr 2016  Update record number
            /* fp = fopen ("Miscellaneous.txt", "r");
             if(fp != NULL)
             {
             fscanf(fp,"%hi,%hi\r\n",&periodic_data.Periodic_hdr.Id, &error_data_log.Err_data_hdr.Id);
             fclose(fp);
             */
            addr_flash = flash_para_record_search(
                             EXT_FLASH_PARA_BACKUP_START_ADDR);
            
						//sks: 17/11/2017 COMMENETED THE BELOW CODE TO TEST NEW DB STRUCTURE
					//	flash_para_backup(); //Store latest parameters for all totals and report parameters in Ext Flash

            /*	fdelete("Miscellaneous.txt");
             `				}
             else
             {
             addr_flash = flash_para_record_search(EXT_FLASH_PARA_BACKUP_START_ADDR);
             flash_para_reload(addr_flash,0);
             }
             */
            //All totals parameters
#ifdef DEBUG_PORT
            sprintf(g_buf, "\r\n Before opening Tot_bkup.txt");
            Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

            fp = fopen("Tot_bkup.txt", "r");
            if (fp != NULL)
            {
#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n Tot_bkup.txt opened");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

                fscanf(fp, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%f\r\n",
                       &Calculation_struct.Total_weight_accum,
                       &Rprts_diag_var.Job_Total, &Rprts_diag_var.Master_total,
                       &Rprts_diag_var.daily_Total_weight,
                       &Rprts_diag_var.monthly_Total_weight,
                       &Rprts_diag_var.yearly_Total_weight,
                       &Rprts_diag_var.weekly_Total_weight,
                       &Calculation_struct.run_time);

                addr_flash = flash_para_record_search(
                                 EXT_FLASH_PARA_BACKUP_START_ADDR);	//Dec31

#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n Before func flash_para_backup()");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

                flash_para_backup(); //Store latest parameters for all totals and report parameters in Ext Flash

#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n Before closing Tot_bkup.txt");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

                fclose(fp);

#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n Tot_bkup.txt closed");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

                fdelete("Tot_bkup.txt");

#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n Tot_bkup.txt deleted");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif
            }
            else
            {
#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n Tot_bkup.txt NOT opened");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif
                //PVK - 14 Jan 2016
                //if there is no file on USB or error occured while opening
                //then get the address for storing totals
                addr_flash = flash_para_record_search(
                                 EXT_FLASH_PARA_BACKUP_START_ADDR);
                flash_para_reload(addr_flash, 0);
            }

            //All Totals_var calculation for internal use
            Totals_var.daily_Total_weight = 0;
            Totals_var.weekly_Total_weight = 0;
            Totals_var.monthly_Total_weight = 0;
            Totals_var.yearly_Total_weight = 0;
            Totals_var.Job_Total = 0;
            Totals_var.Master_total = 0;
            Totals_var.Weight_unit = Scale_setup_var.Weight_unit;
            Convert_totals_weight_values();

            //Clear weight event parameters
#ifdef DEBUG_PORT
            sprintf(g_buf, "\r\n Before opening ClrWeight_bkup.txt");
            Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

            fp = fopen("ClrWeight_bkup.txt", "r");
            if (fp == NULL)
            {
#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n ClrWeight_bkup.txt NOT opened");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif
                load_good_Clr_wt_data();		//14th Jan 2016
            }
            else
            {
#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n ClrWeight_bkup.txt opened");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif
                for (i = 0; i < 8; i++)
                {
                    fscanf(fp,
                           "%lf,%f,%f,%hhu,%hhu,%hu,%hhu,%hhu,%hhu,%d,%d\r\n",
                           &Rprts_diag_var.LastClearedTotal[i],
                           &Rprts_diag_var.Avg_rate_for_mode[i],
                           &Rprts_diag_var.RunTime[i],
                           &Rprts_diag_var.Clear_Date[i].Day,
                           &Rprts_diag_var.Clear_Date[i].Mon,
                           &Rprts_diag_var.Clear_Date[i].Year,
                           &Rprts_diag_var.Clear_Time[i].Hr,
                           &Rprts_diag_var.Clear_Time[i].Min,
                           &Rprts_diag_var.Clear_Time[i].Sec,
                           &Rprts_diag_var.LastCleared_Weight_unit[i],
                           &Rprts_diag_var.LastCleared_Rate_time_unit[i]);
                }
                flash_struct_backup(0);

#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n Before closing ClrWeight_bkup.txt");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

                fclose(fp);

#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n ClrWeight_bkup.txt closed");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif

                fdelete("ClrWeight_bkup.txt");

#ifdef DEBUG_PORT
                sprintf(g_buf, "\r\n ClrWeight_bkup.txt deleted");
                Uart_i2c_data_send((uint8_t *) g_buf, strlen(g_buf));
#endif
            }

            //PVK - 14 Jan 2016
            //Upto 4.19(previous version) the formula used for calculating Avg rate was wrong and it is corrected
            //from 4.20 version
            Calculate_Clr_Wt_Screen_Avg_Rate();

            //PVK-17 Mar-2016
            //To write updated error ID into system log file before providing softreset
            addr_flash = flash_para_record_search(
                             EXT_FLASH_PARA_BACKUP_START_ADDR);
            flash_para_backup(); //Store latest parameters for all totals and report parameters in Ext Flash

            //PVK - Added on 7 Jan 2016
            read_flash = (U16 *) FLASH_BASE_ADDRESS;
            ExtFlash_writeWord_before_task((uint32_t) read_flash,
                                           BACKUP_PRESENT_FLAG);

            //load_good_Clr_wt_data();

            //PT: Read FW version from EEPROM: g_old_fw_ver
            //if(g_old_fw_ver<4.15)
            //{
            //	flash_struct_reload();
            //	flash_clear_weight_backup();
            //}

            //Backup of 8 clear weight events in flash
            //flash_clear_weight_backup();
            //PVK
            //Reset address and record number for all totals and records parameter
            //addr_flash = flash_para_record_search(EXT_FLASH_PARA_BACKUP_START_ADDR);	//Dec31
            // flash_para_reload(addr_flash, 0);
            // Totals_var.Weight_unit = Scale_setup_var.Weight_unit;
            // Convert_totals_weight_values();

            // Weight_unit_conv_func();
            // Calculation_struct.Total_weight = Calculation_struct.Total_weight_accum * Calculation_struct.Weight_conv_factor;
            // Units_var.Unit_rate = getRate_unit();
            // Units_var.Unit_speed = getSpeed_unit();
#if 0
            fp = fopen ("Para.txt", "a");
            if(fp != NULL)
            {
                fprintf(fp, "%s\r\n", "FW Upgrade");
                fprintf(fp, "%d/%d/%d,", Current_time.RTC_Year,Current_time.RTC_Mon, Current_time.RTC_Mday);
                fprintf(fp, " %d:%d:%d,", Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
                fprintf(fp,"%lf,%lf,%lf,%lf,%lf,%lf,%lf\r\n",Calculation_struct.Total_weight_accum,Rprts_diag_var.Job_Total,Rprts_diag_var.Master_total,Rprts_diag_var.daily_Total_weight,Rprts_diag_var.monthly_Total_weight,Rprts_diag_var.yearly_Total_weight,Rprts_diag_var.weekly_Total_weight);
                fclose(fp);
            }
#endif
            //Added by Oaces Team - 08-Mar-2016
            //In BS unit hardware, the microcontroller�s nRSTOUT pin (which is asserted only during reset events)
            //is connected to the reset input of Ethernet PHY device (LAN8720A).
            //The datasheet of the LAN8720 indicates a minimum reset length of 100 uSec.

            //After firmware image programming, the boot loader jumps to main application.
            //In this case the main application starts initializing EMAC & PHY without applying reset to the EMAC & PHY device.
            //Hence sometimes it hangs during EMAC /PHY initialization causing white screen on display.
            //So after retrieving all data provide the system reset to reset all peripherals properly
            //reset the system
            NVIC_SystemReset();
            while (1)
                ;
        }
        else
        {

            //PVK-03-Mar-2016
            //To read error_data_log.Err_data_hdr.Id from NVRAM
            Read_ErrorLog_RecId();
            Create_Errorlog_file_and_Log_events();

            //eeprom_struct_reload();
            EEP_load_good_data();			//Suvrat
            Current_time = RTCGetTime();
            load_good_Clr_wt_data();
            Flags_struct.Log_flags = LPC_RTC->GPREG0;
            Flash_log_data.Current_write_addr_ptr = (U16*) LPC_RTC->GPREG1;
            Flash_log_data.Nos_of_records = LPC_RTC->GPREG2;
            //screen_variables_init();
            rec_Q_init();		//Initialize circular queue related parameters
            //strcpy(&Admin_var.Int_firmware_version[0],FirmwareVer);
            //strcpy(&Admin_var.Int_update_version[0],FirmwareVer);
            strcpy((char *) fheader.File_hdr.Data_type, "HDR");
            fheader.File_hdr.Date.Year = Current_time.RTC_Year;
            fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
            fheader.File_hdr.Date.Day = Current_time.RTC_Mday;
            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
            else if (Admin_var.Current_Time_Format == TWELVE_HR)
            {
                if (Admin_var.AM_PM == AM_TIME_FORMAT)
                {
                    if ((U8) Current_time.RTC_Hour == 12)
                        fheader.File_hdr.Time.Hr = 0;
                    else
                        fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
                }
                else
                {
                    fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour + 12;
                }
            }
            //fheader.File_hdr.Time.Hr = Admin_var.Config_Time.Hr;
            fheader.File_hdr.Time.Min = Current_time.RTC_Min;
            fheader.File_hdr.Time.Sec = Current_time.RTC_Sec;

#ifdef EXT_INT
            read_flash = (U16 *) EXT_FLASH_LAST_SECTOR_START_ADDR;
            Data_valid = *read_flash;
            if (Data_valid == BACKUP_PRESENT_FLAG)
            {
                read_flash = (U16 *) (EXT_FLASH_LAST_SECTOR_START_ADDR + 4);
                for (j = 0; j < sizeof(Calculation_struct.Total_weight_accum);
                        j++)
                {
                    Data_valid = *read_flash;
                    buffer.d_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Calculation_struct.Total_weight_accum = buffer.double_val;
                // For Job totals
                for (j = 0; j < sizeof(Rprts_diag_var.Job_Total); j++)
                {
                    Data_valid = *read_flash;
                    buffer.d_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Rprts_diag_var.Job_Total = buffer.double_val;
                // For Master total
                for (j = 0; j < sizeof(Rprts_diag_var.Master_total); j++)
                {
                    Data_valid = *read_flash;
                    buffer.d_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Rprts_diag_var.Master_total = buffer.double_val;
                // For Daily Report total
                for (j = 0; j < sizeof(Rprts_diag_var.daily_Total_weight);
                        j++)
                {
                    Data_valid = *read_flash;
                    buffer.d_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Rprts_diag_var.daily_Total_weight = buffer.double_val;
                // For weekly Report total
                for (j = 0; j < sizeof(Rprts_diag_var.weekly_Total_weight);
                        j++)
                {
                    Data_valid = *read_flash;
                    buffer.d_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Rprts_diag_var.weekly_Total_weight = buffer.double_val;
                // For monthly Report total
                for (j = 0; j < sizeof(Rprts_diag_var.monthly_Total_weight);
                        j++)
                {
                    Data_valid = *read_flash;
                    buffer.d_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Rprts_diag_var.monthly_Total_weight = buffer.double_val;
                // For monthly Report total
                for (j = 0; j < sizeof(Rprts_diag_var.yearly_Total_weight);
                        j++)
                {
                    Data_valid = *read_flash;
                    buffer.d_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Rprts_diag_var.yearly_Total_weight = buffer.double_val;
                /* End of Restoring weight Totals*/
                /**************************************************************************************/
                memset(read_buffer, 0, sizeof(read_buffer));
                //Old Local Set Point
                for (j = 0; j < FLASH_SIZEOF(PID_Old_Local_Setpoint); j++)
                {
                    read_buffer[j] = *read_flash;
                    read_flash++;
                }
                memcpy((U16 *) &PID_Old_Local_Setpoint, read_buffer,
                       sizeof(PID_Old_Local_Setpoint));
                memset(read_buffer, 0, sizeof(read_buffer));
                //Shutdown_time
                for (j = 0; j < FLASH_SIZEOF(Shutdown_time); j++)
                {
                    read_buffer[j] = *read_flash;
                    read_flash++;
                }
                memcpy((U16 *) &Shutdown_time, read_buffer,
                       sizeof(Shutdown_time));

                //-------------------------------daily-------------------------
                memset(read_buffer, 0, sizeof(read_buffer));
                //zero speed counter
                //read_long_data_from_flash(sizeof(daily_rprt.Cnt_zero_speed));
                for (j = 0; j < sizeof(daily_rprt.Cnt_zero_speed); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                daily_rprt.Cnt_zero_speed = buffer_long.ulong_val;

                /*						 //zero start time
                 Data_valid = *read_flash;
                 daily_rprt.Start_time.Hr = (Data_valid & 0x00FF);
                 daily_rprt.Start_time.Min = (Data_valid & 0xFF00) >> 8;
                 read_flash++;
                 Data_valid = *read_flash;
                 daily_rprt.Start_time.Sec = (Data_valid & 0x00FF);
                 read_flash++;
                 read_flash++;*/

                //zero start time
                Data_valid = *read_flash;
                daily_rprt.Start_time.Hr = (Data_valid & 0x00FF);
                daily_rprt.Start_time.AM_PM = (Data_valid & 0xFF00) >> 8;//DO not use AM/PM variable for daily_rprt.Start_time
                read_flash++;
                Data_valid = *read_flash;
                daily_rprt.Start_time.Min = (Data_valid & 0x00FF);
                daily_rprt.Start_time.Sec = (Data_valid & 0xFF00) >> 8;
                read_flash++;
                read_flash++;
                read_flash++;

                /*						 //zero end time
                 Data_valid = *read_flash;
                 daily_rprt.End_time.Hr = (Data_valid & 0x00FF);
                 daily_rprt.End_time.Min = (Data_valid & 0xFF00) >> 8;
                 read_flash++;
                 Data_valid = *read_flash;
                 daily_rprt.End_time.Sec = (Data_valid & 0x00FF);
                 read_flash++;
                 read_flash++;*/

                //zero end time
                Data_valid = *read_flash;
                daily_rprt.End_time.Hr = (Data_valid & 0x00FF);
                daily_rprt.End_time.AM_PM = (Data_valid & 0xFF00) >> 8;	//used daily_rprt.End_time.AM_PM variable for Last_AM_PM
                read_flash++;
                Data_valid = *read_flash;
                daily_rprt.End_time.Min = (Data_valid & 0x00FF);
                daily_rprt.End_time.Sec = (Data_valid & 0xFF00) >> 8;
                read_flash++;
                read_flash++;
                read_flash++;

                //down time
                for (j = 0; j < sizeof(daily_rprt.Down_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                daily_rprt.Down_time = buffer_long.ulong_val;

                /*						 //start load time
                 Data_valid = *read_flash;
                 daily_rprt.Start_load_time.Hr = (Data_valid & 0x00FF);
                 daily_rprt.Start_load_time.Min = (Data_valid & 0xFF00) >> 8;
                 read_flash++;
                 Data_valid = *read_flash;
                 daily_rprt.Start_load_time.Sec = (Data_valid & 0x00FF);
                 read_flash++;
                 read_flash++;*/

                //start load time
                Data_valid = *read_flash;
                daily_rprt.Start_load_time.Hr = (Data_valid & 0x00FF);
                daily_rprt.Start_load_time.AM_PM = (Data_valid & 0xFF00) >> 8;//DO not use AM/PM variable for daily_rprt.Start_load_time
                read_flash++;
                Data_valid = *read_flash;
                daily_rprt.Start_load_time.Min = (Data_valid & 0x00FF);
                daily_rprt.Start_load_time.Sec = (Data_valid & 0xFF00) >> 8;
                read_flash++;
                read_flash++;
                read_flash++;

                /*						 //start load time
                 Data_valid = *read_flash;
                 daily_rprt.End_load_time.Hr = (Data_valid & 0x00FF);
                 daily_rprt.End_load_time.Min = (Data_valid & 0xFF00) >> 8;
                 read_flash++;
                 Data_valid = *read_flash;
                 daily_rprt.End_load_time.Sec = (Data_valid & 0x00FF);
                 read_flash++;
                 read_flash++;*/

                //end load time
                Data_valid = *read_flash;
                daily_rprt.End_load_time.Hr = (Data_valid & 0x00FF);
                daily_rprt.End_load_time.AM_PM = (Data_valid & 0xFF00) >> 8;//DO not use AM/PM variable for daily_rprt.End_load_time
                read_flash++;
                Data_valid = *read_flash;
                daily_rprt.End_load_time.Min = (Data_valid & 0x00FF);
                daily_rprt.End_load_time.Sec = (Data_valid & 0xFF00) >> 8;
                read_flash++;
                read_flash++;
                read_flash++;

                //run time
                for (j = 0; j < sizeof(daily_rprt.Run_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                daily_rprt.Run_time = buffer_long.ulong_val;

                //-------------------------weekly----------------------------------
                //zero speed counter
                for (j = 0; j < sizeof(weekly_rprt.Cnt_zero_speed); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                weekly_rprt.Cnt_zero_speed = buffer_long.ulong_val;

                //down time
                for (j = 0; j < sizeof(weekly_rprt.Down_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                weekly_rprt.Down_time = buffer_long.ulong_val;

                //run time
                for (j = 0; j < sizeof(weekly_rprt.Run_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                weekly_rprt.Run_time = buffer_long.ulong_val;

                //-----------------------monthly-------------------
                //zero speed counter
                for (j = 0; j < sizeof(monthly_rprt.Cnt_zero_speed); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                monthly_rprt.Cnt_zero_speed = buffer_long.ulong_val;

                //down time
                for (j = 0; j < sizeof(monthly_rprt.Down_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                monthly_rprt.Down_time = buffer_long.ulong_val;

                //run time
                for (j = 0; j < sizeof(monthly_rprt.Run_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                monthly_rprt.Run_time = buffer_long.ulong_val;

                for (j = 0; j < sizeof(monthly_rprt.Run_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                monthly_rprt.Run_time = buffer_long.ulong_val;

                //Daily Report Header Date
                Data_valid = *read_flash;
                daily_rprt.freq_rprt_header.Date.Year = Data_valid;
                read_flash++;
                Data_valid = *read_flash;
                daily_rprt.freq_rprt_header.Date.Mon = (Data_valid & 0x00FF);
                daily_rprt.freq_rprt_header.Date.Day = (Data_valid & 0xFF00)
                                                       >> 8;
                read_flash++;

                //Weekly Report Header Date
                Data_valid = *read_flash;
                weekly_rprt.freq_rprt_header.Date.Year = Data_valid;
                read_flash++;
                Data_valid = *read_flash;
                weekly_rprt.freq_rprt_header.Date.Mon = (Data_valid & 0x00FF);
                weekly_rprt.freq_rprt_header.Date.Day = (Data_valid & 0xFF00)
                                                        >> 8;
                read_flash++;

                //Monthly Report Header Date
                Data_valid = *read_flash;
                monthly_rprt.freq_rprt_header.Date.Year = Data_valid;
                read_flash++;
                Data_valid = *read_flash;
                monthly_rprt.freq_rprt_header.Date.Mon = (Data_valid & 0x00FF);
                monthly_rprt.freq_rprt_header.Date.Day = (Data_valid & 0xFF00)
                        >> 8;
                read_flash++;

                /*						//Daily Report Header Time
                 Data_valid = *read_flash;
                 daily_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
                 daily_rprt.freq_rprt_header.Time.Min = (Data_valid & 0xFF00) >> 8;
                 read_flash++;
                 // 						 Data_valid = *read_flash;
                 // 						 daily_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0x00FF);
                 // 						 read_flash++;*/

                //Daily Report Header Time
                Data_valid = *read_flash;
                daily_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
                daily_rprt.freq_rprt_header.Time.AM_PM = (Data_valid & 0xFF00)
                        >> 8;//DO not use AM/PM variable for daily_rprt.freq_rprt_header
                read_flash++;
                Data_valid = *read_flash;
                daily_rprt.freq_rprt_header.Time.Min = (Data_valid & 0x00FF);
                daily_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0xFF00)
                                                       >> 8;
                read_flash++;

                /*						 //Weekly Report Header Time
                 Data_valid = *read_flash;
                 weekly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
                 weekly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0xFF00) >> 8;
                 read_flash++;
                 // 						 Data_valid = *read_flash;
                 // 						 weekly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0x00FF);
                 // 						 read_flash++;*/

                //Weekly Report Header Time
                Data_valid = *read_flash;
                weekly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
                weekly_rprt.freq_rprt_header.Time.AM_PM = (Data_valid & 0xFF00)
                        >> 8;//DO not use AM/PM variable for weekly_rprt.freq_rprt_header
                read_flash++;
                Data_valid = *read_flash;
                weekly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0x00FF);
                weekly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0xFF00)
                                                        >> 8;
                read_flash++;

                /*						 //Monthly Report Header Time
                 Data_valid = *read_flash;
                 monthly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
                 monthly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0xFF00) >> 8;
                 read_flash++;
                 // 						 Data_valid = *read_flash;
                 // 						 monthly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0x00FF);
                 // 						 read_flash++;*/

                //Monthly Report Header Time
                Data_valid = *read_flash;
                monthly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
                monthly_rprt.freq_rprt_header.Time.AM_PM = (Data_valid & 0xFF00)
                        >> 8;//DO not use AM/PM variable for monthly_rprt.freq_rprt_header
                read_flash++;
                Data_valid = *read_flash;
                monthly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0x00FF);
                monthly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0xFF00)
                        >> 8;
                read_flash++;

                Data_valid = *read_flash;
                daily_rprt.Dummy_var1 = (Data_valid);
                read_flash++;
//SKS record ID
                Data_valid = *read_flash;
                periodic_data.Periodic_hdr.Id = Data_valid;
								periodic_data.Periodic_hdr.Id = LPC_RTC->GPREG2;
                read_flash++;

                Data_valid = *read_flash;
                set_zero_data_log.Set_zero_hdr.Id = Data_valid;
                read_flash++;

                Data_valid = *read_flash;
                clear_data_log.Clr_data_hdr.Id = Data_valid;
                read_flash++;

                Data_valid = *read_flash;
                calib_data_log.Cal_data_hdr.Id = Data_valid;
                read_flash++;

                Data_valid = *read_flash;
                length_cal_data_log.Len_data_hdr.Id = Data_valid;
                read_flash++;

                Data_valid = *read_flash;
                //error_data_log.Err_data_hdr.Id = Data_valid;//commented by DK on 16 Feb 2016
                read_flash++;

                //Run time
                for (j = 0; j < sizeof(Calculation_struct.run_time); j++)
                {
                    Data_valid = *read_flash;
                    buffer_float.f_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_float.f_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }
                Calculation_struct.run_time = buffer_float.float_val;

                //PVK
                //Scale setup weight unit store in Totals_var.Weight_unit
                for (j = 0; j < sizeof(Scale_setup_var.Weight_unit); j++)
                {
                    Data_valid = *read_flash;
                    buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
                    j++;
                    buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
                    read_flash++;
                }

                Totals_var.Weight_unit = buffer_long.ulong_val;
                addr = (uint32_t) read_flash;

                //All Totals_var calculation for internal use
                /*Totals_var.daily_Total_weight = 0;
                 Totals_var.weekly_Total_weight = 0;
                 Totals_var.monthly_Total_weight = 0;
                 Totals_var.yearly_Total_weight = 0;
                 Totals_var.Job_Total = 0;
                 Totals_var.Master_total = 0;

                 Totals_var.Weight_unit = Scale_setup_var.Weight_unit;
                 Convert_totals_weight_values();
                 */

                //PVK
                Block_Length = (addr - EXT_FLASH_LAST_SECTOR_START_ADDR);
                CalcCRC((unsigned char*) EXT_FLASH_LAST_SECTOR_START_ADDR,
                        (unsigned char*) &Calculated_CRC, Block_Length);
                if (Calculated_CRC == *read_flash)
                {
                    //PVK
                    //Read address and record number for all totals and records parameter
                    addr_flash = flash_para_record_search(
                                     EXT_FLASH_PARA_BACKUP_START_ADDR);
                    //Store latest parameters for all totals and report parameters in Ext Flash
                    flash_para_backup();
                }
                else
                {
                    //Search latest record number
                    addr_flash = flash_para_record_search(
                                     EXT_FLASH_PARA_BACKUP_START_ADDR);
                    //Restore all totals and recode parameters
                    //PVK
                    flash_para_reload(addr_flash, 0);
                }

                //PVK
                //All Totals_var calculation for internal use

                Totals_var.daily_Total_weight = 0;
                Totals_var.weekly_Total_weight = 0;
                Totals_var.monthly_Total_weight = 0;
                Totals_var.yearly_Total_weight = 0;
                Totals_var.Job_Total = 0;
                Totals_var.Master_total = 0;
                Totals_var.Weight_unit = Scale_setup_var.Weight_unit;
                Convert_totals_weight_values();

                //Weight_unit_conv_func();
                //Calculation_struct.Total_weight = Calculation_struct.Total_weight_accum * Calculation_struct.Weight_conv_factor;
                //Units_var.Unit_rate = getRate_unit();
                //Units_var.Unit_speed = getSpeed_unit();

                //-----------------------------------------------------------------------
                if (con == 1)
                {
                    //LPC_GPIO2->SET |= TEST_GPIO_P2_31;
                    fp = fopen("backup_file.txt", "a");
                    if (fp != NULL)
                    {
                        fprintf(fp,
                                "%lf %lf %lf %lf %lf %lf %d/%d/%d %02d:%02d:%02d %d %02d:%02d:%02d %02d:%02d:%02d %d %02d:%02d:%02d %02d:%02d:%02d %d %d %d %d %d %d %d\r\n",
                                Calculation_struct.Total_weight_accum,
                                Rprts_diag_var.daily_Total_weight,
                                Rprts_diag_var.weekly_Total_weight,
                                Rprts_diag_var.monthly_Total_weight,
                                Rprts_diag_var.Job_Total,
                                Rprts_diag_var.Master_total,
                                Shutdown_time.RTC_Mday, Shutdown_time.RTC_Mon,
                                Shutdown_time.RTC_Year, Shutdown_time.RTC_Hour,
                                Shutdown_time.RTC_Min, Shutdown_time.RTC_Sec,
                                daily_rprt.Cnt_zero_speed,
                                daily_rprt.Start_time.Hr,
                                daily_rprt.Start_time.Min,
                                daily_rprt.Start_time.Sec,
                                daily_rprt.End_time.Hr, daily_rprt.End_time.Min,
                                daily_rprt.End_time.Sec, daily_rprt.Down_time,
                                daily_rprt.Start_load_time.Hr,
                                daily_rprt.Start_load_time.Min,
                                daily_rprt.Start_load_time.Sec,
                                daily_rprt.End_load_time.Hr,
                                daily_rprt.End_load_time.Min,
                                daily_rprt.End_load_time.Sec,
                                daily_rprt.Run_time, weekly_rprt.Cnt_zero_speed,
                                weekly_rprt.Down_time, weekly_rprt.Run_time,
                                monthly_rprt.Cnt_zero_speed,
                                monthly_rprt.Down_time, monthly_rprt.Run_time);
                        fclose(fp);
                    }
                    Weight_unit_conv_func();
                    Calculation_struct.Total_weight =
                        Calculation_struct.Total_weight_accum
                        * Calculation_struct.Weight_conv_factor;
                    Units_var.Unit_rate = getRate_unit();
                    Units_var.Unit_speed = getSpeed_unit();
                    if (Current_time.RTC_Mday != Shutdown_time.RTC_Mday)
                    {
                        if (daily_rprt.Dummy_var1 == 0x8000)
                            daily_rprt.Dummy_var1 = 0x0101;
                        if (con == 1)
                        {
                            //daily_rprt.freq_rprt_header.Date.Year = Shutdown_time.RTC_Year;
                            //daily_rprt.freq_rprt_header.Date.Mon = Shutdown_time.RTC_Mon;
                            //daily_rprt.freq_rprt_header.Date.Day = Shutdown_time.RTC_Mday;
                            Last_Admin_AM_PM = daily_rprt.End_time.AM_PM;//Added on 21-Oct-2015 for restoring last AM_PM setting

                            strcpy(
                                (char *) daily_rprt.freq_rprt_header.Data_type,
                                "REP");
                            strcpy(
                                (char*) daily_rprt.freq_rprt_header.Report_type,
                                "Daily Report");
                            daily_rprt.Total_time = daily_rprt.Run_time
                                                    + daily_rprt.Down_time;
#ifdef CALCULATION
                            if (daily_rprt.Total_time != 0)
                            {
                                daily_rprt.Average_rate =
                                    (Rprts_diag_var.daily_Total_weight
                                     / daily_rprt.Total_time) * 60.0;
                            }
                            daily_rprt.Total_weight =
                                Rprts_diag_var.daily_Total_weight;
#endif
                            if ((daily_rprt.Dummy_var1 & 0x7F00) == 0x0100)
                            {
                                daily_rprt.Start_load_time.Hr = 24;
                                daily_rprt.End_load_time.Hr = 24;
                            }
                            if ((daily_rprt.Dummy_var1 & 0x00FF) == 0x0001)
                            {
                                daily_rprt.Start_time.Hr = 24;
                                daily_rprt.End_time.Hr = 24;
                            }

                            strcpy((char *) daily_rprt.Total_rate_unit,
                                   Strings[Scale_setup_var.Rate_time_unit].Text);
                            strcpy((char *) daily_rprt.Total_weight_unit,
                                   Strings[Scale_setup_var.Weight_unit].Text);
#ifdef SHOW_REPORTS
                            //report to be written to the USB drive
                            Flags_struct.Report_flags |= DLY_RPRT_WR_FLAG;
                            report_file_write(daily_report);
#endif
                            periodic_data.Periodic_hdr.Date.Year =
                                (U16) daily_rprt.freq_rprt_header.Date.Year;
                            periodic_data.Periodic_hdr.Date.Mon =
                                (U8) daily_rprt.freq_rprt_header.Date.Mon;
                            periodic_data.Periodic_hdr.Date.Day =
                                (U8) daily_rprt.freq_rprt_header.Date.Day;
                            fdt_hdr.Date.Year = Shutdown_time.RTC_Year;
                            fdt_hdr.Date.Mon = Shutdown_time.RTC_Mon;
                            fdt_hdr.Date.Day = Shutdown_time.RTC_Mday;

                            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                            {
                                //periodic_data.Periodic_hdr.Time.Hr  = (U8) Shutdown_time.RTC_Hour;
                                fdt_hdr.Time.Hr = (U8) Shutdown_time.RTC_Hour;
                            }
                            else if (Admin_var.Current_Time_Format
                                     == TWELVE_HR)
                            {
                                if (Last_Admin_AM_PM == AM)
                                {
                                    if (Shutdown_time.RTC_Hour == 12)
                                        fdt_hdr.Time.Hr = 0;
                                    else
                                        fdt_hdr.Time.Hr =
                                            (U8) Shutdown_time.RTC_Hour;
                                }
                                else
                                {
                                    fdt_hdr.Time.Hr =
                                        (U8) Shutdown_time.RTC_Hour + 12;
                                }
                            }
                            fdt_hdr.Time.Min = (U8) Shutdown_time.RTC_Min;
                            fdt_hdr.Time.Sec = (U8) Shutdown_time.RTC_Sec;
//							log_data_populate(periodic_log_stop_at_start);

                            periodic_data.Periodic_hdr.Date.Year =
                                (U16) Current_time.RTC_Year;
                            periodic_data.Periodic_hdr.Date.Mon =
                                (U8) Current_time.RTC_Mon;
                            periodic_data.Periodic_hdr.Date.Day =
                                (U8) Current_time.RTC_Mday;
                            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                            {
                                periodic_data.Periodic_hdr.Time.Hr =
                                    (U8) Current_time.RTC_Hour;
                            }
                            else if (Admin_var.Current_Time_Format
                                     == TWELVE_HR)
                            {
                                if (Admin_var.AM_PM == AM_TIME_FORMAT)
                                {
                                    if (Current_time.RTC_Hour == 12)
                                        periodic_data.Periodic_hdr.Time.Hr = 0;
                                    else
                                        periodic_data.Periodic_hdr.Time.Hr =
                                            (U8) Current_time.RTC_Hour;
                                }
                                else
                                {
                                    if (Current_time.RTC_Hour == 12)
                                        periodic_data.Periodic_hdr.Time.Hr = 12;
                                    else
                                        periodic_data.Periodic_hdr.Time.Hr =
                                            (U8) Current_time.RTC_Hour + 12;
                                }
                            }
                            //periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
                            periodic_data.Periodic_hdr.Time.Min =
                                (U8) Current_time.RTC_Min;
                            periodic_data.Periodic_hdr.Time.Sec =
                                (U8) Current_time.RTC_Sec;

                            fheader.File_hdr.Date.Year = Current_time.RTC_Year;
                            fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
                            fheader.File_hdr.Date.Day = Current_time.RTC_Mday;
                            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                                fheader.File_hdr.Time.Hr =
                                    (U8) Current_time.RTC_Hour;
                            else if (Admin_var.Current_Time_Format == TWELVE_HR)
                            {
                                if (Admin_var.AM_PM == AM_TIME_FORMAT)
                                {
                                    if ((U8) Current_time.RTC_Hour == 12)
                                        fheader.File_hdr.Time.Hr = 0;
                                    else
                                        fheader.File_hdr.Time.Hr =
                                            (U8) Current_time.RTC_Hour;
                                }
                                else
                                {
                                    if ((U8) Current_time.RTC_Hour == 12)
                                        fheader.File_hdr.Time.Hr = 12;
                                    else
                                        fheader.File_hdr.Time.Hr =
                                            (U8) Current_time.RTC_Hour + 12;
                                }
                            }
                            //fheader.File_hdr.Time.Hr = Admin_var.Config_Time.Hr;
                            fheader.File_hdr.Time.Min = Current_time.RTC_Min;
                            fheader.File_hdr.Time.Sec = Current_time.RTC_Sec;
                            Totals_var.daily_Total_weight = 0;
                            Rprts_diag_var.daily_Total_weight = 0;
                            daily_rprt.Total_weight = 0;
                            //log_data_populate(periodic_log_start);
                        }

                        Totals_var.daily_Total_weight = 0;
                        Rprts_diag_var.daily_Total_weight = 0;
                        daily_rprt.Total_weight = 0;
                        daily_rprt.freq_rprt_header.Date.Year =
                            (U16) Current_time.RTC_Year;
                        daily_rprt.freq_rprt_header.Date.Mon =
                            (U8) Current_time.RTC_Mon;
                        daily_rprt.freq_rprt_header.Date.Day =
                            (U8) Current_time.RTC_Mday;
                        if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                            daily_rprt.freq_rprt_header.Time.Hr =
                                (U8) Current_time.RTC_Hour;
                        else if (Admin_var.Current_Time_Format == TWELVE_HR)
                        {
                            if (Admin_var.AM_PM == AM_TIME_FORMAT)
                            {
                                if ((U8) Current_time.RTC_Hour == 12)
                                    daily_rprt.freq_rprt_header.Time.Hr = 0;
                                else
                                    daily_rprt.freq_rprt_header.Time.Hr =
                                        (U8) Current_time.RTC_Hour;
                            }
                            else
                            {
                                if ((U8) Current_time.RTC_Hour == 12)
                                    daily_rprt.freq_rprt_header.Time.Hr = 12;
                                else
                                    daily_rprt.freq_rprt_header.Time.Hr =
                                        (U8) Current_time.RTC_Hour + 12;
                            }
                        }
                        daily_rprt.freq_rprt_header.Time.Min =
                            (U8) Current_time.RTC_Min;
                        daily_rprt.freq_rprt_header.Time.Sec =
                            (U8) Current_time.RTC_Sec;
                    }
                    if (Current_time.RTC_Wday < Shutdown_time.RTC_Wday)
                    {
                        if (con == 1)
                        {
                            strcpy(
                                (char *) weekly_rprt.freq_rprt_header.Data_type,
                                "REP");
                            strcpy(
                                (char *) weekly_rprt.freq_rprt_header.Report_type,
                                "Weekly Report");
                            weekly_rprt.Total_time = weekly_rprt.Run_time
                                                     + weekly_rprt.Down_time;
#ifdef CALCULATION
                            if (weekly_rprt.Total_time != 0)
                            {
                                weekly_rprt.Average_rate =
                                    (Rprts_diag_var.weekly_Total_weight
                                     / weekly_rprt.Total_time * 60.0);
                            }
                            weekly_rprt.Total_weight =
                                Rprts_diag_var.weekly_Total_weight;
#endif
                            strcpy((char *) weekly_rprt.Total_rate_unit,
                                   Strings[Scale_setup_var.Rate_time_unit].Text);
                            strcpy((char *) weekly_rprt.Total_weight_unit,
                                   Strings[Scale_setup_var.Weight_unit].Text);
#ifdef SHOW_REPORTS
                            Flags_struct.Report_flags |= WKLY_RPRT_WR_FLAG;
                            report_file_write(weekly_report);
#endif
                        }
                        Totals_var.weekly_Total_weight = 0;
                        Rprts_diag_var.weekly_Total_weight = 0;
                        weekly_rprt.Total_weight = 0;
                        weekly_rprt.freq_rprt_header.Date.Year =
                            (U16) Current_time.RTC_Year;
                        weekly_rprt.freq_rprt_header.Date.Mon =
                            (U8) Current_time.RTC_Mon;
                        weekly_rprt.freq_rprt_header.Date.Day =
                            (U8) Current_time.RTC_Mday;
                        if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                            weekly_rprt.freq_rprt_header.Time.Hr =
                                (U8) Current_time.RTC_Hour;
                        else if (Admin_var.Current_Time_Format == TWELVE_HR)
                        {
                            if (Admin_var.AM_PM == AM_TIME_FORMAT)
                            {
                                if (weekly_rprt.freq_rprt_header.Time.Hr == 12)
                                    weekly_rprt.freq_rprt_header.Time.Hr = 0;
                                else
                                    weekly_rprt.freq_rprt_header.Time.Hr =
                                        (U8) Current_time.RTC_Hour;
                            }
                            else
                            {
                                weekly_rprt.freq_rprt_header.Time.Hr =
                                    (U8) Current_time.RTC_Hour + 12;
                            }
                        }
                        weekly_rprt.freq_rprt_header.Time.Min =
                            (U8) Current_time.RTC_Min;
                        weekly_rprt.freq_rprt_header.Time.Sec =
                            (U8) Current_time.RTC_Sec;
                    }
                    if (Current_time.RTC_Mon != Shutdown_time.RTC_Mon)
                    {
                        if (con == 1)
                        {
                            strcpy(
                                (char *) monthly_rprt.freq_rprt_header.Data_type,
                                "REP");
                            strcpy(
                                (char *) monthly_rprt.freq_rprt_header.Report_type,
                                "Monthly Report");
                            monthly_rprt.Total_time = monthly_rprt.Run_time
                                                      + monthly_rprt.Down_time;

#ifdef CALCULATION
                            if (monthly_rprt.Total_time != 0)
                            {
                                monthly_rprt.Average_rate =
                                    (Rprts_diag_var.monthly_Total_weight
                                     / monthly_rprt.Total_time * 60.0);
                            }
                            monthly_rprt.Total_weight =
                                Rprts_diag_var.monthly_Total_weight;
#endif
                            strcpy((char *) monthly_rprt.Total_rate_unit,
                                   Strings[Scale_setup_var.Rate_time_unit].Text);
                            strcpy((char *) monthly_rprt.Total_weight_unit,
                                   Strings[Scale_setup_var.Weight_unit].Text);
                            //report to be written to the USB drive
#ifdef SHOW_REPORTS
                            Flags_struct.Report_flags |= MTHLY_RPRT_WR_FLAG;
                            report_file_write(monthly_report);
#endif
                        }
                        Totals_var.monthly_Total_weight = 0;
                        Rprts_diag_var.monthly_Total_weight = 0;
                        monthly_rprt.Total_weight = 0;
                        monthly_rprt.freq_rprt_header.Date.Year =
                            (U16) Current_time.RTC_Year;
                        monthly_rprt.freq_rprt_header.Date.Mon =
                            (U8) Current_time.RTC_Mon;
                        monthly_rprt.freq_rprt_header.Date.Day =
                            (U8) Current_time.RTC_Mday;
                        if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                            monthly_rprt.freq_rprt_header.Time.Hr =
                                (U8) Current_time.RTC_Hour;
                        else if (Admin_var.Current_Time_Format == TWELVE_HR)
                        {
                            if (Admin_var.AM_PM == AM_TIME_FORMAT)
                            {
                                if (monthly_rprt.freq_rprt_header.Time.Hr == 12)
                                    monthly_rprt.freq_rprt_header.Time.Hr = 0;
                                else
                                    monthly_rprt.freq_rprt_header.Time.Hr =
                                        (U8) Current_time.RTC_Hour;
                            }
                            else
                            {
                                if (monthly_rprt.freq_rprt_header.Time.Hr == 12)
                                    monthly_rprt.freq_rprt_header.Time.Hr = 12;
                                else
                                    monthly_rprt.freq_rprt_header.Time.Hr =
                                        (U8) Current_time.RTC_Hour + 12;
                            }
                        }
                        monthly_rprt.freq_rprt_header.Time.Min =
                            (U8) Current_time.RTC_Min;
                        monthly_rprt.freq_rprt_header.Time.Sec =
                            (U8) Current_time.RTC_Sec;
//                        periodic_data.Periodic_hdr.Id = LPC_RTC->GPREG2;
//						periodic_data.Periodic_hdr.Id = 0;//SKS : The record number should never roll over
                        clear_data_log.Clr_data_hdr.Id = 0;//SKS : The record number should never roll over
                    }
                    //LPC_GPIO2->CLR |= TEST_GPIO_P2_31;
                }
                //-----------------------------------------------------------------------
                //erase the flash
                ExtFlash_eraseBlock(EXT_FLASH_LAST_SECTOR_START_ADDR);

#if 0
                fp = fopen ("Para.txt", "a");
                if(fp != NULL)
                {
                    fprintf(fp, "%s\r\n", "Power On");
                    fprintf(fp, "%d/%d/%d,", Current_time.RTC_Year,Current_time.RTC_Mon, Current_time.RTC_Mday);
                    fprintf(fp, " %d:%d:%d,", Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
                    fprintf(fp,"%d,%lf,%lf,%lf,%lf,%lf,%lf,%lf\r\n",addr_flash,Calculation_struct.Total_weight_accum,Rprts_diag_var.Job_Total,Rprts_diag_var.Master_total,Rprts_diag_var.daily_Total_weight,Rprts_diag_var.monthly_Total_weight,Rprts_diag_var.yearly_Total_weight,Rprts_diag_var.weekly_Total_weight);
                    fclose(fp);
                }
#endif
            }
            else		// Soft reset condition
            {
                //PVK
                //eeprom_weight_reload();

                //All Totals_var calculation for internal use
                /*	Totals_var.daily_Total_weight = 0;
                 Totals_var.weekly_Total_weight = 0;
                 Totals_var.monthly_Total_weight = 0;
                 Totals_var.yearly_Total_weight = 0;
                 Totals_var.Job_Total = 0;
                 Totals_var.Master_total = 0;
                 */
                //Search latest record number
                addr_flash = flash_para_record_search(
                                 EXT_FLASH_PARA_BACKUP_START_ADDR);
                //Restore all totals and recode parameters
                //PVK
                flash_para_reload(addr_flash, 0);

                record_flag = flash_para_record_flag(GET_FLAG);
                if (record_flag == 1)
                {
                    periodic_data.Periodic_hdr.Id += 1;
                    clear_data_log.Clr_data_hdr.Id += 1;
                }

#if 0
                fp = fopen ("Para.txt", "a");
                if(fp != NULL)
                {
                    fprintf(fp, "%s - ", "Soft Reset");
                    fprintf(fp, "%d/%d/%d,", Current_time.RTC_Year,Current_time.RTC_Mon, Current_time.RTC_Mday);
                    fprintf(fp, " %d:%d:%d,", Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
                    fprintf(fp,"/r/n%d,%lf,%lf,%lf,%lf,%lf,%lf,%lf\r\n",addr_flash,Calculation_struct.Total_weight_accum,Rprts_diag_var.Job_Total,Rprts_diag_var.Master_total,Rprts_diag_var.daily_Total_weight,Rprts_diag_var.monthly_Total_weight,Rprts_diag_var.yearly_Total_weight,Rprts_diag_var.weekly_Total_weight);
                    fclose(fp);
                }
#endif

                Weight_unit_conv_func();
                //Calculation_struct.Total_weight = Calculation_struct.Total_weight_accum * Calculation_struct.Weight_conv_factor;
                Calculation_struct.Total_weight = ConvWeightUnit(LBS,
                                                  Scale_setup_var.Weight_unit,
                                                  Calculation_struct.Total_weight_accum); //Added by DK on 7 Jan2016
                Units_var.Unit_rate = getRate_unit();
                Units_var.Unit_speed = getSpeed_unit();

                //All Totals_var calculation for internal use
                Totals_var.daily_Total_weight = 0;
                Totals_var.weekly_Total_weight = 0;
                Totals_var.monthly_Total_weight = 0;
                Totals_var.yearly_Total_weight = 0;
                Totals_var.Job_Total = 0;
                Totals_var.Master_total = 0;
                Totals_var.Weight_unit = Scale_setup_var.Weight_unit;
                Convert_totals_weight_values();
            }
            //BS-161 -A duplicate entry of record number is found after soft reset while storing data from flash to USB
            //Added by PVK on 30 Apr 2016 Read the periodic long and calib log record from file
            //as it is not stored in case of soft reset in flash and
            //also not  stored incremented record number in case 0f scale power down
            if (Flags_struct.Connection_flags & USB_CON_FLAG)
                //if (con == 1)
            {
                //Check for folder structure
// 				if ((g_old_fw_ver >= (float) 1.00)
// 						&& (g_old_fw_ver <= (float) 4.09)) {
// 					sprintf(&file_name[0], "%04d-%02d-%02d_periodic_log.txt",
// 							fheader.File_hdr.Date.Year,
// 							fheader.File_hdr.Date.Mon,
// 							fheader.File_hdr.Date.Day);
// 				} else {
// 					sprintf(&file_name[0],
// 							"%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",
// 							fheader.File_hdr.Date.Year,
// 							fheader.File_hdr.Date.Mon,
// 							fheader.File_hdr.Date.Year,
// 							fheader.File_hdr.Date.Mon,
// 							fheader.File_hdr.Date.Day);
// 				}

// 				log_file_init(file_name);
                log_file_init("calib_log.txt");
            }
            /*else
             {
             //erase the flash
             ExtFlash_eraseBlock(EXT_FLASH_LAST_SECTOR_START_ADDR);
             //Calculation_struct.Total_weight_accum = 5;
             daily_rprt.Dummy_var1 = 0x0101;
             eeprom_weight_reload();
             }*/
#else
            //PVK
            //eeprom_weight_reload();
#endif // EXT_INT
        }
    }
    else
#endif /*#ifdef EEPROM*/
    {
        screen_variables_init(); /*Initialize with default values*/
        rec_Q_init();			//Initialize circular queue related parameters
        WDTFeed();
    }
//	Create_Errorlog_file_and_Log_events();
#endif
//Reload Feeder delay timer
//Feed_timer_status=1;

//U8GlobalMBCount = 0;
//Ready_Write = 1; //Commented By DK on 4 May2016 for resolving issue of totals losing on power cycle
    if (Admin_var.MBS_TCP_slid == 0)
        Admin_var.MBS_TCP_slid = 255;

    Rate_blend_load.Feed_delay_timer = Setup_device_var.Feed_Delay;

// Enable and Disable Ethernet

#ifdef ETHERNET
    WDTFeed();
		
    if ((LPC_RTC->ALDOY &0x01)==0x01)badethernet_flag =1; //ethernet fail count
    else
    {
			badethernet_flag =0;
        LPC_RTC->ALDOY|=1;
        init_TcpNet();
        LPC_RTC->ALDOY &=(~0x01);  //  move after RTOS INIT
    }
    if (WDT_EN==1)
        LPC_RTC->ALDOY =0;  //  move after RTOS INIT
#endif
// Initialization of Feeder max rate variable
    if (Setup_device_var.PID_Channel == 1)
    {
        Setup_device_var.Feeder_Max_Rate =
            Setup_device_var.Analog_Output_1_Maxrate;
    }
    else
    {
        Setup_device_var.Feeder_Max_Rate =
            Setup_device_var.Analog_Output_2_Maxrate;
    }

    GUI_data_nav.GUI_structure_backup = 0;

    Ready_Write = 1; //Added By DK on 4 May2016 for resolving issue of totals losing on power cycle

#ifdef RTOS
    os_sys_init(init); /* Initialize RTX and start init */
#endif /*#ifdef RTOS*/
}

void RecordRead(FILE* fp)
{
    static char temp_buff[200], buf1[10];

    while (1)
    {
        if (fgets(temp_buff, sizeof(temp_buff), fp) == NULL)
            break;
    }

    strncpy(buf1, temp_buff, sizeof(buf1));

    return;
}

/*****************************************************************************
 * @note       Function name : void Create_Errorlog_file_and_Log_events(void)
 * @returns    returns       : None
 *                           :
 * @param      arg1          : None
 * @author                   : Oaces Team
 * @date       date created  : 01/02/2016
 * @brief      Description   : Check and create error log file and log the events
 * @note       Notes         : None
 *****************************************************************************/
extern int sdram_error;
//extern   debug_modules;
uint32_t UID[4];
void Create_Errorlog_file_and_Log_events(void)
{
    FILE *fp;
    static char file_name[FILE_NAME_SIZE];
    static char /*temp_buff[200],*/Buff1[10] = "\t";
    char *Ptr;
    U8 u8File_exists = 0, chError = 0;
    U16 Data_valid;

    unsigned int uiRecord_No = 0;
//    float Temp_g_current_fw_ver;

	ReadDeviceSerialNum(UID);
	
    read_flash = (U16 *) FLASH_BASE_ADDRESS;
    Data_valid = *read_flash;

    //By DK on 1 Feb 2016 To log power on reset status into System_Log.txt as per email received from Jonathan Dt:- 29 January 2016
    if (con == 1)
    {
        //error_data_log.Err_data_hdr.Id++;
        error_data_log.Err_data_hdr.Date.Year = (U16) Current_time.RTC_Year;
        error_data_log.Err_data_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
        error_data_log.Err_data_hdr.Date.Day = (U8) Current_time.RTC_Mday;
        error_data_log.Err_data_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
        error_data_log.Err_data_hdr.Time.Min = (U8) Current_time.RTC_Min;
        error_data_log.Err_data_hdr.Time.Sec = (U8) Current_time.RTC_Sec;
        error_data_log.Report_type = error_log;
        Flags_struct.Log_flags |= ERROR_LOG_WR_FLAG;

      //  Temp_g_current_fw_ver = g_current_fw_ver;
        //Check for version and open the file as per the version number older version uses
        // file name error_log.txt else System_Log.txt
        /* if (((Temp_g_current_fw_ver >= (float) 1.00)
                 && (Temp_g_current_fw_ver <= (float) 4.22))
                 || ((Temp_g_current_fw_ver >= (float) 0.90)
                     && (Temp_g_current_fw_ver <= (float) 0.99))
                 || ((Temp_g_current_fw_ver >= (float) 0.50)
                     && (Temp_g_current_fw_ver <= (float) 0.60)))
         {
             strcpy(file_name, "error_log.txt");
             fp = fopen(file_name, "r");
         }
         else */
        {
            strcpy(file_name, "System_Log.txt");
            fp = NULL;
            if(con==1)
                fp = fopen(file_name, "r");
        }

        //Check for file present
        if (fp != NULL)
        {
            u8File_exists = 1;
            fclose(fp);
        }
        else
        {
            u8File_exists = 0;
            //fclose(fp);
        }
        fflush(fp);

        //Open file in read or write mode
        if (u8File_exists == 0)
        {
            fp = fopen(file_name, "w");
        }
        else
        {
            fp = fopen(file_name, "r");
        }

        if (fp == NULL)
        {
            return;
        }
        else
        {
            if (u8File_exists == 0)
            {
                //Added by DK on 2 Feb 2016
                error_data_log.Err_data_hdr.Id = 1;
                fprintf(fp,
                        "Record #\t Date\t Time\t  \tType\t \tMessage\t\r\n");
                //fprintf(fp, " %c\t", '0');
                fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                fprintf(fp, " %04d/%02d/%02d\t",
                        error_data_log.Err_data_hdr.Date.Year,
                        error_data_log.Err_data_hdr.Date.Mon,
                        error_data_log.Err_data_hdr.Date.Day);
                fprintf(fp, " %02d:%02d:%02d\t",
                        error_data_log.Err_data_hdr.Time.Hr,
                        error_data_log.Err_data_hdr.Time.Min,
                        error_data_log.Err_data_hdr.Time.Sec);
                fprintf(fp, " %s\t", "Scale Info");
                fprintf(fp,
                        "Scale name =%c%s%c, Plant name =%c%s%c, Product name =%c%s%c, Firmware Version %s\t\r\n",
                        '"', Admin_var.Scale_Name, '"', '"',
                        Admin_var.Plant_Name, '"', '"', Admin_var.Product_Name,
                        '"', Admin_var.Int_firmware_version);
            }
            //Else part is Added by PVK 05 Apr 2016
            else
            {
                if (Data_valid != BACKUP_PRESENT_FLAG)
                {
                    //RecordRead(fp);

                    while (1)
                    {
                        if (fgets(Syslog_Get_Line_Buff,
                                  sizeof(Syslog_Get_Line_Buff), fp) == NULL)
                        {
                            break;
                        }
                    }

                    Ptr = strtok(Syslog_Get_Line_Buff, Buff1);
                    if (Ptr != NULL)
                    {
                        if (strcmp(Ptr, "Record #") != 0)
                        {
                            uiRecord_No = atoi(Ptr);
                            chError = 1;
                        }
                        else
                        {
                            chError = 0;
                        }
                    }
                    else
                    {
                        chError = 0;
                    }

                    //Check if any error if not then get the record number
                    //if((chError == 0) || (uiFileSize  <= 0) || ( uiFileSize > uiTotal_FileSize))
                    if (chError == 0)
                    {
                        error_data_log.Err_data_hdr.Id = 0;
                    }
                    else
                    {
                        error_data_log.Err_data_hdr.Id = uiRecord_No;
                    }
                }
                //Close the file open in read mode and open it in append mode
                if (0 != fclose(fp))
                {
                    //		printf ("\nFile could not be closed!\n");
                }
                fflush(fp);

                //Open file in append mode to write events
                fp = fopen(file_name, "a");
                if (fp == NULL)
                {
                    return;
                }
            }			//else end

            if (Data_valid != BACKUP_PRESENT_FLAG)
            {
                error_data_log.Err_data_hdr.Id++;
                fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                fprintf(fp, " %04d/%02d/%02d\t",
                        error_data_log.Err_data_hdr.Date.Year,
                        error_data_log.Err_data_hdr.Date.Mon,
                        error_data_log.Err_data_hdr.Date.Day);
                fprintf(fp, " %02d:%02d:%02d\t",
                        error_data_log.Err_data_hdr.Time.Hr,
                        error_data_log.Err_data_hdr.Time.Min,
                        error_data_log.Err_data_hdr.Time.Sec);
                fprintf(fp, " %s\t", "Event");
                fprintf(fp, " %s%s\r\n",
                        "Firmware Upgrade Done! New Firmware Version is ",
                        Admin_var.Int_firmware_version);
            }
            else
            {
                error_data_log.Err_data_hdr.Id++;
                fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                fprintf(fp, " %04d/%02d/%02d\t",
                        error_data_log.Err_data_hdr.Date.Year,
                        error_data_log.Err_data_hdr.Date.Mon,
                        error_data_log.Err_data_hdr.Date.Day);
                fprintf(fp, " %02d:%02d:%02d\t",
                        error_data_log.Err_data_hdr.Time.Hr,
                        error_data_log.Err_data_hdr.Time.Min,
                        error_data_log.Err_data_hdr.Time.Sec);
                fprintf(fp, " %s\t", "Event");

                //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                //Brown Out Detect (BOD), system reset, and lockup)
                memset(file_name, 0, sizeof(file_name));
                //if(((LPC_SC->RSID & 0x01) == 0x01) || ((LPC_SC->RSID & 0x02) == 0x02))
                if ((LPC_SC->RSID & 0x01) == 0x01)
                {
                    //strcpy(file_name,"Power On Or External Reset");
                    strcpy(file_name, "Power On Reset");
                    LPC_SC->RSID |= 0x01;
                }
                else if ((LPC_SC->RSID & 0x02) == 0x02)
                {
                    strcpy(file_name, "External Reset");
                    LPC_SC->RSID |= 0x02;
                }

                if ((LPC_SC->RSID & 0x04) == 0x04)
                {
                    strcpy(file_name, "Watchdog Timeout");
                    LPC_SC->RSID |= 0x04;
                }

                if ((LPC_SC->RSID & 0x08) == 0x08)
                {
                    strcpy(file_name, "Brownout Detection");
                    LPC_SC->RSID |= 0x08;
                }

                if ((LPC_SC->RSID & 0x10) == 0x10)
                {
                    strcpy(file_name, "System Reset Request");
                    LPC_SC->RSID |= 0x10;
                }

                if ((LPC_SC->RSID & 0x20) == 0x20)
                {
                    strcpy(file_name, "Hard Fault");
                    LPC_SC->RSID |= 0x20;
                }


                fprintf(fp, "SDRAM DELAY= %d %s%stask ID = %d,%d,%d,%d,%d\r\n", debug_modules.Delay, "Scale powered on due to "  ,file_name,LPC_RTC->ALSEC,LPC_RTC->ALMIN,LPC_RTC->ALHOUR,LPC_RTC->ALDOM,LPC_RTC->ALDOW);
                //LPC_RTC->ALYEAR=0;
                if ((LPC_RTC->ALDOY &0x01)==0x01)
                {
                    error_data_log.Err_data_hdr.Id++;
                    fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                    fprintf(fp, " %04d/%02d/%02d\t",
                            error_data_log.Err_data_hdr.Date.Year,
                            error_data_log.Err_data_hdr.Date.Mon,
                            error_data_log.Err_data_hdr.Date.Day);
                    fprintf(fp, " %02d:%02d:%02d\t",
                            error_data_log.Err_data_hdr.Time.Hr,
                            error_data_log.Err_data_hdr.Time.Min,
                            error_data_log.Err_data_hdr.Time.Sec);
                    fprintf(fp, " %s\t", "Event");

                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    memset(file_name, 0, sizeof(file_name));
                    fprintf(fp, "BAD ETHERNET IC!\r\n");
                }
                if ((LPC_RTC->ALDOY &(0x01<<2))==(0x01<<2))
                {
                    error_data_log.Err_data_hdr.Id++;
                    fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                    fprintf(fp, " %04d/%02d/%02d\t",
                            error_data_log.Err_data_hdr.Date.Year,
                            error_data_log.Err_data_hdr.Date.Mon,
                            error_data_log.Err_data_hdr.Date.Day);
                    fprintf(fp, " %02d:%02d:%02d\t",
                            error_data_log.Err_data_hdr.Time.Hr,
                            error_data_log.Err_data_hdr.Time.Min,
                            error_data_log.Err_data_hdr.Time.Sec);
                    fprintf(fp, " %s\t", "Event");

                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    memset(file_name, 0, sizeof(file_name));
                    fprintf(fp, "CORRUPT USB!\r\n");
                };
                if (WDT_EN ==0)
                {
                    error_data_log.Err_data_hdr.Id++;
                    fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                    fprintf(fp, " %04d/%02d/%02d\t",
                            error_data_log.Err_data_hdr.Date.Year,
                            error_data_log.Err_data_hdr.Date.Mon,
                            error_data_log.Err_data_hdr.Date.Day);
                    fprintf(fp, " %02d:%02d:%02d\t",
                            error_data_log.Err_data_hdr.Time.Hr,
                            error_data_log.Err_data_hdr.Time.Min,
                            error_data_log.Err_data_hdr.Time.Sec);
                    fprintf(fp, " %s\t", "Event");

                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    memset(file_name, 0, sizeof(file_name));
                    fprintf(fp, "WATCHDOG DISABLED!\r\n NECESSARY WIREMOD NOT DETECTED!\r\n");
                }
                else
                {
                    error_data_log.Err_data_hdr.Id++;
                    fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                    fprintf(fp, " %04d/%02d/%02d\t",
                            error_data_log.Err_data_hdr.Date.Year,
                            error_data_log.Err_data_hdr.Date.Mon,
                            error_data_log.Err_data_hdr.Date.Day);
                    fprintf(fp, " %02d:%02d:%02d\t",
                            error_data_log.Err_data_hdr.Time.Hr,
                            error_data_log.Err_data_hdr.Time.Min,
                            error_data_log.Err_data_hdr.Time.Sec);
                    fprintf(fp, " %s\t", "Event");

                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    memset(file_name, 0, sizeof(file_name));
                    fprintf(fp, "WATCHDOG ENABLED!\r\n RST WIREMOD  DETECTED!\r\n");
                }
								
						
						if(		debug_modules.flash_error == __TRUE){
						error_data_log.Err_data_hdr.Id++;
                    fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                    fprintf(fp, " %04d/%02d/%02d\t",
                            error_data_log.Err_data_hdr.Date.Year,
                            error_data_log.Err_data_hdr.Date.Mon,
                            error_data_log.Err_data_hdr.Date.Day);
                    fprintf(fp, " %02d:%02d:%02d\t",
                            error_data_log.Err_data_hdr.Time.Hr,
                            error_data_log.Err_data_hdr.Time.Min,
                            error_data_log.Err_data_hdr.Time.Sec);
                    fprintf(fp, " %s\t", "Event");

                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    memset(file_name, 0, sizeof(file_name));
                    fprintf(fp, "CORRUPT FLASH  DETECTED!\r\n");
						}
					
						if(		debug_modules.flash_error == __TRUE) {
						error_data_log.Err_data_hdr.Id++;
                    fprintf(fp, " %d\t", error_data_log.Err_data_hdr.Id);
                    fprintf(fp, " %04d/%02d/%02d\t",
                            error_data_log.Err_data_hdr.Date.Year,
                            error_data_log.Err_data_hdr.Date.Mon,
                            error_data_log.Err_data_hdr.Date.Day);
                    fprintf(fp, " %02d:%02d:%02d\t",
                            error_data_log.Err_data_hdr.Time.Hr,
                            error_data_log.Err_data_hdr.Time.Min,
                            error_data_log.Err_data_hdr.Time.Sec);
                    fprintf(fp, " %s\t", "Event");

                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    memset(file_name, 0, sizeof(file_name));
                    fprintf(fp, "CORRUPT SDRAM DETECTED!\r\n");
						}
						 LPC_RTC->ALDOW &= ~(0x03<<0);
						
            }			//else end
            //fprintf(fp, " %s%x\r\n","Scale powered on due to ",LPC_SC->RSID);

            if (ferror(fp))
            {
                //printf("\n Error log file write error");
            }

            if (0 != fclose(fp))
            {
                //		printf ("\nFile could not be closed!\n");
            }

            fflush(fp);
        }
    }
}
/*****************************************************************************
 * @note       Function name : void Read_ErrorLog_RecId(void)
 * @returns    returns       : None
 *                           :
 * @param      arg1          : None
 * @author                   : Oaces Team
 * @date       date created  : 16/02/2016
 * @brief      Description   : Get the error log no
 * @note       Notes         : None
 *****************************************************************************/
//By DK on 16 Feb 2016
void Read_ErrorLog_RecId(void)
{
#ifdef EXT_INT
    U16 j;
    U16 * read_flash;
    unsigned int Start_Address;
    U16 Data_valid;
    U32 addr_flash = 0;

    read_flash = (U16 *) EXT_FLASH_LAST_SECTOR_START_ADDR;
    Data_valid = *read_flash;

    if (Data_valid == BACKUP_PRESENT_FLAG)
    {
        Start_Address = EXT_FLASH_LAST_SECTOR_START_ADDR;
    }
    else
    {
        addr_flash = flash_para_record_search(EXT_FLASH_PARA_BACKUP_START_ADDR);
        if (!Err_data_hdrId_reload(addr_flash))
        {
            error_data_log.Err_data_hdr.Id = 1;
            Start_Address = 0;
        }
        else
        {
            Start_Address = addr_flash;
        }

    }
    if (Start_Address != 0)
    {
        read_flash = (U16 *) (Start_Address + 4);
        j = sizeof(Calculation_struct.Total_weight_accum) / 2;
        read_flash += j;
        j = sizeof(Rprts_diag_var.Job_Total) / 2;
        read_flash += j;
        j = sizeof(Rprts_diag_var.Master_total) / 2;
        read_flash += j;
        j = sizeof(Rprts_diag_var.daily_Total_weight) / 2;
        read_flash += j;
        j = sizeof(Rprts_diag_var.weekly_Total_weight) / 2;
        read_flash += j;
        j = sizeof(Rprts_diag_var.monthly_Total_weight) / 2;
        read_flash += j;
        j = sizeof(Rprts_diag_var.yearly_Total_weight) / 2;
        read_flash += j;
        j = FLASH_SIZEOF(PID_Old_Local_Setpoint);
        read_flash += j;
        j = FLASH_SIZEOF(Current_time);
        read_flash += j;
        j = sizeof(daily_rprt.Cnt_zero_speed) / 2;
        read_flash += j;
        read_flash += 4;
        read_flash += 4;
        j = sizeof(daily_rprt.Down_time) / 2;
        read_flash += j;
        read_flash += 4;
        read_flash += 4;
        j = sizeof(daily_rprt.Run_time) / 2;
        read_flash += j;
        j = sizeof(weekly_rprt.Cnt_zero_speed) / 2;
        read_flash += j;
        j = sizeof(weekly_rprt.Down_time) / 2;
        read_flash += j;
        j = sizeof(weekly_rprt.Run_time) / 2;
        read_flash += j;
        j = sizeof(monthly_rprt.Cnt_zero_speed) / 2;
        read_flash += j;
        j = sizeof(monthly_rprt.Down_time) / 2;
        read_flash += j;
        j = sizeof(monthly_rprt.Run_time) / 2;
        read_flash += j;
        j = sizeof(monthly_rprt.Run_time) / 2;
        read_flash += j;
        read_flash += 2;				//weekly_rprt.freq_rprt_header.Date.Year
        read_flash += 2;
        read_flash += 2;
        read_flash += 2;       //daily_rprt.freq_rprt_header.Time
        read_flash += 2;		//Weekly Report Header Time
        read_flash += 2;       //monthly_rprt.freq_rprt_header.Time
        read_flash++; //daily_rprt.Dummy_var1
        read_flash++; //	periodic_data.Periodic_hdr.Id
        read_flash += 2; //set_zero_data_log.Set_zero_hdr.Id & clear_data_log.Clr_data_hdr.Id
        read_flash += 2; //calib_data_log.Cal_data_hdr.Id & length_cal_data_log.Len_data_hdr.Id
        //read_flash++;
        Data_valid = *read_flash;
        error_data_log.Err_data_hdr.Id = Data_valid;
    }
#endif
}

/*****************************************************************************
 * End of file
 *****************************************************************************/
