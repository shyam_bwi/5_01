L 1 "Expansion_Slot\Expansion_slot_low_level.c"
N/*****************************************************************************
N* @copyright Copyright (c) 2012-2013 Beltway, Inc.
N* @copyright This software is a copyrighted work and/or information
N* @copyright protected as a trade secret. Legal rights of Beltway. In this
N* @copyright software is distinct from ownership of any medium in which
N* @copyright the software is embodied. Copyright or trade secret notices
N* @copyright included must be reproduced in any copies authorized by
N* @copyright Beltway, Inc.
N*
N* @detail Project      : Beltscale Weighing Product - Integrator Board
N* @detail Customer     : Beltway
N*
N* @file Filename       : Expansion_slot_low_level.c
N* @brief               : Serial Input Output for NXP NXP LPC178x/7x
N*
N* @author              : Anagha Basole
N*
N* @date Created        : June 12, 2013
N* @date Last Modified  : June 12, 2013
N*
N* @internal Change Log : <YYYY-MM-DD>
N* @internal            :
N* @internal            :
N*
N*****************************************************************************/
N#ifdef EXPANSION_SLOT
S
S//DRIVER NOT TESTED
S/*============================================================================
S* Include Header Files
S*===========================================================================*/
S#include <stdio.h>
S#include <RTL.h>
S#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
S#include "Expansion_slot_low_level.h"
S
S/*============================================================================
S* Private Macro Definitions
S*===========================================================================*/
S/* Defines Section */
S#define EXP_SLOT_UART            LPC_UART0
S
S#define EXP_SLOT_UART_PCONP      3           /*UART0 corresponds to PCONP bit 3*/
S
S#define EXP_SLOT_PORT_RX         P0_0
S#define EXP_SLOT_PORT_TX         P0_1
S
S#define EXP_SLOT_LCR_COM_PARAM   0x83         /* 8 bits, no Parity, 1 Stop bit   */
S#define EXP_SLOT_LCR_DLAB0       0x03         /* DLAB = 0                        */
S#define EXP_SLOT_DLL_VAL         21           /* 115200 Baud Rate @ 60.0 MHZ PCLK*/
S#define EXP_SLOT_DLM_VAL         0            /* High divisor latch = 0          */
S#define EXP_SLOT_FDR_VAL         0x95         /* FR 1,556, DIVADDVAL=5, MULVAL=9 */
S
S/*============================================================================
S* Private Data Types
S*===========================================================================*/
S
S/*============================================================================
S*Public Variables
S*===========================================================================*/
S/* Constants section */
S
S/* Boolean variables section */
S
S/* Character variables section */
S
S/* unsigned integer variables section */
S
S/* Signed integer variables section */
S
S/* unsigned long integer variables section */
S
S/* signed long integer variables section */
S
S/* unsigned long long integer variables section */
S
S/* signed long long integer variables section */
S
S/* Float variables section */
S
S/* Double variables section */
S
S/* Structure or Union variables section */
S
S/*============================================================================
S* Private Variables
S*===========================================================================*/
S/* Constants section */
S
S/* Boolean variables section */
S
S/* Character variables section */
S
S/* unsigned integer variables section */
S
S/* Signed integer variables section */
S
S/* unsigned long integer variables section */
S
S/* signed long integer variables section */
S
S/* unsigned long long integer variables section */
S
S/* signed long long integer variables section */
S
S/* Float variables section */
S
S/* Double variables section */
S
S/* Structure or Union variables section */
S
S/*============================================================================
S* Private Function Prototypes Declarations
S*===========================================================================*/
S
S/*============================================================================
S* Function Implementation Section
S*===========================================================================*/
S
S /*****************************************************************************
S* @note       Function name: void Expansion_slot_init (void)
S* @returns    returns      : None
S* @param      arg1         : None
S* @param      arg2         : None
S* @author                  : Anagha Basole
S* @date       date created : June 12, 2013
S* @brief      Description  : Initialize Serial Interface
S* @note       Notes        :
S*****************************************************************************/
Svoid Expansion_slot_init (void)
S{
S
S  /* Power Up the UART0 controller. */
S  LPC_SC->PCONP |=  (1 << EXP_SLOT_UART_PCONP);
S
S  /* Configure UART0 pins */
S  LPC_IOCON->EXP_SLOT_PORT_TX =  (1UL << 0);             /* Pin P0.10 used as TXD2*/
S  LPC_IOCON->EXP_SLOT_PORT_RX =  (1UL << 0);             /* Pin P0.11 used as RXD2*/
S
S  /* Init UART0*/
S  EXP_SLOT_UART->LCR   = EXP_SLOT_LCR_COM_PARAM;         /* 8 bits, no Parity, 1 Stop bit   */
S  EXP_SLOT_UART->DLL   = EXP_SLOT_DLL_VAL;               /* 115200 Baud Rate @ 60.0 MHZ PCLK*/
S  EXP_SLOT_UART->FDR   = EXP_SLOT_FDR_VAL;               /* FR 1,556, DIVADDVAL=5, MULVAL=9 */
S  EXP_SLOT_UART->DLM   = EXP_SLOT_DLM_VAL;               /* High divisor latch = 0          */
S  EXP_SLOT_UART->LCR   = EXP_SLOT_LCR_DLAB0;             /* DLAB = 0                        */
S}
S
S/*****************************************************************************
S* @note       Function name: S32 Expansion_slot_putchar (S32 ch)
S* @returns    returns      : The character output
S* @param      arg1         : the character to be output
S* @author                  : Anagha Basole
S* @date       date created : June 12, 2013
S* @brief      Description  : Write a character to the Serial Port
S* @note       Notes        : None
S*****************************************************************************/
SS32 Expansion_slot_putchar (S32 ch)
S{
S  //check if THR contains valid data (0 corresponds to valid data, 1 corresponds to empty)
S  while (!(EXP_SLOT_UART->LSR & 0x20));
S  EXP_SLOT_UART->THR = ch; //load the character to be transmitted into the THR
S
S  return (ch);
S}
S
S/*****************************************************************************
S* @note       Function name: S32 Expansion_slot_getchar (void)
S* @returns    returns      : the parameter read or -1 in case of error
S* @param      arg1         : None
S* @author                  : Anagha Basole
S* @date       date created : June 12, 2013
S* @brief      Description  : Read a character from the Serial Port
S* @note       Notes        : None
S*****************************************************************************/
SS32 Expansion_slot_getchar (void)
S{
S  //check if data has been received
S  if (EXP_SLOT_UART->LSR & 0x01)
S  {
S    //read the received character from the receive buffer.
S    return (EXP_SLOT_UART->RBR);
S  }
S  return (-1);
S
S}
N#endif /*EXPANSION_SLOT*/
N/*****************************************************************************
N* End of file
N*****************************************************************************/
