; generated by ARM C/C++ Compiler, 5.03 [Build 24]
; commandline ArmCC [--list --debug -c --asm --interleave -o.\Build\lpc177x_8x_eeprom.o --asm_dir=".\Linker files\\" --list_dir=".\Linker files\\" --depend=.\Build\lpc177x_8x_eeprom.d --cpu=Cortex-M3 --apcs=interwork -O0 --diag_suppress=9931 -I.\Common -I.\RTOS -I.\Debug -I.\USB -I.\FS -I.\Ethernet -I.\Keypad -I.\Modbus\application_layer -I.\Printer -I.\Scoreboard -I.\GUI -I.\GUI\Application -IC:\Keil\ARM\RV31\INC -IC:\Keil\ARM\RV31\LIB -I.\IOboard -I.\External_Memory -I.\Calculations -I..\LPC1788 -I.\EEPROM -I.\Modbus\Port -I.\Modbus\mbslave -I.\Modbus\mbslave\include -I.\Modbus\application_layer -I.\Modbus\mbmaster -I.\Modbus\mbmaster\include -I.\Modbus\common -I.\Screen_Shot -I.\Expansion_Slot -I.\Master_Variable_Table -I.\ini -I.\wdt -I.\UDP_TEST -IC:\Keil\ARM\RV31\INC -IC:\Keil\ARM\CMSIS\Include -IC:\Keil\ARM\Inc\NXP\LPC177x_8x -D__RTX -D_IAP -D_NVIC --omf_browse=.\Build\lpc177x_8x_eeprom.crf Common\lpc177x_8x_eeprom.c]
                          THUMB

                          AREA ||.text||, CODE, READONLY, ALIGN=2

                  EEPROM_Init PROC
;;;54      **********************************************************************/
;;;55     void EEPROM_Init(void)
000000  b570              PUSH     {r4-r6,lr}
;;;56     {
;;;57     	uint32_t val, cclk;
;;;58     	LPC_EEPROM->PWRDWN = 0x0;
000002  2000              MOVS     r0,#0
000004  49e4              LDR      r1,|L1.920|
000006  6008              STR      r0,[r1,#0]
;;;59     	/* EEPROM is automate turn on after reset */
;;;60     	/* Setting clock:
;;;61     	 * EEPROM required a 375kHz. This clock is generated by dividing the
;;;62     	 * system bus clock.
;;;63     	 */
;;;64        	cclk = CLKPWR_GetCLK(CLKPWR_CLKTYPE_CPU);
000008  f7fffffe          BL       CLKPWR_GetCLK
00000c  4605              MOV      r5,r0
;;;65     	val = (cclk/375000)-1;
00000e  48e3              LDR      r0,|L1.924|
000010  fbb5f0f0          UDIV     r0,r5,r0
000014  1e44              SUBS     r4,r0,#1
;;;66     	LPC_EEPROM->CLKDIV = val;
000016  f44f1000          MOV      r0,#0x200000
00001a  f8c04094          STR      r4,[r0,#0x94]
;;;67     
;;;68     	/* Setting wait state */
;;;69     	val  = ((((cclk / 1000000) * 15) / 1000) + 1);
00001e  48e0              LDR      r0,|L1.928|
000020  fbb5f0f0          UDIV     r0,r5,r0
000024  ebc01000          RSB      r0,r0,r0,LSL #4
000028  f44f717a          MOV      r1,#0x3e8
00002c  fbb0f0f1          UDIV     r0,r0,r1
000030  1c44              ADDS     r4,r0,#1
;;;70     	val |= (((((cclk / 1000000) * 55) / 1000) + 1) << 8);
000032  48db              LDR      r0,|L1.928|
000034  fbb5f0f0          UDIV     r0,r5,r0
000038  eb0001c0          ADD      r1,r0,r0,LSL #3
00003c  ebc11080          RSB      r0,r1,r0,LSL #6
000040  f44f717a          MOV      r1,#0x3e8
000044  fbb0f0f1          UDIV     r0,r0,r1
000048  1c40              ADDS     r0,r0,#1
00004a  ea442400          ORR      r4,r4,r0,LSL #8
;;;71     	val |= (((((cclk / 1000000) * 35) / 1000) + 1) << 16);
00004e  48d4              LDR      r0,|L1.928|
000050  fbb5f0f0          UDIV     r0,r5,r0
000054  ebc000c0          RSB      r0,r0,r0,LSL #3
000058  eb000080          ADD      r0,r0,r0,LSL #2
00005c  fbb0f0f1          UDIV     r0,r0,r1
000060  1c40              ADDS     r0,r0,#1
000062  ea444400          ORR      r4,r4,r0,LSL #16
;;;72     	LPC_EEPROM->WSTATE = val;
000066  48cf              LDR      r0,|L1.932|
000068  6004              STR      r4,[r0,#0]
;;;73     }
00006a  bd70              POP      {r4-r6,pc}
;;;74     
                          ENDP

                  EEPROM_Write PROC
;;;88      **********************************************************************/
;;;89     void EEPROM_Write(uint16_t page_offset, uint16_t page_address, void* data, EEPROM_Mode_Type mode, uint32_t count)
00006c  e92d43f0          PUSH     {r4-r9,lr}
;;;90     {
000070  9f07              LDR      r7,[sp,#0x1c]
;;;91         uint32_t i;
;;;92         uint8_t *tmp8 = (uint8_t *)data;
000072  4615              MOV      r5,r2
;;;93         uint16_t *tmp16 = (uint16_t *)data;
000074  4616              MOV      r6,r2
;;;94         uint32_t *tmp32 = (uint32_t *)data;
000076  4694              MOV      r12,r2
;;;95     
;;;96     	LPC_EEPROM->INT_CLR_STATUS = ((1 << EEPROM_ENDOF_RW)|(1 << EEPROM_ENDOF_PROG));
000078  f04f58a0          MOV      r8,#0x14000000
00007c  f8df9328          LDR      r9,|L1.936|
000080  f8c98000          STR      r8,[r9,#0]
;;;97     	//check page_offset
;;;98     	if(mode == MODE_16_BIT){
000084  2b01              CMP      r3,#1
000086  d104              BNE      |L1.146|
;;;99     		if((page_offset & 0x01)!=0) while(1);
000088  f0100f01          TST      r0,#1
00008c  d008              BEQ      |L1.160|
00008e  bf00              NOP      
                  |L1.144|
000090  e7fe              B        |L1.144|
                  |L1.146|
;;;100    	}
;;;101    	else if(mode == MODE_32_BIT){
000092  2b02              CMP      r3,#2
000094  d104              BNE      |L1.160|
;;;102    	 	if((page_offset & 0x03)!=0) while(1);
000096  f0100f03          TST      r0,#3
00009a  d001              BEQ      |L1.160|
00009c  bf00              NOP      
                  |L1.158|
00009e  e7fe              B        |L1.158|
                  |L1.160|
;;;103    	}
;;;104    	LPC_EEPROM->ADDR = EEPROM_PAGE_OFFSET(page_offset);
0000a0  f000083f          AND      r8,r0,#0x3f
0000a4  f44f1900          MOV      r9,#0x200000
0000a8  f8c98084          STR      r8,[r9,#0x84]
;;;105    	for(i=0;i<count;i++)
0000ac  2400              MOVS     r4,#0
0000ae  e08b              B        |L1.456|
                  |L1.176|
;;;106    	{
;;;107    		//update data to page register
;;;108    		if(mode == MODE_8_BIT){
0000b0  b97b              CBNZ     r3,|L1.210|
;;;109    			LPC_EEPROM->CMD = EEPROM_CMD_8_BIT_WRITE;
0000b2  f04f0803          MOV      r8,#3
0000b6  f44f1900          MOV      r9,#0x200000
0000ba  f8c98080          STR      r8,[r9,#0x80]
;;;110    			LPC_EEPROM -> WDATA = *tmp8;
0000be  f8958000          LDRB     r8,[r5,#0]
0000c2  f8c98088          STR      r8,[r9,#0x88]
;;;111    			tmp8++;
0000c6  1c6d              ADDS     r5,r5,#1
;;;112    			page_offset +=1;
0000c8  f1000801          ADD      r8,r0,#1
0000cc  fa1ff088          UXTH     r0,r8
0000d0  e021              B        |L1.278|
                  |L1.210|
;;;113    		}
;;;114    		else if(mode == MODE_16_BIT){
0000d2  2b01              CMP      r3,#1
0000d4  d10f              BNE      |L1.246|
;;;115    			LPC_EEPROM->CMD = EEPROM_CMD_16_BIT_WRITE;
0000d6  f04f0804          MOV      r8,#4
0000da  f44f1900          MOV      r9,#0x200000
0000de  f8c98080          STR      r8,[r9,#0x80]
;;;116    			LPC_EEPROM -> WDATA = *tmp16;
0000e2  f8b68000          LDRH     r8,[r6,#0]
0000e6  f8c98088          STR      r8,[r9,#0x88]
;;;117    			tmp16++;
0000ea  1cb6              ADDS     r6,r6,#2
;;;118    			page_offset +=2;
0000ec  f1000802          ADD      r8,r0,#2
0000f0  fa1ff088          UXTH     r0,r8
0000f4  e00f              B        |L1.278|
                  |L1.246|
;;;119    		}
;;;120    		else{
;;;121    			LPC_EEPROM->CMD = EEPROM_CMD_32_BIT_WRITE;
0000f6  f04f0805          MOV      r8,#5
0000fa  f44f1900          MOV      r9,#0x200000
0000fe  f8c98080          STR      r8,[r9,#0x80]
;;;122    			LPC_EEPROM -> WDATA = *tmp32;
000102  f8dc8000          LDR      r8,[r12,#0]
000106  f8c98088          STR      r8,[r9,#0x88]
;;;123    			tmp32++;
00010a  f10c0c04          ADD      r12,r12,#4
;;;124    			page_offset +=4;
00010e  f1000804          ADD      r8,r0,#4
000112  fa1ff088          UXTH     r0,r8
                  |L1.278|
;;;125    		}
;;;126    		while(!((LPC_EEPROM->INT_STATUS >> EEPROM_ENDOF_RW)&0x01));
000116  bf00              NOP      
                  |L1.280|
000118  f8df828c          LDR      r8,|L1.936|
00011c  f1a80808          SUB      r8,r8,#8
000120  f8d88000          LDR      r8,[r8,#0]
000124  f3c86880          UBFX     r8,r8,#26,#1
000128  f1b80f00          CMP      r8,#0
00012c  d0f4              BEQ      |L1.280|
;;;127    		LPC_EEPROM->INT_CLR_STATUS = (1 << EEPROM_ENDOF_RW);
00012e  f04f6880          MOV      r8,#0x4000000
000132  f8df9274          LDR      r9,|L1.936|
000136  f8c98000          STR      r8,[r9,#0]
;;;128    		if((page_offset >= EEPROM_PAGE_SIZE)|(i==count-1)){
00013a  2840              CMP      r0,#0x40
00013c  db02              BLT      |L1.324|
00013e  f04f0801          MOV      r8,#1
000142  e001              B        |L1.328|
                  |L1.324|
000144  f04f0800          MOV      r8,#0
                  |L1.328|
000148  f1a70901          SUB      r9,r7,#1
00014c  45a1              CMP      r9,r4
00014e  d102              BNE      |L1.342|
000150  f04f0901          MOV      r9,#1
000154  e001              B        |L1.346|
                  |L1.342|
000156  f04f0900          MOV      r9,#0
                  |L1.346|
00015a  ea580809          ORRS     r8,r8,r9
00015e  d023              BEQ      |L1.424|
;;;129    			//update to EEPROM memory
;;;130    			LPC_EEPROM->INT_CLR_STATUS = (0x1 << EEPROM_ENDOF_PROG);
000160  f04f5880          MOV      r8,#0x10000000
000164  f8df9240          LDR      r9,|L1.936|
000168  f8c98000          STR      r8,[r9,#0]
;;;131    			LPC_EEPROM->ADDR = EEPROM_PAGE_ADRESS(page_address);
00016c  ea4f6881          LSL      r8,r1,#26
000170  ea4f5818          LSR      r8,r8,#20
000174  f44f1900          MOV      r9,#0x200000
000178  f8c98084          STR      r8,[r9,#0x84]
;;;132    			LPC_EEPROM->CMD = EEPROM_CMD_ERASE_PRG_PAGE;
00017c  f04f0806          MOV      r8,#6
000180  f8c98080          STR      r8,[r9,#0x80]
;;;133    			while(!((LPC_EEPROM->INT_STATUS >> EEPROM_ENDOF_PROG)&0x01));
000184  bf00              NOP      
                  |L1.390|
000186  f8df8220          LDR      r8,|L1.936|
00018a  f1a80808          SUB      r8,r8,#8
00018e  f8d88000          LDR      r8,[r8,#0]
000192  f3c87800          UBFX     r8,r8,#28,#1
000196  f1b80f00          CMP      r8,#0
00019a  d0f4              BEQ      |L1.390|
;;;134    			LPC_EEPROM->INT_CLR_STATUS = (1 << EEPROM_ENDOF_PROG);
00019c  f04f5880          MOV      r8,#0x10000000
0001a0  f8df9204          LDR      r9,|L1.936|
0001a4  f8c98000          STR      r8,[r9,#0]
                  |L1.424|
;;;135    		}
;;;136    		if(page_offset >= EEPROM_PAGE_SIZE)
0001a8  2840              CMP      r0,#0x40
0001aa  db0c              BLT      |L1.454|
;;;137    		{
;;;138    			page_offset = 0;
0001ac  2000              MOVS     r0,#0
;;;139    			page_address +=1;
0001ae  f1010801          ADD      r8,r1,#1
0001b2  fa1ff188          UXTH     r1,r8
;;;140    			LPC_EEPROM->ADDR =0;
0001b6  4680              MOV      r8,r0
0001b8  f44f1900          MOV      r9,#0x200000
0001bc  f8c98084          STR      r8,[r9,#0x84]
;;;141    			if(page_address > EEPROM_PAGE_NUM - 1) page_address = 0;
0001c0  293e              CMP      r1,#0x3e
0001c2  dd00              BLE      |L1.454|
0001c4  2100              MOVS     r1,#0
                  |L1.454|
0001c6  1c64              ADDS     r4,r4,#1              ;105
                  |L1.456|
0001c8  42bc              CMP      r4,r7                 ;105
0001ca  f4ffaf71          BCC      |L1.176|
;;;142    		}
;;;143    	}
;;;144    }
0001ce  e8bd83f0          POP      {r4-r9,pc}
;;;145    
                          ENDP

                  EEPROM_Read PROC
;;;156     **********************************************************************/
;;;157    void EEPROM_Read(uint16_t page_offset, uint16_t page_address, void* data, EEPROM_Mode_Type mode, uint32_t count)
0001d2  e92d43f0          PUSH     {r4-r9,lr}
;;;158    {
0001d6  9f07              LDR      r7,[sp,#0x1c]
;;;159            uint32_t i;
;;;160    	uint8_t *tmp8 = (uint8_t *)data;
0001d8  4615              MOV      r5,r2
;;;161    	uint16_t *tmp16 = (uint16_t *)data;
0001da  4616              MOV      r6,r2
;;;162    	uint32_t *tmp32 = (uint32_t *)data;
0001dc  4694              MOV      r12,r2
;;;163    
;;;164    	LPC_EEPROM->INT_CLR_STATUS = ((1 << EEPROM_ENDOF_RW)|(1 << EEPROM_ENDOF_PROG));
0001de  f04f58a0          MOV      r8,#0x14000000
0001e2  f8df91c4          LDR      r9,|L1.936|
0001e6  f8c98000          STR      r8,[r9,#0]
;;;165    	LPC_EEPROM->ADDR = EEPROM_PAGE_ADRESS(page_address)|EEPROM_PAGE_OFFSET(page_offset);
0001ea  f001093f          AND      r9,r1,#0x3f
0001ee  4680              MOV      r8,r0
0001f0  f369189f          BFI      r8,r9,#6,#26
0001f4  f44f1900          MOV      r9,#0x200000
0001f8  f8c98084          STR      r8,[r9,#0x84]
;;;166    	if(mode == MODE_8_BIT)
0001fc  b923              CBNZ     r3,|L1.520|
;;;167    		LPC_EEPROM->CMD = EEPROM_CMD_8_BIT_READ|EEPROM_CMD_RDPREFETCH;
0001fe  f04f0808          MOV      r8,#8
000202  f8c98080          STR      r8,[r9,#0x80]
000206  e016              B        |L1.566|
                  |L1.520|
;;;168    	else if(mode == MODE_16_BIT){
000208  2b01              CMP      r3,#1
00020a  d10a              BNE      |L1.546|
;;;169    		LPC_EEPROM->CMD = EEPROM_CMD_16_BIT_READ|EEPROM_CMD_RDPREFETCH;
00020c  f04f0809          MOV      r8,#9
000210  f44f1900          MOV      r9,#0x200000
000214  f8c98080          STR      r8,[r9,#0x80]
;;;170    		//check page_offset
;;;171    		if((page_offset &0x01)!=0)
000218  f0100f01          TST      r0,#1
00021c  d00b              BEQ      |L1.566|
                  |L1.542|
;;;172    			return;
;;;173    	}
;;;174    	else{
;;;175    		LPC_EEPROM->CMD = EEPROM_CMD_32_BIT_READ|EEPROM_CMD_RDPREFETCH;
;;;176    		//page_offset must be a multiple of 0x04
;;;177    		if((page_offset & 0x03)!=0)
;;;178    			return;
;;;179    	}
;;;180    
;;;181    	//read and store data in buffer
;;;182    	for(i=0;i<count;i++){
;;;183    		 
;;;184    		 if(mode == MODE_8_BIT){
;;;185    			 *tmp8 = (uint8_t)(LPC_EEPROM -> RDATA);
;;;186    			 tmp8++;
;;;187    			 page_offset +=1;
;;;188    		 }
;;;189    		 else if (mode == MODE_16_BIT)
;;;190    		 {
;;;191    			 *tmp16 =  (uint16_t)(LPC_EEPROM -> RDATA);
;;;192    			 tmp16++;
;;;193    			 page_offset +=2;
;;;194    		 }
;;;195    		 else{
;;;196    			 *tmp32 = (uint32_t)(LPC_EEPROM ->RDATA);
;;;197    			 tmp32++;
;;;198    			 page_offset +=4;
;;;199    		 }
;;;200    		 while(!((LPC_EEPROM->INT_STATUS >> EEPROM_ENDOF_RW)&0x01));
;;;201                     LPC_EEPROM->INT_CLR_STATUS = (1 << EEPROM_ENDOF_RW);
;;;202    		 if((page_offset >= EEPROM_PAGE_SIZE) && (i < count - 1)) {
;;;203    			 page_offset = 0;
;;;204    			 page_address++;
;;;205    			 LPC_EEPROM->ADDR = EEPROM_PAGE_ADRESS(page_address)|EEPROM_PAGE_OFFSET(page_offset);
;;;206    			 if(mode == MODE_8_BIT)
;;;207    			 	LPC_EEPROM->CMD = EEPROM_CMD_8_BIT_READ|EEPROM_CMD_RDPREFETCH;
;;;208    			 else if(mode == MODE_16_BIT)
;;;209    				LPC_EEPROM->CMD = EEPROM_CMD_16_BIT_READ|EEPROM_CMD_RDPREFETCH;
;;;210    			 else
;;;211    			 	LPC_EEPROM->CMD = EEPROM_CMD_32_BIT_READ|EEPROM_CMD_RDPREFETCH;
;;;212    		 }
;;;213    	}
;;;214    }
00021e  e8bd83f0          POP      {r4-r9,pc}
                  |L1.546|
000222  f04f080a          MOV      r8,#0xa               ;175
000226  f44f1900          MOV      r9,#0x200000          ;175
00022a  f8c98080          STR      r8,[r9,#0x80]         ;175
00022e  f0100f03          TST      r0,#3                 ;177
000232  d000              BEQ      |L1.566|
000234  e7f3              B        |L1.542|
                  |L1.566|
000236  2400              MOVS     r4,#0                 ;182
000238  e062              B        |L1.768|
                  |L1.570|
00023a  b95b              CBNZ     r3,|L1.596|
00023c  f44f1800          MOV      r8,#0x200000          ;185
000240  f8d8808c          LDR      r8,[r8,#0x8c]         ;185
000244  f8858000          STRB     r8,[r5,#0]            ;185
000248  1c6d              ADDS     r5,r5,#1              ;186
00024a  f1000801          ADD      r8,r0,#1              ;187
00024e  fa1ff088          UXTH     r0,r8                 ;187
000252  e019              B        |L1.648|
                  |L1.596|
000254  2b01              CMP      r3,#1                 ;189
000256  d10b              BNE      |L1.624|
000258  f44f1800          MOV      r8,#0x200000          ;191
00025c  f8d8808c          LDR      r8,[r8,#0x8c]         ;191
000260  f8a68000          STRH     r8,[r6,#0]            ;191
000264  1cb6              ADDS     r6,r6,#2              ;192
000266  f1000802          ADD      r8,r0,#2              ;193
00026a  fa1ff088          UXTH     r0,r8                 ;193
00026e  e00b              B        |L1.648|
                  |L1.624|
000270  f44f1800          MOV      r8,#0x200000          ;196
000274  f8d8808c          LDR      r8,[r8,#0x8c]         ;196
000278  f8cc8000          STR      r8,[r12,#0]           ;196
00027c  f10c0c04          ADD      r12,r12,#4            ;197
000280  f1000804          ADD      r8,r0,#4              ;198
000284  fa1ff088          UXTH     r0,r8                 ;198
                  |L1.648|
000288  bf00              NOP                            ;200
                  |L1.650|
00028a  f8df811c          LDR      r8,|L1.936|
00028e  f1a80808          SUB      r8,r8,#8              ;200
000292  f8d88000          LDR      r8,[r8,#0]            ;200
000296  f3c86880          UBFX     r8,r8,#26,#1          ;200
00029a  f1b80f00          CMP      r8,#0                 ;200
00029e  d0f4              BEQ      |L1.650|
0002a0  f04f6880          MOV      r8,#0x4000000         ;201
0002a4  f8df9100          LDR      r9,|L1.936|
0002a8  f8c98000          STR      r8,[r9,#0]            ;201
0002ac  2840              CMP      r0,#0x40              ;202
0002ae  db26              BLT      |L1.766|
0002b0  f1a70801          SUB      r8,r7,#1              ;202
0002b4  45a0              CMP      r8,r4                 ;202
0002b6  d922              BLS      |L1.766|
0002b8  2000              MOVS     r0,#0                 ;203
0002ba  f1010801          ADD      r8,r1,#1              ;204
0002be  fa1ff188          UXTH     r1,r8                 ;204
0002c2  f001093f          AND      r9,r1,#0x3f           ;205
0002c6  4680              MOV      r8,r0                 ;205
0002c8  f369189f          BFI      r8,r9,#6,#26          ;205
0002cc  f44f1900          MOV      r9,#0x200000          ;205
0002d0  f8c98084          STR      r8,[r9,#0x84]         ;205
0002d4  b923              CBNZ     r3,|L1.736|
0002d6  f04f0808          MOV      r8,#8                 ;207
0002da  f8c98080          STR      r8,[r9,#0x80]         ;207
0002de  e00e              B        |L1.766|
                  |L1.736|
0002e0  2b01              CMP      r3,#1                 ;208
0002e2  d106              BNE      |L1.754|
0002e4  f04f0809          MOV      r8,#9                 ;209
0002e8  f44f1900          MOV      r9,#0x200000          ;209
0002ec  f8c98080          STR      r8,[r9,#0x80]         ;209
0002f0  e005              B        |L1.766|
                  |L1.754|
0002f2  f04f080a          MOV      r8,#0xa               ;211
0002f6  f44f1900          MOV      r9,#0x200000          ;211
0002fa  f8c98080          STR      r8,[r9,#0x80]         ;211
                  |L1.766|
0002fe  1c64              ADDS     r4,r4,#1              ;182
                  |L1.768|
000300  42bc              CMP      r4,r7                 ;182
000302  d39a              BCC      |L1.570|
000304  bf00              NOP      
000306  e78a              B        |L1.542|
;;;215    
                          ENDP

                  EEPROM_Erase PROC
;;;220     **********************************************************************/
;;;221    void EEPROM_Erase(uint16_t page_address)
000308  b510              PUSH     {r4,lr}
;;;222    {
;;;223    	uint32_t i;
;;;224        uint32_t count = EEPROM_PAGE_SIZE/4;
00030a  2210              MOVS     r2,#0x10
;;;225    
;;;226        LPC_EEPROM->INT_CLR_STATUS = ((1 << EEPROM_ENDOF_RW)|(1 << EEPROM_ENDOF_PROG));  //SKS clear rw interrupt flag and program interrupt flag 
00030c  f04f53a0          MOV      r3,#0x14000000
000310  4c25              LDR      r4,|L1.936|
000312  6023              STR      r3,[r4,#0]
;;;227    
;;;228    	//clear page register
;;;229        LPC_EEPROM->ADDR = EEPROM_PAGE_OFFSET(0);	//write 0 to bits 6-11
000314  2300              MOVS     r3,#0
000316  4c25              LDR      r4,|L1.940|
000318  6023              STR      r3,[r4,#0]
;;;230    	LPC_EEPROM->CMD = EEPROM_CMD_32_BIT_WRITE;	//32 bit write
00031a  f04f0305          MOV      r3,#5
00031e  f44f1400          MOV      r4,#0x200000
000322  f8c43080          STR      r3,[r4,#0x80]
;;;231    	for(i=0;i<count;i++)						//0-15
000326  f04f0100          MOV      r1,#0
00032a  e00f              B        |L1.844|
                  |L1.812|
;;;232    	{
;;;233    		LPC_EEPROM->WDATA = 0;					
00032c  2300              MOVS     r3,#0
00032e  4c20              LDR      r4,|L1.944|
000330  6023              STR      r3,[r4,#0]
;;;234    		while(!((LPC_EEPROM->INT_STATUS >> EEPROM_ENDOF_RW)&0x01));
000332  bf00              NOP      
                  |L1.820|
000334  4b1f              LDR      r3,|L1.948|
000336  681b              LDR      r3,[r3,#0]
000338  f3c36380          UBFX     r3,r3,#26,#1
00033c  2b00              CMP      r3,#0
00033e  d0f9              BEQ      |L1.820|
;;;235    		LPC_EEPROM->INT_CLR_STATUS = (1 << EEPROM_ENDOF_RW);
000340  f04f6380          MOV      r3,#0x4000000
000344  4c18              LDR      r4,|L1.936|
000346  6023              STR      r3,[r4,#0]
000348  f1010101          ADD      r1,r1,#1              ;231
                  |L1.844|
00034c  4291              CMP      r1,r2                 ;231
00034e  d3ed              BCC      |L1.812|
;;;236    	}											//why writing 0 on first page at every erase ?
;;;237    
;;;238        LPC_EEPROM->INT_CLR_STATUS = (0x1 << EEPROM_ENDOF_PROG);
000350  f04f5380          MOV      r3,#0x10000000
000354  4c14              LDR      r4,|L1.936|
000356  6023              STR      r3,[r4,#0]
;;;239    	LPC_EEPROM->ADDR = EEPROM_PAGE_ADRESS(page_address);
000358  0683              LSLS     r3,r0,#26
00035a  0d1b              LSRS     r3,r3,#20
00035c  4c13              LDR      r4,|L1.940|
00035e  6023              STR      r3,[r4,#0]
;;;240    	LPC_EEPROM->CMD = EEPROM_CMD_ERASE_PRG_PAGE;
000360  f04f0306          MOV      r3,#6
000364  f44f1400          MOV      r4,#0x200000
000368  f8c43080          STR      r3,[r4,#0x80]
;;;241    	while(!((LPC_EEPROM->INT_STATUS >> EEPROM_ENDOF_PROG)&0x01));
00036c  bf00              NOP      
                  |L1.878|
00036e  4b11              LDR      r3,|L1.948|
000370  681b              LDR      r3,[r3,#0]
000372  f3c37300          UBFX     r3,r3,#28,#1
000376  2b00              CMP      r3,#0
000378  d0f9              BEQ      |L1.878|
;;;242    	LPC_EEPROM->INT_CLR_STATUS = (1 << EEPROM_ENDOF_PROG);
00037a  f04f5380          MOV      r3,#0x10000000
00037e  4c0a              LDR      r4,|L1.936|
000380  6023              STR      r3,[r4,#0]
;;;243    }
000382  bd10              POP      {r4,pc}
;;;244    
                          ENDP

                  EEPROM_PowerDown PROC
;;;251     **********************************************************************/
;;;252    void EEPROM_PowerDown(FunctionalState NewState)
000384  2801              CMP      r0,#1
;;;253    {
;;;254    	if(NewState == ENABLE)
000386  d103              BNE      |L1.912|
;;;255    		LPC_EEPROM->PWRDWN = 0x1;
000388  2101              MOVS     r1,#1
00038a  4a03              LDR      r2,|L1.920|
00038c  6011              STR      r1,[r2,#0]
00038e  e002              B        |L1.918|
                  |L1.912|
;;;256    	else
;;;257    		LPC_EEPROM->PWRDWN = 0x0;
000390  2100              MOVS     r1,#0
000392  4a01              LDR      r2,|L1.920|
000394  6011              STR      r1,[r2,#0]
                  |L1.918|
;;;258    }
000396  4770              BX       lr
;;;259    
                          ENDP

                  |L1.920|
                          DCD      0x00200098
                  |L1.924|
                          DCD      0x0005b8d8
                  |L1.928|
                          DCD      0x000f4240
                  |L1.932|
                          DCD      0x00200090
                  |L1.936|
                          DCD      0x00200fe8
                  |L1.940|
                          DCD      0x00200084
                  |L1.944|
                          DCD      0x00200088
                  |L1.948|
                          DCD      0x00200fe0

;*** Start embedded assembler ***

#line 1 "Common\\lpc177x_8x_eeprom.c"
	AREA ||.rev16_text||, CODE, READONLY
	THUMB
	EXPORT |__asm___19_lpc177x_8x_eeprom_c_2d9ead06____REV16|
#line 115 "C:\\Keil\\ARM\\CMSIS\\Include\\core_cmInstr.h"
|__asm___19_lpc177x_8x_eeprom_c_2d9ead06____REV16| PROC
#line 116

 rev16 r0, r0
 bx lr
	ENDP
	AREA ||.revsh_text||, CODE, READONLY
	THUMB
	EXPORT |__asm___19_lpc177x_8x_eeprom_c_2d9ead06____REVSH|
#line 130
|__asm___19_lpc177x_8x_eeprom_c_2d9ead06____REVSH| PROC
#line 131

 revsh r0, r0
 bx lr
	ENDP

;*** End   embedded assembler ***
