/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename        : RW_FLASH.h
 * @brief               : read write routines for external flash
 *
 * @author               : Shyam Shrivastava
 *
 * @date Created         : February Monday , 2017  <Feb 20, 2017>
 * @date Last Modified   : February Monday , 2017  <Feb 20, 2017>
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal            :
 * @internal            :
 *
 *****************************************************************************/
 
#ifndef __RW_FLASH_h
#define __RW_FLASH_h
#include <RTL.h> 
#define FS_SECTOR_SIZE	4
#define 	FS_PREALLOCATE_SIZE	4

void write_log_to_flash_periodic_log(U16 type_of_log);
int  findRecord(U16 record_ID);
void write_log_to_flash(U8 type_of_log);
void flash_log_sync_to_usb(int dom_to_sync,int mon_to_sync,int year_to_sync,int hour_to_sync,int min_to_sync,int sec_to_sync);
void verify_lastRecord(void);
#endif
