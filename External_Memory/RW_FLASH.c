/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename        : RW_FLASH.c
 * @brief               : read write routines for external flash
 *
 * @author               : Shyam Shrivastava
 *
 * @date Created         : February Monday , 2017  <Feb 20, 2017>
 * @date Last Modified   : February Monday , 2017  <Feb 20, 2017>
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal            :
 * @internal            :
 *
 *****************************************************************************/

/*============================================================================
 * Include Header Files
 *===========================================================================*/
#include "RW_FLASH.h"
#include <stddef.h>
#include "Global_ex.h"
#include "Ext_Data_flag.h"
#include "LPC177x_8x.h"
#include "lpc177x_8x_iap.h"
#include "Screen_global_ex.h"
#include "Ext_flash_high_level.h"
#include "Ext_flash_low_level.h"
#include "File_update.h"
#include "Log_report_data_calculate.h"
#include "LCDConf.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "EEPROM_high_level.h"
#include "RTOS_main.h"
#include "MVT.h"
#include "MVT_Totals.h"
#include "Main.h"
#include "Calibration.h"
#include <ctype.h>
#define	LOG_START_FEED			0XAA55
#define	LOG_STOP_FEED				0X55AA
#define SIZE_OF_BLOCK				4
#define NO_OF_SECTORS				4
#define BLOCKS_PER_SECTOR		32768	//0X8000
#define MAX_NO_OF_RECORDS		4000
#define MAX_SIZE_IN_BYTES		524288	//0X80000

/*
*	<LOG_START_FEED><LOG_SIZE_IDENTIFIER><LOG_STRUCT_DATA><LOG_SIZE_IDENTIFIER><LOG_STOP_FEED>
*/
int firstTime_WLTF	=0;
	U16*	FlashEraseU32_ptr;
char fpath[60];
char fpath1[60];
char file_day_idx_char[3];
char file_mon_idx_char[3];
char line1_char[200],timetocheck_char[12],datetocheck_char[18];

char *tok;
FILE *fin;
int ch,line_count_int,lineidx,found_hour_int,found_min_int,found_sec_int,found_dom_int,found_mon_int,found_year_int;
typedef struct{
								U16 Nos_of_records;
								U16 * FlashHeadWriteU32_ptr;								
								U16 * FlashTailWriteU32_ptr;
							}FLASH_LOG_WRITE;

typedef struct{
U16 StartFeed_U16;
U16 LogTypeStart_U16;
CLR_DATA_LOG_STRUCT	periodic_data;
U16 LogTypeStop_U16;
U16 StopFeed_U16;
}flash_clear_data_log;
typedef struct{
U16 StartFeed_U16;
U16 LogTypeStart_U16;
PERIODIC_LOG_STRUCT	periodic_data;
U16 LogTypeStop_U16;
U16 StopFeed_U16;
}flash_log;
typedef struct{
U16 StartFeed_U16;
U16 LogTypeStart_U16;
SET_ZERO_LOG_STRUCT	periodic_data;
U16 LogTypeStop_U16;
U16 StopFeed_U16;
}flash_set_zero_log;
typedef struct{
U16 StartFeed_U16;
U16 LogTypeStart_U16;
CAL_DATA_LOG_STRUCT	periodic_data;
U16 LogTypeStop_U16;
U16 StopFeed_U16;
}flash_calib_log;
typedef struct{
U16 StartFeed_U16;
U16 LogTypeStart_U16;
LENGTH_DATA_LOG_STRUCT	periodic_data;
U16 LogTypeStop_U16;
U16 StopFeed_U16;
}flash_length_cal_data_log;
__align(4) FLASH_LOG_WRITE	flash_log_data;
__align(4) flash_set_zero_log flash_set_zero_log_write,flash_set_zero_log_read;
__align(4) flash_log flash_log_write,flash_log_read;
__align(4) flash_clear_data_log flash_clear_data_log_write,flash_clear_data_log_read;
__align(4) flash_calib_log flash_calib_log_write,flash_calib_log_read;
__align(4) flash_length_cal_data_log flash_length_cal_data_log_write,flash_length_cal_data_log_read;

void write_log_to_flash_periodic_log(U16);
void write_log_to_flash_set_zero_log(U16 type_of_log);
void write_log_to_flash_clear_data_log(U16 type_of_log);
void write_log_to_flash_calib_log(U16 type_of_log);
void write_log_to_flash_length_cal_data(U16 type_of_log);
int  Get_No_Of_Rec_In_Flash(void);

void write_log_to_flash(U8 type_of_log){
	switch (type_of_log)
	{
		case periodic_log:
		case periodic_log_start:
		case periodic_log_stop:
		case periodic_log_stop_at_start:
		case total_clr_wt_log:
		case calibration_finished:
		case periodic_tst_wt_log:
		case periodic_dig_log:
		case periodic_mat_log:
		case periodic_zer_log:
		case periodic_len_log:
		case calibration_cancel:
		case daily_wt_clr_log:
			write_log_to_flash_periodic_log(type_of_log);
			break;
		case set_zero_log:
			write_log_to_flash_set_zero_log(type_of_log);
			break;
		case	clear_log:
			write_log_to_flash_clear_data_log(type_of_log);
			break;
		case calib_log:
		case digital_log:
		case mat_test_log:
		case tst_wt_log:
				write_log_to_flash_calib_log(type_of_log);
			break;
		case length_log:
			write_log_to_flash_length_cal_data(type_of_log);
		default:
			break;
		}
}
void write_log_to_flash_calib_log(U16 type_of_log){
int i,success;
	U16 * u16Var;
	if (!firstTime_WLTF){
		flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
		{
		if (
			(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_START_ADDR)
																						)||
			(flash_log_data.FlashHeadWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)
			){
			flash_log_data.FlashHeadWriteU32_ptr =  (U16*)SECTOR_127_START_ADDR;
				LPC_RTC->GPREG1 =(U32)SECTOR_127_START_ADDR;
				ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
				firstTime_WLTF =1;
		}
	}
	firstTime_WLTF =1;
}

	{
	flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_calib_log);
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_127_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_128_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_calib_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
		}
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_128_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_128_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_129_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_calib_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_129_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_129_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_130_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_calib_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_130_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_127_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_calib_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
		}
	
	}
	
//Prepare Structure.....
	{
	flash_calib_log_write.StartFeed_U16 = LOG_START_FEED;
	flash_calib_log_write.LogTypeStart_U16 = type_of_log;
	flash_calib_log_write.periodic_data = calib_data_log;
	flash_calib_log_write.LogTypeStop_U16 = type_of_log;	
	flash_calib_log_write.StopFeed_U16 = LOG_STOP_FEED;
		u16Var = (U16*)(&flash_calib_log_write);
}	
//Start Writing into Flash
for(i=0; i<(FLASH_SIZEOF(flash_clear_data_log_write)); i++)
				{
					{
						success = ExtFlash_writeWord((uint32_t)flash_log_data.FlashHeadWriteU32_ptr, *u16Var++);
					}
					
					flash_log_data.FlashHeadWriteU32_ptr++;
				}
					LPC_RTC->GPREG1 = (U32)flash_log_data.FlashHeadWriteU32_ptr;

}
void write_log_to_flash_length_cal_data(U16 type_of_log){
	int i,success;
	U16 * u16Var;
	if (!firstTime_WLTF){
		flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
		{
		if (
			(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_START_ADDR)
																						)||
			(flash_log_data.FlashHeadWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)
			){
			flash_log_data.FlashHeadWriteU32_ptr =  (U16*)SECTOR_127_START_ADDR;
				LPC_RTC->GPREG1 =(U32)SECTOR_127_START_ADDR;
				ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
				firstTime_WLTF =1;
		}
	}
	firstTime_WLTF =1;
}
flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
	{
	flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_length_cal_data_log);
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_127_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_128_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_length_cal_data_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
		}
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_128_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_128_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_129_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_length_cal_data_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_129_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_129_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_130_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_length_cal_data_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_130_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_127_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_length_cal_data_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
		}
	
	}
	
//Prepare Structure.....
	{
	flash_length_cal_data_log_write.StartFeed_U16 = LOG_START_FEED;
	flash_length_cal_data_log_write.LogTypeStart_U16 = type_of_log;
	flash_length_cal_data_log_write.periodic_data = length_cal_data_log;
	flash_length_cal_data_log_write.LogTypeStop_U16 = type_of_log;	
	flash_length_cal_data_log_write.StopFeed_U16 = LOG_STOP_FEED;
		u16Var = (U16*)(&flash_length_cal_data_log_write);
}	
//Start Writing into Flash
for(i=0; i<(FLASH_SIZEOF(flash_length_cal_data_log_write)); i++)
				{
					{
						success = ExtFlash_writeWord((uint32_t)flash_log_data.FlashHeadWriteU32_ptr, *u16Var++);
					}
					
					flash_log_data.FlashHeadWriteU32_ptr++;
				}
					LPC_RTC->GPREG1 = (U32)flash_log_data.FlashHeadWriteU32_ptr;
}


void write_log_to_flash_clear_data_log(U16 type_of_log){
	int i,success;
	U16 * u16Var;
	//if (!firstTime_WLTF){
		verify_lastRecord();
		flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
		{
		if (
			(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_START_ADDR)
																						)||
			(flash_log_data.FlashHeadWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)
			){
			flash_log_data.FlashHeadWriteU32_ptr =  (U16*)SECTOR_127_START_ADDR;
				LPC_RTC->GPREG1 =(U32)SECTOR_127_START_ADDR;
				ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
	//			firstTime_WLTF =1;
		}
	}
	firstTime_WLTF =1;
//}
flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
	{
	flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_clear_data_log);
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_127_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_128_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_clear_data_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
		}
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_128_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_128_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_129_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_clear_data_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_129_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_129_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_130_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_clear_data_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_130_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_127_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_clear_data_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
		}
	
	}
	
//Prepare Structure.....
	{
	flash_clear_data_log_write.StartFeed_U16 = LOG_START_FEED;
	flash_clear_data_log_write.LogTypeStart_U16 = type_of_log;
	flash_clear_data_log_write.periodic_data = clear_data_log;
	flash_clear_data_log_write.LogTypeStop_U16 = type_of_log;	
	flash_clear_data_log_write.StopFeed_U16 = LOG_STOP_FEED;
		u16Var = (U16*)(&flash_clear_data_log_write);
}	
//Start Writing into Flash
for(i=0; i<(FLASH_SIZEOF(flash_clear_data_log_write)); i++)
				{
					{
						success = ExtFlash_writeWord((uint32_t)flash_log_data.FlashHeadWriteU32_ptr, *u16Var++);
					}
					
					flash_log_data.FlashHeadWriteU32_ptr++;
				}
					LPC_RTC->GPREG1 = (U32)flash_log_data.FlashHeadWriteU32_ptr;
}

void write_log_to_flash_set_zero_log(U16 type_of_log){
	int i,success;
	U16 * u16Var;
	//if (!firstTime_WLTF){
		flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
		{
		if (
			(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_START_ADDR)
																						)||
			(flash_log_data.FlashHeadWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)
			){
			flash_log_data.FlashHeadWriteU32_ptr =  (U16*)SECTOR_127_START_ADDR;
				LPC_RTC->GPREG1 =(U32)SECTOR_127_START_ADDR;
				ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
				firstTime_WLTF =1;
		}
	}
	firstTime_WLTF =1;
//}
flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
	{
	flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_set_zero_log_write);
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_127_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_128_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_set_zero_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
		}
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_128_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_128_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_129_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_set_zero_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_129_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_129_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_130_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_set_zero_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_130_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_127_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_set_zero_log_write);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
		}
	
	}
	
//Prepare Structure.....
	{
	flash_set_zero_log_write.StartFeed_U16 = LOG_START_FEED;
	flash_set_zero_log_write.LogTypeStart_U16 = type_of_log;
	flash_set_zero_log_write.periodic_data = set_zero_data_log;
	flash_set_zero_log_write.LogTypeStop_U16 = type_of_log;	
	flash_set_zero_log_write.StopFeed_U16 = LOG_STOP_FEED;
		u16Var = (U16*)(&flash_set_zero_log_write);
}	
//Start Writing into Flash
for(i=0; i<(FLASH_SIZEOF(flash_set_zero_log_write)); i++)
				{
					{
						success = ExtFlash_writeWord((uint32_t)flash_log_data.FlashHeadWriteU32_ptr, *u16Var++);
					}
					
					flash_log_data.FlashHeadWriteU32_ptr++;
				}
					LPC_RTC->GPREG1 = (U32)flash_log_data.FlashHeadWriteU32_ptr;
}

void write_log_to_flash_periodic_log(U16 type_of_log){
	int i,success;
	U16 * u16Var;
//	if (!firstTime_WLTF){
		//in case of power on or, if the function is called for the first time,
		// check for current write pointer
		// a copy og the write pointer is being stored in RTC (U16*)LPC_RTC->GPREG1
		//update from above register, validate and then proccess
		//1. read write pointer
		flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
		//2. validate value of writepointer
		{
		if (
			(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_START_ADDR)
																						)||
			(flash_log_data.FlashHeadWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)
			){
			// do something address out of bound!~!!!!!!!!
			flash_log_data.FlashHeadWriteU32_ptr =  (U16*)SECTOR_127_START_ADDR;
				LPC_RTC->GPREG1 =(U32)SECTOR_127_START_ADDR;
				//do some more .... maybe later.
				//...
				//erase firstTime_WLTF sector, flash being written for the first time!!!!
				ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
				// update firstTime_WLTF , we have been here before..
				firstTime_WLTF =1;
		}
	}
	firstTime_WLTF =1;
//}
//flash_log_data.FlashHeadWriteU32_ptr = (U16*)LPC_RTC->GPREG1;
	//3. OK ! flash_log_data.FlashHeadWriteU32_ptr is up to date!
	//obtain size required and check boundary
	//get Sector number.
	{
	flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_log)-2;
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_127_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_127_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_128_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
		}
	if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_128_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_128_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_129_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_129_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_129_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_130_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
		}
		if(
		(flash_log_data.FlashTailWriteU32_ptr>(
																						(U16*)SECTOR_130_END_ADDR)
																						)&&
		(flash_log_data.FlashHeadWriteU32_ptr<(
																						(U16*)SECTOR_130_END_ADDR)
																						)
	
		){
			//check if data struct will fit in current sector;
			
			flash_log_data.FlashHeadWriteU32_ptr = (U16*)SECTOR_127_START_ADDR;
			flash_log_data.FlashTailWriteU32_ptr = flash_log_data.FlashHeadWriteU32_ptr+FLASH_SIZEOF(flash_log);	
			//erase & prepare the new sector to be written
			ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
		}
	
	}
	
//Prepare Structure.....
	{
	flash_log_write.StartFeed_U16 = LOG_START_FEED;
	flash_log_write.LogTypeStart_U16 = type_of_log;
	flash_log_write.periodic_data = periodic_data;
	flash_log_write.LogTypeStop_U16 = type_of_log;	
	flash_log_write.StopFeed_U16 = LOG_STOP_FEED;
		u16Var = (U16*)(&flash_log_write);
}
flash_log_data.Nos_of_records++;
//Start Writing into Flash
for(i=0; i<(FLASH_SIZEOF(flash_log_write)-2); i++)
				{
					{
						success = ExtFlash_writeWord((uint32_t)flash_log_data.FlashHeadWriteU32_ptr, *u16Var++);
					}
					
					flash_log_data.FlashHeadWriteU32_ptr++;
				}
					LPC_RTC->GPREG1 = (U32)flash_log_data.FlashHeadWriteU32_ptr;
}
U16 Log_restore_buf[120];
typedef struct {
	U16 log_type_start;
	U16 log_type_stop;
	U16 Log_Stop_Feed;
	
}check;
 check check_log;
typedef struct {
	U16 Periodic_hdr;
	U16	Clr_data_hdr;
	U16 Cal_data_hdr;
	U16 Set_zero_hdr;
	U16 Err_data_hdr;
	U16 Len_data_hdr;
}backup_ID;
backup_ID backup_HDR_ID;
void flash_log_sync_to_usb(int dom_to_sync,int mon_to_sync,int year_to_sync,int hour_to_sync,int min_to_sync,int sec_to_sync){
U16* readFlash_u16,*readFlash_u16_temp,file_count=0;
	char Display_Footer [100];
char *tok;
FILE *fin;
int ch,line_count_int,lineidx,found_hour_int,found_min_int,found_sec_int,found_dom_int,found_mon_int,found_year_int,no_of_rec=0;
	char disp[100];
	int j;
	readFlash_u16 = (U16*)SECTOR_127_START_ADDR;
	backup_HDR_ID.Cal_data_hdr = calib_data_log.Cal_data_hdr.Id;
	backup_HDR_ID.Clr_data_hdr = clear_data_log.Clr_data_hdr.Id ;
	backup_HDR_ID.Err_data_hdr = error_data_log.Err_data_hdr.Id;
	backup_HDR_ID.Len_data_hdr = length_cal_data_log.Len_data_hdr.Id;
	backup_HDR_ID.Periodic_hdr = periodic_data.Periodic_hdr.Id;
	backup_HDR_ID.Set_zero_hdr = set_zero_data_log.Set_zero_hdr.Id;
	readFlash_u16_temp = readFlash_u16;
// 	while(1){
// 	{
// 	while(((*readFlash_u16_temp)!=LOG_START_FEED )&&(readFlash_u16_temp< (U16*)SECTOR_130_END_ADDR)){
// 		readFlash_u16_temp++;
// 	}
// 	if(readFlash_u16_temp<(U16*)SECTOR_130_END_ADDR){
// 	}
// }
no_of_rec =Get_No_Of_Rec_In_Flash();
	while(1){
	{
	while(((*readFlash_u16)!=LOG_START_FEED )&&(readFlash_u16< (U16*)SECTOR_130_END_ADDR)){
		readFlash_u16++;
	}//either get a log start feed or reach the end of flash
	if((*readFlash_u16)==LOG_START_FEED ){//log_feed received
		//check for log type
		readFlash_u16_temp = readFlash_u16;
		//readFlash_u16++;
		check_log.log_type_start = *(readFlash_u16+1);
		switch(check_log.log_type_start){
			case periodic_log:
		case periodic_log_start:
		case periodic_log_stop:
		case periodic_log_stop_at_start:
		case total_clr_wt_log:
		case calibration_finished:
		case periodic_tst_wt_log:
		case periodic_dig_log:
		case periodic_mat_log:
		case periodic_zer_log:
		case periodic_len_log:
		case calibration_cancel:
		case daily_wt_clr_log:
			//it is a periodic log
				for(j=0; j<FLASH_SIZEOF(flash_log_read); j++){
				Log_restore_buf[j] = *readFlash_u16;
				readFlash_u16++;
			}	
			memcpy((U16 *)&flash_log_read, Log_restore_buf, sizeof(flash_log_read));
			//cross check if log_type_stop is == log_type_start
			if(flash_log_read.LogTypeStart_U16 == flash_log_read.LogTypeStop_U16){
				//cross check if LOG_STOP_FEED == 0x55AA
				if (flash_log_read.StopFeed_U16==LOG_STOP_FEED){
					//check if log is new
					if(
									(flash_log_read.periodic_data.Periodic_hdr.Date.Year > year_to_sync)||		//year is greater
									(																													//same year newer month
										(flash_log_read.periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Date.Mon > mon_to_sync)
									 )||
									(																													//same year same day new month
										(flash_log_read.periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
										(flash_log_read.periodic_data.Periodic_hdr.Date.Day > dom_to_sync)
									)||
									(																													//same year same day same month new hour
										(flash_log_read.periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
										(flash_log_read.periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Time.Hr> hour_to_sync)
									)||
									(																													//same year same day same month same hour new min
										(flash_log_read.periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
										(flash_log_read.periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Time.Min> min_to_sync)
									)
									||
									(																													//same year same month same day same hour same min new sec
										(flash_log_read.periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
										(flash_log_read.periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Time.Min== min_to_sync)&&
										(flash_log_read.periodic_data.Periodic_hdr.Time.Sec> sec_to_sync)
									)	
								){
				//all OK go ahead and write_log_to_flash log_data_populate 
					periodic_data = flash_log_read.periodic_data;
					sprintf(&disp[0],"          Synchronizing Record  %d of %d ",(file_count++),no_of_rec);
								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
								log_file_write(periodic_log);
							}
						}
				}
			else
				 readFlash_u16=readFlash_u16_temp+1;
			break;
		case set_zero_log:
				for(j=0; j<FLASH_SIZEOF(flash_set_zero_log_read); j++){
				Log_restore_buf[j] = *readFlash_u16;
				readFlash_u16++;
			}	
			memcpy((U16 *)&flash_set_zero_log_read, Log_restore_buf, sizeof(flash_set_zero_log_read));
			//cross check if log_type_stop is == log_type_start
			if(flash_set_zero_log_read.LogTypeStart_U16 == flash_set_zero_log_read.LogTypeStop_U16){
				//cross check if LOG_STOP_FEED == 0x55AA
				if (flash_set_zero_log_read.StopFeed_U16==LOG_STOP_FEED){
					//check if log is new
					if(
									(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Year > year_to_sync)||		//year is greater
									(																													//same year newer month
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Year == year_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Mon > mon_to_sync)
									 )||
									(																													//same year same day new month
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Year == year_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Mon == mon_to_sync)&&	
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Day > dom_to_sync)
									)||
									(																													//same year same day same month new hour
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Year == year_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Mon == mon_to_sync)&&	
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Day == dom_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Time.Hr> hour_to_sync)
									)||
									(																													//same year same day same month same hour new min
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Year == year_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Mon == mon_to_sync)&&	
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Day == dom_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Time.Hr == hour_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Time.Min> min_to_sync)
									)
									||
									(																													//same year same month same day same hour same min new sec
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Year == year_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Mon == mon_to_sync)&&	
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Date.Day == dom_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Time.Hr == hour_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Time.Min== min_to_sync)&&
										(flash_set_zero_log_read.periodic_data.Set_zero_hdr.Time.Sec> sec_to_sync)
									)	
								){
				//all OK go ahead and write_log_to_flash log_data_populate 
					set_zero_data_log = flash_set_zero_log_read.periodic_data;
					sprintf(&disp[0],"          Synchronizing Record  %d of %d ",(file_count++),no_of_rec);
								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
								log_file_write(set_zero_log);
					}
				}
				}
			else
				 readFlash_u16=readFlash_u16_temp+1;
			//it is a zero log;
			break;
		case	clear_log:
			for(j=0; j<FLASH_SIZEOF(flash_clear_data_log_read); j++){
				Log_restore_buf[j] = *readFlash_u16;
				readFlash_u16++;
			}	
			memcpy((U16 *)&flash_clear_data_log_read, Log_restore_buf, sizeof(flash_clear_data_log_read));
			//cross check if log_type_stop is == log_type_start
			if(flash_clear_data_log_read.LogTypeStart_U16 == flash_clear_data_log_read.LogTypeStop_U16){
				//cross check if LOG_STOP_FEED == 0x55AA
				if (flash_clear_data_log_read.StopFeed_U16==LOG_STOP_FEED){
										if(
									(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Year > year_to_sync)||		//year is greater
									(																													//same year newer month
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Year == year_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Mon > mon_to_sync)
									 )||
									(																													//same year same day new month
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Year == year_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Day > dom_to_sync)
									)||
									(																													//same year same day same month new hour
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Year == year_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Day == dom_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Time.Hr> hour_to_sync)
									)||
									(																													//same year same day same month same hour new min
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Year == year_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Day == dom_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Time.Hr == hour_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Time.Min> min_to_sync)
									)
									||
									(																													//same year same month same day same hour same min new sec
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Year == year_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Date.Day == dom_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Time.Hr == hour_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Time.Min== min_to_sync)&&
										(flash_clear_data_log_read.periodic_data.Clr_data_hdr.Time.Sec> sec_to_sync)
									)	
								){
				//all OK go ahead and write_log_to_flash log_data_populate 
					clear_data_log = flash_clear_data_log_read.periodic_data;
					sprintf(&disp[0],"          Synchronizing Record  %d of %d",(file_count++),no_of_rec);
								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
								log_file_write(clear_log);
					}
				}
				}
			//it is a clear log;;
			break;
		case calib_log:
		case digital_log:
		case mat_test_log:
		case tst_wt_log:
				//it is a calib log;;
		for(j=0; j<FLASH_SIZEOF(flash_calib_log_read); j++){
				Log_restore_buf[j] = *readFlash_u16;
				readFlash_u16++;
			}	
			memcpy((U16 *)&flash_calib_log_read, Log_restore_buf, sizeof(flash_calib_log_read));
			//cross check if log_type_stop is == log_type_start
			if(flash_calib_log_read.LogTypeStart_U16 == flash_calib_log_read.LogTypeStop_U16){
				//cross check if LOG_STOP_FEED == 0x55AA
				if (flash_calib_log_read.StopFeed_U16==LOG_STOP_FEED){
					if(
									(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Year > year_to_sync)||		//year is greater
									(																													//same year newer month
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Year == year_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Mon > mon_to_sync)
									 )||
									(																													//same year same day new month
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Year == year_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Day > dom_to_sync)
									)||
									(																													//same year same day same month new hour
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Year == year_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Day == dom_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Time.Hr> hour_to_sync)
									)||
									(																													//same year same day same month same hour new min
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Year == year_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Day == dom_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Time.Hr == hour_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Time.Min> min_to_sync)
									)
									||
									(																													//same year same month same day same hour same min new sec
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Year == year_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Date.Day == dom_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Time.Hr == hour_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Time.Min== min_to_sync)&&
										(flash_calib_log_read.periodic_data.Cal_data_hdr.Time.Sec> sec_to_sync)
									)	
								){
				//all OK go ahead and write_log_to_flash log_data_populate 
					calib_data_log = flash_calib_log_read.periodic_data;
					sprintf(&disp[0],"          Synchronizing Record  %d of %d",(file_count++),no_of_rec);
								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
								log_file_write(calib_log);
					}
				}
				}
			break;
		case length_log:
			//it is a length log;;
		for(j=0; j<FLASH_SIZEOF(flash_calib_log_read); j++){
				Log_restore_buf[j] = *readFlash_u16;
				readFlash_u16++;
			}	
			memcpy((U16 *)&flash_length_cal_data_log_read, Log_restore_buf, sizeof(flash_length_cal_data_log_read));
			//cross check if log_type_stop is == log_type_start
			if(flash_length_cal_data_log_read.LogTypeStart_U16 == flash_length_cal_data_log_read.LogTypeStop_U16){
				//cross check if LOG_STOP_FEED == 0x55AA
				if (flash_length_cal_data_log_read.StopFeed_U16==LOG_STOP_FEED){
					if(
									(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Year > year_to_sync)||		//year is greater
									(																													//same year newer month
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Year == year_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Mon > mon_to_sync)
									 )||
									(																													//same year same day new month
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Year == year_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Day > dom_to_sync)
									)||
									(																													//same year same day same month new hour
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Year == year_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Day == dom_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Time.Hr> hour_to_sync)
									)||
									(																													//same year same day same month same hour new min
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Year == year_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Day == dom_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Time.Hr == hour_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Time.Min> min_to_sync)
									)
									||
									(																													//same year same month same day same hour same min new sec
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Year == year_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Mon == mon_to_sync)&&	
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Date.Day == dom_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Time.Hr == hour_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Time.Min== min_to_sync)&&
										(flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Time.Sec> sec_to_sync)
									)	
								){
				//all OK go ahead and write_log_to_flash log_data_populate 
					length_cal_data_log = flash_length_cal_data_log_read.periodic_data;
					sprintf(&disp[0],"          Synchronizing Record  %d of%d",(file_count++),no_of_rec);
								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
								log_file_write(calib_log);
					}
				}
			}
		default:
			break;
		}
	}
	else{
	//	sprintf(&disp[0],"          Synchronizing Record  %d",(file_count++));
								GUI_DispStringAt("End of   flash!!!", WINDOW_RECT_POS_X0, 230);log_file_write(length_log);
			calib_data_log.Cal_data_hdr.Id	 = backup_HDR_ID.Cal_data_hdr;
	clear_data_log.Clr_data_hdr.Id = backup_HDR_ID.Clr_data_hdr ;
	error_data_log.Err_data_hdr.Id  = backup_HDR_ID.Err_data_hdr;
	length_cal_data_log.Len_data_hdr.Id  = backup_HDR_ID.Len_data_hdr;
	periodic_data.Periodic_hdr.Id  = backup_HDR_ID.Periodic_hdr;
	set_zero_data_log.Set_zero_hdr.Id  = backup_HDR_ID.Set_zero_hdr;
					return;
			}
		}
	}
	calib_data_log.Cal_data_hdr.Id	 = backup_HDR_ID.Cal_data_hdr;
	clear_data_log.Clr_data_hdr.Id = backup_HDR_ID.Clr_data_hdr ;
	error_data_log.Err_data_hdr.Id  = backup_HDR_ID.Err_data_hdr;
	length_cal_data_log.Len_data_hdr.Id  = backup_HDR_ID.Len_data_hdr;
	periodic_data.Periodic_hdr.Id  = backup_HDR_ID.Periodic_hdr;
	set_zero_data_log.Set_zero_hdr.Id  = backup_HDR_ID.Set_zero_hdr;
}

void sync_flash_log_usb(void){
	int file_day,file_mon,file_year;
	int file_day_idx,file_mon_idx,file_year_idx,file_found=0;


// check for the last record.
// get directory listings
	FINFO info;	
  info.fileID = 0;                             /* info.fileID must be set to 0 */
	/*
	check for the latest files as per current RTC values
	*/
	file_year = LPC_RTC->YEAR;									//get current year from RTC
	file_mon = LPC_RTC->MONTH;									//get current month from RTC 
	file_day= LPC_RTC->DOM;									//get current hour from RTC
	for (file_year_idx = file_year; (file_year_idx>(DATE_YR_MIN-1));file_year_idx--){ // start with latest year and search until MVT limit of year
		sprintf(fpath,"U0:\\%d\\*.*",file_year_idx );	
		if (ffind (fpath, &info) == 0){
			file_found=1;
			break;
		}
	}
	if (file_found ==0) file_year_idx =DATE_YR_MIN;
	file_found =0;
	for (file_mon_idx = file_mon; (file_mon_idx>(DATE_MON_MIN-1));file_mon_idx--){
		if (file_mon_idx<10){
			file_mon_idx_char[0] = '0';
			file_mon_idx_char[1] = 0x30 + file_mon_idx;
			file_mon_idx_char[2]=0;
			sprintf(fpath1,"U0:\\%d\\%s",file_year_idx,file_mon_idx_char );
		}
		else sprintf(fpath1,"U0:\\%d\\%d",file_year_idx,file_mon_idx );	
			sprintf(fpath,"%s\\*.*",fpath1 );	
		if (ffind (fpath, &info) == 0){
			file_found=1;
			break;
		}
	}
	if (file_found ==0) file_mon_idx=DATE_MON_MIN;
	file_found =0;
	for (file_day_idx = file_day; (file_day_idx>(DATE_DATE_MIN-1));file_day_idx--){
		if (file_day_idx<10){
			file_day_idx_char[0] = '0';
			file_day_idx_char[1] = 0x30 + file_day_idx;
			file_day_idx_char[2]=0;
			if (file_mon_idx<10){
				sprintf(fpath,"%s\\%d-%s-%s_periodic_log.txt",fpath1,file_year_idx,file_mon_idx_char,file_day_idx_char );
			}
			else sprintf(fpath,"%s\\%d-%d-%s_periodic_log.txt",fpath1,file_year_idx,file_mon_idx,file_day_idx_char );
		}
		else	{
			if (file_mon_idx<10){
				sprintf(fpath,"%s\\%d-%s-%d_periodic_log.txt",fpath1,file_year_idx,file_mon_idx_char,file_day_idx );
			}
			else sprintf(fpath,"%s\\%d-%d-%d_periodic_log.txt",fpath1,file_year_idx,file_mon_idx,file_day_idx );	
		
		}
		if (ffind (fpath, &info) == 0){
			file_found=1;									//file found now read_files_on_usb line by line data
			line_count_int =0;
			fin = fopen (fpath,"r");
			if (fin!=NULL){
			lineidx =0;
			while ((ch = fgetc (fin)) != EOF)  {
				line1_char[lineidx] = ch;
				line1_char[lineidx+1] = '\0';
				lineidx++;
				if (ch=='\n') {
					line_count_int++;
					lineidx =0;
				}
			}
		
			
			// DAT, 146, 2016/12/8, 14:46:00,  354.599287, Tons, 144.770874, Tons/Hour, 38.212299, 45.000000, 45.690681, Feet/Min,  354.599287, Tons,
			// get time details now.
			tok = strtok (line1_char, ",");//DAT
			if(tok!=NULL)
			tok = strtok (NULL, ",");//146
			if(tok!=NULL)
			tok = strtok (NULL, ",");//2016/12/8	date!!!!!! goto it
			strcpy(datetocheck_char,tok);
			if(tok!=NULL)
			tok = strtok (NULL, ",");//14:46:00		time!!!!!! got it.
			strcpy(timetocheck_char,tok);
			while (tok!=NULL)
			tok = strtok (NULL, ",");
			
			
			tok = strtok (datetocheck_char, "/");//2016/12/8
			found_year_int  = atoi(tok);
			tok = strtok (NULL, "/");//2016/12/8
			found_mon_int = atoi(tok);
			tok = strtok (NULL, "/");//2016/12/8
			found_dom_int = atoi(tok);
			
			
			tok = strtok (timetocheck_char, ":");//14:46:00
			found_hour_int = atoi(tok);
			tok = strtok (NULL, ":");//14:46:00
			found_min_int = atoi(tok);
			tok = strtok (NULL, ":");//14:46:00
			found_sec_int = atoi(tok);
			fclose (fin);
			flash_log_sync_to_usb(found_dom_int,found_mon_int,found_year_int,found_hour_int,found_min_int,found_sec_int);
				
		}
			break;
		}
		else{
			flash_log_sync_to_usb(DATE_DATE_MIN,DATE_MON_MIN,DATE_YR_MIN,TIME_HR_MIN,TIME_MINT_MIN,TIME_SEC_MIN);
break;		
}
	}
}
void verify_lastRecord(void){
	U16* readAddress;
	U16 record_Id,log_type,j;
readAddress  = ((U16*)(LPC_RTC->GPREG1)-2) ;// has the new address to write to.
	LPC_RTC->GPREG2;// has the latest record ID;
	//check if stop feed valid
	if (*(readAddress-1)== LOG_STOP_FEED){
		log_type =  * (readAddress-2);
		switch(log_type){
			case periodic_log:
		case periodic_log_start:
		case periodic_log_stop:
		case periodic_log_stop_at_start:
		case total_clr_wt_log:
		case calibration_finished:
		case periodic_tst_wt_log:
		case periodic_dig_log:
		case periodic_mat_log:
		case periodic_zer_log:
		case periodic_len_log:
		case calibration_cancel:
		case daily_wt_clr_log:{
			readAddress = readAddress- (FLASH_SIZEOF(flash_log_read))+2;
			for(j=0; j<FLASH_SIZEOF(flash_log_write); j++){
		Log_restore_buf[j] = *readAddress;
		readAddress++;
	}	
	memcpy((U16 *)&flash_log_read, Log_restore_buf, sizeof(flash_log_read));
	if (flash_log_read.periodic_data.Periodic_hdr.Id ==	LPC_RTC->GPREG2){
		//all is well
	}
	else{
		LPC_RTC->GPREG2 =0;
	}
			break;
}
		case set_zero_log:{
		readAddress = readAddress- (FLASH_SIZEOF(set_zero_log))+2;
			for(j=0; j<FLASH_SIZEOF(set_zero_log); j++){
				Log_restore_buf[j] = *readAddress;
				readAddress++;
			}	
			memcpy((U16 *)&flash_set_zero_log_read, Log_restore_buf, sizeof(flash_set_zero_log_read));
			
			if (flash_set_zero_log_read.periodic_data.Set_zero_hdr.Id  ==	(U16)LPC_RTC->GPREG2){
				//all is well
			}
			else{
				LPC_RTC->GPREG2 =0;
			}
			break;
		}
		case clear_log:{
		readAddress = readAddress- (FLASH_SIZEOF(flash_clear_data_log_read))+2;
			for(j=0; j<FLASH_SIZEOF(flash_clear_data_log_read); j++){
				Log_restore_buf[j] = *readAddress;
				readAddress++;
			}	
			memcpy((U16 *)&flash_clear_data_log_read, Log_restore_buf, sizeof(flash_clear_data_log_read));
			if (flash_clear_data_log_read.periodic_data.Clr_data_hdr.Id  ==	(U16)LPC_RTC->GPREG2){
				//all is well
			}
			else{
				LPC_RTC->GPREG2 =0;
			}
			break;
		}
		case calib_log:
		case digital_log:
		case mat_test_log:
		case tst_wt_log:{
			readAddress = readAddress- (FLASH_SIZEOF(flash_calib_log_read))+2;
			for(j=0; j<FLASH_SIZEOF(flash_calib_log_read); j++){
				Log_restore_buf[j] = *readAddress;
				readAddress++;
			}	
			memcpy((U16 *)&flash_calib_log_read, Log_restore_buf, sizeof(flash_calib_log_read));
			if (flash_calib_log_read.periodic_data.Cal_data_hdr.Id  ==	(U16)LPC_RTC->GPREG2){
				//all is well
			}
			else{
				LPC_RTC->GPREG2 =0;
			}
			break;
		}
		case length_log:{
			readAddress = readAddress- (FLASH_SIZEOF(flash_length_cal_data_log_read))+2;
			for(j=0; j<FLASH_SIZEOF(flash_length_cal_data_log_read); j++){
				Log_restore_buf[j] = *readAddress;
				readAddress++;
			}	
			memcpy((U16 *)&flash_length_cal_data_log_read, Log_restore_buf, sizeof(flash_length_cal_data_log_read));
			if (flash_length_cal_data_log_read.periodic_data.Len_data_hdr.Id  ==	(U16)LPC_RTC->GPREG2){
				//all is well
			}
			else{
				LPC_RTC->GPREG2 =0;
			}
				break;			
			}
		
		default:
			//got nothing :-??
			break;
	}
	}
	//read 
	// traverse backward 
	//check if record ID of last record matches LPC_RTC->GPREG2
	
}

int  Get_No_Of_Rec_In_Flash(void){
//start reading flash from sector 127
	U16* readFlash_u16;
	U32 No_Of_Rec =0;
	int j;
	readFlash_u16 = (U16*)SECTOR_127_START_ADDR;
	while(1){
	{
	while(((*readFlash_u16)!=LOG_START_FEED )&&(readFlash_u16< (U16*)SECTOR_130_END_ADDR)){
			readFlash_u16++;
		}
		readFlash_u16++;
	while(((*readFlash_u16)!=LOG_STOP_FEED )&&(readFlash_u16< (U16*)SECTOR_130_END_ADDR)){
			readFlash_u16++;
		}
		No_Of_Rec++;
if (readFlash_u16> (U16*)SECTOR_130_END_ADDR)
	return No_Of_Rec;
	}
}

}


int  findRecord(U16 record_ID){
//start reading flash from sector 127
	U16* readFlash_u16;
	int j;
	readFlash_u16 = (U16*)SECTOR_127_START_ADDR;
	while(1){
	{
	while(((*readFlash_u16)!=LOG_START_FEED )&&(readFlash_u16< (U16*)SECTOR_130_END_ADDR)){
		readFlash_u16++;
	}
	if((*readFlash_u16)==LOG_START_FEED ){
	//	readFlash_u16++;
		for(j=0; j<FLASH_SIZEOF(flash_log_write); j++)
	{
		Log_restore_buf[j] = *readFlash_u16;
		readFlash_u16++;
	}	
	memcpy((U16 *)&flash_log_read, Log_restore_buf, sizeof(flash_log_read));
	}
	else return -1;		//record ID not found!
	}
}
}
