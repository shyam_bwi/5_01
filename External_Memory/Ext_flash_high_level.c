/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Run_mode_screen.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012  
* @date Last Modified  : November, 2012  
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            : 
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stddef.h>
#include "Global_ex.h"
#include "Ext_Data_flag.h"
#include "LPC177x_8x.h"
#include "lpc177x_8x_iap.h"
#include "Screen_global_ex.h"
#include "Ext_flash_high_level.h"
#include "Ext_flash_low_level.h"
#include "File_update.h"
#include "Log_report_data_calculate.h"
#include "LCDConf.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "EEPROM_high_level.h"
#include "RTOS_main.h"
#include "MVT.h"
#include "MVT_Totals.h"
#include "Main.h"
#include "Calibration.h"
#include <ctype.h>
#ifdef EXT_FLASH
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
U8 g_restore_prog = 0;

/* unsigned integer variables section */
//PVK - 11 Jan 2015
U16	gTotals_Calculated_CRC = 0;
U16 Record_no;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
	FLASH_LOG_STRUCTURE Flash_log_data;
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */
const char GUI_PARAM_fixDATAstring[] __attribute__((at(FLASH_BASE_ADDRESS))) =
	{
		0xFF, 0xFF,0x00, 0x00
	};
// Below constant array is stored in last sector of External memory to display firmware version Number in HEX File 	
const char GUI_PARAM_firmVerstring[] __attribute__((at(FLASH_BASE_ADDRESS + FLASH_LAST_SEC_ADDRESS))) = 
{
		0x00, 0x00,FIRMWARE_VER_MAJOR, FIRMWARE_VER
};

static const unsigned char crc_table[512] =
{
	0x00, 0x00, 0xc0, 0xc1, 0xc1, 0x81, 0x01, 0x40, 0xc3, 0x01, 0x03, 0xc0, 0x02, 0x80, 0xc2, 0x41,
	0xc6, 0x01, 0x06, 0xc0, 0x07, 0x80, 0xc7, 0x41, 0x05, 0x00, 0xc5, 0xc1, 0xc4, 0x81, 0x04, 0x40,
	0xcc, 0x01, 0x0c, 0xc0, 0x0d, 0x80, 0xcd, 0x41, 0x0f, 0x00, 0xcf, 0xc1, 0xce, 0x81, 0x0e, 0x40,
	0x0a, 0x00, 0xca, 0xc1, 0xcb, 0x81, 0x0b, 0x40, 0xc9, 0x01, 0x09, 0xc0, 0x08, 0x80, 0xc8, 0x41,
	0xd8, 0x01, 0x18, 0xc0, 0x19, 0x80, 0xd9, 0x41, 0x1b, 0x00, 0xdb, 0xc1, 0xda, 0x81, 0x1a, 0x40,
	0x1e, 0x00, 0xde, 0xc1, 0xdf, 0x81, 0x1f, 0x40, 0xdd, 0x01, 0x1d, 0xc0, 0x1c, 0x80, 0xdc, 0x41,
	0x14, 0x00, 0xd4, 0xc1, 0xd5, 0x81, 0x15, 0x40, 0xd7, 0x01, 0x17, 0xc0, 0x16, 0x80, 0xd6, 0x41,
	0xd2, 0x01, 0x12, 0xc0, 0x13, 0x80, 0xd3, 0x41, 0x11, 0x00, 0xd1, 0xc1, 0xd0, 0x81, 0x10, 0x40,
	0xf0, 0x01, 0x30, 0xc0, 0x31, 0x80, 0xf1, 0x41, 0x33, 0x00, 0xf3, 0xc1, 0xf2, 0x81, 0x32, 0x40,
	0x36, 0x00, 0xf6, 0xc1, 0xf7, 0x81, 0x37, 0x40, 0xf5, 0x01, 0x35, 0xc0, 0x34, 0x80, 0xf4, 0x41,
	0x3c, 0x00, 0xfc, 0xc1, 0xfd, 0x81, 0x3d, 0x40, 0xff, 0x01, 0x3f, 0xc0, 0x3e, 0x80, 0xfe, 0x41,
	0xfa, 0x01, 0x3a, 0xc0, 0x3b, 0x80, 0xfb, 0x41, 0x39, 0x00, 0xf9, 0xc1, 0xf8, 0x81, 0x38, 0x40,
	0x28, 0x00, 0xe8, 0xc1, 0xe9, 0x81, 0x29, 0x40, 0xeb, 0x01, 0x2b, 0xc0, 0x2a, 0x80, 0xea, 0x41,
	0xee, 0x01, 0x2e, 0xc0, 0x2f, 0x80, 0xef, 0x41, 0x2d, 0x00, 0xed, 0xc1, 0xec, 0x81, 0x2c, 0x40,
	0xe4, 0x01, 0x24, 0xc0, 0x25, 0x80, 0xe5, 0x41, 0x27, 0x00, 0xe7, 0xc1, 0xe6, 0x81, 0x26, 0x40,
	0x22, 0x00, 0xe2, 0xc1, 0xe3, 0x81, 0x23, 0x40, 0xe1, 0x01, 0x21, 0xc0, 0x20, 0x80, 0xe0, 0x41,
	0xa0, 0x01, 0x60, 0xc0, 0x61, 0x80, 0xa1, 0x41, 0x63, 0x00, 0xa3, 0xc1, 0xa2, 0x81, 0x62, 0x40,
	0x66, 0x00, 0xa6, 0xc1, 0xa7, 0x81, 0x67, 0x40, 0xa5, 0x01, 0x65, 0xc0, 0x64, 0x80, 0xa4, 0x41,
	0x6c, 0x00, 0xac, 0xc1, 0xad, 0x81, 0x6d, 0x40, 0xaf, 0x01, 0x6f, 0xc0, 0x6e, 0x80, 0xae, 0x41,
	0xaa, 0x01, 0x6a, 0xc0, 0x6b, 0x80, 0xab, 0x41, 0x69, 0x00, 0xa9, 0xc1, 0xa8, 0x81, 0x68, 0x40,
	0x78, 0x00, 0xb8, 0xc1, 0xb9, 0x81, 0x79, 0x40, 0xbb, 0x01, 0x7b, 0xc0, 0x7a, 0x80, 0xba, 0x41,
	0xbe, 0x01, 0x7e, 0xc0, 0x7f, 0x80, 0xbf, 0x41, 0x7d, 0x00, 0xbd, 0xc1, 0xbc, 0x81, 0x7c, 0x40,
	0xb4, 0x01, 0x74, 0xc0, 0x75, 0x80, 0xb5, 0x41, 0x77, 0x00, 0xb7, 0xc1, 0xb6, 0x81, 0x76, 0x40,
	0x72, 0x00, 0xb2, 0xc1, 0xb3, 0x81, 0x73, 0x40, 0xb1, 0x01, 0x71, 0xc0, 0x70, 0x80, 0xb0, 0x41,
	0x50, 0x00, 0x90, 0xc1, 0x91, 0x81, 0x51, 0x40, 0x93, 0x01, 0x53, 0xc0, 0x52, 0x80, 0x92, 0x41,
	0x96, 0x01, 0x56, 0xc0, 0x57, 0x80, 0x97, 0x41, 0x55, 0x00, 0x95, 0xc1, 0x94, 0x81, 0x54, 0x40,
	0x9c, 0x01, 0x5c, 0xc0, 0x5d, 0x80, 0x9d, 0x41, 0x5f, 0x00, 0x9f, 0xc1, 0x9e, 0x81, 0x5e, 0x40,
	0x5a, 0x00, 0x9a, 0xc1, 0x9b, 0x81, 0x5b, 0x40, 0x99, 0x01, 0x59, 0xc0, 0x58, 0x80, 0x98, 0x41,
	0x88, 0x01, 0x48, 0xc0, 0x49, 0x80, 0x89, 0x41, 0x4b, 0x00, 0x8b, 0xc1, 0x8a, 0x81, 0x4a, 0x40,
	0x4e, 0x00, 0x8e, 0xc1, 0x8f, 0x81, 0x4f, 0x40, 0x8d, 0x01, 0x4d, 0xc0, 0x4c, 0x80, 0x8c, 0x41,
	0x44, 0x00, 0x84, 0xc1, 0x85, 0x81, 0x45, 0x40, 0x87, 0x01, 0x47, 0xc0, 0x46, 0x80, 0x86, 0x41,
	0x82, 0x01, 0x42, 0xc0, 0x43, 0x80, 0x83, 0x41, 0x41, 0x00, 0x81, 0xc1, 0x80, 0x81, 0x40, 0x40
};

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
	U16 Log_restore_buffer[70];
  U16 WRstatus;
	U32 flash_para_addr;
	U16 flash_para_record_no;
	U16 record_no = 0;
	static U16 g_Clr_wt_CRC = 0xFFFF;
U16* readAddress_u16;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void EEP_FL_create_backup (void);
void CalcCRC(unsigned char* pTargetData, unsigned char* pTargetCrc, unsigned int msg_len);
U8 EEP_FL_chk_FL_data_validity (void);
char Reload_totals_from_flash_with_CRC_Check(U32 addr);

void load_good_Clr_wt_data(void);
unsigned int Flash_ClrWt_Data_Validity_Org(void);
unsigned int Flash_ClrWt_Data_Validity_Bkp(void);
void Flash_ClrWt_Write_RAM_to_Org(void);
void Flash_ClrWt_Write_RAM_to_Bkp(void);
void Flash_ClrWt_Org_to_RAM(void);
void Flash_ClrWt_Bkp_to_RAM(void);
void Clear_weight_load_Default(void);
void Calculate_Clr_Wt_Screen_Avg_Rate(void);
void	flash_log_sync_to_usb(int found_dom_int,int found_mon_int,int found_year_int,int found_hour_int,int found_min_int,int found_sec_int);

/*============================================================================
* Function Implementation Section
*===========================================================================*/
//#if 0
/*****************************************************************************
* @note       Function name: void flash_struct_backup(U8 first_time_write)
* @returns    returns		   : None
* @param      arg1			   : Flag to indicate if the flash is being written for
*														 the first time
* @author			             : Anagha Basole
* @date       date created : July 12, 2012
* @brief      Description	 : Writes the backup of the configuration to the flash
														 to the 1st 8K sector. Flag is checked, to verify if
														 the flash is being written for the first time and changed
														 to 0x5A5A
* @note       Notes		     : None
*****************************************************************************/
void flash_struct_backup(U8 first_time_write)
{}
// {
// 	U16 * u16Var;
// 	U8 success;
// 	U16 * read_flash;
// 	U16 i;
// 	U16 u16_CRC = 0xFFFF;

// 	if (ExtFlash_eraseBlock(GUI_PARAM_SECTOR_126_START_ADDR))
// 	{
// // 		if (first_time_write == 1)
// // 		{
// // 			read_flash = (U16 *)DEF_PARAM_WRITE_FLAG_SECTOR_126;
// // 			ExtFlash_writeWord((uint32_t)read_flash, BACKUP_PRESENT_FLAG);
// // 		}
// // 		
// // 		/*Scale setup structure*/
// // 		read_flash = SCALE_SETUP_ADDR;
// // 		u16Var = (U16*)&Scale_setup_var;
// // 		for(i=0; i<FLASH_SIZEOF(Scale_setup_var); i++)
// // 		{					
// // 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// // 			read_flash++;
// // 			if(success == __FALSE)
// // 			{
// // 				break;
// // 			}
// // 		}
// // 		
// // 		/*CALIBRATION structure*/
// // 		u16Var = (U16*)&Calibration_var;
// // 		for(i=0; i<FLASH_SIZEOF(Calibration_var); i++)
// // 		{					
// // 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// // 			read_flash++;
// // 			if(success == __FALSE)
// // 			{
// // 				break;
// // 			}
// // 		}
// // 		/*SETUP DEVICE structure*/
// // 		u16Var = (U16*)&Setup_device_var;
// // 		for(i=0; i<FLASH_SIZEOF(Setup_device_var); i++)
// // 		{					
// // 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// // 			read_flash++;
// // 			if(success == __FALSE)
// // 			{
// // 				break;
// // 			}
// // 		}
// 		
// 		/*REPORTS structure*/
// 		
// 		//PVK - 10- Feb -2016 
// 	  //Verify All Historical Totals
// 		Verify_Historical_Totals();
// 		
// 		read_flash = (U16*)RPRTS_DIAG_ADDR;
// 		u16Var = (U16*)&Rprts_diag_var;

// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}

// 		//Calculate CRC over the Last 8 Cleared Weight values and units
// 		CalcCRC((unsigned char*)&Rprts_diag_var.LastClearedTotal[0], (unsigned char*)&u16_CRC, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
// 		//Set the pointer to point to the CRC Location on Flash
// 		read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_CRC_ADDR;
// 		//Write the CRC in the Flash
// 		ExtFlash_writeWord((uint32_t)read_flash, u16_CRC);

// 	}
// 	
// 	//Create the backup copy
// 	if (ExtFlash_eraseBlock(RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_ADDR))
// 	{
// 		//Save the Back up of the same at a different location 0x8060 0000
// 		//Set the pointer to point to the Clear Weight Backup Data Location on Flash
// 		read_flash = (U16*)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_ADDR;
// 		//Load the variable with starting address of the CLear weight data in Rprts_diag_var structure
// 		u16Var = (U16*)&Rprts_diag_var.LastClearedTotal[0];
// 		//Write the Backup Data in Flash
// 		for(i=0; i<(SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT/2); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}
// 		
// 		//Set the pointer to point to the location of CRC of the Backup data on Flash
// 		//CRC is already Calculated above
// 		read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_CRC_ADDR;
// 		//Write the CRC in the Flash
// 		ExtFlash_writeWord((uint32_t)read_flash, u16_CRC);
// 	}
// 	//PVK - 10- Feb -2016 
// 	Historical_Totals_set_to_default_flag = 0;
// 	
// 			/*ADMIN structure*/
// // 		u16Var = (U16*)&Admin_var;
// // 		for(i=0; i<FLASH_SIZEOF(Admin_var); i++)
// // 		{					
// // 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// // 			read_flash++;
// // 			if(success == __FALSE)
// // 			{
// // 				break;
// // 			}
// // 		}
// 	return;
// }

// /*****************************************************************************
// * @note       Function name: void flash_struct_reload(void)
// * @returns    returns		   : None
// * @param      arg1			   : None
// * @author			             : Anagha Basole
// * @date       date created : July 12, 2012
// * @brief      Description	 : Reloads the data from the flash to the configuration structures
// * @note       Notes		     : None
// *****************************************************************************/
// void flash_struct_reload(void)
// {	
// 	U16 * read_flash;	
// 	
// // 	read_flash = SCALE_SETUP_ADDR;
// // 	memcpy((U16 *)&Scale_setup_var, read_flash, sizeof(Scale_setup_var));
// // 	
// // 	read_flash = CALIBRATION_ADDR;
// // 	memcpy((U16 *)&Calibration_var, read_flash, sizeof(Calibration_var));
// // 	
// // 	read_flash = SETUP_DEVICE_ADDR;
// // 	memcpy((U16 *)&Setup_device_var, read_flash, sizeof(Setup_device_var));
// 	
// //	read_flash = RPRTS_DIAG_ADDR;	//010416
// //	memcpy((U16 *)&Rprts_diag_var, read_flash, sizeof(Rprts_diag_var));
// 	
// 	//read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_ADDR;	//010416
// 	//memcpy((U16 *)&Rprts_diag_var.LastClearedTotal[0], read_flash, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
// 	load_good_Clr_wt_data();
// // 	read_flash = ADMIN_ADDR;
// // 	memcpy((U16 *)&Admin_var, read_flash, sizeof(Admin_var));
// 	return;
// }
//#endif
/*****************************************************************************
* @note       Function name: U8 sector_erase(U16 * read_flash)
* @returns    returns		   : status of the erase
* @param      arg1			   : The end address of the data to be written to the flash
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Checks if the data to be written exceeds the current sector.
*														 If the current sector does exceed then the next sector is
*														 erased. When the data exceeds the sector 130 then 
*														 the first sector for the data log(sector 128)of the flash
*														 needs to be erased to store the next logs. This overwrites
*														 the oldest records.
* @note       Notes		     : None
*****************************************************************************/
U8 sector_erase(U16 * read_flash)
{
	U8 error = 1;
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_127_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_127_END_ADDR) && 
			(read_flash >= (U16 *)SECTOR_127_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
	}
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_128_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_128_END_ADDR) && 
			(read_flash >= (U16 *)SECTOR_128_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
	}
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_129_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_129_END_ADDR) && 
			(read_flash >= (U16 *)SECTOR_129_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
	}
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_130_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_130_END_ADDR) &&
			(read_flash >= (U16 *)SECTOR_130_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
	}
	
	return (error);
}

/*****************************************************************************
* @note       Function name: U8 flash_log_store(U8 type_of_log)
* @returns    returns		   : FALSE if there is any error while wrting to the flash
*														 TRUE if there is no error writing to the flash
* @param      arg1			   : Flag to indicate type of log being stored in the
*														 flash
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Stores the data log structures to the flash.
*  													 Checks if the sector has exceeded the current sector
*														 has exceeded, then it will erase the next sector to store
*														 the data.
* @note       Notes		     : None
*****************************************************************************/
U8 flash_log_store(U8 type_of_log)
{
	U16 * u16Var;
	U8 success = __TRUE;	
	U16 i;
	static int first_time = 0 ;
	U16 log_type = (U16)type_of_log;
	__align(4) PERIODIC_LOG_STRUCT 		clr_periodic_data;
	
	if (first_time == 0)
	{
		if((Flags_struct.Log_flags & LOGS_WR_FLAG) == LOGS_WR_FLAG)
		{
			Flash_log_data.Current_write_addr_ptr = (U16*)LPC_RTC->GPREG1;
		}
		else
		{
			Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
		}
		Flash_log_data.Nos_of_records = 0;
		first_time = 1;
	}
	Flash_log_data.Nos_of_records++;
		
	switch (type_of_log)
	{
		case periodic_log:
		case periodic_log_start:
		case periodic_log_stop:
		case periodic_log_stop_at_start:
		case total_clr_wt_log:
		case calibration_finished:
		case periodic_tst_wt_log:
		case periodic_dig_log:
		case periodic_mat_log:
		case periodic_zer_log:
		case periodic_len_log:
		case calibration_cancel:
		case daily_wt_clr_log:	/*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/	
			u16Var = (U16*)&periodic_data;
			if (sector_erase(Flash_log_data.Current_write_addr_ptr + FLASH_SIZEOF(periodic_data) + (2*FLASH_SIZEOF(U16))))
			{
				for(i=0; i<(FLASH_SIZEOF(periodic_data)+(2*FLASH_SIZEOF(U16))); i++)
				{
					if(i==0)
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					else if((i>0) && (i<(FLASH_SIZEOF(periodic_data)+FLASH_SIZEOF(U16))))
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, *u16Var++);
					}
					else
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					Flash_log_data.Current_write_addr_ptr++;
					if(Flash_log_data.Current_write_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
					{
						Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
					}
					if(success == __FALSE)
					{
						break;
					}
				}
			}			
		break;
		
		case set_zero_log:
			u16Var = (U16*)&set_zero_data_log;
			if (sector_erase(Flash_log_data.Current_write_addr_ptr + FLASH_SIZEOF(set_zero_data_log) + (2*FLASH_SIZEOF(U16))))
			{
				for(i=0; i<(FLASH_SIZEOF(set_zero_data_log)+(2*FLASH_SIZEOF(U16))); i++)
				{
					if(i==0)
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					else if((i>0) && (i<(FLASH_SIZEOF(set_zero_data_log)+FLASH_SIZEOF(U16))))
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, *u16Var++);
					}
					else
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					Flash_log_data.Current_write_addr_ptr++;
					if(Flash_log_data.Current_write_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
					{
						Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
					}
					if(success == __FALSE)
					{
						break;
					}				
				}
			}
			break;
		
		case clear_log:
			//Updated this logic on 23-Nov-2015 to save clear log along with periodic log data
			memcpy((U16 *)&clr_periodic_data, &periodic_data, sizeof(periodic_data));
			memcpy((U16 *)&clr_periodic_data.Periodic_hdr.Data_type[0], &clear_data_log.Clr_data_hdr.Data_type[0], sizeof(clear_data_log.Clr_data_hdr.Data_type));
			clr_periodic_data.Periodic_hdr.Id = clear_data_log.Clr_data_hdr.Id;
			u16Var = (U16*)&clr_periodic_data;
			if (sector_erase(Flash_log_data.Current_write_addr_ptr + FLASH_SIZEOF(clr_periodic_data) + (2*FLASH_SIZEOF(U16))))
			{
				for(i=0; i<(FLASH_SIZEOF(clr_periodic_data)+(2*FLASH_SIZEOF(U16))); i++)
				{
					if(i==0)
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					else if((i>0) && (i<(FLASH_SIZEOF(clr_periodic_data)+FLASH_SIZEOF(U16))))
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, *u16Var++);
					}
					else
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					Flash_log_data.Current_write_addr_ptr++;
					if(Flash_log_data.Current_write_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
					{
						Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
					}
					if(success == __FALSE)
					{
						break;
					}			
				}
			}			
			break;
		
    case calib_log:
		case digital_log:
		case mat_test_log:
		case tst_wt_log:	
			u16Var = (U16*)&calib_data_log;
			if (sector_erase(Flash_log_data.Current_write_addr_ptr + FLASH_SIZEOF(calib_data_log) + (2*FLASH_SIZEOF(U16))))
			{
				for(i=0; i<(FLASH_SIZEOF(calib_data_log)+(2*FLASH_SIZEOF(U16))); i++)
				{
					WRstatus = i;
					if(i==0)
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					else if((i>0) && (i<(FLASH_SIZEOF(calib_data_log)+FLASH_SIZEOF(U16))))
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, *u16Var++);
					}
					else
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					Flash_log_data.Current_write_addr_ptr++;
					if(Flash_log_data.Current_write_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
					{
						Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
					}
					if(success == __FALSE)
					{
						break;
					}				
				}
			}			
			break;
		
// 		case error_log:
// 			u16Var = (U16*)&error_data_log;
// 			if (sector_erase(Flash_log_data.Current_write_addr_ptr + FLASH_SIZEOF(error_data_log) + (2*FLASH_SIZEOF(U16))))
// 			{
// 				for(i=0; i<(FLASH_SIZEOF(error_data_log)+(2*FLASH_SIZEOF(U16))); i++)
// 				{
// 					if(i==0)
// 					{
// 						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
// 					}
// 					else if((i>0) && (i<(FLASH_SIZEOF(error_data_log)+FLASH_SIZEOF(U16))))
// 					{
// 						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, *u16Var++);
// 					}
// 					else
// 					{
// 						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
// 					}
// 					Flash_log_data.Current_write_addr_ptr++;
// 					if(Flash_log_data.Current_write_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
// 					{
// 						Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
// 					}
// 					if(success == __FALSE)
// 					{
// 						break;
// 					}				
// 				}
// 			}			
// 			break;
		
		case length_log:
			u16Var = (U16*)&length_cal_data_log;
			if (sector_erase(Flash_log_data.Current_write_addr_ptr + FLASH_SIZEOF(length_cal_data_log) + (2*FLASH_SIZEOF(U16))))
			{
				for(i=0; i<(FLASH_SIZEOF(length_cal_data_log)+(2*FLASH_SIZEOF(U16))); i++)
				{
					if(i==0)
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					else if((i>0) && (i<(FLASH_SIZEOF(length_cal_data_log)+FLASH_SIZEOF(U16))))
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, *u16Var++);
					}
					else
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					Flash_log_data.Current_write_addr_ptr++;
					if(Flash_log_data.Current_write_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
					{
						Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
					}
					if(success == __FALSE)
					{
						break;
					}
				}
			}			
			break;
	  default:
			break;
	}
	LPC_RTC->GPREG1 = (U32)Flash_log_data.Current_write_addr_ptr;
	LPC_RTC->GPREG2 = Flash_log_data.Nos_of_records;
	
	return(success);
}
/*****************************************************************************
* @note       Function name: void read_flash_log_data(U16 size_of_struct)
* @returns    returns		   : None
* @param      arg1			   : Size of data to be read from the external flash
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Writes the backup of the configuration to the flash
														 to the 1st 8K sector. Flag is checked, to verify if
														 the flash is being written for the first time and changed
														 to 0x5A5A
* @note       Notes		     : None
*****************************************************************************/

void read_flash_log_data(U16 size_of_struct)
{
	U16 j;
	
	for(j=0; j<size_of_struct; j++)
	{
		Log_restore_buffer[j] = *readAddress_u16;
		readAddress_u16++;
// 		if (readAddress_u16 >= (U16 *)SECTOR_130_END_ADDR)
// 		{
// 			readAddress_u16 = (U16 *)SECTOR_127_START_ADDR;
// 		}
	}	
	return;
}

/*****************************************************************************
* @note       Function name: void flash_log_restore_to_usb(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : SKS
* @date       date created : DEC 1, 2016
* @brief      Description	 : write logs stored in Flash to USB drive.
															Only missing logs should be written.
* @note       Notes		     : None
*****************************************************************************/
// char fpath[60];
// char fpath1[60];
// char file_day_idx_char[3];
// char file_mon_idx_char[3];
// char line1_char[200],timetocheck_char[12],datetocheck_char[18];

// char *tok;
// FILE *fin;
// int ch,line_count_int,lineidx,found_hour_int,found_min_int,found_sec_int,found_dom_int,found_mon_int,found_year_int;
// void sync_flash_log_usb(void){
// 	int file_day,file_mon,file_year;
// 	int file_day_idx,file_mon_idx,file_year_idx,file_found=0;
// // check for the last record.
// // get directory listings
// 	FINFO info;	
//   info.fileID = 0;                             /* info.fileID must be set to 0 */
// 	/*
// 	check for the latest files as per current RTC values
// 	*/
// 	file_year = LPC_RTC->YEAR;									//get current year from RTC
// 	file_mon = LPC_RTC->MONTH;									//get current month from RTC 
// 	file_day= LPC_RTC->DOM;									//get current hour from RTC
// 	for (file_year_idx = file_year; (file_year_idx>(DATE_YR_MIN-1));file_year_idx--){ // start with latest year and search until MVT limit of year
// 		sprintf(fpath,"U0:\\%d\\*.*",file_year_idx );	
// 		if (ffind (fpath, &info) == 0){
// 			file_found=1;
// 			break;
// 		}
// 	}
// 	if (file_found ==0) file_year_idx =DATE_YR_MIN;
// 	file_found =0;
// 	for (file_mon_idx = file_mon; (file_mon_idx>(DATE_MON_MIN-1));file_mon_idx--){
// 		if (file_mon_idx<10){
// 			file_mon_idx_char[0] = '0';
// 			file_mon_idx_char[1] = 0x30 + file_mon_idx;
// 			file_mon_idx_char[2]=0;
// 			sprintf(fpath1,"U0:\\%d\\%s",file_year_idx,file_mon_idx_char );
// 		}
// 		else sprintf(fpath1,"U0:\\%d\\%d",file_year_idx,file_mon_idx );	
// 			sprintf(fpath,"%s\\*.*",fpath1 );	
// 		if (ffind (fpath, &info) == 0){
// 			file_found=1;
// 			break;
// 		}
// 	}
// 	if (file_found ==0) file_mon_idx=DATE_MON_MIN;
// 	file_found =0;
// 	for (file_day_idx = file_day; (file_day_idx>(DATE_DATE_MIN-1));file_day_idx--){
// 		if (file_day_idx<10){
// 			file_day_idx_char[0] = '0';
// 			file_day_idx_char[1] = 0x30 + file_day_idx;
// 			file_day_idx_char[2]=0;
// 			if (file_mon_idx<10){
// 				sprintf(fpath,"%s\\%d-%s-%s_periodic_log.txt",fpath1,file_year_idx,file_mon_idx_char,file_day_idx_char );
// 			}
// 			else sprintf(fpath,"%s\\%d-%d-%s_periodic_log.txt",fpath1,file_year_idx,file_mon_idx,file_day_idx_char );
// 		}
// 		else	{
// 			if (file_mon_idx<10){
// 				sprintf(fpath,"%s\\%d-%s-%d_periodic_log.txt",fpath1,file_year_idx,file_mon_idx_char,file_day_idx );
// 			}
// 			else sprintf(fpath,"%s\\%d-%d-%d_periodic_log.txt",fpath1,file_year_idx,file_mon_idx,file_day_idx );	
// 		
// 		}
// 		if (ffind (fpath, &info) == 0){
// 			file_found=1;									//file found now read_files_on_usb line by line data
// 			line_count_int =0;
// 			fin = fopen (fpath,"r");
// 			if (fin!=NULL){
// 			lineidx =0;
// 			while ((ch = fgetc (fin)) != EOF)  {
// 				line1_char[lineidx] = ch;
// 				line1_char[lineidx+1] = '\0';
// 				lineidx++;
// 				if (ch=='\n') {
// 					line_count_int++;
// 					lineidx =0;
// 				}
// 			}
// 		
// 			
// 			// DAT, 146, 2016/12/8, 14:46:00,  354.599287, Tons, 144.770874, Tons/Hour, 38.212299, 45.000000, 45.690681, Feet/Min,  354.599287, Tons,
// 			// get time details now.
// 			tok = strtok (line1_char, ",");//DAT
// 			if(tok!=NULL)
// 			tok = strtok (NULL, ",");//146
// 			if(tok!=NULL)
// 			tok = strtok (NULL, ",");//2016/12/8	date!!!!!! goto it
// 			strcpy(datetocheck_char,tok);
// 			if(tok!=NULL)
// 			tok = strtok (NULL, ",");//14:46:00		time!!!!!! got it.
// 			strcpy(timetocheck_char,tok);
// 			while (tok!=NULL)
// 			tok = strtok (NULL, ",");
// 			
// 			
// 			tok = strtok (datetocheck_char, "/");//14:46:00
// 			found_year_int  = atoi(tok);
// 			tok = strtok (NULL, "/");//14:46:00
// 			found_mon_int = atoi(tok);
// 			tok = strtok (NULL, "/");//14:46:00
// 			found_dom_int = atoi(tok);
// 			
// 			
// 			tok = strtok (timetocheck_char, ":");//14:46:00
// 			found_hour_int = atoi(tok);
// 			tok = strtok (NULL, ":");//14:46:00
// 			found_min_int = atoi(tok);
// 			tok = strtok (NULL, ":");//14:46:00
// 			found_sec_int = atoi(tok);
// 			fclose (fin);
// 			flash_log_sync_to_usb(found_dom_int,found_mon_int,found_year_int,found_hour_int,found_min_int,found_sec_int);
// 				
// 		}
// 			break;
// 		}
// 		else{
// 			flash_log_sync_to_usb(DATE_DATE_MIN,DATE_MON_MIN,DATE_YR_MIN,TIME_HR_MIN,TIME_MINT_MIN,TIME_SEC_MIN);
// break;		
// }
// 	}
// }
/*****************************************************************************
* @note       Function name: void flash_log_restore_to_usb(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : SKS
* @date       date created : Dec 8, 2016
* @brief      Description	 : Writes the backup of the configuration to the flash
														 to the 1st 8K sector. Flag is checked, to verify if
														 the flash is being written for the first time and changed
														 to 0x5A5A
														only those logs newer than the ones in USB are written.
* @note       Notes		     : None
*****************************************************************************/

// void flash_log_sync_to_usb_old(int dom_to_sync,int mon_to_sync,int year_to_sync,int hour_to_sync,int min_to_sync,int sec_to_sync){
// 	volatile U16 log_type;
// 	U16 count_idx=0,file_count=0; 
// 	U16 i;
// 	U16 size=0;
// 	U16 *this_record = NULL;
// 	U8 first_time = 0;
// 	U16 *periodic_id_ptr = NULL;
// 	char disp[100];	
// 	g_restore_prog = 1;
// 	
// 	//Flash_log_data.Current_write_addr_ptr;
// 	//SKS 1000->3000
// 	;

// 	readAddress_u16 = ((U16 *)SECTOR_127_START_ADDR);
// 	while (
// 					(
// 						(
// 						(((U16)*readAddress_u16)!=periodic_log)&&
// 						(((U16)*readAddress_u16)!=periodic_log_start)&&
// 						(((U16)*readAddress_u16)!=periodic_log_stop)&&
// 						(((U16)*readAddress_u16)!=periodic_log_stop_at_start)&&
// 						(((U16)*readAddress_u16)!=total_clr_wt_log)&&
// 						(((U16)*readAddress_u16)!=calibration_finished)&&
// 						(((U16)*readAddress_u16)!=periodic_tst_wt_log)&&
// 						(((U16)*readAddress_u16)!=periodic_dig_log)&&
// 						(((U16)*readAddress_u16)!=periodic_mat_log)&&
// 						(((U16)*readAddress_u16)!=periodic_zer_log)&&
// 						(((U16)*readAddress_u16)!=periodic_len_log)&&
// 						(((U16)*readAddress_u16)!=calibration_cancel)&&
// 						(((U16)*readAddress_u16)!=daily_wt_clr_log)&&
// 						(((U16)*readAddress_u16)!=set_zero_log)&&
// 						(((U16)*readAddress_u16)!=clear_log)&&
// 						(((U16)*readAddress_u16)!=calib_log)&&
// 						(((U16)*readAddress_u16)!=digital_log)&&
// 						(((U16)*readAddress_u16)!=mat_test_log)&&
// 						(((U16)*readAddress_u16)!=tst_wt_log)&&
// 						(((U16)*readAddress_u16)!=length_log)
// 				)&& ((readAddress_u16)<(U16*)SECTOR_130_END_ADDR))
// 			){
// 		readAddress_u16++;
// 	}
// 	for(count_idx=0; /*(count_idx<Flash_log_data.Nos_of_records)&&*/ ((readAddress_u16)<(U16*)SECTOR_130_END_ADDR); count_idx++)
// 	//for(i=0; i<Flash_log_data.Nos_of_records; i++)
// 	{
// 		
// 		//	this_record = readAddress_u16;	//store address where we started for record retrieval
// 			//get the type of log that is being read
// 			if(count_idx) //if it is not first record
// 			{
// 				readAddress_u16++;	//increment pointer as need to skip log type present at the end
// 			}
// // 			sprintf(&disp[0],"          Synchronizing Record  %d  of %d  ",(count_idx+1),Flash_log_data.Nos_of_records);
// // 			GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
// 			
// 			log_type = (U16)*readAddress_u16;
// 			readAddress_u16++;
// 			switch(log_type & 0x00FF)
// 			{
// 					case periodic_log:	//if the log type is a periodic data log
// 					case periodic_log_start:
// 					case periodic_log_stop:
// 					case periodic_log_stop_at_start:
// 					case total_clr_wt_log:
// 					case calibration_finished:
// 					case periodic_tst_wt_log:
// 					case periodic_dig_log:
// 					case periodic_mat_log:
// 					case periodic_zer_log:
// 					case periodic_len_log:
// 					case calibration_cancel:
// 					case daily_wt_clr_log:	/*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/			
// 							read_flash_log_data(FLASH_SIZEOF(periodic_data));
// 							
// 							{
// 								memcpy((U16 *)&periodic_data, Log_restore_buffer, sizeof(periodic_data));
// 								clear_data_log.Clr_data_hdr.Id = periodic_data.Periodic_hdr.Id;		// To match both hdr ID
// 								
// 								if(
// 									(periodic_data.Periodic_hdr.Date.Year > year_to_sync)||		//year is greater
// 									(																													//same year newer month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon > mon_to_sync)
// 									 )||
// 									(																													//same year same day new month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day > dom_to_sync)
// 									)||
// 									(																													//same year same day same month new hour
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr> hour_to_sync)
// 									)||
// 									(																													//same year same day same month same hour new min
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min> min_to_sync)
// 									)
// 									||
// 									(																													//same year same month same day same hour same min new sec
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min== min_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Sec> sec_to_sync)
// 									)	
// 								){
// 								sprintf(&disp[0],"          Synchronizing Record  %d   ",(file_count++));
// 								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
// 								log_file_write(periodic_log);}
// 							}							
// 						break;
// 				
// 					case set_zero_log:	//if the log type is a set zero data log					
// 							read_flash_log_data(FLASH_SIZEOF(set_zero_data_log));
// 							memcpy((U16 *)&set_zero_data_log, Log_restore_buffer, sizeof(set_zero_data_log));
// 							if(
// 									(periodic_data.Periodic_hdr.Date.Year > year_to_sync)||		//year is greater
// 									(																													//same year newer month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon > mon_to_sync)
// 									 )||
// 									(																													//same year same day new month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day > dom_to_sync)
// 									)||
// 									(																													//same year same day same month new hour
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr> hour_to_sync)
// 									)||
// 									(																													//same year same day same month same hour new min
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min> min_to_sync)
// 									)
// 									||
// 									(																													//same year same month same day same hour same min new sec
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min== min_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Sec> sec_to_sync)
// 									)	
// 								){
// 								sprintf(&disp[0],"          Synchronizing Record  %d  ",(file_count++));
// 								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);log_file_write(set_zero_log);}
// 						break;
// 			
// 					case clear_log:	//if the log type is a clear data log
// 							read_flash_log_data(FLASH_SIZEOF(periodic_data));
// 							//Updated this logic on 23-Nov-2015 to restore clear log along with periodic log data
// 							memcpy((U16 *)&periodic_data, Log_restore_buffer, sizeof(periodic_data));
// 							memcpy((U16 *)&clear_data_log.Clr_data_hdr.Data_type[0], &periodic_data.Periodic_hdr.Data_type[0], sizeof(clear_data_log.Clr_data_hdr.Data_type));
// 							if(
// 									(periodic_data.Periodic_hdr.Date.Year > year_to_sync)||		//year is greater
// 									(																													//same year newer month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon > mon_to_sync)
// 									 )||
// 									(																													//same year same day new month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day > dom_to_sync)
// 									)||
// 									(																													//same year same day same month new hour
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr> hour_to_sync)
// 									)||
// 									(																													//same year same day same month same hour new min
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min> min_to_sync)
// 									)
// 									||
// 									(																													//same year same month same day same hour same min new sec
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min== min_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Sec> sec_to_sync)
// 									)	
// 								){
// 								sprintf(&disp[0],"          Synchronizing Record  %d  ",(file_count++));
// 								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);log_file_write(clear_log);}
// 						break;
// 			
// 					case calib_log:	//if the log type is a calibration data log
// 					case digital_log: //"CAL"
// 					case mat_test_log: //"CAL"
// 					case tst_wt_log: //"CAL"
// 							read_flash_log_data(FLASH_SIZEOF(calib_data_log));
// 							memcpy((U16 *)&calib_data_log, Log_restore_buffer, sizeof(calib_data_log));
// 							if(
// 									(periodic_data.Periodic_hdr.Date.Year > year_to_sync)||		//year is greater
// 									(																													//same year newer month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon > mon_to_sync)
// 									 )||
// 									(																													//same year same day new month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day > dom_to_sync)
// 									)||
// 									(																													//same year same day same month new hour
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr> hour_to_sync)
// 									)||
// 									(																													//same year same day same month same hour new min
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min> min_to_sync)
// 									)
// 									||
// 									(																													//same year same month same day same hour same min new sec
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min== min_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Sec> sec_to_sync)
// 									)	
// 								){
// 								sprintf(&disp[0],"          Synchronizing Record  %d ",(file_count++));
// 								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);{
// 									if ((log_type & 0x00FF) == calib_log)
// 									{
// 										log_file_write(calib_log);
// 									}
// 									else if ((log_type & 0x00FF) == digital_log)
// 									{
// 										log_file_write(digital_log);
// 									}
// 									else if ((log_type & 0x00FF) == mat_test_log)
// 									{
// 										log_file_write(mat_test_log);
// 									}
// 									else if ((log_type & 0x00FF) == tst_wt_log)
// 									{
// 										log_file_write(tst_wt_log);
// 									}
// 								}
// 							}
// 						break;

// // 					case error_log:	//if the log type is a error data log "ERR"
// // 							read_flash_log_data(FLASH_SIZEOF(error_data_log));
// // 							memcpy((U16 *)&error_data_log, Log_restore_buffer, sizeof(error_data_log));
// // 							log_file_write(error_log);
// // 						break;
// 					
// 					case length_log:	//if the log type is a length data log "LEN"
// 							read_flash_log_data(FLASH_SIZEOF(length_cal_data_log));
// 							memcpy((U16 *)&length_cal_data_log, Log_restore_buffer, sizeof(length_cal_data_log));
// 							if(
// 									(periodic_data.Periodic_hdr.Date.Year > year_to_sync)||		//year is greater
// 									(																													//same year newer month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon > mon_to_sync)
// 									 )||
// 									(																													//same year same day new month
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day > dom_to_sync)
// 									)||
// 									(																													//same year same day same month new hour
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr> hour_to_sync)
// 									)||
// 									(																													//same year same day same month same hour new min
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min> min_to_sync)
// 									)
// 									||
// 									(																													//same year same month same day same hour same min new sec
// 										(periodic_data.Periodic_hdr.Date.Year == year_to_sync)&&
// 										(periodic_data.Periodic_hdr.Date.Mon == mon_to_sync)&&	
// 										(periodic_data.Periodic_hdr.Date.Day == dom_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Hr == hour_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Min== min_to_sync)&&
// 										(periodic_data.Periodic_hdr.Time.Sec> sec_to_sync)
// 									)	
// 								){
// 								sprintf(&disp[0],"          Synchronizing Record  %d",(file_count++));
// 								GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);log_file_write(length_log);}
// 						break;
// 					
// 					default:
// 						break;
// 			}
// 	}//for loop
// 	if((Flash_log_data.Nos_of_records == 0)&&(flash_restore_to_usb_fg == 1))
// 	{
// 		sprintf(&disp[0],"      NO RECORD FOUND IN FLASH     ");
// 		GUI_DispString(disp);	
// 					flash_restore_to_usb_fg = 0;
// 			
// 	}
// 	
// 	//ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
// 	//ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
// 	//ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
// 	//ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
// 		flash_restore_to_usb_fg = 0;
// 	g_restore_prog = 0;
// 	ferror_cnt=0;	//Data got written successfully so decrement count
// 	return;
// }

/*****************************************************************************
* @note       Function name: void flash_log_restore_to_usb(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Writes the backup of the configuration to the flash
														 to the 1st 8K sector. Flag is checked, to verify if
														 the flash is being written for the first time and changed
														 to 0x5A5A
* @note       Notes		     : None
*****************************************************************************/

/*****************************************************************************
* @note       Function name: void flash_para_backup(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Megha Gaikwad
* @date       date created : Aug 10, 2015
* @brief      Description	 : Writes the backup of the parameters to the flash. 
														 Parametres are All totals, Daily report, weekly report and 
														 monthly report parameters, all header IDs
														 These parameters will be restored after soft reset condition.
* @note       Notes		     : None
*****************************************************************************/
void flash_para_backup(void)
{
}
// {
// 	U16 *read_flash_addr, *Temp_FlashStartAdd;
// 	union double_union buffer;
// 	union float_union buffer_float;
// 	U16 * u16Var;
// 	int i;
// 	U32 addr,addr1;
// 	U16 Calculated_CRC = 0;
// 	U16 Block_Length;
// #if 0
// 	FILE *fp;
// #endif
// 	

// 	addr = flash_para_addr;
// 	record_no = flash_para_record_no;
// 	Block_Length = EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH;
// 	
// 	if((addr == 0) && (record_no == 0))
// 	{
// 		addr = EXT_FLASH_PARA_BACKUP_START_ADDR;
// 		ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_START_ADDR);		
// #if 0
// 		fp = fopen ("Para.txt", "a");
// 		if(fp != NULL)
// 		{
// 			fprintf(fp, "%s\r\n", "First Region");
// 			fclose(fp);
// 		}
// #endif		
// 	}
// 	else
// 	{	
// 		addr = (addr + (EXT_FLASH_PAGE_SIZE_BYTES*4));
// 	}	
// 	
// 	//Reset Flash address and record no
// 	if(addr > EXT_FLASH_PARA_BACKUP_END_ADDR)
// 	//if(addr >= (EXT_FLASH_PARA_BACKUP_START_ADDR + 0x200))
// 	{
// 		//PVK - for garbage collection
// 		ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_COPY_START_ADDR);
// 		addr1 = addr;
// 		addr = addr1 - (EXT_FLASH_PAGE_SIZE_BYTES*4) ;
// 		read_flash_addr = (U16 *)addr; 
// 		Temp_FlashStartAdd = (U16 *)EXT_FLASH_PARA_BACKUP_COPY_START_ADDR;
// 		
// 		//Take backup copy of data
// 		for (i=0; i<(Block_Length/2); i++)
// 		{
// 			//Read data from current flash and wirte it into backup copy
// 			ExtFlash_writeWord((uint32_t)Temp_FlashStartAdd, *read_flash_addr);
// 			read_flash_addr++;
// 			Temp_FlashStartAdd++;
// 		}	
// 		
// 		//PVK - Copy data from backup to working copy
// 		ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_START_ADDR);
// 		Temp_FlashStartAdd = (U16 *)EXT_FLASH_PARA_BACKUP_START_ADDR;
// 		addr = EXT_FLASH_PARA_BACKUP_COPY_START_ADDR;
// 		read_flash_addr = (U16 *)addr;
// 		
// 		//Take backup copy of data
// 		for (i=0; i<(Block_Length/2); i++)
// 		{
// 			//Read data from current flash and wirte it into backup copy
// 			ExtFlash_writeWord((uint32_t)Temp_FlashStartAdd, *read_flash_addr);
// 			read_flash_addr++;
// 			Temp_FlashStartAdd++;
// 		}	
// 			
// 		addr = (EXT_FLASH_PARA_BACKUP_START_ADDR + (EXT_FLASH_PAGE_SIZE_BYTES*4)) ;
// 		record_no = 1;
// 	}
// 	
// 	//PVK - 10- Feb -2016 
// 	//Verify All Totals
// 	Verify_Totals();
// 	//PVK
//   //Increment Rcord number
// 	record_no++;	
// 	read_flash_addr = (U16 *)addr; 
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, record_no);
// 	//Dummy Rcord number write for Alignment
// 	read_flash_addr = (U16 *)(addr + 2); 
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, record_no);
// 	
// 	//Backup of Total_weight_accum
// 	buffer.double_val = Calculation_struct.Total_weight_accum;
// 	//Store the accumulated weight into flash 
// 	read_flash_addr = (U16 *)(addr + 4);
//   u16Var = (U16*)&buffer;

// 	for (i=0; i<FLASH_SIZEOF(buffer); i++)
// 	{
// 	   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	   read_flash_addr++;
// 	}
// 		
// 	//Backup of Job Total
// 	buffer.double_val = Rprts_diag_var.Job_Total;
// 	read_flash_addr = (U16 *)(addr + 12);	 
// 	u16Var = (U16*)&buffer;
// 	for (i=0; i<FLASH_SIZEOF(buffer); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}	
// 	//Backup of Master Total
// 	buffer.double_val = Rprts_diag_var.Master_total;
// 	read_flash_addr = (U16 *)(addr + 20);
// 	u16Var = (U16*)&buffer;
// 	for (i=0; i<FLASH_SIZEOF(buffer); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}   	 
// 	//Backup of Daily total
// 	buffer.double_val = Rprts_diag_var.daily_Total_weight;
// 	read_flash_addr = (U16 *)(addr + 28);
// 	u16Var = (U16*)&buffer;
// 	for (i=0; i<FLASH_SIZEOF(buffer); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	//Backup of weekly total
// 	buffer.double_val = Rprts_diag_var.weekly_Total_weight;
// 	read_flash_addr = (U16 *)(addr + 36);
// 	u16Var = (U16*)&buffer;
// 	for (i=0; i<FLASH_SIZEOF(buffer); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	//Backup of monthly total
// 	buffer.double_val = Rprts_diag_var.monthly_Total_weight;
// 	read_flash_addr = (U16 *)(addr + 44);
// 	u16Var = (U16*)&buffer;
// 	for (i=0; i<FLASH_SIZEOF(buffer); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	} 
// 	//Backup of Yearly total
// 	buffer.double_val = Rprts_diag_var.yearly_Total_weight;
// 	read_flash_addr = (U16 *)(addr + 52);
// 	u16Var = (U16*)&buffer;
// 	for (i=0; i<FLASH_SIZEOF(buffer); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	/********************************************************************/
// 	//store the local set point
// 	u16Var = (U16*)&PID_Old_Local_Setpoint;
// 	for (i=0; i<FLASH_SIZEOF(PID_Old_Local_Setpoint); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}//133,420.00	
// 	//store the current time
// 	u16Var = (U16*)&Current_time;
// 	for (i=0; i<FLASH_SIZEOF(Current_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	if(!(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR) && ((daily_rprt.Dummy_var1&0x0002) == 0x0002))
// 	{
// 		if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
// 		{				
// 				daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
// 				daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
// 				daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
// 		}
// 		else if(Admin_var.Current_Time_Format == TWELVE_HR)
// 		{
// 			if(Admin_var.AM_PM == AM_TIME_FORMAT)
// 			{
// 				if((U8) Current_time.RTC_Hour == 12)
// 					daily_rprt.End_time.Hr = 0;
// 				else
// 					daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
// 				daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
// 				daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;				
// 			}
// 			else
// 			{
// 				daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour+12;
// 				daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
// 				daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;								
// 			}
// 		}		
// 		//daily_rprt.Cnt_zero_speed++;
// 		//weekly_rprt.Cnt_zero_speed++;
// 		//monthly_rprt.Cnt_zero_speed++;
// 		daily_rprt.Dummy_var1 &= 0xFF00;			
// 		daily_rprt.Dummy_var1 |= 0x0004;			
// 	}
// 	
// 	//-------------------------------Daily report-------------------------
// 	//Daily report zero speed counter
// 	u16Var = (U16*)&daily_rprt.Cnt_zero_speed;
// 	for (i=0; i<FLASH_SIZEOF(daily_rprt.Cnt_zero_speed); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//Daily report start time for zero speed
// 	u16Var = (U16*)&daily_rprt.Start_time;
// 	for (i=0; i<sizeof(daily_rprt.Start_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//Daily report end time for zero speed
// 	u16Var = (U16*)&daily_rprt.End_time;
// 	for (i=0; i<sizeof(daily_rprt.End_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//Daily report downtime
// 	u16Var = (U16*)&daily_rprt.Down_time;
// 	for (i=0; i<FLASH_SIZEOF(daily_rprt.Down_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//Daily report start load time
// 	u16Var = (U16*)&daily_rprt.Start_load_time;
// 	for (i=0; i<sizeof(daily_rprt.Start_load_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// /*	if((Calculation_struct.Rate > Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
// 				|| (Calculation_struct.Rate > 0 && Admin_var.Zero_rate_status == Screen6171_str5))*/
// 	//By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152
// 	if((Admin_var.Zero_rate_status == Screen6171_str2) ||(Calculation_struct.Rate_for_display > 0 && Admin_var.Zero_rate_status == Screen6171_str5)) //BS-152 fixed by DK on 27 April 2016  ?
// 	{
// 		if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
// 		{				
// 				daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour;
// 				daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
// 				daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
// 		}
// 		else if(Admin_var.Current_Time_Format == TWELVE_HR)
// 		{
// 			if(Admin_var.AM_PM == AM_TIME_FORMAT)
// 			{
// 				if((U8) Current_time.RTC_Hour == 12)
// 					daily_rprt.End_load_time.Hr = 0;
// 				else
// 					daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour;
// 				daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
// 				daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
// 			}
// 			else
// 			{
// 					daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour+12;
// 					daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
// 					daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;		
// 			}			
// 		}				 
// 	 daily_rprt.Dummy_var1 &= 0x0000; 
// 	 daily_rprt.Dummy_var1 |= (0x0304);
// 	}
// 	//Daily report end load time
// 	u16Var = (U16*)&daily_rprt.End_load_time;
// 	for (i=0; i<sizeof(daily_rprt.End_load_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	//Daily report rum time
// 	u16Var = (U16*)&daily_rprt.Run_time;
// 	for (i=0; i<FLASH_SIZEOF(daily_rprt.Run_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//-------------------------------Weekly report-------------------------
// 	//weekly report zero speed counter
// 	u16Var = (U16*)&weekly_rprt.Cnt_zero_speed;
// 	for (i=0; i<FLASH_SIZEOF(weekly_rprt.Cnt_zero_speed); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	//weekly report down time
// 	u16Var = (U16*)&weekly_rprt.Down_time;
// 	for (i=0; i<FLASH_SIZEOF(weekly_rprt.Down_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//weekly report run time
// 	u16Var = (U16*)&weekly_rprt.Run_time;
// 	for (i=0; i<FLASH_SIZEOF(weekly_rprt.Run_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//-------------------------------Monthly report-------------------------
// 	//monthly report count zero speed
// 	u16Var = (U16*)&monthly_rprt.Cnt_zero_speed;
// 	for (i=0; i<FLASH_SIZEOF(monthly_rprt.Cnt_zero_speed); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	//monthly report down time
// 	u16Var = (U16*)&monthly_rprt.Down_time;
// 	for (i=0; i<FLASH_SIZEOF(monthly_rprt.Down_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//monthly report run time
// 	u16Var = (U16*)&monthly_rprt.Run_time;
// 	for (i=0; i<FLASH_SIZEOF(monthly_rprt.Run_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//extra write for writing the special id
// 	u16Var = (U16*)&monthly_rprt.Run_time;
// 	for (i=0; i<FLASH_SIZEOF(monthly_rprt.Run_time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	//------------------------Report Header Daily Date and Time-----------------
// 	u16Var = (U16*)&daily_rprt.freq_rprt_header.Date;
// 	for (i=0; i<FLASH_SIZEOF(daily_rprt.freq_rprt_header.Date); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	u16Var = (U16*)&weekly_rprt.freq_rprt_header.Date;
// 	for (i=0; i<FLASH_SIZEOF(weekly_rprt.freq_rprt_header.Date); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	
// 	u16Var = (U16*)&monthly_rprt.freq_rprt_header.Date;
// 	for (i=0; i<FLASH_SIZEOF(monthly_rprt.freq_rprt_header.Date); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}

// 	u16Var = (U16*)&daily_rprt.freq_rprt_header.Time;
// 	for (i=0; i<FLASH_SIZEOF(daily_rprt.freq_rprt_header.Time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}	 
// 	
// 	//------------------------Report Header Weekly Date and Time-----------------
// 	u16Var = (U16*)&weekly_rprt.freq_rprt_header.Time;
// 	for (i=0; i<FLASH_SIZEOF(weekly_rprt.freq_rprt_header.Time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}	  
// 	
// 	//------------------------Report Header Monthly Date and Time-----------------	 
// 	u16Var = (U16*)&monthly_rprt.freq_rprt_header.Time;
// 	for (i=0; i<FLASH_SIZEOF(monthly_rprt.freq_rprt_header.Time); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}	  

// 	daily_rprt.Dummy_var1 |= 0x8000;
// 	u16Var = (U16*)&daily_rprt.Dummy_var1;
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	read_flash_addr++;	 
// 	
// 	/********************************************************************/
// 	u16Var = (U16*)&periodic_data.Periodic_hdr.Id;
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	read_flash_addr++;	

// 	u16Var = (U16*)&set_zero_data_log.Set_zero_hdr.Id;
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	read_flash_addr++;	

// 	u16Var = (U16*)&clear_data_log.Clr_data_hdr.Id;
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	read_flash_addr++;	

// 	u16Var = (U16*)&calib_data_log.Cal_data_hdr.Id;
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	read_flash_addr++;	

// 	u16Var = (U16*)&length_cal_data_log.Len_data_hdr.Id;
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	read_flash_addr++;	

// 	u16Var = (U16*)&error_data_log.Err_data_hdr.Id;
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 	read_flash_addr++;
// 	
// 	buffer_float.float_val = (float)Calculation_struct.run_time;
// 	u16Var = (U16*)&buffer_float;
// 	for (i=0; i<FLASH_SIZEOF(buffer_float); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	 
// 	//Scale setup weight unit
// 	u16Var = (U16*)&Scale_setup_var.Weight_unit;
// 	for (i=0; i<FLASH_SIZEOF(Scale_setup_var.Weight_unit); i++)
// 	{
// 		 ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
// 		 read_flash_addr++;
// 	}
// 	
// 	//PVK
// 	//Block_Length = ((uint32_t)read_flash_addr - addr);
// 	CalcCRC((unsigned char*)addr, (unsigned char*)&Calculated_CRC,(Block_Length-4));
// 	//Write CRC
// 	//store CRC at end of the block
// 	read_flash_addr = (U16 *)(addr + (Block_Length-4)) ; 
// 	ExtFlash_writeWord((uint32_t)read_flash_addr, Calculated_CRC);
// 		
// 	flash_para_addr = (uint32_t)addr;
// 	flash_para_record_no = record_no;
// 	
// 	//PVK - 10- Feb -2016 
// 	Totals_set_to_default_flag = 0;
//   Report_Log_set_to_default_flag = 0;
// 	
// #if 0
// 		fp = fopen ("Para.txt", "a");
// 		if(fp != NULL)
// 		{
// 			fprintf(fp, "%s\r\n", "Backup");
// 			fprintf(fp,"%d,%d\r\n",flash_para_addr,flash_para_record_no);
// 			fclose(fp);
// 		}
// #endif
// 					
// }


/*****************************************************************************
* @note       Function name: void flash_para_reload(U32 Totals_Current_addr,char Status_condition)
* @returns    returns		   : None
* @param      arg1			   : addr - Flash address to read data
* @author			             : Oaces Team
* @date       date created : dec 29, 2015
* @brief      Description	 : Read the all totals from the flash and check for CRC. 
                             If Matches then reload it else set to zero
* @note       Notes		     : None
*****************************************************************************/
void flash_para_reload(U32 Totals_Current_addr, char Status_condition)
{
  char Status;
	U32 Current_Address;
//	U16 * read_flash;
	U16 Record_Mismatch_Counter = 0;
	
//	uint16_t Temp_data[EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH];
	
	Current_Address = Totals_Current_addr;
	Status = Status_condition;
	
	//PVK	
  if((g_old_fw_ver >= (float)4.19) || (!((g_old_fw_ver >= (float)0.90) && (g_old_fw_ver <= (float)0.99))))
	{	
		while(Status == 0)
		{
			Status = Reload_totals_from_flash_with_CRC_Check(Current_Address);
			
			if(Status == 0)
			{	
				Record_Mismatch_Counter++;
				Current_Address = Current_Address - EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH;
				if(Current_Address <SECTOR_131_START_ADDR/* EXT_FLASH_PARA_BACKUP_START_ADDR*/)
				{
					//All blocks CRC not matched then set status to 2
					Status = 2;
					record_no = Record_Mismatch_Counter;
				}
			}  			
		}
		
		
		if((Status == 1) && (Record_Mismatch_Counter == 0))
		{
       //PVK - 10- Feb -2016 
			 //Verify All Totals
			 Verify_Totals();
			
			 if((1 == Totals_set_to_default_flag) || (1 == Report_Log_set_to_default_flag))
			 {
 				 Flash_Write_In_progress = 1; //Added on 27 April 2016  
				 
				 flash_para_backup();
				 
				 Flash_Write_In_progress = 0; //Added on 27 April 2016  
       } 				 
    }			
		else if((Status == 1) && (Record_Mismatch_Counter != 0))
		{
			record_no = Record_Mismatch_Counter + record_no;	//Identify next rec no.
			
			/*Store all totals and report parameters in Ext Flash*/
			//PVK-08 Jan-2015
			//read_flash = (U16 *)Current_Address;
			//flash_para_record_no = *read_flash;
			flash_para_record_no = record_no;
			//PVK - 11 Jan 2015
			flash_para_addr = Totals_Current_addr;
			
			Flash_Write_In_progress = 1; //Added on 27 April 2016  
			flash_para_backup();
      Flash_Write_In_progress = 0; //Added on 27 April 2016  
		}
				
		//All blocks CRC not matched then clear all totals
		if(Status == 2)
		{
				//Erase and set param to zero
				Current_Address = EXT_FLASH_PARA_BACKUP_START_ADDR;
			  Flash_Write_In_progress = 1; //Added on 27 April 2016  
				ExtFlash_eraseBlock(Current_Address);
			  Flash_Write_In_progress = 0; //Added on 27 April 2016  
			
			/*  //Advance the address for writing default values into flash
				Current_Address = (Totals_Current_addr + EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH);
				//PVK -  from last to first block  checking of  CRC failed and after advancing the address
				//it cross the sector boundary then erase the sector and start from record no 1
			  if(Current_Address > EXT_FLASH_PARA_BACKUP_END_ADDR)
				{
					ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_START_ADDR);
					Current_Address = EXT_FLASH_PARA_BACKUP_START_ADDR;
					record_no = 1;
				}	
				
			  read_flash = (U16 *)Current_Address;
				ExtFlash_writeWord((uint32_t)read_flash, record_no);
				read_flash++;
				ExtFlash_writeWord((uint32_t)read_flash, record_no);
				read_flash++;
				memset(Temp_data, 0x00, sizeof(Temp_data));
			
				ExtFlash_writeBuff((uint32_t)read_flash, Temp_data, ((EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH / 2)- 4));
				Reload_totals_from_flash_with_CRC_Check(Current_Address);
			  
			  //PVK - 11 Jan 2015
			  //store CRC at end of the block
				read_flash = (U16 *)(Current_Address + (EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH-4)) ; 
				ExtFlash_writeWord((uint32_t)read_flash, gTotals_Calculated_CRC);
				
			  //PVK-08 Jan-2015
				read_flash = (U16 *)Current_Address;
				flash_para_addr = Current_Address;
				flash_para_record_no = record_no; 
				*/
				Reload_totals_from_flash_with_CRC_Check(Current_Address);
				flash_para_addr = 0;
				flash_para_record_no = 0; //*read_flash;
				
				Flash_Write_In_progress = 1; //Added on 27 April 2016  
				flash_para_backup();
				Flash_Write_In_progress = 0; //Added on 27 April 2016  
				
				//PVK - 10- Feb -2016 
				//Verify All Totals
				/*Verify_Totals();
			  if((1 == Totals_set_to_default_flag) || (1 == Report_Log_set_to_default_flag))
				{
					flash_para_backup();
				} 				 
				*/
				
				
				//Erase and set backup copy to zero
				/*
				Current_Address = EXT_FLASH_PARA_BACKUP_COPY_START_ADDR;
				read_flash = (U16 *)Current_Address;
				ExtFlash_writeWord((uint32_t)read_flash, 0xFFFF);
				read_flash++;
				ExtFlash_writeWord((uint32_t)read_flash, 0xFFFF);
				read_flash++;
				memset(Temp_data, 0x00, sizeof(Temp_data));
				ExtFlash_writeBuff((uint32_t)read_flash, Temp_data, ((EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH / 2)- 2));
				Reload_totals_from_flash_with_CRC_Check(EXT_FLASH_PARA_BACKUP_START_ADDR);
				*/
				/*Store all totals and report parameters in Ext Flash*/
				//flash_para_backup();
		}//if end		
	}//else end
	else	
	{
		Current_Address = Totals_Current_addr;
		//Restore all totals and records parameter
		Reload_totals_from_flash_with_CRC_Check(Current_Address);	
		//do not change the sequence of calling function as Calculation_struct.Total_weight_accum is read from eprom
		//and wrtie into flash
		eeprom_weight_reload();
		//Store latest parameters for all totals and report parameters in Ext Flash
		
		Flash_Write_In_progress = 1; //Added on 27 April 2016  
		flash_para_backup();
		Flash_Write_In_progress = 0; //Added on 27 April 2016
		
	}
	
}//Function end	
/*****************************************************************************
* @note       Function name: Reload_totals_from_flash_with_CRC_Check(U32 addr)
* @returns    returns		   : None
* @param      arg1			   : addr - Flash address to read data
* @author			             : Megha Gaikwad
* @date       date created : Aug 11, 2015
* @brief      Description	 : Read the backup of the parameters from the flash. 
														 Parametres are All totals, Daily report, weekly report and 
														 monthly report parameters, all header IDs
														 These parameters will be restored after soft reset condition.
* @note       Notes		     : None
*****************************************************************************/
char Reload_totals_from_flash_with_CRC_Check(U32 addr)
{
	U16 * read_flash;
	U16 Data_valid;
	union double_union buffer;
	union ulong_union buffer_long;
	union float_union buffer_float;
	//RTCTime Shutdown_time;
	U16 j=0, read_buffer[30];
	U16 Calculated_CRC = 0;
	U16 Block_Length;
	U32 addr1;
#if 0
	FILE *fp;
#endif
	
#if 0
		fp = fopen ("Para.txt", "a");
		if(fp != NULL)
		{
			fprintf(fp, "%s\r\n", "Reload");
			fprintf(fp,"%d\r\n",addr);
			fclose(fp);
		}
#endif
   addr1 = addr;
		
	//read Record number
	read_flash = (U16 *)addr1; 
	record_no = *read_flash;
		
	 read_flash = (U16 *)(addr1 + 4);
	 //PVK	
	 //for Total_weight_accum
    for(j=0; j<sizeof(Calculation_struct.Total_weight_accum); j++)
	 {
		 Data_valid = *read_flash;
		 buffer.d_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 Calculation_struct.Total_weight_accum = buffer.double_val;			
	 
	 // For Job totals
	 for(j=0; j<sizeof(Rprts_diag_var.Job_Total); j++)
	 {
		 Data_valid = *read_flash;
		 buffer.d_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 Rprts_diag_var.Job_Total = buffer.double_val;
	 // For Master total
	 for(j=0; j<sizeof(Rprts_diag_var.Master_total); j++)
	 {
		 Data_valid = *read_flash;
		 buffer.d_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 Rprts_diag_var.Master_total = buffer.double_val;
	 // For Daily Report total
	 for(j=0; j<sizeof(Rprts_diag_var.daily_Total_weight); j++)
	 {
		 Data_valid = *read_flash;
		 buffer.d_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 Rprts_diag_var.daily_Total_weight = buffer.double_val;
	 // For weekly Report total
	 for(j=0; j<sizeof(Rprts_diag_var.weekly_Total_weight); j++)
	 {
		 Data_valid = *read_flash;
		 buffer.d_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 Rprts_diag_var.weekly_Total_weight = buffer.double_val;
	 // For monthly Report total
	 for(j=0; j<sizeof(Rprts_diag_var.monthly_Total_weight); j++)
	 {
		 Data_valid = *read_flash;
		 buffer.d_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 Rprts_diag_var.monthly_Total_weight = buffer.double_val;
	 // For monthly Report total
	 for(j=0; j<sizeof(Rprts_diag_var.yearly_Total_weight); j++)
	 {
		 Data_valid = *read_flash;
		 buffer.d_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer.d_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 Rprts_diag_var.yearly_Total_weight = buffer.double_val;
	 /* End of Restoring weight Totals*/
/**************************************************************************************/
	 memset(read_buffer, 0, sizeof(read_buffer));
	 //Old Local Set Point
	 for(j=0; j<FLASH_SIZEOF(PID_Old_Local_Setpoint); j++)
	 {
		 read_buffer[j] = *read_flash;
		 read_flash++;
	 }
	 memcpy((U16 *)&PID_Old_Local_Setpoint, read_buffer, sizeof(PID_Old_Local_Setpoint));
	 memset(read_buffer, 0, sizeof(read_buffer));
	 //Shutdown_time
	 for(j=0; j<FLASH_SIZEOF(Shutdown_time); j++)
	 {
		 read_buffer[j] = *read_flash;
		 read_flash++;
	 }
	 memcpy((U16 *)&Shutdown_time, read_buffer, sizeof(Shutdown_time));
	 
	 //-------------------------------daily-------------------------
	 memset(read_buffer, 0, sizeof(read_buffer));
	 //zero speed counter
	 //read_long_data_from_flash(sizeof(daily_rprt.Cnt_zero_speed));
	 for(j=0; j<sizeof(daily_rprt.Cnt_zero_speed); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 daily_rprt.Cnt_zero_speed = buffer_long.ulong_val;
	 
/*						 //zero start time
	 Data_valid = *read_flash;
	 daily_rprt.Start_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.Start_time.Min = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.Start_time.Sec = (Data_valid & 0x00FF);
	 read_flash++;
	 read_flash++;*/
	 
	 //zero start time
	 Data_valid = *read_flash;
	 daily_rprt.Start_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.Start_time.AM_PM = (Data_valid & 0xFF00) >> 8;		//DO not use AM_PM variable for daily_rprt.Start_time
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.Start_time.Min = (Data_valid & 0x00FF);
	 daily_rprt.Start_time.Sec = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 read_flash++;
	 read_flash++;
	 
/*						 //zero end time
	 Data_valid = *read_flash;
	 daily_rprt.End_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.End_time.Min = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.End_time.Sec = (Data_valid & 0x00FF);
	 read_flash++;
	 read_flash++;*/
	 
	 //zero end time
	 Data_valid = *read_flash;
	 daily_rprt.End_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.End_time.AM_PM = (Data_valid & 0xFF00) >> 8;	//used daily_rprt.End_time.AM_PM variable for Last_AM_PM 
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.End_time.Min = (Data_valid & 0x00FF);
	 daily_rprt.End_time.Sec = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 read_flash++;
	 read_flash++;						 
	 
	 //down time
	 for(j=0; j<sizeof(daily_rprt.Down_time); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 daily_rprt.Down_time = buffer_long.ulong_val;
	 
/*						 //start load time
	 Data_valid = *read_flash;
	 daily_rprt.Start_load_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.Start_load_time.Min = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.Start_load_time.Sec = (Data_valid & 0x00FF);
	 read_flash++;
	 read_flash++;*/
	 
	 //start load time
	 Data_valid = *read_flash;
	 daily_rprt.Start_load_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.Start_load_time.AM_PM = (Data_valid & 0xFF00) >> 8;	//DO not use AM/PM variable for daily_rprt.Start_load_time
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.Start_load_time.Min = (Data_valid & 0x00FF);
	 daily_rprt.Start_load_time.Sec = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 read_flash++;						 
	 read_flash++;
	 
/*						 //end load time
	 Data_valid = *read_flash;
	 daily_rprt.End_load_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.End_load_time.Min = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.End_load_time.Sec = (Data_valid & 0x00FF);
	 read_flash++;
	 read_flash++;*/
	 
	 //end load time
	 Data_valid = *read_flash;
	 daily_rprt.End_load_time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.End_load_time.AM_PM = (Data_valid & 0xFF00) >> 8;	//DO not use AM/PM variable for daily_rprt.End_load_time
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.End_load_time.Min = (Data_valid & 0x00FF);
	 daily_rprt.End_load_time.Sec = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 read_flash++;						 
	 read_flash++;
	 
	 //run time
	 for(j=0; j<sizeof(daily_rprt.Run_time); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 daily_rprt.Run_time = buffer_long.ulong_val;
	 
	 //-------------------------weekly----------------------------------
	 //zero speed counter
	 for(j=0; j<sizeof(weekly_rprt.Cnt_zero_speed); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 weekly_rprt.Cnt_zero_speed = buffer_long.ulong_val;
	 
	 //down time
	 for(j=0; j<sizeof(weekly_rprt.Down_time); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 weekly_rprt.Down_time = buffer_long.ulong_val;
	 
	 //run time
	 for(j=0; j<sizeof(weekly_rprt.Run_time); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 weekly_rprt.Run_time = buffer_long.ulong_val;
	 
	 //-----------------------monthly-------------------
	 //zero speed counter
	 for(j=0; j<sizeof(monthly_rprt.Cnt_zero_speed); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 monthly_rprt.Cnt_zero_speed = buffer_long.ulong_val;
	 
	 //down time
	 for(j=0; j<sizeof(monthly_rprt.Down_time); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 monthly_rprt.Down_time = buffer_long.ulong_val;
	 
	 //run time
	 for(j=0; j<sizeof(monthly_rprt.Run_time); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 monthly_rprt.Run_time = buffer_long.ulong_val;

	 for(j=0; j<sizeof(monthly_rprt.Run_time); j++)
	 {
		 Data_valid = *read_flash;
		 buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		 j++;
		 buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		 read_flash++;
	 }
	 monthly_rprt.Run_time = buffer_long.ulong_val;
		
	 //Daily Report Header Date
	 Data_valid =*read_flash;
	 daily_rprt.freq_rprt_header.Date.Year = Data_valid;
	 read_flash++;
	 Data_valid =*read_flash;						 
	 daily_rprt.freq_rprt_header.Date.Mon = (Data_valid & 0x00FF);
	 daily_rprt.freq_rprt_header.Date.Day = (Data_valid & 0xFF00) >> 8;
	 read_flash++;

	 //Weekly Report Header Date
	 Data_valid =*read_flash;
	 weekly_rprt.freq_rprt_header.Date.Year = Data_valid;
	 read_flash++;
	 Data_valid =*read_flash;						 
	 weekly_rprt.freq_rprt_header.Date.Mon = (Data_valid & 0x00FF);
	 weekly_rprt.freq_rprt_header.Date.Day = (Data_valid & 0xFF00) >> 8;
	 read_flash++;						 
	 

	 //Monthly Report Header Date
	 Data_valid =*read_flash;
	 monthly_rprt.freq_rprt_header.Date.Year = Data_valid;
	 read_flash++;
	 Data_valid =*read_flash;						 
	 monthly_rprt.freq_rprt_header.Date.Mon = (Data_valid & 0x00FF);
	 monthly_rprt.freq_rprt_header.Date.Day = (Data_valid & 0xFF00) >> 8;
	 read_flash++;

/*						//Daily Report Header Time
	 Data_valid = *read_flash;
	 daily_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.freq_rprt_header.Time.Min = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
// 						 Data_valid = *read_flash;
// 						 daily_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0x00FF);
// 						 read_flash++;	 */

	//Daily Report Header Time
	 Data_valid = *read_flash;
	 daily_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
	 daily_rprt.freq_rprt_header.Time.AM_PM = (Data_valid & 0xFF00) >> 8;	//DO not use AM/PM variable for daily_rprt.freq_rprt_header
	 read_flash++;
	 Data_valid = *read_flash;
	 daily_rprt.freq_rprt_header.Time.Min = (Data_valid & 0x00FF);
	 daily_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0xFF00) >> 8;
	 read_flash++;	
	 
/*						 //Weekly Report Header Time
	 Data_valid = *read_flash;
	 weekly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
	 weekly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
// 						 Data_valid = *read_flash;
// 						 weekly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0x00FF);
// 						 read_flash++;*/

	 //Weekly Report Header Time
	 Data_valid = *read_flash;
	 weekly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
	 weekly_rprt.freq_rprt_header.Time.AM_PM = (Data_valid & 0xFF00) >> 8;	//DO not use AM/PM variable for weekly_rprt.freq_rprt_header
	 read_flash++;
	 Data_valid = *read_flash;
	 weekly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0x00FF);
	 weekly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 
/*						 //Monthly Report Header Time
	 Data_valid = *read_flash;
	 monthly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
	 monthly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
// 						 Data_valid = *read_flash;
// 						 monthly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0x00FF);
// 						 read_flash++;*/

	 //Monthly Report Header Time
	 Data_valid = *read_flash;
	 monthly_rprt.freq_rprt_header.Time.Hr = (Data_valid & 0x00FF);
	 monthly_rprt.freq_rprt_header.Time.AM_PM = (Data_valid & 0xFF00) >> 8;	//DO not use AM/PM variable for monthly_rprt.freq_rprt_header
	 read_flash++;
	 Data_valid = *read_flash;
	 monthly_rprt.freq_rprt_header.Time.Min = (Data_valid & 0x00FF);
	 monthly_rprt.freq_rprt_header.Time.Sec = (Data_valid & 0xFF00) >> 8;
	 read_flash++;
	 
	 Data_valid = *read_flash;
	 daily_rprt.Dummy_var1 = (Data_valid);
	 read_flash++;	

	 Data_valid = *read_flash;	
	// periodic_data.Periodic_hdr.Id = Data_valid;
	 periodic_data.Periodic_hdr.Id = LPC_RTC->GPREG2;
	 read_flash++;	
//sks record id
	Data_valid = *read_flash;
	set_zero_data_log.Set_zero_hdr.Id = Data_valid; 
	read_flash++;		 

	Data_valid = *read_flash;
	clear_data_log.Clr_data_hdr.Id = Data_valid;
	read_flash++;	

	Data_valid = *read_flash;
	calib_data_log.Cal_data_hdr.Id = Data_valid;
	read_flash++;		 

	Data_valid = *read_flash;
	length_cal_data_log.Len_data_hdr.Id = Data_valid;
	read_flash++;	

	Data_valid = *read_flash;
	//error_data_log.Err_data_hdr.Id = Data_valid; //Commented 16-Feb-2016
	read_flash++;	
	
	//Run time 
	for(j=0; j<sizeof(Calculation_struct.run_time); j++)
	{
		Data_valid = *read_flash;
		buffer_float.f_array[j] = (Data_valid & 0x00FF);
		j++;
		buffer_float.f_array[j] = (Data_valid & 0xFF00) >> 8;
		read_flash++;
	}
	Calculation_struct.run_time = buffer_float.float_val;				
	
	//Scale setup weight unit store in Totals_var.Weight_unit
	for(j=0; j<sizeof(Scale_setup_var.Weight_unit); j++)
	{
		Data_valid = *read_flash;
		buffer_long.ulong_array[j] = (Data_valid & 0x00FF);
		j++;
		buffer_long.ulong_array[j] = (Data_valid & 0xFF00) >> 8;
		read_flash++;
	}
	Totals_var.Weight_unit = buffer_long.ulong_val;	
  
	//PVK
	//Read CRC from end of the block
	Block_Length = EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH;
	read_flash = (U16 *)(addr1 + (Block_Length-4)); 
	CalcCRC((unsigned char*)addr, (unsigned char*)&Calculated_CRC,(Block_Length-4));
	
	//PVK - 11 Jan 2015
	gTotals_Calculated_CRC = Calculated_CRC;
	
	//Check if calculated CRC matches with store CRC if yes return 1 else 0
	if(*read_flash == Calculated_CRC)
	{
		 return(1);
  }
  else
  { 
    return(0);
  }		
	
}


/*****************************************************************************
* @note       Function name: U32 flash_para_record_search(U32 addr)
* @returns    returns		   : addr - Record address
* @param      arg1			   : addr - Flash start address
* @author			             : Megha Gaikwad
* @date       date created : Aug 11, 2015
* @brief      Description	 : Read the record number from the flash and point to the latest  
														 Parametres records. Parameters will be All totals, Daily report, 
														 weekly report and monthly report parameters, all header IDs
* @note       Notes		     : None
*****************************************************************************/
U32 flash_para_record_search(U32 addr)
{
	U16 *read_flash;
	U16 Data_valid;
	U16 *Temp_read_flash;
	U16 Temp_Data_valid;
	U16 *read_flash_addr, *Temp_FlashStartAdd;
	int i;
	U32 addr1;
	U16 Block_Length;
	
#if 0
	FILE *fp;
#endif
	
#if 0
	fp = fopen ("Para.txt", "a");
	if(fp != NULL)
	{
		fprintf(fp, "%s\r\n", "Record search");
		fclose(fp);
	}
#endif
	
  //PVK
	//Check for data validity 
 	Temp_read_flash = (U16 *)EXT_FLASH_PARA_BACKUP_COPY_START_ADDR;
	Temp_Data_valid = *Temp_read_flash;	
	read_flash = (U16 *)addr;
	Data_valid = *read_flash;
	Block_Length = (EXT_FLASH_PAGE_SIZE_BYTES*4);
	
	if((g_old_fw_ver >= (float)4.19) || (!((g_old_fw_ver >= (float)0.90) && (g_old_fw_ver <= (float)0.99))))
	{
		//if working copy is not erased then take data from working copy else check for backup copy
		if(Data_valid != 0xFFFF)
		{
			read_flash = (U16 *)addr;
			Data_valid = *read_flash;
		}
		else
		{	
			if(Temp_Data_valid != 0xFFFF)
			{	
				//PVK - Copy data from backup to working copy
				Flash_Write_In_progress = 1; //Added on 27 April 2016  
				ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_START_ADDR);
				Flash_Write_In_progress = 0; //Added on 27 April 2016  
				Temp_FlashStartAdd = (U16 *)EXT_FLASH_PARA_BACKUP_START_ADDR;
				addr1 = EXT_FLASH_PARA_BACKUP_COPY_START_ADDR;
				read_flash_addr = (U16 *)addr1;
			
				//Take backup copy of data
				for (i=0; i<(Block_Length/2); i++)
				{
					//Read data from current flash and wirte it into backup copy
					Flash_Write_In_progress = 1; //Added on 27 April 2016  
					ExtFlash_writeWord((uint32_t)Temp_FlashStartAdd, *read_flash_addr);
					Flash_Write_In_progress = 0; //Added on 27 April 2016  
					read_flash_addr++;
					Temp_FlashStartAdd++;
				}	
				//Advance the address by block size
				addr = (EXT_FLASH_PARA_BACKUP_START_ADDR + (EXT_FLASH_PAGE_SIZE_BYTES*4));
				//addr = EXT_FLASH_PARA_BACKUP_START_ADDR;
				//record_no = 1;
				read_flash = (U16 *)addr;
				Data_valid = *read_flash;
				//Advance the address by block size
				//addr = (EXT_FLASH_PARA_BACKUP_START_ADDR + (EXT_FLASH_PAGE_SIZE_BYTES*4));
			}
			else
			{
				//If both sector are erased then take start address of working copy
				read_flash = (U16 *)EXT_FLASH_PARA_BACKUP_START_ADDR;
				Data_valid = *read_flash;
			}			
		}	
	}
	
	while(Data_valid != 0xFFFF)
	{	
#if 0
		fp = fopen ("Para.txt", "a");
		if(fp != NULL)
		{
			fprintf(fp,"%d,%d\r\n",addr,Data_valid);
			fclose(fp);
		}	
#endif
						
		addr = (addr + (EXT_FLASH_PAGE_SIZE_BYTES*4));
		
		//Added PVK Mar 2016 
		if(addr > EXT_FLASH_PARA_BACKUP_END_ADDR)
		{
      break;
    } 
		read_flash = (U16 *)addr;
		Data_valid = *read_flash;
	}
	
	//Check address range
	if(addr > EXT_FLASH_PARA_BACKUP_END_ADDR)
	{
		addr = EXT_FLASH_PARA_BACKUP_START_ADDR;
		flash_para_addr = 0;
		flash_para_record_no = 0;
	}
	else if(addr == EXT_FLASH_PARA_BACKUP_START_ADDR)
	{
		addr = EXT_FLASH_PARA_BACKUP_START_ADDR;
		flash_para_addr = 0;
		flash_para_record_no = 0;
	}
	else
	{
		addr = (addr - (EXT_FLASH_PAGE_SIZE_BYTES*4));
		read_flash = (U16 *)addr;
		Data_valid = *read_flash;	
		flash_para_addr = addr;
		flash_para_record_no = Data_valid;
	}

#if 0
	fp = fopen ("Para.txt", "a");
	if(fp != NULL)
	{
		fprintf(fp,"%d,%d\r\n",flash_para_addr,flash_para_record_no);
		fclose(fp);
	}	
#endif
						
	return addr;

}

/*****************************************************************************
* @note       Function name: U16 flash_para_record_flag(U8 type)
* @returns    returns		   : 0 - for set operation
														 data - for get operation
* @param      arg1			   : None
* @author			             : Megha Gaikwad
* @date       date created : Aug 27, 2015
* @brief      Description	 : Write/read flag to/from flash address.
* @note       Notes		     : This flag will be written to flash after writing periodic 
														 data into file. After soft reset this flag will be read.
*****************************************************************************/
U16 flash_para_record_flag(U8 type)
{

#if 0	
	U16 * read_flash_addr;
	U16 Data_valid;
	uint32_t addr;

#if 0
	FILE *fp;
#endif
	
	addr = flash_para_addr;

	addr = (addr + (EXT_FLASH_PAGE_SIZE_BYTES*4) - 2);			// Last address of record region
	read_flash_addr = (U16 *)addr;	
	
	if(type == SET_FLAG)
	{
		ExtFlash_writeWord((uint32_t)read_flash_addr, SET);
	
#if 0
		fp = fopen ("Para.txt", "a");
		if(fp != NULL)
		{
			fprintf(fp, "%s\r\n", "File write");
			fprintf(fp,"%d\r\n",addr);
			fclose(fp);
		}
#endif		
		
	}
	else
	{
		Data_valid = *read_flash_addr;		
		return Data_valid;
		//return 0;
	}
	#endif 
	
	return 0;
}

/*****************************************************************************
* @note       Function name: void flash_clear_weight_backup(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Megha Gaikwad
* @date       date created : Sep 29, 2015
* @brief      Description	 : Writes the backup of the last 8 clear weight parameters 
														 to the flash after firmware upgrade to keep records
* @note       Notes		     : Megha Gaikwad updated this function on 18-Nov-2015
														 for cleared weight unit and rate unit
*****************************************************************************/
void flash_clear_weight_backup(void)
{
}
// {
// 	U16 * u16Var;
// 	U8 success;
// 	U16 * read_flash;
// 	U16 i;
// 	
// 	Flash_Write_In_progress = 1; //Added on 27 April 2016  
// 	if (ExtFlash_eraseBlock(GUI_PARAM_SECTOR_126_START_ADDR))
// 	{	
// 		/*Clear weight parameters in REPORTS structure*/
// 		read_flash = (U16*)RPRTS_DIAG_CLEAR_WEIGHT_ADDR;
// 		u16Var = (U16*)&Rprts_diag_var.LastClearedTotal[0];
// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var.LastClearedTotal); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}
// 		
// 		u16Var = (U16*)&Rprts_diag_var.Avg_rate_for_mode[0];
// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var.Avg_rate_for_mode); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}

// 		u16Var = (U16*)&Rprts_diag_var.RunTime[0];
// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var.RunTime); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}	
// 		
// 		u16Var = (U16*)&Rprts_diag_var.Clear_Date[0];
// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var.Clear_Date); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}	

// 		u16Var = (U16*)&Rprts_diag_var.Clear_Time[0];
// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var.Clear_Time); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}		
// 		
// 		u16Var = (U16*)&Rprts_diag_var.LastCleared_Weight_unit[0];
// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var.LastCleared_Weight_unit); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}

// 		u16Var = (U16*)&Rprts_diag_var.LastCleared_Rate_time_unit[0];
// 		for(i=0; i<FLASH_SIZEOF(Rprts_diag_var.LastCleared_Rate_time_unit); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}		
// 	}
// 	Flash_Write_In_progress = 0; //Added on 27 April 2016  
// }

/*****************************************************************************
* @note       Function name: void EEP_FL_create_backup (void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Creates a backup copy of the EEPROM config data
														 into flash memory
* @note       Notes		     : 
*****************************************************************************/
void EEP_FL_create_backup (void)
{
}
// {
// 	U16 * u16Var;
// 	U16 i;
// 	U16 *Current_write_addr_ptr;
// 	
// 	#ifdef DEBUG_PORT
// 	g_buf[12] = 'T';
// 	g_buf[13] = '4';
// 	g_buf[14] = ' ';
// 	#endif
// 	
// 	Flash_Write_In_progress = 1; //Added on 27 April 2016  
// 	
// 	ExtFlash_eraseBlock(SECTOR_123_START_ADDR);
// 	
// 	#ifdef DEBUG_PORT
// 	g_buf[15] = 'T';
// 	g_buf[16] = '5';
// 	g_buf[17] = ' ';
// 	#endif
// 	
// 	//Update the CRC first in RAM copy
// 	CalcCRC((unsigned char*)&Scale_setup_var, (unsigned char*)&Scale_setup_var.CRC_SCALE_SETUP, (unsigned int)offsetof(SCALE_SETUP_STRUCT, CRC_SCALE_SETUP));
// 	Current_write_addr_ptr = (U16 *)EEP_FL_SCALE_SETUP_START;
// 	u16Var = (U16*)&Scale_setup_var;
// 	for(i=0; i<(FLASH_SIZEOF(Scale_setup_var)); i++)
// 	{
// 		ExtFlash_writeWord((uint32_t)Current_write_addr_ptr, *u16Var++);
// 		Current_write_addr_ptr++;
// 	}
//   

// 	//Update the CRC first in RAM copy
// 	CalcCRC((unsigned char*)&Calibration_var, (unsigned char*)&Calibration_var.CRC_CALIBRATION_STRUCT, (unsigned int)offsetof(CALIBRATION_STRUCT, CRC_CALIBRATION_STRUCT));
// 	Current_write_addr_ptr = (U16 *)EEP_FL_CALIBRATION_START;
// 	u16Var = (U16*)&Calibration_var;
// 	for(i=0; i<(FLASH_SIZEOF(Calibration_var)); i++)
// 	{
// 		ExtFlash_writeWord((uint32_t)Current_write_addr_ptr, *u16Var++);
// 		Current_write_addr_ptr++;
// 	}

// 	//Update the CRC first in RAM copy
// 	CalcCRC((unsigned char*)&Setup_device_var, (unsigned char*)&Setup_device_var.CRC_SETUP_DEVICES, (unsigned int)offsetof(SETUP_DEVICES_STRUCT, CRC_SETUP_DEVICES));
// 	Current_write_addr_ptr = (U16 *)EEP_FL_SETUP_DEVICE_START;
// 	u16Var = (U16*)&Setup_device_var;
// 	for(i=0; i<(FLASH_SIZEOF(Setup_device_var)); i++)
// 	{
// 		ExtFlash_writeWord((uint32_t)Current_write_addr_ptr, *u16Var++);
// 		Current_write_addr_ptr++;
// 	}
// 	
// 	//Update the CRC first in RAM copy
// 	CalcCRC((unsigned char*)&Admin_var, (unsigned char*)&Admin_var.CRC_ADMIN, (unsigned int)offsetof(ADMIN_STRUCT, CRC_ADMIN));
// 	Current_write_addr_ptr = (U16 *)EEP_FL_ADMIN_START;
// 	u16Var = (U16*)&Admin_var;
// 	for(i=0; i<(FLASH_SIZEOF(Admin_var)); i++)
// 	{
// 		ExtFlash_writeWord((uint32_t)Current_write_addr_ptr, *u16Var++);
// 		Current_write_addr_ptr++;
// 	}
// 	Current_write_addr_ptr = (U16 *)EEP_FL_MISC_START;
// 	u16Var = (U16*)&Misc_var;
// 	for(i=0; i<(FLASH_SIZEOF(Misc_var)); i++)
// 	{
// 		ExtFlash_writeWord((uint32_t)Current_write_addr_ptr, *u16Var++);
// 		Current_write_addr_ptr++;
// 	}	
// 	Flash_Write_In_progress = 0; //Added on 27 April 2016  
// }

/*****************************************************************************
* @note       Function name: U8 EEP_FL_chk_FL_data_validity (void)
* @returns    returns		   : status (valid-1/invalid-0)
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Checks the flash based backup copy's data validity
* @note       Notes		     : 
*****************************************************************************/
U8 EEP_FL_chk_FL_data_validity (void)
{
	U16 calculated_CRC = 0;
	U16 stored_CRC = 0;
	U8 data_buf[1000];
	U8 status = 0;
	
	memcpy((U16 *)&data_buf, (U16*)EEP_FL_SCALE_SETUP_START, offsetof(SCALE_SETUP_STRUCT, CRC_SCALE_SETUP));
	CalcCRC((unsigned char*)data_buf, (unsigned char*)&calculated_CRC, (unsigned int)offsetof(SCALE_SETUP_STRUCT, CRC_SCALE_SETUP));
	stored_CRC = (U16)*(U16*)(EEP_FL_SCALE_SETUP_START + offsetof(SCALE_SETUP_STRUCT, CRC_SCALE_SETUP));
	
	if (calculated_CRC != stored_CRC)
	{
		status = 0;
	}
	else
	{
		memcpy((U16 *)&data_buf, (U16*)EEP_FL_CALIBRATION_START, offsetof(CALIBRATION_STRUCT, CRC_CALIBRATION_STRUCT));
		CalcCRC((unsigned char*)data_buf, (unsigned char*)&calculated_CRC, (unsigned int)offsetof(CALIBRATION_STRUCT, CRC_CALIBRATION_STRUCT));
		stored_CRC = (U16)*(U16*)(EEP_FL_CALIBRATION_START + offsetof(CALIBRATION_STRUCT, CRC_CALIBRATION_STRUCT));
		if (calculated_CRC != stored_CRC)
		{
			status = 0;
		}
		else
		{
			memcpy((U16 *)&data_buf, (U16*)EEP_FL_SETUP_DEVICE_START, offsetof(SETUP_DEVICES_STRUCT, CRC_SETUP_DEVICES));
			CalcCRC((unsigned char*)data_buf, (unsigned char*)&calculated_CRC, (unsigned int)offsetof(SETUP_DEVICES_STRUCT, CRC_SETUP_DEVICES));
			stored_CRC = (U16)*(U16*)(EEP_FL_SETUP_DEVICE_START + offsetof(SETUP_DEVICES_STRUCT, CRC_SETUP_DEVICES));
			if (calculated_CRC != stored_CRC)
			{
				status = 0;
			}
			else
			{
				memcpy((U16 *)&data_buf, (U16*)EEP_FL_ADMIN_START, offsetof(ADMIN_STRUCT, CRC_ADMIN));
				CalcCRC((unsigned char*)data_buf, (unsigned char*)&calculated_CRC, (unsigned int)offsetof(ADMIN_STRUCT, CRC_ADMIN));
				stored_CRC = (U16)*(U16*)(EEP_FL_ADMIN_START + offsetof(ADMIN_STRUCT, CRC_ADMIN));
				if (calculated_CRC != stored_CRC)
				{
					status = 0;
				}
				else
				{
					status = 1;
				}
			}
		}
	}
	return status;	
}

/*****************************************************************************
* @note       Function name: void CalcCRC(unsigned char* pTargetData, 
														 unsigned char* pTargetCrc, unsigned int msg_len)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Calculates the 16-bit CRC using the table
* @note       Notes		     : 
*****************************************************************************/
void CalcCRC(unsigned char* pTargetData, unsigned char* pTargetCrc, unsigned int msg_len)
{
  unsigned char CrcHigh, CrcLow, Temp;
  unsigned int Length;
  
  Length = msg_len; 
  CrcLow = 0xFF;
  CrcHigh = 0xFF;

  while(Length)
  {
    Temp = CrcHigh;
    CrcHigh = (unsigned char)(crc_table[2 * ((*pTargetData) ^ CrcLow)]);
    CrcLow = (unsigned char)(Temp ^ crc_table[(2 * ((*pTargetData) ^ CrcLow)) + 1]);
    pTargetData++;
    Length--;
  };

  *pTargetCrc = CrcLow;
  *(pTargetCrc + 1) = CrcHigh;
}

/*****************************************************************************
* @note       Function name: void load_good_Clr_wt_data(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : December 29, 2015
* @brief      Description	 : Reloads the last 8 historical weight data from the flash 
															to the Rprts_diag_var structure
* @note       Notes		     : None
*****************************************************************************/
void load_good_Clr_wt_data(void)
{	
	U8 uc_Clr_Wt_Org_data = 0;
	U8 uc_Clr_Wt_BckUp_data = 0;
	
	//Validate the Original data without updating the struct in RAM
	uc_Clr_Wt_Org_data = Flash_ClrWt_Data_Validity_Org();
	//Validate the BackUp data without updating the struct in RAM
	uc_Clr_Wt_BckUp_data = Flash_ClrWt_Data_Validity_Bkp();
	
	if(uc_Clr_Wt_Org_data)
	{
		//Copy the Original Data into RAM
		Flash_ClrWt_Org_to_RAM();
		
		//PVK - 10- Feb -2016 
	  //Verify All Historical Totals
		Verify_Historical_Totals();
		if(Historical_Totals_set_to_default_flag == 1)
		{
 			Flash_ClrWt_Write_RAM_to_Org();
			//Calc CRC
		  Flash_ClrWt_Org_to_RAM();
			Flash_ClrWt_Write_RAM_to_Org();
			Flash_ClrWt_Write_RAM_to_Bkp();
			Historical_Totals_set_to_default_flag = 0;
    }	
		
		if(!uc_Clr_Wt_BckUp_data)
		{
			//Copy the Original Data into Backup location since Backup was corrupted
			Flash_ClrWt_Write_RAM_to_Bkp();
		}
				
	}
	else
	{
		if(uc_Clr_Wt_BckUp_data)
		{
			//Copy the Back Up Data into RAM
			Flash_ClrWt_Bkp_to_RAM();
			
			//PVK - 10- Feb -2016 
			//Verify All Historical Totals
			Verify_Historical_Totals();
			if(Historical_Totals_set_to_default_flag == 1)
			{
				Flash_ClrWt_Write_RAM_to_Org();
			  //Calc CRC
		    Flash_ClrWt_Org_to_RAM();
				Flash_ClrWt_Write_RAM_to_Bkp();
				Flash_ClrWt_Write_RAM_to_Org();
				Historical_Totals_set_to_default_flag = 0;
			}	
			
			//Original copy is corrupted, load from Backup and Copy the Backup into Original data which is now corrupted
			Flash_ClrWt_Write_RAM_to_Org();
		}
		else
		{
			//PVK - 10- Feb -2016 - Modified
			//if(g_old_fw_ver < (float)4.20) 
			if((g_old_fw_ver >= (float)4.20) || (!((g_old_fw_ver >= (float)0.90) && (g_old_fw_ver <= (float)0.99))))
			{
				Clear_weight_load_Default();
			  //PVK - 10- Feb -2016 
				//Verify All Historical Totals
			  Verify_Historical_Totals();
				
				if(Historical_Totals_set_to_default_flag == 1)
			  {
					Flash_ClrWt_Write_RAM_to_Org();
					//Calc CRC
					Flash_ClrWt_Org_to_RAM();
			  }	
			  Flash_ClrWt_Write_RAM_to_Bkp();
				Flash_ClrWt_Write_RAM_to_Org();
				
				Historical_Totals_set_to_default_flag = 0;
			}
			else
			{
				//Copy the Original Data into RAM
				Flash_ClrWt_Org_to_RAM();
			  //PVK - 10- Feb -2016  
			  //Verify All Historical Totals
			  Verify_Historical_Totals();
			  if(Historical_Totals_set_to_default_flag == 1)
			  {
					Flash_ClrWt_Write_RAM_to_Org();
					//Calc CRC
					Flash_ClrWt_Org_to_RAM();
				}	
				
				Flash_ClrWt_Write_RAM_to_Bkp();
				Flash_ClrWt_Write_RAM_to_Org();
			
			  Historical_Totals_set_to_default_flag = 0;
			}
			
			/*
			//PVK - 10- Feb -2016 commented
			if(g_old_fw_ver < (float)4.20) 
			{
				//Copy the Original Data into RAM
				Flash_ClrWt_Org_to_RAM();
			  //PVK - 10- Feb -2016  
			  //Verify All Historical Totals
			  Verify_Historical_Totals();
			  
				Flash_ClrWt_Write_RAM_to_Bkp();
				Flash_ClrWt_Write_RAM_to_Org();
			
			  Historical_Totals_set_to_default_flag = 0;
			}
			else
			{
				Clear_weight_load_Default();
			  //PVK - 10- Feb -2016 
				//Verify All Historical Totals
			  Verify_Historical_Totals();
				
			  Flash_ClrWt_Write_RAM_to_Bkp();
				Flash_ClrWt_Write_RAM_to_Org();
				
				Historical_Totals_set_to_default_flag = 0;
			}
			*/
		}
	}
	//PVK - 14 Jan 2016
	//Upto 4.19(previous version) the formula used for calculating Avg rate was wrong and it is corrected
	//from 4.20 version
	Calculate_Clr_Wt_Screen_Avg_Rate();
}

/*****************************************************************************
* @note       Function name: void Calculate_Clr_Wt_Screen_Avg_Rate(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Oaces Team
* @date       date created : 14 January, 2016
* @brief      Description	 : This function calculates the Avg Rate bsed on total weight,
*                            run time and rate unit
* @note       Notes		     : None
*****************************************************************************/
void Calculate_Clr_Wt_Screen_Avg_Rate(void)
{
  int i;
  if(g_old_fw_ver < (float)4.20)
	{
		for(i=0;i<8;i++)
		{
			if(Rprts_diag_var.RunTime[i] == 0)
			{
				Rprts_diag_var.Avg_rate_for_mode[i] = 0;
			}
			else
			{	
			
				if (Rprts_diag_var.LastCleared_Rate_time_unit[i] == Screen26_str2)//hours
				{	 
					 Rprts_diag_var.Avg_rate_for_mode[i] = ((Rprts_diag_var.LastClearedTotal[i]/Rprts_diag_var.RunTime[i]) * 60.0);
				}
				else//Min
				{	 
					 Rprts_diag_var.Avg_rate_for_mode[i] = (Rprts_diag_var.LastClearedTotal[i]/Rprts_diag_var.RunTime[i]);
				}
			}	
		}	
	}	
}	
/*****************************************************************************
* @note       Function name: unsigned int Flash_ClrWt_Data_Validity_Org(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : January 04, 2016
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
unsigned int Flash_ClrWt_Data_Validity_Org(void)
{
	U16 * read_flash;	
	U16 u16_CRC_local = 0xFFFF;	
	U16 u16_CRC_stored = 0xFFFF;
	U8 data_buf[1000];
	U8 status = 0;
	
	memset(data_buf, 0Xff, sizeof(data_buf));
	
	//Retrieve data and put into the structure
	read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_ADDR;
	memcpy((U16 *)&data_buf, read_flash, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
	
	//Retrieve CRC
	read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_CRC_ADDR;
	memcpy((U16 *)&u16_CRC_stored, read_flash, sizeof(U16));
	
	//calculate CRC of the data
	CalcCRC((unsigned char*)&data_buf, (unsigned char*)&u16_CRC_local, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
	
	if(u16_CRC_local != u16_CRC_stored)
	{
		status = 0;
	}
	else
	{
		status = 1;
	}
	return status;
}

/*****************************************************************************
* @note       Function name: unsigned int Flash_ClrWt_Data_Validity_Bkp(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : January 04, 2016
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
unsigned int Flash_ClrWt_Data_Validity_Bkp(void)
{
	U16 * read_flash;	
	U16 u16_CRC_local = 0xFFFF;	
	U16 u16_CRC_stored = 0xFFFF;
	U8 data_buf[1000];
	U8 status = 0;
	
	memset(data_buf, 0Xff, sizeof(data_buf));
	
	//Retrieve data and put into the structure
	read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_ADDR;
	memcpy((U16 *)&data_buf, read_flash, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
	
	//Retrieve CRC
	read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_CRC_ADDR;
	memcpy((U16 *)&u16_CRC_stored, read_flash, sizeof(U16));
	
	//calculate CRC of the data
	CalcCRC((unsigned char*)&data_buf, (unsigned char*)&u16_CRC_local, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
	
	if(u16_CRC_local != u16_CRC_stored)
	{
		status = 0;
	}
	else
	{
		status = 1;
	}
	return status;
}

/*****************************************************************************
* @note       Function name: void Flash_ClrWt_Org_to_RAM(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : January 04, 2016
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
void Flash_ClrWt_Org_to_RAM(void)
{
	U16 * read_flash;
	int i = 0;
//	int temp=0;
	
	//Retrieve data and put into the structure
	read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_ADDR;
	//temp = SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT;
	memcpy((U16 *)Rprts_diag_var.LastClearedTotal, (U16 *)read_flash, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
		
	for(i = 0; i < 8 ; i++)
	{
		if(
				(Rprts_diag_var.LastCleared_Weight_unit[i] != TONS)
				&& (Rprts_diag_var.LastCleared_Weight_unit[i] != LONG_TON)
				&& (Rprts_diag_var.LastCleared_Weight_unit[i] != LBS)
				&& (Rprts_diag_var.LastCleared_Weight_unit[i] != TONNE)
				&& (Rprts_diag_var.LastCleared_Weight_unit[i] != KG)
				
				&& (Rprts_diag_var.LastCleared_Rate_time_unit[i] != Screen26_str2)	//Hours
				&& (Rprts_diag_var.LastCleared_Rate_time_unit[i] != Screen26_str4)		//Minutes
			)
		{
			Rprts_diag_var.LastClearedTotal[i]	= 0;
			Rprts_diag_var.Avg_rate_for_mode[i]	= 0;
			Rprts_diag_var.RunTime[i] 					= 0;
			Rprts_diag_var.Clear_Date[i].Day  	= 0;
			Rprts_diag_var.Clear_Date[i].Mon  	= 0;
			Rprts_diag_var.Clear_Date[i].Year 	= 0;
			Rprts_diag_var.Clear_Time[i].Hr   	= 0;
			Rprts_diag_var.Clear_Time[i].Min  	= 0;
			Rprts_diag_var.Clear_Time[i].Sec  	= 0;
			Rprts_diag_var.Clear_Time[i].AM_PM 	= 0; //AM_TIME_FORMAT;
			Rprts_diag_var.LastCleared_Weight_unit[i] = TONS;
			Rprts_diag_var.LastCleared_Rate_time_unit[i] = Screen26_str2;
		}
	}
	
	//Always calculate CRC over the Last 8 Cleared Weight values and units
	CalcCRC((unsigned char*)&Rprts_diag_var.LastClearedTotal[0], (unsigned char*)&g_Clr_wt_CRC, (U16)SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
}

/*****************************************************************************
* @note       Function name: void Flash_ClrWt_Write_RAM_to_Org(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : December 29, 2015
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
void Flash_ClrWt_Write_RAM_to_Org(void)
{
}
// {
// 	U16 * u16Var;
// 	U8 success;
// 	U16 * read_flash;
// 	U16 i;
// 	//U16 u16_CRC = 0xFFFF;
// 	
// 	Flash_Write_In_progress = 1; //Added on 27 April 2016  
// 	
// 	if (ExtFlash_eraseBlock(RPRTS_DIAG_CLEAR_WEIGHT_ADDR))
// 	{
// 		//Set the pointer to point to the Clear Weight Backup Data Location on Flash
// 		read_flash = (U16*)RPRTS_DIAG_CLEAR_WEIGHT_ADDR;
// 		//Load the variable with starting address of the CLear weight data in Rprts_diag_var structure
// 		u16Var = (U16*)&Rprts_diag_var.LastClearedTotal[0];
// 		//Write the Backup Data in Flash
// 		for(i=0; i<(SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT/2); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}
// 		
// 		//Calculate CRC over the Last 8 Cleared Weight values and units
// 		//CalcCRC((unsigned char*)&Rprts_diag_var.LastClearedTotal[0], (unsigned char*)&u16_CRC, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
// 		//Set the pointer to point to the location of CRC of the Backup data on Flash
// 		read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_CRC_ADDR;
// 		//Write the CRC in the Flash
// 		ExtFlash_writeWord((uint32_t)read_flash, g_Clr_wt_CRC);
// 	}
// 	Flash_Write_In_progress = 0; //Added on 27 April 2016  
// }

/*****************************************************************************
* @note       Function name: void Flash_ClrWt_Bkp_to_RAM(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : January 04, 2016
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
void Flash_ClrWt_Bkp_to_RAM(void)
{
	U16 * read_flash;
	
	//Retrieve data and put into the structure
	read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_ADDR;
	memcpy((U16 *)&Rprts_diag_var.LastClearedTotal[0], read_flash, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
	
	//Retrieve CRC of the valid data from the Flash and update the global Clear weight CRC variable
	read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_CRC_ADDR;
	memcpy((U16 *)&g_Clr_wt_CRC, read_flash, sizeof(unsigned short));
}

/*****************************************************************************
* @note       Function name: void Flash_ClrWt_Write_RAM_to_BackUp(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : December 29, 2015
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
void Flash_ClrWt_Write_RAM_to_Bkp(void)
{
}
// {
// 	U16 * u16Var;
// 	U8 success;
// 	U16 * read_flash;
// 	U16 i;
// 	//U16 u16_CRC = 0xFFFF;
// 	Flash_Write_In_progress = 1; //Added on 27 April 2016  
// 	if (ExtFlash_eraseBlock(RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_ADDR))
// 	{
// 		//Save the Back up of the same at a different location 0x8060 0000
// 		//Set the pointer to point to the Clear Weight Backup Data Location on Flash
// 		read_flash = (U16*)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_ADDR;
// 		//Load the variable with starting address of the CLear weight data in Rprts_diag_var structure
// 		u16Var = (U16*)&Rprts_diag_var.LastClearedTotal[0];
// 		//Write the Backup Data in Flash
// 		for(i=0; i<(SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT/2); i++)
// 		{					
// 			success = ExtFlash_writeWord((uint32_t)read_flash, *u16Var++);
// 			read_flash++;
// 			if(success == __FALSE)
// 			{
// 				break;
// 			}
// 		}
// 		
// 		//Calculate CRC over the Last 8 Cleared Weight values and units
// 		//CalcCRC((unsigned char*)&Rprts_diag_var.LastClearedTotal[0], (unsigned char*)&u16_CRC, SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);
// 		//Set the pointer to point to the location of CRC of the Backup data on Flash
// 		read_flash = (U16 *)RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_CRC_ADDR;
// 		//Write the CRC in the Flash
// 		ExtFlash_writeWord((uint32_t)read_flash, g_Clr_wt_CRC);
// 	}
// 	Flash_Write_In_progress = 0; //Added on 27 April 2016  
// }

/*****************************************************************************
* @note       Function name: void Clear_weight_load_Default(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Prasad Tandale
* @date       date created : January 04, 2016
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
void Clear_weight_load_Default(void)
{	
	int i = 0;
	
	for(i = 0; i < 8 ; i++)
	{
		Rprts_diag_var.LastClearedTotal[i]	= 0;
		Rprts_diag_var.Avg_rate_for_mode[i]	= 0;
		Rprts_diag_var.RunTime[i] 					= 0;
		Rprts_diag_var.Clear_Date[i].Day  	= 0;
		Rprts_diag_var.Clear_Date[i].Mon  	= 0;
		Rprts_diag_var.Clear_Date[i].Year 	= 0;
		Rprts_diag_var.Clear_Time[i].Hr   	= 0;
		Rprts_diag_var.Clear_Time[i].Min  	= 0;
		Rprts_diag_var.Clear_Time[i].Sec  	= 0;
		Rprts_diag_var.Clear_Time[i].AM_PM 	= 0;//AM_TIME_FORMAT;
		Rprts_diag_var.LastCleared_Weight_unit[i] = TONS;
		Rprts_diag_var.LastCleared_Rate_time_unit[i] = Screen26_str2;
	}
	
	//Calculate CRC over the Last 8 Cleared Weight values and units
	//CalcCRC((unsigned char*)&Rprts_diag_var.LastClearedTotal[0], (unsigned char*)&g_Clr_wt_CRC, sizeof(unsigned short));
	//PVK-3-Mar-2016
	CalcCRC((unsigned char*)&Rprts_diag_var.LastClearedTotal[0], (unsigned char*)&g_Clr_wt_CRC, (U16)SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT);

}
/*****************************************************************************
* @note       Function name: char Err_data_hdrId_reload(U32 Totals_Current_addr)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Oaces Team
* @date       date created : 16-Feb-2016
* @brief      Description	 : 
* @note       Notes		     : None
*****************************************************************************/
//Added by DK on 16Feb 2016 
char Err_data_hdrId_reload(U32 Totals_Current_addr)
{
  char Status;
	U32 Current_Address;
	U16 Record_Mismatch_Counter = 0;
	
	//uint16_t Temp_data[EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH];
	
	Current_Address = Totals_Current_addr;
	Status = 0;
	
  if((g_old_fw_ver >= (float)4.19) || (!((g_old_fw_ver >= (float)0.90) && (g_old_fw_ver <= (float)0.99))))
	{	
		while(Status == 0)
		{
			Status = Reload_totals_from_flash_with_CRC_Check(Current_Address);
			
			if(Status == 0)
			{	
				Record_Mismatch_Counter++;
				Current_Address = Current_Address - EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH;
				if(Current_Address < EXT_FLASH_PARA_BACKUP_START_ADDR)
				{
					//All blocks CRC not matched then set status to 2
					Status = 2;
					record_no = Record_Mismatch_Counter;
				}
			}  			
		}
		
		if((Status == 1) && (Record_Mismatch_Counter == 0))
		{
				return 1; 						
    }			
		else if((Status == 1) && (Record_Mismatch_Counter != 0))
		{
			 //return 0;
			 return  1;
    }
				
		//All blocks CRC not matched then clear all totals
		if(Status == 2)
		{
		  return 0;
		}//if end		
	}//else end
	else	
	{
		Current_Address = Totals_Current_addr;
		//Restore all totals and records parameter
		Status = Reload_totals_from_flash_with_CRC_Check(Current_Address);	
		//return Status; 
		return 1;
	}
	return 0;
}//Function end

#endif /*#ifdef EXT_FLASH*/
/*****************************************************************************
* End of file
*****************************************************************************/
