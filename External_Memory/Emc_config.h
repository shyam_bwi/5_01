/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Emc_config.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 12, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __EMC_CONFIG_H
#define __EMC_CONFIG_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

#define RB_FLASH_PIN							29
#define SDRAM_BASE_ADDR  					0xA0000000
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void _EMC_Init(void);
extern void DelayMs(U32 ms);

typedef struct debug_modules_tag{
int sdram_error;
int flash_error;
int Delay;
}debug_modules_type;
extern debug_modules_type debug_modules;
#endif /*EMC_CONFIG_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
