/*****************************************************************************
 * @copyright Copyright (c) 2017-2018 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename       : flash_rw.c
 * @brief               : Integrator Board :  Structures Backup and Restore API
 *
 * @author              : Shyam Shrivastava
 *
 * @date Created        : Nov  Friday, 2017  <Nov 03, 2017>
 * @date Last Modified  : ----- -----, 2017  <--- --, 2017>
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal Version No : 4_82
 * @internal DEV ID     : 4_82_1
 *
 *****************************************************************************
||================================================================================||
|| 4 Log types :																																	||
||																																								||
||==========================Flash Memory For storage of Structure ================||
||																																								||
||		||===================1 cyclic buffer====================================||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||  ||periodic Log|| || System Log || ||   Totals   || ||  Variables ||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||  ||p_LogBack1  || ||s_logBack1  || ||   T_Back1  || ||  V_back1   ||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||			...							...								...							...						||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||  ||p_LogBack_n || ||s_logBack_n || ||   T_Back_n || ||  V_back_n  ||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||																																			||	||
||		||======================================================================||	||
||																.......																					||
||		||===================n cyclic buffer====================================||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||  ||periodic Log|| || System Log || ||   Totals   || ||  Variables ||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||  ||p_LogBack1  || ||s_logBack1  || ||   T_Back1  || ||  V_back1   ||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||			...							...								...							...						||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||  ||p_LogBack_n || ||s_logBack_n || ||   T_Back_n || ||  V_back_n  ||	||	||
||		||	||============|| ||============|| ||============|| ||============||	||	||
||		||																																			||	||
||		||======================================================================||	||
||================================================================================||
||   EACH LOG TYPE ABOVE CAN HAVE equal or greater than zero backups.							||
|| 	the same record is written simultaneously in all backup sectors								||
|| 	all records in the backup area of the same original record should have				||
||  same version number																														||
||  all backup sectors should be of the same size of original sector							||
|| 	Each log type has at least one cyclic sector together constituting a cyclic		||
||	buffer .																																			||
||================================================================================||
*/
/*============================================================================
 * Include Header Files
 *===========================================================================*/
#include "flash_rw.h"
#include "Ext_flash_high_level.h"
/*============================================================================
 * Private Macro Definitions
 *===========================================================================*/


/*============================================================================
 * Private Data Types
 *===========================================================================*/

/*============================================================================
 *Public Variables
 *===========================================================================*/
struct_myapi sMyApi  __attribute__ ((section ("STRUCTURES_STACK"))); 													// place the structure on external RAM
/* Constants section */

/*============================================================================
 * Private Function Prototypes Declarations
 *===========================================================================*/


/*****************************************************************************
 * @note       Function name: void myApi_Init(struct_myapi* spMyApiStruct)
 * @returns    returns      : None
 * @param      arg1         : struct_myapi*
 * @author                  : Shyam Shrivastava
 * @date       date created : 03-11-2017
 * @brief      Description  : Initialise API structure in RAM
 * @note       Notes        : should be called only once , at bootup
 *****************************************************************************/
struct_myapi myApi;
void Init_Api_test(void)
{
	myApi_Init(&myApi);
}
void myApi_Init(struct_myapi* spMyApiStruct)																										// Initialise API structure in RAM
{
    sRecordDetails *psHeaderDetails;																														// get a sRecordDetails pointer to traverse Logs
    spMyApiStruct->u32ApiVerNo = BACKUP_RESTORE_API_VER;																				// a hard coded macro definition , helpful
    //if referred along with changelo
    spMyApiStruct->sMemConfig.s_Base_address = NOR_FLASH_BASE_ADDRESS;													// check with schematic
    spMyApiStruct->sMemConfig.sSize_of_sector = SIZE_OF_SECTOR;																	// based on the CHIP part (NOR FLASH)
    spMyApiStruct->sMemConfig.sBaseOffset.periodic_log = BASE_OFFSET_P_LOG;											// starting address of Periodic Log Records
    spMyApiStruct->sMemConfig.sBaseOffset.sys_log = BASE_OFFSET_SYS_LOG;												// starting address of system log records
    spMyApiStruct->sMemConfig.sBaseOffset.totals = BASE_OFFSET_TOTALS;													// starting address of Totals Records
    spMyApiStruct->sMemConfig.sBaseOffset.variables = BASE_OFFSET_VAR;													// starting address of Variables Records
    spMyApiStruct->sMemConfig.sNo_of_Backups.periodic_log = NO_OF_BACKUP_P_LOG;									// NO OF BACKUPS FOR PERIODIC LOGS
    spMyApiStruct->sMemConfig.sNo_of_Backups.sys_log = NO_OF_BACKUP_SYS_LOG;										// no of backups for system log
    spMyApiStruct->sMemConfig.sNo_of_Backups.totals = NO_OF_BACKUP_TOTALS;											// no of backups for totals records
    spMyApiStruct->sMemConfig.sNo_of_Backups.variables = NO_OF_BACKUP_VAR ;											// no of backups for Variable Records
    spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.periodic_log = NO_OF_CYC_SECTORS_P_LOG;			// no of cyclic sectors for periodic logs
    spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.sys_log = NO_OF_CYC_SECTORS_SYS_LOG;				// no of cyclic sectors for system log
    spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.totals = NO_OF_CYC_SECTORS_TOTALS;					// no of cyclic sectors for Totals records
    spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.variables = NO_OF_CYC_SECTORS_VAR;					// no of cyclic sectors for variables records
    spMyApiStruct->sNo_of_Rollbacks.periodic_log = NO_OF_ROLLBACKS_P_LOG;												// no of allowed rollbacks for periodic logs
    spMyApiStruct->sNo_of_Rollbacks.sys_log = NO_OF_ROLLBACKS_SYS_LOG;													// no of rollbacks allowed for system logs
    spMyApiStruct->sNo_of_Rollbacks.totals = NO_OF_ROLLBACKS_TOTALS;														// no of rollbacks allowed for Total records
    spMyApiStruct->sNo_of_Rollbacks.variables = NO_OF_ROLLBACKS_VAR;														// no of rollbacks allowed for Variable Records
    spMyApiStruct->sVersionNo.min.totals = getVersionNo (spMyApiStruct,enum_totals,MIN_VERSION_FIND);					// find and update the minimum version of totals
    spMyApiStruct->sVersionNo.max.totals = getVersionNo (spMyApiStruct,enum_totals,MAX_VERSION_FIND);					// find and update the maximum version number of totals
    spMyApiStruct->sVersionNo.min.periodic_log = getVersionNo (spMyApiStruct,enum_periodic_log,MIN_VERSION_FIND);		// find and update the minimum version number of periodic log
    spMyApiStruct->sVersionNo.max.periodic_log = getVersionNo (spMyApiStruct,enum_periodic_log,MAX_VERSION_FIND);		// find and update the maximum version number of periodic log
    spMyApiStruct->sVersionNo.min.sys_log = getVersionNo (spMyApiStruct,enum_system_log,MIN_VERSION_FIND);					// find and update the minimum version number of system log
    spMyApiStruct->sVersionNo.max.sys_log = getVersionNo (spMyApiStruct,enum_system_log,MAX_VERSION_FIND);					// find and update the maximum version number of system log
    spMyApiStruct->sVersionNo.min.variables = getVersionNo (spMyApiStruct,enum_variables,MIN_VERSION_FIND);					// find and update the minimum version number of variables
    spMyApiStruct->sVersionNo.max.variables = getVersionNo (spMyApiStruct,enum_variables,MAX_VERSION_FIND);					// find and update the maximum version number of variables
    {
        psHeaderDetails = TraverseStructure(spMyApiStruct,enum_periodic_log);																				//	traverse flash for periodic log
        spMyApiStruct->sAddr.firstRecord.periodic_log = psHeaderDetails->u32firstRecordAddress;											//	copy the address of the first record to RAM
        spMyApiStruct->sAddr.BackLink.periodic_log =(uint32_t)(((sPerodicLogBackup*)( psHeaderDetails->u32lastrecordAddress))->backLink );												// copy the address of last record to RAM
    }
    {
        psHeaderDetails = TraverseStructure(spMyApiStruct,enum_system_log);																					//	traverse flash for system log
        spMyApiStruct->sAddr.firstRecord.sys_log = psHeaderDetails->u32firstRecordAddress;													//	copy the address of the first record to RAM
        spMyApiStruct->sAddr.BackLink.sys_log =(uint32_t)(((sSysLogBackup*)( psHeaderDetails->u32lastrecordAddress))->backLink );												// copy the address of last record to RAM
    }
    {
        psHeaderDetails = TraverseStructure(spMyApiStruct,enum_totals);																							//	traverse flash for Totals record
        spMyApiStruct->sAddr.firstRecord.totals = psHeaderDetails->u32firstRecordAddress;														//	copy the address of the first record to RAM
        spMyApiStruct->sAddr.BackLink.totals =(uint32_t)(((sTotalBackup*)( psHeaderDetails->u32lastrecordAddress))->backLink );												// copy the address of last record to RAM
    }
    {
        psHeaderDetails = TraverseStructure(spMyApiStruct,enum_variables);																					//	traverse flash for Variables record
        spMyApiStruct->sAddr.firstRecord.variables = psHeaderDetails->u32firstRecordAddress;												//	copy the address of the first record to RAM
        spMyApiStruct->sAddr.BackLink.variables  =(uint32_t)(((sVarBackup*)( psHeaderDetails->u32lastrecordAddress))->backLink );												// copy the address of last record to RAM
    }
}

/*****************************************************************************
* @note       Function name	: int getreadAddressBase(struct_myapi* ,unsigned char ,int )
* @returns    returns				: base address of structure where the address resides
*
*
* @param      arg1					: datapointer to the API in RAm
* @param      arg2					: type of records to search
* @param      arg3					: address to search
* @author			        			: Shyam Shrivastava
* @date       date  Created	: 03-11-2017
* @brief      Description		: gets base address of structure where the address resides
* @note       Notes					: None
*****************************************************************************/
int getreadAddressBase(struct_myapi* spMyApiStruct,unsigned char u8Type,int readAddress)														// get start address of structure of the given address
{
    unsigned int u32startAddress, u32endAddress,u32TempStartAddress,u32TempEndAddress;															// declare temporary variables
    switch (u8Type)																																																	// select log types and narrow down the search to the base address of each log type
    {
    case enum_periodic_log:																																													// if the type is Periodic Log
        u32startAddress = spMyApiStruct->sMemConfig.s_Base_address
                          + spMyApiStruct->sMemConfig.sBaseOffset.periodic_log;																			// start address of the record type
        u32endAddress = spMyApiStruct->sMemConfig.s_Base_address 																										//	calculate nd address = flash base address+
                        +spMyApiStruct->sMemConfig.sBaseOffset.periodic_log +																				// periodic log offset
                        (																																														// + total size including backups of all cyclic sectors
                            (spMyApiStruct->sMemConfig.sNo_of_Backups.periodic_log +1)*
                            spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.periodic_log
                        )
                        *(spMyApiStruct->sMemConfig.sSize_of_sector);
        break;
    case enum_system_log:
        u32startAddress = spMyApiStruct->sMemConfig.s_Base_address +spMyApiStruct->sMemConfig.sBaseOffset.sys_log;
        u32endAddress = spMyApiStruct->sMemConfig.s_Base_address +spMyApiStruct->sMemConfig.sBaseOffset.sys_log +
                        (
                            (spMyApiStruct->sMemConfig.sNo_of_Backups.sys_log +1)*
                            spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.sys_log
                        )
                        *(spMyApiStruct->sMemConfig.sSize_of_sector);
        break;
    case enum_totals:
        u32startAddress = spMyApiStruct->sMemConfig.s_Base_address +spMyApiStruct->sMemConfig.sBaseOffset.totals;
        u32endAddress = spMyApiStruct->sMemConfig.s_Base_address +spMyApiStruct->sMemConfig.sBaseOffset.totals +
                        (
                            (spMyApiStruct->sMemConfig.sNo_of_Backups.totals +1)*
                            spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.totals
                        )
                        *(spMyApiStruct->sMemConfig.sSize_of_sector);
        break;
    case enum_variables:
        u32startAddress = spMyApiStruct->sMemConfig.s_Base_address +spMyApiStruct->sMemConfig.sBaseOffset.variables;
        u32endAddress = spMyApiStruct->sMemConfig.s_Base_address +spMyApiStruct->sMemConfig.sBaseOffset.periodic_log +
                        (
                            (spMyApiStruct->sMemConfig.sNo_of_Backups.variables +1)*
                            spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.variables
                        )
                        * (spMyApiStruct->sMemConfig.sSize_of_sector);
        break;
    default :
        break;
    }
    for (u32TempStartAddress = u32startAddress; u32startAddress < u32endAddress;)																		// traverse from the start to end of buffer
    {
        u32TempEndAddress  = u32TempStartAddress +(spMyApiStruct->sMemConfig.sSize_of_sector);											// calculate end address of sector
        if ((readAddress >= u32TempStartAddress) && (readAddress <u32TempEndAddress)) return  u32TempStartAddress;	// if it is within limits then return start adddress
        u32TempStartAddress = u32TempEndAddress;																																		// else move to the next sector
    }
    return FALSE;																																																		// got nothing!
}


/*****************************************************************************
* @note       Function name	: uint32_t searchForward(struct_myapi* ,unsigned char ,unsigned int )
* @returns    returns				: address of version number
*
*
* @param      arg1					: datapointer to the API in RAm
* @param      arg2					: type of records to search
* @param      arg3					: version number to search
* @author			        			: Shyam Shrivastava
* @date       date  Created	: 03-11-2017
* @brief      Description		: search for a version number  (Forward)
* @note       Notes					: None
*****************************************************************************/
uint32_t searchForward(struct_myapi* spMyApiStruct,unsigned char u8Type,unsigned int Version_No)										// start search for a version number from the base address of log
{
    uint32_t u32BaseAddress,readAddress,u32CyclicCnt_max,u32NoOfbackups_max,u32SectorSize,u32CyclicCnt,readAddressBase;	//some temporary variables
    uint32_t u32BaseCount;
    sRecordHeader *psHeader;																																														//some more
    switch (u8Type)
    {
    case enum_periodic_log:
        u32BaseAddress = spMyApiStruct ->sMemConfig.s_Base_address + spMyApiStruct ->sMemConfig.sBaseOffset.periodic_log;	// get base address to start searching
        u32NoOfbackups_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.periodic_log;																			// get number of backups allotted for this type of record
        u32CyclicCnt_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.periodic_log;																				// get number of cyclic buffers allotted for this type of record
        break;
    case enum_system_log:
        u32BaseAddress = spMyApiStruct ->sMemConfig.s_Base_address + spMyApiStruct ->sMemConfig.sBaseOffset.sys_log;
        u32NoOfbackups_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.sys_log;
        u32CyclicCnt_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.sys_log;
        break;
    case enum_totals:
        u32BaseAddress = spMyApiStruct ->sMemConfig.s_Base_address + spMyApiStruct ->sMemConfig.sBaseOffset.totals;
        u32NoOfbackups_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.totals;
        u32CyclicCnt_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.totals;
        break;
    case enum_variables:
        u32BaseAddress = spMyApiStruct ->sMemConfig.s_Base_address + spMyApiStruct ->sMemConfig.sBaseOffset.variables;
        u32NoOfbackups_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.variables;
        u32CyclicCnt_max = spMyApiStruct ->sMemConfig.sNo_of_Backups.variables;
        break;
    default :
        break;
    }
    u32SectorSize = spMyApiStruct ->sMemConfig.sSize_of_sector;																													// get size of sectors from RAM
    readAddress = spMyApiStruct->sAddr.firstRecord.periodic_log;																												// get the address of the first record from RAM
    readAddressBase = getreadAddressBase(spMyApiStruct,u8Type,readAddress);																							// get the start address of the sector for dthe above memory
    do
    {
        psHeader = (sRecordHeader*)readAddress;																																					// read header of the structure at the above memory


        if(psHeader-> u16structHeader == VALID_HEADER)																																	// do we have a valid header to start with?
        {
            // If Yes
            if (psHeader->u32sVersionNo == Version_No)																																	// compare version numbers do they match?
                return readAddress; //version found																																			// Yes they match, end search return addrerss
            else
            {
                // no match!
                readAddress  = readAddress + psHeader ->u16Size ;																												// move to the next structure

            }
        }
        else																																																						// valid header not found!
        {
            // check the backups
            for(u32BaseCount = 0; u32BaseCount <u32NoOfbackups_max ; u32BaseCount++)																		// check in all available backups!
            {
                psHeader =(sRecordHeader*) (readAddress + u32BaseCount*u32SectorSize);
                if(psHeader-> u16structHeader == VALID_HEADER)																													// do we have a valid header to start with?
                {
                    if (psHeader->u32sVersionNo == Version_No)																													// compare version numbers do they match?
                        return readAddress; //version found																															// Yes they match, end search return addrerss
                    else
                    {
                        // no match?
                        readAddress  = readAddress + psHeader ->u16Size ;																								// get back to original sector and update read pointe
                        break;
                    }
                }
            }																																																						// End of backup check
        }
        if ((readAddress+ psHeader->u16Size)> (readAddressBase + u32SectorSize) )																				// check if read address overflows
        {
            // current Cyclic buffer
            if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;																														// increase cyclic count if less than max
            else break;																																																	// get out of loop all buffers checked!
            readAddressBase =( readAddressBase + (u32CyclicCnt*(u32NoOfbackups_max+1))*u32SectorSize);									// update new sector start address
            readAddress = readAddressBase;																																							// update read pointer to start of sector
            psHeader =(sRecordHeader *)readAddress;																																			// read structure header
        }
    }
    while(readAddress < (u32BaseAddress + (u32SectorSize * (((u32NoOfbackups_max+1)*(u32CyclicCnt_max))) +1 )));				// CHECK UNTIL END OF BUFFER IS REACHED
    return 0;
    //get size move ahead.
}
/****************************
*	searchBackwards Yet to be implemented
*/
/*****************************************************************************
* @note       Function name	: uint32_t searchBackwards(struct_myapi* ,unsigned char ,unsigned int )
* @returns    returns				: address of version number
*
*
* @param      arg1					: datapointer to the API in RAm
* @param      arg2					: type of records to search
* @param      arg3					: version number to search
* @author			        			: Shyam Shrivastava
* @date       date  Created	: 03-11-2017
* @brief      Description		: search for a version number  (backward)
* @note       Notes					: not implemented  (03-11-2017)
*****************************************************************************/
uint32_t searchBackwards(struct_myapi* spMyApiStruct,unsigned char u8Type,unsigned int Version_No)
{
// 	sRecordHeader *psHeader;
// 	psHeader = spMyApiStruct->sAddr.lastRecord.periodic_log;
    return TRUE;
}


/*****************************************************************************
* @note       Function name	: uint32_t searchBackwards(struct_myapi* ,unsigned char ,unsigned int )
* @returns    returns				: address of version number
*
*
* @param      arg1					: datapointer to the API in RAm
* @param      arg2					: type of records to search
* @param      arg3					: version number to search
* @author			        			: Shyam Shrivastava
* @date       date  Created	: 03-11-2017
* @brief      Description		: search for a version number  (backward)
* @note       Notes					: not implemented  (03-11-2017)
*****************************************************************************/
void readVersion (struct_myapi* spMyApiStruct,unsigned char u8Type,unsigned int Version_No)
{
    uint32_t u32MinVersion,u32MaxVersion;																													// declare some temporary variables
    switch (u8Type)																																								// get the record type
    {
    case enum_periodic_log :
        u32MinVersion = spMyApiStruct->sVersionNo.min.periodic_log;																// find minimum version number for the record type
        u32MaxVersion = spMyApiStruct->sVersionNo.max.periodic_log;																// find maximum version number for the record type
        break;
    case enum_system_log :
        u32MinVersion = spMyApiStruct->sVersionNo.min.sys_log;
        u32MaxVersion = spMyApiStruct->sVersionNo.max.sys_log;
        break;
    case enum_totals :
        u32MinVersion = spMyApiStruct->sVersionNo.min.totals;
        u32MaxVersion = spMyApiStruct->sVersionNo.max.totals;
        break;
    case enum_variables :
        u32MinVersion = spMyApiStruct->sVersionNo.min.variables;
        u32MaxVersion = spMyApiStruct->sVersionNo.max.variables;
        break;
    default:
        break;
    }
    //need to do a binary search
    if ((Version_No - u32MinVersion)<( u32MaxVersion -Version_No))																// if version number is closer to minimum version
    {
        // YES?
        searchForward(spMyApiStruct,u8Type,Version_No);																						// search forward for the version
    }
    else																																													// NO ?
        searchBackwards(spMyApiStruct,u8Type,Version_No);																					// search backward for the version.
}

/*****************************************************************************
* @note       Function name	: writeRamtoFlash
* @returns    returns				: TRUE = success
*															FALSE = Failure
*
* @param      arg1					: datapointer to the buffer
* @param      arg2					: addres of flash to write in
* @param      arg3					: size of the data to be written in words
* @author			        			: Shyam Shrivastava
* @date       date  Created	: 03-11-2017
* @brief      Description		: writes an array of words into flash
* @note       Notes					: None
*****************************************************************************/

int writeRamtoFlash(uint16_t *ramData,uint32_t u32Address2Write,uint32_t u32Size )
{
    uint32_t noofWords;																																					// NO OF BYTES TO WRITE
    for (noofWords =0; noofWords<(u32Size); noofWords++)																				// start to write data
    {
        if (ExtFlash_writeWord(u32Address2Write,*ramData))																			// write a word on  the write address
        {
            ramData++;																																					// fetch the next word
            u32Address2Write++;																																	// update the address pointer
        }
        else
            return FALSE  																																			//write to flash failed
                   ;
    }
    return TRUE;
}
/*****************************************************************************
* @note       Function name	: int populateStructure(struct_myapi* ,unsigned char )
* @returns    returns				: TRUE = success
*															FALSE = Failure
*
* @param      arg1					: datapointer to the buffer
* @param      arg2					: type of record that needs to be populated
* @param      arg3					:
* @author			        			: SKS
* @date       date  Created	: 03-11-2017
* @brief      Description		: updates respective structure with the latest values
* @note       Notes					: None
*****************************************************************************/

int populateStructure(struct_myapi* spMyApiStruct,unsigned char u8Type)
{
    switch(u8Type)																																											// get the type of trecords that needs to be populated
    {
    case enum_totals:																																										// its a totals type record
        spMyApiStruct->sTotals.sHeader.u16structHeader = VALID_HEADER;																	// add a valid header
        spMyApiStruct->sTotals.totals = Totals_var;																											// copy totals structure and update to API structure
        spMyApiStruct->sTotals.Zero_Weight = Calculation_struct.Zero_weight;														// copy zero number and update
        spMyApiStruct->sTotals.Accum_weight = periodic_data.Accum_weight;																// copy accumulated weight and update
        break;
    case enum_periodic_log:																																							// its a periodic log
        spMyApiStruct->sPerodicLog.sHeader.u16structHeader = VALID_HEADER;															// adda a valid header
        spMyApiStruct->sPerodicLog.sPeriodicData = periodic_data;																				// copy periodic data structure
        break;
    case enum_system_log:																																								// its a system log
        spMyApiStruct->sPerodicLog.sHeader.u16structHeader = VALID_HEADER;															// ADD a valid header
        //spMyApiStruct->sSysLog.strSysLog
        // update this string and call append syslog
        if ((strlen(spMyApiStruct->sSysLog.strSysLog)%2))strncat(spMyApiStruct->sSysLog.strSysLog," ",1);	// if string length is odd add an space character
        break;
    case enum_variables:																																								//	its a variables structure
        spMyApiStruct->sVar.admin_struct = Admin_var;																										//	copy admin variables and update in RAM
        spMyApiStruct->sVar.calib_struct = Calibration_var;																							//	copy Calibration Variables and update in RAM
        spMyApiStruct->sVar.scale_setup_struct  = Scale_setup_var;																			//	copy scale setup variables and update in RAM
        spMyApiStruct->sVar.setup_device_struct = Setup_device_var;																			//	copy	device Setup Variables and update in RAM
        spMyApiStruct->sVar.sHeader.u16structHeader=VALID_HEADER;																				// 	add a valid header
        break;
    default:
        return FALSE;
        break;																																													// should not reach here
    }
    return TRUE;																																												//	successfully Populated the required structure
}
/*****************************************************************************
* @note       Function name	: int AppendRecord(struct_myapi* ,unsigned char )
* @returns    returns				: TRUE = success
*															FALSE = Failure
*
* @param      arg1					: datapointer to the buffer
* @param      arg2					: type of record that needs to be appended
* @param      arg3					:
* @author			        			: SKS
* @date       date  Created	: 03-11-2017
* @brief      Description		: appends the latest values of selected structure
*															into FLASH
* @note       Notes					: None
*****************************************************************************/
int AppendRecord(struct_myapi* spMyApiStruct,unsigned char u8Type)
{
    int tempAddr,no_of_backups;																																//	just  a temporary variable
    switch(u8Type)																															//	check the type of Record that needs to be appended
    {
    case enum_totals:																														//	Its a Totals Record
    {
        spMyApiStruct->sTotals.backLink = (sTotalBackup*)(spMyApiStruct->sAddr.BackLink.totals);				//	sTotals is the structure to be written
        if(((uint32_t)(spMyApiStruct->sAddr.CurrLink.totals) == NULL)&&(spMyApiStruct->sTotals.backLink)==NULL)
        {
            spMyApiStruct->sTotals.backLink = NULL;																												//	writing first Record!!
            spMyApiStruct->sAddr.CurrLink.totals =spMyApiStruct->sMemConfig.s_Base_address +							//	Start with the first address of Totals Buffer
                                                  spMyApiStruct->sMemConfig.sBaseOffset.totals ;
            spMyApiStruct->sTotals.sHeader.u16Size = FLASH_SIZEOF(sTotalBackup);// +;
            spMyApiStruct->sTotals.sHeader.u32sVersionNo=0;
        }
        else
        {
            spMyApiStruct->sAddr.BackLink.totals = spMyApiStruct->sAddr.CurrLink.totals;							//spMyApiStruct->sAddr is our API reference
            // update back link with current structures address
            spMyApiStruct->sTotals.sHeader.u16Size = FLASH_SIZEOF(sTotalBackup);// +;									// get size of structure (including CRC)
            tempAddr = getreadAddressBase(spMyApiStruct,u8Type,spMyApiStruct->sAddr.CurrLink.totals);	//	get starting address of sector of new structure

            spMyApiStruct->sAddr.CurrLink.totals = (spMyApiStruct->sAddr.CurrLink.totals) + 					// get current link address for the new structure
                                                   (spMyApiStruct->sTotals.sHeader.u16Size) ;
        }
//       tempAddr = getreadAddressBase(spMyApiStruct,u8Type,spMyApiStruct->sAddr.CurrLink.totals);	//	get starting address of sector of new structure
        if (spMyApiStruct->sAddr.CurrLink.totals >  (tempAddr +spMyApiStruct->sMemConfig.sSize_of_sector))			//	validate for sector overflow
        {
            // sector overflow detected
            if (spMyApiStruct->sAddr.CurrLink.totals > 																													//	check for buffer overflow (full quotat of space allocated
                    (spMyApiStruct->sMemConfig.s_Base_address +																												//	Flash Base address
                     spMyApiStruct->sMemConfig.sBaseOffset.totals +																									//	Offset where Totals will be saved
                     (
                         (spMyApiStruct->sMemConfig.sNo_of_Backups.totals +1) * 																		//	group of (Backups + Originals)
                         spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.totals
                     )*spMyApiStruct->sMemConfig.sSize_of_sector
                    )
               )																	//	no of cyclic buffers

            {
                //	Buffer overflow detected!
                tempAddr = spMyApiStruct->sMemConfig.s_Base_address +																						//	assign the sector base address to
                           spMyApiStruct->sMemConfig.sBaseOffset.totals;																				//	first sector of Totals Buffer
                for (no_of_backups =0; no_of_backups <spMyApiStruct->sMemConfig.sNo_of_Backups.totals; no_of_backups++)
                    ExtFlash_eraseBlock(tempAddr + (no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector)));																																		//	Erase the Sector to prepare for write
                spMyApiStruct->sAddr.CurrLink.totals = tempAddr;
            }
            else																																																// Not an overflow
            {
                tempAddr = tempAddr+																																						//	move the start of sector pointer
                           (spMyApiStruct->sMemConfig.sNo_of_Backups.totals+1 )*																	//	to the next sector of cyclic buffer
                           spMyApiStruct->sMemConfig.sSize_of_sector;																//
                //erase all backups

                for (no_of_backups =0; no_of_backups <spMyApiStruct->sMemConfig.sNo_of_Backups.totals; no_of_backups++)
                    ExtFlash_eraseBlock(tempAddr + (no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector)));																																	//	erase the sector
                spMyApiStruct->sAddr.CurrLink.totals = tempAddr;																								//	update current address Pointer
            }
        }
        spMyApiStruct->sTotals.sHeader.u32sVersionNo++;																													//	Increment the version number
        populateStructure(spMyApiStruct,u8Type);																																//	populate the respective record type

        for (no_of_backups =0; no_of_backups < spMyApiStruct->sMemConfig.sNo_of_Backups.totals; no_of_backups++)	//	write the same structure
        {
            //	on all the backup spaces
            spMyApiStruct->sTotals.CRC = CalculateCheckSum((uint8_t*)(&spMyApiStruct->sTotals), FLASH_SIZEOF(sTotalBackup)-2);
            writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sTotals),
                             (spMyApiStruct->sAddr.CurrLink.totals + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector)),		//	calculate next backup address
                             FLASH_SIZEOF(sTotalBackup));																																				//	CRC is yet to be written
//
//             writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sTotals.CRC),
//                              (spMyApiStruct->sAddr.CurrLink.totals + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector))+1,
//                              FLASH_SIZEOF(spMyApiStruct->sTotals.CRC));
        }
        break;
    }
    case enum_variables:
    {
        spMyApiStruct->sVar.backLink = (sVarBackup*)(spMyApiStruct->sAddr.BackLink.totals);				//	sVar is the structure to be written
        if(((uint32_t)(spMyApiStruct->sVar.backLink) == 0xFFFF)||(spMyApiStruct->sVar.backLink)==0)
        {
            spMyApiStruct->sVar.backLink = NULL;																												//	writing first Record!!
            spMyApiStruct->sAddr.CurrLink.variables =spMyApiStruct->sMemConfig.s_Base_address +					//	Start with the first address of Variables Buffer
                    spMyApiStruct->sMemConfig.sBaseOffset.variables ;
        }
        spMyApiStruct->sAddr.BackLink.variables = spMyApiStruct->sAddr.CurrLink.variables;							//spMyApiStruct->sAddr is our API reference
        // update back link with current structures address
        spMyApiStruct->sVar.sHeader.u16Size = FLASH_SIZEOF(sVarBackup);// +;									// get size of structure (including CRC)
        spMyApiStruct->sAddr.CurrLink.variables = (spMyApiStruct->sAddr.CurrLink.variables) + 					// get current link address for the new structure
                (spMyApiStruct->sVar.sHeader.u16Size) ;
        tempAddr = getreadAddressBase(spMyApiStruct,u8Type,spMyApiStruct->sAddr.CurrLink.variables);	//	get starting address of sector of new structure
        if (spMyApiStruct->sAddr.CurrLink.variables >  (tempAddr +spMyApiStruct->sMemConfig.sSize_of_sector))			//	validate for sector overflow
        {
            // sector overflow detected
            if (spMyApiStruct->sAddr.CurrLink.variables > 																													//	check for buffer overflow (full quota of space allocated
                    (spMyApiStruct->sMemConfig.s_Base_address +																												//	Flash Base address
                     spMyApiStruct->sMemConfig.sBaseOffset.variables +																									//	Offset where Totals will be saved
                     ((spMyApiStruct->sMemConfig.sNo_of_Backups.variables +1) * 																		//	group of (Backups + Originals)
                      spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.variables)))																	//	no of cyclic buffers
            {
                //	Buffer overflow detected!
                tempAddr = spMyApiStruct->sMemConfig.s_Base_address +																						//	assign the sector base address to
                           spMyApiStruct->sMemConfig.sBaseOffset.variables;																				//	first sector of Totals Buffer
                ExtFlash_eraseBlock(tempAddr);																																	//	Erase the Sector to prepare for write
                spMyApiStruct->sAddr.CurrLink.variables = tempAddr;
            }
            else																																																// Not an overflow
            {
                tempAddr = tempAddr+																																						//	move the start of sector pointer
                           (spMyApiStruct->sMemConfig.sNo_of_Backups.variables+1 )*																	//	to the next sector of cyclic buffer
                           spMyApiStruct->sMemConfig.sSize_of_sector;																//
                ExtFlash_eraseBlock(tempAddr);																																	//	erase the sector
                spMyApiStruct->sAddr.CurrLink.variables = tempAddr;																								//	update current address Pointer
            }
        }
        spMyApiStruct->sVar.sHeader.u32sVersionNo++;																													//	Increment the version number
        populateStructure(spMyApiStruct,u8Type);																																//	populate the respective record type

        for (no_of_backups =0; no_of_backups < spMyApiStruct->sMemConfig.sNo_of_Backups.variables; no_of_backups++)	//	write the same structure
        {
            //	on all the backup spaces
            writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sVar),
                             (spMyApiStruct->sAddr.CurrLink.variables + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector)),		//	calculate next backup address
                             FLASH_SIZEOF(sVarBackup)-2);																																				//	CRC is yet to be written
            spMyApiStruct->sVar.CRC = CalculateCheckSum((uint8_t*)(&spMyApiStruct->sVar), FLASH_SIZEOF(sVarBackup)-2);
            writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sVar.CRC), 																									//write CRC
                             (spMyApiStruct->sAddr.CurrLink.variables + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector))+1,
                             FLASH_SIZEOF(spMyApiStruct->sVar.CRC));
        }
        break;
    }
    case enum_periodic_log:
    {
        spMyApiStruct->sPerodicLog.backLink = (sPerodicLogBackup*)(spMyApiStruct->sAddr.BackLink.periodic_log);				//	sPerodicLog is the structure to be written
        if(((uint32_t)(spMyApiStruct->sPerodicLog.backLink) == 0xFFFF)||(spMyApiStruct->sPerodicLog.backLink)==0)
        {
            spMyApiStruct->sPerodicLog.backLink = NULL;																												//	writing first Record!!
            spMyApiStruct->sAddr.CurrLink.periodic_log =spMyApiStruct->sMemConfig.s_Base_address +							//	Start with the first address of periodic log Buffer
                    spMyApiStruct->sMemConfig.sBaseOffset.periodic_log ;
        }
        spMyApiStruct->sAddr.BackLink.periodic_log = spMyApiStruct->sAddr.CurrLink.periodic_log;							//spMyApiStruct->sAddr is our API reference
        // update back link with current structures address
        spMyApiStruct->sPerodicLog.sHeader.u16Size = FLASH_SIZEOF(sPerodicLogBackup);// +;									// get size of structure (including CRC)
        spMyApiStruct->sAddr.CurrLink.periodic_log = (spMyApiStruct->sAddr.CurrLink.periodic_log) + 					// get current link address for the new structure
                (spMyApiStruct->sPerodicLog.sHeader.u16Size) ;
        tempAddr = getreadAddressBase(spMyApiStruct,u8Type,spMyApiStruct->sAddr.CurrLink.periodic_log);	//	get starting address of sector of new structure
        if (spMyApiStruct->sAddr.CurrLink.periodic_log >  (tempAddr +spMyApiStruct->sMemConfig.sSize_of_sector))			//	validate for sector overflow
        {
            // sector overflow detected
            if (spMyApiStruct->sAddr.CurrLink.periodic_log > 																										//	check for buffer overflow (full quotat of space allocated
                    (spMyApiStruct->sMemConfig.s_Base_address +																									//	Flash Base address
                     spMyApiStruct->sMemConfig.sBaseOffset.periodic_log +																				//	Offset where Totals will be saved
                     ((spMyApiStruct->sMemConfig.sNo_of_Backups.periodic_log +1) * 															//	group of (Backups + Originals)
                      spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.periodic_log)))														//	no of cyclic buffers
            {
                //	Buffer overflow detected!
                tempAddr = spMyApiStruct->sMemConfig.s_Base_address +																						//	assign the sector base address to
                           spMyApiStruct->sMemConfig.sBaseOffset.periodic_log;																				//	first sector of Totals Buffer
                ExtFlash_eraseBlock(tempAddr);																																	//	Erase the Sector to prepare for write
                spMyApiStruct->sAddr.CurrLink.periodic_log = tempAddr;
            }
            else																																																// Not an overflow
            {
                tempAddr = tempAddr+																																						//	move the start of sector pointer
                           (spMyApiStruct->sMemConfig.sNo_of_Backups.periodic_log+1 )*																	//	to the next sector of cyclic buffer
                           spMyApiStruct->sMemConfig.sSize_of_sector;																//
                ExtFlash_eraseBlock(tempAddr);																																	//	erase the sector
                spMyApiStruct->sAddr.CurrLink.periodic_log = tempAddr;																								//	update current address Pointer
            }
        }
        spMyApiStruct->sPerodicLog.sHeader.u32sVersionNo++;																													//	Increment the version number
        populateStructure(spMyApiStruct,u8Type);																																//	populate the respective record type

        for (no_of_backups =0; no_of_backups < spMyApiStruct->sMemConfig.sNo_of_Backups.periodic_log; no_of_backups++)	//	write the same structure
        {
            //	on all the backup spaces
            writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sPerodicLog),
                             (spMyApiStruct->sAddr.CurrLink.periodic_log + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector)),		//	calculate next backup address
                             FLASH_SIZEOF(sPerodicLogBackup)-2);																																				//	CRC is yet to be written
            spMyApiStruct->sPerodicLog.CRC = CalculateCheckSum((uint8_t*)(&spMyApiStruct->sPerodicLog), FLASH_SIZEOF(sPerodicLogBackup)-2);
            writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sPerodicLog.CRC),
                             (spMyApiStruct->sAddr.CurrLink.periodic_log + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector))+1,
                             FLASH_SIZEOF(spMyApiStruct->sPerodicLog.CRC));
        }
        break;
    }
    case enum_system_log:
    {
        spMyApiStruct->sSysLog.backLink = (sSysLogBackup*)(spMyApiStruct->sAddr.BackLink.totals);						//	sTotals is the structure to be written
        if(((uint32_t)(spMyApiStruct->sSysLog.backLink) == 0xFFFF)||(spMyApiStruct->sSysLog.backLink)==0)
        {
            spMyApiStruct->sSysLog.backLink = NULL;																													//	writing first Record!!
            spMyApiStruct->sAddr.CurrLink.sys_log =spMyApiStruct->sMemConfig.s_Base_address +								//	Start with the first address of Totals Buffer
                                                   spMyApiStruct->sMemConfig.sBaseOffset.sys_log ;
        }
        spMyApiStruct->sAddr.BackLink.sys_log = spMyApiStruct->sAddr.CurrLink.sys_log;												//spMyApiStruct->sAddr is our API reference
        // update back link with current structures address
        spMyApiStruct->sSysLog.sHeader.u16Size = FLASH_SIZEOF(sSysLogBackup);// +;														// get size of structure (including CRC)
        spMyApiStruct->sAddr.CurrLink.sys_log = (spMyApiStruct->sAddr.CurrLink.sys_log) + 										// get current link address for the new structure
                                                (spMyApiStruct->sSysLog.sHeader.u16Size) ;
        tempAddr = getreadAddressBase(spMyApiStruct,u8Type,spMyApiStruct->sAddr.CurrLink.sys_log);						//	get starting address of sector of new structure
        if (spMyApiStruct->sAddr.CurrLink.sys_log >  (tempAddr +spMyApiStruct->sMemConfig.sSize_of_sector))			//	validate for sector overflow
        {
            // sector overflow detected
            if (spMyApiStruct->sAddr.CurrLink.sys_log > 																													//	check for buffer overflow (full quotat of space allocated
                    (spMyApiStruct->sMemConfig.s_Base_address +																												//	Flash Base address
                     spMyApiStruct->sMemConfig.sBaseOffset.sys_log +																									//	Offset where Totals will be saved
                     ((spMyApiStruct->sMemConfig.sNo_of_Backups.sys_log +1) * 																		//	group of (Backups + Originals)
                      spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.sys_log)))																	//	no of cyclic buffers
            {
                //	Buffer overflow detected!
                tempAddr = spMyApiStruct->sMemConfig.s_Base_address +																						//	assign the sector base address to
                           spMyApiStruct->sMemConfig.sBaseOffset.sys_log;																				//	first sector of Totals Buffer
                ExtFlash_eraseBlock(tempAddr);																																	//	Erase the Sector to prepare for write
                spMyApiStruct->sAddr.CurrLink.sys_log = tempAddr;
            }
            else																																																// Not an overflow
            {
                tempAddr = tempAddr+																																						//	move the start of sector pointer
                           (spMyApiStruct->sMemConfig.sNo_of_Backups.sys_log+1 )*																	//	to the next sector of cyclic buffer
                           spMyApiStruct->sMemConfig.sSize_of_sector;																//
                ExtFlash_eraseBlock(tempAddr);																																	//	erase the sector
                spMyApiStruct->sAddr.CurrLink.sys_log = tempAddr;																								//	update current address Pointer
            }
        }
        spMyApiStruct->sSysLog.sHeader.u32sVersionNo++;																													//	Increment the version number
        populateStructure(spMyApiStruct,u8Type);																																//	populate the respective record type

        for (no_of_backups =0; no_of_backups < spMyApiStruct->sMemConfig.sNo_of_Backups.totals; no_of_backups++)	//	write the same structure
        {
            //	on all the backup spaces
            writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sSysLog),
                             (spMyApiStruct->sAddr.CurrLink.sys_log + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector)),		//	calculate next backup address
                             FLASH_SIZEOF(sSysLogBackup)-2);																																				//	CRC is yet to be written
            spMyApiStruct->sSysLog.CRC = CalculateCheckSum((uint8_t*)(&spMyApiStruct->sSysLog), FLASH_SIZEOF(sSysLogBackup)-2);
            writeRamtoFlash ((uint16_t*)(&spMyApiStruct->sSysLog.CRC),
                             (spMyApiStruct->sAddr.CurrLink.sys_log + no_of_backups*(spMyApiStruct->sMemConfig.sSize_of_sector))+1,
                             FLASH_SIZEOF(spMyApiStruct->sSysLog.CRC));																														//now write the CRC
        }
        break;
    }
    }
// write  a system log saying variable updated  to version# ----
    return 0;
}
/*****************************************************************************
* @note       Function name	: sRecordDetails* TraverseStructure(struct_myapi* ,unsigned char )
* @returns    returns				: sRecordDetails pointer
*
*
* @param      arg1					: datapointer to the buffer
* @param      arg2					: type of record that needs to be traversed
* @param      arg3					:
* @author			        			: SKS
* @date       date  Created	: 03-11-2017
* @brief      Description		: traverse the log area of the type of record supplied
*															collects relevant data to init API structure
* @note       Notes					: will not be called too often.. probably just once
*****************************************************************************/
sRecordDetails* TraverseStructure(struct_myapi* spMyApiStruct,unsigned char u8Type)
{
    uint32_t u32BaseAddress,u32BaseOffset,max_version=0,min_version=0xFFFFFFFF,temp_version;
    uint32_t u32CyclicCnt=0,u32CyclicCnt_max,u32NoofBackup =0,u32NoofBackup_max=0,u32SectorSize;
    uint16_t * readAddress,*readAddressBase;//,*readAddressTemp;
    sRecordHeader *psHeader;
    sRecordDetails *psHeaderDetails;

    u32BaseAddress = spMyApiStruct->sMemConfig.s_Base_address;
    u32SectorSize = spMyApiStruct->sMemConfig.sSize_of_sector;
    u32CyclicCnt =0;
    u32NoofBackup =0;
    switch(u8Type)
    {
    case enum_totals:
        u32CyclicCnt_max =spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.totals;
        u32NoofBackup_max =spMyApiStruct->sMemConfig.sNo_of_Backups.totals;
        u32BaseOffset = spMyApiStruct->sMemConfig.sBaseOffset.totals;
				
        break;
    case enum_periodic_log:
        u32CyclicCnt_max =spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.periodic_log;
        u32NoofBackup_max =spMyApiStruct->sMemConfig.sNo_of_Backups.periodic_log;
        u32BaseOffset = spMyApiStruct->sMemConfig.sBaseOffset.periodic_log;
        break;
    case enum_system_log:
        u32CyclicCnt_max =spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.sys_log;
        u32NoofBackup_max =spMyApiStruct->sMemConfig.sNo_of_Backups.sys_log;
        u32BaseOffset = spMyApiStruct->sMemConfig.sBaseOffset.sys_log;
        break;
    case enum_variables:
        u32CyclicCnt_max =spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.variables;
        u32NoofBackup_max =spMyApiStruct->sMemConfig.sNo_of_Backups.variables;
        u32BaseOffset = spMyApiStruct->sMemConfig.sBaseOffset.variables;
        break;
    }
		u32BaseAddress+=u32BaseOffset;
    readAddress = (	uint16_t *)u32BaseAddress;																										// HEAD POINTER FOR TRAVERSING
    psHeader = (	sRecordHeader *)u32BaseAddress;
    do
    {
        if(psHeader-> u16structHeader == VALID_HEADER)			// COMPARE CHECKSUM AND HEADER CONSTANT
        {
            // MATCHES WELL -> ALL IS WELL
            temp_version = psHeader->u32sVersionNo;																										// GET VERSION NUMBER
            if (temp_version > max_version )																													// UPDATE MAX VERSION NUMBER
            {
                max_version = temp_version;
//                u32CurrentAddress = (uint32_t)readAddress ;/*+psHeader -> u16Size;	*/											// IF THE RECORD IS LATEST UPDATE CURRENT READ ADDRESS
            }
            if (temp_version < min_version)																														// UPDATE OLDEST VERSION NUMBER
            {
                min_version = temp_version;
//               u32OldestAddress = (uint32_t)readAddress ;
            }
            psHeaderDetails->u32lastrecordAddress=(uint32_t)readAddress;
            readAddress=readAddress +psHeader -> u16Size;																							// UPDATE STRUCT HEADER POINTER TO THE NEXT LOCATION
            psHeader =(sRecordHeader *)readAddress;																										// READ THE NEXT HEADER
        }
        else
        {
            //THE AREA AHEAD MIGHT BE EMPTY!!
            //CHECK FOR 0Xff
            if (psHeader-> u16structHeader == 0xFFFF)
            {
                //THE AREA SEEMS TO BE EMPTY CHECK WITH AT LEAST 1 BACKUP SECTOR IF AVAILABLE.
                if (u32NoofBackup_max >0)
                {
                    for (u32NoofBackup = 0; u32NoofBackup <u32NoofBackup_max; u32NoofBackup++ )
                    {
                        psHeader = (sRecordHeader *)(readAddress + u32NoofBackup *u32SectorSize);
                        if (psHeader->u16structHeader == 0xFFFF)
                        {
                            if((u32CyclicCnt<u32CyclicCnt_max)&&(u32NoofBackup==(u32NoofBackup_max-1)))
                            {
                                u32CyclicCnt++;
                                u32NoofBackup=0;
                            }
                            else
                            {
                                // init API addresses to NULL
																 if((u32CyclicCnt==u32CyclicCnt_max)){
                                spMyApiStruct->sAddr.BackLink.totals = NULL;
                                spMyApiStruct->sAddr.CurrLink.totals= NULL;//(uint32_t )(spMyApiStruct->sMemConfig.s_Base_address+ u32BaseOffset);
                                spMyApiStruct->sAddr.firstRecord.totals= NULL;
																 return psHeaderDetails;
																 }
																
                            }
                            readAddressBase =(uint16_t *)( (spMyApiStruct->sMemConfig.s_Base_address+ u32BaseOffset) +
                                                           (u32CyclicCnt*(u32NoofBackup_max+1))*u32SectorSize);
                            readAddress = readAddressBase;
                            psHeader =(sRecordHeader *)readAddress;
                        }
                        else
                        {
                            if (psHeader->u16structHeader == VALID_HEADER)
                            {
                                //system log entry : corrupt sector found at address : ?readAddress +(u32NoofBackup-1) *SECTOR_SIZE
                                psHeaderDetails->u32lastrecordAddress=(uint32_t)readAddress;
                                readAddress += psHeader->u16Size;
                                psHeader = (sRecordHeader *)(readAddress);
                                psHeaderDetails->u16Size =  psHeader->u16Size;
                                psHeaderDetails->u16structHeader =  psHeader->u16structHeader;
                                psHeaderDetails->u16structFirmwareversion =  psHeader->u16structFirmwareversion;
                                psHeaderDetails->u32sVersionNo =  psHeader->u32sVersionNo;
                                psHeaderDetails->u32sVersionNoMin =  min_version;
                                psHeaderDetails->u32sVersionNoMax =  max_version;
                                return psHeaderDetails;
                            }
                        }

                    }

                    if ((psHeader->u16structHeader != 0xFFFF) && (u32NoofBackup>= u32NoofBackup_max))
                        while(1); // we are stuck here .... Invalid data on all sectors!
                    // put something on screen to know a critical error has occurred.
                    else
                    {

                    }

                }
                else
                {
                    //we have no backup sectors Assuming the sector is clear
                    psHeader = (sRecordHeader *)(readAddress);
                    psHeaderDetails->u16Size =  psHeader->u16Size;
                    psHeaderDetails->u16structHeader =  psHeader->u16structHeader;
                    psHeaderDetails->u16structFirmwareversion =  psHeader->u16structFirmwareversion;
                    psHeaderDetails->u32sVersionNo =  psHeader->u32sVersionNo;
                    psHeaderDetails->u32sVersionNoMin =  min_version;
                    psHeaderDetails->u32sVersionNoMax =  max_version;
                    return psHeaderDetails;
                }
            }

            else
            {

                //if we do not have backup, then die..
//                 if (u32NoofBackup_max ==0)
//                 {
//                     while(1);// put something on screen to know a critical error has occurred.
//                 }
                //the area is corrupt
                //skip to the next location based on the size stored in the backup sectors
                // check the backup sectors until a valid header is found

                for (u32NoofBackup = 0; u32NoofBackup <u32NoofBackup_max; u32NoofBackup++ )
                {
                    psHeader = (sRecordHeader *)(readAddress + u32NoofBackup *(u32SectorSize));
                    if (psHeader->u16structHeader == VALID_HEADER)
                    {
                        //found valid header
                        //update read address
                        readAddress += psHeader->u16Size;
                        break;
                    }
                }
                if ((psHeader->u16structHeader != VALID_HEADER) && (u32NoofBackup>= u32NoofBackup_max))
                    while(1); // we are stuck here .... Invalid data on all sectors!
                // put something on screen to know a critical error has occurred.
            }
        }
        if ((readAddress+ psHeader->u16Size)> (readAddressBase + u32SectorSize) )
        {
            if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;
            else break;
            readAddressBase =(uint16_t *)( (spMyApiStruct->sMemConfig.s_Base_address+ u32BaseOffset) + (u32CyclicCnt*(u32NoofBackup_max+1))*u32SectorSize);
            readAddress = readAddressBase;
            psHeader =(sRecordHeader *)readAddress;
        }
    }
    while(readAddress <(uint16_t *)(u32BaseAddress + (u32SectorSize * (((		(u32NoofBackup_max)+1)*(u32CyclicCnt_max))) +1 )));											// CHECK UNTIL END OF SECTOR IS REACHED
    psHeaderDetails->u16Size =  psHeader->u16Size;
    psHeaderDetails->u16structHeader =  psHeader->u16structHeader;
    psHeaderDetails->u16structFirmwareversion =  psHeader->u16structFirmwareversion;
    psHeaderDetails->u32sVersionNo =  psHeader->u32sVersionNo;
    psHeaderDetails->u32sVersionNoMin =  min_version;
    psHeaderDetails->u32sVersionNoMax =  max_version;
    return psHeaderDetails;
}

/*****************************************************************************
* @note       Function name	: int getVersionNo(struct_myapi* ,unsigned char ,int )
* @returns    returns				: VERSION NUMBER
*
*
* @param      arg1					: datapointer to the buffer
* @param      arg2					: type of record that needs to be traversed
* @param      arg3					:	minimum or maximum version selection
* @author			        			: SKS
* @date       date  Created	: 03-11-2017
* @brief      Description		: traverse the log area of the type of record supplied
*															gets the minimum or maximum version numbers
* @note       Notes					: none
*****************************************************************************/
int getVersionNo(struct_myapi* spMyApiStruct,unsigned char u8Type,int minmax)
{
    sRecordDetails *psHeaderDetails;
    psHeaderDetails = TraverseStructure(spMyApiStruct,u8Type);
    if (minmax ==1)return psHeaderDetails->u32sVersionNoMax ;
    else
        return psHeaderDetails->u32sVersionNoMin ;
}

/*****************************************************************************
* @note       Function name	: uint16_t CalculateCheckSum(uint8_t *, uint16_t )
* @returns    returns				: 16 bit checksum
*
*
* @param      arg1					: datapointer to the buffer
* @param      arg2					: length of data
* @param      arg3					:
* @author			        			: SKS
* @date       date  Created	: 03-11-2017
* @brief      Description		: calculate 16 bit checksum of the supplied data
*
* @note       Notes					: none
*****************************************************************************/
uint16_t CalculateCheckSum(uint8_t *StartBuffer, uint16_t Length)
{
    /*Optimized CRC-16 calculation.*/

    /*Polynomial: x^16 + x^15 + x^2 + 1 (0xa001)*/
    /*Initial value: 0xffff*/
    /* This CRC is normally used in disk-drive controllers.*/

    uint8_t test_cnt;
    uint16_t CntIndex;
    uint16_t uiCRC_Byte;

    uiCRC_Byte = 0xFFFF;

    for(CntIndex = 0; CntIndex < Length; CntIndex++)
    {
        //EXOR word constant with CRC_test
        uiCRC_Byte = uiCRC_Byte  ^ *StartBuffer;

        // Test for all 16 - bits
        for(test_cnt=16; test_cnt !=0; test_cnt--)
        {
            if( uiCRC_Byte & 0x0001)
            {
                uiCRC_Byte ^= 0xA001;  //CRC_polynomial ; //A001
            }
            uiCRC_Byte = (uiCRC_Byte>>1);
        }

        StartBuffer++;
    }
    return(uiCRC_Byte);
}



area_t area_details;

void	ListSectors(struct_myapi* spMyApiStruct, char* strType)
{
    unsigned char u8Type;
		if (!strncmp(strType,"TOTALS",6))
		{
		u8Type =enum_totals;
		}
		if (!strncmp(strType,"VARIABLES",8))
		{
		u8Type =enum_variables;
		}
		if (!strncmp(strType,"PERIOD",6))
		{
		u8Type =enum_periodic_log;
		}
		if (!strncmp(strType,"SYS_LOG",6))
		{
		u8Type =enum_system_log;
		}
		
		switch (u8Type)
    {
    case enum_totals:
        area_details.no_of_backups  = spMyApiStruct->sMemConfig.sNo_of_Backups.totals;
        area_details.no_of_cyclic_sectors = spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.totals;
        area_details.size_of_sector = spMyApiStruct->sMemConfig.sSize_of_sector;
        area_details.base_address= spMyApiStruct->sMemConfig.s_Base_address+ spMyApiStruct->sMemConfig.sBaseOffset.totals;
        break;
    case enum_periodic_log:
        area_details.no_of_backups  = spMyApiStruct->sMemConfig.sNo_of_Backups.periodic_log;
        area_details.no_of_cyclic_sectors = spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.periodic_log;
        area_details.size_of_sector = spMyApiStruct->sMemConfig.sSize_of_sector;
        area_details.base_address= spMyApiStruct->sMemConfig.s_Base_address+ spMyApiStruct->sMemConfig.sBaseOffset.periodic_log;
        break;
    case enum_system_log:
        area_details.no_of_backups  = spMyApiStruct->sMemConfig.sNo_of_Backups.sys_log;
        area_details.no_of_cyclic_sectors = spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.sys_log;
        area_details.size_of_sector = spMyApiStruct->sMemConfig.sSize_of_sector;
        area_details.base_address= spMyApiStruct->sMemConfig.s_Base_address+ spMyApiStruct->sMemConfig.sBaseOffset.sys_log;
				
        break;
    case enum_variables:
		area_details.no_of_backups  = spMyApiStruct->sMemConfig.sNo_of_Backups.variables;
        area_details.no_of_cyclic_sectors = spMyApiStruct->sMemConfig.sNo_of_Cyclic_Storage.variables;
        area_details.size_of_sector = spMyApiStruct->sMemConfig.sSize_of_sector;
        area_details.base_address= spMyApiStruct->sMemConfig.s_Base_address+ spMyApiStruct->sMemConfig.sBaseOffset.variables;
        break;
    }

}
/*
Todo :
1.	SYNC USB from FLASH
=====================================
*/

