#ifndef _FLASH_RW_H
#define _FLASH_RW_H

#include <stdint.h>
#include "Screen_global_ex.h"
#include "Ext_flash_low_level.h"
#include "File_update.h"

/* Defines Section */
#define BACKUP_RESTORE_API_VER			1																			//
#define NOR_FLASH_BASE_ADDRESS			0x80000000														//as per rev4 board 05
#define SIZE_OF_SECTOR							0x20000																//128 kb
#define NO_OF_BACKUP_P_LOG					0																			//number of backups for periodic log
#define NO_OF_BACKUP_SYS_LOG				1																			// number of backups for system log
#define NO_OF_BACKUP_TOTALS					3																			// number of backups for totals
#define NO_OF_BACKUP_VAR						3																			// number of backups for variables
#define NO_OF_CYC_SECTORS_P_LOG			24																		// number of cyclic sectors for periodic logs
#define NO_OF_CYC_SECTORS_SYS_LOG		6																			// number of cyclic sectors for system log
#define NO_OF_CYC_SECTORS_TOTALS		2																			// number of cyclic sectors for Totals
#define NO_OF_CYC_SECTORS_VAR				2																			// number of cyclic sectors for variables
#define NO_OF_ROLLBACKS_P_LOG				5																			// number of rollback allowed for periodic log
#define NO_OF_ROLLBACKS_SYS_LOG			0																			// number of rollback allowed for system log
#define NO_OF_ROLLBACKS_TOTALS			5																			// number of rollback allowed for totals
#define NO_OF_ROLLBACKS_VAR					5																			// number of rollback allowed for variabls 
#define MIN_VERSION_FIND						0																			// Minimum value of MIN/MAX option
#define MAX_VERSION_FIND						1																			// maximum values of Min/Max option
#define BASE_OFFSET_P_LOG						0x00600000														// Base offset for periodic log records
#define BASE_OFFSET_SYS_LOG					0x00500000														// Base offset for System Log Records
#define BASE_OFFSET_TOTALS					0x00400000														// Base Offset for Totals Records
#define BASE_OFFSET_VAR							0x00300000														// base offset for Variables records
#define VALID_HEADER 								0xAA55																// Key to Identify Valid data
#ifndef TRUE
#define TRUE                           ( BOOL )1
#endif

#ifndef FALSE
#define FALSE                          ( BOOL )0
#endif

enum
{
    enum_totals = 0,																																						// select totals records
    enum_variables,																																							// select Variables records
    enum_periodic_log,																																					// select periodic log records
    enum_system_log																																							// select system log records
};

typedef struct RecHeader
{
    uint16_t u16structHeader;
    uint16_t u16structFirmwareversion;
    uint16_t u16Size;
    uint32_t u32sVersionNo;
} sRecordHeader;


typedef struct RecFooter
{
  uint32_t u32prevStructAddress;
	uint16_t CRC;
} sRecordFooter;
typedef struct RecDetails
{
    uint16_t u16structHeader;
    uint16_t u16structFirmwareversion;
    uint16_t u16Size;
    uint32_t u32sVersionNo;
    uint32_t u32sVersionNoMin;
    uint32_t u32sVersionNoMax;
		uint32_t u32firstRecordAddress;
		uint32_t u32lastrecordAddress;
} sRecordDetails;
typedef struct sTotalBackup_t
{
	sRecordHeader sHeader;
	TOTALS_STRUCT totals;
	float Zero_Weight ;
	float Accum_weight;
	struct sTotalBackup_t *backLink;
	uint16_t CRC;
}sTotalBackup;

typedef struct sVarBackup_t
{
	sRecordHeader sHeader;
	ADMIN_STRUCT admin_struct;  
	SETUP_DEVICES_STRUCT setup_device_struct;
	CALIBRATION_STRUCT calib_struct;
	SCALE_SETUP_STRUCT scale_setup_struct;
	struct sVarBackup_t* backLink;
	uint16_t CRC;
} sVarBackup;

typedef struct sPerodicLogBackup_t
{
	sRecordHeader sHeader;
	PERIODIC_LOG_STRUCT sPeriodicData;
	struct sPerodicLogBackup_t* backLink;
	uint16_t CRC;
} sPerodicLogBackup;
typedef struct sSysLogBackup_t
{
	sRecordHeader sHeader;
	char* strSysLog;
	struct sSysLogBackup_t* backLink;
	uint16_t CRC;
} sSysLogBackup;

typedef struct 
{
uint32_t totals ;
uint32_t			variables;
uint32_t			sys_log;
uint32_t			periodic_log ;
}s_u32ValType;

typedef struct
{
	s_u32ValType min;
	s_u32ValType max;
} sVersionNoType;

typedef struct
{
	s_u32ValType firstRecord;
	s_u32ValType BackLink;
	s_u32ValType CurrLink;
}sAddrType;

typedef struct
{
	uint32_t s_Base_address;
	uint32_t sSize_of_sector;
	s_u32ValType sBaseOffset;
	s_u32ValType sNo_of_Backups;
	s_u32ValType sNo_of_Cyclic_Storage;
}sMemConfigType;


typedef struct myapi_type
{
    uint32_t u32ApiVerNo;
    sTotalBackup sTotals;
		sVarBackup 	sVar;
		sPerodicLogBackup sPerodicLog;
		sSysLogBackup sSysLog;
		sVersionNoType sVersionNo;
		sAddrType sAddr;
		sMemConfigType sMemConfig;
		s_u32ValType sNo_of_Rollbacks;
} struct_myapi;
typedef struct
{
    int no_of_backups;
    int no_of_cyclic_sectors;
    int size_of_sector;
    int base_address;
} area_t;
sRecordDetails* TraverseStructure(struct_myapi* ,unsigned char );
uint16_t 	CalculateCheckSum(uint8_t *ptr,uint16_t u16Size);
int getVersionNo(struct_myapi*,unsigned char ,int );									//returns the minimum or maximum version of the
void	ListSectors(struct_myapi* , char* );
void myApi_Init(struct_myapi* spMyApiStruct)	;
void Init_Api_test(void);
extern struct_myapi myApi;
int AppendRecord(struct_myapi* spMyApiStruct,unsigned char u8Type);
void Append_debug(struct_myapi* spMyApiStruct, char* strType);
#endif
