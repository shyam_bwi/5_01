/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : MVT.c
* @brief               : Controller Board
*
* @author              : Oaces Team
*
* @date Created        : January Monday, 2016  <Jan 25, 2016>
* @date Last Modified  : January Monday, 2016  <Jan 25, 2016>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            : 
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stddef.h>
#include <float.h>
#include "Global_ex.h"
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"
#include "Log_report_data_calculate.h"
#include "Conversion_functions.h"
#include "math.h"
#include "MVT.h"
#include "MVT_Totals.h"
#include "EEPROM_high_level.h"

#ifdef MVT_TABLE
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables*/
unsigned int Current_val;
/*===========================================================================*/
/* Constants section */
 //Scale Setup parameters Min Max and Default constant values
 const float Conveyor_angle_Min = 0.000; //1
 const float Conveyor_angle_Max = 90.000;
 const float Conveyor_angle_Def = 12.000;
 
 const float Idler_distance_Min = 0.000; //1.000; //2
 const float Idler_distance_Max = 350; //100.000;
 const float Idler_distance_Def = 48.000;
 
 const float Wheel_diameter_Min = 0.100;//0.000; //1.000; //3
 const float Wheel_diameter_Max = 84.000;
 const float Wheel_diameter_Def = 7.970;
 
 const float Pulses_Per_Revolution_Min = 1.000; //4
 const float Pulses_Per_Revolution_Max = 1024.000;
 const float Pulses_Per_Revolution_Def = 100.000;
 
 const float User_Belt_Speed_Min = 0.000; //5
 const float User_Belt_Speed_Max = 2500.000;
 const float User_Belt_Speed_Def = 0.000;
 
 const unsigned int Run_mode_Min = RUN_MODE_WEIGHT_RATE; //6
 const unsigned int Run_mode_Max = RUN_MODE_WEIGHT;
 const unsigned int Run_mode_Def = RUN_MODE_WEIGHT_RATE;
 
 const unsigned int Load_cell_size_Min = LC_SIZE_45KG; //7
 const unsigned int Load_cell_size_Max = LC_SIZE_CUSTOM; 
 const unsigned int Load_cell_size_Def = LC_SIZE_100KG; 
 
 const unsigned int Distance_unit_Min = DISTANCE_UNIT_ENGLISH; //8
 const unsigned int Distance_unit_Max = DISTANCE_UNIT_METRIC;
 const unsigned int Distance_unit_Def = DISTANCE_UNIT_ENGLISH;
 
 const unsigned int Weight_unit_Min = WEIGHT_UNIT_TONS; //9
 const unsigned int Weight_unit_Max = WEIGHT_UNIT_KG;
 const unsigned int Weight_unit_Def = WEIGHT_UNIT_TONS;
 
 const unsigned int Weight_Def_unit = TONS;
	
 const unsigned int Rate_time_unit_Min = RATE_UNIT_HOURS;//10
 const unsigned int Rate_time_unit_Max = RATE_UNIT_MIN;
 const unsigned int Rate_time_unit_Def = RATE_UNIT_HOURS;
 
 const unsigned int Rate_time_Def_unit = Screen26_str2;
 
 const unsigned int Custom_LC_unit_Min = CUSTOM_LC_UNIT_LBS;//11
 const unsigned int Custom_LC_unit_Max = CUSTOM_LC_UNIT_KG;
 const unsigned int Custom_LC_unit_Def = CUSTOM_LC_UNIT_KG;
 
 const unsigned int Custom_LC_capacity_Def_unit = KG;
 
 const unsigned int Idler_unit_Min = IDLER_DISTANCE_UNIT_INCHES;//12
 const unsigned int Idler_unit_Max = IDLER_DISTANCE_UNIT_METER;
 const unsigned int Idler_unit_Def = IDLER_DISTANCE_UNIT_INCHES;
 
 const unsigned int Idler_Def_unit = INCHES;
 
 const unsigned int Wheel_diameter_unit_Min = WHEEL_DIAMETER_INCHES; //13
 const unsigned int Wheel_diameter_unit_Max = WHEEL_DIAMETER_CNTIMETERS;
 const unsigned int Wheel_diameter_unit_Def = WHEEL_DIAMETER_INCHES;
 
 const unsigned int Wheel_diameter_Def_unit = INCHES;
 
 const unsigned int Belt_length_unit_Min = BELT_LENGTH_UNIT_FEET; //14
 const unsigned int Belt_length_unit_Max = BELT_LENGTH_UNIT_METER;
 const unsigned int Belt_length_unit_Def = BELT_LENGTH_UNIT_FEET; 
 
  const unsigned int Belt_length_Def_unit = FEET;
	
 const unsigned int Speed_unit_Min = SPEED_UNIT_FEET_PER_MIN; //15
 const unsigned int Speed_unit_Max = SPEED_UNIT_METER_PER_MIN;
 const unsigned int Speed_unit_Def = SPEED_UNIT_FEET_PER_MIN;
 
 const unsigned int Speed_Def_unit =  FEET; //speed_unit_str1;
 
 const unsigned int AngleSensor_Install_status_Min = ANGLE_SENSOR_INSTALLED; //16
 const unsigned int AngleSensor_Install_status_Max = ANGLE_SENSOR_NOT_INSTALLED;
 const unsigned int AngleSensor_Install_status_Def = ANGLE_SENSOR_INSTALLED;
 
 const unsigned int Number_of_idlers_Min = 1; //NUMBER_OF_IDLERS_1; //17
 const unsigned int Number_of_idlers_Max = 4; //NUMBER_OF_IDLERS_4;
 const unsigned int Number_of_idlers_Def = 1; //NUMBER_OF_IDLERS_1;
 
 const unsigned int Custom_LC_capacity_Min = 0.000; //5; //18
 const unsigned int Custom_LC_capacity_Max = 10000;
 const unsigned int Custom_LC_capacity_Def = 200;
 
 const unsigned int Custom_LC_output_Min =  0;//1; //19
 const unsigned int Custom_LC_output_Max = 50;
 const unsigned int Custom_LC_output_Def = 2;
 
 const unsigned int Decimal_digits_Min = DECIMAL_DIGITS_NONE;   // 20
 const unsigned int Decimal_digits_Max = DECIMAL_DIGITS_THREE;   
 const unsigned int Decimal_digits_Def = DECIMAL_DIGITS_ONE;  
 
 const unsigned int IO_board_install_Min = IO_BOARD_INSTALLED; // 21
 const unsigned int IO_board_install_Max = IO_BOARD_NOT_INSTALL;
 const unsigned int IO_board_install_Def = IO_BOARD_INSTALLED;
 
 const float Idler_distanceA_Min = 6.000;//22
 const float Idler_distanceA_Max = 84.000;
 const float Idler_distanceA_Def = 48.000;
 
 const float Idler_distanceB_Min = 6.000;//23
 const float Idler_distanceB_Max = 84.000;
 const float Idler_distanceB_Def = 48.000;
 
 const float Idler_distanceC_Min = 6.000; //24
 const float Idler_distanceC_Max = 84.000;
 const float Idler_distanceC_Def = 48.000;
 
 const float Idler_distanceD_Min = 6.000;//25
 const float Idler_distanceD_Max = 84.000;
 const float Idler_distanceD_Def = 48.000;
 
 const float Idler_distanceE_Min = 6.000;//26
 const float Idler_distanceE_Max = 84.000;
 const float Idler_distanceE_Def = 48.000;

 const float User_Belt_Speeed_WD_Min = 0.000;//27
 const float User_Belt_Speeed_WD_Max = FLT_MAX; //2500.000;
 const float User_Belt_Speeed_WD_Def = 7.970;
  

 
 const float Test_weight_Min =  0.000;//5.000;
 const float Test_weight_Max = 2000.000;
 const float Test_weight_Def = 100.000;
 
const float Belt_scale_weight_Min = 0.000; //1.000;
const float Belt_scale_weight_Max = 500000000.000;
const float Belt_scale_weight_Def = 1.000;

const float Cert_scale_weight_Min = 0.000; //1.000;
const float Cert_scale_weight_Max = 500000000.000;
const float Cert_scale_weight_Def = 1.000;

const float Old_belt_length_Min = 10;//1.000;
const float Old_belt_length_Max = 9999;
const float Old_belt_length_Def = 10;

const float New_belt_length_Min = 10;//1.000;
const float New_belt_length_Max = 9999;
const float New_belt_length_Def = 10;

const float Belt_length_diff_Min = -10000;
const float Belt_length_diff_Max = 10000;
const float Belt_length_diff_Def = 0.000;

const float Old_zero_value_Min = 0.1;//1.000;
const float Old_zero_value_Max = 99999.0;
const float Old_zero_value_Def = 1.000;

const float New_zero_value_Min = 0.1; //1.000;
const float New_zero_value_Max = 99999.0;
const float New_zero_value_Def = 1.000;

const float Zero_diff_Min = -19999;
const float Zero_diff_Max = 19999;
const float Zero_diff_Def = 0.000;

const float old_span_value_Min = 0.1;
const float old_span_value_Max = 1000000;
const float old_span_value_Def = 1.000;

const float new_span_value_Min = 0.1;
const float new_span_value_Max = 1000000;
const float new_span_value_Def = 1.000;

const float span_diff_Min = -999999.9;
const float span_diff_Max = 999999.900;
const float span_diff_Def = 0.000;

const float real_time_rate_Min = -20000000.000;
const float real_time_rate_Max = 20000000.000;
const float real_time_rate_Def = 0.000;

 const unsigned int Belt_scale_unit_Min = WEIGHT_UNIT_TONS;		//NCY
 const unsigned int Belt_scale_unit_Max = WEIGHT_UNIT_KG;		//NCY
const unsigned int Belt_scale_unit_Def = WEIGHT_UNIT_TONS;		//NCY

const unsigned int Belt_scale_Def_unit = TONS;

// const unsigned int Cert_scale_unit_Min = 1;		//NCY
// const unsigned int Cert_scale_unit_Max = 1;		//NCY
const unsigned int Cert_scale_unit_Def = WEIGHT_UNIT_TONS;		//NCY

const unsigned int Cert_scale_Def_unit = TONS;

// const unsigned int Test_zero_weight_unit_Min = 1;		//NCY
// const unsigned int Test_zero_weight_unit_Max = 1;		//NCY
const unsigned int Test_zero_weight_unit_Def = WEIGHT_UNIT_LBS;		//NCY

const unsigned int Test_zero_weight_Def_unit = LBS;		//NCY

//Admin Section
const U16 Config_Date_Year_Min = DATE_YR_MIN;       // Year     [1980..2107]
const U16 Config_Date_Year_Max = DATE_YR_MAX;
const U16 Config_Date_Year_Def = DATE_YR_DEF;

const U8 Config_Date_Mon_Min = DATE_MON_MIN;        // Month    [1..12]                  
const U8 Config_Date_Mon_Max = DATE_MON_MAX;
const U8 Config_Date_Mon_Def = DATE_MON_DEF;

const U8 Config_Date_Day_Min = DATE_DATE_MIN;
const U8 Config_Date_Day_Max = DATE_DATE_MAX;
const U8 Config_Date_Day_Def = DATE_DATE_DEF;

//TIME_STRUCT Config_Time;
const U8 Config_Time_Hr_Min = TIME_HR_MIN;         // Hours    [0..23]
const U8 Config_Time_Hr_Max = TIME_HR_MAX;
const U8 Config_Time_Hr_Def = TIME_HR_DEF;

// const U8 Config_Time_AM_PM_Min = (U8)TIME_AM_PM_MIN;		// Added on 20-Oct-2015 //Dummy Data	This is essential for packing structure
// const U8 Config_Time_AM_PM_Max = (U8)TIME_AM_PM_MAX;
// const U8 Config_Time_AM_PM_Def = (U8)TIME_AM_PM_DEF;

const U8 Config_Time_AM_PM_Min = AM;		// Added on 20-Oct-2015 //Dummy Data	This is essential for packing structure
const U8 Config_Time_AM_PM_Max = PM;
const U8 Config_Time_AM_PM_Def = AM;

const U8 Config_Time_Min_Min = TIME_MINT_MIN;      // Minutes  [0..59] 
const U8 Config_Time_Min_Max = TIME_MINT_MAX;
const U8 Config_Time_Min_Def = TIME_MINT_DEF;

const U8 Config_Time_Sec_Min = TIME_SEC_MIN; 
const U8 Config_Time_Sec_Max = TIME_SEC_MAX;
const U8 Config_Time_Sec_Def = TIME_SEC_DEF;

//IP_STRUCT IP_Addr;
const IP_STRUCT IP_Addr_Min = {1, 1, 1, 0};
const IP_STRUCT IP_Addr_Max = {255, 255, 255, 250};
const IP_STRUCT IP_Addr_Def = {1, 1, 1, 1}; //{192, 168, 1, 100};

//unsigned int Aggre_IP_Addr_Min;
//unsigned int Aggre_IP_Addr_Max;

const IP_STRUCT Subnet_Mask_Min = {255, 0, 0, 0};
const IP_STRUCT Subnet_Mask_Max = {255, 255, 255, 254};	//<<SKS 
const IP_STRUCT Subnet_Mask_Def = {255, 255, 255, 0};

//unsigned int Aggre_Subnet_Mask_Min;
//unsigned int Aggre_Subnet_Mask_Max;

const IP_STRUCT Gateway_Min = {1, 1, 1, 0};
const IP_STRUCT Gateway_Max = {255, 255, 255, 250};
const IP_STRUCT Gateway_Def = {1, 1, 1, 1}; //{192, 168, 1, 1};

//unsigned int Aggre_Gateway_Min;
//unsigned int Aggre_Gateway_Max;

//const U8 IP_Addr_Min = IP_ADDR_MIN;		
//const U8 IP_Addr_Max = IP_ADDR_MAX;	
//const U8 IP_Addr_Def = IP_ADDR_DEF;	

// const U8 IP_Addr_Byte_Min = IP_ADDR_BYTE_MIN;
// const U8 IP_Addr_Byte_Max = IP_ADDR_BYTE_MAX;
// const U8 IP_Addr_Byte_Def = IP_ADDR_BYTE_DEF;

// const U8 IP_Addr_Byte1_Def = 192;
// const U8 IP_Addr_Byte2_Def = 168;
// const U8 IP_Addr_Byte3_Def = 1;
// const U8 IP_Addr_Byte4_Def = 100;

// const U8 Subnet_Mask_Byte1_Def = 255;
// const U8 Subnet_Mask_Byte2_Def = 255;
// const U8 Subnet_Mask_Byte3_Def = 255;
// const U8 Subnet_Mask_Byte4_Def = 0;

// const U8 Gateway_Byte1_Def = 192;
// const U8 Gateway_Byte2_Def = 168;
// const U8 Gateway_Byte3_Def = 1;
// const U8 Gateway_Byte4_Def = 1;

// const U8 IP_Addr_Addr3_Id,
// const U8 IP_Addr_Addr4_Id,
// 		//IP_STRUCT Subnet_Mask;
// const U8 Subnet_Mask_Addr1_Id,
// const U8 Subnet_Mask_Addr2_Id,
// const U8 Subnet_Mask_Addr3_Id,
// const U8 Subnet_Mask_Addr4_Id,
// 		//IP_STRUCT Gateway;
// const U8 Gateway_Addr1_Id,
// const U8 Gateway_Addr2_Id,
// const U8 Gateway_Addr3_Id,
// const U8 Gateway_Addr4_Id,

const float Auto_zero_tolerance_Min = 0.0;				//NCY
const float Auto_zero_tolerance_Max = 5.0;		//NCY
const float Auto_zero_tolerance_Def = 1.0;				//NCY

const float Negative_rate_limit_Min = -20000000.000;
const float Negative_rate_limit_Max = 20000000.000;
const float Negative_rate_limit_Def = 0.0;

const float Negative_rate_time_Min = 0.000;
const float Negative_rate_time_Max = 100.000;
const float Negative_rate_time_Def = 10.0;

const float Zero_rate_limit_Min = -20000000.000;
const float Zero_rate_limit_Max = 20000000.000;
const float Zero_rate_limit_Def = -5;

// const char Int_firmware_version[INT_FIRM_VER_NOC];
// const char Int_update_version[INT_UPD_VER_NOC];
// const char Scale_Name[SCALE_NAME_NOC];
// const char Plant_Name[PLANT_NAME_NOC];
// const char Product_Name[PRODUCT_NAME_NOC];
// const char Password[PASSWD_NOC];
// const char New_Password[NEW_PASSWD_NOC];
// const char Verify_New_Password[VERIFY_PASSWD_NOC];
// const char Int_update_file_name[INT_UPD_FILE_NAME_NOC];
// const char Reload_file_name[RELOAD_FILE_NAME_NOC];
// const char Backup_file_name[BACKUP_FILE_NAME_NOC];


const unsigned int Lock_Status_Min = LOCK;
const unsigned int Lock_Status_Max = UNLOCK;
const unsigned int Lock_Status_Def = UNLOCK;

// const unsigned int Admin_Locked_Min;
// const unsigned int Admin_Locked_Max;
// const unsigned int Admin_Locked_Def;

const unsigned int AM_PM_Min = (unsigned int)TIME_AM_PM_MIN;
const unsigned int AM_PM_Max = (unsigned int)TIME_AM_PM_MAX;
const unsigned int AM_PM_Def = (unsigned int)TIME_AM_PM_DEF;

// const unsigned int Calibration_Locked_Min;
// const unsigned int Calibration_Locked_Max;
// const unsigned int Calibration_Locked_Def;

// const unsigned int Clear_Weight_Locked_Min;
// const unsigned int Clear_Weight_Locked_Max;
// const unsigned int Clear_Weight_Locked_Def;

const unsigned int Current_Time_Format_Min = H12_HR;
const unsigned int Current_Time_Format_Max = H24_HR;
const unsigned int Current_Time_Format_Def = H24_HR;

const unsigned int Current_Date_Format_Min = FMT_MMDDYYYY;
const unsigned int Current_Date_Format_Max = FMT_DDMMYYYY;
const unsigned int Current_Date_Format_Def = FMT_MMDDYYYY;

const unsigned int Day_of_week_Min = DOW_SUNDAY;
const unsigned int Day_of_week_Max = DOW_SATURDAY;
const unsigned int Day_of_week_Def = DOW_SUNDAY;

const unsigned int DHCP_Status_Min = DHCP_DISABLE;
const unsigned int DHCP_Status_Max = DHCP_ENABLE;
const unsigned int DHCP_Status_Def = DHCP_ENABLE;

// const unsigned int Setup_Devices_Locked_Min;
// const unsigned int Setup_Devices_Locked_Max;
// const unsigned int Setup_Devices_Locked_Def;

// const unsigned int Setup_Wizard_Locked_Min;
// const unsigned int Setup_Wizard_Locked_Max;
// const unsigned int Setup_Wizard_Locked_Def;

// const unsigned int Zero_Calibration_Locked_Min;
// const unsigned int Zero_Calibration_Locked_Max;
// const unsigned int Zero_Calibration_Locked_Def;

const unsigned int Zero_rate_status_Min = ZERO_RATE_ON;
const unsigned int Zero_rate_status_Max = ZERO_RATE_OFF;
const unsigned int Zero_rate_status_Def = ZERO_RATE_OFF;

const U8 MBS_TCP_slid_Min = 1;
const U8 MBS_TCP_slid_Max = 247;
const U8 MBS_TCP_slid_Def = 247;

const unsigned int Test_Spd_Load_status_Min = STATUS_ON;
const unsigned int Test_Spd_Load_status_Max = STATUS_OFF;
const unsigned int Test_Spd_Load_status_Def = STATUS_OFF;

// const unsigned int Test_load_status_Min = TEST_SPD_LOAD_ON;
// const unsigned int Test_load_status_Max = TEST_SPD_LOAD_OFF;
// const unsigned int Test_load_status_Def = TEST_SPD_LOAD_OFF;

const float Test_load_Min =  0.000;//1.0;
const float Test_load_Max = 20000.0;
const float Test_load_Def = 1.0;

const float Test_speed_Min = 0.000; //1.0;
const float Test_speed_Max = 5000.0;
const float Test_speed_Def = 1.0;

 const unsigned int Language_sel_min = ENGLISH; //8
 const unsigned int Language_sel_max = POLISH;
 const unsigned int Language_sel_def = ENGLISH;


//Setup Device Section

const float Analog_Output_Setpoint_Min =  0.000;//1.0;
const float Analog_Output_Setpoint_Max = 10000.0;
const float Analog_Output_Setpoint_Def = 100.0;

const float Analog_Output_Maxrate_Min =  0.000;//1.0;
const float Analog_Output_Maxrate_Max = 20000000.0;
const float Analog_Output_Maxrate_Def = 500.0;

const float Analog_Output_ZeroCal_Min = 0.0;
const float Analog_Output_ZeroCal_Max = 4.0;
const float Analog_Output_ZeroCal_Def = 0.0;

// float Analog_Output_2_Setpoint;
// float Analog_Output_2_Maxrate;
// float Analog_Output_2_ZeroCal;

const float Analog_Input_Maxrate_Min =  0.000;//1.0;
const float Analog_Input_Maxrate_Max = 20000000.0;
const float Analog_Input_Maxrate_Def = 500.0;

const float Cutoff_Min = 0.0;
const float Cutoff_Max = 20000000.0;
const float Cutoff_Def = 0.0;
// float Cutoff_4;
// float Cutoff_5;
// float Cutoff_6;
// float Cutoff_7;
// float Cutoff_8;

const float Load_weight_Min = 0.000;//1.0;
const float Load_weight_Max = 500000000.0;
const float Load_weight_Def = 1.0;
// float Load_weight_4;
// float Load_weight_5;
// float Load_weight_6;
// float Load_weight_7;
// float Load_weight_8;

const float MinMax_Rate_Setpoint_Min = -20000000.0;
const float MinMax_Rate_Setpoint_Max = 20000000;
const float MinMax_Rate_Setpoint_Def = 0.0;
// float MinMax_Rate_Setpoint_4;
// float MinMax_Rate_Setpoint_5;
// float MinMax_Rate_Setpoint_6;

const float MinMax_Speed_Setpoint_Min = 0.0;
const float MinMax_Speed_Setpoint_Max = 10000.0;
const float MinMax_Speed_Setpoint_Def = 0.0;
// float MinMax_Speed_Setpoint_4;
// float MinMax_Speed_Setpoint_5;
// float MinMax_Speed_Setpoint_6;

const float Min_Belt_Speed_Min = 0.0;
const float Min_Belt_Speed_Max = 10000.0;
const float Min_Belt_Speed_Def = 0.0;

const float Empty_Belt_Speed_Min = 0.0;
const float Empty_Belt_Speed_Max = 10000.0;
const float Empty_Belt_Speed_Def = 0.0;

const float Feeder_Max_Rate_Min = 0.000;//1.0;
const float Feeder_Max_Rate_Max = 20000000.0;
const float Feeder_Max_Rate_Def = 1.0;

const float Preload_Distance_Min = 0.0;
const float Preload_Distance_Max = 20000000.0;		//NCY
const float Preload_Distance_Def = 0.0;

const float Target_rate_Min = 0.0;
const float Target_rate_Max = 20000000;
const float Target_rate_Def = 0.0;

const float Target_Load_Min = 0.000;//1.0;
const float Target_Load_Max = 500000000.0;
const float Target_Load_Def = 1.0;

const float Analog_Output_Min = 0.0;
const float Analog_Output_Max = 24.0; //20.0;
const float Analog_Output_Def = 4.0;					//Need to be decided as per selection

//float Analog_Output2;

const unsigned int Analog_Output_Function_Min = UNASSIGNED_AO;
const unsigned int Analog_Output_Function_Max = MA_4_20;
const unsigned int Analog_Output_Function_Def = MA_4_20;


//unsigned int Analog_Output_2_Function;

const unsigned int Digital_Input_Function_Min = UNASSIGNED_DIN;
const unsigned int Digital_Input_Function_Max = ERR_ACK;
const unsigned int Digital_Input_Function_Def = UNASSIGNED_DIN;

// unsigned int Digital_Input_2_Function;
// unsigned int Digital_Input_3_Function;
// unsigned int Digital_Input_4_Function;

const unsigned int Digital_Output_Function_Min = UNASSIGNED_DO;
const unsigned int Digital_Output_Function_Max = ZERO_CALIB_DO;
const unsigned int Digital_Output_Function_Def = UNASSIGNED_DO;

//unsigned int Digital_Output_2_Function;
//unsigned int Digital_Output_3_Function;

const unsigned int error_alarm_Min = LOAD_CELL;
const unsigned int error_alarm_Max = ANY_ERR;
const unsigned int error_alarm_Def = ANY_ERR;

// unsigned int error_alarm_5;
// unsigned int error_alarm_4;
// unsigned int error_alarm_3;
// unsigned int error_alarm_2;
// unsigned int error_alarm_1;

const unsigned int Log_cal_data_Min = STATUS_ON;
const unsigned int Log_cal_data_Max = STATUS_OFF;
const unsigned int Log_cal_data_Def = STATUS_ON;

const unsigned int Log_run_time_data_Min = STATUS_ON;
const unsigned int Log_run_time_data_Max = STATUS_OFF;
const unsigned int Log_run_time_data_Def = STATUS_ON;

const unsigned int Log_err_data_Min = STATUS_ON;
const unsigned int Log_err_data_Max = STATUS_OFF;
const unsigned int Log_err_data_Def = STATUS_ON;

const unsigned int Log_zero_cal_data_Min = STATUS_ON;
const unsigned int Log_zero_cal_data_Max = STATUS_OFF;
const unsigned int Log_zero_cal_data_Def = STATUS_ON;

const unsigned int Log_clear_wt_data_Min = STATUS_ON;
const unsigned int Log_clear_wt_data_Max = STATUS_OFF;
const unsigned int Log_clear_wt_data_Def = STATUS_ON;

const unsigned int PID_Action_Min = REVERSE;
const unsigned int PID_Action_Max = FORWARD;
const unsigned int PID_Action_Def = REVERSE;

const unsigned int PID_Setpoint_Type_Min = LOCAL_TARGET;
const unsigned int PID_Setpoint_Type_Max = REMOTE_TARGET;
const unsigned int PID_Setpoint_Type_Def = LOCAL_TARGET;

const unsigned int Printer_flow_control_Min = FC_NONE;
const unsigned int Printer_flow_control_Max = FC_XON_XOFF;
const unsigned int Printer_flow_control_Def = FC_NONE;

const unsigned int Printer_status_Min = TICKETS_ON;
const unsigned int Printer_status_Max = TICKETS_OFF;
const unsigned int Printer_status_Def = TICKETS_ON;

const unsigned int Printer_type_Min = PSBTYPE_BELT_WAY;
const unsigned int Printer_type_Max = PSBTYPE_CUSTOM;
const unsigned int Printer_type_Def = PSBTYPE_BELT_WAY;

const unsigned int Rate_Output_Type_Min = MIN_RATE_OP;
const unsigned int Rate_Output_Type_Max = MAX_RATE_OP;
const unsigned int Rate_Output_Type_Def = MIN_RATE_OP;

// unsigned int Rate_Output_Type_2;
// unsigned int Rate_Output_Type_3;
// unsigned int Rate_Output_Type_4;
// unsigned int Rate_Output_Type_5;
// unsigned int Rate_Output_Type_6;

const unsigned int Relay_Output_Function_Min = ROF_UNASSIGNED;
const unsigned int Relay_Output_Function_Max = ROF_ZERO_CALIB;
const unsigned int Relay_Output_Function_Def = ROF_UNASSIGNED;

// unsigned int Relay_Output_2_Function;
// unsigned int Relay_Output_3_Function;

const unsigned int Speed_Output_Type_Min = MIN_SPD_OP;
const unsigned int Speed_Output_Type_Max = MAX_SPD_OP;
const unsigned int Speed_Output_Type_Def = MIN_SPD_OP;

// unsigned int Speed_Output_Type_2;
// unsigned int Speed_Output_Type_3;
// unsigned int Speed_Output_Type_4;
// unsigned int Speed_Output_Type_5;
// unsigned int Speed_Output_Type_6;

const unsigned int Scoreboard_display_data_Min = SB_DISP_RATE;
const unsigned int Scoreboard_display_data_Max = SB_DISP_ALT; //sks
const unsigned int Scoreboard_display_data_Def = SB_DISP_WEIGHT;

const unsigned int Scoreboard_type_Min = PSBTYPE_BELT_WAY;
const unsigned int Scoreboard_type_Max = PSBTYPE_CUSTOM;
const unsigned int Scoreboard_type_Def = PSBTYPE_BELT_WAY;

const unsigned int Scoreboard_TMMode_Min = STATUS_ON;
const unsigned int Scoreboard_TMMode_Max = STATUS_OFF;
const unsigned int Scoreboard_TMMode_Def = STATUS_OFF;

const unsigned int Units_Per_Pulse_Min = _0_1_;
const unsigned int Units_Per_Pulse_Max = _0_01_;
const unsigned int Units_Per_Pulse_Def = _0_01_;

// unsigned int Units_Per_Pulse_2;
// unsigned int Units_Per_Pulse_3;

const unsigned int Feed_Delay_Min = 0;
const unsigned int Feed_Delay_Max = 1000;		//NCY
const unsigned int Feed_Delay_Def = 1;

const unsigned int Network_Addr_Min = 0;
const unsigned int Network_Addr_Max = 10;
const unsigned int Network_Addr_Def = 2;

const float Percent_Ingredient_Min = 0.0;
const float Percent_Ingredient_Max = 10000.0;
const float Percent_Ingredient_Def = 0.0;

const unsigned int Preload_Delay_Min = 0;
const unsigned int Preload_Delay_Max = 100;
const unsigned int Preload_Delay_Def = 1;

const unsigned int Periodic_log_interval_Min = 1;
const unsigned int Periodic_log_interval_Max = 1000;//15;
const unsigned int Periodic_log_interval_Def = 1;

const unsigned int PID_Channel_Min = 1;
const unsigned int PID_Channel_Max = 2;
const unsigned int PID_Channel_Def = 1;

const unsigned int PID_P_Term_Min = 1;
const unsigned int PID_P_Term_Max = 1000;
const unsigned int PID_P_Term_Def = 1;

const unsigned int PID_I_Term_Min = 0;
const unsigned int PID_I_Term_Max = 1000;
const unsigned int PID_I_Term_Def = 1;

const unsigned int PID_D_Term_Min = 0;
const unsigned int PID_D_Term_Max = 1000;
const unsigned int PID_D_Term_Def = 1;

const float PID_Local_Setpoint_Min = 0.000;//1.0;
const float PID_Local_Setpoint_Max = 20000000.0;
const float PID_Local_Setpoint_Def = 1.0;

const unsigned int Printer_ticket_num_Min = 1;
const unsigned int Printer_ticket_num_Max = (unsigned int)0xFFFFFFFF;
const unsigned int Printer_ticket_num_Def = 1;


const unsigned int Printer_baud_Min =  BAUD_9600;
const unsigned int Printer_baud_Max = BAUD_115200;
const unsigned int Printer_baud_Def = BAUD_9600;

const unsigned int Printer_data_bits_Min = DATA_BITS_8;
const unsigned int Printer_data_bits_Max = DATA_BITS_7;
const unsigned int Printer_data_bits_Def = DATA_BITS_8;

const unsigned int Printer_stop_bits_Min = STOP_BITS_1;
const unsigned int Printer_stop_bits_Max = STOP_BITS_2;
const unsigned int Printer_stop_bits_Def = STOP_BITS_1;


const unsigned int Pulse_on_time_Min = 1;
const unsigned int Pulse_on_time_Max = 1000;
const unsigned int Pulse_on_time_Def = 50;

// unsigned int Pulse_on_time_2;
// unsigned int Pulse_on_time_3;

const unsigned int Scoreboard_alt_delay_Min = 1;
const unsigned int Scoreboard_alt_delay_Max = 10;
const unsigned int Scoreboard_alt_delay_Def = 3;

const unsigned int Scoreboard_baud_Min = BAUD_9600;
const unsigned int Scoreboard_baud_Max = BAUD_115200;
const unsigned int Scoreboard_baud_Def = BAUD_9600;

const unsigned int Scoreboard_data_bits_Min = DATA_BITS_8;
const unsigned int Scoreboard_data_bits_Max = DATA_BITS_7;
const unsigned int Scoreboard_data_bits_Def = DATA_BITS_8;

const unsigned int Scoreboard_stop_bits_Min = STOP_BITS_1;
const unsigned int Scoreboard_stop_bits_Max = STOP_BITS_2;
const unsigned int Scoreboard_stop_bits_Def = STOP_BITS_1;

// char Addr1[24];
// char Addr2[24];
// char Addr3[24];
// char Company_name[24];
// char Phone[24];


const unsigned int Run_mode_enum_list[NO_OF_RUN_MODE] =
{
	Screen21_str2, 	//weight/rate
	Screen21_str8,	//Load out
	Screen21_str11,	//Belending
	Screen21_str14, //Load Control
	Screen21_str17, //Rate Control
	Screen21_str20, //Weight
};

const unsigned int Load_cell_size_enum_list[NO_OF_LC_SIZES] =
{
	Screen23_str2,
	Screen23_str5,
	Screen23_str8,
	Screen23_str11,
	Screen23_str14,
	Screen23_str17,
	Screen23_str20,
	Screen23_str23,
	Screen23_str26,
	Screen23_str29,
};
const unsigned int Distance_unit_enum_list[NO_OF_DISTANCE_UINTS] =
{
	Screen24_str2,
	Screen24_str5,
};

const unsigned int Weight_unit_enum_list[NO_OF_TOTAL_WT_UNITS] =
{
	TONS, //Screen25_str2,
	LONG_TON, //Screen25_str5,
	LBS, //Screen25_str8,
	TONNE, //Screen25_str9,
	KG, //Screen25_str12,
};

const unsigned int Rate_time_unit_enum_list[NO_OF_RATE_TIME_UNITS] =
{
	Screen26_str2,
	Screen26_str4,
};

const unsigned int Custom_LC_unit_enum_list[NO_OF_CUSTOM_LC_UNITS] =
{
	Screen23121_str2,
	Screen23121_str5,
};
const unsigned int Idler_unit_enum_list[NO_OF_IDLER_DISTANCE_UNITS] =
{
	Screen24_str8,
	Screen24_str10,
};
const unsigned int Wheel_diameter_unit_enum_list[NO_OF_WHEEL_DIAMETER_UNITS] =
{
	Screen24_str8,
	Screen24_str11,
};

const unsigned int Belt_length_unit_enum_list[NO_OF_BELT_LENGTH_UNITS] =
{
	Screen24_str9,
	Screen24_str10,
};

const unsigned int Speed_unit_enum_list[NO_OF_SPEED_UNITS] =
{
	//speed_unit_str1, 
	//speed_unit_str2, 
	Screen24_str9,
	Screen24_str10,
};

const unsigned int AngleSensor_Install_status_enum_list[NO_OF_ANGLESENSOR_INSTALLED] =
{
	//Screen2_str27, 
	//Screen2_str29,
	Screen271_M_str1,
	Screen271_M_str4,
};

const unsigned int Number_of_idlers_enum_list[NO_OF_IDLERS] =
{
	Screen22_str2, 
	Screen22_str5,
	Screen22_str8,
	Screen22_str11
};

const unsigned int Decimal_digits_enum_list[NO_OF_DECIMAL_DIGITS] =
{
	Screen2_str32, 
	Screen2_str21,
	Screen2_str22,
	Screen2_str23,
};

const unsigned int IO_board_install_enum_list[NO_OF_IO_BOARD_INSTALLED] =
{
	Screen2_str27,
  Screen2_str29,
};


//Suvrat parameters
const unsigned int AM_PM_enum_list[] =
{
	AM_TIME_FORMAT,
	PM_TIME_FORMAT,
};  

const unsigned int Lock_Status_enum_list[] =
{
	Screen631_str1,
	Screen631_str4,
}; 

const unsigned int Time_Format_enum_list[] =
{
	TWELVE_HR,
	TWENTYFOUR_HR,
};

const unsigned int Date_Format_enum_list[] =
{
	MMDDYYYY,
	DDMMYYYY,
};

const unsigned int Day_of_Week_enum_list[] =
{
  SUNDAY,                                  
  MONDAY,                                  
  TUESDAY,                                 
  WEDNESDAY,                               
  THURSDAY,                                
  FRIDAY,                                  
  SATURDAY,                                
};

const unsigned int DHCP_Status_enum_list[] =
{
	Screen661_str5,
	Screen661_str2,
};

const unsigned int Zero_Rate_Status_enum_list[] =
{
	Screen6171_str2,
	Screen6171_str5,
};

const unsigned int On_Off_status_enum_list[] =
{
	Screen431_str2,
	Screen431_str5,
};

//SKS changed name to AO1_Function_enum_list and added
//AO2_Function_enum_list to resolve BS210
const unsigned int AO1_Function_enum_list[] =	{
	Port_string3,			
	Screen44411_str2,
	Screen44411_str5     
};
const unsigned int AO2_Function_enum_list[] ={
	Port_string3,
	Screen44421_str2,
	Screen44421_str5
};

const unsigned int Din_Function_enum_list[] =
{
	Port_string3,
	Screen4411_str2,
	Screen4411_str5,
	Screen4411_str8,
	Screen4411_str11,
	Screen4411_str14,
	Screen4411_str17,
	Screen4411_str20
};

const unsigned int DO_Function_enum_list[] =
{
	Port_string3, 		// "Unassigned"
	Screen4421_str2, 	// "Pulsed Output"
	Screen4421_str5, 	// "Quadrature Wave"
	Screen4421_str8, 	// "Error Alarm"
	Screen4421_str11,	// "Min/Max Speed"
	Screen4421_str14, // "Min/Max Rate"
	Screen4421_str17,	// "Batching/Loadout"
	Screen4421_str20, // "Zero Calibration"
};

const unsigned int Err_Alm_enum_list[] =
{
	Screen44213_str2, 	// "Load Cell"
	Screen44213_str5, 	// "Angle Sensor"
	Screen44213_str8, 	// "Communications"
	Screen44213_str11,	// "Negative Rate"
	Screen44213_str14 	// "Any Error"
};

const unsigned int PID_Action_enum_list[] =
{
	Screen4512_str1,  // "Reverse"
	Screen4512_str4, 	// "Forward"
};

const unsigned int PID_Setpt_Type_enum_list[] =
{
	Screen4513_str1, // "Local Target"
	Screen4513_str7, // "4-20mA Input Target"
	Screen4513_str4, // "Remote Target"
};

const unsigned int Flow_Control_enum_list[] =
{
	Screen412214_str2, // "None"
	Screen412214_str4, // "Hardware"
	Screen412214_str7, // "Xon/Xoff"
};

const unsigned int Printer_Status_enum_list[] =
{
	Screen411_str1, //Tickets On
	Screen411_str4, //Ticket Off
};

const unsigned int Print_Type_enum_list[] =
{
	Screen4121_str1, //"Belt-Way"
	Screen4122_str1, //"Custom"
};

const unsigned int SB_Type_enum_list[] =
{
	Screen4221_str1, //"Belt-Way"
	Screen4222_str1, //"Custom"
};

const unsigned int Rate_OP_Type_enum_list[] =
{
	Screen442152_str1, // "Min Rate Output"
	Screen442152_str4, // "Max Rate Output"
};

const unsigned int Rly_OP_Func_enum_list[] =
{
	Port_string3, 		// "Unassigned"
	Screen4421_str8, 	// "Error Alarm"
	Screen4421_str11,	// "Min/Max Speed"
	Screen4421_str14, // "Min/Max Rate"
	Screen4421_str17,	// "Batching/Loadout"
	Screen4421_str20, // "Zero Calibration"
};

const unsigned int Speed_OP_Type_enum_list[] =
{
	Screen442142_str1, // "Min Speed Output"
	Screen442142_str4, // "Max Speed Output"			
};

const unsigned int SB_Disp_Data_enum_list[] =
{
	Screen421_str2,	//"Rate"
	Screen421_str5, //"Weight"
	Screen421_str8,			//sks "Alternate Wt & Rate"
};

const unsigned int Units_Per_Pulse_enum_list[] =
{
  Screen442111_str1,  //0.1 Weight Units
	Screen442111_str4,  //1.0 Weight Units
  Screen442111_str6,	//10 Weight Units
  Screen442111_str8,	//100 Weight Units
	Screen442111_str10, //0.01 Weight Units
};
/*
const unsigned int Baud_Rate_enum_list[] =
{
	Screen412211_str2, //"9600"
	Screen412211_str4, //"19200"
	Screen412211_str7, //"57600"
	Screen412211_str9, //"115200"
};
*/
const unsigned int Baud_Rate_enum_list[] =
{
	9600,
	19200,
	57600,
	115200,
};

const unsigned int Data_bits_enum_list[] =
{
	8,
	7
};

/*const unsigned int Data_bits_enum_list[] =
{
	Screen412212_str2, //"8"
	Screen412212_str4, //"7"
};


const unsigned int Stop_bits_enum_list[] =
{
	Screen412213_str2, //"1"
	Screen412213_str4, //"2"
};
*/
const unsigned int Stop_bits_enum_list[] =
{
	1,
	2
};

const unsigned int Language_enum_list[NO_OF_LANGUAGES] =
{
	Screen61B1_str0,
	Screen61B2_str0,
};


//Match the sequence of variable with SCALE_SETUP_STRUCT Scale_setup_var
DATA_VARIABLE_STRUCT Scale_var_Data[] __attribute__ ((section ("GUI_DATA_RAM")))=
{
		&Scale_setup_var.AngleSensor_Install_status,
		&Scale_setup_var.Distance_unit,
		&Scale_setup_var.User_Belt_Speed,
		&Scale_setup_var.Weight_unit,
		&Scale_setup_var.Idler_unit,
		&Scale_setup_var.Wheel_diameter_unit,
		&Scale_setup_var.Belt_length_unit,
		&Scale_setup_var.Speed_unit,
		&Scale_setup_var.Conveyor_angle,
		&Scale_setup_var.Idler_distance,
		&Scale_setup_var.Wheel_diameter,
		&Scale_setup_var.Pulses_Per_Revolution,
		&Scale_setup_var.Run_mode,
		&Scale_setup_var.Load_cell_size,
		&Scale_setup_var.Rate_time_unit,
		&Scale_setup_var.Custom_LC_unit,
		&Scale_setup_var.Number_of_idlers,
		&Scale_setup_var.Custom_LC_capacity,
		&Scale_setup_var.Custom_LC_output,
		&Scale_setup_var.Decimal_digits  ,
		&Scale_setup_var.IO_board_install ,
		&Scale_setup_var.Idler_distanceA,
		&Scale_setup_var.Idler_distanceB,
		&Scale_setup_var.Idler_distanceC,
		&Scale_setup_var.Idler_distanceD,
		&Scale_setup_var.Idler_distanceE,
		&Scale_setup_var.User_Belt_Speeed_WD,
		//&Scale_setup_var.CRC_SCALE_SETUP,
		
		//Calibration vars
		&Calibration_var.Belt_scale_unit,
		&Calibration_var.Cert_scale_unit,
		&Calibration_var.Test_zero_weight_unit,
		&Calibration_var.Test_weight,
		&Calibration_var.Belt_scale_weight,
		&Calibration_var.Cert_scale_weight,
		&Calibration_var.Old_belt_length,
		&Calibration_var.New_belt_length,
		&Calibration_var.Belt_length_diff,
		&Calibration_var.Old_zero_value,
		&Calibration_var.New_zero_value,
		&Calibration_var.Zero_diff,
		&Calibration_var.old_span_value,
		&Calibration_var.new_span_value,
		&Calibration_var.span_diff,
		&Calibration_var.real_time_rate,
		//&Calibration_var.CRC_CALIBRATION_STRUCT,
		
		//Admin Vars
		&Admin_var.Config_Date.Year,
		&Admin_var.Config_Date.Mon,
		&Admin_var.Config_Date.Day,
		&Admin_var.Config_Time.Hr,
		&Admin_var.Config_Time.AM_PM,
		&Admin_var.Config_Time.Min,
		&Admin_var.Config_Time.Sec,
		&Admin_var.IP_Addr.Addr1,
		//&Admin_var.IP_Addr.Addr2,
		//&Admin_var.IP_Addr.Addr3,
		//&Admin_var.IP_Addr.Addr4,
		&Admin_var.Subnet_Mask.Addr1,
		//&Admin_var.Subnet_Mask.Addr2,
		//&Admin_var.Subnet_Mask.Addr3,
		//&Admin_var.Subnet_Mask.Addr4,
		&Admin_var.Gateway.Addr1,
		//&Admin_var.Gateway.Addr2,
		//&Admin_var.Gateway.Addr3,
		//&Admin_var.Gateway.Addr4,
		&Admin_var.Auto_zero_tolerance,
		&Admin_var.Negative_rate_limit,
		&Admin_var.Negative_rate_time,
		&Admin_var.Zero_rate_limit,
		&Admin_var.Int_firmware_version[INT_FIRM_VER_NOC],
		&Admin_var.Int_update_version[INT_UPD_VER_NOC],
		&Admin_var.Scale_Name[SCALE_NAME_NOC],
		&Admin_var.Plant_Name[PLANT_NAME_NOC],
		&Admin_var.Product_Name[PRODUCT_NAME_NOC],
		&Admin_var.Password[PASSWD_NOC],
		&Admin_var.New_Password[NEW_PASSWD_NOC],
		&Admin_var.Verify_New_Password[VERIFY_PASSWD_NOC],
		&Admin_var.Int_update_file_name[INT_UPD_FILE_NAME_NOC],
		&Admin_var.Reload_file_name[RELOAD_FILE_NAME_NOC],
		&Admin_var.Backup_file_name[BACKUP_FILE_NAME_NOC],
		&Admin_var.Admin_Locked,
		&Admin_var.AM_PM,
		&Admin_var.Calibration_Locked,
		&Admin_var.Clear_Weight_Locked,
		&Admin_var.Current_Time_Format,
		&Admin_var.Current_Date_Format,
		&Admin_var.Day_of_week,
		&Admin_var.DHCP_Status,
		&Admin_var.Setup_Devices_Locked,
		&Admin_var.Setup_Wizard_Locked,
		&Admin_var.Zero_Calibration_Locked,
		&Admin_var.Zero_rate_status,
		&Admin_var.MBS_TCP_slid,
		&Admin_var.Test_speed_status,
		&Admin_var.Test_load_status,
		&Admin_var.Test_load,
		&Admin_var.Test_speed,
//		&Admin_var.Language_select,
		//&Admin_var.CRC_ADMIN,
		
		//Setup Devices var
		&Setup_device_var.Analog_Output_1_Setpoint,
		&Setup_device_var.Analog_Output_1_Maxrate,
		&Setup_device_var.Analog_Output_1_ZeroCal,
		&Setup_device_var.Analog_Output_2_Setpoint,
		&Setup_device_var.Analog_Output_2_Maxrate,
		&Setup_device_var.Analog_Output_2_ZeroCal,
		&Setup_device_var.Analog_Input_Maxrate,
		&Setup_device_var.Cutoff_1,
		&Setup_device_var.Cutoff_2,
		&Setup_device_var.Cutoff_3,
		&Setup_device_var.Cutoff_4,
		&Setup_device_var.Cutoff_5,
		&Setup_device_var.Cutoff_6,
		&Setup_device_var.Cutoff_7,
		&Setup_device_var.Cutoff_8,
		&Setup_device_var.Load_weight_1,
		&Setup_device_var.Load_weight_2,
		&Setup_device_var.Load_weight_3,
		&Setup_device_var.Load_weight_4,
		&Setup_device_var.Load_weight_5,
		&Setup_device_var.Load_weight_6,
		&Setup_device_var.Load_weight_7,
		&Setup_device_var.Load_weight_8,
		&Setup_device_var.MinMax_Rate_Setpoint_1,
		&Setup_device_var.MinMax_Rate_Setpoint_2,
		&Setup_device_var.MinMax_Rate_Setpoint_3,
		&Setup_device_var.MinMax_Rate_Setpoint_4,
		&Setup_device_var.MinMax_Rate_Setpoint_5,
		&Setup_device_var.MinMax_Rate_Setpoint_6,
		&Setup_device_var.MinMax_Speed_Setpoint_1,
		&Setup_device_var.MinMax_Speed_Setpoint_2,
		&Setup_device_var.MinMax_Speed_Setpoint_3,
		&Setup_device_var.MinMax_Speed_Setpoint_4,
		&Setup_device_var.MinMax_Speed_Setpoint_5,
		&Setup_device_var.MinMax_Speed_Setpoint_6,
		&Setup_device_var.Min_Belt_Speed,
		&Setup_device_var.Empty_Belt_Speed,
		&Setup_device_var.Feeder_Max_Rate,
		&Setup_device_var.Preload_Distance,
		&Setup_device_var.Target_rate,
		&Setup_device_var.Target_Load,
		&Setup_device_var.Analog_Output_1_Function,
		&Setup_device_var.Analog_Output_2_Function,
		&Setup_device_var.Analog_Output1,
		&Setup_device_var.Analog_Output2,
		&Setup_device_var.Digital_Input_1_Function,
		&Setup_device_var.Digital_Input_2_Function,
		&Setup_device_var.Digital_Input_3_Function,
		&Setup_device_var.Digital_Input_4_Function,
		&Setup_device_var.Digital_Output_1_Function,
		&Setup_device_var.Digital_Output_2_Function,
		&Setup_device_var.Digital_Output_3_Function,
		&Setup_device_var.error_alarm_6,
		&Setup_device_var.error_alarm_5,
		&Setup_device_var.error_alarm_4,
		&Setup_device_var.error_alarm_3,
		&Setup_device_var.error_alarm_2,
		&Setup_device_var.error_alarm_1,
		&Setup_device_var.Log_cal_data,
		&Setup_device_var.Log_run_time_data,
		&Setup_device_var.Log_err_data,
		&Setup_device_var.Log_zero_cal_data,
		&Setup_device_var.Log_clear_wt_data,
		&Setup_device_var.PID_Action,
		&Setup_device_var.PID_Setpoint_Type,
		&Setup_device_var.Printer_flow_control,
		&Setup_device_var.Printer_status,
		&Setup_device_var.Printer_type,
		&Setup_device_var.Rate_Output_Type_1,
		&Setup_device_var.Rate_Output_Type_2,
		&Setup_device_var.Rate_Output_Type_3,
		&Setup_device_var.Rate_Output_Type_4,
		&Setup_device_var.Rate_Output_Type_5,
		&Setup_device_var.Rate_Output_Type_6,
		&Setup_device_var.Relay_Output_1_Function,
		&Setup_device_var.Relay_Output_2_Function,
		&Setup_device_var.Relay_Output_3_Function,
		&Setup_device_var.Speed_Output_Type_1,
		&Setup_device_var.Speed_Output_Type_2,
		&Setup_device_var.Speed_Output_Type_3,
		&Setup_device_var.Speed_Output_Type_4,
		&Setup_device_var.Speed_Output_Type_5,
		&Setup_device_var.Speed_Output_Type_6,
		&Setup_device_var.Scoreboard_display_data,
		&Setup_device_var.Scoreboard_type,
		&Setup_device_var.Scoreboard_TMMode,
		&Setup_device_var.Units_Per_Pulse_1,
		&Setup_device_var.Units_Per_Pulse_2,
		&Setup_device_var.Units_Per_Pulse_3,
		&Setup_device_var.Feed_Delay,
		&Setup_device_var.Network_Addr,
		&Setup_device_var.Percent_Ingredient,
		&Setup_device_var.Preload_Delay,
		&Setup_device_var.Periodic_log_interval,
		&Setup_device_var.PID_Channel,
		&Setup_device_var.PID_P_Term,
		&Setup_device_var.PID_I_Term,
		&Setup_device_var.PID_D_Term,
		&Setup_device_var.PID_Local_Setpoint,
		&Setup_device_var.Printer_ticket_num,
		&Setup_device_var.Printer_baud,
		&Setup_device_var.Printer_data_bits,
		&Setup_device_var.Printer_stop_bits,
		&Setup_device_var.Pulse_on_time_1,
		&Setup_device_var.Pulse_on_time_2,
		&Setup_device_var.Pulse_on_time_3,
		&Setup_device_var.Scoreboard_alt_delay,
		&Setup_device_var.Scoreboard_baud,
		&Setup_device_var.Scoreboard_data_bits,
		&Setup_device_var.Scoreboard_stop_bits,
		&Setup_device_var.Addr1[24],
		&Setup_device_var.Addr2[24],
		&Setup_device_var.Addr3[24],
		&Setup_device_var.Company_name[24],
		&Setup_device_var.Phone[24],
		//&Setup_device_var.CRC_SETUP_DEVICES,
		&daily_rprt.Start_time.Hr,//Populated MVT table for report log parameters
	  &daily_rprt.Start_time.Min,
	  &daily_rprt.Start_time.Sec, 
    &daily_rprt.End_time.Hr, 
    &daily_rprt.End_time.Min,
	  &daily_rprt.End_time.Sec,
	  &daily_rprt.Start_load_time.Hr, 
	  &daily_rprt.Start_load_time.Min,
	  &daily_rprt.Start_load_time.Sec,
    &daily_rprt.End_load_time.Hr, 
	  &daily_rprt.End_load_time.Min,
	  &daily_rprt.End_load_time.Sec,
	  &daily_rprt.Down_time, 
	  &daily_rprt.Run_time, 
	  &weekly_rprt.Down_time,
    &weekly_rprt.Run_time, 
	  &monthly_rprt.Down_time,
    &monthly_rprt.Run_time, 
    &daily_rprt.freq_rprt_header.Date.Year, 
	  &daily_rprt.freq_rprt_header.Date.Mon, 
	  &daily_rprt.freq_rprt_header.Date.Day, 
	  &daily_rprt.freq_rprt_header.Time.Hr, 
	  &daily_rprt.freq_rprt_header.Time.Min,
	  &daily_rprt.freq_rprt_header.Time.Sec,
	  &weekly_rprt.freq_rprt_header.Date.Year,
	  &weekly_rprt.freq_rprt_header.Date.Mon, 
	  &weekly_rprt.freq_rprt_header.Date.Day, 
	  &weekly_rprt.freq_rprt_header.Time.Hr, 
	  &weekly_rprt.freq_rprt_header.Time.Min,
	  &weekly_rprt.freq_rprt_header.Time.Sec,
	  &monthly_rprt.freq_rprt_header.Date.Year,
	  &monthly_rprt.freq_rprt_header.Date.Mon, 
	  &monthly_rprt.freq_rprt_header.Date.Day, 
	  &monthly_rprt.freq_rprt_header.Time.Hr, 
	  &monthly_rprt.freq_rprt_header.Time.Min,
	  &monthly_rprt.freq_rprt_header.Time.Sec,
	  &Calculation_struct.run_time,
 
};



/* Boolean variables section */

/* Character variables section */
unsigned char  Param_set_to_default_flag = 0;
unsigned char  Scale_setup_param_set_to_default_flag = 0;
unsigned char  Calib_param_set_to_default_flag = 0;
unsigned char  Admin_param_set_to_default_flag = 0;
unsigned char  Setup_device_param_set_to_default_flag = 0;

//Variable �variable name� has value (value here) which is outside valid range (XXX to XXX).  Value is reset to default value = XXXXX
//char Error_log_option_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=  "Variable %c%s%c has value (%s) which is outside valid range(%s to %s). Value is reset to default value=%s";
char Error_log_option_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=  "Variable %c%s%c has an invalid option which is outside valid range(%s to %s). Value is reset to default value=%s";
char Error_log_float_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=   "Variable %c%s%c has value (%.3f) which is outside valid range(%.3f to %.3f). Value is reset to default value=%.3f";
char Error_log_double_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=  "Variable %c%s%c has value (%.3lf) which is outside valid range(%.3lf to %0.3lf). Value is reset to default value=%.3lf";
char Error_log_unsignedinteger_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))= "Variable %c%s%c has value (%u) which is outside valid range(%u to %u). Value is reset to default value=%u";
char Error_log_integer_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))= "Variable %c%s%c has value (%d) which is outside valid range(%d to %d). Value is reset to default value=%d";
//char Error_log_unsignedchar_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=    "Variable %c%s%c has value (%d) which is outside valid range(%d to %d). Value is reset to default value=%d";
char Error_log_IP_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=      "Variable %c%s%c has value (%d.%d.%d.%d) which is outside valid range(%d.%d.%d.%d to %d.%d.%d.%d). Value is reset to default value=%d.%d.%d.%d";
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

// Below constant array is stored in last sector of External memory to display firmware version Number in HEX File 	

//Populated the MVT table for Scale setup parameters
const ELEMENT_REC Elements_Record_Scale_Setup[] =
{
	//Element_Section Element_ID Element_String_Enum	Element_Data_Type	Element_Length	Element_Min_Value	Element_Max_Value	Element_Default_Value
  {Scale_Setup_Section, AngleSensor_Install_status_Id, Screen27_M_str1, unsigned_int_type, sizeof(Scale_setup_var.AngleSensor_Install_status), &AngleSensor_Install_status_Min, &AngleSensor_Install_status_Max, &AngleSensor_Install_status_Def, NO_OF_ANGLESENSOR_INSTALLED, (unsigned int *)&AngleSensor_Install_status_enum_list}, //16	
	{Scale_Setup_Section, Distance_unit_Id, Screen2_str7, unsigned_int_type, sizeof(Scale_setup_var.Distance_unit), &Distance_unit_Min, &Distance_unit_Max, &Distance_unit_Def, NO_OF_DISTANCE_UINTS, (unsigned int *)&Distance_unit_enum_list}, //8
	{Scale_Setup_Section, User_Belt_Speed_Id, Screen29_str7, float_type2, sizeof(Scale_setup_var.User_Belt_Speed), &User_Belt_Speed_Min, &User_Belt_Speed_Max, &User_Belt_Speed_Def, 0, NULL}, //5
	{Scale_Setup_Section, Weight_unit_Id, Screen2_str9, unsigned_int_type, sizeof(Scale_setup_var.Weight_unit), &Weight_unit_Min, &Weight_unit_Max, &Weight_unit_Def, NO_OF_TOTAL_WT_UNITS, (unsigned int *)&Weight_unit_enum_list}, //9
  {Scale_Setup_Section, Idler_unit_Id, Idler_units_str, unsigned_int_type, sizeof(Scale_setup_var.Idler_unit), &Idler_unit_Min, &Idler_unit_Max, &Idler_unit_Def, NO_OF_IDLER_DISTANCE_UNITS, (unsigned int *)&Idler_unit_enum_list}, //12
  {Scale_Setup_Section, Wheel_diameter_unit_Id, Wheel_diameter_unit_str, unsigned_int_type, sizeof(Scale_setup_var.Wheel_diameter_unit), &Wheel_diameter_unit_Min, &Wheel_diameter_unit_Max, &Wheel_diameter_unit_Def, NO_OF_WHEEL_DIAMETER_UNITS, (unsigned int *)&Wheel_diameter_unit_enum_list}, //13
  {Scale_Setup_Section, Belt_length_unit_Id, Belt_length_unit_str, unsigned_int_type, sizeof(Scale_setup_var.Belt_length_unit), &Belt_length_unit_Min, &Belt_length_unit_Max, &Belt_length_unit_Def, NO_OF_BELT_LENGTH_UNITS, (unsigned int *)&Belt_length_unit_enum_list}, //14
  {Scale_Setup_Section, Speed_unit_Id, Speed_unit_str, unsigned_int_type, sizeof(Scale_setup_var.Speed_unit), &Speed_unit_Min, &Speed_unit_Max, &Speed_unit_Def, NO_OF_SPEED_UNITS, (unsigned int *)&Speed_unit_enum_list}, //15
	{Scale_Setup_Section, Conveyor_angle_Id, Screen2_str13, float_type2, sizeof(Scale_setup_var.Conveyor_angle), &Conveyor_angle_Min, &Conveyor_angle_Max, &Conveyor_angle_Def,0, NULL}, //1
  {Scale_Setup_Section, Idler_distance_Id, Screen2_str15, float_type2, sizeof(Scale_setup_var.Idler_distance),&Idler_distance_Min, &Idler_distance_Max, &Idler_distance_Def, 0, NULL}, //2
  {Scale_Setup_Section, Wheel_diameter_Id, Screen29_str1, float_type2, sizeof(Scale_setup_var.Wheel_diameter), &Wheel_diameter_Min, &Wheel_diameter_Max, &Wheel_diameter_Def, 0, NULL},//3
  {Scale_Setup_Section, Pulses_Per_Revolution_Id, Screen29_str4, float_type2, sizeof(Scale_setup_var.Pulses_Per_Revolution), &Pulses_Per_Revolution_Min, &Pulses_Per_Revolution_Max, &Pulses_Per_Revolution_Def,0, NULL},//4
  {Scale_Setup_Section, Run_mode_Id, Screen2_str1, unsigned_int_type, sizeof(Scale_setup_var.Run_mode), &Run_mode_Min, &Run_mode_Max, &Run_mode_Def, NO_OF_RUN_MODE, (unsigned int *)&Run_mode_enum_list[0]}, //6
  {Scale_Setup_Section, Load_cell_size_Id, Screen2_str5, unsigned_int_type, sizeof(Scale_setup_var.Load_cell_size), &Load_cell_size_Min, &Load_cell_size_Max, &Load_cell_size_Def, NO_OF_LC_SIZES, (unsigned int *)&Load_cell_size_enum_list}, //7
  {Scale_Setup_Section, Rate_time_unit_Id, Screen2_str11, unsigned_int_type, sizeof(Scale_setup_var.Rate_time_unit), &Rate_time_unit_Min, &Rate_time_unit_Max, &Rate_time_unit_Def, NO_OF_RATE_TIME_UNITS,(unsigned int *)&Rate_time_unit_enum_list}, //10
  {Scale_Setup_Section, Custom_LC_unit_Id, Screen2312_str1, unsigned_int_type, sizeof(Scale_setup_var.Custom_LC_unit), &Custom_LC_unit_Min, &Custom_LC_unit_Max, &Custom_LC_unit_Def, NO_OF_CUSTOM_LC_UNITS, (unsigned int *)&Custom_LC_unit_enum_list}, //11
  {Scale_Setup_Section, Number_of_idlers_Id, Screen2_str3, unsigned_int_type, sizeof(Scale_setup_var.Number_of_idlers), &Number_of_idlers_Min, &Number_of_idlers_Max, &Number_of_idlers_Def, 0, NULL/*NO_OF_IDLERS, (unsigned int *)&Number_of_idlers_enum_list*/}, //17
  {Scale_Setup_Section, Custom_LC_capacity_Id, Screen2311_str1, unsigned_int_type, sizeof(Scale_setup_var.Custom_LC_capacity), &Custom_LC_capacity_Min, &Custom_LC_capacity_Max, &Custom_LC_capacity_Def, 0, NULL}, //18
  {Scale_Setup_Section, Custom_LC_output_Id, Screen2313_str1, unsigned_int_type, sizeof(Scale_setup_var.Custom_LC_output), &Custom_LC_output_Min, &Custom_LC_output_Max, &Custom_LC_output_Def, 0, NULL}, //19
  {Scale_Setup_Section, Decimal_digits_Id, Screen2_str19, unsigned_int_type, sizeof(Scale_setup_var.Decimal_digits), &Decimal_digits_Min, &Decimal_digits_Max, &Decimal_digits_Def, NO_OF_DECIMAL_DIGITS, (unsigned int *)&Decimal_digits_enum_list}, //20
  {Scale_Setup_Section, IO_board_install_Id, Screen2_str25, unsigned_int_type, sizeof(Scale_setup_var.IO_board_install), &IO_board_install_Min, &IO_board_install_Max, &IO_board_install_Def, NO_OF_IO_BOARD_INSTALLED, (unsigned int *)&IO_board_install_enum_list}, //21
  {Scale_Setup_Section, Idler_distanceA_Id, Screen281_str1, float_type2, sizeof(Scale_setup_var.Idler_distanceA), &Idler_distanceA_Min, &Idler_distanceA_Max, &Idler_distanceA_Def, 0, NULL}, //22
  {Scale_Setup_Section, Idler_distanceB_Id, Screen282_str1, float_type2, sizeof(Scale_setup_var.Idler_distanceB), &Idler_distanceB_Min, &Idler_distanceB_Max, &Idler_distanceB_Def, 0, NULL}, //23
  {Scale_Setup_Section, Idler_distanceC_Id, Screen283_str1, float_type2, sizeof(Scale_setup_var.Idler_distanceC), &Idler_distanceC_Min, &Idler_distanceC_Max, &Idler_distanceC_Def, 0, NULL}, //24
  {Scale_Setup_Section, Idler_distanceD_Id, Screen284_str1, float_type2, sizeof(Scale_setup_var.Idler_distanceD), &Idler_distanceD_Min, &Idler_distanceD_Max, &Idler_distanceD_Def, 0, NULL}, //25
  {Scale_Setup_Section, Idler_distanceE_Id, Screen285_str1, float_type2, sizeof(Scale_setup_var.Idler_distanceE), &Idler_distanceE_Min, &Idler_distanceE_Max, &Idler_distanceE_Def, 0, NULL}, //26
  {Scale_Setup_Section, User_Belt_Speeed_WD_Id, Cal_User_Belt_Speed_WD, float_type2, sizeof(Scale_setup_var.User_Belt_Speeed_WD), &User_Belt_Speeed_WD_Min, &User_Belt_Speeed_WD_Max, &User_Belt_Speeed_WD_Def, 0, NULL}, //27
};	

const ELEMENT_REC Elements_Record_Calibration[] =
{
	//Element_ID Element_Section Element_String_Enum	Element_Data_Type	Element_Length	Element_Min_Value	Element_Max_Value	Element_Default_Value
    {Calibration_Section, Belt_scale_unit_Id, Screen322_str1, unsigned_int_type, sizeof(Calibration_var.Belt_scale_unit), &Belt_scale_unit_Min, &Belt_scale_unit_Max, &Belt_scale_unit_Def, NO_OF_TOTAL_WT_UNITS, (unsigned int *)&Weight_unit_enum_list},
    {Calibration_Section, Cert_scale_unit_Id, Screen324_str1, unsigned_int_type, sizeof(Calibration_var.Cert_scale_unit), &Weight_unit_Min, &Weight_unit_Max, &Cert_scale_unit_Def, NO_OF_TOTAL_WT_UNITS, (unsigned int *)&Weight_unit_enum_list},
    {Calibration_Section, Test_zero_weight_unit_Id, MVT_str11, unsigned_int_type, sizeof(Calibration_var.Test_zero_weight_unit), &Weight_unit_Min, &Weight_unit_Max, &Test_zero_weight_unit_Def, NO_OF_TOTAL_WT_UNITS, (unsigned int *)&Weight_unit_enum_list},
		{Calibration_Section, Test_weight_Id, Screen3_str1, float_type2, sizeof(Calibration_var.Test_weight), &Test_weight_Min, &Test_weight_Max, &Test_weight_Def,0, NULL},
    {Calibration_Section, Belt_scale_weight_Id, Screen321_str1,float_type2, sizeof(Calibration_var.Belt_scale_weight), &Belt_scale_weight_Min, &Belt_scale_weight_Max, &Belt_scale_weight_Def,0, NULL},
    {Calibration_Section, Cert_scale_weight_Id, Screen323_str1,float_type2, sizeof(Calibration_var.Cert_scale_weight), &Cert_scale_weight_Min, &Cert_scale_weight_Max, &Cert_scale_weight_Def,0, NULL},
    {Calibration_Section, Old_belt_length_Id, MVT_str1, float_type2, sizeof(Calibration_var.Old_belt_length), &Old_belt_length_Min, &Old_belt_length_Max, &Old_belt_length_Def,0, NULL},
    {Calibration_Section, New_belt_length_Id, MVT_str2, float_type2, sizeof(Calibration_var.New_belt_length), &New_belt_length_Min, &New_belt_length_Max, &New_belt_length_Def,0, NULL},
    {Calibration_Section, Belt_length_diff_Id, MVT_str3,float_type2, sizeof(Calibration_var.Belt_length_diff), &Belt_length_diff_Min, &Belt_length_diff_Max, &Belt_length_diff_Def,0, NULL},
    {Calibration_Section, Old_zero_value_Id, MVT_str4, float_type2, sizeof(Calibration_var.Old_zero_value), &Old_zero_value_Min, &Old_zero_value_Max, &Old_zero_value_Def,0, NULL},
    {Calibration_Section, New_zero_value_Id, MVT_str5, float_type2, sizeof(Calibration_var.New_zero_value), &New_zero_value_Min, &New_zero_value_Max, &New_zero_value_Def,0, NULL},
    {Calibration_Section, Zero_diff_Id, MVT_str6,float_type2, sizeof(Calibration_var.Zero_diff), &Zero_diff_Min, &Zero_diff_Max, &Zero_diff_Def,0, NULL},
    {Calibration_Section, old_span_value_Id, MVT_str7, float_type2, sizeof(Calibration_var.old_span_value), &old_span_value_Min, &old_span_value_Max, &old_span_value_Def,0, NULL},
    {Calibration_Section, new_span_value_Id, MVT_str8, float_type2, sizeof(Calibration_var.new_span_value), &new_span_value_Min, &new_span_value_Max, &new_span_value_Def,0, NULL},
    {Calibration_Section, span_diff_Id, MVT_str9, float_type2, sizeof(Calibration_var.span_diff), &span_diff_Min, &span_diff_Max, &span_diff_Def,0, NULL},
    {Calibration_Section, real_time_rate_Id, MVT_str10, float_type2, sizeof(Calibration_var.real_time_rate), &real_time_rate_Min, &real_time_rate_Max, &real_time_rate_Def,0, NULL},
};

//__align(4) 
const ELEMENT_REC Elements_Record_Admin[] =
{
//Element_ID Element_Section Element_String_Enum	Element_Data_Type	Element_Length	Element_Min_Value	Element_Max_Value	Element_Default_Value
		{Admin_Section, Config_Date_Year_Id, MVT_str12, unsigned_short_type, sizeof(Admin_var.Config_Date.Year), &Config_Date_Year_Min, &Config_Date_Year_Max, &Config_Date_Year_Def, 0, NULL},
		{Admin_Section, Config_Date_Mon_Id, MVT_str13, unsigned_char_type, sizeof(Admin_var.Config_Date.Mon), &Config_Date_Mon_Min, &Config_Date_Mon_Max, &Config_Date_Mon_Def, 0, NULL},
		{Admin_Section, Config_Date_Day_Id, MVT_str14, unsigned_char_type, sizeof(Admin_var.Config_Date.Day), &Config_Date_Day_Min, &Config_Date_Day_Max, &Config_Date_Day_Def, 0, NULL},
		{Admin_Section, Config_Time_Hr_Id, MVT_str15, unsigned_char_type, sizeof(Admin_var.Config_Time.Hr), &Config_Time_Hr_Min, &Config_Time_Hr_Max, &Config_Time_Hr_Def, 0, NULL},
		{Admin_Section, Config_Time_AM_PM_Id, MVT_str16, unsigned_char_type, sizeof(Admin_var.Config_Time.AM_PM), &Config_Time_AM_PM_Min, &Config_Time_AM_PM_Max, &Config_Time_AM_PM_Def, 0, NULL},
		{Admin_Section, Config_Time_Min_Id, MVT_str17, unsigned_char_type, sizeof(Admin_var.Config_Time.Min), &Config_Time_Min_Min, &Config_Time_Min_Max, &Config_Time_Min_Def, 0, NULL},
		{Admin_Section, Config_Time_Sec_Id, MVT_str18, unsigned_char_type, sizeof(Admin_var.Config_Time.Sec), &Config_Time_Sec_Min, &Config_Time_Sec_Max, &Config_Time_Sec_Def, 0, NULL},
		{Admin_Section, IP_Addr_Addr1_Id, Screen66_str3, ip_struct, sizeof(Admin_var.IP_Addr.Addr1), &IP_Addr_Min, &IP_Addr_Max, &IP_Addr_Def, 0, NULL},	//Aggre_
		//{Admin_Section, IP_Addr_Addr2_Id, Screen66_str3, unsigned_char_type, sizeof(Admin_var.IP_Addr.Addr2), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &IP_Addr_Byte2_Def, 0, NULL},
		//{Admin_Section, IP_Addr_Addr3_Id, Screen66_str3, unsigned_char_type, sizeof(Admin_var.IP_Addr.Addr3), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &IP_Addr_Byte3_Def, 0, NULL},
		//{Admin_Section, IP_Addr_Addr4_Id, Screen66_str3, unsigned_char_type, sizeof(Admin_var.IP_Addr.Addr4), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &IP_Addr_Byte4_Def, 0, NULL},
		{Admin_Section, Subnet_Mask_Addr1_Id, Screen66_str5, ip_struct, sizeof(Admin_var.Subnet_Mask.Addr1), &Subnet_Mask_Min, &Subnet_Mask_Max, &Subnet_Mask_Def, 0, NULL},
		//{Admin_Section, Subnet_Mask_Addr2_Id, Screen66_str5, unsigned_char_type, sizeof(Admin_var.Subnet_Mask.Addr2), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &Subnet_Mask_Byte2_Def, 0, NULL},
		//{Admin_Section, Subnet_Mask_Addr3_Id, Screen66_str5, unsigned_char_type, sizeof(Admin_var.Subnet_Mask.Addr3), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &Subnet_Mask_Byte3_Def, 0, NULL},
		//{Admin_Section, Subnet_Mask_Addr4_Id, Screen66_str5, unsigned_char_type, sizeof(Admin_var.Subnet_Mask.Addr4), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &Subnet_Mask_Byte4_Def, 0, NULL},
		{Admin_Section, Gateway_Addr1_Id, Screen66_str7, ip_struct, sizeof(Admin_var.Gateway.Addr1), &Gateway_Min, &Gateway_Max, &Gateway_Def, 0, NULL},
		//{Admin_Section, Gateway_Addr2_Id, Screen66_str7, unsigned_char_type, sizeof(Admin_var.Gateway.Addr2), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &Gateway_Byte2_Def, 0, NULL},
		//{Admin_Section, Gateway_Addr3_Id, Screen66_str7, unsigned_char_type, sizeof(Admin_var.Gateway.Addr3), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &Gateway_Byte3_Def, 0, NULL},
		//{Admin_Section, Gateway_Addr4_Id, Screen66_str7, unsigned_char_type, sizeof(Admin_var.Gateway.Addr4), &IP_Addr_Byte_Min, &IP_Addr_Byte_Max, &Gateway_Byte4_Def, 0, NULL},
		{Admin_Section, Auto_zero_tolerance_Id, Screen61_str11, float_type2, sizeof(Admin_var.Auto_zero_tolerance), &Auto_zero_tolerance_Min, &Auto_zero_tolerance_Max, &Auto_zero_tolerance_Def, 0, NULL},
		{Admin_Section, Negative_rate_limit_Id, Screen618_str2, float_type2, sizeof(Admin_var.Negative_rate_limit), &Negative_rate_limit_Min, &Negative_rate_limit_Max, &Negative_rate_limit_Def, 0, NULL},
		{Admin_Section, Negative_rate_time_Id, Screen618_str4, float_type2, sizeof(Admin_var.Negative_rate_time), &Negative_rate_time_Min, &Negative_rate_time_Max, &Negative_rate_time_Def, 0, NULL},
		{Admin_Section, Zero_rate_limit_Id, Screen61_str13, float_type2, sizeof(Admin_var.Zero_rate_limit), &Zero_rate_limit_Min, &Zero_rate_limit_Max, &Zero_rate_limit_Def, 0, NULL},
		{Admin_Section, Int_firmware_version_Id, Screen53_str4, signed_char_type, sizeof(Admin_var.Int_firmware_version), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Int_update_version_Id, MVT_str19, signed_char_type, sizeof(Admin_var.Int_update_version), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Scale_Name_Id, Screen61_str1, signed_char_type, sizeof(Admin_var.Scale_Name), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Plant_Name_Id, Screen61_str3, signed_char_type, sizeof(Admin_var.Plant_Name), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Product_Name_Id, Screen61_str5, signed_char_type, sizeof(Admin_var.Product_Name), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Password_Id, MVT_str20, signed_char_type, sizeof(Admin_var.Password), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, New_Password_Id, Screen637_str3, signed_char_type, sizeof(Admin_var.New_Password), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Verify_New_Password_Id, Screen637_str5, signed_char_type, sizeof(Admin_var.Verify_New_Password), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Int_update_file_name_Id, MVT_str21, signed_char_type, sizeof(Admin_var.Int_update_file_name), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Reload_file_name_Id, MVT_str22, signed_char_type, sizeof(Admin_var.Reload_file_name), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Backup_file_name_Id, MVT_str23, signed_char_type, sizeof(Admin_var.Backup_file_name), NULL, NULL, NULL, 0, NULL},
		{Admin_Section, Admin_Locked_Id, Screen63_str6, unsigned_int_type, sizeof(Admin_var.Admin_Locked), &Lock_Status_Min, &Lock_Status_Max, &Lock_Status_Def, NO_OF_LOCK_STATUS, (unsigned int *)&Lock_Status_enum_list[0]},
		{Admin_Section, AM_PM_Id, MVT_str16, unsigned_int_type, sizeof(Admin_var.AM_PM), &AM_PM_Min, &AM_PM_Max, &AM_PM_Def, NO_OF_AM_PM, (unsigned int *)&AM_PM_enum_list[0]},
		{Admin_Section, Calibration_Locked_Id, Screen63_str3, unsigned_int_type, sizeof(Admin_var.Calibration_Locked), &Lock_Status_Min, &Lock_Status_Max, &Lock_Status_Def, NO_OF_LOCK_STATUS, (unsigned int *)&Lock_Status_enum_list[0]},
		{Admin_Section, Clear_Weight_Locked_Id, Screen63_str7, unsigned_int_type, sizeof(Admin_var.Clear_Weight_Locked), &Lock_Status_Min, &Lock_Status_Max, &Lock_Status_Def, NO_OF_LOCK_STATUS, (unsigned int *)&Lock_Status_enum_list[0]},
		{Admin_Section, Current_Time_Format_Id, Screen6153_str1, unsigned_int_type, sizeof(Admin_var.Current_Time_Format), &Current_Time_Format_Min, &Current_Time_Format_Max, &Current_Time_Format_Def, NO_OF_TIME_FORMAT, (unsigned int *)&Time_Format_enum_list[0]},
		{Admin_Section, Current_Date_Format_Id, Screen6142_str1, unsigned_int_type, sizeof(Admin_var.Current_Date_Format), &Current_Date_Format_Min, &Current_Date_Format_Max, &Current_Date_Format_Def, NO_OF_DATE_FORMAT, (unsigned int *)&Date_Format_enum_list[0]},
		{Admin_Section, Day_of_week_Id, Screen6143_str1, unsigned_int_type, sizeof(Admin_var.Day_of_week), &Day_of_week_Min, &Day_of_week_Max, &Day_of_week_Def, NO_OF_DAY_OF_WEEK, (unsigned int *)&Day_of_Week_enum_list[0]},
		{Admin_Section, DHCP_Status_Id, Screen66_str1, unsigned_int_type, sizeof(Admin_var.DHCP_Status), &DHCP_Status_Min, &DHCP_Status_Max, &DHCP_Status_Def, NO_OF_DHCP_STATUS, (unsigned int *)&DHCP_Status_enum_list[0]},
		{Admin_Section, Setup_Devices_Locked_Id, Screen63_str5, unsigned_int_type, sizeof(Admin_var.Setup_Devices_Locked), &Lock_Status_Min, &Lock_Status_Max, &Lock_Status_Def, NO_OF_LOCK_STATUS, (unsigned int *)&Lock_Status_enum_list[0]},
		{Admin_Section, Setup_Wizard_Locked_Id, Screen63_str1, unsigned_int_type, sizeof(Admin_var.Setup_Wizard_Locked), &Lock_Status_Min, &Lock_Status_Max, &Lock_Status_Def, NO_OF_LOCK_STATUS, (unsigned int *)&Lock_Status_enum_list[0]},
		{Admin_Section, Zero_Calibration_Locked_Id, Screen63_str4, unsigned_int_type, sizeof(Admin_var.Zero_Calibration_Locked), &Lock_Status_Min, &Lock_Status_Max, &Lock_Status_Def, NO_OF_LOCK_STATUS, (unsigned int *)&Lock_Status_enum_list[0]},
		{Admin_Section, Zero_rate_status_Id, Screen617_str1, unsigned_int_type, sizeof(Admin_var.Zero_rate_status), &Zero_rate_status_Min, &Zero_rate_status_Max, &Zero_rate_status_Def, NO_OF_ZERO_RATE_STATUS, (unsigned int *)&Zero_Rate_Status_enum_list[0]},
		{Admin_Section, MBS_TCP_slid_Id, Screen66500_str1, unsigned_char_type, sizeof(Admin_var.MBS_TCP_slid), &MBS_TCP_slid_Min, &MBS_TCP_slid_Max, &MBS_TCP_slid_Def, 0, NULL},
		{Admin_Section, Test_speed_status_Id, Screen61A1_str0, unsigned_int_type, sizeof(Admin_var.Test_speed_status), &Test_Spd_Load_status_Min, &Test_Spd_Load_status_Max, &Test_Spd_Load_status_Def, NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Admin_Section, Test_load_status_Id, Screen61A2_str0, unsigned_int_type, sizeof(Admin_var.Test_load_status), &Test_Spd_Load_status_Min, &Test_Spd_Load_status_Max, &Test_Spd_Load_status_Def, NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Admin_Section, Test_load_Id, Screen61A21_str0, float_type2, sizeof(Admin_var.Test_load), &Test_load_Min, &Test_load_Max, &Test_load_Def, 0, NULL},
		{Admin_Section, Test_speed_Id, Screen61A11_str0, float_type2, sizeof(Admin_var.Test_speed), &Test_speed_Min, &Test_speed_Max, &Test_speed_Def, 0, NULL},
//		{Admin_Section,Language_sel_Id,Screen61B_str0,unsigned_int_type,sizeof(Admin_var.Language_select),&Language_sel_min,&Language_sel_max,&Language_sel_def,NO_OF_LANGUAGES,(unsigned int *)&Language_enum_list},
};


const ELEMENT_REC Elements_Record_Setup_Device[] =
{
//Element_ID Element_Section Element_String_Enum	Element_Data_Type	Element_Length	Element_Min_Value	Element_Max_Value	Element_Default_Value
    {Device_Setup_Section, Analog_Output_1_Setpoint_Id, Screen44412_str1, float_type2, sizeof(Setup_device_var.Analog_Output_1_Setpoint), &Analog_Output_Setpoint_Min, &Analog_Output_Setpoint_Max, &Analog_Output_Setpoint_Def, 0, NULL},
    {Device_Setup_Section, Analog_Output_1_Maxrate_Id, Screen44413_str1, float_type2, sizeof(Setup_device_var.Analog_Output_1_Maxrate), &Analog_Output_Maxrate_Min, &Analog_Output_Maxrate_Max, &Analog_Output_Maxrate_Def, 0, NULL},
    {Device_Setup_Section, Analog_Output_1_ZeroCal_Id, Screen44414_str1, float_type2, sizeof(Setup_device_var.Analog_Output_1_ZeroCal), &Analog_Output_ZeroCal_Min, &Analog_Output_ZeroCal_Max, &Analog_Output_ZeroCal_Def, 0, NULL},
    {Device_Setup_Section, Analog_Output_2_Setpoint_Id, Screen44422_str1, float_type2, sizeof(Setup_device_var.Analog_Output_2_Setpoint), &Analog_Output_Setpoint_Min, &Analog_Output_Setpoint_Max, &Analog_Output_Setpoint_Def, 0, NULL},
    {Device_Setup_Section, Analog_Output_2_Maxrate_Id, Screen44423_str1, float_type2, sizeof(Setup_device_var.Analog_Output_2_Maxrate), &Analog_Output_Maxrate_Min, &Analog_Output_Maxrate_Max, &Analog_Output_Maxrate_Def, 0, NULL},
    {Device_Setup_Section, Analog_Output_2_ZeroCal_Id, Screen44424_str1, float_type2, sizeof(Setup_device_var.Analog_Output_2_ZeroCal), &Analog_Output_ZeroCal_Min, &Analog_Output_ZeroCal_Max, &Analog_Output_ZeroCal_Def, 0, NULL},
		{Device_Setup_Section, Analog_Input_Maxrate_Id, MVT_str24,float_type2, sizeof(Setup_device_var.Analog_Input_Maxrate), &Analog_Input_Maxrate_Min, &Analog_Input_Maxrate_Max, &Analog_Input_Maxrate_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_1_Id, Screen4521_str3, float_type2, sizeof(Setup_device_var.Cutoff_1), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_2_Id, Screen4522_str3, float_type2, sizeof(Setup_device_var.Cutoff_2), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_3_Id, Screen4523_str3, float_type2, sizeof(Setup_device_var.Cutoff_3), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_4_Id, Screen4524_str3, float_type2, sizeof(Setup_device_var.Cutoff_4), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_5_Id, Screen4525_str3, float_type2, sizeof(Setup_device_var.Cutoff_5), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_6_Id, Screen4526_str3, float_type2, sizeof(Setup_device_var.Cutoff_6), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_7_Id, Screen4527_str3, float_type2, sizeof(Setup_device_var.Cutoff_7), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Cutoff_8_Id, Screen4528_str3, float_type2, sizeof(Setup_device_var.Cutoff_8), &Cutoff_Min, &Cutoff_Max, &Cutoff_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_1_Id, Screen4521_str1, float_type2, sizeof(Setup_device_var.Load_weight_1), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_2_Id, Screen4522_str1, float_type2, sizeof(Setup_device_var.Load_weight_2), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_3_Id, Screen4523_str1, float_type2, sizeof(Setup_device_var.Load_weight_3), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_4_Id, Screen4524_str1, float_type2, sizeof(Setup_device_var.Load_weight_4), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_5_Id, Screen4525_str1, float_type2, sizeof(Setup_device_var.Load_weight_5), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_6_Id, Screen4526_str1, float_type2, sizeof(Setup_device_var.Load_weight_6), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_7_Id, Screen4527_str1, float_type2, sizeof(Setup_device_var.Load_weight_7), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, Load_weight_8_Id, Screen4528_str1, float_type2, sizeof(Setup_device_var.Load_weight_8), &Load_weight_Min, &Load_weight_Max, &Load_weight_Def, 0, NULL},
    {Device_Setup_Section, MinMax_Rate_Setpoint_1_Id, Screen44215_str0, float_type2, sizeof(Setup_device_var.MinMax_Rate_Setpoint_1), &MinMax_Rate_Setpoint_Min, &MinMax_Rate_Setpoint_Max, &MinMax_Rate_Setpoint_Def, 0, NULL},
    {Device_Setup_Section, MinMax_Rate_Setpoint_2_Id, Screen44225_str0, float_type2, sizeof(Setup_device_var.MinMax_Rate_Setpoint_2), &MinMax_Rate_Setpoint_Min, &MinMax_Rate_Setpoint_Max, &MinMax_Rate_Setpoint_Def, 0, NULL},
    {Device_Setup_Section, MinMax_Rate_Setpoint_3_Id, Screen44235_str0, float_type2, sizeof(Setup_device_var.MinMax_Rate_Setpoint_3), &MinMax_Rate_Setpoint_Min, &MinMax_Rate_Setpoint_Max, &MinMax_Rate_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Rate_Setpoint_4_Id, Screen44313_str0, float_type2, sizeof(Setup_device_var.MinMax_Rate_Setpoint_4), &MinMax_Rate_Setpoint_Min, &MinMax_Rate_Setpoint_Max, &MinMax_Rate_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Rate_Setpoint_5_Id, Screen44323_str0, float_type2, sizeof(Setup_device_var.MinMax_Rate_Setpoint_5), &MinMax_Rate_Setpoint_Min, &MinMax_Rate_Setpoint_Max, &MinMax_Rate_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Rate_Setpoint_6_Id, Screen44333_str0, float_type2, sizeof(Setup_device_var.MinMax_Rate_Setpoint_6), &MinMax_Rate_Setpoint_Min, &MinMax_Rate_Setpoint_Max, &MinMax_Rate_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Speed_Setpoint_1_Id, Screen44214_str0, float_type2, sizeof(Setup_device_var.MinMax_Speed_Setpoint_1), &MinMax_Speed_Setpoint_Min, &MinMax_Speed_Setpoint_Max, &MinMax_Speed_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Speed_Setpoint_2_Id, Screen44224_str0, float_type2, sizeof(Setup_device_var.MinMax_Speed_Setpoint_2), &MinMax_Speed_Setpoint_Min, &MinMax_Speed_Setpoint_Max, &MinMax_Speed_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Speed_Setpoint_3_Id, Screen44234_str0, float_type2, sizeof(Setup_device_var.MinMax_Speed_Setpoint_3), &MinMax_Speed_Setpoint_Min, &MinMax_Speed_Setpoint_Max, &MinMax_Speed_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Speed_Setpoint_4_Id, Screen44312_str0, float_type2, sizeof(Setup_device_var.MinMax_Speed_Setpoint_4), &MinMax_Speed_Setpoint_Min, &MinMax_Speed_Setpoint_Max, &MinMax_Speed_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Speed_Setpoint_5_Id, Screen44322_str0, float_type2, sizeof(Setup_device_var.MinMax_Speed_Setpoint_5), &MinMax_Speed_Setpoint_Min, &MinMax_Speed_Setpoint_Max, &MinMax_Speed_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, MinMax_Speed_Setpoint_6_Id, Screen44332_str0, float_type2, sizeof(Setup_device_var.MinMax_Speed_Setpoint_6), &MinMax_Speed_Setpoint_Min, &MinMax_Speed_Setpoint_Max, &MinMax_Speed_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, Min_Belt_Speed_Id, Screen455_str3, float_type2, sizeof(Setup_device_var.Min_Belt_Speed), &Min_Belt_Speed_Min, &Min_Belt_Speed_Max, &Min_Belt_Speed_Def, 0, NULL},
		{Device_Setup_Section, Empty_Belt_Speed_Id, Screen455_str7, float_type2, sizeof(Setup_device_var.Empty_Belt_Speed), &Empty_Belt_Speed_Min, &Empty_Belt_Speed_Max, &Empty_Belt_Speed_Def, 0, NULL},
		{Device_Setup_Section, Feeder_Max_Rate_Id, Screen453_str11, float_type2, sizeof(Setup_device_var.Feeder_Max_Rate), &Feeder_Max_Rate_Min, &Feeder_Max_Rate_Max, &Feeder_Max_Rate_Def, 0, NULL},
		{Device_Setup_Section, Preload_Distance_Id, Screen455_str5, float_type2, sizeof(Setup_device_var.Preload_Distance), &Preload_Distance_Min, &Preload_Distance_Max, &Preload_Distance_Def, 0, NULL},
		{Device_Setup_Section, Target_rate_Id, Screen73_str10, float_type2, sizeof(Setup_device_var.Target_rate), &Target_rate_Min, &Target_rate_Max, &Target_rate_Def, 0, NULL},
		{Device_Setup_Section, Target_Load_Id, Screen75_str2, float_type2, sizeof(Setup_device_var.Target_Load), &Target_Load_Min, &Target_Load_Max, &Target_Load_Def, 0, NULL},
		{Device_Setup_Section, Analog_Output_1_Function_Id, Screen4441_str1, unsigned_int_type, sizeof(Setup_device_var.Analog_Output_1_Function), &Analog_Output_Function_Min, &Analog_Output_Function_Max, &Analog_Output_Function_Def, NO_OF_AO_FUNCTION, (unsigned int *)&AO1_Function_enum_list[0]},
		{Device_Setup_Section, Analog_Output_2_Function_Id, Screen4442_str1, unsigned_int_type, sizeof(Setup_device_var.Analog_Output_2_Function), &Analog_Output_Function_Min, &Analog_Output_Function_Max, &Analog_Output_Function_Def, NO_OF_AO_FUNCTION, (unsigned int *)&AO2_Function_enum_list[0]},
		{Device_Setup_Section, Analog_Output1_Id, Screen5235_str1, float_type2, sizeof(Setup_device_var.Analog_Output1), &Analog_Output_Min, &Analog_Output_Max, &Analog_Output_Def, 0, NULL},
		{Device_Setup_Section, Analog_Output2_Id, Screen5235_str2, float_type2, sizeof(Setup_device_var.Analog_Output2), &Analog_Output_Min, &Analog_Output_Max, &Analog_Output_Def, 0, NULL},
		{Device_Setup_Section, Digital_Input_1_Function_Id, Screen4411_str0, unsigned_int_type, sizeof(Setup_device_var.Digital_Input_1_Function), &Digital_Input_Function_Min, &Digital_Input_Function_Max, &Digital_Input_Function_Def, NO_OF_DIN_FUNCTION, (unsigned int *)&Din_Function_enum_list[0]},
		{Device_Setup_Section, Digital_Input_2_Function_Id, Screen4412_str0, unsigned_int_type, sizeof(Setup_device_var.Digital_Input_2_Function), &Digital_Input_Function_Min, &Digital_Input_Function_Max, &Digital_Input_Function_Def, NO_OF_DIN_FUNCTION, (unsigned int *)&Din_Function_enum_list[0]},
		{Device_Setup_Section, Digital_Input_3_Function_Id, Screen4413_str0, unsigned_int_type, sizeof(Setup_device_var.Digital_Input_3_Function), &Digital_Input_Function_Min, &Digital_Input_Function_Max, &Digital_Input_Function_Def, NO_OF_DIN_FUNCTION, (unsigned int *)&Din_Function_enum_list[0]},
		{Device_Setup_Section, Digital_Input_4_Function_Id, Screen4414_str0, unsigned_int_type, sizeof(Setup_device_var.Digital_Input_4_Function), &Digital_Input_Function_Min, &Digital_Input_Function_Max, &Digital_Input_Function_Def, NO_OF_DIN_FUNCTION, (unsigned int *)&Din_Function_enum_list[0]},
		{Device_Setup_Section, Digital_Output_1_Function_Id, Screen442_str1, unsigned_int_type, sizeof(Setup_device_var.Digital_Output_1_Function), &Digital_Output_Function_Min, &Digital_Output_Function_Max, &Digital_Output_Function_Def, NO_OF_DO_FUNCTION, (unsigned int *)&DO_Function_enum_list[0]},
		{Device_Setup_Section, Digital_Output_2_Function_Id, Screen442_str3, unsigned_int_type, sizeof(Setup_device_var.Digital_Output_2_Function), &Digital_Output_Function_Min, &Digital_Output_Function_Max, &Digital_Output_Function_Def, NO_OF_DO_FUNCTION, (unsigned int *)&DO_Function_enum_list[0]},
		{Device_Setup_Section, Digital_Output_3_Function_Id, Screen442_str4, unsigned_int_type, sizeof(Setup_device_var.Digital_Output_3_Function), &Digital_Output_Function_Min, &Digital_Output_Function_Max, &Digital_Output_Function_Def, NO_OF_DO_FUNCTION, (unsigned int *)&DO_Function_enum_list[0]},
		{Device_Setup_Section, error_alarm_6_Id, Screen44331_str0, unsigned_int_type, sizeof(Setup_device_var.error_alarm_6), &error_alarm_Min, &error_alarm_Max, &error_alarm_Def, NO_OF_ERR_ALM, (unsigned int *)&Err_Alm_enum_list[0]},
		{Device_Setup_Section, error_alarm_5_Id, Screen44321_str0, unsigned_int_type, sizeof(Setup_device_var.error_alarm_5), &error_alarm_Min, &error_alarm_Max, &error_alarm_Def, NO_OF_ERR_ALM, (unsigned int *)&Err_Alm_enum_list[0]},
		{Device_Setup_Section, error_alarm_4_Id, Screen44311_str0, unsigned_int_type, sizeof(Setup_device_var.error_alarm_4), &error_alarm_Min, &error_alarm_Max, &error_alarm_Def, NO_OF_ERR_ALM, (unsigned int *)&Err_Alm_enum_list[0]},
		{Device_Setup_Section, error_alarm_3_Id, Screen44233_str0, unsigned_int_type, sizeof(Setup_device_var.error_alarm_3), &error_alarm_Min, &error_alarm_Max, &error_alarm_Def, NO_OF_ERR_ALM, (unsigned int *)&Err_Alm_enum_list[0]},
		{Device_Setup_Section, error_alarm_2_Id, Screen44223_str0, unsigned_int_type, sizeof(Setup_device_var.error_alarm_2), &error_alarm_Min, &error_alarm_Max, &error_alarm_Def, NO_OF_ERR_ALM, (unsigned int *)&Err_Alm_enum_list[0]},
		{Device_Setup_Section, error_alarm_1_Id, Screen44213_str0, unsigned_int_type, sizeof(Setup_device_var.error_alarm_1), &error_alarm_Min, &error_alarm_Max, &error_alarm_Def, NO_OF_ERR_ALM, (unsigned int *)&Err_Alm_enum_list[0]},
		{Device_Setup_Section, Log_cal_data_Id, Screen43_str1, unsigned_int_type, sizeof(Setup_device_var.Log_cal_data), &Log_cal_data_Min, &Log_cal_data_Max, &Log_cal_data_Def, NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Device_Setup_Section, Log_run_time_data_Id, Screen43_str3, unsigned_int_type, sizeof(Setup_device_var.Log_run_time_data), &Log_run_time_data_Min, &Log_run_time_data_Max, &Log_run_time_data_Def, NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Device_Setup_Section, Log_err_data_Id, Screen433_str0, unsigned_int_type, sizeof(Setup_device_var.Log_err_data), &Log_err_data_Min, &Log_err_data_Max, &Log_err_data_Def, NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Device_Setup_Section, Log_zero_cal_data_Id, Screen434_str0, unsigned_int_type, sizeof(Setup_device_var.Log_zero_cal_data), &Log_zero_cal_data_Min, &Log_zero_cal_data_Max, &Log_zero_cal_data_Def,NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Device_Setup_Section, Log_clear_wt_data_Id, Screen435_str0, unsigned_int_type, sizeof(Setup_device_var.Log_clear_wt_data), &Log_clear_wt_data_Min, &Log_clear_wt_data_Max, &Log_clear_wt_data_Def, NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Device_Setup_Section, PID_Action_Id, Screen4512_str0, unsigned_int_type, sizeof(Setup_device_var.PID_Action), &PID_Action_Min, &PID_Action_Max, &PID_Action_Def, NO_OF_PID_ACTION, (unsigned int *)&PID_Action_enum_list[0]},
		{Device_Setup_Section, PID_Setpoint_Type_Id, Screen4513_str0, unsigned_int_type, sizeof(Setup_device_var.PID_Setpoint_Type), &PID_Setpoint_Type_Min, &PID_Setpoint_Type_Max, &PID_Setpoint_Type_Def, NO_OF_PID_SETPT_TYPE, (unsigned int *)&PID_Setpt_Type_enum_list[0]},
		{Device_Setup_Section, Printer_flow_control_Id, Screen412214_str0, unsigned_int_type, sizeof(Setup_device_var.Printer_flow_control), &Printer_flow_control_Min, &Printer_flow_control_Max, &Printer_flow_control_Def, NO_OF_FLOW_CONTROL, (unsigned int *)&Flow_Control_enum_list[0]},
		{Device_Setup_Section, Printer_status_Id, Screen41_str1, unsigned_int_type, sizeof(Setup_device_var.Printer_status), &Printer_status_Min, &Printer_status_Max, &Printer_status_Def, NO_OF_PRINTER_STATUS, (unsigned int *)&Printer_Status_enum_list[0]},
		{Device_Setup_Section, Printer_type_Id, Screen412_str0, unsigned_int_type, sizeof(Setup_device_var.Printer_type), &Printer_type_Min, &Printer_type_Max, &Printer_type_Def, NO_OF_PRINT_SB_TYPE, (unsigned int *)&Print_Type_enum_list[0]},
		{Device_Setup_Section, Rate_Output_Type_1_Id, Screen442152_str0, unsigned_int_type, sizeof(Setup_device_var.Rate_Output_Type_1), &Rate_Output_Type_Min, &Rate_Output_Type_Max, &Rate_Output_Type_Def, NO_OF_RATE_OP_TYPE, (unsigned int *)&Rate_OP_Type_enum_list[0]},
		{Device_Setup_Section, Rate_Output_Type_2_Id, Screen442252_str0, unsigned_int_type, sizeof(Setup_device_var.Rate_Output_Type_2), &Rate_Output_Type_Min, &Rate_Output_Type_Max, &Rate_Output_Type_Def, NO_OF_RATE_OP_TYPE, (unsigned int *)&Rate_OP_Type_enum_list[0]},
		{Device_Setup_Section, Rate_Output_Type_3_Id, Screen442352_str0, unsigned_int_type, sizeof(Setup_device_var.Rate_Output_Type_3), &Rate_Output_Type_Min, &Rate_Output_Type_Max, &Rate_Output_Type_Def, NO_OF_RATE_OP_TYPE, (unsigned int *)&Rate_OP_Type_enum_list[0]},
		{Device_Setup_Section, Rate_Output_Type_4_Id, Screen443132_str0, unsigned_int_type, sizeof(Setup_device_var.Rate_Output_Type_4), &Rate_Output_Type_Min, &Rate_Output_Type_Max, &Rate_Output_Type_Def, NO_OF_RATE_OP_TYPE, (unsigned int *)&Rate_OP_Type_enum_list[0]},
		{Device_Setup_Section, Rate_Output_Type_5_Id, Screen443232_str0, unsigned_int_type, sizeof(Setup_device_var.Rate_Output_Type_5), &Rate_Output_Type_Min, &Rate_Output_Type_Max, &Rate_Output_Type_Def, NO_OF_RATE_OP_TYPE, (unsigned int *)&Rate_OP_Type_enum_list[0]},
		{Device_Setup_Section, Rate_Output_Type_6_Id, Screen443332_str0, unsigned_int_type, sizeof(Setup_device_var.Rate_Output_Type_6), &Rate_Output_Type_Min, &Rate_Output_Type_Max, &Rate_Output_Type_Def, NO_OF_RATE_OP_TYPE, (unsigned int *)&Rate_OP_Type_enum_list[0]},
		{Device_Setup_Section, Relay_Output_1_Function_Id, Screen443_str1, unsigned_int_type, sizeof(Setup_device_var.Relay_Output_1_Function), &Relay_Output_Function_Min, &Relay_Output_Function_Max, &Relay_Output_Function_Def, NO_OF_RLY_OP_FUNC, (unsigned int *)&Rly_OP_Func_enum_list[0]},
		{Device_Setup_Section, Relay_Output_2_Function_Id, Screen443_str3, unsigned_int_type, sizeof(Setup_device_var.Relay_Output_2_Function), &Relay_Output_Function_Min, &Relay_Output_Function_Max, &Relay_Output_Function_Def, NO_OF_RLY_OP_FUNC, (unsigned int *)&Rly_OP_Func_enum_list[0]},
		{Device_Setup_Section, Relay_Output_3_Function_Id, Screen443_str4, unsigned_int_type, sizeof(Setup_device_var.Relay_Output_3_Function), &Relay_Output_Function_Min, &Relay_Output_Function_Max, &Relay_Output_Function_Def, NO_OF_RLY_OP_FUNC, (unsigned int *)&Rly_OP_Func_enum_list[0]},
		{Device_Setup_Section, Speed_Output_Type_1_Id, Screen442142_str0, unsigned_int_type, sizeof(Setup_device_var.Speed_Output_Type_1), &Speed_Output_Type_Min, &Speed_Output_Type_Max, &Speed_Output_Type_Def, NO_OF_SPEED_OP_TYPE, (unsigned int *)&Speed_OP_Type_enum_list[0]},
		{Device_Setup_Section, Speed_Output_Type_2_Id, Screen442242_str0, unsigned_int_type, sizeof(Setup_device_var.Speed_Output_Type_2), &Speed_Output_Type_Min, &Speed_Output_Type_Max, &Speed_Output_Type_Def, NO_OF_SPEED_OP_TYPE, (unsigned int *)&Speed_OP_Type_enum_list[0]},
		{Device_Setup_Section, Speed_Output_Type_3_Id, Screen442342_str0, unsigned_int_type, sizeof(Setup_device_var.Speed_Output_Type_3), &Speed_Output_Type_Min, &Speed_Output_Type_Max, &Speed_Output_Type_Def, NO_OF_SPEED_OP_TYPE, (unsigned int *)&Speed_OP_Type_enum_list[0]},
		{Device_Setup_Section, Speed_Output_Type_4_Id, Screen443122_str0, unsigned_int_type, sizeof(Setup_device_var.Speed_Output_Type_4), &Speed_Output_Type_Min, &Speed_Output_Type_Max, &Speed_Output_Type_Def, NO_OF_SPEED_OP_TYPE, (unsigned int *)&Speed_OP_Type_enum_list[0]},
		{Device_Setup_Section, Speed_Output_Type_5_Id, Screen443222_str0, unsigned_int_type, sizeof(Setup_device_var.Speed_Output_Type_5), &Speed_Output_Type_Min, &Speed_Output_Type_Max, &Speed_Output_Type_Def, NO_OF_SPEED_OP_TYPE, (unsigned int *)&Speed_OP_Type_enum_list[0]},
		{Device_Setup_Section, Speed_Output_Type_6_Id, Screen443322_str0, unsigned_int_type, sizeof(Setup_device_var.Speed_Output_Type_6), &Speed_Output_Type_Min, &Speed_Output_Type_Max, &Speed_Output_Type_Def, NO_OF_SPEED_OP_TYPE, (unsigned int *)&Speed_OP_Type_enum_list[0]},
		{Device_Setup_Section, Scoreboard_display_data_Id, Screen421_str0, unsigned_int_type, sizeof(Setup_device_var.Scoreboard_display_data), &Scoreboard_display_data_Min, &Scoreboard_display_data_Max, &Scoreboard_display_data_Def, NO_OF_SB_DISP_DATA, (unsigned int *)&SB_Disp_Data_enum_list[0]},
		{Device_Setup_Section, Scoreboard_type_Id, Screen42_str3, unsigned_int_type, sizeof(Setup_device_var.Scoreboard_type), &Scoreboard_type_Min, &Scoreboard_type_Max, &Scoreboard_type_Def, NO_OF_PRINT_SB_TYPE, (unsigned int *)&SB_Type_enum_list[0]},
		{Device_Setup_Section, Scoreboard_TMMode_Id, Screen42_str5, unsigned_int_type, sizeof(Setup_device_var.Scoreboard_TMMode), &Scoreboard_TMMode_Min, &Scoreboard_TMMode_Max, &Scoreboard_TMMode_Def, NO_OF_ON_OFF_STATUS, (unsigned int *)&On_Off_status_enum_list[0]},
		{Device_Setup_Section, Units_Per_Pulse_1_Id, Screen442111_str0, unsigned_int_type, sizeof(Setup_device_var.Units_Per_Pulse_1), &Units_Per_Pulse_Min, &Units_Per_Pulse_Max, &Units_Per_Pulse_Def, NO_OF_UNITS_PER_PULSE, (unsigned int *)&Units_Per_Pulse_enum_list[0]},
		{Device_Setup_Section, Units_Per_Pulse_2_Id, Screen442211_str0, unsigned_int_type, sizeof(Setup_device_var.Units_Per_Pulse_2), &Units_Per_Pulse_Min, &Units_Per_Pulse_Max, &Units_Per_Pulse_Def, NO_OF_UNITS_PER_PULSE, (unsigned int *)&Units_Per_Pulse_enum_list[0]},
		{Device_Setup_Section, Units_Per_Pulse_3_Id, Screen442311_str0, unsigned_int_type, sizeof(Setup_device_var.Units_Per_Pulse_3), &Units_Per_Pulse_Min, &Units_Per_Pulse_Max, &Units_Per_Pulse_Def, NO_OF_UNITS_PER_PULSE, (unsigned int *)&Units_Per_Pulse_enum_list[0]},
		{Device_Setup_Section, Feed_Delay_Id, Screen453_str7, unsigned_int_type, sizeof(Setup_device_var.Feed_Delay), &Feed_Delay_Min, &Feed_Delay_Max, &Feed_Delay_Def, 0, NULL},
		{Device_Setup_Section, Network_Addr_Id, Screen453_str1, unsigned_int_type, sizeof(Setup_device_var.Network_Addr), &Network_Addr_Min, &Network_Addr_Max, &Network_Addr_Def, 0, NULL},
		{Device_Setup_Section, Percent_Ingredient_Id, Screen453_str5, float_type2, sizeof(Setup_device_var.Percent_Ingredient), &Percent_Ingredient_Min, &Percent_Ingredient_Max, &Percent_Ingredient_Def, 0, NULL},
		{Device_Setup_Section, Preload_Delay_Id, Screen453_str9, unsigned_int_type, sizeof(Setup_device_var.Preload_Delay), &Preload_Delay_Min, &Preload_Delay_Max, &Preload_Delay_Def, 0, NULL},
		{Device_Setup_Section, Periodic_log_interval_Id, Screen4321_str1, unsigned_int_type, sizeof(Setup_device_var.Periodic_log_interval), &Periodic_log_interval_Min, &Periodic_log_interval_Max, &Periodic_log_interval_Def, 0, NULL},
		{Device_Setup_Section, PID_Channel_Id, Screen451_str1, unsigned_int_type, sizeof(Setup_device_var.PID_Channel), &PID_Channel_Min, &PID_Channel_Max, &PID_Channel_Def, 0, NULL},
		{Device_Setup_Section, PID_P_Term_Id, MVT_str25, unsigned_int_type, sizeof(Setup_device_var.PID_P_Term), &PID_P_Term_Min, &PID_P_Term_Max, &PID_P_Term_Def, 0, NULL},
		{Device_Setup_Section, PID_I_Term_Id, MVT_str26, unsigned_int_type, sizeof(Setup_device_var.PID_I_Term), &PID_I_Term_Min, &PID_I_Term_Max, &PID_I_Term_Def, 0, NULL},
		{Device_Setup_Section, PID_D_Term_Id, MVT_str27, unsigned_int_type, sizeof(Setup_device_var.PID_D_Term), &PID_D_Term_Min, &PID_D_Term_Max, &PID_D_Term_Def, 0, NULL},
		{Device_Setup_Section, PID_Local_Setpoint_Id, Screen4513_str1, float_type2, sizeof(Setup_device_var.PID_Local_Setpoint), &PID_Local_Setpoint_Min, &PID_Local_Setpoint_Max, &PID_Local_Setpoint_Def, 0, NULL},
		{Device_Setup_Section, Printer_ticket_num_Id, MVT_str28, unsigned_int_type, sizeof(Setup_device_var.Printer_ticket_num), &Printer_ticket_num_Min, &Printer_ticket_num_Max, &Printer_ticket_num_Def, 0, NULL},
		{Device_Setup_Section, Printer_baud_Id, Screen41221_str1, unsigned_int_type, sizeof(Setup_device_var.Printer_baud), &Printer_baud_Min, &Printer_baud_Max, &Printer_baud_Def, NO_OF_BAUD_RATE, (unsigned int *)&Baud_Rate_enum_list[0]},
		{Device_Setup_Section, Printer_data_bits_Id, Screen41221_str3, unsigned_int_type, sizeof(Setup_device_var.Printer_data_bits), &Printer_data_bits_Min, &Printer_data_bits_Max, &Printer_data_bits_Def, NO_OF_DATA_BITS, (unsigned int *)&Data_bits_enum_list[0]},
		{Device_Setup_Section, Printer_stop_bits_Id, Screen41221_str5, unsigned_int_type, sizeof(Setup_device_var.Printer_stop_bits), &Printer_stop_bits_Min, &Printer_stop_bits_Max, &Printer_stop_bits_Def, NO_OF_STOP_BITS, (unsigned int *)&Stop_bits_enum_list[0]},
		{Device_Setup_Section, Pulse_on_time_1_Id, Screen442112_str0, unsigned_int_type, sizeof(Setup_device_var.Pulse_on_time_1), &Pulse_on_time_Min, &Pulse_on_time_Max, &Pulse_on_time_Def, 0, NULL},
		{Device_Setup_Section, Pulse_on_time_2_Id, Screen442212_str0, unsigned_int_type, sizeof(Setup_device_var.Pulse_on_time_2), &Pulse_on_time_Min, &Pulse_on_time_Max, &Pulse_on_time_Def, 0, NULL},
		{Device_Setup_Section, Pulse_on_time_3_Id, Screen442312_str0, unsigned_int_type, sizeof(Setup_device_var.Pulse_on_time_3), &Pulse_on_time_Min, &Pulse_on_time_Max, &Pulse_on_time_Def, 0, NULL},
		{Device_Setup_Section, Scoreboard_alt_delay_Id, Screen4211_str0, unsigned_int_type, sizeof(Setup_device_var.Scoreboard_alt_delay), &Scoreboard_alt_delay_Min, &Scoreboard_alt_delay_Max, &Scoreboard_alt_delay_Def, 0, NULL},
		{Device_Setup_Section, Scoreboard_baud_Id, Screen422211_str0, unsigned_int_type, sizeof(Setup_device_var.Scoreboard_baud), &Scoreboard_baud_Min, &Scoreboard_baud_Max, &Scoreboard_baud_Def, NO_OF_BAUD_RATE, (unsigned int *)&Baud_Rate_enum_list[0]},
		{Device_Setup_Section, Scoreboard_data_bits_Id, Screen422212_str0, unsigned_int_type, sizeof(Setup_device_var.Scoreboard_data_bits), &Scoreboard_data_bits_Min, &Scoreboard_data_bits_Max, &Scoreboard_data_bits_Def, NO_OF_DATA_BITS, (unsigned int *)&Data_bits_enum_list[0]},
		{Device_Setup_Section, Scoreboard_stop_bits_Id, Screen422213_str0, unsigned_int_type, sizeof(Setup_device_var.Scoreboard_stop_bits), &Scoreboard_stop_bits_Min, &Scoreboard_stop_bits_Max, &Scoreboard_stop_bits_Def, NO_OF_STOP_BITS, (unsigned int *)&Stop_bits_enum_list[0]},
		{Device_Setup_Section, Addr1_Id, Screen413_str3, signed_char_type, sizeof(Setup_device_var.Addr1), NULL, NULL, NULL, 0, NULL},
		{Device_Setup_Section, Addr2_Id, Screen413_str5, signed_char_type, sizeof(Setup_device_var.Addr2), NULL, NULL, NULL, 0, NULL},
		{Device_Setup_Section, Addr3_Id, Screen413_str7, signed_char_type, sizeof(Setup_device_var.Addr3), NULL, NULL, NULL, 0, NULL},
		{Device_Setup_Section, Company_name_Id, Screen413_str1, signed_char_type, sizeof(Setup_device_var.Company_name), NULL, NULL, NULL, 0, NULL},
		{Device_Setup_Section, Phone_Id, Screen413_str9, signed_char_type, sizeof(Setup_device_var.Phone), NULL, NULL, NULL, 0, NULL},
};

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void Verify_Scale_Setup_Variables(void);
void Verify_Calibration_Variables(void);
void Verify_Admin_Variables(void);
void Verify_Setup_Device_Variables(void);
//void Get_Default_Value(U16 Variables_ID, void* pdefault_val);
void Get_Default_Value(U16 Variables_ID, void* pdefault_val, void* pmin_val, void* pmax_val,void* pcurr_val);
void Verify_Variable(const ELEMENT_REC *MVT_Table);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

void Get_Default_Value(U16 Variables_ID, void* pdefault_val, void* pmin_val, void* pmax_val,void* pcurr_val)
{
	float *f_def_val = ((float*)pdefault_val);
	float *f_min_val = ((float*)pmin_val);
	float *f_max_val = ((float*)pmax_val);
//	float *f_curr_val = ((float*)pcurr_val);
	//signed int *S32_ret_val = ((signed int*)pdefault_val);
	unsigned int *U32_ret_val = ((unsigned int*)pdefault_val);
	
	double *d_def_val = ((double*)pdefault_val);
	double *d_min_val = ((double*)pmin_val);
	double *d_max_val = ((double*)pmax_val);
//	double *d_curr_val = ((double*)pcurr_val);
	
	unsigned int *ui_def_val = ((unsigned int*)pdefault_val);
	unsigned int *ui_min_val = ((unsigned int*)pmin_val);
	unsigned int *ui_max_val = ((unsigned int*)pmax_val);
//	unsigned int *ui_curr_val = ((unsigned int*)pcurr_val);
	
	switch(Variables_ID)
	{
		//case Conveyor_angle_Id:		//Not required
		//	break;
		case Idler_distance_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_max_val);
	//	if(	*f_curr_val !=0)
	//	*f_curr_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_curr_val);
			
		break;
		
		case Wheel_diameter_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_max_val);
	//		*f_curr_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_curr_val);
				
		break;
		
		case User_Belt_Speed_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_curr_val);
			break;
		
		case Weight_unit_Id:
			//If English = distance_unit
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = TONS;									//5
					//Scale_setup_var.Weight_unit = TONS;									//5
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = TONNE;								//5
			}
			break;
		
// 		case Rate_time_unit_Id:
// 			break;
//		case Custom_LC_unit_Id:	//(default unit KG)
//			break;
			
		case Idler_unit_Id:
			//If English = distance_unit	
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = INCHES;								//2
					//Scale_setup_var.Idler_unit = INCHES;								//2
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = METERS;								//2
			}
			break;
			
		case Wheel_diameter_unit_Id:
			//If English = distance_unit	
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = INCHES;				//3
					//Scale_setup_var.Wheel_diameter_unit = INCHES;				//3
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = CENTIMETERS;	//3
			}
			break;
			
		case Belt_length_unit_Id:
			//If English = distance_unit	
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = FEET;						//4
					//Scale_setup_var.Belt_length_unit = FEET;						//4
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = METERS;					//4
			}
			break;
			
		case Speed_unit_Id:
			//If English = distance_unit	
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = FEET;									//1
					//Scale_setup_var.Speed_unit = FEET;									//1
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = METERS;								//1
			}
			break;
		
// 		case AngleSensor_Install_status_Id:
// 			break;
// 		case Number_of_idlers_Id:
// 			break;
			
			case Custom_LC_capacity_Id:
			//	*f_def_val = (unsigned int)ConvWeightUnit((unsigned int)Custom_LC_capacity_Def_unit, (unsigned int)Scale_setup_var.Custom_LC_unit, (double)*ui_def_val);
			//	*f_min_val = (unsigned int)ConvWeightUnit((unsigned int)Custom_LC_capacity_Def_unit, (unsigned int)Scale_setup_var.Custom_LC_unit, (double)*ui_min_val);
			//	*f_max_val = (unsigned int)ConvWeightUnit((unsigned int)Custom_LC_capacity_Def_unit, (unsigned int)Scale_setup_var.Custom_LC_unit, (double)*ui_max_val);
				break;
		
			
// 		case Custom_LC_output_Id:
// 			break;
// 		case Decimal_digits_Id:
// 			break;
// 		case IO_board_install_Id:
// 			break;
		
		case Idler_distanceA_Id:
		case Idler_distanceB_Id:
		case Idler_distanceC_Id:
		case Idler_distanceD_Id:
		case Idler_distanceE_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvDistUnit((unsigned int)Idler_Def_unit, (unsigned int)Scale_setup_var.Idler_unit, (double)*f_curr_val);
			break;
		
		case User_Belt_Speeed_WD_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvDistUnit((unsigned int)Wheel_diameter_Def_unit, (unsigned int)Scale_setup_var.Wheel_diameter_unit, (double)*f_curr_val);
			break;

		//Calibration section
		case Test_weight_Id:
			//double ConvWeightUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double WeightToConv)
			*f_def_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit,(double)*f_def_val);
			*f_min_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit,(double)*f_min_val);
			*f_max_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit,(double)*f_max_val);
//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_curr_val);

		//*f_ret_val = (*f_ret_val) * KG_TO_LBS_CONV_FACTOR;
			break;
		
    case Belt_scale_weight_Id:
			*f_def_val = (float)ConvWeightUnit((unsigned int)Belt_scale_Def_unit, (unsigned int)Calibration_var.Belt_scale_unit, (double)*f_def_val);
			*f_min_val = (float)ConvWeightUnit((unsigned int)Belt_scale_Def_unit, (unsigned int)Calibration_var.Belt_scale_unit, (double)*f_min_val);
			*f_max_val = (float)ConvWeightUnit((unsigned int)Belt_scale_Def_unit, (unsigned int)Calibration_var.Belt_scale_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Belt_scale_Def_unit, (unsigned int)Calibration_var.Belt_scale_unit, (double)*f_curr_val);
			break;
		
    case Cert_scale_weight_Id:
			*f_def_val = (float)ConvWeightUnit((unsigned int)Cert_scale_Def_unit, (unsigned int)Calibration_var.Cert_scale_unit, (double)*f_def_val);
			*f_min_val = (float)ConvWeightUnit((unsigned int)Cert_scale_Def_unit, (unsigned int)Calibration_var.Cert_scale_unit, (double)*f_min_val);
			*f_max_val = (float)ConvWeightUnit((unsigned int)Cert_scale_Def_unit, (unsigned int)Calibration_var.Cert_scale_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Cert_scale_Def_unit, (unsigned int)Calibration_var.Cert_scale_unit, (double)*f_curr_val);
			break;
		
    case Old_belt_length_Id:
		case New_belt_length_Id:
		case Belt_length_diff_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Belt_length_Def_unit, (unsigned int)Scale_setup_var.Belt_length_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Belt_length_Def_unit, (unsigned int)Scale_setup_var.Belt_length_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Belt_length_Def_unit, (unsigned int)Scale_setup_var.Belt_length_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvDistUnit((unsigned int)Belt_length_Def_unit, (unsigned int)Scale_setup_var.Belt_length_unit, (double)*f_curr_val);
			break;

    case Old_zero_value_Id:
		case New_zero_value_Id:
		case Zero_diff_Id:
			*f_def_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_def_val);
			*f_min_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_min_val);
			*f_max_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Belt_length_Def_unit, (unsigned int)Scale_setup_var.Belt_length_unit, (double)*f_curr_val);
			break;

//     case old_span_value_Id:
// 			break;
//     case new_span_value_Id:
// 			break;
//     case span_diff_Id:
// 			break;

    case real_time_rate_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_curr_val);
			break;
		
    case Belt_scale_unit_Id:
			//If English = distance_unit	
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = TONS;							//7
					//Calibration_var.Belt_scale_unit = TONS;							//7
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = TONNE;						//7
			}
			break;
			
    case Cert_scale_unit_Id:
			//If English = distance_unit	
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = TONS;							//6
					//Calibration_var.Cert_scale_unit = TONS;							//6
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = TONNE;						//6
			}
 			break;
			
    case Test_zero_weight_unit_Id:
			//If English = distance_unit	
			if(Scale_setup_var.Distance_unit == Screen24_str2)
			{
					*U32_ret_val = LBS;				//8
					//Calibration_var.Test_zero_weight_unit = LBS;				//8
			}
			else //If Metric = distance_unit
			{
					*U32_ret_val = KG;					//8
			}
			break;

		///////////////////////////////////////////////////////////
		
		//Admin Section
// 		case Config_Date_Year_Id:
// 			break;         
// 		case Config_Date_Mon_Id:
// 			break;
// 		case Config_Date_Day_Id:
// 			break;
// 		case Config_Time_Hr_Id:
// 			break;
// 		case Config_Time_AM_PM_Id,														 // Added on 20-Oct-2015 //Dummy Data	This is essential for packing structure
// 		case Config_Time_Min_Id,                               // Minutes  [0..59] 
// 		case Config_Time_Sec_Id, 
// 		case //IP_STRUCT IP_Addr,
// 		case IP_Addr_Addr1_Id,
// 		case IP_Addr_Addr2_Id,
// 		case IP_Addr_Addr3_Id,
// 		case IP_Addr_Addr4_Id,
// 		case //IP_STRUCT Subnet_Mask;
// 		case Subnet_Mask_Addr1_Id,
// 		case Subnet_Mask_Addr2_Id,
// 		case Subnet_Mask_Addr3_Id,
// 		case Subnet_Mask_Addr4_Id,
// 		case //IP_STRUCT Gateway;
// 		case Gateway_Addr1_Id,
// 		case Gateway_Addr2_Id,
// 		case Gateway_Addr3_Id,
// 		case Gateway_Addr4_Id,
// 		case Auto_zero_tolerance_Id,

		case Negative_rate_limit_Id:	//sks: copy of real_time_rate_Id ??
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_curr_val);
			break;
		
		//case Negative_rate_time_Id,
		
		case Zero_rate_limit_Id://sks: copy of real_time_rate_Id ??
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_curr_val);
			break;
		
// 		case Int_firmware_version_Id,
// 		case Int_update_version_Id,
// 		case Scale_Name_Id,
// 		case Plant_Name_Id,
// 		case Product_Name_Id,
// 		case Password_Id,
// 		case New_Password_Id,
// 		case Verify_New_Password_Id,
// 		case Int_update_file_name_Id,
// 		case Reload_file_name_Id,
// 		case Backup_file_name_Id,
// 		case Admin_Locked_Id,
// 		case AM_PM_Id,
// 		case Calibration_Locked_Id,
// 		case Clear_Weight_Locked_Id,
// 		case Current_Time_Format_Id,
// 		case Current_Date_Format_Id,
// 		case Day_of_week_Id,
// 		case DHCP_Status_Id,
// 		case Setup_Devices_Locked_Id,
// 		case Setup_Wizard_Locked_Id,
// 		case Zero_Calibration_Locked_Id,
// 		case Zero_rate_status_Id,
// 		case MBS_TCP_slid_Id,
// 		case Test_speed_status_Id,
// 		case Test_load_status_Id,

		case Test_load_Id:

			*f_def_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_def_val);
			*f_min_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_min_val);
			*f_max_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)*f_curr_val);
		//	Admin_var.Test_load = *f_curr_val;
		break;
		
		case Test_speed_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_curr_val);
		//	Admin_var.Test_speed = *f_curr_val;
		break;

		///////////////////////////////////////////////////////////
		//Setup Device Section
    //------analog output-----
    //case Analog_Output_1_Setpoint_Id,
		
   /* case Analog_Output_1_Maxrate_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
			break;
		*/
    //case Analog_Output_1_ZeroCal_Id,
    //case Analog_Output_2_Setpoint_Id,
		/*
    case Analog_Output_2_Maxrate_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
			break;
		*/
    //case Analog_Output_2_ZeroCal_Id,
		
		case Analog_Input_Maxrate_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_curr_val);
			break;

    case Cutoff_1_Id:
    case Cutoff_2_Id:
    case Cutoff_3_Id:
    case Cutoff_4_Id:
    case Cutoff_5_Id:
    case Cutoff_6_Id:
    case Cutoff_7_Id:
    case Cutoff_8_Id:
    case Load_weight_1_Id:
    case Load_weight_2_Id:
    case Load_weight_3_Id:
    case Load_weight_4_Id:
    case Load_weight_5_Id:
    case Load_weight_6_Id:
    case Load_weight_7_Id:
    case Load_weight_8_Id:
			*f_def_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_def_val);
			*f_min_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_min_val);
			*f_max_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_curr_val);
			break;
		

    case MinMax_Rate_Setpoint_1_Id:
    case MinMax_Rate_Setpoint_2_Id:
    case MinMax_Rate_Setpoint_3_Id:
		case MinMax_Rate_Setpoint_4_Id:
		case MinMax_Rate_Setpoint_5_Id:
		case MinMax_Rate_Setpoint_6_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_curr_val);
			break;

		case MinMax_Speed_Setpoint_1_Id:
		case MinMax_Speed_Setpoint_2_Id:
		case MinMax_Speed_Setpoint_3_Id:
		case MinMax_Speed_Setpoint_4_Id:
		case MinMax_Speed_Setpoint_5_Id:
		case MinMax_Speed_Setpoint_6_Id:
		case Min_Belt_Speed_Id:
		case Empty_Belt_Speed_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_curr_val);
			break;
	/*	
		case Feeder_Max_Rate_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
			break;			
	*/	
		case Preload_Distance_Id:
			*f_def_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_def_val);
			*f_min_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_min_val);
			*f_max_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvDistUnit((unsigned int)Speed_Def_unit, (unsigned int)Scale_setup_var.Speed_unit, (double)*f_curr_val);
	
		case Target_rate_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_curr_val);
			break;
		
		case Target_Load_Id:
			*f_def_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_def_val);
			*f_min_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_min_val);
			*f_max_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_curr_val);
			break;
		
		case Analog_Output1_Id:
			if(Setup_device_var.Analog_Output_1_Function == MA_0_20)
			{
				*f_def_val = 0.0;
			}
			else
			{
				*f_def_val = 4.0;
			}
			break;
			
		case Analog_Output2_Id:
			if(Setup_device_var.Analog_Output_2_Function == MA_0_20)
			{
				*f_def_val = 0.0;
			}
			else
			{
				*f_def_val = 4.0;
			}
			break;
		
// 		case //------------------------------
// 		case //----analog output-----
// 		case Analog_Output_1_Function_Id,
// 		case Analog_Output_2_Function_Id,
// 		case //-------IO - inputs ------
// 		case Digital_Input_1_Function_Id,
// 		case Digital_Input_2_Function_Id,
// 		case Digital_Input_3_Function_Id,
// 		case Digital_Input_4_Function_Id,
// 		case //------IO - outputs ------
// 		case Digital_Output_1_Function_Id,
// 		case Digital_Output_2_Function_Id,
// 		case Digital_Output_3_Function_Id,
// 		case //---error alarm-------
// 		case error_alarm_6_Id,
// 		case error_alarm_5_Id,
// 		case error_alarm_4_Id,
// 		case error_alarm_3_Id,
// 		case error_alarm_2_Id,
// 		case error_alarm_1_Id,
// 		case //-----USB-----
// 		case Log_cal_data_Id,
// 		case Log_run_time_data_Id,
// 		case Log_err_data_Id,
// 		case Log_zero_cal_data_Id,
// 		case Log_clear_wt_data_Id,
// 		case PID_Action_Id,
// 		case PID_Setpoint_Type_Id,
// 		case Printer_flow_control_Id,
// 		case Printer_status_Id,
// 		case Printer_type_Id,
// 		case // rate output types
// 		case Rate_Output_Type_1_Id,
// 		case Rate_Output_Type_2_Id,
// 		case Rate_Output_Type_3_Id,
// 		case Rate_Output_Type_4_Id,
// 		case Rate_Output_Type_5_Id,
// 		case Rate_Output_Type_6_Id,
// 		case //----relay---
// 		case Relay_Output_1_Function_Id,
// 		case Relay_Output_2_Function_Id,
// 		case Relay_Output_3_Function_Id,
// 		case //speed output types
// 		case Speed_Output_Type_1_Id,
// 		case Speed_Output_Type_2_Id,
// 		case Speed_Output_Type_3_Id,
// 		case Speed_Output_Type_4_Id,
// 		case Speed_Output_Type_5_Id,
// 		case Speed_Output_Type_6_Id,
// 		case Scoreboard_display_data_Id,
// 		case Scoreboard_type_Id,
// 		case Scoreboard_TMMode_Id,
// 		case Units_Per_Pulse_1_Id,
// 		case Units_Per_Pulse_2_Id,
// 		case Units_Per_Pulse_3_Id,
// 		case //------------------------------
// 		case //----blending setup----
// 		case Feed_Delay_Id,
// 		case Network_Addr_Id,
// 		case //unsigned int Percent_Ingredient;
// 		case Percent_Ingredient_Id,
// 		case Preload_Delay_Id,
// 		case //----- periodic log interval-----
// 		case Periodic_log_interval_Id,
// 		case //----PID----
// 		case PID_Channel_Id,
// 		case PID_P_Term_Id,
// 		case PID_I_Term_Id,
// 		case PID_D_Term_Id,

		case PID_Local_Setpoint_Id:
			*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
			*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
			*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
//			*f_curr_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_curr_val);
			break;
		
// 		case //-----printer
// 		case Printer_ticket_num_Id,
// 		case Printer_baud_Id,
// 		case Printer_data_bits_Id,
// 		case Printer_stop_bits_Id,
// 		case //pulse on times
// 		case Pulse_on_time_1_Id,
// 		case Pulse_on_time_2_Id,
// 		case Pulse_on_time_3_Id,
// 		case //-----scoreboard
// 		case Scoreboard_alt_delay_Id,
// 		case Scoreboard_baud_Id,
// 		case Scoreboard_data_bits_Id,
// 		case Scoreboard_stop_bits_Id,
// 		case //------------------------------
// 		case Addr1_Id,
// 		case Addr2_Id,
// 		case Addr3_Id,
// 		case Company_name_Id,
// 		case Phone_Id,		

		//Totals parameters
		 case Total_weight_accum_Id:
		 case Total_weight_Id: 
		 case Daily_Total_weight_Id:
		 case Weekly_Total_weight_Id:
		 case Monthly_Total_weight_Id:
		 case Yearly_Total_weight_Id:
		 case Job_Total_Id:
		 case Master_total_Id:
		 //case LastClearedTotal_Id:
				*d_def_val = (double)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*d_def_val);
				*d_min_val = (double)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*d_min_val);
				*d_max_val = (double)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*d_max_val);
	//			*f_curr_val = (float)ConvWeightUnit((unsigned int)Weight_Def_unit, (unsigned int)Scale_setup_var.Weight_unit, (double)*f_curr_val);
				break;
		 
		 //case LastClearedTotal_Id:
		 //break;
		 
		 /*case Avg_rate_for_mode_Id:
				*f_def_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_def_val);
				*f_min_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_min_val);
				*f_max_val = (float)ConvRateUnit((unsigned int)Rate_time_Def_unit, (unsigned int)Scale_setup_var.Rate_time_unit, (double)*f_max_val);
				break;
		 */
		 
// 		 
// 		 case RunTime_Id:
// 				break;
		 
		/* case LastCleared_Weight_unit_Id:
				//If English = distance_unit
				if(Scale_setup_var.Distance_unit == Screen24_str2)
				{
						*U32_ret_val = TONS;									//5
						//Scale_setup_var.Weight_unit = TONS;									//5
				}
				else //If Metric = distance_unit
				{
						*U32_ret_val = TONNE;								//5
				}
				break;   
 */
				
		 //case LastCleared_Rate_time_unit_Id:
 		//		break;

		default:
			
			//Dont modify the value
			break;
	}
}



/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
	
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */



/*****************************************************************************
* @note       Function name : void Verify_Scale_Setup_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Oaces Team
* @date       date created  : 10/02/2016
* @brief      Description   : This function verify all scale parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_All_Scale_Variables(void)
{
  //Verify setup veriables
	Verify_Scale_Setup_Variables();
	//Verify Calibration data
	Verify_Calibration_Variables();
	//Verify Admin Variables
	Verify_Admin_Variables();
	//Verify Device Setup Variables
	Verify_Setup_Device_Variables();
}	//function end
/*****************************************************************************
* @note       Function name : void Verify_Scale_Setup_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Oaces Team
* @date       date created  : 03/02/2016
* @brief      Description   : Verify All scale setup parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Scale_Setup_Variables(void)
{
  U16 ID_of_Variable, Temp_Id;
//	U16 size = 0;
	//Clear flags
	Scale_setup_param_set_to_default_flag = 0;
	Param_set_to_default_flag = 0;
	
	//size = sizeof(ELEMENT_REC);
	
	for(ID_of_Variable = START_OF_SCALE_SETUP_ID; ID_of_Variable <= END_OF_SCALE_SETUP_ID; ID_of_Variable++)
  {	
		Temp_Id = (ID_of_Variable - START_OF_SCALE_SETUP_ID);
		Verify_Variable((const ELEMENT_REC*)&Elements_Record_Scale_Setup[Temp_Id]);
	}	//for end
	
	//Check Param_set_to_default_flag if set then
  //set Scale_setup_param_set_to_default_flag to indicate that some params are set to default 
	if(1 == Param_set_to_default_flag)
	{
	  Scale_setup_param_set_to_default_flag = 1;
		Param_set_to_default_flag = 0;
  }

}	//function end

/*****************************************************************************
* @note       Function name : void Verify_Calibration_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Suvrat Joshi
* @date       date created  : 09/02/2016
* @brief      Description   : Verify All Calibration parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Calibration_Variables(void)
{
  U16 ID_of_Variable, Temp_Id;
	//Clear flags
	Calib_param_set_to_default_flag = 0;
	Param_set_to_default_flag = 0;
	
	for(ID_of_Variable = START_OF_CALIBRATION_ID; ID_of_Variable <= END_OF_CALIBRATION_ID; ID_of_Variable++)
  {	
		Temp_Id = (ID_of_Variable - START_OF_CALIBRATION_ID);
		Verify_Variable((const ELEMENT_REC*)&Elements_Record_Calibration[Temp_Id]);
	}	//for end
	
	//Check Param_set_to_default_flag if set then
  //set Calib_param_set_to_default_flag to indicate that some params are set to default 
	if(1 == Param_set_to_default_flag)
	{
	  Calib_param_set_to_default_flag = 1;
		Param_set_to_default_flag = 0;
  }
}	//function end

/*****************************************************************************
* @note       Function name : void Verify_Admin_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Suvrat Joshi
* @date       date created  : 09/02/2016
* @brief      Description   : Verify All Admin parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Admin_Variables(void)
{
  U16 ID_of_Variable, Temp_Id;
	Admin_param_set_to_default_flag = 0;
	Param_set_to_default_flag = 0;
	
  for(ID_of_Variable = START_OF_ADMIN_ID; ID_of_Variable <= END_OF_ADMIN_ID; ID_of_Variable++)
  {	
		//Verify_Variable((const ELEMENT_REC*)&Elements_Record_Admin[ID_of_Variable - START_OF_ADMIN_ID]);
		Temp_Id = (ID_of_Variable - START_OF_ADMIN_ID);
		Verify_Variable((const ELEMENT_REC*)&Elements_Record_Admin[Temp_Id]);
	}	//for end

	//Check Param_set_to_default_flag if set then
  //set Admin_param_set_to_default_flag to indicate that some params are set to default 
	if(1 == Param_set_to_default_flag)
	{
	  Admin_param_set_to_default_flag = 1;
		Param_set_to_default_flag = 0;
  }
}	//function end

/*****************************************************************************
* @note       Function name : void Verify_Admin_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Suvrat Joshi
* @date       date created  : 09/02/2016
* @brief      Description   : Verify All Admin parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Setup_Device_Variables(void)
{
  U16 ID_of_Variable, Temp_Id;
	Setup_device_param_set_to_default_flag = 0;
	Param_set_to_default_flag = 0;
	
	for(ID_of_Variable = START_OF_SETUP_DEVICE_ID; ID_of_Variable <= END_OF_SETUP_DEVICE_ID; ID_of_Variable++)
  {	
		Temp_Id = (ID_of_Variable - START_OF_SETUP_DEVICE_ID);
		Verify_Variable((const ELEMENT_REC*)&Elements_Record_Setup_Device[Temp_Id]);
	}	//for end

	//Check Param_set_to_default_flag if set then
  //set Setup_device_param_set_to_default_flag to indicate that some params are set to default 
	if(1 == Param_set_to_default_flag)
	{
	  Setup_device_param_set_to_default_flag = 1;
		Param_set_to_default_flag = 0;
  }
}	//function end

/*****************************************************************************
* @note       Function name : void Verify_Selected_Run_mode(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Oaces Team
* @date       date created  : 02/02/2016
* @brief      Description   : Verify Selected variable for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
unsigned int VariableId_corrupt =0;
void Verify_Variable(const ELEMENT_REC *MVT_Table)
{
	float fElemt_max,fElemt_max_t,fElemt_min,fElemt_def,fCurrent_val; 
	double dElemt_max,dElemt_min,dElemt_def,dCurrent_val;
	unsigned int uiElemt_max,uiElemt_min,uiElemt_def,uiCurrent_val;
	signed int iElemt_max,iElemt_min,iElemt_def,iCurrent_val;
	unsigned short usElemt_max,usElemt_min,usElemt_def,usCurrent_val;
	unsigned char uchElemt_max,uchElemt_min,uchElemt_def,uchCurrent_val;
	IP_STRUCT *ipElemt_max,*ipElemt_min,ipElemt_def, *ptr_ipCurrentVal;
	
	U16 Variables_Data_Type;
	void *ptr_CurrentVal;
	
	unsigned int temp_unit = 0xFFFF, i;
	unsigned int Elemt_idx_max,Elemt_idx_min,Elemt_idx_def,*Elemt_List_Address;
	unsigned int Elemt_str_min, Elemt_str_max, Elemt_str_def;//,Current_val;
	
	U16 NO_OF_OPTIONS, Variables_Section;
	U16 Variable_ID;
	
	//Get Data type of the variable
	Variables_Data_Type = MVT_Table->Element_Data_Type;
	//Get Variable ID
	Variable_ID = MVT_Table->Element_ID;
	//Get Variable ID
	Variables_Section = MVT_Table->Element_Section;
	//Get no of options
	NO_OF_OPTIONS = MVT_Table->Element_Has_Options ;
	
	if(NO_OF_OPTIONS != 0)
	{
		//Get Max, Min and Def value from table
		Elemt_idx_max = *(unsigned int*)MVT_Table->Element_Max;
		Elemt_idx_min = *(unsigned int*)MVT_Table->Element_Min;	
		Elemt_idx_def = *(unsigned int*)MVT_Table->Element_Def;	
		ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
		Current_val =    (*(unsigned int*)ptr_CurrentVal);
		
		//Get start address of option list 
		Elemt_List_Address = (unsigned int *)(MVT_Table->Element_enum_list);
		Elemt_str_min = (*(&Elemt_List_Address[Elemt_idx_min]));
		Elemt_str_max = (*(&Elemt_List_Address[Elemt_idx_max]));
		Elemt_str_def = (*(&Elemt_List_Address[Elemt_idx_def]));
		
		//Check for vaild values are entered in MVT table for Max, Min and Def 
		if((Elemt_idx_max <= (unsigned int)Elemt_idx_max) && (Elemt_idx_min <= Elemt_idx_max) && (Elemt_idx_def <= Elemt_idx_max))
		{	
			//check for all options  
			for(i = Elemt_idx_min; i < (NO_OF_OPTIONS & (~OPTION_MASK)); i++)
			{
				//Actual_Option_Val = (unsigned int *)(MVT_Table->Element_enum_list);
				//arr[i] = *(&dummy[i]);
				//Actual_Option_Val = *(unsigned int *)(&MVT_Table->Element_enum_list[i]);
				
				//Actual_Option_Val = (*(unsigned int*)&(MVT_Table->Element_enum_list[i]));
				//if(*(&Actual_Option_Val[i]) == Current_val)
				if(*(&Elemt_List_Address[i]) == Current_val)
				{
					temp_unit = i; 
				}	 
			}   
		}	
		//Check for actual option is within range if not then log error into error log file
		if((temp_unit < Elemt_idx_min) || (temp_unit > Elemt_idx_max)) 			
		{
			//Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
			
			if(NO_OF_OPTIONS & OPTION_MASK) //& 0x80000
			{	
					sprintf(error_data_log.Error_desc, Error_log_unsignedinteger_format_string,\
									'"',Strings[MVT_Table->Element_String_Enum].Text,'"', Current_val,\
									Elemt_str_min, Elemt_str_max, Elemt_str_def);	
			}	
      else
			{	
					/*sprintf(error_data_log.Error_desc, Error_log_option_format_string,\
							'"',Strings[MVT_Table->Element_String_Enum].Text, Strings[Current_val],\
							Strings[Elemt_str_min].Text, Strings[Elemt_str_max].Text, Strings[Elemt_str_def].Text);	*/
				
				sprintf(error_data_log.Error_desc, Error_log_option_format_string,\
							'"',Strings[MVT_Table->Element_String_Enum].Text,'"',\
							Strings[Elemt_str_min].Text, Strings[Elemt_str_max].Text, Strings[Elemt_str_def].Text);	
			}
			
			log_data_populate(error_log);
			
		//	if((GUI_data_nav.GUI_structure_backup != 1) )
		//	{	
				//load default value
				//Get_Default_Value(Variable_ID, (unsigned int*)&Elemt_str_def);
				Get_Default_Value(Variable_ID, (unsigned int*)&Elemt_str_def, (unsigned int*)&Elemt_str_min, (unsigned int*)&Elemt_str_max,0);
				*(unsigned int*)Scale_var_Data[Variable_ID].DataVar = Elemt_str_def;
		//	}
			Param_set_to_default_flag = 1;

			/*
			collect the variable id of the corrupt 
			element pass it for restoring from eeprom
			*/
			VariableId_corrupt = Variable_ID;
			
		}
  }//if end
	else
  {		
		switch(Variables_Data_Type)
		{
			case float_type:
			case float_type1:
			case float_type2:
				fElemt_max = *((float *)(MVT_Table->Element_Max));
				fElemt_max_t = *((float *)(MVT_Table->Element_Max));
				fElemt_min = *((float *)MVT_Table->Element_Min);
				fElemt_def = *(float *)MVT_Table->Element_Def;
				ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
				fCurrent_val = (*(float *)ptr_CurrentVal);

				
//		fCurrent_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)fCurrent_val);
 //(*(float *)ptr_CurrentVal)=fCurrent_val;
			  Get_Default_Value(Variable_ID, (float*)&fElemt_def, (float*)&fElemt_min, (float*)&fElemt_max,(float*)&fCurrent_val);
	//		*(Scale_var_Data[Variable_ID].DataVar) = fCurrent_val;
			//sks check for english to metric
				//Get_Default_Value(Variable_ID, (float*)&fElemt_def);
        //Check for range  
				if((fCurrent_val > fElemt_max) || (fCurrent_val < fElemt_min) ||(isnan(fCurrent_val)))
				{
					//Open System_Log.txt in append mode & log error
					memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
					memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
					//strcpy(error_data_log.Error_code, "MVT Warning");
					strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
					
		 			sprintf(error_data_log.Error_desc, Error_log_float_format_string,\
									'"',Strings[MVT_Table->Element_String_Enum].Text,'"',fCurrent_val,\
									fElemt_min, fElemt_max, fElemt_def);
					
					log_data_populate(error_log);
					
					//if(GUI_data_nav.GUI_structure_backup != 1)
					//{
						//load default value
						//Get_Default_Value(Variable_ID, (float*)&fElemt_def);
						*(float *)Scale_var_Data[Variable_ID].DataVar = fElemt_def;
					//}	
					Param_set_to_default_flag = 1;
				}
			break;
			case double_type:
				dElemt_max = *(double *)MVT_Table->Element_Max;
				dElemt_min = *(double *)MVT_Table->Element_Min;
				dElemt_def = *(double *)MVT_Table->Element_Def;
				ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
				dCurrent_val = (*(double *)ptr_CurrentVal);
				Get_Default_Value(Variable_ID, (double*)&dElemt_def, (double*)&dElemt_min, (double*)&dElemt_max,0);
				//Get_Default_Value(Variable_ID, (double*)&dElemt_def);
			
        //Check for range 
				if((dCurrent_val > dElemt_max) || (dCurrent_val < dElemt_min) ||(isnan(dCurrent_val)))
				{
					//Open System_Log.txt in append mode & log error
					memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
					memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
					//strcpy(error_data_log.Error_code, "MVT Warning");
					strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
					
					sprintf(error_data_log.Error_desc, Error_log_float_format_string,\
									'"',Strings[MVT_Table->Element_String_Enum].Text,'"',dCurrent_val,\
									dElemt_min, dElemt_max, dElemt_def);
					
					log_data_populate(error_log);
					
				//	if(GUI_data_nav.GUI_structure_backup != 1)
					{
						//load default value
						//Get_Default_Value(Variable_ID, (double*)&dElemt_def);
						*(double *)Scale_var_Data[Variable_ID].DataVar = dElemt_def;
					}
					Param_set_to_default_flag = 1;		
				}
			break;
			case unsigned_int_type:
				uiElemt_max = *(unsigned int *)MVT_Table->Element_Max;
				uiElemt_min = *(unsigned int *)MVT_Table->Element_Min;
				uiElemt_def = *(unsigned int *)MVT_Table->Element_Def;
				ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
				uiCurrent_val = (*(unsigned int *)ptr_CurrentVal);
        Get_Default_Value(Variable_ID, (unsigned int*)&uiElemt_def, (unsigned int*)&uiElemt_min, (unsigned int*)&uiElemt_max,0);
				//Get_Default_Value(Variable_ID, (unsigned int*)&uiElemt_def);
			
			  //Check for range     
				if((uiCurrent_val > uiElemt_max) || (uiCurrent_val < uiElemt_min) ||(isnan(uiCurrent_val)))
				{
					//Open System_Log.txt in append mode & log error
					memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
					memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
					//strcpy(error_data_log.Error_code, "MVT Warning");
					strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
					
					sprintf(error_data_log.Error_desc, Error_log_unsignedinteger_format_string,\
									'"',Strings[MVT_Table->Element_String_Enum].Text,'"',uiCurrent_val,\
									uiElemt_min, uiElemt_max, uiElemt_def);
					
					log_data_populate(error_log);
					
				//	if(GUI_data_nav.GUI_structure_backup != 1)
					{
						//load default value
						//Get_Default_Value(Variable_ID, (unsigned int*)&uiElemt_def);
						*(unsigned int *)Scale_var_Data[Variable_ID].DataVar = uiElemt_def;
					}	
					Param_set_to_default_flag = 1;
				}
			break;
			case signed_int_type:
				iElemt_max = *(signed int *)MVT_Table->Element_Max;
				iElemt_min = *(signed int *)MVT_Table->Element_Min;
				iElemt_def = *(signed int *)MVT_Table->Element_Def;
				ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
				iCurrent_val = (*(signed int *)ptr_CurrentVal);
				Get_Default_Value(Variable_ID, (signed int*)&iElemt_def, (signed int*)&iElemt_min, (signed int*)&iElemt_max,0);
				//Get_Default_Value(Variable_ID, (signed int*)&iElemt_def);
			
				if((iCurrent_val > iElemt_max) || (iCurrent_val < iElemt_min) ||(isnan(iCurrent_val)))
				{
					//Open System_Log.txt in append mode & log error
					memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
					memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
					//strcpy(error_data_log.Error_code, "MVT Warning");
					strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
					
					sprintf(error_data_log.Error_desc, Error_log_integer_format_string,\
									'"',Strings[MVT_Table->Element_String_Enum].Text,'"',iCurrent_val,\
									iElemt_min, iElemt_max, iElemt_def);
					
					log_data_populate(error_log);
					
			//		if(GUI_data_nav.GUI_structure_backup != 1)
					{
						//load default value
						//Get_Default_Value(Variable_ID, (signed int*)&iElemt_def);
						*(signed int *)Scale_var_Data[Variable_ID].DataVar = iElemt_def;
					}	
					Param_set_to_default_flag = 1;
				}
			break;
			case unsigned_short_type:
				usElemt_max = *(unsigned short *)MVT_Table->Element_Max;
				usElemt_min = *(unsigned short *)MVT_Table->Element_Min;
				usElemt_def = *(unsigned short *)MVT_Table->Element_Def;
				ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
				usCurrent_val = (*(unsigned short *)ptr_CurrentVal);
				Get_Default_Value(Variable_ID, (unsigned short*)&usElemt_def, (unsigned short*)&usElemt_min, (unsigned short*)&usElemt_max,0);
				//Get_Default_Value(Variable_ID, (unsigned short*)&usElemt_def);
			
				//Check for range
			  if((usCurrent_val > usElemt_max) || (usCurrent_val < usElemt_min) ||(isnan(usCurrent_val)))
				{
					//Open System_Log.txt in append mode & log error
					memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
					memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
					//strcpy(error_data_log.Error_code, "MVT Warning");
					strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
					
					sprintf(error_data_log.Error_desc, Error_log_unsignedinteger_format_string,\
									'"',Strings[MVT_Table->Element_String_Enum].Text,'"',usCurrent_val,\
									usElemt_min, usElemt_max, usElemt_def);
					
					log_data_populate(error_log);
					
				//	if(GUI_data_nav.GUI_structure_backup != 1)
					{
						//load default value
						//Get_Default_Value(Variable_ID, (unsigned short*)&usElemt_def);
						*(unsigned short *)Scale_var_Data[Variable_ID].DataVar = usElemt_def;
					}	
					Param_set_to_default_flag = 1;
				}
      break;				
			case unsigned_char_type:
				uchElemt_max = *(unsigned char *)MVT_Table->Element_Max;
				uchElemt_min = *(unsigned char *)MVT_Table->Element_Min;
			
			  //This is special case for to load the current time as default for some reportlog variables
				if((Variables_Section == Rprt_Log_Section) && (Variable_ID <= daily_rprt_End_load_time_Sec_Id))
				{
					if((Variable_ID == daily_rprt_Start_time_Hr_Id) || (Variable_ID == daily_rprt_End_time_Hr_Id) 
					 ||(Variable_ID == daily_rprt_Start_load_time_Hr_Id) ||(Variable_ID == daily_rprt_End_load_time_Hr_Id))
					{
					  uchElemt_def = *(unsigned char *)Report_Log_Time_Def[0].DataVar;
					}
          else if((Variable_ID == daily_rprt_Start_time_Min_Id) || (Variable_ID == daily_rprt_End_time_Min_Id) 
					 ||(Variable_ID == daily_rprt_Start_load_time_Min_Id) ||(Variable_ID == daily_rprt_End_load_time_Min_Id))
					{
						uchElemt_def = uchElemt_def = *(unsigned char *)Report_Log_Time_Def[1].DataVar;
          } 
					else if((Variable_ID == daily_rprt_Start_time_Sec_Id) || (Variable_ID == daily_rprt_End_time_Sec_Id) 
					 ||(Variable_ID == daily_rprt_Start_load_time_Sec_Id) ||(Variable_ID == daily_rprt_End_load_time_Sec_Id))
					{
						uchElemt_def = uchElemt_def = *(unsigned char *)Report_Log_Time_Def[2].DataVar;
          } 	
				}  					
			  else
				{	
				  uchElemt_def = *(unsigned char *)MVT_Table->Element_Def;
				}	
				
				Get_Default_Value(Variable_ID, (unsigned char*)&uchElemt_def, (unsigned char*)&uchElemt_min, (unsigned char*)&uchElemt_max,0);
				//Get_Default_Value(Variable_ID, (unsigned char*)&uchElemt_def);
				
				//Check for range
				if((uchElemt_min == NULL) && (uchElemt_max == NULL) && (uchElemt_def == NULL))
				{
					;	//condition for character strings
				}
				else
				{				
					ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
					uchCurrent_val = (*(unsigned char *)ptr_CurrentVal);

					if((uchCurrent_val > uchElemt_max) || (uchCurrent_val < uchElemt_min) ||(isnan(uchCurrent_val)))
					{
						//Open System_Log.txt in append mode & log error
						memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
						memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
						//strcpy(error_data_log.Error_code, "MVT Warning");
						strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
						
						sprintf(error_data_log.Error_desc, Error_log_unsignedinteger_format_string,\
										'"',Strings[MVT_Table->Element_String_Enum].Text,'"',uchCurrent_val,\
										uchElemt_min, uchElemt_max, uchElemt_def);
						
						log_data_populate(error_log);
						
					//	if(GUI_data_nav.GUI_structure_backup != 1)
						{
							//load default value
							//Get_Default_Value(Variable_ID, (unsigned char*)&uchElemt_def);
							*(unsigned char *)Scale_var_Data[Variable_ID].DataVar = uchElemt_def;
						}
						Param_set_to_default_flag = 1;
					}	//
				}
			break;	
			case signed_char_type:
           switch(Variable_ID) 				
		       { 
						case Int_firmware_version_Id:
							Admin_var.Int_firmware_version[INT_FIRM_VER_NOC-1] = '\0';
							break;
						case Int_update_version_Id:
							Admin_var.Int_update_version[INT_UPD_VER_NOC-1] = '\0';
							break;
						case Scale_Name_Id:
							Admin_var.Scale_Name[SCALE_NAME_NOC-1] = '\0';
							break;
						case Plant_Name_Id:
							Admin_var.Plant_Name[PLANT_NAME_NOC-1] = '\0'; 
							break;
						case Product_Name_Id:
	            Admin_var.Product_Name[PRODUCT_NAME_NOC-1] = '\0';
							break;  						
						case Password_Id:
							Admin_var.Password[PASSWD_NOC-1] = '\0';
							break;
						case New_Password_Id:
              Admin_var.New_Password[NEW_PASSWD_NOC-1] = '\0';
							break;  						
						case Verify_New_Password_Id:
              Admin_var.Verify_New_Password[VERIFY_PASSWD_NOC-1] = '\0';
							break;						
						case Int_update_file_name_Id:
							Admin_var.Int_update_file_name[INT_UPD_FILE_NAME_NOC-1] = '\0';
							break;
						case Reload_file_name_Id:
							Admin_var.Reload_file_name[RELOAD_FILE_NAME_NOC-1] = '\0';
							break;						
						case Backup_file_name_Id:
							Admin_var.Backup_file_name[BACKUP_FILE_NAME_NOC-1] = '\0';
							break;
						case Addr1_Id:
							Setup_device_var.Addr1[CONTACT_LEN-1] = '\0';
							break;
						case Addr2_Id:
							Setup_device_var.Addr2[CONTACT_LEN-1] = '\0';
							break;
						case Addr3_Id:
							Setup_device_var.Addr3[CONTACT_LEN-1] = '\0';
							break;
						case Company_name_Id:
							Setup_device_var.Company_name[CONTACT_LEN-1] = '\0';
							break;
						case Phone_Id:
							Setup_device_var.Phone[CONTACT_LEN-1] = '\0';
							break;
						default:
							break;
						}
      break;
			case ip_struct:
				ipElemt_max = (IP_STRUCT *)MVT_Table->Element_Max;
				ipElemt_min = (IP_STRUCT *)MVT_Table->Element_Min;
				uiElemt_max = ConvIPAddr((IP_STRUCT*)MVT_Table->Element_Max);
				uiElemt_min = ConvIPAddr((IP_STRUCT*)MVT_Table->Element_Min);
				memcpy((IP_STRUCT *)&ipElemt_def, (IP_STRUCT *)MVT_Table->Element_Def, sizeof(IP_STRUCT));
			
				//ipElemt_def.Addr1 = *(IP_STRUCT *)MVT_Table->Element_Def.Addr1;
				ptr_CurrentVal = Scale_var_Data[Variable_ID].DataVar;
			  ptr_ipCurrentVal = (IP_STRUCT*)ptr_CurrentVal;
			  uiCurrent_val = ConvIPAddr((IP_STRUCT*) ptr_CurrentVal);
				//uiCurrent_val = (*(unsigned int *)ptr_CurrentVal);
        
			  //Check for range     
				if((uiCurrent_val > uiElemt_max) || (uiCurrent_val < uiElemt_min))
				{
					//Open System_Log.txt in append mode & log error
					memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
					memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
					//strcpy(error_data_log.Error_code, "MVT Warning");
					strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
					
					sprintf(error_data_log.Error_desc, Error_log_IP_format_string,\
									'"',Strings[MVT_Table->Element_String_Enum].Text,'"',\
					        ptr_ipCurrentVal->Addr1, ptr_ipCurrentVal->Addr2, ptr_ipCurrentVal->Addr3, ptr_ipCurrentVal->Addr4,\
									ipElemt_min->Addr1, ipElemt_min->Addr2, ipElemt_min->Addr3, ipElemt_min->Addr4,\
					        ipElemt_max->Addr1, ipElemt_max->Addr2, ipElemt_max->Addr3, ipElemt_max->Addr4,\
									ipElemt_def.Addr1, ipElemt_def.Addr2, ipElemt_def.Addr3, ipElemt_def.Addr4);
					
					log_data_populate(error_log);
					
				//	if(GUI_data_nav.GUI_structure_backup != 1)
					{
						//load default value
						//Get_Default_Value(Variable_ID, (unsigned int*)&uiElemt_def);
						//*(unsigned int *)Scale_var_Data[Variable_ID].DataVar = uiElemt_def;
					
						memcpy((IP_STRUCT *)Scale_var_Data[Variable_ID].DataVar, (IP_STRUCT *)&ipElemt_def, sizeof(IP_STRUCT));
					}	
					Param_set_to_default_flag = 1;
				}
			break;
			default:
			break;					
		}//switch break	
	}//else break	
}//function break

#endif /*#ifdef MVT_TABLE*/
/*****************************************************************************
* End of file
*****************************************************************************/


