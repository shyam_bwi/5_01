/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : MVT_Totals.c
* @brief               : Controller Board
*
* @author              : Oaces Team
*
* @date Created        : January Monday, 2016  <Jan 25, 2016>
* @date Last Modified  : January Monday, 2016  <Jan 25, 2016>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            : 
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stddef.h>
#include "Global_ex.h"
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"
#include "float.h"
#include "File_update.h"
#include "Log_report_data_calculate.h"
#include "MVT.h"
#include "MVT_Totals.h"
#include "math.h"
#include "RTOS_main.h"


#ifdef MVT_TABLE
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/*#define TOTAL_WEIGHT_REC_NUM   		0 
#define DAILY_TOTAL_REC_NUM   		1 
#define WEEKLY_TOTAL_REC_NUM   		2
*/
#define NO_OF_TOTALS											8
#define HIST_TOTAL_REC_NUM   							NO_OF_TOTALS + 0 
#define HIST_AVG_RATE_REC_NUM   					NO_OF_TOTALS + 1 
#define HIST_RUN_TIME_REC_NUM   					NO_OF_TOTALS + 2
#define HIST_TOTAL_UNIT_REC_NUM     			NO_OF_TOTALS + 3
#define HIST_RATE_TIME_UNIT_REC_NUM     	NO_OF_TOTALS + 4

#define HIST_CLR_DATE_YEAR_REC_NUM     	NO_OF_TOTALS + 5
#define HIST_CLR_DATE_MON_REC_NUM      	NO_OF_TOTALS + 6
#define HIST_CLR_DATE_SEC_REC_NUM     	  NO_OF_TOTALS + 7
#define HIST_CLR_TIME_HR_REC_NUM     	  NO_OF_TOTALS + 8
#define HIST_CLR_TIME_MIN_REC_NUM     	  NO_OF_TOTALS + 9
#define HIST_CLR_TIME_SEC_REC_NUM     	  NO_OF_TOTALS + 10
#define HIST_CLR_AMPM_REC_NUM     	      NO_OF_TOTALS + 11


#define NO_OF_HIST_TOTALS       					8

/*============================================================================
* Private Data Types
*===========================================================================*/
double *All_Totals_Table[NO_OF_TOTALS] __attribute__ ((section ("GUI_DATA_RAM")))=
{
&Calculation_struct.Total_weight_accum,
&Calculation_struct.Total_weight,
&Rprts_diag_var.daily_Total_weight,
&Rprts_diag_var.weekly_Total_weight,
&Rprts_diag_var.monthly_Total_weight,
&Rprts_diag_var.yearly_Total_weight,
&Rprts_diag_var.Job_Total,
&Rprts_diag_var.Master_total	
};

unsigned char *Hist_Clr_Wt_date_time[] __attribute__ ((section ("GUI_DATA_RAM")))=
{
	&Rprts_diag_var.Clear_Date[0].Mon,
	&Rprts_diag_var.Clear_Date[1].Mon,
	&Rprts_diag_var.Clear_Date[2].Mon,
	&Rprts_diag_var.Clear_Date[3].Mon,
	&Rprts_diag_var.Clear_Date[4].Mon,
	&Rprts_diag_var.Clear_Date[5].Mon,
	&Rprts_diag_var.Clear_Date[6].Mon,
	&Rprts_diag_var.Clear_Date[7].Mon,
	&Rprts_diag_var.Clear_Date[0].Day,
	&Rprts_diag_var.Clear_Date[1].Day,
	&Rprts_diag_var.Clear_Date[2].Day,
	&Rprts_diag_var.Clear_Date[3].Day,
	&Rprts_diag_var.Clear_Date[4].Day,
	&Rprts_diag_var.Clear_Date[5].Day,
	&Rprts_diag_var.Clear_Date[6].Day,
	&Rprts_diag_var.Clear_Date[7].Day,
	&Rprts_diag_var.Clear_Time[0].Hr,
	&Rprts_diag_var.Clear_Time[1].Hr,
	&Rprts_diag_var.Clear_Time[2].Hr,
	&Rprts_diag_var.Clear_Time[3].Hr,
	&Rprts_diag_var.Clear_Time[4].Hr,
	&Rprts_diag_var.Clear_Time[5].Hr,
	&Rprts_diag_var.Clear_Time[6].Hr,
	&Rprts_diag_var.Clear_Time[7].Hr,
	&Rprts_diag_var.Clear_Time[0].Min,
	&Rprts_diag_var.Clear_Time[1].Min,
	&Rprts_diag_var.Clear_Time[2].Min,
	&Rprts_diag_var.Clear_Time[3].Min,
	&Rprts_diag_var.Clear_Time[4].Min,
	&Rprts_diag_var.Clear_Time[5].Min,
	&Rprts_diag_var.Clear_Time[6].Min,
	&Rprts_diag_var.Clear_Time[7].Min,
	&Rprts_diag_var.Clear_Time[0].Sec,
	&Rprts_diag_var.Clear_Time[1].Sec,
	&Rprts_diag_var.Clear_Time[2].Sec,
	&Rprts_diag_var.Clear_Time[3].Sec,
	&Rprts_diag_var.Clear_Time[4].Sec,
	&Rprts_diag_var.Clear_Time[5].Sec,
	&Rprts_diag_var.Clear_Time[6].Sec,
	&Rprts_diag_var.Clear_Time[7].Sec,
	&Rprts_diag_var.Clear_Time[0].AM_PM,
	&Rprts_diag_var.Clear_Time[1].AM_PM,
	&Rprts_diag_var.Clear_Time[2].AM_PM,
	&Rprts_diag_var.Clear_Time[3].AM_PM,
	&Rprts_diag_var.Clear_Time[4].AM_PM,
	&Rprts_diag_var.Clear_Time[5].AM_PM,
	&Rprts_diag_var.Clear_Time[6].AM_PM,
	&Rprts_diag_var.Clear_Time[7].AM_PM,
};
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */
 //Totals parameters Min, Max and Default constant values
 const double Total_Weight_Min = -200000; //-(DBL_MAX); //-200000 this is converted into tons
 const double Total_Weight_Max = DBL_MAX;
 const double Total_Weight_Def = 0.0;
 
 //Only for testing by DK on 27January 2016 
 /*const double Total_Weight_Min = 20.0; //1
 const double Total_Weight_Max = 200.00;
 const double Total_Weight_Def = 50.0;
 */

/* Boolean variables section */

/* Character variables section */

char Totals_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=                     		"Variable %c%s%c has value (%.3lf) which is outside valid range(%e to %e). Value is reset to default value=%.3lf";
//char Historical_Totals_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=         		"Variable %c%s%c has value (%.3lf) which is outside valid range(%e to %e). Value is reset to default value=%.3lf";
char Historical_AvgRate_RunTime_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=  		"Variable %c%s for Hist Total %d%c has value (%.3f) which is outside valid range(%e to %e). Value is reset to default value=%.3f";
//char Historical_RunTime_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=        		"Variable %c%s for Hist Total %d%c has value (%.3lf) which is outside valid range(%.3f to %.3f). Value is reset to default value=%.3lf";
char Historical_Wt_RateTime_Unit_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=  	"Variable %c%s for Hist Total %d%c  has value (%s) which is outside valid range(%s to %s). Value is reset to default value=%s";
//char Historical_RateTimeUnit_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=    	"Variable %c%s for Hist Total %d%c  has value (%s) which is outside valid range(%s to %s). Value is reset to default value=%s";
//char Historical_Clear_DateTime_Year_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))= "Variable %c%s for Hist Total %d%c has value (%d) which is outside valid range(%d to %d). Value is reset to default value=%d";
char Historical_Clear_DateTime_format_string[] __attribute__ ((section ("GUI_DATA_RAM")))=      "Variable %c%s for Hist Total %d%c has value (%u) which is outside valid range(%u to %u). Value is reset to default value=%u";

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/

DATA_VARIABLE_STRUCT Report_Log_Time_Def[] =
{
		{&Current_time.RTC_Hour},
		{&Current_time.RTC_Min},
	  {&Current_time.RTC_Sec},
};	
/* Constants section */
const float Avg_rate_for_mode_Min =  -20000000; //0.000; 
const float Avg_rate_for_mode_Max =  20000000; //2500.000;
const float Avg_rate_for_mode_Def =  0.000; 

const float Run_Time_Min =   0.000; //Min
const float Run_Time_Max =  FLT_MAX; //2628000.000;
const float Run_Time_Def =   0.000; //Min


const U8 Rprt_log_time_Hr_Min = TIME_HR_MIN;
const U8 Rprt_log_time_Hr_Max = TIME_HR_MAX;
const U8 Rprt_log_time_Hr_Def = TIME_HR_DEF;

const U8 Rprt_log_time_Min_Min = TIME_MINT_MIN;
const U8 Rprt_log_time_Min_Max = TIME_MINT_MAX;
const U8 Rprt_log_time_Min_Def = TIME_MINT_DEF;

const U8 Rprt_log_time_Sec_Min = TIME_SEC_MIN;
const U8 Rprt_log_time_Sec_Max = TIME_SEC_MAX; //59
const U8 Rprt_log_time_Sec_Def = TIME_SEC_DEF;

const U8 Rprt_log_time_Sec_1_Max =  TIME_SEC_MAX; //REPORT_LOG_TIME_SEC_1_MAX; //58
const U8 Rprt_log_time_Sec_1_Min =  TIME_SEC_MIN; //REPORT_LOG_TIME_SEC_1_MIN; //1


const U32 Daily_run_down_time_Min = TOTAL_SCALE_RUN_DOWN_TIME_MIN; 
const U32 Daily_run_down_time_Max = TOTAL_SCALE_RUN_DOWN_TIME_MAX;
const U32 Daily_run_down_time_Def = TOTAL_SCALE_RUN_DOWN_TIME_DEF;

const U32 Weekly_run_down_time_Min = (Daily_run_down_time_Min * 7); 
const U32 Weekly_run_down_time_Max = (Daily_run_down_time_Max * 7);
const U32 Weekly_run_down_time_Def = (Daily_run_down_time_Def * 7);

const U32 Monthly_run_down_time_Min = (Daily_run_down_time_Min * 31); 
const U32 Monthly_run_down_time_Max = (Daily_run_down_time_Max * 31);
const U32 Monthly_run_down_time_Def = (Daily_run_down_time_Def * 31);


const U16 Year_Min = DATE_YR_MIN;
const U16 Year_Max = DATE_YR_MAX;
const U16 Year_Def = DATE_YR_DEF;
 
const U8  Mon_Min = DATE_MON_MIN;
const U8  Mon_Max = DATE_MON_MAX;
const U8  Mon_Def = DATE_MON_DEF;
  
const U8  Day_Min = DATE_DATE_MIN;
const U8  Day_Max = DATE_DATE_MAX;
const U8  Day_Def = DATE_DATE_DEF;
 
const U8  Hr_Min = TIME_HR_MIN;
const U8  Hr_Max = TIME_HR_MAX;	
const U8  Hr_Def = TIME_HR_DEF;
 
const U8  Min_Min = TIME_MINT_MIN;
const U8  Min_Max = TIME_MINT_MAX;
const U8  Min_Def = TIME_MINT_DEF;
 
const U8  Sec_Min = TIME_SEC_MIN;
const U8  Sec_Max = TIME_SEC_MAX;
const U8  Sec_Def = TIME_SEC_DEF;

const unsigned char His_Clr_Wt_AM_PM_Min = (unsigned char)AM_TIME;
const unsigned char His_Clr_Wt_AM_PM_Max = (unsigned char)PM_TIME;
const unsigned char His_Clr_Wt_AM_PM_Def = (unsigned char)AM_TIME;

/*
const U16 Rprt_freq_header_Date_Year_Min = DATE_YR_MIN;
const U16 Rprt_freq_header_Date_Year_Max = DATE_YR_MAX;
const U16 Rprt_freq_header_Date_Year_Def  = DATE_YR_DEF;

const U8 Rprt_freq_header_Date_Mon_Min = DATE_MON_MIN;
const U8 Rprt_freq_header_Date_Mon_Max = DATE_MON_MAX;
const U8 Rprt_freq_header_Date_Mon_Def = DATE_MON_DEF;
 
const U8 Rprt_freq_header_Date_Day_Min = DATE_DATE_MIN;
const U8 Rprt_freq_header_Date_Day_Max = DATE_DATE_MAX;
const U8 Rprt_freq_header_Date_Day_Def = DATE_DATE_DEF;

const U8 Rprt_freq_header_Time_Hr_Min = TIME_HR_MIN;
const U8 Rprt_freq_header_Time_Hr_Max = TIME_HR_MAX;	
const U8 Rprt_freq_header_Time_Hr_Def = TIME_HR_DEF;

const U8 Rprt_freq_header_Time_Min_Min = TIME_MINT_MIN;
const U8 Rprt_freq_header_Time_Min_Max = TIME_MINT_MAX;
const U8 Rprt_freq_header_Time_Min_Def = TIME_MINT_DEF;

const U8 Rprt_freq_header_Time_Sec_Min = TIME_SEC_MIN;
const U8 Rprt_freq_header_Time_Sec_Max = TIME_SEC_MAX;
const U8 Rprt_freq_header_Time_Sec_Def = TIME_SEC_DEF;
*/

const float Calculation_struct_run_time_Min = Run_Time_Min;
const float Calculation_struct_run_time_Max = Run_Time_Max;
const float Calculation_struct_run_time_Def = Run_Time_Def;

//Populated MVT table for Totals setup parameters
const ELEMENT_REC Elements_Record_Totals_Setup[] =
{
	//Element_Section, Element_ID, Element_String_Enum,	Element_Data_Type,	Element_Data_Length, Element_Min_Value	Element_Max_Value	Element_Default_Value, Has Element Option, Element Validation function ptr
  {Totals_Section, Total_weight_accum_Id, Screen7_str1, double_type, sizeof(Calculation_struct.Total_weight_accum), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, Total_weight_Id, Total_Weight, double_type, sizeof(Calculation_struct.Total_weight), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, Daily_Total_weight_Id, Screen51_str3, double_type, sizeof(Rprts_diag_var.daily_Total_weight), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, Weekly_Total_weight_Id, Screen51_str5, double_type, sizeof(Rprts_diag_var.weekly_Total_weight), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
  {Totals_Section, Monthly_Total_weight_Id, Screen51_str7, double_type, sizeof(Rprts_diag_var.monthly_Total_weight), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, Yearly_Total_weight_Id, Screen51_str9, double_type, sizeof(Rprts_diag_var.yearly_Total_weight), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, Job_Total_Id, Screen51_str1, double_type, sizeof(Rprts_diag_var.Job_Total), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, Master_total_Id, Screen51_str11, double_type, sizeof(Rprts_diag_var.Master_total), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, LastClearedTotal_Id, Screen76_str3, double_type, sizeof(Rprts_diag_var.LastClearedTotal[0]), &Total_Weight_Min, &Total_Weight_Max, &Total_Weight_Def, 0, NULL},//
	{Totals_Section, Avg_rate_for_mode_Id, Screen76_str1, float_type, sizeof(Rprts_diag_var.Avg_rate_for_mode[0]), &Avg_rate_for_mode_Min, &Avg_rate_for_mode_Max, &Avg_rate_for_mode_Def, 0, NULL},//
  {Totals_Section, RunTime_Id, Screen76_str2, float_type, sizeof(Rprts_diag_var.RunTime[0]), &Run_Time_Min, &Run_Time_Max, &Run_Time_Def, 0, NULL},//
  {Totals_Section, LastCleared_Weight_unit_Id, Screen2_str9, unsigned_int_type, sizeof(Rprts_diag_var.LastCleared_Weight_unit[0]), &Weight_unit_Min, &Weight_unit_Max, &Weight_unit_Def, 0, NULL},//
	{Totals_Section, LastCleared_Rate_time_unit_Id, Screen2_str11, unsigned_int_type, sizeof(Rprts_diag_var.LastCleared_Rate_time_unit[0]), &Rate_time_unit_Min, &Rate_time_unit_Max, &Rate_time_unit_Def, 0, NULL},//
  
	{Totals_Section, LastClearedTotal_Date_Year_Id, MVT_str12, unsigned_short_type, sizeof(Rprts_diag_var.Clear_Date[0].Year), &Year_Min, &Year_Max, &Year_Def, 0, NULL},
	{Totals_Section, LastClearedTotal_Date_Mon_Id, MVT_str13, unsigned_char_type, sizeof(Rprts_diag_var.Clear_Date[0].Mon), &Mon_Min, &Mon_Max, &Mon_Def, 0, NULL},
	{Totals_Section, LastClearedTotal_Date_Day_Id, MVT_str14, unsigned_char_type, sizeof(Rprts_diag_var.Clear_Date[0].Day ), &Day_Min, &Day_Max, &Day_Def, 0, NULL},
	{Totals_Section, LastClearedTotal_Time_Hr_Id, MVT_str15, unsigned_char_type, sizeof(Rprts_diag_var.Clear_Time[0].Hr), &Hr_Min, &Hr_Max, &Hr_Def, 0, NULL},
	{Totals_Section, LastClearedTotal_Time_Min_Id, MVT_str17, unsigned_char_type, sizeof(Rprts_diag_var.Clear_Time[0].Min), &Min_Min, &Min_Max, &Min_Def, 0, NULL},
	{Totals_Section, LastClearedTotal_Time_Sec_Id, MVT_str18, unsigned_char_type, sizeof(Rprts_diag_var.Clear_Time[0].Sec), &Sec_Min, &Sec_Max, &Sec_Def, 0, NULL},
  {Totals_Section, LastClearedTotal_AM_PM_Id, MVT_str16, unsigned_char_type, sizeof(Rprts_diag_var.Clear_Time[0].AM_PM), &His_Clr_Wt_AM_PM_Min, &His_Clr_Wt_AM_PM_Max, &His_Clr_Wt_AM_PM_Def, 0, NULL},


};	


//Populated MVT table for report log parameters
const ELEMENT_REC Elements_Record_Rpt_Log_Variables[] =
{
	//Element_Section, Element_ID, Element_String_Enum,	Element_Data_Type,	Element_Data_Length, Element_Min_Value	Element_Max_Value	Element_Default_Value, Has Element Option, Element Validation function ptr
	{Rprt_Log_Section, daily_rprt_Start_time_Hr_Id, MVT_Rpt_Str1, unsigned_char_type, sizeof(daily_rprt.Start_time.Hr), &Rprt_log_time_Hr_Min, &Rprt_log_time_Hr_Max, &Rprt_log_time_Hr_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_Start_time_Min_Id, MVT_Rpt_Str2, unsigned_char_type, sizeof(daily_rprt.Start_time.Min), &Rprt_log_time_Min_Min, &Rprt_log_time_Min_Max, &Rprt_log_time_Min_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_Start_time_Sec_Id, MVT_Rpt_Str3, unsigned_char_type, sizeof(daily_rprt.Start_time.Sec), &Rprt_log_time_Sec_Min, &Rprt_log_time_Sec_1_Max, &Rprt_log_time_Sec_Def, 0, NULL},

  {Rprt_Log_Section, daily_rprt_End_time_Hr_Id, MVT_Rpt_Str4, unsigned_char_type, sizeof(daily_rprt.End_time.Hr), &Rprt_log_time_Hr_Min, &Rprt_log_time_Hr_Max, &Rprt_log_time_Hr_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_End_time_Min_Id, MVT_Rpt_Str5, unsigned_char_type, sizeof(daily_rprt.End_time.Min), &Rprt_log_time_Min_Min, &Rprt_log_time_Min_Max, &Rprt_log_time_Min_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_End_time_Sec_Id, MVT_Rpt_Str6, unsigned_char_type, sizeof(daily_rprt.End_time.Sec), &Rprt_log_time_Sec_1_Min, &Rprt_log_time_Sec_Max, &Rprt_log_time_Sec_Def, 0, NULL},

	{Rprt_Log_Section, daily_rprt_Start_load_time_Hr_Id, MVT_Rpt_Str7, unsigned_char_type, sizeof(daily_rprt.Start_load_time.Hr), &Rprt_log_time_Hr_Min, &Rprt_log_time_Hr_Max, &Rprt_log_time_Hr_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_Start_load_time_Min_Id, MVT_Rpt_Str8, unsigned_char_type, sizeof(daily_rprt.Start_load_time.Min), &Rprt_log_time_Min_Min, &Rprt_log_time_Min_Max, &Rprt_log_time_Min_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_Start_load_time_Sec_Id, MVT_Rpt_Str9, unsigned_char_type, sizeof(daily_rprt.Start_load_time.Sec), &Rprt_log_time_Sec_Min, &Rprt_log_time_Sec_1_Max, &Rprt_log_time_Sec_Def, 0, NULL},

  {Rprt_Log_Section, daily_rprt_Start_load_time_Hr_Id, MVT_Rpt_Str10, unsigned_char_type, sizeof(daily_rprt.End_load_time.Hr), &Rprt_log_time_Hr_Min, &Rprt_log_time_Hr_Max, &Rprt_log_time_Hr_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_Start_load_time_Min_Id, MVT_Rpt_Str11, unsigned_char_type, sizeof(daily_rprt.End_load_time.Min), &Rprt_log_time_Min_Min, &Rprt_log_time_Min_Max, &Rprt_log_time_Min_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_Start_load_time_Sec_Id, MVT_Rpt_Str12, unsigned_char_type, sizeof(daily_rprt.End_load_time.Sec), &Rprt_log_time_Sec_Min, &Rprt_log_time_Sec_1_Max, &Rprt_log_time_Sec_Def, 0, NULL},
	
	{Rprt_Log_Section, daily_rprt_Down_time_Id, MVT_Rpt_Str13, unsigned_int_type, sizeof(daily_rprt.Down_time), &Daily_run_down_time_Min, &Daily_run_down_time_Max, &Daily_run_down_time_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_Run_time_Id, MVT_Rpt_Str14, unsigned_int_type, sizeof(daily_rprt.Run_time), &Daily_run_down_time_Min, &Daily_run_down_time_Max, &Daily_run_down_time_Def, 0, NULL},

	{Rprt_Log_Section, weekly_rprt_Down_time_Id, MVT_Rpt_Str15, unsigned_int_type, sizeof(weekly_rprt.Down_time), &Weekly_run_down_time_Min, &Weekly_run_down_time_Max, &Weekly_run_down_time_Def, 0, NULL},
  {Rprt_Log_Section, weekly_rprt_Run_time_Id, MVT_Rpt_Str16, unsigned_int_type, sizeof(weekly_rprt.Run_time), &Weekly_run_down_time_Min, &Weekly_run_down_time_Max, &Weekly_run_down_time_Def, 0, NULL},
	
	{Rprt_Log_Section, monthly_rprt_Down_time_Id, MVT_Rpt_Str17, unsigned_int_type, sizeof(monthly_rprt.Down_time), &Monthly_run_down_time_Min, &Monthly_run_down_time_Max, &Monthly_run_down_time_Def, 0, NULL},
  {Rprt_Log_Section, monthly_rprt_Run_time_Id, MVT_Rpt_Str18, unsigned_int_type, sizeof(monthly_rprt.Run_time), &Monthly_run_down_time_Min, &Monthly_run_down_time_Max, &Monthly_run_down_time_Def, 0, NULL},

  {Rprt_Log_Section, daily_rprt_freq_rprt_header_Date_Year_Id, MVT_Rpt_Str19, unsigned_short_type, sizeof(daily_rprt.freq_rprt_header.Date.Year), &Year_Min, &Year_Max, &Year_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_freq_rprt_header_Date_Mon_Id, MVT_Rpt_Str20, unsigned_char_type, sizeof(daily_rprt.freq_rprt_header.Date.Mon), &Mon_Min, &Mon_Max, &Mon_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_freq_rprt_header_Date_Day_Id, MVT_Rpt_Str21, unsigned_char_type, sizeof(daily_rprt.freq_rprt_header.Date.Day ), &Day_Min, &Day_Max, &Day_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_freq_rprt_header_Time_Hr_Id, MVT_Rpt_Str22, unsigned_char_type, sizeof(daily_rprt.freq_rprt_header.Time.Hr), &Hr_Min, &Hr_Max, &Hr_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_freq_rprt_header_Time_Min_Id, MVT_Rpt_Str23, unsigned_char_type, sizeof(daily_rprt.freq_rprt_header.Time.Min), &Min_Min, &Min_Max, &Min_Def, 0, NULL},
	{Rprt_Log_Section, daily_rprt_freq_rprt_header_Time_Sec_Id, MVT_Rpt_Str24, unsigned_char_type, sizeof(daily_rprt.freq_rprt_header.Time.Sec), &Sec_Min, &Sec_Max, &Sec_Def, 0, NULL},

	{Rprt_Log_Section, weekly_rprt_freq_rprt_header_Date_Year_Id, MVT_Rpt_Str25, unsigned_short_type, sizeof(weekly_rprt.freq_rprt_header.Date.Year), &Year_Min, &Year_Max, &Year_Def, 0, NULL},
	{Rprt_Log_Section, weekly_rprt_freq_rprt_header_Date_Mon_Id, MVT_Rpt_Str26, unsigned_char_type, sizeof(weekly_rprt.freq_rprt_header.Date.Mon), &Mon_Min, &Mon_Max, &Mon_Def, 0, NULL},
	{Rprt_Log_Section, weekly_rprt_freq_rprt_header_Date_Day_Id, MVT_Rpt_Str27, unsigned_char_type, sizeof(weekly_rprt.freq_rprt_header.Date.Day ), &Day_Min, &Day_Max, &Day_Def, 0, NULL},
	{Rprt_Log_Section, weekly_rprt_freq_rprt_header_Time_Hr_Id, MVT_Rpt_Str28, unsigned_char_type, sizeof(weekly_rprt.freq_rprt_header.Time.Hr), &Hr_Min, &Hr_Max, &Hr_Def, 0, NULL},
	{Rprt_Log_Section, weekly_rprt_freq_rprt_header_Time_Min_Id, MVT_Rpt_Str29, unsigned_char_type, sizeof(weekly_rprt.freq_rprt_header.Time.Min), &Min_Min, &Min_Max, &Min_Def, 0, NULL},
	{Rprt_Log_Section, weekly_rprt_freq_rprt_header_Time_Sec_Id, MVT_Rpt_Str30, unsigned_char_type, sizeof(weekly_rprt.freq_rprt_header.Time.Sec), &Sec_Min, &Sec_Max, &Sec_Def, 0, NULL},

	{Rprt_Log_Section, monthly_rprt_freq_rprt_header_Date_Year_Id, MVT_Rpt_Str31, unsigned_short_type, sizeof(monthly_rprt.freq_rprt_header.Date.Year), &Year_Min, &Year_Max, &Year_Def, 0, NULL},
	{Rprt_Log_Section, monthly_rprt_freq_rprt_header_Date_Mon_Id, MVT_Rpt_Str32, unsigned_char_type, sizeof(monthly_rprt.freq_rprt_header.Date.Mon), &Mon_Min, &Mon_Max, &Mon_Def, 0, NULL},
	{Rprt_Log_Section, monthly_rprt_freq_rprt_header_Date_Day_Id, MVT_Rpt_Str33, unsigned_char_type, sizeof(monthly_rprt.freq_rprt_header.Date.Day ), &Day_Min, &Day_Max, &Day_Def, 0, NULL},
	{Rprt_Log_Section, monthly_rprt_freq_rprt_header_Time_Hr_Id, MVT_Rpt_Str34, unsigned_char_type, sizeof(monthly_rprt.freq_rprt_header.Time.Hr), &Hr_Min, &Hr_Max, &Hr_Def, 0, NULL},
	{Rprt_Log_Section, monthly_rprt_freq_rprt_header_Time_Min_Id, MVT_Rpt_Str35, unsigned_char_type, sizeof(monthly_rprt.freq_rprt_header.Time.Min), &Min_Min, &Min_Max, &Min_Def, 0, NULL},
	{Rprt_Log_Section, monthly_rprt_freq_rprt_header_Time_Sec_Id, MVT_Rpt_Str36, unsigned_char_type, sizeof(monthly_rprt.freq_rprt_header.Time.Sec), &Sec_Min, &Sec_Max, &Sec_Def, 0, NULL},
	
	{Rprt_Log_Section, Calculation_struct_run_time_Id, MVT_Rpt_Str37, float_type2, sizeof(Calculation_struct.run_time), &Calculation_struct_run_time_Min, &Calculation_struct_run_time_Max, &Calculation_struct_run_time_Def, 0, NULL},

};

/* Boolean variables section */

/* Character variables section */
unsigned char  Totals_set_to_default_flag = 0;
unsigned char  Report_Log_set_to_default_flag = 0;
unsigned char Historical_Totals_set_to_default_flag = 0;
/* unsigned integer variables section */
	
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
//extern DATA_VARIABLE_STRUCT Report_Log_Time_Def[];
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void Verify_Totals(void);
void Verify_Historical_Totals(void);

void Verify_Rprt_Log_Variables(void);
void VerifyAllTotals(void);
void VerifyHistoricalTotals(void);
void VerifyHistoricalAvgRate(void); 
void VerifyHistoricalRunTime(void);
void VerifyHistoricalWtUnits(void);
void VerifyHistoricalRateTimeUnit(void);
void VerifyHistorical_Clear_DateTime(void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name : void Verify_Totals_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Oaces Team
* @date       date created  : 08/02/2016
* @brief      Description   : Verify All totals parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Totals(void)
{
	//Clear flags
	Totals_set_to_default_flag = 0;
	Param_set_to_default_flag = 0;
	
	VerifyAllTotals();
		
	//Check Param_set_to_default_flag if set then
  //set Totals_set_to_default_flag to indicate that some params are set to default 
	if(1 == Param_set_to_default_flag)
	{
		Totals_set_to_default_flag = 1;
		Param_set_to_default_flag = 0;
  }
	
 //Verify report parameters
	Verify_Rprt_Log_Variables();
	
}	//function end

/*****************************************************************************
* @note       Function name : void Verify_Totals_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Oaces Team
* @date       date created  : 08/02/2016
* @brief      Description   : Verify All Historical totals parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Historical_Totals(void)
{
	//Clear flags
	Historical_Totals_set_to_default_flag = 0;
	Param_set_to_default_flag = 0;
	
	VerifyHistoricalTotals();
	VerifyHistoricalAvgRate();
	VerifyHistoricalRunTime();
	VerifyHistoricalWtUnits();
	VerifyHistoricalRateTimeUnit();
	VerifyHistorical_Clear_DateTime();
	//Check Param_set_to_default_flag if set then
  //set Totals_set_to_default_flag to indicate that some params are set to default 
	if(1 == Param_set_to_default_flag)
	{
		Historical_Totals_set_to_default_flag = 1;
		Param_set_to_default_flag = 0;
  }		
}	//function end
/*****************************************************************************
* @note       Function name : void Verify_Rprt_Log_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Oaces Team
* @date       date created  : 08/02/2016
* @brief      Description   : Verify All report log parameter for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Rprt_Log_Variables(void)
{
  U16 ID_of_Variable, ID_Of_Varibale_After_Offset;

	//Get current time for init the defult time
	Current_time = RTCGetTime();
	
	//Clear flags
	Report_Log_set_to_default_flag = 0;
	Param_set_to_default_flag = 0;
	
	for(ID_of_Variable = START_OF_REPORT_LOG_ID; ID_of_Variable <= END_OF_REPORT_LOG_ID; ID_of_Variable++)
  {	
		ID_Of_Varibale_After_Offset = ID_of_Variable - START_OF_REPORT_LOG_ID;
		Verify_Variable(&Elements_Record_Rpt_Log_Variables[ID_Of_Varibale_After_Offset]);
	}	//for end
	
  //Check Param_set_to_default_flag if set tehn
  //set Report_Log_set_to_default_flag to indicate that some params are set to default 
	if(1 == Param_set_to_default_flag)
	{
	  Report_Log_set_to_default_flag = 1;
		Param_set_to_default_flag = 0;
  }
}	//function end
//*****************************************************************************
void VerifyAllTotals(void)
{
	unsigned int i; 
	//double *temp1,*temp2; 
	//char temp_buf[30];
	U16 Variable_ID = 0;
	double Elemt_def;
	double dTemp_Min,dTemp_Max; 
	
	for(i=0; i < NO_OF_TOTALS; i++)
	{	
		Variable_ID = Elements_Record_Totals_Setup[i].Element_ID;
		
		dTemp_Max = *(double*)Elements_Record_Totals_Setup[i].Element_Max;
		dTemp_Min = *(double*)Elements_Record_Totals_Setup[i].Element_Min;
		Elemt_def = (*(double*)(Elements_Record_Totals_Setup[i].Element_Def));
   	Get_Default_Value(Variable_ID, (double*)&Elemt_def, (double*)&dTemp_Min, (double*)&dTemp_Max,(double*)All_Totals_Table[i]);
		
		/*memset(temp_buf, 0x20, sizeof(temp_buf)); //fill with space 
		if(sizeof(Strings[Elements_Record_Totals_Setup[i].Element_String_Enum].Text) > sizeof(temp_buf))
		strncpy(temp_buf,Strings[Elements_Record_Totals_Setup[i].Element_String_Enum].Text,sizeof(temp_buf));
    else 		
		strcpy(temp_buf,Strings[Elements_Record_Totals_Setup[i].Element_String_Enum].Text);
		*/
		
		if((*(double*)All_Totals_Table[i] > dTemp_Max) ||
			 (*(double*)All_Totals_Table[i] < dTemp_Min) || isnan(*(double*)All_Totals_Table[i]))
		{
			//Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
			//strcpy(error_data_log.Error_code, "MVT Warning");
			/*sprintf(error_data_log.Error_desc,"%s\t %s\t\t %11.3lf\t %e\t %e\t %11.3lf\t",\
			"All Totals",temp_buf,*(double*)All_Totals_Table[i],*(double*)temp2,*(double*)temp1,\
			*(double*)Elements_Record_Totals_Setup[i].Element_Def);*/
			sprintf(error_data_log.Error_desc,Totals_format_string,\
			        '"',Strings[Elements_Record_Totals_Setup[i].Element_String_Enum].Text, '"',*(double*)All_Totals_Table[i],\
			        dTemp_Min,dTemp_Max,Elemt_def);
			
			log_data_populate(error_log);
			
			//*(double*)All_Totals_Table[i] = *(double*)Elements_Record_Totals_Setup[i].Element_Def;
			
			//Elemt_def = (*(double*)(Elements_Record_Totals_Setup[i].Element_Def));
			//Get_Default_Value(Variable_ID, (double*)&Elemt_def);		//Suvrat
			*(double*)All_Totals_Table[i] = Elemt_def;
			Param_set_to_default_flag = 1;
		 }
		 /*else //only for testing remove it 
		 {
			 //Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, "MVT Warning");
			sprintf(error_data_log.Error_desc,"Variable %c%s%c is outside valid range(%e to %e). Value is reset to default value=%.3lf",\
			        '"',Strings[Elements_Record_Totals_Setup[i].Element_String_Enum].Text,'"',\
			        *(double*)temp2,*(double*)temp1,*(double*)Elements_Record_Totals_Setup[i].Element_Def);
			 
			log_data_populate(error_log);
     }*/ 
	}	 
}


void VerifyHistoricalTotals(void)
{
	unsigned int i; 
	//double *temp1,*temp2; 
	//char temp_buf[30];
	U16 Variable_ID = 0;
	double Elemt_def;
	double dTemp_Max,dTemp_Min; 
	
	Variable_ID = Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_ID;
	
	dTemp_Max = *(double*)Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_Max;
	dTemp_Min = *(double*)Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_Min;	
	Elemt_def = (*(double*)(Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_Def));
	Get_Default_Value(Variable_ID, (double*)&Elemt_def, (double*)&dTemp_Min,(double*)&dTemp_Max,0);
	
	//Rprts_diag_var.LastClearedTotal[i]
	for(i=0; i < NO_OF_HIST_TOTALS; i++)
	{
		/*
		memset(temp_buf, 0x20, sizeof(temp_buf)); //fill with space 
		if(sizeof(Strings[Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_String_Enum + i].Text) > sizeof(temp_buf))
		strncpy(temp_buf,Strings[Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_String_Enum + i].Text,sizeof(temp_buf));
    else 		
		strcpy(temp_buf,Strings[Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_String_Enum + i].Text);
		*/
		
		if((Rprts_diag_var.LastClearedTotal[i] > dTemp_Max) || \
			 (Rprts_diag_var.LastClearedTotal[i] < dTemp_Min) || \
		   (isnan(Rprts_diag_var.LastClearedTotal[i])))
		{
			//Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
			//strcpy(error_data_log.Error_code, "MVT Warning");
			/*sprintf(error_data_log.Error_desc,"%s\t %s\t\t %11.3lf\t %e\t %e\t %11.3lf\t",\
			"All Totals",temp_buf,Rprts_diag_var.LastClearedTotal[i],*(double*)temp2,*(double*)temp1,\
			*(double*)Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_Def);*/
			sprintf(error_data_log.Error_desc,Totals_format_string,\
			        '"',Strings[Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_String_Enum + i].Text,'"',Rprts_diag_var.LastClearedTotal[i],\
			        dTemp_Min,dTemp_Max,Elemt_def);
			
			log_data_populate(error_log);
			
			//Elemt_def = (*(double*)(Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_Def));
			//Get_Default_Value(Variable_ID, (double*)&Elemt_def);		//Suvrat
			Rprts_diag_var.LastClearedTotal[i] = Elemt_def;
			//Rprts_diag_var.LastClearedTotal[i] = *(double*)Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_Def;
			Historical_Totals_set_to_default_flag = 1;
		 }
		 /*else //only for testing remove it 
		 {
			 //Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, "MVT Warning");
			
			sprintf(error_data_log.Error_desc,"Variable %c%s%c is outside valid range(%e to %e). Value is reset to default value=%.3lf",\
			        '"',Strings[Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_String_Enum + i].Text,'"',\
			        *(double*)temp2,*(double*)temp1,*(double*)Elements_Record_Totals_Setup[HIST_TOTAL_REC_NUM].Element_Def);
			log_data_populate(error_log);
     }*/
	 }		 
}

void VerifyHistoricalAvgRate(void)
{
	unsigned int i; 
	//float *temp1,*temp2; 
	float fTemp_Max,fTemp_Min; 
	
	//char temp_buf[60];
	U16 Variable_ID = 0;
	float Elemt_def;
	
	Variable_ID = Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_ID;
	
	fTemp_Max = *(float*)Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Max;
	fTemp_Min = *(float*)Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Min;	
	Elemt_def = (*(float*)(Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Def));
		
	
	Get_Default_Value(Variable_ID, (float*)&Elemt_def, (float*)&fTemp_Min, (float*)&fTemp_Max,0);
	
	
	//Rprts_diag_var.Avg_rate_for_mode[i]
	for(i=0; i < NO_OF_HIST_TOTALS; i++)
	{
		/*memset(temp_buf, 0x20, sizeof(temp_buf)); //fill with space 
		if(sizeof(Strings[Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_String_Enum].Text) > sizeof(temp_buf))
		strncpy(temp_buf,Strings[Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_String_Enum].Text,sizeof(temp_buf));
    else 		
		sprintf(temp_buf,"%s for Hist Total %d",Strings[Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_String_Enum].Text,i+1);
		*/
		
		if((Rprts_diag_var.Avg_rate_for_mode[i] > fTemp_Max) || \
			 (Rprts_diag_var.Avg_rate_for_mode[i] < fTemp_Min) || \
		   (isnan(Rprts_diag_var.Avg_rate_for_mode[i])))
		{
			//Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
			//strcpy(error_data_log.Error_code, "MVT Warning");
			
			/*sprintf(error_data_log.Error_desc,"%s\t %s\t\t %11.3f\t %11.3f\t %11.3f\t %11.3f\t",\
			"All Totals",temp_buf,Rprts_diag_var.Avg_rate_for_mode[i],*(float*)temp2,*(float*)temp1,\
			*(float*)Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Def);*/
			sprintf(error_data_log.Error_desc,Historical_AvgRate_RunTime_format_string,\
			        '"',Strings[Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_String_Enum].Text,i+1,'"',Rprts_diag_var.Avg_rate_for_mode[i],\
			        fTemp_Min,fTemp_Max,Elemt_def);
			
			log_data_populate(error_log);
			//Elemt_def = (*(float*)(Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Def));
			//Get_Default_Value(Variable_ID, (float*)&Elemt_def);		//Suvrat
			Rprts_diag_var.Avg_rate_for_mode[i] = Elemt_def;
			//Rprts_diag_var.Avg_rate_for_mode[i] = *(float*)Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Def;
			Historical_Totals_set_to_default_flag = 1;
		 }
		 /*else //only for testing remove it 
		 {
			 //Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, "MVT Warning");
			sprintf(error_data_log.Error_desc,"Variable %c%s for Hist Total %d%c is outside valid range(%.3f to %.3f). Value is reset to default value=%.3lf",\
			        '"',Strings[Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_String_Enum].Text,i+1,'"',\
			        *(float*)temp2,*(float*)temp1,*(float*)Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Def);
			
			 log_data_populate(error_log);
     }*/
	 }		 
}

void VerifyHistoricalRunTime(void)
{
	unsigned int i; 
	//float *temp1,*temp2; 
	//char temp_buf[60];
	U16 Variable_ID = 0;
	float Elemt_def;
	float fTemp_Max,fTemp_Min; 
	
	Variable_ID = Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_ID;
	
	fTemp_Max = *(float*)Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_Max;
	fTemp_Min = *(float*)Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_Min;	
	Elemt_def = (*(float*)(Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_Def));
	Get_Default_Value(Variable_ID, (float*)&Elemt_def, (float*)&fTemp_Min, (float*)&fTemp_Max,0);
	
	//Rprts_diag_var.RunTime[i]
	for(i=0; i < NO_OF_HIST_TOTALS; i++)
	{
		/*memset(temp_buf, 0x20, sizeof(temp_buf)); //fill with space 
		if(sizeof(Strings[Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_String_Enum].Text) > sizeof(temp_buf))
		strncpy(temp_buf,Strings[Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_String_Enum].Text,sizeof(temp_buf));
    else 		
		sprintf(temp_buf,"%s for Hist Total %d",Strings[Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_String_Enum].Text,i+1);
		*/
		
		if((Rprts_diag_var.RunTime[i] > fTemp_Max) || \
			 (Rprts_diag_var.RunTime[i] < fTemp_Min) || \
		   (isnan(Rprts_diag_var.RunTime[i])))
		{
			//Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
			//strcpy(error_data_log.Error_code, "MVT Warning");
			
			/*sprintf(error_data_log.Error_desc,"%s\t %s\t\t %11.3f\t %11.3f\t %11.3f\t %11.3f\t",\
			"All Totals",temp_buf,Rprts_diag_var.RunTime[i],*(float*)temp2,*(float*)temp1,\
			*(float*)Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_Def);*/
			sprintf(error_data_log.Error_desc,Historical_AvgRate_RunTime_format_string,\
			        '"',Strings[Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_String_Enum].Text,i+1,'"',Rprts_diag_var.RunTime[i],\
			        fTemp_Min,fTemp_Max,Elemt_def);
			
			log_data_populate(error_log);
		//	Elemt_def = (*(float*)(Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_Def));
		//	Get_Default_Value(Variable_ID, (float*)&Elemt_def);		//Suvrat
			Rprts_diag_var.RunTime[i] = Elemt_def;
			//Rprts_diag_var.RunTime[i] = *(float*)Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_Def;
			Historical_Totals_set_to_default_flag = 1;
		 }
		 /*else //only for testing remove it 
		 {
			 //Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			strcpy(error_data_log.Error_code, "MVT Warning");
			sprintf(error_data_log.Error_desc,"Variable %c%s for Hist Total %d%c is outside valid range(%.3f to %.3f). Value is reset to default value=%.3lf",\
			        '"',Strings[Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_String_Enum].Text,i+1,'"',\
			        *(float*)temp2,*(float*)temp1,*(float*)Elements_Record_Totals_Setup[HIST_RUN_TIME_REC_NUM].Element_Def);
			log_data_populate(error_log);
     }*/
	 }		 
}

//Weight_Unit_enum_list
void VerifyHistoricalWtUnits(void)
{
	unsigned int i,j; 
	//unsigned int *temp1,*temp2,*temp3; 
	unsigned int temp_unit;
	//char temp_buf[60];
	U16 Variable_ID = 0;
	unsigned int Elemt_def;
	
	unsigned int uiTemp_Max,uiTemp_Min,Temp_Def;
	
	Variable_ID = Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_ID;
	
	uiTemp_Max = *(unsigned int*)Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_Max;
	uiTemp_Min = *(unsigned int*)Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_Min;	
	Temp_Def = *(unsigned int*)Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_Def;	
	
	Elemt_def = (*(&Weight_unit_enum_list[Temp_Def]));
	Get_Default_Value(Variable_ID, (unsigned int*)&Elemt_def, (unsigned int*)&uiTemp_Min, (unsigned int*)&uiTemp_Max,0);
	
	//Rprts_diag_var.LastCleared_Weight_unit[i]
	/*if(*temp1 >= WEIGHT_UNIT_TONS && *temp2 >= WEIGHT_UNIT_TONS && *temp1 <= WEIGHT_UNIT_KG && *temp2 <= WEIGHT_UNIT_KG \
		&& *temp3 >= WEIGHT_UNIT_TONS && *temp3 <= WEIGHT_UNIT_KG)*/
	if(uiTemp_Max <= WEIGHT_UNIT_KG && uiTemp_Max <= WEIGHT_UNIT_KG && Temp_Def <= WEIGHT_UNIT_KG)
	{	
		for(i=0; i < NO_OF_HIST_TOTALS; i++)
		{
		/*	memset(temp_buf, 0x20, sizeof(temp_buf)); //fill with space 
			if(sizeof(Strings[Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_String_Enum].Text) > sizeof(temp_buf))
			strncpy(temp_buf,Strings[Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_String_Enum].Text,sizeof(temp_buf));
			else 		
			sprintf(temp_buf,"%s for Hist Total %d",Strings[Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_String_Enum].Text,i+1);
		*/
			//temp_unit = WEIGHT_UNIT_TONS;
			temp_unit= 0xFFFF;
  		for(j=0;j < NO_OF_TOTAL_WT_UNITS;  j++)
			{
				if(Weight_unit_enum_list[j] == Rprts_diag_var.LastCleared_Weight_unit[i])
				{
          temp_unit = j; 
        }	 
      }	
			
		/*if(temp_unit >= *(unsigned int*)temp2 && temp_unit <= *(unsigned int*)temp1)  //no error 
			{ 
				//only for testing remove it 
			 //Open System_Log.txt in append mode & log error
				memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
				memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
				strcpy(error_data_log.Error_code, "MVT Warning");
				sprintf(error_data_log.Error_desc,"Variable %c%s for Hist Total %d%c is outside valid range(%s to %s). Value is reset to default value=%s",\
			          '"',Strings[Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_String_Enum].Text, i+1,'"',\
			          Strings[Weight_Unit_enum_list[*temp2]].Text,\
                Strings[Weight_Unit_enum_list[*temp1]].Text,\
				        Strings[Weight_Unit_enum_list[*temp3]].Text);				
				
				log_data_populate(error_log);
			} 
			else*/
      if(temp_unit != uiTemp_Min && temp_unit > uiTemp_Max) 			//error 
			{
				//Open System_Log.txt in append mode & log error
				memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
				memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
				//strcpy(error_data_log.Error_code, "MVT Warning");
				strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
				
				/*sprintf(error_data_log.Error_desc,"%s\t %s\t\t %s\t %s\t %s\t %s\t",\
				"All Totals",temp_buf,Strings[Weight_unit_enum_list[temp_unit]].Text,\
				Strings[Weight_unit_enum_list[*temp2]].Text,\
				Strings[Weight_unit_enum_list[*temp1]].Text,\
				Strings[Weight_unit_enum_list[*temp3]].Text);*/
				sprintf(error_data_log.Error_desc,Historical_Wt_RateTime_Unit_format_string,\
			          '"',Strings[Elements_Record_Totals_Setup[HIST_TOTAL_UNIT_REC_NUM].Element_String_Enum].Text, i+1,'"',\
			          Strings[Rprts_diag_var.LastCleared_Weight_unit[i]].Text,\
				        Strings[Weight_unit_enum_list[uiTemp_Min]].Text,\
                Strings[Weight_unit_enum_list[uiTemp_Max]].Text,\
				        Strings[Weight_unit_enum_list[Temp_Def]].Text);					
				log_data_populate(error_log);
				//Elemt_def = (*(&Weight_unit_enum_list[*temp3]));
				//Get_Default_Value(Variable_ID, (unsigned int *)&Elemt_def);		//Suvrat
				Rprts_diag_var.LastCleared_Weight_unit[i] = Elemt_def;
				
				//Rprts_diag_var.LastCleared_Weight_unit[i] = Weight_unit_enum_list[*temp3];
				Historical_Totals_set_to_default_flag = 1;
			}
	 }
	}	 
}

void VerifyHistoricalRateTimeUnit(void)
{
	unsigned int i,j; 
	//unsigned int *temp1,*temp2,*temp3; 
	unsigned int temp_unit;
	U16 Variable_ID = 0;
	unsigned int Elemt_def;
	
	unsigned int uiTemp_Max,uiTemp_Min,Temp_Def;
	
	Variable_ID = Elements_Record_Totals_Setup[HIST_RATE_TIME_UNIT_REC_NUM].Element_ID;
	
	uiTemp_Max = *(unsigned int*)Elements_Record_Totals_Setup[HIST_RATE_TIME_UNIT_REC_NUM].Element_Max;
	uiTemp_Min = *(unsigned int*)Elements_Record_Totals_Setup[HIST_RATE_TIME_UNIT_REC_NUM].Element_Min;	
	Temp_Def = *(unsigned int*)Elements_Record_Totals_Setup[HIST_RATE_TIME_UNIT_REC_NUM].Element_Def;
	
	Elemt_def = (*(&Rate_time_unit_enum_list[Temp_Def]));
	Get_Default_Value(Variable_ID, (unsigned int*)&Elemt_def, (unsigned int*)&uiTemp_Min, (unsigned int*)&uiTemp_Max,0);
	
	//Rprts_diag_var.LastCleared_Rate_time_unit[i]
	/*if(*temp1 >= RATE_UNIT_HOURS && *temp2 >= RATE_UNIT_HOURS && *temp1 <= RATE_UNIT_MIN && *temp2 <= RATE_UNIT_MIN \
		&& *temp3 >= RATE_UNIT_HOURS && *temp3 <= RATE_UNIT_MIN)*/
	if(uiTemp_Max <= RATE_UNIT_MIN && uiTemp_Min <= RATE_UNIT_MIN && Temp_Def <= RATE_UNIT_MIN)
	{	
		for(i=0; i < NO_OF_HIST_TOTALS; i++)
		{
			//temp_unit = RATE_UNIT_HOURS;
			temp_unit= 0xFFFF;
  		for(j=0;j < NO_OF_RATE_TIME_UNITS;  j++)
			{
				if(Rate_time_unit_enum_list[j] == Rprts_diag_var.LastCleared_Rate_time_unit[i])
				{
          temp_unit = j; 
        }	 
      }	
			
			/*
			if(temp_unit >= *(unsigned int*)temp2 && temp_unit <= *(unsigned int*)temp1)  //no error 
			{ 
				//only for testing remove it 
			 //Open System_Log.txt in append mode & log error
				memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
				memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
				strcpy(error_data_log.Error_code, "MVT Warning");
				sprintf(error_data_log.Error_desc,"Variable %c%s for Hist Total %d%c is outside valid range(%s to %s). Value is reset to default value=%s",\
			          '"',Strings[Elements_Record_Totals_Setup[HIST_RATE_TIME_UNIT_REC_NUM].Element_String_Enum].Text, i+1,'"',\
			          Strings[Rate_time_unit_enum_list[*temp2]].Text,\
                Strings[Rate_time_unit_enum_list[*temp1]].Text,\
				        Strings[Rate_time_unit_enum_list[*temp3]].Text);				
				log_data_populate(error_log);
			} 
			else*/    
			if(temp_unit != uiTemp_Min && temp_unit > uiTemp_Max) //error 
			{
				//Open System_Log.txt in append mode & log error
				memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
				memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
				//strcpy(error_data_log.Error_code, "MVT Warning");
				strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
				
				sprintf(error_data_log.Error_desc,Historical_Wt_RateTime_Unit_format_string,\
			          '"',Strings[Elements_Record_Totals_Setup[HIST_RATE_TIME_UNIT_REC_NUM].Element_String_Enum].Text, i+1,'"',\
				        Strings[Rprts_diag_var.LastCleared_Rate_time_unit[i]].Text,  
			          Strings[Rate_time_unit_enum_list[uiTemp_Min]].Text,\
                Strings[Rate_time_unit_enum_list[uiTemp_Max]].Text,\
				        Strings[Rate_time_unit_enum_list[Temp_Def]].Text);					
				log_data_populate(error_log);
				//Elemt_def = (*(&Rate_time_unit_enum_list[*temp3]));
				//Get_Default_Value(Variable_ID, (unsigned int *)&Elemt_def);		//Suvrat
				Rprts_diag_var.LastCleared_Rate_time_unit[i] = Elemt_def;
				//Rprts_diag_var.LastCleared_Rate_time_unit[i] = Rate_time_unit_enum_list[*temp3];
				Historical_Totals_set_to_default_flag = 1;
			}
	  }
	}	 
}



void VerifyHistorical_Clear_DateTime(void)
{
	//unsigned short *temp1,*temp2; 
	unsigned short usTemp_Max,usTemp_Min; 
	//unsigned char *uchtemp1,*uchtemp2; 
	unsigned char uchTemp_Max,uchTemp_Min; 
	//unsigned int *uitemp1,*uitemp2, *uitemp3; 
	
	unsigned short Elemt_def;
  unsigned char uchElemt_def;
	//unsigned int uiElemt_def;

	unsigned int i,Inital_Start_Index = 0; 
	//unsigned int temp_unit = 0xFFFF;
	U16 Variable_ID = 0, Variable_Index = 0;
	
	//Check for Year
	usTemp_Max = *(unsigned short*)Elements_Record_Totals_Setup[HIST_CLR_DATE_YEAR_REC_NUM].Element_Max;
	usTemp_Min = *(unsigned short*)Elements_Record_Totals_Setup[HIST_CLR_DATE_YEAR_REC_NUM].Element_Min;	
	Elemt_def = (*(unsigned short*)(Elements_Record_Totals_Setup[HIST_CLR_DATE_YEAR_REC_NUM].Element_Def));
	
	Get_Default_Value(Variable_ID, (unsigned short*)&Elemt_def, (unsigned short*)&usTemp_Min, (unsigned short*)&usTemp_Max,0);
	
	Inital_Start_Index = 0;
	//check for all clr wt year
	for(i=0; i < NO_OF_HIST_TOTALS; i++)
	{
		if((Rprts_diag_var.Clear_Date[i].Year > usTemp_Max) || \
			 (Rprts_diag_var.Clear_Date[i].Year < usTemp_Min) || \
			 (isnan(Rprts_diag_var.Clear_Date[i].Year)))
		{
			//Open System_Log.txt in append mode & log error
			memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
			memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
			//strcpy(error_data_log.Error_code, "MVT Warning");
			strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
			
			sprintf(error_data_log.Error_desc,Historical_Clear_DateTime_format_string,\
							'"',Strings[Elements_Record_Totals_Setup[HIST_CLR_DATE_YEAR_REC_NUM].Element_String_Enum].Text,i+1,'"',\
			        Rprts_diag_var.Clear_Date[i].Year,\
			        usTemp_Min,usTemp_Max,Elemt_def);
			
			log_data_populate(error_log);
		//	Elemt_def = (*(unsigned short*)(Elements_Record_Totals_Setup[HIST_CLR_DATE_YEAR_REC_NUM].Element_Def));
		//	Get_Default_Value(Variable_ID, (unsigned short*)&Elemt_def);		//Suvrat
			Rprts_diag_var.Clear_Date[i].Year = Elemt_def;
			//Rprts_diag_var.Avg_rate_for_mode[i] = *(float*)Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Def;
			Historical_Totals_set_to_default_flag = 1;
		}
	} 
	
	
	//check for all clr wt date and time 
	for(Variable_Index = HIST_CLR_DATE_MON_REC_NUM; Variable_Index <= HIST_CLR_AMPM_REC_NUM ;Variable_Index++)
	{
		uchTemp_Max = *(unsigned char *)Elements_Record_Totals_Setup[Variable_Index].Element_Max;
		uchTemp_Min = *(unsigned char *)Elements_Record_Totals_Setup[Variable_Index].Element_Min;	
		uchElemt_def = (*(unsigned char*)(Elements_Record_Totals_Setup[Variable_Index].Element_Def));
		Get_Default_Value(Variable_ID, (unsigned char*)&Elemt_def, (unsigned char*)&uchTemp_Min, (unsigned char*)&uchTemp_Max,0);
		
		Variable_ID = Elements_Record_Totals_Setup[Variable_Index].Element_ID;
		 
		for(i = 0; i < NO_OF_HIST_TOTALS; i++)
		{
			if((*(unsigned char *)Hist_Clr_Wt_date_time[Inital_Start_Index]   > uchTemp_Max) || \
				 (*(unsigned char *)Hist_Clr_Wt_date_time[Inital_Start_Index] <  uchTemp_Min) || \
				 (isnan(*(unsigned char *)Hist_Clr_Wt_date_time[Inital_Start_Index])))
			{
				//Open System_Log.txt in append mode & log error
				memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
				memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));
				//strcpy(error_data_log.Error_code, "MVT Warning");
				strcpy(error_data_log.Error_code, Strings[Warning_Msg1].Text);
				
				sprintf(error_data_log.Error_desc,Historical_Clear_DateTime_format_string,\
								'"',Strings[Elements_Record_Totals_Setup[Variable_Index].Element_String_Enum].Text,i+1,'"',\
				        *(unsigned char *)Hist_Clr_Wt_date_time[Inital_Start_Index],\
								uchTemp_Min,uchTemp_Max,uchElemt_def);
				
				log_data_populate(error_log);
			//	uchElemt_def = (*(unsigned char*)(Elements_Record_Totals_Setup[Variable_Index].Element_Def));
			//	Get_Default_Value(Variable_ID, (unsigned char*)&uchElemt_def);		//Suvrat
				*(unsigned char *)Hist_Clr_Wt_date_time[Inital_Start_Index] = uchElemt_def;
				//Rprts_diag_var.Avg_rate_for_mode[Inital_Start_Index] = *(float*)Elements_Record_Totals_Setup[HIST_AVG_RATE_REC_NUM].Element_Def;
				Historical_Totals_set_to_default_flag = 1;
			}//if end
			
		  Inital_Start_Index = Inital_Start_Index + 1;	
			
		} //for i end
	}//for Variable_Index end	
}//function end	
	
#endif /*#ifdef MVT_TABLE*/
/*****************************************************************************
* End of file
*****************************************************************************/


