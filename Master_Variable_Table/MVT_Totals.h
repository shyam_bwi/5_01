/******************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : MVT_Totals.h
* @brief			         : Controller Board
*
* @author			         : Oaces Team
*
* @date Created		     : January Monday, 2016  <Jan 25, 2016>
* @date Last Modified	 : January Monday, 2016  <Jan 25, 2016>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __MVT_TOTALS_H
#define __MVT_TOTALS_H

/*============================================================================
* Include Header Files
*===========================================================================*/

#ifdef MVT_TABLE
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
//#include "MVT.h"

/*============================================================================
* Public Data Types
*===========================================================================*/



/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */


/* Boolean variables section */

/* Character variables section */
extern unsigned char  Totals_set_to_default_flag;
extern unsigned char  Report_Log_set_to_default_flag;
extern unsigned char Historical_Totals_set_to_default_flag;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
//extern DATA_VARIABLE_STRUCT Report_Log_Time_Def[];
/*============================================================================
* Public Function Declarations
*===========================================================================*/
//extern void VerifyTotalWeight(void);
//extern void VerifyDailyTotal(void); 
/*
extern void VerifyAllTotals(void);
extern void VerifyHistoricalTotals(void);
extern void VerifyHistoricalAvgRate(void); 
extern void VerifyHistoricalRunTime(void);
extern void VerifyHistoricalWtUnits(void);
extern void VerifyHistoricalRateTimeUnit(void);
*/

extern void Verify_Totals(void);
extern void Verify_Historical_Totals(void);

#endif /*#ifdef MVT_TABLE*/

#endif /*__MVT_TOTALS_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
