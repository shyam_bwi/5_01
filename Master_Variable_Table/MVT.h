/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : MVT.h
* @brief			         : Controller Board
*
* @author			         : Oaces Team
*
* @date Created		     : January Monday, 2016  <Jan 25, 2016>
* @date Last Modified	 : January Monday, 2016  <Jan 25, 2016>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __MVT_H
#define __MVT_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "MVT_Totals.h"

#ifdef MVT_TABLE
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define NO_OF_RUN_MODE							6
#define NO_OF_LC_SIZES							10
#define NO_OF_DISTANCE_UINTS				2
#define NO_OF_TOTAL_WT_UNITS 				5
#define NO_OF_RATE_TIME_UNITS 			2
#define NO_OF_CUSTOM_LC_UNITS 			2
#define NO_OF_IDLER_DISTANCE_UNITS	2
#define NO_OF_WHEEL_DIAMETER_UNITS	2
#define NO_OF_BELT_LENGTH_UNITS 		2
#define NO_OF_SPEED_UNITS 					2
#define NO_OF_ANGLESENSOR_INSTALLED	2
#define NO_OF_IDLERS				 				4
#define NO_OF_DECIMAL_DIGITS 				4
#define NO_OF_IO_BOARD_INSTALLED		2
#define NO_OF_LANGUAGES							2


#define OPTION_MASK									(U16)0x8000 

#define START_OF_SCALE_SETUP_ID 			AngleSensor_Install_status_Id
#define END_OF_SCALE_SETUP_ID 				User_Belt_Speeed_WD_Id

#define START_OF_CALIBRATION_ID 			Belt_scale_unit_Id
#define END_OF_CALIBRATION_ID 				real_time_rate_Id

#define START_OF_ADMIN_ID 						Config_Date_Year_Id
#define END_OF_ADMIN_ID 							Test_speed_Id

#define START_OF_SETUP_DEVICE_ID 			Analog_Output_1_Setpoint_Id
#define END_OF_SETUP_DEVICE_ID 				Phone_Id

//Suvrat Parameters
#define NO_OF_AM_PM									2
#define NO_OF_LOCK_STATUS						2
#define NO_OF_TIME_FORMAT						2
#define NO_OF_DATE_FORMAT						2
#define NO_OF_DAY_OF_WEEK						7
#define NO_OF_DHCP_STATUS						2
#define NO_OF_ZERO_RATE_STATUS			2
#define NO_OF_ON_OFF_STATUS					2
#define NO_OF_AO_FUNCTION						3
#define NO_OF_DIN_FUNCTION					8
#define NO_OF_DO_FUNCTION						8
#define NO_OF_ERR_ALM								5
#define NO_OF_PID_ACTION						2
#define NO_OF_PID_SETPT_TYPE				3
#define NO_OF_FLOW_CONTROL					3
#define NO_OF_PRINTER_STATUS				2
#define NO_OF_PRINT_SB_TYPE					2
#define NO_OF_RATE_OP_TYPE					2
#define NO_OF_RLY_OP_FUNC						6
#define NO_OF_SPEED_OP_TYPE					2
#define NO_OF_SB_DISP_DATA					3
#define NO_OF_UNITS_PER_PULSE				5
#define NO_OF_BAUD_RATE							(U16)(4 | OPTION_MASK)
#define NO_OF_DATA_BITS							(U16)(2 | OPTION_MASK)
#define NO_OF_STOP_BITS							(U16)(2 | OPTION_MASK) 

#define DATE_YR_MIN			(U16)1980
#define DATE_YR_MAX			(U16)2107
#define DATE_YR_DEF			(U16)2015

#define DATE_MON_MIN		(U8)1
#define DATE_MON_MAX		(U8)12
#define DATE_MON_DEF		(U8)1

#define DATE_DATE_MIN		(U8)1
#define DATE_DATE_MAX		(U8)31
#define DATE_DATE_DEF		(U8)1

#define TIME_HR_MIN			(U16)0
#define TIME_HR_MAX			(U16)23
#define TIME_HR_DEF			(U16)0

#define TIME_AM_PM_MIN		AM_TIME
#define TIME_AM_PM_MAX		PM_TIME
#define TIME_AM_PM_DEF		AM_TIME

#define TIME_MINT_MIN		(U8)0
#define TIME_MINT_MAX		(U8)59
#define TIME_MINT_DEF		(U8)0

#define TIME_SEC_MIN		(U8)0
#define TIME_SEC_MAX		(U8)59
#define TIME_SEC_DEF		(U8)0

#define IP_ADDR_BYTE_MIN (U8)0x00
#define IP_ADDR_BYTE_MAX (U8)0xFF
#define IP_ADDR_BYTE_DEF (U8)0x00

#define TOTAL_SCALE_RUN_DOWN_TIME_MIN (U32)00 //This time is in Min
#define TOTAL_SCALE_RUN_DOWN_TIME_MAX (U32)2500 //1440/60 = 24 hrs increased to 2500 
#define TOTAL_SCALE_RUN_DOWN_TIME_DEF (U32)00

/*
#define Conveyor_angle_Min_Val  (float)0.000
#define Conveyor_angle_Max_Val (float)90.000
#define Conveyor_angle_Default_Val (float)12.000
*/
/*
#define REPORT_TIME_HR_DEF (U8)Current_time.RTC_Hour
#define REPORT_TIME_MINT_DEF (U8)Current_time._RTC_Min
#define REPORT_TIME_SEC_DEF (U8)Current_time.RTC_Sec
*/

#define REPORT_LOG_TIME_SEC_1_MAX (U8)58
#define REPORT_LOG_TIME_SEC_1_MIN (U8)01

#define START_OF_REPORT_LOG_ID daily_rprt_Start_time_Hr_Id 
#define END_OF_REPORT_LOG_ID   Calculation_struct_run_time_Id


/*============================================================================
* Public Data Types
*===========================================================================*/
typedef enum
{
    None = 0,
	  Scale_Setup_Section,
	  Device_Setup_Section,
	  Admin_Section,
	  Calibration_Section,
	  Totals_Section,
	  Hist_Clear_Section,
	  Rprt_Log_Section,
	  CRC_Section,
}ELEMENT_SECTION;	

typedef enum
{
		AngleSensor_Install_status_Id = 0,
		Distance_unit_Id,
		User_Belt_Speed_Id,
		Weight_unit_Id,
		Idler_unit_Id,
		Wheel_diameter_unit_Id,
		Belt_length_unit_Id,
		Speed_unit_Id,
		Conveyor_angle_Id,
		Idler_distance_Id,
		Wheel_diameter_Id,
		Pulses_Per_Revolution_Id,
		Run_mode_Id,
		Load_cell_size_Id,
		Rate_time_unit_Id,
		Custom_LC_unit_Id,
		//AngleSensor_Install_status_Id,
		Number_of_idlers_Id,
		Custom_LC_capacity_Id,
		Custom_LC_output_Id,
		Decimal_digits_Id,   
		IO_board_install_Id, 
		Idler_distanceA_Id,
		Idler_distanceB_Id,
		Idler_distanceC_Id,
		Idler_distanceD_Id,
		Idler_distanceE_Id,
		User_Belt_Speeed_WD_Id,		//26
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		//Calibration section
    Belt_scale_unit_Id,				//27
    Cert_scale_unit_Id,
    Test_zero_weight_unit_Id,
		Test_weight_Id,
    Belt_scale_weight_Id,
    Cert_scale_weight_Id,
    Old_belt_length_Id,
    New_belt_length_Id,
    Belt_length_diff_Id,
    Old_zero_value_Id,
    New_zero_value_Id,
    Zero_diff_Id,
    old_span_value_Id,
    new_span_value_Id,
    span_diff_Id,
    real_time_rate_Id,			//42
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		//Admin Section
		//DATE_STRUCT Config_Date;
		Config_Date_Year_Id,       //43                       // Year     [1980..2107]             
		Config_Date_Mon_Id,                               // Month    [1..12]                  
		Config_Date_Day_Id, 
		Config_Time_Hr_Id,                                // Hours    [0..23]
		Config_Time_AM_PM_Id,														 // Added on 20-Oct-2015 //Dummy Data	This is essential for packing structure
		Config_Time_Min_Id,                               // Minutes  [0..59] 
		Config_Time_Sec_Id, 
		IP_Addr_Addr1_Id,
		//IP_Addr_Addr2_Id,
		//IP_Addr_Addr3_Id,
		//IP_Addr_Addr4_Id,
		Subnet_Mask_Addr1_Id,
		//Subnet_Mask_Addr2_Id,
		//Subnet_Mask_Addr3_Id,
		//Subnet_Mask_Addr4_Id,
		Gateway_Addr1_Id,
		//Gateway_Addr2_Id,
		//Gateway_Addr3_Id,
		//Gateway_Addr4_Id,
		Auto_zero_tolerance_Id,
		Negative_rate_limit_Id,
		Negative_rate_time_Id,
		Zero_rate_limit_Id,
		Int_firmware_version_Id,
		Int_update_version_Id,
		Scale_Name_Id,
		Plant_Name_Id,
		Product_Name_Id,
		Password_Id,
		New_Password_Id,
		Verify_New_Password_Id,
		Int_update_file_name_Id,
		Reload_file_name_Id,
		Backup_file_name_Id,
		Admin_Locked_Id,
		AM_PM_Id,
		Calibration_Locked_Id,
		Clear_Weight_Locked_Id,
		Current_Time_Format_Id,
		Current_Date_Format_Id,
		Day_of_week_Id,
		DHCP_Status_Id,
		Setup_Devices_Locked_Id,
		Setup_Wizard_Locked_Id,
		Zero_Calibration_Locked_Id,
		Zero_rate_status_Id,
		MBS_TCP_slid_Id,
		Test_speed_status_Id,
		Test_load_status_Id,
		Test_load_Id,
		Test_speed_Id,
//		Language_sel_Id,	//94
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		//Setup Device Section
    //------analog output-----
    Analog_Output_1_Setpoint_Id,		//95
    Analog_Output_1_Maxrate_Id,
    Analog_Output_1_ZeroCal_Id,
    Analog_Output_2_Setpoint_Id,
    Analog_Output_2_Maxrate_Id,
    Analog_Output_2_ZeroCal_Id,	//100
		Analog_Input_Maxrate_Id,
    Cutoff_1_Id,
    Cutoff_2_Id,
    Cutoff_3_Id,
    Cutoff_4_Id,
    Cutoff_5_Id,
    Cutoff_6_Id,
    Cutoff_7_Id,
    Cutoff_8_Id,
    Load_weight_1_Id,			//110
    Load_weight_2_Id,
    Load_weight_3_Id,
    Load_weight_4_Id,
    Load_weight_5_Id,
    Load_weight_6_Id,
    Load_weight_7_Id,
    Load_weight_8_Id,
    MinMax_Rate_Setpoint_1_Id,
    MinMax_Rate_Setpoint_2_Id,
    MinMax_Rate_Setpoint_3_Id,	//120
		MinMax_Rate_Setpoint_4_Id,
		MinMax_Rate_Setpoint_5_Id,
		MinMax_Rate_Setpoint_6_Id,
		MinMax_Speed_Setpoint_1_Id,
		MinMax_Speed_Setpoint_2_Id,
		MinMax_Speed_Setpoint_3_Id,
		MinMax_Speed_Setpoint_4_Id,
		MinMax_Speed_Setpoint_5_Id,
		MinMax_Speed_Setpoint_6_Id,
		Min_Belt_Speed_Id,					//130
		Empty_Belt_Speed_Id,
		Feeder_Max_Rate_Id,
		Preload_Distance_Id,
		Target_rate_Id,							//134
		Target_Load_Id,
		Analog_Output_1_Function_Id,
		Analog_Output_2_Function_Id,
		Analog_Output1_Id,
		Analog_Output2_Id,
		Digital_Input_1_Function_Id,
		Digital_Input_2_Function_Id,
		Digital_Input_3_Function_Id,
		Digital_Input_4_Function_Id,
		Digital_Output_1_Function_Id,
		Digital_Output_2_Function_Id,
		Digital_Output_3_Function_Id,
		error_alarm_6_Id,
		error_alarm_5_Id,
		error_alarm_4_Id,
		error_alarm_3_Id,
		error_alarm_2_Id,
		error_alarm_1_Id,
		Log_cal_data_Id,
		Log_run_time_data_Id,
		Log_err_data_Id,
		Log_zero_cal_data_Id,
		Log_clear_wt_data_Id,
		PID_Action_Id,
		PID_Setpoint_Type_Id,
		Printer_flow_control_Id,
		Printer_status_Id,
		Printer_type_Id,
		Rate_Output_Type_1_Id,
		Rate_Output_Type_2_Id,
		Rate_Output_Type_3_Id,
		Rate_Output_Type_4_Id,
		Rate_Output_Type_5_Id,
		Rate_Output_Type_6_Id,
		Relay_Output_1_Function_Id,
		Relay_Output_2_Function_Id,
		Relay_Output_3_Function_Id,
		Speed_Output_Type_1_Id,
		Speed_Output_Type_2_Id,
		Speed_Output_Type_3_Id,
		Speed_Output_Type_4_Id,
		Speed_Output_Type_5_Id,
		Speed_Output_Type_6_Id,
		Scoreboard_display_data_Id,
		Scoreboard_type_Id,
		Scoreboard_TMMode_Id,
		Units_Per_Pulse_1_Id,
		Units_Per_Pulse_2_Id,
		Units_Per_Pulse_3_Id,
		Feed_Delay_Id,
		Network_Addr_Id,
		Percent_Ingredient_Id,
		Preload_Delay_Id,
		Periodic_log_interval_Id,
		PID_Channel_Id,
		PID_P_Term_Id,
		PID_I_Term_Id,
		PID_D_Term_Id,
		PID_Local_Setpoint_Id,
		Printer_ticket_num_Id,
		Printer_baud_Id,
		Printer_data_bits_Id,
		Printer_stop_bits_Id,
		Pulse_on_time_1_Id,
		Pulse_on_time_2_Id,
		Pulse_on_time_3_Id,
		Scoreboard_alt_delay_Id,
		Scoreboard_baud_Id,
		Scoreboard_data_bits_Id,
		Scoreboard_stop_bits_Id,
		Addr1_Id,
		Addr2_Id,
		Addr3_Id,
		Company_name_Id,
		Phone_Id,									//208
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		daily_rprt_Start_time_Hr_Id,	//Rpt Log parameters ID
		daily_rprt_Start_time_Min_Id,
		daily_rprt_Start_time_Sec_Id,
		daily_rprt_End_time_Hr_Id,
		daily_rprt_End_time_Min_Id,
		daily_rprt_End_time_Sec_Id,
		daily_rprt_Start_load_time_Hr_Id,
		daily_rprt_Start_load_time_Min_Id,
		daily_rprt_Start_load_time_Sec_Id,
		daily_rprt_End_load_time_Hr_Id,
		daily_rprt_End_load_time_Min_Id,
		daily_rprt_End_load_time_Sec_Id,
		daily_rprt_Down_time_Id,
		daily_rprt_Run_time_Id,
		weekly_rprt_Down_time_Id,
		weekly_rprt_Run_time_Id,
		monthly_rprt_Down_time_Id,
		monthly_rprt_Run_time_Id,
		daily_rprt_freq_rprt_header_Date_Year_Id,
		daily_rprt_freq_rprt_header_Date_Mon_Id,                                               
		daily_rprt_freq_rprt_header_Date_Day_Id,
		daily_rprt_freq_rprt_header_Time_Hr_Id,	
		daily_rprt_freq_rprt_header_Time_Min_Id,
		daily_rprt_freq_rprt_header_Time_Sec_Id,
		weekly_rprt_freq_rprt_header_Date_Year_Id,
		weekly_rprt_freq_rprt_header_Date_Mon_Id,                                               
		weekly_rprt_freq_rprt_header_Date_Day_Id,
		weekly_rprt_freq_rprt_header_Time_Hr_Id,	
		weekly_rprt_freq_rprt_header_Time_Min_Id,
		weekly_rprt_freq_rprt_header_Time_Sec_Id,
		monthly_rprt_freq_rprt_header_Date_Year_Id,
		monthly_rprt_freq_rprt_header_Date_Mon_Id,                                               
		monthly_rprt_freq_rprt_header_Date_Day_Id,
		monthly_rprt_freq_rprt_header_Time_Hr_Id,	
		monthly_rprt_freq_rprt_header_Time_Min_Id,
		monthly_rprt_freq_rprt_header_Time_Sec_Id,
		Calculation_struct_run_time_Id,
		////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////
		//Totals parameters
		Total_weight_accum_Id,
		Total_weight_Id,
		Daily_Total_weight_Id,
		Weekly_Total_weight_Id,
		Monthly_Total_weight_Id,
		Yearly_Total_weight_Id,
		Job_Total_Id,
		Master_total_Id,
		LastClearedTotal_Id,
		Avg_rate_for_mode_Id,
		RunTime_Id,
		LastCleared_Weight_unit_Id,
		LastCleared_Rate_time_unit_Id,
		LastClearedTotal_Date_Year_Id, 
    LastClearedTotal_Date_Mon_Id, 
    LastClearedTotal_Date_Day_Id,
    LastClearedTotal_Time_Hr_Id, 
    LastClearedTotal_Time_Min_Id, 
    LastClearedTotal_Time_Sec_Id,
		LastClearedTotal_AM_PM_Id,
		
}ELEMENT_ID;	

typedef struct 
{
		U16 Element_Section;
    U16 Element_ID;	    
    U16 Element_String_Enum; 
    U16 Element_Data_Type;
    U16 Element_Length;
    const void *Element_Min;
    const void *Element_Max;
    const void *Element_Def; 
	  U16 Element_Has_Options;
	  void *Element_enum_list;
}ELEMENT_REC;

/*typedef struct 
{
		U8 Element_Section;
    U16 Element_ID;	    
    U16 Element_String_Enum; 
    U8 Element_Data_Type;
    U16 Element_Length;
    const void *Element_Min;
    const void *Element_Max;
    const void *Element_Def; 
	  U8 Element_Has_Options;
	  void *Element_enum_list;
}ELEMENT_REC;
*/

typedef struct
{
	void *DataVar;
}	
DATA_VARIABLE_STRUCT;

extern DATA_VARIABLE_STRUCT Scale_var_Data[];
extern DATA_VARIABLE_STRUCT Report_Log_Time_Def[];

//Run Mode Enum
typedef enum
{
	RUN_MODE_WEIGHT_RATE = 0,
	RUN_MODE_LOAD_OUT,
	RUN_MODE_BLENDING,
	RUN_MODE_LOAD_CONTROL,
	RUN_MODE_RATE_CONTROL,
	RUN_MODE_WEIGHT,
} RUN_MODE_OPTIONS;



//Load cell Size
typedef enum
{
	LC_SIZE_45KG = 0,
	LC_SIZE_50KG,
  LC_SIZE_100KG,
	LC_SIZE_150KG,
  LC_SIZE_200KG,
	LC_SIZE_350KG,
  LC_SIZE_500KG,
	LC_SIZE_1000KG,
  LC_SIZE_1000LBS,
	LC_SIZE_CUSTOM,
} LC_SIZE_OPTIONS;



//Distance_unit enum
typedef enum
{
	DISTANCE_UNIT_ENGLISH = 0,
  DISTANCE_UNIT_METRIC,
} DISTANCE_UNIT_OPTIONS;

typedef enum
{
	ENGLISH = 0,
  POLISH,
} LANGUAGE_SELECT_OPTIONS;

    
//Weight Unit Enums
typedef enum
{
	WEIGHT_UNIT_TONS = 0,
	WEIGHT_UNIT_LONG_TON,
	WEIGHT_UNIT_LBS,
	WEIGHT_UNIT_TONNE,
	WEIGHT_UNIT_KG,
} WEIGHT_UNIT_OPTIONS;



//Rate Unit Enums
typedef enum
{
	RATE_UNIT_HOURS = 0,
	RATE_UNIT_MIN,
} RATE_UNIT_OPTIONS;



//Custom LC Unit Enums
typedef enum
{
	CUSTOM_LC_UNIT_LBS = 0,
	CUSTOM_LC_UNIT_KG, 
} CUSTOM_LC_UNIT_OPTIONS;


//Idler Distance Unit Enums
typedef enum
{
	IDLER_DISTANCE_UNIT_INCHES = 0,
	IDLER_DISTANCE_UNIT_METER,
}IDLER_DISTANCE_UNIT_OPTIONS;



//Idler Distance Unit Enums
typedef enum
{
	WHEEL_DIAMETER_INCHES = 0,
	WHEEL_DIAMETER_CNTIMETERS,
}WHEEL_DIAMETER_UNIT_OPTIONS;




//Belt_length_unit Enums
typedef enum
{
	BELT_LENGTH_UNIT_FEET = 0,
	BELT_LENGTH_UNIT_METER,
}BELT_LENGTH_UNIT_OPTIONS;



//Speed_unit Enums
typedef enum
{
	SPEED_UNIT_FEET_PER_MIN = 0,
	SPEED_UNIT_METER_PER_MIN,
}SPEED_UNIT_OPTIONS;

//AngleSensor_Install_status Enums
typedef enum
{
	ANGLE_SENSOR_INSTALLED = 0,
	ANGLE_SENSOR_NOT_INSTALLED,
}ANGLE_SENSOR_INSTALL_OPTIONS;

//Number_of_idlers Enums
typedef enum
{
	NUMBER_OF_IDLERS_1 = 0,
  NUMBER_OF_IDLERS_2,
  NUMBER_OF_IDLERS_3,
  NUMBER_OF_IDLERS_4,
}NUMBER_OF_IDLERS_OPTIONS;

//Decimal digits Enums
typedef enum
{
	DECIMAL_DIGITS_NONE = 0,
	DECIMAL_DIGITS_ONE,
	DECIMAL_DIGITS_TWO,
	DECIMAL_DIGITS_THREE,
}DECIMAL_DIGITS_OPTIONS;


//IO_board_install Enums
typedef enum
{
	IO_BOARD_INSTALLED = 0,
	IO_BOARD_NOT_INSTALL,
}IO_BOARD_INSTALL_OPTIONS;


//AM_PM format
typedef enum
{
	AM_TIME = 0,
  PM_TIME,
} AM_PM_OPTIONS;

//Lock status
typedef enum
{
	LOCK = 0,
  UNLOCK,
} LOCK_STATUS;

//Time Format
typedef enum
{
	H12_HR = 0,
  H24_HR,
} TIME_FORMAT;

//Date Format
typedef enum
{
	FMT_MMDDYYYY = 0,
  FMT_DDMMYYYY,
} DATE_FORMAT;

//Week of the day
typedef enum
{
  DOW_SUNDAY = 0,                                   
  DOW_MONDAY,                                  
  DOW_TUESDAY,                                 
  DOW_WEDNESDAY,                               
  DOW_THURSDAY,                                
  DOW_FRIDAY,                                  
  DOW_SATURDAY,                                
}DAY_OF_WEEK;

//DHCP Status
typedef enum
{
	DHCP_DISABLE = 0,
	DHCP_ENABLE,
} DHCP_STATUS;



//Zero Rate Limit
typedef enum
{
	ZERO_RATE_ON = 0,
	ZERO_RATE_OFF,
} ZERO_RATE_STATUS;



//Zero Rate Limit
typedef enum
{
	STATUS_ON = 0,
	STATUS_OFF,
} ON_OFF_STATUS;


//AO Function
typedef enum
{
	UNASSIGNED_AO = 0,
	MA_0_20,
	MA_4_20,
} AO_FUNCTION;



//Ain Function
typedef enum
{
	UNASSIGNED_DIN = 0,
	CLR_WT,
	PRINT_TKT,
	PRNT_THN_CLR,
	ENTR_LOAD,
	PID_RATE_ZERO,
	ZERO_CALIB_DIN,
	ERR_ACK,
} DIN_FUNCTION;



//Ain Function
typedef enum
{
	UNASSIGNED_DO = 0,
	PULSED_OUT,
	QUAD_WAVE,
	ERR_ALM,
	MIN_MAX_SPD,
	MIN_MAX_RATE,
	BATCH_LOADOUT,
	ZERO_CALIB_DO,
} DO_FUNC;



//Error Alarm
typedef enum
{
	LOAD_CELL = 0,
	ANGLE_SENSOR,
	COMMUNICATIONS,
	NEG_RATE,
	ANY_ERR,
} ERROR_ALARM;



//PID Action
typedef enum
{
	REVERSE = 0,
	FORWARD,
} PID_ACTION;



//PID Setpoint Type
typedef enum
{
	LOCAL_TARGET = 0,
	IP_TRGT_4_20_MA,
	REMOTE_TARGET,
} PID_SETPOINT_TYPE;



//Flow Control
typedef enum
{
	FC_NONE = 0,
	FC_HARDWARE,
	FC_XON_XOFF,
} FLOW_CONTROL;



/*
//Flow Control
typedef enum
{
	NONE = 0,
	HARDWARE,
	XON_XOFF,
} FLOW_CONTROL;

const unsigned int Flow_Control_enum_list[] =
{
	Screen412214_str2, // "None"
	Screen412214_str4, // "Hardware"
	Screen412214_str7, // "Xon/Xoff"
};
*/

//Printer Status
typedef enum
{
	TICKETS_ON = 0,
	TICKETS_OFF,
} PRINTER_STATUS;



//Printer TYPE
typedef enum
{
	PSBTYPE_BELT_WAY = 0,
	PSBTYPE_CUSTOM,
} PRINT_SB_TYPE;



//Rate Output Type
typedef enum
{
	MIN_RATE_OP = 0,
	MAX_RATE_OP,
} RATE_OP_TYPE;



//Rate Output Type
typedef enum
{
	ROF_UNASSIGNED = 0,
	ROF_ERR_ALM,
	ROF_MIN_MAX_SPD,
	ROF_MIN_MAX_RATE,
	ROF_BATCH_LOADOUT,
	ROF_ZERO_CALIB,
} RLY_OP_FUNC;



//Speed Output Type
typedef enum
{
	MIN_SPD_OP = 0,
	MAX_SPD_OP,
} SPD_OP_TYPE;



//Scoreboard Display Data
typedef enum
{
	SB_DISP_RATE = 0,
	SB_DISP_WEIGHT,
	SB_DISP_ALT,
} SB_DISP_DATA;



//Units Per Pulse
typedef enum
{
	_0_1_ = 0,
	_1_0_,
	_10_,
	_100_,
	_0_01_,
} UNITS_PER_PULSE;




//Printer/Scoreboard Baud Rate
typedef enum
{
	BAUD_9600 = 0,
	BAUD_19200,
	BAUD_57600,
	BAUD_115200,
} BAUD_RATE;



//Printer/Scoreboard Data Bits
typedef enum
{
	DATA_BITS_8 = 0,
	DATA_BITS_7,
} DATA_BITS;



//Printer/Scoreboard Stop Bits
typedef enum
{
	STOP_BITS_1 = 0,
	STOP_BITS_2,
} STOP_BITS;




extern const ELEMENT_REC Elements_Record_Scale_Setup[];

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */
 extern const float Conveyor_angle_Max;
 
 extern const unsigned int Weight_unit_Min; //9
 extern const unsigned int Weight_unit_Max;
 extern const unsigned int Weight_unit_Def;
 extern const unsigned int Rate_time_unit_Min;//10
 extern const unsigned int Rate_time_unit_Max;
 extern const unsigned int Rate_time_unit_Def;

extern const unsigned int AM_PM_Min;
extern const unsigned int AM_PM_Max ;
extern const unsigned int AM_PM_Def;


 extern const unsigned int AM_PM_enum_list[];
 extern const unsigned int Weight_unit_enum_list[];
 extern const unsigned int Rate_time_unit_enum_list[];
/* Boolean variables section */

/* Character variables section */
extern unsigned char  Param_set_to_default_flag;
extern unsigned char  Scale_setup_param_set_to_default_flag;
extern unsigned char  Calib_param_set_to_default_flag;
extern unsigned char  Admin_param_set_to_default_flag;
extern unsigned char  Setup_device_param_set_to_default_flag;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void Verify_Scale_Setup_Variables(void);
extern void Verify_Calibration_Variables(void);
extern void Verify_Admin_Variables(void);
extern void Verify_Setup_Device_Variables(void);
extern void Verify_Variable(const ELEMENT_REC *MVT_Table);
extern void Verify_All_Scale_Variables(void);
//extern void Get_Default_Value(U16 Variables_ID, void* pdefault_val);
extern void Get_Default_Value(U16 Variables_ID, void* pdefault_val, void* pmin_val, void* pmax_val, void* pcurr_val);

#endif /*#ifdef MVT_TABLE*/

#endif /*__MVT_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
