#include "flash_rw.h"
#include "Screen_global_ex.h"
#define NO_OF_BACKUPS					3
#define BACKUP_INSTANCES  				1
#define SECTOR_SIZE						0x20000
#define VARIABLE_STORAGE_BASE_ADDR		0x300000
#define TOTAL_STORAGE_BASE_ADDR			0x400000
#define VALID_HEADER 			0xAABB

uint8_t u8RecursiveWriteBackUpVar = 0;

uint32_t LatestStructureVersion=0x00;
uint32_t OldestStructureVersion=0xFFFFFFFF;

struct sTotalBackupStruct;
struct sVariableStruct;

typedef struct RecHeader
{
    uint16_t u16structHeader;
    uint16_t u16structFirmwareversion;
    uint16_t u16Size;
    uint32_t u32sVersionNo;
} sRecordHeader;

typedef struct
{
    uint16_t u16structHeader;
    TOTALS_STRUCT totals;
		float Zero_Weight ;
		uint32_t u32prevStructAddress;
		uint16_t u16CRC;
} sTotalBackupStruct;


typedef struct
{
    sRecordHeader sHeader;
    uint64_t u64AccumulatedWeight;
} sVariableStruct;

struct
{
    sRecordHeader sHeader;
    uint64_t u64AccumulatedWeight;

} sVariableStructV1;

enum
{
    enum_totals = 0,
    enum_variables,
    enum_periodic_log,
    enum_system_log

};


uint32_t GetCurrentWriteAddress(unsigned char u8Type);
uint16_t 	CalculateCheckSum(uint8_t *ptr,uint16_t u16Size);

int InsertNewRecord(unsigned char u8Type,uint16_t* Flash_Buf)
{
//		sTotalBackupStruct *sTotalsReadFlash;
	sRecordHeader *psHeader;
    uint32_t u32WriteAddress,u32prevWriteAddress;
		u32prevWriteAddress = GetCurrentWriteAddress(u8Type);						//get the current address available for the respective log type.
		psHeader = 	(	sRecordHeader *)u32prevWriteAddress;																													//get size of the structure
		u32WriteAddress = u32prevWriteAddress + psHeader->u16Size;
	//u32WriteAddress += psHeader->u16Size
																																//add address of previous structure.
																																// calculate CRC to be written at end
		
    switch(u8Type)
    {
    case enum_totals:

        GetCurrentWriteAddress(u8Type);
				//write to the new location
		for (;;)
		
		// 
        break;
    }
    return 0;
}

uint32_t GetCurrentWriteAddress(unsigned char u8Type)
{
    uint32_t u32BaseAddress,max_version=0,min_version=0xFFFFFFFF,temp_version;
    uint32_t u32CurrentAddress,u32OldestAddress,data_valid,size_of_struct =0;
    uint32_t u32CyclicCnt=0,u32CyclicCnt_max,u32NoofBackup =0,u32NoofBackup_max=0;
    uint8_t * readAddress,*readAddressBase,*readAddressTemp;
    sVariableStruct *sptrVariableStruct;
    sRecordHeader *psHeader;
    uint16_t u16CalculatedCheckSum,u16CheckSum;
    switch(u8Type)
    {
    case enum_totals:																																								//TRAVERSING THE ORIGINALS FOR TOTALS
        u32BaseAddress = TOTAL_STORAGE_BASE_ADDR;																										// TOTALS ORIGINALS , SECTOR  STARTS HERE
        readAddress = (	uint8_t *)u32BaseAddress;																										// HEAD POINTER FOR TRAVERSING
        psHeader = (	sRecordHeader *)u32BaseAddress;
        u32CyclicCnt =0;
        u32NoofBackup =0;
        u32CyclicCnt_max =2;
        u32NoofBackup_max =3;
        readAddressBase = (uint8_t *)(TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE);
        break;
    }
    do
    {

        u16CalculatedCheckSum = CalculateCheckSum((uint8_t *)psHeader,psHeader-> u16Size);						//CALCULATE CHECKSUM OF STORED DATA
        u16CheckSum = *(readAddress + psHeader->u16Size-1);																						// READ STORED CHECKSUM
        if((psHeader-> u16structHeader == VALID_HEADER) && u16CheckSum == u16CalculatedCheckSum)			// COMPARE CHECKSUM AND HEADER CONSTANT
        {
            // MATCHES WELL -> ALL IS WELL
            temp_version = psHeader->u32sVersionNo;																										// GET VERSION NUMBER
            if (temp_version > max_version )																													// UPDATE MAX VERSION NUMBER
            {
                max_version = temp_version;
                u32CurrentAddress = (uint32_t)readAddress ;/*+psHeader -> u16Size;	*/											// IF THE RECORD IS LATEST UPDATE CURRENT READ ADDRESS
            }
            if (temp_version < min_version)																														// UPDATE OLDEST VERSION NUMBER
            {
                min_version = temp_version;
                u32OldestAddress = (uint32_t)readAddress ;
            }
            readAddress=readAddress +psHeader -> u16Size;																							// UPDATE STRUCT HEADER POINTER TO THE NEXT LOCATION
            psHeader =(sRecordHeader *)readAddress;																										// READ THE NEXT HEADER
        }
        else
        {
            /*
            either VALID_HEADER isnt found or u16CheckSum is not valid
            1. if valid header is not found  , that means the area ahead might be empty.
            2. if the checksum fails, that means the area ahead is corrupt.
            */
            //check for condition 1 above :
            if (psHeader-> u16structHeader != VALID_HEADER)
            {
                if ((readAddress+ psHeader->u16Size)> (readAddressBase + SECTOR_SIZE) )
                {
                    if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;
                    else break;
                    readAddressBase =(uint8_t *)( TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE);
                    readAddress = readAddressBase;
                }
            }
            else
            {
                /*
                jump to the backup structures check if there is a valid data, get header details from there ,
                get back to the current/original data sector
                */
                for (u32NoofBackup = 0; u32NoofBackup <u32NoofBackup_max; u32NoofBackup++ )
                {
                    readAddressTemp =readAddress + u32NoofBackup * SECTOR_SIZE ;
                    psHeader = (sRecordHeader *) readAddressTemp ;
                    if (psHeader-> u16structHeader == VALID_HEADER)
                    {
											readAddress += psHeader->u16Size;
											break;
                    }
                }
								if (u32NoofBackup >=u32NoofBackup_max)
								{																																								//if nothing is found in the backup sectors , skip to the next cyclic sector.
								 if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;
                    else break;
                    readAddressBase =(uint8_t *)( TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE);
                    readAddress = readAddressBase;
								}
            }
            //not a valid structure!
  /*          if(1)
            {

            }
            else
                // jump to the backup sector 1
            {
                //jump to backup sector 2
                if(1)
                {
                }
                else if(1)
                {
                    // jump to backup sector 3
                    if (1)
                    {
                        //else
                        {
                            //die
                            while(1);
                        }
                    }
                }
            }
						*/
        }
    }
    while(readAddress <(uint8_t *)(u32BaseAddress + (SECTOR_SIZE * (((NO_OF_BACKUPS+1)*(u32CyclicCnt_max))) +1 )));											// CHECK UNTIL END OF SECTOR IS REACHED
    return(u32CurrentAddress);
}


uint32_t GetCurrentWriteAddress_v0(unsigned char u8Type)
{
    uint32_t u32BaseAddress,max_version=0,min_version=0xFFFFFFFF,temp_version;
    uint32_t u32CurrentAddress,u32OldestAddress,data_valid,size_of_struct =0;
    uint32_t u32CyclicCnt=0,u32CyclicCnt_max,u32NoofBackup =0,u32NoofBackup_max=0;
    uint8_t * readAddress,*readAddressBase,*readAddressTemp;
    sVariableStruct *sptrVariableStruct;
    sRecordHeader *psHeader;
    uint16_t u16CalculatedCheckSum,u16CheckSum;
    switch(u8Type)
    {
    case enum_totals:																																								//TRAVERSING THE ORIGINALS FOR TOTALS
        u32BaseAddress = TOTAL_STORAGE_BASE_ADDR;																										// TOTALS ORIGINALS , SECTOR  STARTS HERE
        readAddress = (	uint8_t *)u32BaseAddress;																										// HEAD POINTER FOR TRAVERSING
        psHeader = (	sRecordHeader *)u32BaseAddress;
        u32CyclicCnt =0;
        u32NoofBackup =0;
        u32CyclicCnt_max =2;
        u32NoofBackup_max =3;
        readAddressBase = (uint8_t *)(TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE);
        break;
    }
    do
    {

        u16CalculatedCheckSum = CalculateCheckSum((uint8_t *)psHeader,psHeader-> u16Size);						//CALCULATE CHECKSUM OF STORED DATA
        u16CheckSum = *(readAddress + psHeader->u16Size-1);																						// READ STORED CHECKSUM
        if((psHeader-> u16structHeader == VALID_HEADER) && u16CheckSum == u16CalculatedCheckSum)			// COMPARE CHECKSUM AND HEADER CONSTANT
        {
            // MATCHES WELL -> ALL IS WELL
            temp_version = psHeader->u32sVersionNo;																										// GET VERSION NUMBER
            if (temp_version > max_version )																													// UPDATE MAX VERSION NUMBER
            {
                max_version = temp_version;
                u32CurrentAddress = (uint32_t)readAddress ;/*+psHeader -> u16Size;	*/											// IF THE RECORD IS LATEST UPDATE CURRENT READ ADDRESS
            }
            if (temp_version < min_version)																														// UPDATE OLDEST VERSION NUMBER
            {
                min_version = temp_version;
                u32OldestAddress = (uint32_t)readAddress ;
            }
            readAddress=readAddress +psHeader -> u16Size;																							// UPDATE STRUCT HEADER POINTER TO THE NEXT LOCATION
            psHeader =(sRecordHeader *)readAddress;																										// READ THE NEXT HEADER
        }
        else
        {
            /*
            either VALID_HEADER isnt found or u16CheckSum is not valid
            1. if valid header is not found  , that means the area ahead might be empty.
            2. if the checksum fails, that means the area ahead is corrupt.
            */
            //check for condition 1 above :
            if (psHeader-> u16structHeader != VALID_HEADER)
            {
                if ((readAddress+ psHeader->u16Size)> (readAddressBase + SECTOR_SIZE) )
                {
                    if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;
                    else break;
                    readAddressBase =(uint8_t *)( TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE);
                    readAddress = readAddressBase;
                }
            }
            else
            {
                /*
                jump to the backup structures check if there is a valid data, get header details from there ,
                get back to the current/original data sector
                */
                for (u32NoofBackup = 0; u32NoofBackup <u32NoofBackup_max; u32NoofBackup++ )
                {
                    readAddressTemp =readAddress + u32NoofBackup * SECTOR_SIZE ;
                    psHeader = (sRecordHeader *) readAddressTemp ;
                    if (psHeader-> u16structHeader == VALID_HEADER)
                    {
											readAddress += psHeader->u16Size;
											break;
                    }
                }
								if (u32NoofBackup >=u32NoofBackup_max)
								{																																								//if nothing is found in the backup sectors , skip to the next cyclic sector.
								 if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;
                    else break;
                    readAddressBase =(uint8_t *)( TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE);
                    readAddress = readAddressBase;
								}
            }
            //not a valid structure!
  /*          if(1)
            {

            }
            else
                // jump to the backup sector 1
            {
                //jump to backup sector 2
                if(1)
                {
                }
                else if(1)
                {
                    // jump to backup sector 3
                    if (1)
                    {
                        //else
                        {
                            //die
                            while(1);
                        }
                    }
                }
            }
						*/
        }
    }
    while(readAddress <(uint8_t *)(u32BaseAddress + (SECTOR_SIZE * (((NO_OF_BACKUPS+1)*(u32CyclicCnt_max))) +1 )));											// CHECK UNTIL END OF SECTOR IS REACHED
    return(u32CurrentAddress);
}


// uint16_t 	CalculateCheckSum(uint8_t *ptr,uint16_t u16Size)
// {
//     uint16_t u16CheckSum= 0;
// 	CalculateCRC((uint8_t *)&(ptr),u16Size);
//     return(u16CheckSum);
// }
uint16_t CalculateCheckSum(uint8_t *StartBuffer, uint16_t Length)
{
    /*Optimized CRC-16 calculation.*/

    /*Polynomial: x^16 + x^15 + x^2 + 1 (0xa001)*/
    /*Initial value: 0xffff*/
    /* This CRC is normally used in disk-drive controllers.*/

    uint8_t test_cnt;
    uint16_t CntIndex;
    uint16_t uiCRC_Byte;

    uiCRC_Byte = 0xFFFF;

    for(CntIndex = 0; CntIndex < Length; CntIndex++)
    {
        //EXOR word constant with CRC_test
        uiCRC_Byte = uiCRC_Byte  ^ *StartBuffer;

        // Test for all 16 - bits
        for(test_cnt=16; test_cnt !=0; test_cnt--)
        {
            if( uiCRC_Byte & 0x0001)
            {
                uiCRC_Byte ^= 0xA001;  //CRC_polynomial ; //A001
            }
            uiCRC_Byte = (uiCRC_Byte>>1);
        }

        StartBuffer++;
    }
    return(uiCRC_Byte);
}

