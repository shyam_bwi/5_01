/****************************************************************************
 *   $Id:: wdt.c 5752 2010-12-01 00:01:10Z usb00423                         $
 *   Project: NXP LPC17xx Watchdog Timer example
 *
 *   Description:
 *     This file contains WDT code example which include WDT initialization,
 *     WDT interrupt handler, and APIs for WDT access.
 *
 ****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
****************************************************************************/
//#include "LPC17xx.h"
#include <LPC177x_8x.H>

#ifndef FALSE
#define FALSE   (0)
#endif

#ifndef TRUE
#define TRUE    (1)
#endif
#include "rtos_main.h"
#include "wdt.h"
#include "rtl.h"
volatile uint32_t wdt_counter;

/*****************************************************************************
** Function name:		WDT_IRQHandler
**
** Descriptions:		Watchdog timer interrupt handler
**
** parameters:			None
** Returned value:		None
**
*****************************************************************************/
volatile int prevt=0,currt=0,wdt_T=0;
extern int state,WDT_EN;
void WDT_IRQHandler(void)
{
    //NVIC_DisableIRQ(WDT_IRQn);

//  if (state==0)
// 	 {
// 	 state =1;
// 	 	LPC_GPIO1->DIR |=0x01<<5;
// 		LPC_GPIO1->CLR |=0x01<<5;
// 	 }
// 	 else {
// 	 state =0;
// 	 	LPC_GPIO1->DIR |=0x01<<5;
// 		LPC_GPIO1->SET |=0x01<<5;
// 	 }
    LPC_WDT->MOD &= ~WDTOF;		/* clear the time-out terrupt flag */
//		LPC_WDT->MOD |= WDINT;
    //	LPC_WDT->MOD |= WDEN;


    wdt_counter++;
    //WDTInit();
    return;
}

/*****************************************************************************
** Function name:		WDTInit
**
** Descriptions:		Initialize watchdog timer, install the
**				watchdog timer interrupt handler
**
** parameters:			None
** Returned value:		true or false, return false if the VIC table
**				is full and WDT interrupt handler can be
**				installed.
**
*****************************************************************************/

//sks DISABLED ON 23/06/2017
uint32_t WDTInit( void )
{
    if (WDT_EN)
    {
        wdt_counter = 0;

        NVIC_EnableIRQ(WDT_IRQn);

        LPC_WDT->TC = WDT_FEED_VALUE;	/* once WDEN is set, the WDT will start after feeding */
        LPC_WDT->MOD = WDEN|WDRESET;
//LPC_WDT->WARNINT = 1000;
//  LPC_WDT->MOD = WDEN;

        LPC_WDT->FEED = 0xAA;		/* Feeding sequence */
        LPC_WDT->FEED = 0x55;

        return( TRUE );
    }
    return FALSE;
}

uint32_t Set_WDT_Tout( int tout )
{
//   LPC_WDT->TC = tout;	/* once WDEN is set, the WDT will start after feeding */
//   LPC_WDT->MOD = WDEN|WDRESET;
//  // LPC_WDT->MOD = WDEN;
//   LPC_WDT->FEED = 0xAA;		/* Feeding sequence */
//   LPC_WDT->FEED = 0x55;
//   return( TRUE );
}

uint32_t WDT_Un_Init( void )
{
    if (WDT_EN)
    {
        wdt_counter = 0;

// NVIC_DisableIRQ(WDT_IRQn);

// LPC_WDT->TC = WDT_FEED_VALUE;	/* once WDEN is set, the WDT will start after feeding */
        LPC_WDT->MOD &=~( WDEN|WDRESET);
//LPC_WDT->MOD &=~( WDEN);
        LPC_WDT->FEED = 0xAA;		/* Feeding sequence */
        LPC_WDT->FEED = 0x55;
        return( TRUE );
    }
    return FALSE;
}
/*****************************************************************************
** Function name:		WDTFeed
**
** Descriptions:		Feed watchdog timer to prevent it from timeout
**
** parameters:			None
** Returned value:		None
**
*****************************************************************************/
extern OS_MUT WDT_Feed_Mutex;
extern BOOL OS_Init_Done_Flag;
void WDTFeed( void )
{
//__disable_irq();
    if (WDT_EN)
    {
        if (OS_Init_Done_Flag ==1)
        {
            if (OS_R_OK == os_mut_wait (&WDT_Feed_Mutex,WDT_MUTEX_TIMEOUT))
            {
                LPC_WDT->FEED = 0xAA;		/* Feeding sequence */
                LPC_WDT->FEED = 0x55;
                os_mut_release (&WDT_Feed_Mutex);
            }
        }
        else
        {
            LPC_WDT->FEED = 0xAA;		/* Feeding sequence */
            LPC_WDT->FEED = 0x55;
        }
    }
    return;

}

/******************************************************************************
**                            End Of File
******************************************************************************/
