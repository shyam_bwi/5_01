/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Keypad.h
* @brief			         : Includes all keypad related functionality
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 12, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __SCREENSHOT_BMP_H
#define __SCREENSHOT_BMP_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

#ifdef SCREENSHOT

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern U8 Take_Screen_Shot_Flag ;

/* unsigned integer variables section */
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */


/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void MainTask(void) ;

#endif /*#ifdef SCREENSHOT_BMP_H*/

#endif /*__SCREENSHOT_BMP_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
