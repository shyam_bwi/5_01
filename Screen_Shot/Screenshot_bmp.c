/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Screeshot_bmp.c
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 26, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 26, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/

#include <stdio.h>
#include <RTL.h>
//#include "Global_ex.h"
//#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "GUI.h"
#include "Screenshot_bmp.h"
#include "File_Config.h"
#include "rtc.h"
#include "Screen_global_ex.h"
#include "LCDConf.h"
#include "Screen_data_enum.h"


#ifdef SCREENSHOT 
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define BufferSize 511
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */
FILE *Screen_Shot_fp;
extern FAT_DRV usb0_drv;
/* Boolean variables section */

/* Character variables section */
U8 Take_Screen_Shot_Flag = __FALSE;

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
//static char file_name[FILE_NAME_SIZE];	
__align(8) static U8 _SS_Buff[(480 * 272 * 2)+2000] __attribute__ ((section ("VRAM"), zero_init));
/* unsigned integer variables section */
U32 Written;
static unsigned int bytecnt=0;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
//static void _WriteByte2File(U8 Data, void * p);
static void _ExportToFile(void);

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*********************************************************************
*
*       _WriteByte2File
*
*  This function will be called by GUI_BMP_Serialize to write the
*  bytes to the file
*/

static void _WriteByte2File(U8 Data, void * p) 
{
	  _SS_Buff[bytecnt] = Data;
	 //  fwrite(&Data, sizeof(Data), 1, p);	
		 bytecnt++;
// 		 if(bytecnt % 3000 == 0)
// 		 {
// 			 fclose(Screen_Shot_fp);	
// 			 fopen (file_name, "a");
// 		 }
}


/*********************************************************************
*
*       _ExportToFile
*
*  Demonstrates the use of GUI_BMP_Serialize
*/
static void _ExportToFile(void) 
{	  
	char file_name[FILE_NAME_SIZE];	
	
	 //Opens the File
	sprintf(file_name, "Screen Shots\\%02d%02d%04d_%02d%02d%02d.bmp", Current_time.RTC_Mday,
                                           Current_time.RTC_Mon,	
	                                         Current_time.RTC_Year,   
																					 Current_time.RTC_Hour, 
																					 Current_time.RTC_Min,
	                                         Current_time.RTC_Sec);
	if(Flags_struct.Connection_flags & USB_CON_FLAG)
	{
		unsigned int i=0;
	  Screen_Shot_fp = fopen (file_name, "w");
		GUI_BMP_Serialize(_WriteByte2File, Screen_Shot_fp);
GUI_DispStringAt("                                                                      ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
		GUI_DispStringAt(Strings[saving_scn_shot].Text, WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
		for(i =0 ; i<bytecnt;i++)
		{
			fwrite(&_SS_Buff[i], sizeof(char), 1,Screen_Shot_fp);
		  if(i % 1500 == 0)
		  {
			   fclose(Screen_Shot_fp);	
			   fopen (file_name, "a");
		  }
		}	
		fclose(Screen_Shot_fp);
GUI_DispStringAt("                                                                      ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
   				GUI_DispStringAt(Strings[Screen_shot_completed_Str].Text, WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
		GUI_Delay(1000);
GUI_DispStringAt("                                                                      ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
   				bytecnt = 0;
	}
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       MainTask
*/
void MainTask(void) 
{
  _ExportToFile();
 
}
#endif //#ifdef SCREENSHOT
/*************************** End of file ****************************/

