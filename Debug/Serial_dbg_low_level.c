/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Serial_dbg_low_level.c
* @brief			         : Serial Input Output for NXP NXP LPC178x/7x
*
* @author			         : Anagha Basole
*
* @date Created		     : July Monday, 2012  <July 16, 2012>
* @date Last Modified	 : July Monday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifdef EVALUATION_BOARD
/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdio.h>
#include <RTL.h>
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "Serial_dbg_low_level.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define DEBUG_UART    				LPC_UART2

#define DBG_UART_PCONP				24					/*UART2 corresponds to PCONP bit 24*/

#define DBG_PORT_RX   				P0_11
#define DBG_PORT_TX   				P0_10

#define DBG_LCR_COM_PARAM 		0x83 				/* 8 bits, no Parity, 1 Stop bit   */
#define DBG_LCR_DLAB0     		0x03 				/* DLAB = 0                        */
#define DBG_DLL_VAL						21   				/* 115200 Baud Rate @ 60.0 MHZ PCLK*/
#define DBG_DLM_VAL						0		 				/* High divisor latch = 0          */
#define DBG_FDR_VAL						0x95 				/* FR 1,556, DIVADDVAL=5, MULVAL=9 */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

 /*****************************************************************************
* @note       Function name: void Debugport_init (void) 
* @returns    returns		   : None
* @param      arg1			   : None
* @param      arg2			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Initialize Serial Interface
* @note       Notes		     :                                    													
*****************************************************************************/
void Debugport_init (void) 
{

  /* Power Up the UART2 controller. */
  LPC_SC->PCONP |=  (1 << DBG_UART_PCONP);
  
  /* Configure UART2 pins */
  LPC_IOCON->DBG_PORT_TX =  ( 1UL <<  0);        /* Pin P0.10 used as TXD2          */
  LPC_IOCON->DBG_PORT_RX =  ( 1UL <<  0);        /* Pin P0.11 used as RXD2          */

  /* Init UART2                                                               */
 #ifdef our_board	
  UART->LCR   = 0x83;              /* 8 bits, no parity, 1 stop bit      */
  UART->DLL   = 36;                /* 115200 Baudrate @ 100 MHz PCLK      */
  UART->DLM   = 0;								 /* High divisor latch = 0          */
  UART->FDR   = 0x21;              /*FR 1.507, DIVADDVAL = 1, MULVAL = 2*/
  UART->LCR   = 0x03;              /* DLAB = 0                           */
 #endif
	DEBUG_UART->LCR   = DBG_LCR_COM_PARAM;         /* 8 bits, no Parity, 1 Stop bit   */
  DEBUG_UART->DLL   = DBG_DLL_VAL;               /* 115200 Baud Rate @ 60.0 MHZ PCLK*/
  DEBUG_UART->FDR   = DBG_FDR_VAL;               /* FR 1,556, DIVADDVAL=5, MULVAL=9 */
  DEBUG_UART->DLM   = DBG_DLM_VAL;               /* High divisor latch = 0          */
  DEBUG_UART->LCR   = DBG_LCR_DLAB0;             /* DLAB = 0                        */
}

/*****************************************************************************
* @note       Function name: S32 Debugport_putchar (S32 ch) 
* @returns    returns		   : The character output
* @param      arg1			   : the character to be output
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Write a character to the Serial Port
* @note       Notes		     : None
*****************************************************************************/
S32 Debugport_putchar (S32 ch) 
{
  while (!(DEBUG_UART->LSR & 0x20));
  DEBUG_UART->THR = ch;

  return (ch);
}

/*****************************************************************************
* @note       Function name: int32_t Debugport_getchar (void)
* @returns    returns		   : the parameter read or -1 in case of error
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Read a character from the Serial Port
* @note       Notes		     : None
*****************************************************************************/
S32 Debugport_getchar (void) 
{

 if (DEBUG_UART->LSR & 0x01) 
	{
    return (DEBUG_UART->RBR);
  }
  return (-1);
	
}
#endif //#ifdef EVALUATION_BOARD
/*****************************************************************************
* End of file
*****************************************************************************/
