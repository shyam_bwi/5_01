/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Expansion_slot_low_level.c
* @brief               : Serial Input Output for NXP NXP LPC178x/7x
*
* @author              : Anagha Basole
*
* @date Created        : June 12, 2013
* @date Last Modified  : June 12, 2013
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifdef EXPANSION_SLOT

//DRIVER NOT TESTED
/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdio.h>
#include <RTL.h>
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "Expansion_slot_low_level.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define EXP_SLOT_UART            LPC_UART0

#define EXP_SLOT_UART_PCONP      3           /*UART0 corresponds to PCONP bit 3*/

#define EXP_SLOT_PORT_RX         P0_0
#define EXP_SLOT_PORT_TX         P0_1

#define EXP_SLOT_LCR_COM_PARAM   0x83         /* 8 bits, no Parity, 1 Stop bit   */
#define EXP_SLOT_LCR_DLAB0       0x03         /* DLAB = 0                        */
#define EXP_SLOT_DLL_VAL         21           /* 115200 Baud Rate @ 60.0 MHZ PCLK*/
#define EXP_SLOT_DLM_VAL         0            /* High divisor latch = 0          */
#define EXP_SLOT_FDR_VAL         0x95         /* FR 1,556, DIVADDVAL=5, MULVAL=9 */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

 /*****************************************************************************
* @note       Function name: void Expansion_slot_init (void)
* @returns    returns      : None
* @param      arg1         : None
* @param      arg2         : None
* @author                  : Anagha Basole
* @date       date created : June 12, 2013
* @brief      Description  : Initialize Serial Interface
* @note       Notes        :
*****************************************************************************/
void Expansion_slot_init (void)
{

  /* Power Up the UART0 controller. */
  LPC_SC->PCONP |=  (1 << EXP_SLOT_UART_PCONP);

  /* Configure UART0 pins */
  LPC_IOCON->EXP_SLOT_PORT_TX =  (1UL << 0);             /* Pin P0.10 used as TXD2*/
  LPC_IOCON->EXP_SLOT_PORT_RX =  (1UL << 0);             /* Pin P0.11 used as RXD2*/

  /* Init UART0*/
  EXP_SLOT_UART->LCR   = EXP_SLOT_LCR_COM_PARAM;         /* 8 bits, no Parity, 1 Stop bit   */
  EXP_SLOT_UART->DLL   = EXP_SLOT_DLL_VAL;               /* 115200 Baud Rate @ 60.0 MHZ PCLK*/
  EXP_SLOT_UART->FDR   = EXP_SLOT_FDR_VAL;               /* FR 1,556, DIVADDVAL=5, MULVAL=9 */
  EXP_SLOT_UART->DLM   = EXP_SLOT_DLM_VAL;               /* High divisor latch = 0          */
  EXP_SLOT_UART->LCR   = EXP_SLOT_LCR_DLAB0;             /* DLAB = 0                        */
}

/*****************************************************************************
* @note       Function name: S32 Expansion_slot_putchar (S32 ch)
* @returns    returns      : The character output
* @param      arg1         : the character to be output
* @author                  : Anagha Basole
* @date       date created : June 12, 2013
* @brief      Description  : Write a character to the Serial Port
* @note       Notes        : None
*****************************************************************************/
S32 Expansion_slot_putchar (S32 ch)
{
  //check if THR contains valid data (0 corresponds to valid data, 1 corresponds to empty)
  while (!(EXP_SLOT_UART->LSR & 0x20));
  EXP_SLOT_UART->THR = ch; //load the character to be transmitted into the THR

  return (ch);
}

/*****************************************************************************
* @note       Function name: S32 Expansion_slot_getchar (void)
* @returns    returns      : the parameter read or -1 in case of error
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : June 12, 2013
* @brief      Description  : Read a character from the Serial Port
* @note       Notes        : None
*****************************************************************************/
S32 Expansion_slot_getchar (void)
{
  //check if data has been received
  if (EXP_SLOT_UART->LSR & 0x01)
  {
    //read the received character from the receive buffer.
    return (EXP_SLOT_UART->RBR);
  }
  return (-1);

}
#endif /*EXPANSION_SLOT*/
/*****************************************************************************
* End of file
*****************************************************************************/
