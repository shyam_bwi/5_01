/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Expansion_slot_low_level.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 16, 2012>
* @date Last Modified  : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __EXPANSION_SLOT_LOW_LEVEL_H
#define __EXPANSION_SLOT_LOW_LEVEL_H

#ifdef EXPANSION_SLOT
/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdint.h>
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void Expansion_slot_init (void);
extern S32 Expansion_slot_putchar (S32 ch);
extern S32 Expansion_slot_getchar (void);
#endif /*EXPANSION_SLOT*/

#endif /* __EXPANSION_SLOT_LOW_LEVEL */
/*****************************************************************************
* End of file
*****************************************************************************/
