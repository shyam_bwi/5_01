

#include "Global_ex.h"
#include "udp_test_main.h"
#include "Ext_flash_low_level.h"
#include"flash_rw.h"
#define FLASH_BASE_ADDRESS			0x80000000
#define FLASH_END_ADDRESS				0x80FFFFFF
#define SECTOR_LENGTH_EXT_FLASH 0x20000
extern U8 *sendbuf;
extern  udpPacket_type udpdata;
extern area_t area_details;
void Remove_file_from_list(	FILE *file);
void ListSectors_debug(struct_myapi* spMyApiStruct, char* strType);

void Flash_sector_erase(char* ptr)
{
    int success,i;
    //int record_Id;
    //char *ptr;
    U32 read_flash_addr1;
// U16 *read_data;
//	char data_buf[40];
//	char erase_sector[13];
//	char*udp_ptr;
    char *p;


    read_flash_addr1 =0x80000000+strtol(ptr,&p,16);
    //converts string to long with a base of 16 (hexadecimal.
    //p is a pointer to the unconverted string ,ptr is the string to convert
    //values should be ex..  (80f0045d47)
//read_flash_addr1 = atoi(ptr);
    if((read_flash_addr1>= 0x80000000)&&(read_flash_addr1 <= 0x80FFFFFF))
    {

        success = ExtFlash_eraseBlock((uint32_t)read_flash_addr1);
        if(success == 1)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "\r\nSector erase successful");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

        }
        if(success == 0)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "\r\nSector erase failed");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

        }
    }
    else
    {
        sendbuf = udp_get_buf (50);

        strcpy((char*)sendbuf, "\r\nEnter correct flash address");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

    }

    //	sendflag =0;
}


void Flash_write_word(char* data1,char* data2)
{
    int success,i;
    U32 read_flash_addr1;
    U16 data_word;
    char*udp_ptr;



    read_flash_addr1 = atoi(data1);
    data_word = atoi(data2);

    if((read_flash_addr1>= 0x80000000)&&(read_flash_addr1 <= 0x807FFFFF))
    {
        success = ExtFlash_writeWord((uint32_t)read_flash_addr1, 	data_word );
        if(success == 1)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "\r\nWrite word successful");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

        }
        if(success == 0)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "\r\nWrite word failed");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

        }
    }

    else
    {

        sendbuf = udp_get_buf (50);
        //sprintf((char*)sendbuf,"%s", "\r\nEnter flash address in range");
        strcpy((char*)sendbuf, "\r\nEnter flash address in range");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
}


void Flash_Read_word(void)
{

    int success,i;
    int record_Id;
    char *ptr;
    U32 read_flash_addr1;
    U16 *read_data;

    char data_buf[40];
    char read_sector[13];
    char*udp_ptr;

    sprintf(read_sector,"%s","READ_WORD:");
    udp_ptr =(char*) udpdata.buf;
    // udp_ptr = &IP_Data_str_rx[0];
    if((ptr = strstr( udp_ptr,read_sector)))
    {
        ptr = ptr + strlen(read_sector);
        memset(data_buf,'\0',sizeof(data_buf));
        i=0;
        while(1)
        {
            if(*(ptr + i) == '!' )
            {
                break;
            }
            data_buf[i] |= (*(ptr + i)- 0x30);
            i++;
            if(i > (sizeof(data_buf) -1))break;
        }
        for(i=0; i<4; i++)
        {
            read_flash_addr1 = read_flash_addr1 << 4;
            read_flash_addr1 |= data_buf[i*2];
            read_flash_addr1 = read_flash_addr1 << 4;
            read_flash_addr1|=data_buf[i*2+1];
        }
        if((read_flash_addr1>= 0x80000000)&&(read_flash_addr1 <= 0x807FFFFF))
        {
            read_data = (U16*)read_flash_addr1;

            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf,"\r\n""Data read is 0x%X",*read_data);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
        }
        else
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "\r\nEnter flash address in range");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

        }

        //sendflag =0;
    }
}

void Nor_list_sector (void)
{
    char *ptr;
    U32 read_flash_addr1;
    char*udp_ptr;
    U32 tmp_flash_addr,sector_no;
    udp_ptr = (char*)udpdata.buf;

    //U16 *read_data;
  //  if((ptr = strstr(udp_ptr,"NOR_LIST_SECTORS:")))
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "\r\nSECTOR NO  ,\tSECTOR START ADDRESS");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
        //sprintf(IP_Data_str_tx,"\r\nSECTOR NO  ,\tSECTOR START ADDRESS");
        //send_data_tcp(IP_Data_str_tx);
        os_dly_wait(1);
        for(read_flash_addr1=FLASH_BASE_ADDRESS; read_flash_addr1<FLASH_END_ADDRESS;)
        {
            tmp_flash_addr = read_flash_addr1 - 0x80000000;
            sector_no = (tmp_flash_addr / 0x20000);
            os_dly_wait(1);
            sendbuf = udp_get_buf (50);
            sprintf( (char*)sendbuf,"\r\n%d  ,\t\t\t0x%X",sector_no+1,read_flash_addr1);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
            read_flash_addr1 = read_flash_addr1 + 0x20000;
        }
        //	sendflag =0;
    }
}
void write_sector(char *data1,char*data2)
{
    char *ptr;
    int success,i;
    U32 read_flash_addr1;
    char*udp_ptr;
    char data_buf[40];
    U32 tmp_flash_addr,sector_no;
    U32 tmp_flash_addr1,sector_no1,base_address;
    U32 Sector_size=0x20000;
    char data[13];
    U16 data_word;
    U32 j=0;
    U32 No_of_word=0;
    char *ptr1;
    char *p;
    ptr1=data1;
    read_flash_addr1 =strtol(ptr1,&p,16);
    read_flash_addr1=read_flash_addr1+FLASH_BASE_ADDRESS;

    data_word=	atoi(data2);
// 		 ptr1=data2;
// 		data_buf[0] = *ptr1;
// 		ptr1++;
// 		data_buf[1]=*ptr1;

// 		data_word = data_buf[0];
// 		data_word = data_word << 8;
// 		data_word |=data_buf[1];

    tmp_flash_addr = read_flash_addr1 - 0x80000000;
    sector_no = (tmp_flash_addr / 0x20000);

    read_flash_addr1=0x80000000+(sector_no*0x20000);
    if((read_flash_addr1>= FLASH_BASE_ADDRESS	)&&(read_flash_addr1 <= FLASH_END_ADDRESS))
    {
        success = ExtFlash_eraseBlock((uint32_t)read_flash_addr1);
        if(success == 1)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Sector erase successful");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

        }
        if(success == 0)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Sector erase failed");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

        }


        for(j=0; j<Sector_size/2; j++)
        {
            success = ExtFlash_writeWord((uint32_t)read_flash_addr1, 	data_word );
            if (success ==1)
            {
                No_of_word++;
            }
            if (success==0)
            {
                sendbuf = udp_get_buf (50);
                sprintf((char*)sendbuf, "Write failed at address 0x%X",read_flash_addr1);
                udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
            }
            read_flash_addr1+=2;

        }
        sendbuf = udp_get_buf (50);
        sprintf((char*)sendbuf, "Write Sector sucess with no of words %d",No_of_word);
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
    else
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Enter correct flash address in range");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

    }

}
void Verify_sector(char *data1,char*data2)
{
    char *ptr;
    int success,i;
    U32 read_flash_addr1;
    char*udp_ptr;
    char data_buf[40];
    U32 tmp_flash_addr,sector_no;
    U32 tmp_flash_addr1,sector_no1,base_address;
    U32 Sector_size=0x20000;
    char data[13];
    U16 data_word;
    U32 j=0;
    U32 No_of_word=0;
    U16 Data_verify;
    U16 *read_data;
    char *ptr1;
    char *p;
    ptr1=data1;
// 		for(i=0;i<4;i++)
// 		{
// 			read_flash_addr1 = read_flash_addr1 << 4;
// 			read_flash_addr1 |= *ptr1-0x30;
// 			read_flash_addr1 = read_flash_addr1 << 4;
// 			ptr1++;
// 			read_flash_addr1|=*ptr1-0x30;
// 			ptr1++;
// 		}
    read_flash_addr1 =strtol(ptr1,&p,16);
    read_flash_addr1=read_flash_addr1+FLASH_BASE_ADDRESS;
    data_word=	atoi(data2);
// 		     ptr1=data2;
// 			  data_buf[0] = *ptr1;
// 				ptr1++;
// 				data_buf[1]=*ptr1;
// 			  data_word = data_buf[0];
// 				data_word = data_word << 8;
// 				data_word |=data_buf[1];
//
    tmp_flash_addr = read_flash_addr1 - 0x80000000;
    sector_no = (tmp_flash_addr / 0x20000);

    read_flash_addr1=0x80000000+(sector_no*0x20000);
    if((read_flash_addr1>= FLASH_BASE_ADDRESS	)&&(read_flash_addr1 <= FLASH_END_ADDRESS))
    {
        for(j=0; j<Sector_size/2; j++)
        {
            read_data = (U16*)read_flash_addr1;
            Data_verify = *read_data ;
            if (Data_verify ==data_word)
            {
                No_of_word++;
            }
            if (Data_verify !=data_word)
            {
                sendbuf = udp_get_buf (50);
                sprintf((char*)sendbuf, " Verification failed at address 0x%X",read_flash_addr1);
                udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
            }
            read_flash_addr1+=2;

        }
        sendbuf = udp_get_buf (50);
        sprintf((char*)sendbuf, "Verify Sector sucess with no of words %d",No_of_word);
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
    }
    else
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Enter correct flash address in range");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

    }

}


void write_and_verify(char *data1,char *data2)
{
    char *ptr;
    int success,i;
    U32 read_flash_addr1;
    char*udp_ptr;
    char data_buf[40];
    char data[13];
    U16 data_word;
    U32 j=0;
    U32 No_of_word=0;
    U16 Data_verify;
    U16 *read_data;
    char *ptr1;
    char *p;
    ptr1=data1;
// 		for(i=0;i<4;i++)
// 		{
// 			read_flash_addr1 = read_flash_addr1 << 4;
// 			read_flash_addr1 |= *ptr1-0x30;
// 			read_flash_addr1 = read_flash_addr1 << 4;
// 			ptr1++;
// 			read_flash_addr1|=*ptr1-0x30;
// 			ptr1++;
// 		}
    read_flash_addr1 =strtol(ptr1,&p,16);
    read_flash_addr1=read_flash_addr1+FLASH_BASE_ADDRESS;


    if((read_flash_addr1>=FLASH_BASE_ADDRESS	)&&(read_flash_addr1 <= FLASH_END_ADDRESS))
    {
        success = ExtFlash_eraseBlock((uint32_t)read_flash_addr1);

        if(success == 1)
        {

            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Sector erase successful");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
        if(success == 0)
        {

            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Sector erase failed");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        }
    }
    data_word=	atoi(data2);
// 				ptr1=data2;
// 			  data_buf[0] = *ptr1;
// 				ptr1++;
// 				data_buf[1]=*ptr1;
// 			  data_word = data_buf[0];
// 				data_word = data_word << 8;
// 				data_word |=data_buf[1];

    if((read_flash_addr1>= FLASH_BASE_ADDRESS	)&&(read_flash_addr1 <= FLASH_END_ADDRESS))
    {
        success = ExtFlash_writeWord((uint32_t)read_flash_addr1, 	data_word );
        if(success == 1)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Write word successful");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        }
        if(success == 0)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Write word failed");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        }
    }
    else
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Enter proper address");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

    }
    read_data = (U16*)read_flash_addr1;
    data_word = *read_data;
    sendbuf = udp_get_buf (50);
    sprintf((char*)sendbuf,"Data read is 0x%X",data_word);
    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

}






void USB_Status(char *data1)
{
    char *ptr;
    int success;
    char*udp_ptr;
    if(*data1== '0')
    {
        success = usbh_msc_status(0, 0);//finit("U0");
    }
    else if(*data1 == '1')
    {

        success = usbh_msc_status(1, 1);//finit("U1");

    }
    switch(success)
    {
    case 1:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf,"usb device  is connected");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        break;
    case 0:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf," usb device is not connected ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        break;


    }
    //	send_data_tcp(IP_Data_str_tx);
}
void write_to_file(char* data1,char* data2,char* data3)
{
    char *ptr;
    int success,i;
    char*udp_ptr;
    U16 len;
    U16 size;
    char data_buf[256];
    FILE* file_pointer;
    char fname[FILE_NAME_SIZE] = { 0 };
    int error;
    //file_pointer= (FILE*)malloc(sizeof(FILE*)*1);
    //file_pointer_global[file_pointer_increment];//=file_pointer;
    strcpy(fname,data1);
    len=atoi(data2);
//file_pointer_global[file_pointer_increment]= fopen(fname, "w");
    file_pointer = fopen(fname, "r");
    if (file_pointer!= NULL)
    {

        size = fread (data_buf, 1, len, file_pointer);
        if(size < len)
        {

            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Error in file reading");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
        else if(size == len)
        {

            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, " File reading is successful with file pointer =%d",file_pointer);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, " Data read from file is\n\r %s",data_buf);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
            fclose(file_pointer);
        }
        //ADD_file_to_list(file_pointer_global[file_pointer_increment],fname);

    }
    else
    {

        file_pointer = fopen(fname, "w");
        if (file_pointer!= NULL)
        {
            size = fwrite (data3, 1, len,file_pointer);
            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, " File pointer is =%d ",file_pointer);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
            fclose(file_pointer);
        }

        else
        {
            file_pointer = fopen(fname, "a");
            if (file_pointer!= NULL)
            {
                size = fwrite (data3, 1, len, file_pointer);
                if(size < len)
                {

                    sendbuf = udp_get_buf (50);
                    strcpy((char*)sendbuf, " Error in data writing");
                    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
                }
                else if(size ==len)
                {
                    sendbuf = udp_get_buf (50);
                    sprintf((char*)sendbuf, " File pointer is =%d ",file_pointer);
                    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

                    sendbuf = udp_get_buf (50);
                    strcpy((char*)sendbuf, " Data writing in file is successful");
                    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
                    fclose(file_pointer);

                }
            }

        }
    }



}
void ADD_file_to_list(FILE *file,char *fname1);
void Read_to_file(char* data1,char* data2)
{

    char *ptr;
    int success,i;
    char*tcp_ptr;
    U16 len;
    U16 size;
    char data_buf[256];
    FILE* file_pointer1;
    char fname[FILE_NAME_SIZE] = { 0 };
    int error;
    strcpy(fname,data1);
    len=atoi(data2);
    memset(data_buf,'\0',sizeof(data_buf));

    file_pointer1= fopen(fname, "r");
    if ( file_pointer1!= NULL)
    {
        //file_open_count++;
        ADD_file_to_list(file_pointer1,fname);
        size = fread (data_buf, 1, len, file_pointer1);
        if(size < len)
        {

            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Error in file reading");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
        else if(size == len)
        {

            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, " File reading is successful with file pointer =%d",file_pointer1);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, " Data read from file is\n\r %s",data_buf);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
// 						if (file_pointer_increment>12)
// 						{
// 							file_pointer_increment=0;
// 						}
// 						else
// 						{
// 						 file_pointer_increment++;
// 						}

    }
    else
    {

        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Error in file opening in read mode");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
    }



}
FILE* file_pointer_global[12],*file_pointer_array[12];
int file_pointer_increment,file_open_count,file_close_count,FILE_CHECK_COUNT,file_still_open_flag,file_still_open_flag;
char* file_name_array[12];
void Append_to_file(char* data1,char* data2,char* data3)
{
    char *ptr;
    int success,i;
    char*tcp_ptr;
    U16 len;
    U16 size;
    char data_buf[256];
    FILE* file_pointer2;
    char fname[FILE_NAME_SIZE] = { 0 };
    int error;

    strcpy(fname,data1);
    len=atoi(data2);
    file_pointer_global[file_pointer_increment]= fopen(fname, "a");
    if ( file_pointer_global[file_pointer_increment]!= NULL)
    {

        ADD_file_to_list(file_pointer_global[file_pointer_increment],fname);


        size = fwrite (data3, 1, len, file_pointer_global[file_pointer_increment]);
        if(size < len)
        {

            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Error in data writing");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
        else if(size == len)
        {
            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, " File pointer is =%d ",file_pointer_global[file_pointer_increment]);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));


            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Data writing in file is successful");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        }

        if (file_pointer_increment>12)
        {
            file_pointer_increment=0;
        }
        else
        {
            file_pointer_increment++;
        }
    }
    else
    {

        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Error in file opening in append mode");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

    }
}

void verify_file_open_close(void)
{
    int i=0;
    sendbuf = udp_get_buf (50);
    sprintf((char*)sendbuf, "\r\n no of files open= %d   \t no of files closed= %d   ",file_open_count,	file_close_count);
    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
    for(i=0; i<FILE_CHECK_COUNT; i++)
    {
        if(	file_pointer_array[i]!=0)
        {
            file_still_open_flag=1;
            break;

        }
    }
    if (file_still_open_flag==1)
    {
        file_still_open_flag=0;

        sendbuf = udp_get_buf (50);
        sprintf((char*)sendbuf, "\r\n no of files still open are %d ",(file_open_count-file_close_count));
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        sendbuf = udp_get_buf (50);
        sprintf((char*)sendbuf, "\r\nFile name\t\t\t\t	File pointer");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        for(i=0; i<FILE_CHECK_COUNT; i++)
        {
            if(	file_pointer_array[i]!=0)
            {
                //strcpy(temp_file_name_array,file_name_array[i]);
                sendbuf = udp_get_buf (50);
                sprintf((char*)sendbuf, "\r\n%s\t\t\t\t%d",file_name_array[i],file_pointer_array[i]);
                udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

            }
        }

    }
}

void close_file(char *data1)
{

    char *ptr;
    int success,i;
    char*tcp_ptr;
    U16 len;
    U16 size;
    char data_buf[256];
    FILE* file;
    char fname[FILE_NAME_SIZE] = { 0 };
    U32 file_pointer;
    char temp_file_name_array[40];
    void  *file_pointer1;
    file_pointer=	atoi(data1);
    file_pointer1	= 	( unsigned int *)file_pointer;
    file=	file_pointer1;
    // if (file != NULL)
    {
        success=fclose(file);
        if (success!=0)
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, " Error in file closing");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
        if (success==0)
        {
            //	file_open_count++;
            Remove_file_from_list(file);
            //file_count--;
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, " file closed successfully ");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
            verify_file_open_close();
        }

    }
}




void USB_FINIT(char *data1)

{
    char *ptr;
    int success;

    ptr	=data1;

    if(*ptr == '0')
    {

        success = finit("U0");
    }
    else if(*ptr == '1')
    {

        success = finit("U1");

    }
    switch(success)
    {
    case 0:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " File system initialized ok");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 1:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " IO Error. IO/driver initialization failed");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 2:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Volume Error. Mount failed.invalid FAT formatting");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 3:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Driver Configuration Error");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 4:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Journal initialization failed");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;

    }
}



void USB_LAST_ERROR(char *data1)
{
    char *ptr;
    int success;
    char*tcp_ptr;
    tcp_ptr = udpdata.buf;
    ptr=data1;


    if(*ptr == '0')
    {

        success = usbh_msc_get_last_error(0,0);  //finit("U0");
    }
    else if(*ptr == '1')
    {

        success = usbh_msc_get_last_error(1,1);

    }
    switch(success)
    {
    case 0:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " No Errors ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 1:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Host Ctrl Instance does not exist ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 2:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Host Ctrl Driver does not exist ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 3:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Device Class Instance does not exst");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 4:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Device Class Driver does not exist ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 5:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Memory initialization failed ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 6:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Memory allocation failed  ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 7:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "  Memory deallocation failed          ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 8:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Pins not conf/unconfigured          ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 9:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Controller not init/uninitialized   ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 10:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Port power driving failed           ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 11:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "  Port reset failed                   ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 12:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Endpoint does not exist             ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 13:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Endpoint was not added              ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 14:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Endpoint was not configured         ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 15:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Endpoint was not removed            ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 16:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " URB does not exist                 ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 17:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " URB was not submitted       ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 18:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "URB was not cancelled              ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 19:
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " URB transfered with error           ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 20://1A
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "URB has timed-out  ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 21://1B
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Device enumeration failed           ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 22://1E
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Device uninitialization failed     ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 23://1A
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "MSC Device Instance does not exist  ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 24://1B
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " MSC Class CSW Signature not correct ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 25://1E
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "MSC Class different tags            ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 26://1A
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " MSC Class data residue not 0        ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 27://1B
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "HID Device Instance does not exist  ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    case 30://1E
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " USB Interface Disabled through Code ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;

    }
}

void USB_FUNINIT(char *data1)
{
    char *ptr;
    int success;
    char*tcp_ptr;

    ptr = data1;

    if(*ptr == '0')
    {
        success = funinit("U0");
    }
    else if(*ptr == '1')
    {
        success = funinit("U1");

    }
    switch(success)
    {
    case 0:
        //sprintf(IP_Data_str_tx,"\r\nFile system uninitialized successfully");
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "File system uninitialized successfully ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;
    default:
        //sprintf(IP_Data_str_tx,"\r\nError in file system uninit");
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Error in file system uninit");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;

    }


}


void f_seek(char *data1,char* data2,char*data3)
{
    char *ptr;
    int success,i;
    char*tcp_ptr;
    char data_buf[256];
    char data_length[256];
    FILE* file;
    char fname[FILE_NAME_SIZE] = { 0 };
    U32 file_pointer;
    void  *file_pointer1;
    // U16 SET_CURSOR1=0;
    long len;
    char ch1;
//U16 buffer;
    file_pointer=	atoi(data1);
    file_pointer1	= 	( unsigned int *)file_pointer;
    file=	file_pointer1;
    len=	atoi(data3);

    switch(*data2)
    {
    case '0':
    {
        success=	fseek (file, len,SEEK_SET);                     // Read the 5th character from file
        ch1 = fgetc (file);
        break;

    }
    case'1':
        {
            success=	fseek (file, len,SEEK_CUR);                     // Read the 5th character from file
            //	success=	fseek (file, 4L,SEEK_CUR);                     // Read the 5th character from file
            ch1 = fgetc (file);
            break;
        }
        case '2' :
    {
        success=	fseek (file, len,SEEK_END);                     // Read the 5th character from file
        ch1 = fgetc (file);
        break;
    }

    }
    memset(data_buf,'\0',sizeof(data_buf));
    data_buf[0]=ch1;

    switch(success)
    {
    case 0:
        //sprintf(IP_Data_str_tx,"\r\nFile system uninitialized successfully");
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "file pointer set to new location successfully ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        sendbuf = udp_get_buf (50);
        sprintf((char*)sendbuf, " return character %s ",data_buf);
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        break;
    default:
        //sprintf(IP_Data_str_tx,"\r\nError in file system uninit");
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "Error in fseek");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        break;

    }

}



void f_format(char * data1,char *data2)
{
    char *Format;
    int success;
    char*tcp_ptr;
    char *Option;
    //U16 option;
    Format=data2;
    Option=data1;

//   if(ptr = strstr( tcp_ptr ,"USB_FORMAT:"))
// 	 {
//    ptr = ptr + 11;
// 	Format= *ptr;
// 	if(ptr = strstr( tcp_ptr ,"OPTION:"))
//  {
// 	ptr = ptr + 7;
// 	option = *ptr;
// 		if(option == '!')
// 			{
// 				option = '0';
// 			}
    if(	*Format== '0')
    {
        if(*Option== '0')
        {
            success=fformat ("U0:");
        }
        else
        {
            success= fformat ("U0:/FAT32");
        }
    }
    if(*Format== '1')
    {
        if(*Option == '0')
        {
            success= fformat ("U1:");
        }
        else
        {
            success= fformat ("U1:/FAT32");
        }

    }
    if( success==0)
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "\r\n formatting success ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
    }
    else
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "\r\n formatting failed ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
    }

}



void f_tell(void)
{
    char *ptr;
    int success;
    char*tcp_ptr;
    U16 Formate;
    U16 option;


    tcp_ptr = udpdata.buf;
    if(ptr = strstr( tcp_ptr,"USB_FORMAT:"))
    {

        ptr = ptr + 11;


        Formate= 	*ptr;
        if(ptr = strstr( tcp_ptr,"OPTION:"))
        {
            ptr = ptr + 7;
            option= *ptr;
            success=	fformat ("Formate:option");
            if( success==0)
            {
                sendbuf = udp_get_buf (50);
                strcpy((char*)sendbuf, "\r\n formatting success ");
                udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
            }
            else
            {
                sendbuf = udp_get_buf (50);
                strcpy((char*)sendbuf, "\r\n formatting failed ");
                udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
            }




        }
    }

}
void Device_id(void)
{

    char *ptr;
    int success;
    char*tcp_ptr;
    U16 Formate;
    U16 option;
    uint16_t info[14];
    int status=0;
    uint32_t Device_size;
    uint32_t Num_mem_region1;
    uint32_t Mem_region1_size;
    uint32_t Num_mem_region2;
    uint32_t Mem_region2_size;
    uint32_t Result;
    uint32_t device_id;
    uint32_t Manufacture_id;

    //tcp_ptr = udpdata.buf;

// 	if(ptr = strstr( tcp_ptr ,"READ_DEVICE_INFO:"));
    //{
    Result=	Read_DeviceId_Flash();
    device_id=Result|0xffff0000;
    device_id=device_id & 0x0000ffff;
    Manufacture_id=Result|0x0000ffff;
    Manufacture_id= Manufacture_id&0xffff0000;
    Manufacture_id= Manufacture_id>>16;
    sendbuf = udp_get_buf (50);
    sprintf((char*)sendbuf, "Device id =0x%X  Manufacturer id=0x%X",device_id, Manufacture_id);
    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

    getGeoInfo(info);
    Device_size = 1 << info[0];
    Num_mem_region1 = ((info[7] << 16) | info[6]) + 1;
    Mem_region1_size = ((info[9] << 16) | info[8]) ;
    Num_mem_region2 =  ((info[11] << 16) | info[10]) ;
    Mem_region2_size = ((info[13] << 16) | info[12]) ;
    memset(	sendbuf,'\0',sizeof(sendbuf));

    sendbuf = udp_get_buf (50);
    sprintf((char*)sendbuf, "Device Size =%d  No of Sector in region1= %d Sector size in bytes in region 1 =%d No of Sectors in region 2=%d sector size in bytes in region 2=%d ",	Device_size,Num_mem_region1,Mem_region1_size,	Num_mem_region2,	Mem_region2_size );
    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

}
void erase_chip(void)
{
    int success,i;
    success=	ExtFlash_eraseEntireChip();

    for(i=0; i<100000; i++)
    {
    }
    if(success == 1)
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Erasing entire chip successful");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

    }
    if(success == 0)
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, " Erasing entire chip failed");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
}

void Verify_flash(char *data1)
{
    char *ptr;
    int success;
    U32 read_flash_addr1;
    char*udp_ptr;
    char data_buf[40];
    U16 *read_data;
    U16 Data_verify;
    char data[13];
    U64 data_word;
    int chip_erase_flag=0;
    U32 j=0,i=0;
    U32 No_of_word=0;
    data_word=	atoi(data1);
    read_flash_addr1=0x80000000;
    data_word=	atoi(data1);
    for(j=0; j<(64); j++)
    {
        success = ExtFlash_eraseBlock((uint32_t)read_flash_addr1);
        for (i=0; i<0x10000; i++)
        {
            success = ExtFlash_writeWord((uint32_t)read_flash_addr1, data_word );
            read_data = (U16*)read_flash_addr1;
            Data_verify= *read_data;

            if (Data_verify !=data_word)
            {
                sendbuf = udp_get_buf (50);
                sprintf((char*)sendbuf, " Verification failed at address 0x%X",read_flash_addr1);
                udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
            }
            read_flash_addr1+=2;
        }
        sendbuf = udp_get_buf (50);
        sprintf((char*)sendbuf,"no of sector verify %d",j);
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        //read_flash_addr1+=0x20000;
    }
    sendbuf = udp_get_buf (50);
    sprintf((char*)sendbuf,"sector address0x%X",read_flash_addr1);
    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
}




void ADD_file_to_list(FILE *file,char *fname1)
{

    char fname[FILE_NAME_SIZE] = { 0 };
    int i=0;
    strcpy(fname,fname1);
    file_open_count++;
    for(i=0; i<FILE_CHECK_COUNT; i++)
    {
        if( file_pointer_array[i]==0)
        {
            file_pointer_array[i]=file;
            strcpy(&file_name_array[i][0],fname);
            break;
        }
    }
}

void Remove_file_from_list(	FILE *file)
{

    int i=0;
    file_close_count++;
    for(i=0; i<FILE_CHECK_COUNT; i++)
    {
        if( file_pointer_array[i]==	file)
        {
            file_pointer_array[i]=0;
            //sprintf(file_name_array[i][40], '\0', sizeof(file_name_array[i][40]));
            strcpy(&file_name_array[i][0],"0");
            break;
        }
    }
}

void flash_write_in_buff1(char* data1,char*data2,char*data3)
{
    char *ptr1;
    int i=0;
    U32 read_flash_addr1;
    int status=0;
    unsigned long length;
    char *p;
    ptr1=data1;

    read_flash_addr1 =strtol(ptr1,&p,16);
    read_flash_addr1=read_flash_addr1+FLASH_BASE_ADDRESS;
    length=	atoi (data2);
    if((read_flash_addr1>= FLASH_BASE_ADDRESS	)&&(read_flash_addr1 <= FLASH_END_ADDRESS))
    {
        status=flash_write_in_buff((uint32_t)read_flash_addr1,length,data3);
        if (status==1)
        {
            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, "Write buffer successfull = %s",data3);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
        else
        {
            sendbuf = udp_get_buf (50);
            strcpy((char*)sendbuf, "Write buffer failed");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
        }
    }
    else
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "please enter correct flash address");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));
    }
}
void flash_read_buff1(char* data1,char*data2)
{

    char *ptr1;
    int i=0;
    U32 read_flash_addr1;
    int status=0;
    unsigned long length;
    char * data3;
    U16 *read_value;
    char data_buffer[200];
    char*p;
    ptr1=data1;

    read_flash_addr1 =strtol(ptr1,&p,16);
    read_flash_addr1=read_flash_addr1+FLASH_BASE_ADDRESS;
    if((read_flash_addr1>= FLASH_BASE_ADDRESS		)&&(read_flash_addr1 <= FLASH_END_ADDRESS	))
    {
        length=	atoi (data2);
        memset(data_buffer,'\0',sizeof(data_buffer));
        for (i = 0; i < ((length)); i++)
        {

            read_value=  (uint16_t*)read_flash_addr1;
            data_buffer[i]=*read_value;
            read_flash_addr1 ++;
        }
        sendbuf = udp_get_buf (50);
        sprintf((char*)sendbuf, "Read buffer successfull = %s",data_buffer);
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

    }
    else
    {
        sendbuf = udp_get_buf (50);
        strcpy((char*)sendbuf, "please enter correct flash address ");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

    }



}
void backup_to_usb(char* ptr,char* data2)
{
    char *p;
    U32 read_flash_addr1;
    int i,j,k;
    U16 data;
    U32 tmp_flash_addr,sector_no;
    U32 Sector_size=0x20000;
    U8 data_buf[512];
    FILE* file;
    char fname[FILE_NAME_SIZE] = { 0 };
    U16 * read_flash_addr;
    U32 size = 0,size1 = 0;

//	p = data1;
    tmp_flash_addr =strtol(data2,&p,16);
    //converts string to long with a base of 16 (hexadecimal.
    //p is a pointer to the unconverted string ,ptr is the string to convert
    //values should be ex..  (80f0045d4d)
//read_flash_addr1 = atoi(ptr);
// 			for(i=0;i<4;i++)
// 			{
// 				read_flash_addr1 = read_flash_addr1 << 4;
// 				read_flash_addr1 |= *p-0x30;
// 				read_flash_addr1 = read_flash_addr1 << 4;
// 				p++;
// 				read_flash_addr1|=*p-0x30;
// 				p++;
// 			}
// 			tmp_flash_addr = read_flash_addr1 - 0x80000000;
    sector_no = (tmp_flash_addr / 0x20000);

    read_flash_addr1=tmp_flash_addr = 0x80000000+(sector_no*0x20000);
    //memset(fname,'\0',sizeof(fname));
    strcpy(fname,ptr);
    file = fopen(fname, "w");
    if (file != NULL)
    {
        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"File opened successfully in write mode with file pointer %d",file);
//					strcpy((char*)sendbuf, "Sector erase successful");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

    }
    else
    {
        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"Error in file opening in write mode");
//					strcpy((char*)sendbuf, "Sector erase successful");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
    fclose (file);
//			read_flash_addr1 = tmp_flash_addr;
    file = fopen(fname, "a");
    if (file != NULL)
    {
        for(i=0; read_flash_addr1< (tmp_flash_addr +SECTOR_LENGTH_EXT_FLASH ) ; i++)	//SECTOR_XX_ADDR
        {
            //				read_flash_addr1 = read_flash_addr1 + (SECTOR_LENGTH_EXT_FLASH )*j;
            //				ulFlashSectorEndAddress = ulFlashAddress + SECTOR_LENGTH_EXT_FLASH;
            //				read_flash_addr = (U16 *) (ulFlashAddress);
            //				read_flash_end_addr = (U16 *)ulFlashSectorEndAddress;

            for( j= 0; j < (SECTOR_LENGTH_EXT_FLASH/512); j++) //read_flash_addr <= read_flash_end_addr;i++)
            {

                read_flash_addr = (U16 *) (read_flash_addr1);
                for(k=0; k<256; k++)
                {
                    data =*read_flash_addr;
                    data_buf[k*2+1] = data;
                    data_buf[k*2] = (data >>8);
                    read_flash_addr++;
                }
                read_flash_addr1 = read_flash_addr1 + (512);

                fprintf(file,"%s",&data_buf[0]);
                size += 512;

            }

            // 				else
            // 				{
            // 					 sendbuf = udp_get_buf (50);
            // 						sprintf(sendbuf,"Error in file opening in append mode");
            // //					strcpy((char*)sendbuf, "Sector erase successful");
            // 					udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
            // 				}


        }

        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"No. of bytes written on file is %d",size);
//					strcpy((char*)sendbuf, "Sector erase successful");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }

    else
    {
        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"Error in file opening in append mode");
//					strcpy((char*)sendbuf, "Sector erase successful");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
    fclose(file);


}
void restore_from_usb(char* ptr,char* data2)
{
    char *p;
    U32 read_flash_addr1;
    int i,j,k;
    U16 data;
    U32 tmp_flash_addr,sector_no;
    U32 Sector_size=0x20000;
    U8 data_buf[512];
    FILE* file;
    char fname[40] = { 0 };
    U16 * read_flash_addr2;
    U32 size = 0,size1 = 0,total_file_size = 0;
    U32 file_offset = 0,bytes_to_read = 0;
//	p = data1;
    tmp_flash_addr =strtol(data2,&p,16);
    //converts string to long with a base of 16 (hexadecimal.
    //p is a pointer to the unconverted string ,ptr is the string to convert
    //values should be ex..  (80f0045d4d)
//read_flash_addr1 = atoi(ptr);
// 			for(i=0;i<4;i++)
// 			{
// 				read_flash_addr1 = read_flash_addr1 << 4;
// 				read_flash_addr1 |= *p-0x30;
// 				read_flash_addr1 = read_flash_addr1 << 4;
// 				p++;
// 				read_flash_addr1|=*p-0x30;
// 				p++;
// 			}
// 			tmp_flash_addr = read_flash_addr1 - 0x80000000;
    sector_no = (tmp_flash_addr / 0x20000);

    read_flash_addr1=tmp_flash_addr = 0x80000000+(sector_no*0x20000);
    memset(fname,'\0',sizeof(fname));
    strcpy(fname,ptr);
    file = fopen(fname, "r");
    if (file != NULL)
    {
        fseek(file,0,SEEK_SET);
        size1 = ftell(file);
        fseek(file,-1L,SEEK_END);
        size = ftell(file);
        total_file_size = size - size1;
        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"File opened successfully in read mode with file pointer %d",file);
//					strcpy((char*)sendbuf, "Sector erase successful");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

    }
    else
    {
        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"Error in file opening in read mode");
//					strcpy((char*)sendbuf, "Sector erase successful");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
    fclose (file);
//			read_flash_addr1 = tmp_flash_addr;
    size = 0;
    file = fopen(fname, "r");
    if (file != NULL)
    {
        for(i=0; read_flash_addr1< (tmp_flash_addr +SECTOR_LENGTH_EXT_FLASH ) ; i++)	//SECTOR_XX_ADDR
        {
            //				read_flash_addr1 = read_flash_addr1 + (SECTOR_LENGTH_EXT_FLASH )*j;
            //				ulFlashSectorEndAddress = ulFlashAddress + SECTOR_LENGTH_EXT_FLASH;
            //				read_flash_addr = (U16 *) (ulFlashAddress);
            //				read_flash_end_addr = (U16 *)ulFlashSectorEndAddress;


            for( j= 0; j < (SECTOR_LENGTH_EXT_FLASH/512); j++) //read_flash_addr <= read_flash_end_addr;i++)
            {

                //						file_offset += (bytes_to_read*j);
                fseek(file,file_offset,SEEK_SET);
                memset(data_buf,'\0',sizeof(data_buf));
                bytes_to_read = fread (&data_buf[0], sizeof (CHAR), sizeof (data_buf), file);

                read_flash_addr2 = (U16 *) (read_flash_addr1);
                for(k=0; k<(512/2); k++)
                {
                    data = data_buf[k*2];
                    data = data<<8 ;
                    data |= data_buf[k*2+1];
                    ExtFlash_writeWord((uint32_t)read_flash_addr2, 	data );
                    read_flash_addr2++;
                }
                file_offset = file_offset + 512;
                read_flash_addr1 = read_flash_addr1 + (512);

                //					fprintf(file,"\r\n %s",&data_buf[0]);
                size += bytes_to_read;


            }
        }

        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"No. of bytes read from file is %d",size);
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
    fclose(file);




}
void restore_file_from_usb(char* ptr,char* data2)
{
    char *p;
    U32 read_flash_addr1;
    int i,j,k;
    U16 data,success;

    U32 tmp_flash_addr,sector_no;
    U32 Sector_size=0x20000;
    U8 data_buf[512];
    FILE* file;
    char fname[40];
    U16 * read_flash_addr;
    U32 size = 0,size1 = 0,size2=0,total_file_size =0 ;
    U32 file_offset = 0,bytes_to_read = 0;

    //converts string to long with a base of 16 (hexadecimal.
    //p is a pointer to the unconverted string ,ptr is the string to convert
    //values should be ex..  (80f0045d4d)

    tmp_flash_addr =strtol(data2,&p,16);
    sector_no = (tmp_flash_addr / 0x20000);
    read_flash_addr1=0x80000000+(sector_no*0x20000);
    memset(fname,'\0',sizeof(fname));
    strcpy(fname,ptr);
    file = fopen(fname, "r");
    if (file != NULL)
    {
        fseek(file,0,SEEK_SET);
        size1 = ftell(file);
        fseek(file,-1L,SEEK_END);
        size = ftell(file);
        total_file_size = size1 = size2 =size - size1;
        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"File opened successfully in read mode with file pointer %d",file);
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));

    }
    else
    {
        sendbuf = udp_get_buf (50);
        sprintf(sendbuf,"Error in file opening in read mode");
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
    }
    fclose (file);

    sector_no = (total_file_size / 0x20000);
    if(total_file_size % 0x20000)
    {
        sector_no++;
    }
    for(i=0; i<sector_no; i++)
    {
        read_flash_addr1 = read_flash_addr1 + (SECTOR_LENGTH_EXT_FLASH )*i;
        success = ExtFlash_eraseBlock((uint32_t)read_flash_addr1);
        if(success == 1)
        {
            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, "Sector 0x%X erase successful",read_flash_addr1);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        }
        if(success == 0)
        {
            sendbuf = udp_get_buf (50);
            sprintf((char*)sendbuf, "Sector 0x%X erase failed",read_flash_addr1);
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen(sendbuf));

        }
    }

    j=0;
    for(size=0; size< total_file_size ;)	//SECTOR_XX_ADDR
    {
        if(size1 < 512)
        {
            file = fopen(fname, "r");
            if (file != NULL)
            {
                read_flash_addr = (U16 *) (read_flash_addr1);
                file_offset += (bytes_to_read);
                fseek(file,file_offset,SEEK_SET);
                read_flash_addr = (U16 *) (read_flash_addr1);
                bytes_to_read = fread (&data_buf[0], sizeof (CHAR), sizeof (data_buf), file);
                size1 = total_file_size-bytes_to_read;
                size = size+bytes_to_read;
                for(k=0; k<(bytes_to_read/2); k++)
                {
                    data = data_buf[k*2];
                    data = data<<8 ;
                    data |= data_buf[k*2+1];
                    ExtFlash_writeWord((uint32_t)read_flash_addr, 	data );
                    read_flash_addr++;
                }
                //							break;
                fclose(file);
            }
        }
        else
        {
            for(j = 0; j < (size2/512); j++) //read_flash_addr <= read_flash_end_addr;i++)
            {
                file = fopen(fname, "r");
                if (file != NULL)
                {
                    read_flash_addr = (U16 *) (read_flash_addr1);
                    file_offset +=(bytes_to_read );
                    fseek(file,file_offset,SEEK_SET);
                    bytes_to_read = fread (&data_buf[0], sizeof (CHAR), sizeof (data_buf), file);

                }
                fclose(file);
                for(k=0; k<(bytes_to_read/2); k++)
                {
                    data = data_buf[k*2];
                    data = data<<8 ;
                    data |= data_buf[k*2+1];
                    ExtFlash_writeWord((uint32_t)read_flash_addr, 	data );
                    read_flash_addr++;
                }
                read_flash_addr1 = read_flash_addr1 + (bytes_to_read);
                size = size+bytes_to_read;
                size1 = total_file_size-bytes_to_read;


            }
        }
    }

    sendbuf = udp_get_buf (50);
    sprintf(sendbuf,"No. of bytes read from file is %d",size);
    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
}
char command[40],data1[40],data2[40],data3[40],data4[40],data5[16],data6[16],data7[16],data8[16];
char dataBig[100];
uint32_t UDP_parser(U8* buf)
{
    int argc=0;

    char *strUdpParser,*tok;
    strUdpParser = (char*)malloc(strlen((char*)buf));
    strcpy(strUdpParser,(char*)buf);
    tok = strtok (strUdpParser,"\t");
    strcpy(command,tok);
    argc++;
    while (tok != NULL)
    {
        tok = strtok (NULL, "\t");
        switch (argc)
        {
        case	1:
            strcpy(data1,tok);
            break;
        case	2:
            strcpy(data2,tok);
            break;
        case	3:
            strcpy(data3,tok);
            break;
        case	4:
            strcpy(data4,tok);
            break;
        case	5:
            strcpy(data5,tok);
            break;
        case	6:
            strcpy(data6,tok);
            break;
        case	7:
            strcpy(data7,tok);
            break;
        case	8:
            strcpy(data8,tok);
            break;
        }
        argc++;
    }
    if (!strncmp(command,"ERASE_SECTOR",12))
    {
        Flash_sector_erase(data1);
    }
    if (!strncmp(command,"WRITE_WORD",10))
    {
        Flash_write_word(data1,data2);
    }
    if (!strncmp(command,"READ_WORD",9))
    {
//		Flash_Read_word(data1);
    }
    if (!strncmp(command,"NOR_LIST_SECTORS",16))
    {
        Nor_list_sector();
    }
    if (!strncmp(command,"WRITE_SECTOR",12))
    {
        write_sector(data1,data2);
    }
    if (!strncmp(command,"VERIFY_SECTOR",13))
    {
        Verify_sector(data1,data2);
    }
    if (!strncmp(command,"WRITE_AND_VERIFY",12))
    {
        write_and_verify(data1,data2);
    }
    if (!strncmp(command,"USB_FINIT",9))
    {
        USB_FINIT(data1);
    }
    if (!strncmp(command,"USB_FUNINIT",11))
    {
        USB_FUNINIT(data1);
    }
    if (!strncmp(command,"USB_STATUS",10))
    {
        USB_Status(data1);
    }
    if (!strncmp(command,"USB_LAST_ERROR",14))
    {
        USB_LAST_ERROR(data1);
    }
    if (!strncmp(command,"WRITE_TO_FILE",13))
    {
        write_to_file(data1,data2,data3);
    }
    if (!strncmp(command,"APPEND_TO_FILE",14))
    {
        Append_to_file(data1,data2,data3);
    }
    if (!strncmp(command,"READ_FILE",9))
    {
        Read_to_file(data1,data2);
    }
    if (!strncmp(command,"CLOSE_FILE",10))
    {
        close_file(data1);
    }
    if (!strncmp(command,"READ_DEVICE_INFO",16))
    {
        Device_id();
    }
    if (!strncmp(command,"USB_FSEEK",9))
    {
        f_seek(data1,data2,data3);
    }
    if (!strncmp(command,"ERASE_CHIP",10))
    {
        erase_chip();
    }
    if (!strncmp(command,"VERIFY_FLASH",12))
    {
        Verify_flash(data1);
    }
    if (!strncmp(command,"NOR_WRITE",9))
    {
        flash_write_in_buff1(data1,data2,data3);
    }
    if (!strncmp(command,"NOR_READ",8))
    {
        flash_read_buff1(data1,data2);
    }
    if (!strncmp(command,"BACKUP_TO_USB",13))
    {
        backup_to_usb(data1,data2);
    }
    if (!strncmp(command,"RESTORE_FROM_USB",16))
    {
        restore_from_usb(data1,data2);
    }
    if (!strncmp(command,"RESTORE_FILE_FROM_USB",21))
    {
        restore_file_from_usb(data1,data2);
    }
    if (!strncmp(command,"INIT_DB",7))
    {
        Init_Api_test();
    }
    if (!strncmp(command,"LIST_SECTORS",12))
    {
        ListSectors_debug(&myApi,data1);

    }
		if (!strncmp(command,"APPEND_DB",9))
    {
        Append_debug(&myApi,data1);

    }
    free (strUdpParser);
}
void Append_debug(struct_myapi* spMyApiStruct, char* strType)
{
    unsigned char u8Type;
    if (!strncmp(strType,"TOTALS",6))
    {
        u8Type =enum_totals;
    }
    if (!strncmp(strType,"VARIABLES",8))
    {
        u8Type =enum_variables;
    }
    if (!strncmp(strType,"PERIOD",6))
    {
        u8Type =enum_periodic_log;
    }
    if (!strncmp(strType,"SYS_LOG",6))
    {
        u8Type =enum_system_log;
    }

    AppendRecord(&myApi,u8Type);
}
void ListSectors_debug(struct_myapi* spMyApiStruct, char* strType)
{
    ListSectors(&myApi,data1);			//main api
    sendbuf = udp_get_buf (200);
    sprintf(sendbuf,"base address =%x \r\n no of cyclic sectors=%x\r\n no of backups = %x\r\n size of sector = %x\r\n",
            area_details.base_address,area_details.no_of_cyclic_sectors,area_details.no_of_backups,area_details.size_of_sector);
    udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
}
