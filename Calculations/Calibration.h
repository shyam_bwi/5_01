/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Calibration.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : 14th February Thursday, 2013  <Feb 14, 2013>
* @date Last Modified  : 14th February Thursday, 2013  <Feb 14, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __CALIBRATION_H
#define __CALIBRATION_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define DATA_LOG_FLAG_RESET        0
#define CALIB_DATA_LOG_WRITE       1
#define SET_ZERO_DATA_LOG_WRITE    2
#define LEN_ZER_DATA_LOG_WRITE     3
#define LENGTH_DATA_LOG_WRITE      4

#define TSTWT_PLNT_CNCT_WRITE      1
#define MAT_TST_PLNT_CNCT_WRITE    2
#define MAN_CAL_PLNT_CNCT_WRITE    3

#define PROGRESS_BAR_RESET         0
#define PROGRESS_BAR_MOVE          1
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
extern BOOL Flash_Write_In_progress ; //Added on 27 April 2016  
extern BOOL enable_clr_wt_write_flag; //Added on 27 April 2016  
extern BOOL clr_wt_cmd_recd; //Added on 27 April 2016  
extern BOOL clear_weight_in_process; //Added by DK on 4 May2016

/* Character variables section */
extern U8 enable_clr_wt_write_counter; //Added on 27 April 2016
extern U8 Calib_Cancel_flag;

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
{
      unsigned char Cal_status_flag;
      unsigned char Set_progress_bar;
      unsigned char Start_flag;
      unsigned char Calib_log_flag;
      unsigned char BeltLenCal_Comp;
	    unsigned char Plant_connect_write_flag;
}CALIB_STRUCT_DEF;
extern CALIB_STRUCT_DEF Calib_struct;

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void test_weight_cal (void);
extern void material_test_cal (void);
extern void digital_cal (void);
extern void dynamic_zero_cal (void);
extern void static_zero_cal (void);
extern void clear_weight (void);
extern void LengthAndZero(void);
extern void AutoBeltLength(void);
extern void Restore_Calib_Value_Upon_Cancel_Calibration_Process(void);

#endif /*__CALIBRATION_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
