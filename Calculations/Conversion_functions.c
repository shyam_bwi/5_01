/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Conversion_functions.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 12, 2012>
* @date Last Modified  : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <RTL.h>
#include "Screen_global_ex.h"
#include "Conversion_functions.h"
#include "Sensor_board_data_process.h"
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "Screen_data_enum.h"
#include "File_update.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: float ConvWeightUnit(char *PresentUnit, char *ConvertedUnit, float WeightToConv)
* @returns    returns      : converted weight
* @param      arg1         : Pointer to the current unit
*             arg2         : Pointer to the new unit
*             arg3         : Weight to be converted
* @author                  : Viren Dobariya
* @date       date created : 15th April 2013
* @brief      Description  : Converts weight from the current unit to the new unit and returns
*                          : the new value.
* @note       Notes        : None
*****************************************************************************/
//float ConvWeightUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, float WeightToConv)
double ConvWeightUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double WeightToConv)
{
  //float NewWeight;
	double NewWeight;

      if(ConvertedUnit != PresentUnit)
      {
        //Conversion of certified scale Weight to Belt scale units
        if (ConvertedUnit  == TONS) //Short Ton
        {
            if (PresentUnit == LONG_TON) //Long Ton
            {
              NewWeight = 1.12*WeightToConv;			//Suvrat - ok
            }
            else if (PresentUnit == LBS) //lbs
            {
              NewWeight = 0.0005*WeightToConv;		//Suvrat - ok
            }
            else if (PresentUnit == TONNE) //Tonne
            {
              //NewWeight = 1.10231*WeightToConv;		//Suvrat
							NewWeight = 1.102311311*WeightToConv;
            }
            else if (PresentUnit == KG) //Kg
            {
              //NewWeight = 0.00110231*WeightToConv;		//Suvrat
							NewWeight = 0.001102311*WeightToConv;
            }
        }
        else if (ConvertedUnit == LONG_TON) //Long Ton
        {
            if (PresentUnit == TONS) //Short Ton
            {
              NewWeight = 0.892857143*WeightToConv;				//Suvrat - ok
            }
            else if (PresentUnit == LBS ) //lbs
            {
              NewWeight = 0.000446428571*WeightToConv;		//Suvrat - ok
            }
            else if ( PresentUnit == TONNE) //Tonne
            {
              NewWeight = 0.984206528 *WeightToConv;		//Suvrat - ok
            }
            else if (PresentUnit == KG) //Kg
            {
              NewWeight = 0.000984206528*WeightToConv;		//Suvrat - ok
            }
        }
        else if (ConvertedUnit == LBS) //lbs
        {
            if (PresentUnit == TONS) //Short Ton
            {
              NewWeight = 2000.0 *WeightToConv;			//Suvrat - ok
            }
            else if ( PresentUnit == LONG_TON)//Long Ton
            {
              NewWeight = 2240.0 *WeightToConv;			//Suvrat - ok
            }
            else if ( PresentUnit == TONNE) //Tonne
            {
              //NewWeight = 2204.62 *WeightToConv;		//Suvrat
							NewWeight = 2204.622621849 *WeightToConv;
            }
            else if ( PresentUnit == KG) //Kg
            {
              //NewWeight = 2.20462 *WeightToConv;		//Suvrat
							NewWeight = 2.204622622 *WeightToConv;
            }
        }
        else if (ConvertedUnit == TONNE) //Tonne
        {
            if ( PresentUnit == TONS) //Short Ton
            {
              //NewWeight = 0.907185*WeightToConv;		//Suvrat
							NewWeight = 0.90718474*WeightToConv;
            }
            else if ( PresentUnit == LONG_TON)//Long Ton
            {
              //NewWeight = 1.01604691 *WeightToConv;			//Suvrat
							NewWeight = 1.016046909 *WeightToConv;
            }
            else if ( PresentUnit == LBS) //lbs
            {
              NewWeight = 0.000453592*WeightToConv;		//Suvrat - ok
            }
            else if ( PresentUnit == KG) //Kg
            {
              NewWeight = 0.001 *WeightToConv;				//Suvrat - ok
            }
        }
        else if ( ConvertedUnit == KG ) //Kg
        {
          if ( PresentUnit == TONS) //Short Ton
          {
            //NewWeight = 907.185*WeightToConv;			//Suvrat
						NewWeight = 907.18474*WeightToConv;
          }
          else if ( PresentUnit == LONG_TON)//Long Ton
          {
            //NewWeight = 1016.05 *WeightToConv;			//Suvrat
						NewWeight = 1016.0469088 *WeightToConv;
          }
          else if ( PresentUnit == LBS) //lbs
          {
            //NewWeight = 0.453592*WeightToConv;		//Suvrat
						NewWeight = 0.45359237*WeightToConv;
          }
          else if ( PresentUnit == TONNE) //Tonne
          {
            NewWeight = 1000*WeightToConv;		//Suvrat - ok
          }
        }
      }
      else
      {
        NewWeight = WeightToConv;
      }

  return (NewWeight);

}

/*****************************************************************************
* @note       Function name: void Weight_unit_conv_func (void)
* @returns    returns      : Conversion factor according to the weight unit selected
* @param      arg1         : none
* @author                  : Anagha Basole
* @date       date created : 29th April 2013
* @brief      Description  : Returns the weight conversion factor according to
*                          : the user selection of weight unit from pounds
* @note       Notes        : None
*****************************************************************************/
void Weight_unit_conv_func (void)
{
    if (Scale_setup_var.Weight_unit == TONS) //short ton
    {
      Calculation_struct.Weight_conv_factor = LBS_TO_SHORT_TON_CONV_FACTOR;
			periodic_data.Accum_weight_unit_val = 101;
    }
    else if ( Scale_setup_var.Weight_unit == LONG_TON) //long ton
    {
      Calculation_struct.Weight_conv_factor = LBS_TO_LONG_TON_CONV_FACTOR;
			periodic_data.Accum_weight_unit_val = 102;
    }
    else if ( Scale_setup_var.Weight_unit == LBS) //lbs
    {
      Calculation_struct.Weight_conv_factor = 1.0;
			periodic_data.Accum_weight_unit_val = 100;
    }
    else if ( Scale_setup_var.Weight_unit == TONNE) //metric ton or tonne
    {
      Calculation_struct.Weight_conv_factor = LBS_TO_TONNE_CONV_FACTOR;
			periodic_data.Accum_weight_unit_val = 104;
    }
    else if (Scale_setup_var.Weight_unit == KG) //kg
    {
      Calculation_struct.Weight_conv_factor = LBS_TO_KG_CONV_FACTOR;
			periodic_data.Accum_weight_unit_val = 103;
    }

    //convert the test weight to pounds if in kg. no conversion is required if unit is in pounds
    if (Calibration_var.Test_zero_weight_unit == KG) //kg
    {
      Calculation_struct.Test_weight_conv_factor = KG_TO_LBS_CONV_FACTOR;
      Calculation_struct.Zero_value_conv_factor = LBS_TO_KG_CONV_FACTOR;
    }
    else
    {
      Calculation_struct.Test_weight_conv_factor = 1.0;
      Calculation_struct.Zero_value_conv_factor = 1.0;
    }

    return;
}

/*****************************************************************************
* @note       Function name: float Load_cell_capacity_calc (void)
* @returns    returns      : load cell capacity in pounds
* @param      arg1         : none
* @author                  : Anagha Basole
* @date       date created : 31st August 2012
* @brief      Description  : Calculate the load cell capacity in pounds depending
*                          : the user selection
* @note       Notes        : None
*****************************************************************************/
float Load_cell_capacity_calc (void)
{
    float load_cell_capacity = 0;

    if (Scale_setup_var.Load_cell_size == Screen23_str2) //45kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_45KG;
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str5) //50kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_50KG;
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str8) //100kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_100KG;
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str11) //150kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_150KG;
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str14) //200kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_200KG;
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str17) //350kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_350KG;
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str20) //500kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_500KG;
    }
    else if ( Scale_setup_var.Load_cell_size == Screen23_str23) //1000kg
    {
      load_cell_capacity = KG_TO_LBS_CONV_FACTOR * LOADCELL_CAPACITY_1000KG_LBS;
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str26) //1000lbs
    {
      load_cell_capacity = LOADCELL_CAPACITY_1000KG_LBS;
    }
    else if ( Scale_setup_var.Load_cell_size == Screen23_str29) //custom loadcell
    {
      if(Scale_setup_var.Custom_LC_unit == Screen23121_str5) //custom loadcell capacity in kg
      {
        load_cell_capacity = KG_TO_LBS_CONV_FACTOR * Scale_setup_var.Custom_LC_capacity;
      }
      else
      {
        load_cell_capacity = Scale_setup_var.Custom_LC_capacity;
      }
    }

    return(load_cell_capacity);
}

/*****************************************************************************
* @note       Function name: void Distance_unit_conversion(void)
* @returns    returns      : none
* @param      arg1         : none
* @author                  : Anagha Basole
* @date       date created : 8th May 2013
* @brief      Description  : Sets the conversion factors depending on english or metric
* @note       Notes        : None
*****************************************************************************/
void Distance_unit_conversion(void)
{
    if(Scale_setup_var.Distance_unit == Screen24_str5)//If Metric = distance_unit
    {
      Calculation_struct.Idler_dist_conv_factor = METER_TO_INCH_CONV_FACTOR;  //convert meters to inches(english unit) for calculation-idler center distance
      Calculation_struct.Wheel_diameter_conv_factor = CENTIMETER_TO_INCH_CONV_FACTOR;  //convert centimeters to inches(english unit) for calculation-wheel diameter
      Calculation_struct.Speed_conv_factor = INCH_TO_METER_CONV_FACTOR; //convert inches to meters(metric) for speed calculation
			periodic_data.Speed_unit_val = 201;
      Calculation_struct.Belt_length_conv_factor = METER_TO_INCH_CONV_FACTOR;  //convert meters to inches(english unit) for length calibration
      Calculation_struct.Belt_len_conv_fact_display = INCH_TO_METER_CONV_FACTOR;//convert inches to meters for display
    }
    else//If English = distance_unit
    {
      Calculation_struct.Idler_dist_conv_factor = 1; //already in inches (english unit)
      Calculation_struct.Wheel_diameter_conv_factor = 1; //already in inches (english unit)
      Calculation_struct.Speed_conv_factor = INCH_TO_FEET_CONV_FACTOR; //convert inches to feet for speed calculation, since speed unit is feet
			periodic_data.Speed_unit_val = 200;
      Calculation_struct.Belt_length_conv_factor = FEET_TO_INCH_CONV_FACTOR; //convert from feet to inches for belt length calibration
      Calculation_struct.Belt_len_conv_fact_display = INCH_TO_FEET_CONV_FACTOR;//convert inches to meters for display
    }

    if ( Scale_setup_var.Rate_time_unit == Screen26_str2) //hours
    {
      Calculation_struct.Rate_conv_factor = SEC_TO_MIN_TO_HOUR_CONV_FACTOR; //convert min to hours
			periodic_data.Rate_unit_val = 300;
    }
    else
    {
      Calculation_struct.Rate_conv_factor = 1;
			periodic_data.Rate_unit_val = 301;			
    }
    return;
}

/*****************************************************************************
* @note       Function name: void Convert_rate_user_values(char * prev_rate_unit)
* @returns    returns      : none
* @param      arg1         : Pointer to the previous rate unit
* @author                  : Anagha Basole
* @date       date created : 8th May 2013
* @brief      Description  : Converts all the current rates to the new selected unit(time conversion)
* @note       Notes        : None
*****************************************************************************/
void Convert_rate_user_values(unsigned int prev_rate_unit)
{
    float rate_conv_factor = 0;

    if(prev_rate_unit != Scale_setup_var.Rate_time_unit)
    {
        if (Scale_setup_var.Rate_time_unit == Screen26_str2) //hours
        {
          rate_conv_factor = SEC_TO_MIN_TO_HOUR_CONV_FACTOR; //convert min to hours
        }
        else
        {
          rate_conv_factor = 1 / SEC_TO_MIN_TO_HOUR_CONV_FACTOR; //convert hour to min
        }
    }
    Setup_device_var.MinMax_Rate_Setpoint_1 = Setup_device_var.MinMax_Rate_Setpoint_1 * rate_conv_factor;
    Setup_device_var.MinMax_Rate_Setpoint_2 = Setup_device_var.MinMax_Rate_Setpoint_2 * rate_conv_factor;
    Setup_device_var.MinMax_Rate_Setpoint_3 = Setup_device_var.MinMax_Rate_Setpoint_3 * rate_conv_factor;
    Setup_device_var.MinMax_Rate_Setpoint_4 = Setup_device_var.MinMax_Rate_Setpoint_4 * rate_conv_factor;
    Setup_device_var.MinMax_Rate_Setpoint_5 = Setup_device_var.MinMax_Rate_Setpoint_5 * rate_conv_factor;
    Setup_device_var.MinMax_Rate_Setpoint_6 = Setup_device_var.MinMax_Rate_Setpoint_6 * rate_conv_factor;
    Setup_device_var.Target_rate = Setup_device_var.Target_rate * rate_conv_factor;
    //Sens_brd_param.Rate_input_4_20ma = Sens_brd_param.Rate_input_4_20ma * rate_conv_factor;
    //Calculation_struct.Rate_from_int_board = Calculation_struct.Rate_from_int_board * rate_conv_factor;
    Setup_device_var.Feeder_Max_Rate = Setup_device_var.Feeder_Max_Rate * rate_conv_factor;

		Init_FIFO(Rate_disp_FIFO);
		
    return;
}

/*****************************************************************************
* @note       Function name: void Convert_user_values(char * prev_dist_unit)
* @returns    returns      : none
* @param      arg1         : Pointer to the previous distance unit
* @author                  : Anagha Basole
* @date       date created : 8th May 2013
* @brief      Description  : Converts all the current distances and weights to the new selected unit
* @note       Notes        : None
*****************************************************************************/
void Convert_user_values(unsigned int prev_dist_unit)
{
    //If English = distance_unit
    if(Scale_setup_var.Distance_unit != prev_dist_unit)
    {
       if(Scale_setup_var.Distance_unit == Screen24_str5)//If Metric = distance_unit
       {
         //convert all values from english to metric
         Scale_setup_var.Idler_distanceA = Scale_setup_var.Idler_distanceA * INCH_TO_METER_CONV_FACTOR; //inches to meters
         Scale_setup_var.Idler_distanceB = Scale_setup_var.Idler_distanceB * INCH_TO_METER_CONV_FACTOR; //inches to meters
         Scale_setup_var.Idler_distanceC = Scale_setup_var.Idler_distanceC * INCH_TO_METER_CONV_FACTOR; //inches to meters
         Scale_setup_var.Idler_distanceD = Scale_setup_var.Idler_distanceD * INCH_TO_METER_CONV_FACTOR; //inches to meters
         Scale_setup_var.Idler_distanceE = Scale_setup_var.Idler_distanceE * INCH_TO_METER_CONV_FACTOR; //inches to meters
         Scale_setup_var.Wheel_diameter = Scale_setup_var.Wheel_diameter * INCH_TO_CENTIMETER_CONV_FACTOR; //inches to cm
         Scale_setup_var.User_Belt_Speed = Scale_setup_var.User_Belt_Speed * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Calibration_var.New_belt_length = Calibration_var.New_belt_length * FEET_TO_METER_CONV_FACTOR;//feet to meter
         Setup_device_var.Preload_Distance = Setup_device_var.Preload_Distance * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Setup_device_var.MinMax_Speed_Setpoint_1 = Setup_device_var.MinMax_Speed_Setpoint_1 * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Setup_device_var.MinMax_Speed_Setpoint_2 = Setup_device_var.MinMax_Speed_Setpoint_2 * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Setup_device_var.MinMax_Speed_Setpoint_3 = Setup_device_var.MinMax_Speed_Setpoint_3 * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Setup_device_var.MinMax_Speed_Setpoint_4 = Setup_device_var.MinMax_Speed_Setpoint_4 * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Setup_device_var.MinMax_Speed_Setpoint_5 = Setup_device_var.MinMax_Speed_Setpoint_5 * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Setup_device_var.MinMax_Speed_Setpoint_6 = Setup_device_var.MinMax_Speed_Setpoint_6 * FEET_TO_METER_CONV_FACTOR;//feet to meters
         Setup_device_var.Min_Belt_Speed = Setup_device_var.Min_Belt_Speed * FEET_TO_METER_CONV_FACTOR;//feet to meter
         Setup_device_var.Empty_Belt_Speed = Setup_device_var.Empty_Belt_Speed * FEET_TO_METER_CONV_FACTOR;//feet to meter
         Calibration_var.New_zero_value = Calibration_var.New_zero_value * LBS_TO_KG_CONV_FACTOR; //pounds to kg
         Calibration_var.Test_weight = Calibration_var.Test_weight * LBS_TO_KG_CONV_FACTOR; //pounds to kg
       }
       else //distance unit = English
       {
         //convert all values from metric to english
         Scale_setup_var.Idler_distanceA = Scale_setup_var.Idler_distanceA * METER_TO_INCH_CONV_FACTOR; //meters to inches
         Scale_setup_var.Idler_distanceB = Scale_setup_var.Idler_distanceB * METER_TO_INCH_CONV_FACTOR; //meters to inches
         Scale_setup_var.Idler_distanceC = Scale_setup_var.Idler_distanceC * METER_TO_INCH_CONV_FACTOR; //meters to inches
         Scale_setup_var.Idler_distanceD = Scale_setup_var.Idler_distanceD * METER_TO_INCH_CONV_FACTOR; //meters to inches
         Scale_setup_var.Idler_distanceE = Scale_setup_var.Idler_distanceE * METER_TO_INCH_CONV_FACTOR; //meters to inches
         Scale_setup_var.Wheel_diameter = Scale_setup_var.Wheel_diameter * CENTIMETER_TO_INCH_CONV_FACTOR; //cm to inches
         Scale_setup_var.User_Belt_Speed = Scale_setup_var.User_Belt_Speed * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Calibration_var.New_belt_length = Calibration_var.New_belt_length * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.Preload_Distance = Setup_device_var.Preload_Distance * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.MinMax_Speed_Setpoint_1 = Setup_device_var.MinMax_Speed_Setpoint_1 * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.MinMax_Speed_Setpoint_2 = Setup_device_var.MinMax_Speed_Setpoint_2 * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.MinMax_Speed_Setpoint_3 = Setup_device_var.MinMax_Speed_Setpoint_3 * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.MinMax_Speed_Setpoint_4 = Setup_device_var.MinMax_Speed_Setpoint_4 * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.MinMax_Speed_Setpoint_5 = Setup_device_var.MinMax_Speed_Setpoint_5 * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.MinMax_Speed_Setpoint_6 = Setup_device_var.MinMax_Speed_Setpoint_6 * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.Min_Belt_Speed = Setup_device_var.Min_Belt_Speed * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Setup_device_var.Empty_Belt_Speed = Setup_device_var.Empty_Belt_Speed * METER_TO_FEET_CONV_FACTOR;//meter to feet
         Calibration_var.New_zero_value = Calibration_var.New_zero_value * KG_TO_LBS_CONV_FACTOR; //kg to pounds
         Calibration_var.Test_weight = Calibration_var.Test_weight * KG_TO_LBS_CONV_FACTOR; //kg to pounds
       }
			 //Added PVK-21 Mar 2016 - Called Idler_center_dist_calc(); to calculate Idler distance Immediately
			 Idler_center_dist_calc();
    }
    return;
}

/*****************************************************************************
* @note       Function name: void Convert_user_weight_values(unsigned int prev_weight_unit)
* @returns    returns      : none
* @param      arg1         : Pointer to the previous weight unit
* @author                  : Anagha Basole
* @date       date created : 8th May 2013
* @brief      Description  : Converts all the current weights to the new selected unit
* @note       Notes        : None
*****************************************************************************/
void Convert_user_weight_values(unsigned int prev_weight_unit)
{
    double conversion_factor = 0, converted_value = 0;

    //calculate the conversion factor depending on the previous unit and current selection
    //100.0 is just used as a dummy value to obtain a conversion factor since the conversion factor
    //will be zero if the weight input to the function is zero. Since the conversion function
    //returns the converted value, it is divided by 100.0 to get the conversion factor
    converted_value = ConvWeightUnit(prev_weight_unit, Scale_setup_var.Weight_unit, 100.0);
    conversion_factor = converted_value / 100.0;
    Setup_device_var.Cutoff_1 = Setup_device_var.Cutoff_1 * conversion_factor;
    Setup_device_var.Cutoff_2 = Setup_device_var.Cutoff_2 * conversion_factor;
    Setup_device_var.Cutoff_3 = Setup_device_var.Cutoff_3 * conversion_factor;
    Setup_device_var.Cutoff_4 = Setup_device_var.Cutoff_4 * conversion_factor;
    Setup_device_var.Cutoff_5 = Setup_device_var.Cutoff_5 * conversion_factor;
    Setup_device_var.Cutoff_6 = Setup_device_var.Cutoff_6 * conversion_factor;
    Setup_device_var.Cutoff_7 = Setup_device_var.Cutoff_7 * conversion_factor;
    Setup_device_var.Cutoff_8 = Setup_device_var.Cutoff_8 * conversion_factor;
    Setup_device_var.Load_weight_1 = Setup_device_var.Load_weight_1 * conversion_factor;
    Setup_device_var.Load_weight_2 = Setup_device_var.Load_weight_2 * conversion_factor;
    Setup_device_var.Load_weight_3 = Setup_device_var.Load_weight_3 * conversion_factor;
    Setup_device_var.Load_weight_4 = Setup_device_var.Load_weight_4 * conversion_factor;
    Setup_device_var.Load_weight_5 = Setup_device_var.Load_weight_5 * conversion_factor;
    Setup_device_var.Load_weight_6 = Setup_device_var.Load_weight_6 * conversion_factor;
    Setup_device_var.Load_weight_7 = Setup_device_var.Load_weight_7 * conversion_factor;
    Setup_device_var.Load_weight_8 = Setup_device_var.Load_weight_8 * conversion_factor;

    //rate calculations
    Setup_device_var.MinMax_Rate_Setpoint_1 = Setup_device_var.MinMax_Rate_Setpoint_1 * conversion_factor;
    Setup_device_var.MinMax_Rate_Setpoint_2 = Setup_device_var.MinMax_Rate_Setpoint_2 * conversion_factor;
    Setup_device_var.MinMax_Rate_Setpoint_3 = Setup_device_var.MinMax_Rate_Setpoint_3 * conversion_factor;
    Setup_device_var.MinMax_Rate_Setpoint_4 = Setup_device_var.MinMax_Rate_Setpoint_4 * conversion_factor;
    Setup_device_var.MinMax_Rate_Setpoint_5 = Setup_device_var.MinMax_Rate_Setpoint_5 * conversion_factor;
    Setup_device_var.MinMax_Rate_Setpoint_6 = Setup_device_var.MinMax_Rate_Setpoint_6 * conversion_factor;
    Setup_device_var.Target_rate = Setup_device_var.Target_rate * conversion_factor;
    //Sens_brd_param.Rate_input_4_20ma = Sens_brd_param.Rate_input_4_20ma * conversion_factor;
    //Calculation_struct.Rate_from_int_board = Calculation_struct.Rate_from_int_board * conversion_factor;
    Setup_device_var.Feeder_Max_Rate = Setup_device_var.Feeder_Max_Rate * conversion_factor;

    return;
}


/*****************************************************************************
* @note       Function name: void Convert_totals_weight_values(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Megha Gaikwad
* @date       date created : 21st Sep 2015
* @brief      Description  : Converts all total weights to the selected unit or in LBS
														 for internal calculation purpose.
* @note       Notes        : Totals_var struct is only for internal use.
														 This function should get called only after firmware upgrade, 
														 power on and soft reset condition.
														 In run time, all totals caluculation and conversion is done 
														 in Calculation() function.

*****************************************************************************/
void Convert_totals_weight_values(void)
{
	if(Totals_var.Weight_unit != Scale_setup_var.Weight_unit)
	{
		//Daily total weight
		Totals_var.daily_Total_weight = ConvWeightUnit(Totals_var.Weight_unit, Scale_setup_var.Weight_unit, Rprts_diag_var.daily_Total_weight);						
		Totals_var.daily_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Totals_var.daily_Total_weight);			// For internal calculation purpose			
		//Weekly total weight
		Totals_var.weekly_Total_weight = ConvWeightUnit(Totals_var.Weight_unit, Scale_setup_var.Weight_unit, Rprts_diag_var.weekly_Total_weight);
		Totals_var.weekly_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Totals_var.weekly_Total_weight);			// For internal calculation purpose
		//Monthly total weight
		Totals_var.monthly_Total_weight = ConvWeightUnit(Totals_var.Weight_unit, Scale_setup_var.Weight_unit, Rprts_diag_var.monthly_Total_weight);
		Totals_var.monthly_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Totals_var.monthly_Total_weight);			// For internal calculation purpose
		//Yearly total weight
		Totals_var.yearly_Total_weight = ConvWeightUnit(Totals_var.Weight_unit, Scale_setup_var.Weight_unit, Rprts_diag_var.yearly_Total_weight);
		Totals_var.yearly_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Totals_var.yearly_Total_weight);			// For internal calculation purpose
		//Job total weight
		Totals_var.Job_Total = ConvWeightUnit(Totals_var.Weight_unit, Scale_setup_var.Weight_unit, Rprts_diag_var.Job_Total);
		Totals_var.Job_Total = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Totals_var.Job_Total);			// For internal calculation purpose
		//Master total weight
		Totals_var.Master_total = ConvWeightUnit(Totals_var.Weight_unit, Scale_setup_var.Weight_unit, Rprts_diag_var.Master_total);
		Totals_var.Master_total = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Totals_var.Master_total);			// For internal calculation purpose
	}
	else
	{
		Totals_var.daily_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Rprts_diag_var.daily_Total_weight);						
		Totals_var.weekly_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Rprts_diag_var.weekly_Total_weight);
		Totals_var.monthly_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Rprts_diag_var.monthly_Total_weight);
		Totals_var.yearly_Total_weight = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Rprts_diag_var.yearly_Total_weight);
		Totals_var.Job_Total = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Rprts_diag_var.Job_Total);
		Totals_var.Master_total = ConvWeightUnit(Scale_setup_var.Weight_unit, LBS, Rprts_diag_var.Master_total);
	}
	
}

/*****************************************************************************
* @note       Function name: double ConvDistUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double DistToConv)
* @returns    returns      : converted weight
* @param      arg1         : Pointer to the current unit
*             arg2         : Pointer to the new unit
*             arg3         : Weight to be converted
* @author                  : Suvrat Joshi
* @date       date created : 9th Feb 2016
* @brief      Description  : Converts distance from the current unit to the new unit and returns
*                          : the new value.
* @note       Notes        : None
*****************************************************************************/
double ConvDistUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double DistToConv)
{
	double NewDist;
	if(ConvertedUnit != PresentUnit)
	{
		//Conversion of certified scale distance to Belt scale units
		if (ConvertedUnit == INCHES) //inches
		{
				if (PresentUnit == METERS) //meters
				{
					NewDist = METER_TO_INCH_CONV_FACTOR * DistToConv;	
				}
				else if (PresentUnit == CENTIMETERS) //cm
				{
					NewDist = CENTIMETER_TO_INCH_CONV_FACTOR * DistToConv;
				}
				else if (PresentUnit == FEET) //ft
				{
					NewDist = FEET_TO_INCH_CONV_FACTOR * DistToConv;
				}
		}
		else if (ConvertedUnit == FEET) //ft
		{
				if (PresentUnit == METERS) //meters
				{
					NewDist = METER_TO_FEET_CONV_FACTOR * DistToConv;
				}
				else if (PresentUnit == INCHES) //inches
				{
					NewDist = INCH_TO_FEET_CONV_FACTOR * DistToConv;
				}
		}
		else if (ConvertedUnit == METERS) //meters
		{
				if (PresentUnit == INCHES) //inches
				{
					NewDist = INCH_TO_METER_CONV_FACTOR * DistToConv;
				}
				else if ( PresentUnit == FEET)//ft
				{
					NewDist = FEET_TO_METER_CONV_FACTOR * DistToConv;
				}
		}
		else if (ConvertedUnit == CENTIMETERS) //cm
		{
				if ( PresentUnit == INCHES) //inches
				{
					NewDist = INCH_TO_CENTIMETER_CONV_FACTOR * DistToConv;
				}
		}
	}
	else
	{
		NewDist = DistToConv;
	}

	return (NewDist);
}

/*****************************************************************************
* @note       Function name: double ConvRateUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double RateToConv)
* @returns    returns      : none
* @param      arg1         : Pointer to the previous rate unit
* @author                  : Suvrat Joshi
* @date       date created : 9th Feb 2016
* @brief      Description  : Converts the current rate to the new selected unit(time conversion)
* @note       Notes        : None
*****************************************************************************/
//void Convert_rate_user_values(unsigned int prev_rate_unit)
double ConvRateUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double RateToConv)
{
    //float rate_conv_factor = 0;
		double NewRate = 0.0;

    if(PresentUnit != ConvertedUnit)
    {
        if (ConvertedUnit == Screen26_str2) //hours
        {
          NewRate = RateToConv * SEC_TO_MIN_TO_HOUR_CONV_FACTOR; //convert min to hours
        }
        else
        {
          NewRate = RateToConv / SEC_TO_MIN_TO_HOUR_CONV_FACTOR; //convert hour to min
        }
    }
		else
		{
			NewRate = RateToConv;
		}
		
		return (NewRate);
}

/*****************************************************************************
* @note       Function name: U32 ConvIPAddr(IP_STRUCT *IP_addr)
* @returns    returns      : none
* @param      arg1         : Pointer to the IP structure
* @author                  : Suvrat Joshi
* @date       date created : 2nd Mar 2016
* @brief      Description  : Converts the IP address to aggregate address
* @note       Notes        : None
*****************************************************************************/
//void Convert_rate_user_values(unsigned int prev_rate_unit)
unsigned int ConvIPAddr(IP_STRUCT *IP_addr)
{
		unsigned int aggregate_ip = 0;
		aggregate_ip = ((IP_addr->Addr1)*1000) + ((IP_addr->Addr2)*100) + ((IP_addr->Addr3)*10) + ((IP_addr->Addr4)*1);

		return (aggregate_ip);
}
/*****************************************************************************
* End of file
*****************************************************************************/
