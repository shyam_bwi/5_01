/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Log_report_data_calculate.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : 13th February Wednesday, 2013  <Feb 13, 2013>
* @date Last Modified  : 13th February Wednesday, 2013  <Feb 13, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "Log_report_data_calculate.h"
#include "Sensor_board_data_process.h"
#include "Ext_flash_high_level.h"
#include "Rtos_main.h"
#include "Screen_data_enum.h"
#include "Plant_connect.h"
#include "I2C_driver.h"
#include "Calibration.h"
#include "RTOS_main.h"
#include "RW_FLASH.h"
#ifdef FILE_SYS
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define FIRST_TIME_DETECTED     1
#define START_TIME_STORED       2
#define DATA_TO_BE_POPULATED    3
#define NEXT_TIME_DETECTED			4

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL File_Write_In_Progress = 0;
/* Character variables section */
U8 U8GlobalMBCount=0;
U16 nooftimes,nooftimestemp;
double daily_weight_at_1;
U8 g_Qfront = 0;
U8 g_Qrear = 0;
U8 g_Q_Status = (U8)Q_EMPTY;
U8 g_Rec_idx_mb = 0;
U8 g_Q_addition_prog = 0;
U16 g_last_periodic_id = 0;
U8 new_Q_item_added = 0;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void rec_Q_init (void);
void rec_Q_add_item (PERIODIC_LOG_STRUCT *periodic_data_item);
void write_missed_log_recs (void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name: void error_status (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : April 25, 2013
* @brief      Description  : Checks the error flag and writes the log and report
* @note       Notes        : None
*****************************************************************************/
void error_status (void)
{
    char disp_value[ARRAY_SIZE];
    U8 status = 0;

    /******************************* Check Warning Flags************************************/
    if ((Flags_struct.Warning_flags & LOW_SPEED_WARNING) &&
        (Flags_struct.Err_Wrn_log_written & LOW_SPEED_WRN_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "W001");
        strncpy(error_data_log.Error_desc, Strings[Warning_1].Text, strlen(Strings[Warning_1].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~LOW_SPEED_WRN_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_LOW_BELT_SPEED_ERR_FLAG);
    }

    if ((Flags_struct.Warning_flags & HIGH_SPEED_WARNING) &&
        (Flags_struct.Err_Wrn_log_written & HIGH_SPEED_WRN_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "W002");
        strncpy(error_data_log.Error_desc, Strings[Warning_2].Text, strlen(Strings[Warning_2].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~HIGH_SPEED_WRN_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_HIGH_BELT_SPEED_ERR_FLAG);
    }

    if ((Flags_struct.Warning_flags & ZERO_CAL_CHANGE_WARNING) &&
        (Flags_struct.Err_Wrn_log_written & ZERO_CAL_CHANGE_WRN_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "W003");
        strncpy(error_data_log.Error_desc, Strings[Warning_3].Text, strlen(Strings[Warning_3].Text)+1);

        //if logging is enabled
        if ( Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~ZERO_CAL_CHANGE_WRN_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_ZERO_CAL_ERR_FLAG);
    }

    if ((Flags_struct.Warning_flags & SEN_BRD_COMM_WARNING) &&
        (Flags_struct.Err_Wrn_log_written & SEN_BRD_COMM_WRN_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "W004");
        strncpy(error_data_log.Error_desc, Strings[Warning_4].Text, strlen(Strings[Warning_4].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~SEN_BRD_COMM_WRN_LOGGED;
        }
        report_data_populate(error_report);
    }

    if ((Flags_struct.Warning_flags & SCALE_COMM_WARNING) &&
        (Flags_struct.Err_Wrn_log_written & SCALE_COMM_WRN_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        //copy till the string 'XXX.' and replace with the actual value
        strcpy(error_data_log.Error_code, "W005");
        strncpy(error_data_log.Error_desc, Strings[Warning_5].Text, 25);
        sprintf(disp_value, "%d", Setup_device_var.Network_Addr);
        strcat(error_data_log.Error_desc, disp_value);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~SCALE_COMM_WRN_LOGGED;
        }
        report_data_populate(error_report);
    }

    if ((Flags_struct.Warning_flags & LOW_RATE_WARNING) &&
        (Flags_struct.Err_Wrn_log_written & LOW_RATE_WRN_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "W006");
        strncpy(error_data_log.Error_desc, Strings[Warning_6].Text, strlen(Strings[Warning_6].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~LOW_RATE_WRN_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_LOW_RATE_ERR_FLAG);
    }

    if ((Flags_struct.Warning_flags & HIGH_RATE_WARNING) &&
        (Flags_struct.Err_Wrn_log_written & HIGH_RATE_WRN_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "W007");
        strncpy(error_data_log.Error_desc, Strings[Warning_7].Text, strlen(Strings[Warning_7].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~HIGH_RATE_WRN_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_HIGH_RATE_ERR_FLAG);
    }

    /******************************* Check Error Flags************************************/
    // If belt speed is zero
    if ((Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR) &&
        (Flags_struct.Err_Wrn_log_written & ZERO_BELT_SPEED_ERR_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "E001");
        strncpy(error_data_log.Error_desc, Strings[Error_1].Text, strlen(Strings[Error_1].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~ZERO_BELT_SPEED_ERR_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_BELT_SPEED_ERR_FLAG);
    }

    //if load cell data has an error
    if ((Flags_struct.Error_flags & LOAD_CELL_DATA_ERR) &&
        (Flags_struct.Err_Wrn_log_written & LOAD_CELL_DATA_ERR_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "E002");
        strncpy(error_data_log.Error_desc, Strings[Error_2].Text, strlen(Strings[Error_2].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~LOAD_CELL_DATA_ERR_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_LOAD_CELL_ERR_FLAG);
    }

    //Angle sensor error
    if ((Flags_struct.Error_flags & ANGLE_SENSOR_ERR) &&
        (Flags_struct.Err_Wrn_log_written & ANGLE_SENSOR_ERR_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "E003");
        strncpy(error_data_log.Error_desc, Strings[Error_3].Text, strlen(Strings[Error_3].Text)+1);

        //if logging is enabled
        if ( Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~ANGLE_SENSOR_ERR_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_ANGLE_SENSOR_ERR_FLAG);
    }

    //sensor board communication error
    if ((Flags_struct.Error_flags & SEN_BRD_COMM_ERR) &&
        (Flags_struct.Err_Wrn_log_written & SEN_BRD_COMM_ERR_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "E004");
        strncpy(error_data_log.Error_desc, Strings[Error_4].Text, strlen(Strings[Error_4].Text)+1);

        //if logging is enabled
        if ( Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~SEN_BRD_COMM_ERR_LOGGED;
        }
        report_data_populate(error_report);
				populate_error_code_plant_connect_var(PLANT_CONNECT_SEN_BRD_COM_ERR_FLAG);
    }

    //scale communication error
    if ((Flags_struct.Error_flags & SCALE_COMM_ERR) &&
        (Flags_struct.Err_Wrn_log_written & SCALE_COMM_ERR_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "E005");
        //copy till the string 'XXX.' and replace with the actual value
        strncpy(error_data_log.Error_desc, Strings[Error_5].Text, 19);
        sprintf(disp_value, "%d", Setup_device_var.Network_Addr);
        strcat(error_data_log.Error_desc, disp_value);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~SCALE_COMM_ERR_LOGGED;
        }
        report_data_populate(error_report);
    }

    //negative rate
    if ((Flags_struct.Error_flags & NEGATIVE_RATE_ERR) &&
        (Flags_struct.Err_Wrn_log_written & NEGATIVE_RATE_ERR_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "E006");
        //copy till the string 'XXX.' and replace with the actual value
        strncpy(error_data_log.Error_desc, Strings[Error_6].Text, 27);
        sprintf(disp_value, "%f", Admin_var.Negative_rate_time);
        strcat(error_data_log.Error_desc, disp_value);
        strcat(error_data_log.Error_desc, " seconds");

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~NEGATIVE_RATE_ERR_LOGGED;
        }
        report_data_populate(error_report);
    }

    //IO board communication error
    if ((Flags_struct.Error_flags & IO_BRD_COMM_ERR) &&
        (Flags_struct.Err_Wrn_log_written & IO_BRD_COMM_ERR_LOGGED))
    {
        memset(error_data_log.Error_code, 0, sizeof(error_data_log.Error_code));
        memset(error_data_log.Error_desc, 0, sizeof(error_data_log.Error_desc));

        strcpy(error_data_log.Error_code, "E007");
        strncpy(error_data_log.Error_desc, Strings[Error_7].Text, strlen(Strings[Error_7].Text)+1);

        //if logging is enabled
        if (Setup_device_var.Log_err_data == Screen431_str2)
        {
          status = log_data_populate(error_log);
        }
        else
        {
          status = 1;
        }
        if (status == 1)
        {
          Flags_struct.Err_Wrn_log_written &= ~IO_BRD_COMM_ERR_LOGGED;
        }
        report_data_populate(error_report);
    }

    return;
}


/*****************************************************************************
* @note       Function name: U8 log_data_populate (U8 log_type)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : Feb 13, 2013
* @brief      Description  :
* @note       Notes        : None
*****************************************************************************/
int ConvDistUnit_int(unsigned int PresentUnit, unsigned int ConvertedUnit, int DistToConv)
{
	int NewDist;
	if(ConvertedUnit != PresentUnit)
	{
		//Conversion of certified scale distance to Belt scale units
		if (ConvertedUnit == INCHES) //inches
		{
				if (PresentUnit == METERS) //meters
				{
					NewDist = METER_TO_INCH_CONV_FACTOR * DistToConv;	
				}
				else if (PresentUnit == CENTIMETERS) //cm
				{
					NewDist = CENTIMETER_TO_INCH_CONV_FACTOR * DistToConv;
				}
				else if (PresentUnit == FEET) //ft
				{
					NewDist = FEET_TO_INCH_CONV_FACTOR * DistToConv;
				}
		}
		else if (ConvertedUnit == FEET) //ft
		{
				if (PresentUnit == METERS) //meters
				{
					NewDist = METER_TO_FEET_CONV_FACTOR * DistToConv;
				}
				else if (PresentUnit == INCHES) //inches
				{
					NewDist = INCH_TO_FEET_CONV_FACTOR * DistToConv;
				}
		}
		else if (ConvertedUnit == METERS) //meters
		{
				if (PresentUnit == INCHES) //inches
				{
					NewDist = INCH_TO_METER_CONV_FACTOR * DistToConv;
				}
				else if ( PresentUnit == FEET)//ft
				{
					NewDist = FEET_TO_METER_CONV_FACTOR * DistToConv;
				}
		}
		else if (ConvertedUnit == CENTIMETERS) //cm
		{
				if ( PresentUnit == INCHES) //inches
				{
					NewDist = INCH_TO_CENTIMETER_CONV_FACTOR * DistToConv;
				}
		}
	}
	else
	{
		NewDist = DistToConv;
	}

	return (NewDist);
}

int ConvWeightUnit_int(unsigned int PresentUnit, unsigned int ConvertedUnit, int WeightToConv)
{
  //float NewWeight;
	int NewWeight;

      if(ConvertedUnit != PresentUnit)
      {
        //Conversion of certified scale Weight to Belt scale units
        if (ConvertedUnit  == TONS) //Short Ton
        {
            if (PresentUnit == LONG_TON) //Long Ton
            {
              NewWeight = 1.12*WeightToConv;			//Suvrat - ok
            }
            else if (PresentUnit == LBS) //lbs
            {
              NewWeight = 0.0005*WeightToConv;		//Suvrat - ok
            }
            else if (PresentUnit == TONNE) //Tonne
            {
              //NewWeight = 1.10231*WeightToConv;		//Suvrat
							NewWeight = 1.102311311*WeightToConv;
            }
            else if (PresentUnit == KG) //Kg
            {
              //NewWeight = 0.00110231*WeightToConv;		//Suvrat
							NewWeight = 0.001102311*WeightToConv;
            }
        }
        else if (ConvertedUnit == LONG_TON) //Long Ton
        {
            if (PresentUnit == TONS) //Short Ton
            {
              NewWeight = 0.892857143*WeightToConv;				//Suvrat - ok
            }
            else if (PresentUnit == LBS ) //lbs
            {
              NewWeight = 0.000446428571*WeightToConv;		//Suvrat - ok
            }
            else if ( PresentUnit == TONNE) //Tonne
            {
              NewWeight = 0.984206528 *WeightToConv;		//Suvrat - ok
            }
            else if (PresentUnit == KG) //Kg
            {
              NewWeight = 0.000984206528*WeightToConv;		//Suvrat - ok
            }
        }
        else if (ConvertedUnit == LBS) //lbs
        {
            if (PresentUnit == TONS) //Short Ton
            {
              NewWeight = 2000.0 *WeightToConv;			//Suvrat - ok
            }
            else if ( PresentUnit == LONG_TON)//Long Ton
            {
              NewWeight = 2240.0 *WeightToConv;			//Suvrat - ok
            }
            else if ( PresentUnit == TONNE) //Tonne
            {
              //NewWeight = 2204.62 *WeightToConv;		//Suvrat
							NewWeight = 2204.622621849 *WeightToConv;
            }
            else if ( PresentUnit == KG) //Kg
            {
              //NewWeight = 2.20462 *WeightToConv;		//Suvrat
							NewWeight = 2.204622622 *WeightToConv;
            }
        }
        else if (ConvertedUnit == TONNE) //Tonne
        {
            if ( PresentUnit == TONS) //Short Ton
            {
              //NewWeight = 0.907185*WeightToConv;		//Suvrat
							NewWeight = 0.90718474*WeightToConv;
            }
            else if ( PresentUnit == LONG_TON)//Long Ton
            {
              //NewWeight = 1.01604691 *WeightToConv;			//Suvrat
							NewWeight = 1.016046909 *WeightToConv;
            }
            else if ( PresentUnit == LBS) //lbs
            {
              NewWeight = 0.000453592*WeightToConv;		//Suvrat - ok
            }
            else if ( PresentUnit == KG) //Kg
            {
              NewWeight = 0.001 *WeightToConv;				//Suvrat - ok
            }
        }
        else if ( ConvertedUnit == KG ) //Kg
        {
          if ( PresentUnit == TONS) //Short Ton
          {
            //NewWeight = 907.185*WeightToConv;			//Suvrat
						NewWeight = 907.18474*WeightToConv;
          }
          else if ( PresentUnit == LONG_TON)//Long Ton
          {
            //NewWeight = 1016.05 *WeightToConv;			//Suvrat
						NewWeight = 1016.0469088 *WeightToConv;
          }
          else if ( PresentUnit == LBS) //lbs
          {
            //NewWeight = 0.453592*WeightToConv;		//Suvrat
						NewWeight = 0.45359237*WeightToConv;
          }
          else if ( PresentUnit == TONNE) //Tonne
          {
            NewWeight = 1000*WeightToConv;		//Suvrat - ok
          }
        }
      }
      else
      {
        NewWeight = WeightToConv;
      }

  return (NewWeight);

}



int first_len_LOG=0;
U8 log_data_populate (U8 log_type)
{
	/*
	#ifdef DEBUG_PORT
	char buf[30];
	#endif
	*/

#if 1
	
  U8 Flash_log_write_status;
	static U8 Last_PER_function_code = 0;

	File_Write_In_Progress = 1;
	Flash_log_write_status = 1;
	
  //populate the structure
  switch(log_type)
  {
    /*if the log is a periodic data log*/
    case periodic_log:
		case periodic_log_start:
		case periodic_log_stop:
		case periodic_log_stop_at_start:
		case total_clr_wt_log:
		case calibration_finished:
		case periodic_tst_wt_log:
		case periodic_dig_log:
		case periodic_mat_log:
		case periodic_zer_log:
		case periodic_len_log:
		case calibration_cancel:
		case daily_wt_clr_log:	/*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/
			if(log_type == periodic_len_log)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "LEN");
				periodic_data.Function_code = PER_LEN;						
			}	
			else if(log_type == calibration_cancel)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "CAN");
				periodic_data.Function_code = PER_CAN;		
				first_len_LOG =0 ;				
			}				
			else if(log_type == periodic_zer_log)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "ZER");
				periodic_data.Function_code = PER_ZER;						
			}	
			else if(log_type == periodic_mat_log)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "MAT");
				periodic_data.Function_code = PER_MAT;						
			}	
			else if(log_type == periodic_dig_log)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "DIG");
				periodic_data.Function_code = PER_DIG;						
			}
			else if(log_type == periodic_tst_wt_log)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "TST");
				periodic_data.Function_code = PER_TST;						
			}
			else if(log_type == calibration_finished)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "FIN");
				periodic_data.Function_code = PER_FIN;	
first_len_LOG =0 ;				
			}
			else if(log_type == total_clr_wt_log)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "TCW");
				periodic_data.Function_code = PER_TCW;						
			}
			//Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix
			else if(log_type == daily_wt_clr_log)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "DCW");
				periodic_data.Function_code = PER_DCW;				
			}
			else if(log_type == periodic_log_stop || log_type == periodic_log_stop_at_start)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "FDT");
				periodic_data.Function_code = PER_FDT;
				periodic_data.daily_weight =  Plant_connect_var.Accumulated_weight_before_clr;		
				periodic_data.Periodic_hdr.Date.Year = (U16) (U16)fdt_hdr.Date.Year;
				periodic_data.Periodic_hdr.Date.Mon = (U8) fdt_hdr.Date.Mon;
				periodic_data.Periodic_hdr.Date.Day = (U8) fdt_hdr.Date.Day;
				//--Commented on 12-Oct-2015 for correct FDT entry in 12/24 hr mode when scale is on
				//--and also for correct FDT entry in 12/24 hr mode when scale is off over midnight
				/*if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
				{				
						periodic_data.Periodic_hdr.Time.Hr = (U8) fdt_hdr.Time.Hr;
				}
				else if(Admin_var.Current_Time_Format == TWELVE_HR)
				{
					if(Admin_var.AM_PM == AM_TIME_FORMAT)
					{
						if(Current_time.RTC_Hour == 12)
							periodic_data.Periodic_hdr.Time.Hr = 0;
						else
							periodic_data.Periodic_hdr.Time.Hr = (U8) fdt_hdr.Time.Hr;	
					}
					else
					{
						if(Current_time.RTC_Hour == 12)
							periodic_data.Periodic_hdr.Time.Hr = 12;
						else
							periodic_data.Periodic_hdr.Time.Hr = (U8) fdt_hdr.Time.Hr+12;				
					//}
				}	*/
				periodic_data.Periodic_hdr.Time.Hr = (U8) fdt_hdr.Time.Hr;
				periodic_data.Periodic_hdr.Time.Min = (U8) fdt_hdr.Time.Min;
				periodic_data.Periodic_hdr.Time.Sec = (U8) fdt_hdr.Time.Sec;
			}
			else if(log_type == periodic_log_start)
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "IDT");					
				periodic_data.Function_code = PER_IDT;			
				periodic_data.daily_weight = 0;
				periodic_data.Periodic_hdr.Time.Sec = 0;
				
				periodic_data.Periodic_hdr.Time.Min = 0;
				periodic_data.Periodic_hdr.Time.Hr = 0;
			}
			else
			{
				strcpy((char *)periodic_data.Periodic_hdr.Data_type, "DAT");
				periodic_data.Function_code = PER_DAT;
				//daily_weight_at_1 = periodic_data.daily_weight;
			}
			
			if(new_Q_item_added)
			{
				periodic_data.Periodic_hdr.Id = g_last_periodic_id;					//SKS check here
				clear_data_log.Clr_data_hdr.Id = g_last_periodic_id;
				new_Q_item_added = 0;
			}
			
			periodic_data.Periodic_hdr.Id++;
			LPC_RTC->GPREG2=	periodic_data.Periodic_hdr.Id;
			clear_data_log.Clr_data_hdr.Id++;
//		  LPC_RTC->GPREG2 = periodic_data.Periodic_hdr.Id;	//SKS : The record number should never roll over 		
		//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
		//  periodic_data.Accum_weight = log_total_weight;
			
		if(log_type!= periodic_log_stop_at_start && log_type!= periodic_log_stop && log_type!= periodic_log_start)
		{
     //Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
			periodic_data.Periodic_hdr.Date.Year = (U16) Current_time.RTC_Year;
      periodic_data.Periodic_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
      periodic_data.Periodic_hdr.Date.Day = (U8) Current_time.RTC_Mday;
      
			//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
			/*periodic_data.Periodic_hdr.Date.Year = (U16) log_current_time.RTC_Year;
      periodic_data.Periodic_hdr.Date.Mon = (U8) log_current_time.RTC_Mon;
      periodic_data.Periodic_hdr.Date.Day = (U8) log_current_time.RTC_Mday;
      */
			
			if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
			{				
				//Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log	
				periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
				//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
			//	 periodic_data.Periodic_hdr.Time.Hr = (U8) log_current_time.RTC_Hour;
			}
			else if(Admin_var.Current_Time_Format == TWELVE_HR)
			{
				if(Admin_var.AM_PM == AM_TIME_FORMAT)
				{
					if(Current_time.RTC_Hour == 12)
						periodic_data.Periodic_hdr.Time.Hr = 0;
					else
						//Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
						periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;	
					//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
						//periodic_data.Periodic_hdr.Time.Hr = (U8) log_current_time.RTC_Hour;
				}
				else
				{
					if(Current_time.RTC_Hour == 12)
						periodic_data.Periodic_hdr.Time.Hr = 12;
					else
				//Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
					periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour+12;	
					//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
				//	periodic_data.Periodic_hdr.Time.Hr = (U8) log_current_time.RTC_Hour+12;				
				}
			}	
      //periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
      //Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
			periodic_data.Periodic_hdr.Time.Min = (U8) Current_time.RTC_Min;
			//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
			//periodic_data.Periodic_hdr.Time.Min = (U8) log_current_time.RTC_Min;
			if(log_type != periodic_log_start)
				//Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
				periodic_data.Periodic_hdr.Time.Sec = (U8) Current_time.RTC_Sec;
			//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
			//	periodic_data.Periodic_hdr.Time.Sec = (U8) log_current_time.RTC_Sec;
		}
    #ifdef CALCULATION
		 //Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
      //periodic_data.Accum_weight = Calculation_struct.Total_weight;
      periodic_data.Rate = Calculation_struct.Rate;
      periodic_data.Inst_load_pcnt = Calculation_struct.Load_cell_perc;
      periodic_data.Inst_conv_speed = Calculation_struct.Belt_speed;
    #endif //#ifdef CALCULATION
      strcpy(periodic_data.Accum_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      strcpy(periodic_data.Rate_unit, Strings[Scale_setup_var.Rate_time_unit].Text);
      periodic_data.Inst_conv_angle = Calculation_struct.Angle;
      strcpy(periodic_data.Speed_unit, Strings[Scale_setup_var.Speed_unit].Text);
      periodic_data.Report_type = periodic_log;
			//Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
		  periodic_data.Accum_weight = Calculation_struct.Total_weight;
			//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
		  //periodic_data.Accum_weight = log_total_weight;
		
		  if(log_type != periodic_log_start)
			{
				//Commented by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
				periodic_data.daily_weight = Rprts_diag_var.daily_Total_weight;
				//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
				//periodic_data.daily_weight = log_daily_total_weight;
			}	
			//SKS 10/10/2017 -->
			periodic_data.master_total_pound = Totals_var.Master_total*100;
			if(log_type == periodic_log_start)Totals_var.daily_Total_weight=0; 
			periodic_data.daily_total_pound = Totals_var.daily_Total_weight*100;
			periodic_data.zero_number_pound = Calculation_struct.Zero_weight*100;///Calculation_struct.Weight_conv_factor;
			//SKS 12/10/2017
			// 
			//
			
			//
			//need to convert units here
			//	
				// get length unit
			
			// get time unit

			periodic_data.Speed_Feet_Min = Calculation_struct.Belt_speed;
			periodic_data.Speed_Feet_Min = ConvDistUnit_int(Scale_setup_var.Belt_length_unit,FEET,periodic_data.Speed_Feet_Min);
			periodic_data.Speed_Feet_Min= periodic_data.Speed_Feet_Min*60*100;///Calculation_struct.Speed_conv_factor;
			//Scale_setup_var.Speed_unit
			
			if (Scale_setup_var.Rate_time_unit != Screen26_str2)
			periodic_data.Inst_Rate_Pounds_Hour= (Calculation_struct.Rate*60);///;
			else
				periodic_data.Inst_Rate_Pounds_Hour= (Calculation_struct.Rate);///;
			periodic_data.Inst_Rate_Pounds_Hour = ConvWeightUnit_int(Scale_setup_var.Weight_unit,LBS ,periodic_data.Inst_Rate_Pounds_Hour);
			periodic_data.Inst_Rate_Pounds_Hour = periodic_data.Inst_Rate_Pounds_Hour*100;
			//<--
			
			rec_Q_add_item(&periodic_data);		//Add new record to the queue
			if(U8GlobalMBCount < REC_QUEUE_SIZE)
			{
				U8GlobalMBCount++;
			}

      Flags_struct.Log_flags |= PERIODIC_LOG_WR_FLAG;
			nooftimestemp = nooftimes ;
			
			/*Added on 23-Nov-2015 for BS-120 fix*/
			//Save last PER function code to check TCW /DCW event for clear weight.
			Last_PER_function_code = periodic_data.Function_code;
      break;

    /*if the log is a set zero function data log*/
    case set_zero_log:
      strcpy((char *)set_zero_data_log.Set_zero_hdr.Data_type, "ZER");
      calib_data_log.Cal_data_hdr.Id++;
			set_zero_data_log.Set_zero_hdr.Id++;
      calibration_rprt.Cal_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
      calibration_rprt.Cal_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
      calibration_rprt.Cal_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
			if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
			{				
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
			}
			else if(Admin_var.Current_Time_Format == TWELVE_HR)
			{
				if(Admin_var.AM_PM == AM_TIME_FORMAT)
				{
					if(Current_time.RTC_Hour == 12)
						calibration_rprt.Cal_rprt_header.Time.Hr = 0;
					else
						calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;	
				}
				else
				{
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour+12;				
				}
			}		
      //Added by PVK 4 Apr 2016			
			calibration_rprt.Cal_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
			calibration_rprt.Cal_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;
			
      set_zero_data_log.Prev_set_zero_val = Calibration_var.Old_zero_value;
      set_zero_data_log.New_set_zero_val = Calibration_var.New_zero_value;
		  set_zero_data_log.Set_zero_accum_weight = Calibration_var.New_zero_value;
      strcpy(set_zero_data_log.Accum_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      set_zero_data_log.belt_length = Calibration_var.New_belt_length;
      strcpy(set_zero_data_log.belt_length_unit, Strings[Scale_setup_var.Belt_length_unit].Text);
      set_zero_data_log.Inst_conv_angle = Calculation_struct.Angle;
      set_zero_data_log.Report_type = set_zero_log;
      Flags_struct.Log_flags |= SET_ZERO_LOG_WR_FLAG;
      #ifdef CALCULATION
      #endif //#ifdef CALCULATION
      break;

    /*if the log is a clear data function data log*/
    case clear_log:
      strcpy((char *)clear_data_log.Clr_data_hdr.Data_type, "CLR");
			periodic_data.Function_code = PER_CLR;
    
		  clear_data_log.Clr_data_hdr.Id++;
			periodic_data.Periodic_hdr.Id++;
       LPC_RTC->GPREG2 = periodic_data.Periodic_hdr.Id;	//SKS : The record number should never roll over
		periodic_data.Periodic_hdr.Date.Year = (U16) Current_time.RTC_Year;
      periodic_data.Periodic_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
      periodic_data.Periodic_hdr.Date.Day = (U8) Current_time.RTC_Mday;
			if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
			{				
					periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
			}
			else if(Admin_var.Current_Time_Format == TWELVE_HR)
			{
				if(Admin_var.AM_PM == AM_TIME_FORMAT)
				{
					if(Current_time.RTC_Hour == 12)
						periodic_data.Periodic_hdr.Time.Hr = 0;
					else
						periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;	
				}
				else
				{
					if(Current_time.RTC_Hour == 12)
						periodic_data.Periodic_hdr.Time.Hr = 12;
					else					
						periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour+12;				
				}
			}	
      //periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
      periodic_data.Periodic_hdr.Time.Min = (U8) Current_time.RTC_Min;
      periodic_data.Periodic_hdr.Time.Sec = (U8) Current_time.RTC_Sec;
    #ifdef CALCULATION
      strcpy(clear_data_log.Accum_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      periodic_data.Rate = Calculation_struct.Rate;
      periodic_data.Inst_load_pcnt = Calculation_struct.Load_cell_perc;
      periodic_data.Inst_conv_speed = Calculation_struct.Belt_speed;
    #endif //#ifdef CALCULATION
      strcpy(periodic_data.Accum_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      strcpy(periodic_data.Rate_unit, Strings[Scale_setup_var.Rate_time_unit].Text);
      periodic_data.Inst_conv_angle = Calculation_struct.Angle;
      strcpy(periodic_data.Speed_unit, Strings[Scale_setup_var.Speed_unit].Text);
			/*Added on 23-Nov-2015 for BS-120 fix*/
			if(Last_PER_function_code == PER_TCW)
			{
				periodic_data.Accum_weight = 0.0;
				periodic_data.daily_weight = Rprts_diag_var.daily_Total_weight;	
			}
			else if(Last_PER_function_code == PER_DCW)
			{
				periodic_data.Accum_weight = Calculation_struct.Total_weight;
				periodic_data.daily_weight = 0.0;		
			}		
			//periodic_data.daily_weight =  daily_weight_at_1 + ((nooftimes/20.0) * (Calculation_struct.Rate_for_display/3600.0));
      clear_data_log.Report_type = clear_log;
      Flags_struct.Log_flags |= CLEAR_LOG_WR_FLAG;
			periodic_data.master_total_pound = Totals_var.Master_total;
			rec_Q_add_item(&periodic_data);		//Add new record to the queue
			if(U8GlobalMBCount < REC_QUEUE_SIZE)
			{
				U8GlobalMBCount++;
			}

      break;

    /*if the log is a calibration function data log*/
    case calib_log:
		case digital_log:
		case mat_test_log:
		case tst_wt_log:			
      strcpy((char *)calib_data_log.Cal_data_hdr.Data_type, "CAL");
      calib_data_log.Cal_data_hdr.Id++;
			set_zero_data_log.Set_zero_hdr.Id++;

//       calib_data_log.Cal_data_hdr.Date.Year = (U16) Current_time.RTC_Year;
//       calib_data_log.Cal_data_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
//       calib_data_log.Cal_data_hdr.Date.Day = (U8) Current_time.RTC_Mday;
//       calib_data_log.Cal_data_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
//       calib_data_log.Cal_data_hdr.Time.Min = (U8) Current_time.RTC_Min;
//       calib_data_log.Cal_data_hdr.Time.Sec = (U8) Current_time.RTC_Sec;
      calibration_rprt.Cal_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
      calibration_rprt.Cal_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
      calibration_rprt.Cal_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
      //calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
			if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
			{				
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
			}
			else if(Admin_var.Current_Time_Format == TWELVE_HR)
			{
				if(Admin_var.AM_PM == AM_TIME_FORMAT)
				{
					if(Current_time.RTC_Hour == 12)
						calibration_rprt.Cal_rprt_header.Time.Hr = 0;
					else
						calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;	
				}
				else
				{
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour+12;				
				}
			}			
		
      calib_data_log.Accum_weight = Calculation_struct.Total_weight;
      strcpy(calib_data_log.Accum_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      calib_data_log.Cert_weight = Calibration_var.Cert_scale_weight;
      strcpy(calib_data_log.Cert_weight_unit, Strings[Calibration_var.Cert_scale_unit].Text);
      calib_data_log.Prev_span = Calibration_var.old_span_value;
      calib_data_log.New_span = Calibration_var.new_span_value;
      calib_data_log.Idler_spacing1 = Scale_setup_var.Idler_distanceA;
      calib_data_log.Idler_spacing2 = Scale_setup_var.Idler_distanceB;
      calib_data_log.Idler_spacing3 = Scale_setup_var.Idler_distanceC;
      calib_data_log.Cal_type = calib_log;
      calib_data_log.Report_type = calib_log;
      Flags_struct.Log_flags |= CALIB_LOG_WR_FLAG;
      break;

    /*if the log is a error function data log*/
    case error_log:
      strcpy((char *)error_data_log.Err_data_hdr.Data_type, "ERR");
      error_data_log.Err_data_hdr.Id++;
      error_data_log.Err_data_hdr.Date.Year = (U16) Current_time.RTC_Year;
      error_data_log.Err_data_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
      error_data_log.Err_data_hdr.Date.Day = (U8) Current_time.RTC_Mday;
      error_data_log.Err_data_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
      error_data_log.Err_data_hdr.Time.Min = (U8) Current_time.RTC_Min;
      error_data_log.Err_data_hdr.Time.Sec = (U8) Current_time.RTC_Sec;
      //strcpy((char *)error_data_log.Error_code, "001");
      //strcpy((char *)error_data_log.Error_desc, "ABCD");
      error_data_log.Report_type = error_log;
      Flags_struct.Log_flags |= ERROR_LOG_WR_FLAG;
      break;

    /*if the log is a length function data log*/
    case length_log:
      strcpy((char *)length_cal_data_log.Len_data_hdr.Data_type, "LEN");
      length_cal_data_log.Len_data_hdr.Id++;
      calibration_rprt.Cal_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
      calibration_rprt.Cal_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
      calibration_rprt.Cal_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
      //calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
			if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
			{				
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
			}
			else if(Admin_var.Current_Time_Format == TWELVE_HR)
			{
				if(Admin_var.AM_PM == AM_TIME_FORMAT)
				{
					if(Current_time.RTC_Hour == 12)
						calibration_rprt.Cal_rprt_header.Time.Hr = 0;
					else
						calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;	
				}
				else
				{
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour+12;				
				}
			}			
		
//       length_cal_data_log.Len_data_hdr.Date.Year = (U16) Current_time.RTC_Year;
//       length_cal_data_log.Len_data_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
//       length_cal_data_log.Len_data_hdr.Date.Day = (U8) Current_time.RTC_Mday;
//       length_cal_data_log.Len_data_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
//       length_cal_data_log.Len_data_hdr.Time.Min = (U8) Current_time.RTC_Min;
//       length_cal_data_log.Len_data_hdr.Time.Sec = (U8) Current_time.RTC_Sec;
      length_cal_data_log.Prev_nos_pulses = length_cal_data_log.New_nos_pulses;
      length_cal_data_log.New_nos_pulses = Sens_brd_param.No_of_pulses;
      length_cal_data_log.Prev_time_duration = length_cal_data_log.New_time_duration;
      length_cal_data_log.New_time_duration = Sens_brd_param.Time;
      length_cal_data_log.Report_type = length_log;
      Flags_struct.Log_flags |= LENGTH_LOG_WR_FLAG;
      break;

    default:
      break;
  }
  //SKS -> BS 239  write all logs to flash.
 // else
	if (
		((log_type ==length_log )&&(first_len_LOG == 0))||
	(log_type !=length_log)
	)
  {
		first_len_LOG =1 ;
// 		//Added by PVK 4 May 2016 for 1 sec logging
//     // if (PERIODIC_LOG_UNIT_SEC != Setup_device_var.Periodic_log_interval) 
// 		// {
// 				Flash_Write_In_progress = 1; //Added on 27 April 2016  		
// 			 write_log_to_flash(log_type);
// 				//Flash_log_write_status = flash_log_store(log_type);
// 				Flash_Write_In_progress = 0; //Added on 27 April 2016  
// 			
// //				Flags_struct.Log_flags |= LOGS_WR_FLAG;
// 				LPC_RTC->GPREG0 = Flags_struct.Log_flags;
// 		// } 
// 			 
//   
	//write the logs to the file or to the flash
  if (Flags_struct.Connection_flags & USB_CON_FLAG)
  {
		//SKS -> BS 239 , write all records in flash
			if (g_restore_prog)
			{
				ferror_cnt++;
			}
			else
			{
				log_file_write(log_type);
			}
  }
	}
	File_Write_In_Progress = 0;
	
  return (Flash_log_write_status);
	
	#endif
}

/*****************************************************************************
* @note       Function name: void report_data_populate (U8 report_type)
* @returns    returns       : none
*                           :
* @param      arg1         : type of report to be written to be written
* @author                   : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description   : Populates the current data in the report structure
* @note       Notes         : None
*****************************************************************************/
void report_data_populate (U8 report_type)
{
  static U16 Belt_speed_flag = FIRST_TIME_DETECTED;
  static U16 Belt_rate_flag = FIRST_TIME_DETECTED;
  U8 report_write_status;

	if((daily_rprt.Dummy_var1 &0x8000) == 0x8000)
	{
		if(daily_rprt.Dummy_var1!= 0x8000)
		{
			Belt_speed_flag = daily_rprt.Dummy_var1&0x000F;
			Belt_rate_flag  = (daily_rprt.Dummy_var1>>8)&0x000F;	
			daily_rprt.Dummy_var1 = 0;
			daily_rprt.Dummy_var1 |= Belt_speed_flag;
			daily_rprt.Dummy_var1 |= (Belt_rate_flag<<8);
		}
		daily_rprt.Dummy_var1 &=0x7FFF;
	}
  //modify the report structures
  switch(report_type)
  {
    /*if the report is calibration report*/
    case calib_report:
		case material_test_report:
		case digital_test_report:
		case set_zero_test_report:
      strcpy((char *)calibration_rprt.Cal_rprt_header.Data_type, "REP");
      strcpy((char *)calibration_rprt.Cal_rprt_header.Report_type, "Cal Report");
      calibration_rprt.Cal_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
      calibration_rprt.Cal_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
      calibration_rprt.Cal_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
      //calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
			if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
			{				
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
			}
			else if(Admin_var.Current_Time_Format == TWELVE_HR)
			{
				if(Admin_var.AM_PM == AM_TIME_FORMAT)
				{
					if(Current_time.RTC_Hour == 12)
						calibration_rprt.Cal_rprt_header.Time.Hr = 0;
					else
						calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;	
				}
				else
				{
					calibration_rprt.Cal_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour+12;				
				}
			}			
      calibration_rprt.Cal_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
      calibration_rprt.Cal_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;
		
		  
		  //if logging is enabled
		  if (Setup_device_var.Log_cal_data == Screen431_str2)
			{
		      length_cal_data_log.Prev_nos_pulses = length_cal_data_log.New_nos_pulses;
          length_cal_data_log.New_nos_pulses = Sens_brd_param.No_of_pulses;
          length_cal_data_log.Prev_time_duration = length_cal_data_log.New_time_duration;
          length_cal_data_log.New_time_duration = Sens_brd_param.Time;  
				
					calib_data_log.Accum_weight = Calculation_struct.Total_weight;
					strcpy(calib_data_log.Accum_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
					calib_data_log.Cert_weight = Calibration_var.Cert_scale_weight;
					strcpy(calib_data_log.Cert_weight_unit, Strings[Calibration_var.Cert_scale_unit].Text);
					calib_data_log.Prev_span = Calibration_var.old_span_value;
					calib_data_log.New_span = Calibration_var.new_span_value;
					calib_data_log.Idler_spacing1 = Scale_setup_var.Idler_distanceB;
					calib_data_log.Idler_spacing2 = Scale_setup_var.Idler_distanceC;
					calib_data_log.Idler_spacing3 = Scale_setup_var.Idler_distanceD;
					calib_data_log.Cal_type = calib_log;
					strcpy(set_zero_data_log.belt_length_unit, Strings[Scale_setup_var.Belt_length_unit].Text);
			}
		
      //if logging is disabled
		  if (Setup_device_var.Log_zero_cal_data != Screen431_str2)
			{
		      set_zero_data_log.Prev_set_zero_val = Calibration_var.Old_zero_value;
					set_zero_data_log.New_set_zero_val = Calibration_var.New_zero_value;
					set_zero_data_log.Set_zero_accum_weight = Calibration_var.New_zero_value;
					strcpy(set_zero_data_log.Accum_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
					set_zero_data_log.Inst_conv_angle = Calculation_struct.Angle;
			}

      //report to be written to the USB drive
      Flags_struct.Report_flags |= CAL_RPRT_WR_FLAG;
      break;

    /*if the report is error report*/
    case error_report:
      strcpy((char *)error_rprt.Err_rprt_header.Data_type, "REP");
      strcpy((char *)error_rprt.Err_rprt_header.Report_type, "Error Report");
      error_rprt.Err_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
      error_rprt.Err_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
      error_rprt.Err_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
      error_rprt.Err_rprt_header.Time.Hr = (U8) Current_time.RTC_Hour;
      error_rprt.Err_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
      error_rprt.Err_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;
      //report to be written to the USB drive
      Flags_struct.Report_flags |= ERR_RPRT_WR_FLAG;
      break;

    /*if the report is daily report*/
    case daily_report:
      strcpy((char *)daily_rprt.freq_rprt_header.Data_type, "REP");
      strcpy((char *)daily_rprt.freq_rprt_header.Report_type, "Daily Report");
      daily_rprt.Total_time = daily_rprt.Run_time + daily_rprt.Down_time;
			if(Belt_rate_flag == START_TIME_STORED)
			{
				daily_rprt.End_load_time.Hr  = 23;
				daily_rprt.End_load_time.Min = 59;
				daily_rprt.End_load_time.Sec = 59;

			}
			else if(Belt_rate_flag == FIRST_TIME_DETECTED)
			{
					daily_rprt.Start_load_time.Hr  = 24;
					daily_rprt.End_load_time.Hr  = 24;
	
			}
			if(Belt_speed_flag == START_TIME_STORED)
			{
				daily_rprt.End_time.Hr  = 23;
				daily_rprt.End_time.Min = 59;
				daily_rprt.End_time.Sec = 59;
  		}
			else if(Belt_speed_flag == FIRST_TIME_DETECTED)
			{
					daily_rprt.Start_time.Hr  = 24;
					daily_rprt.End_time.Hr  = 24;
			}	
    #ifdef CALCULATION
		  if (daily_rprt.Total_time != 0)
			{
				daily_rprt.Average_rate = (Rprts_diag_var.daily_Total_weight/daily_rprt.Total_time)*60.0;
			}
			//daily_rprt.Total_weight = periodic_data_mb[U8GlobalMBCount-1].daily_weight;
    #endif
      strcpy((char *)daily_rprt.Total_rate_unit, Strings[Scale_setup_var.Rate_time_unit].Text);
      strcpy((char *)daily_rprt.Total_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      //report to be written to the USB drive
      Flags_struct.Report_flags |= DLY_RPRT_WR_FLAG;
      break;

    /*if the report is weekly report*/
    case weekly_report:
      strcpy((char *)weekly_rprt.freq_rprt_header.Data_type, "REP");
      strcpy((char *)weekly_rprt.freq_rprt_header.Report_type, "Weekly Report");
      weekly_rprt.Total_time = weekly_rprt.Run_time + weekly_rprt.Down_time;
		
    #ifdef CALCULATION
      if (weekly_rprt.Total_time != 0)
			{
				weekly_rprt.Average_rate = (Rprts_diag_var.weekly_Total_weight /weekly_rprt.Total_time*60.0);
			}
			weekly_rprt.Total_weight = Rprts_diag_var.weekly_Total_weight;
    #endif
      strcpy((char *)weekly_rprt.Total_rate_unit, Strings[Scale_setup_var.Rate_time_unit].Text);
      strcpy((char *)weekly_rprt.Total_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      //report to be written to the USB drive
      Flags_struct.Report_flags |= WKLY_RPRT_WR_FLAG;
      break;

    /*if the report is monthly report*/
    case monthly_report:
      strcpy((char *)monthly_rprt.freq_rprt_header.Data_type, "REP");
      strcpy((char *)monthly_rprt.freq_rprt_header.Report_type, "Monthly Report");
      monthly_rprt.Total_time = monthly_rprt.Run_time + monthly_rprt.Down_time;
			
    #ifdef CALCULATION
      if (monthly_rprt.Total_time != 0)
			{
				monthly_rprt.Average_rate = (Rprts_diag_var.monthly_Total_weight/monthly_rprt.Total_time*60.0);
			}
			monthly_rprt.Total_weight  = Rprts_diag_var.monthly_Total_weight;
    #endif	
      strcpy((char *)monthly_rprt.Total_rate_unit, Strings[Scale_setup_var.Rate_time_unit].Text);
      strcpy((char *)monthly_rprt.Total_weight_unit, Strings[Scale_setup_var.Weight_unit].Text);
      //report to be written to the USB drive
      Flags_struct.Report_flags |= MTHLY_RPRT_WR_FLAG;
      break;

			case populate_regular_reports:
        //Calculate the down time for speed = 0
        if (!(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR) && (Belt_speed_flag == FIRST_TIME_DETECTED))
        {
            Belt_speed_flag = START_TIME_STORED;
						if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
						{				
								daily_rprt.Start_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.Start_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.Start_time.Sec = (U8) Current_time.RTC_Sec;
						}
						else if(Admin_var.Current_Time_Format == TWELVE_HR)
						{
							if(Admin_var.AM_PM == AM_TIME_FORMAT)
							{
								if(Current_time.RTC_Hour == 12)
									daily_rprt.Start_time.Hr = 0;
								else
									daily_rprt.Start_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.Start_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.Start_time.Sec = (U8) Current_time.RTC_Sec;					
							}
							else
							{
								daily_rprt.Start_time.Hr = (U8) Current_time.RTC_Hour+12;
								daily_rprt.Start_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.Start_time.Sec = (U8) Current_time.RTC_Sec;										
							}
						}							
						daily_rprt.Dummy_var1 &= 0xFF00;
						daily_rprt.Dummy_var1 |= Belt_speed_flag;

            //daily_rprt.Cnt_zero_speed = 0;
            //weekly_rprt.Cnt_zero_speed = 0;
            //monthly_rprt.Cnt_zero_speed = 0;
        }
        else if (!(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR) && (Belt_speed_flag == NEXT_TIME_DETECTED))
        {
            Belt_speed_flag = START_TIME_STORED;
					  daily_rprt.Dummy_var1 &= 0xFF00;
						daily_rprt.Dummy_var1 |= Belt_speed_flag;					
        }				
        else if (((Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR)) && (Belt_speed_flag == START_TIME_STORED))
        {
						if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
						{				
								daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
						}
						else if(Admin_var.Current_Time_Format == TWELVE_HR)
						{
							if(Admin_var.AM_PM == AM_TIME_FORMAT)
							{
								if(Current_time.RTC_Hour == 12)
									daily_rprt.End_time.Hr = 0;
								else
									daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;				
							}
							else
							{
								daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour+12;
								daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;								
							}
						}						
						daily_rprt.Dummy_var1 &= 0xFF00;
            Belt_speed_flag = DATA_TO_BE_POPULATED;
						daily_rprt.Dummy_var1 |= Belt_speed_flag;					
        }

				if ((Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR) && (Belt_speed_flag == DATA_TO_BE_POPULATED))
        {
            daily_rprt.Cnt_zero_speed++;
            weekly_rprt.Cnt_zero_speed++;
            monthly_rprt.Cnt_zero_speed++;
					  daily_rprt.Dummy_var1 &= 0xFF00;
					  Belt_speed_flag = NEXT_TIME_DETECTED;
						daily_rprt.Dummy_var1 |= Belt_speed_flag;					
        }				

				
        //Calculate the down time for rate < 0
        //if (!(Flags_struct.Error_flags & NEGATIVE_RATE_ERR) && (Belt_rate_flag == FIRST_TIME_DETECTED))
			/*	if (((Calculation_struct.Rate > Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate > 0 && Admin_var.Zero_rate_status == Screen6171_str5)) && (Belt_rate_flag == FIRST_TIME_DETECTED) && !(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR))*/
				//By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152
				if (((Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate_for_display > 0 && Admin_var.Zero_rate_status == Screen6171_str5)) && (Belt_rate_flag == FIRST_TIME_DETECTED) && !(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR))  //BS-152 fixed by DK on 27 April 2016 
	       {
            Belt_rate_flag = START_TIME_STORED;
						if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
						{				
								daily_rprt.Start_load_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.Start_load_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.Start_load_time.Sec = (U8) Current_time.RTC_Sec;
						}
						else if(Admin_var.Current_Time_Format == TWELVE_HR)
						{
							if(Admin_var.AM_PM == AM_TIME_FORMAT)
							{
								if(Current_time.RTC_Hour == 12)
									daily_rprt.Start_load_time.Hr = 0;
								else
									daily_rprt.Start_load_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.Start_load_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.Start_load_time.Sec = (U8) Current_time.RTC_Sec;		
							}
							else
							{
								daily_rprt.Start_load_time.Hr = (U8) Current_time.RTC_Hour+12;
								daily_rprt.Start_load_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.Start_load_time.Sec = (U8) Current_time.RTC_Sec;				
							}
						}							
					  daily_rprt.Dummy_var1 &= 0x00FF;
						daily_rprt.Dummy_var1 |= (Belt_rate_flag<<8);
        }
				/*else if (((Calculation_struct.Rate > Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate > 0 && Admin_var.Zero_rate_status == Screen6171_str5)) && (Belt_rate_flag == DATA_TO_BE_POPULATED))*/
				//By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152
				else if (((Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate_for_display > 0 && Admin_var.Zero_rate_status == Screen6171_str5)) && (Belt_rate_flag == DATA_TO_BE_POPULATED)) //BS-152 fixed by DK on 27 April 2016 
				{
            Belt_rate_flag = START_TIME_STORED;
					  daily_rprt.Dummy_var1 &= 0x00FF;					
						daily_rprt.Dummy_var1 |= (Belt_rate_flag<<8);					
        }				
        //else if (((Flags_struct.Error_flags & NEGATIVE_RATE_ERR)) && (Belt_rate_flag == START_TIME_STORED))
			/*	else if (((Calculation_struct.Rate < Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate <= 0 && Admin_var.Zero_rate_status == Screen6171_str5)) && (Belt_rate_flag == START_TIME_STORED))*/
				else if (((Calculation_struct.Rate_for_display < Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate_for_display <= 0 && Admin_var.Zero_rate_status == Screen6171_str5)) && (Belt_rate_flag == START_TIME_STORED))  //BS-152 fixed by DK on 27 April 2016 
	      {
					if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
					{				
							daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour;
							daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
							daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
					}
					else if(Admin_var.Current_Time_Format == TWELVE_HR)
					{
						if(Admin_var.AM_PM == AM_TIME_FORMAT)
						{
							if(Current_time.RTC_Hour == 12)
								daily_rprt.End_load_time.Hr = 0;								
							else
								daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour;
							daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
							daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
						}
						else
						{
								daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour+12;
								daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;		
						}			
					}						
            Belt_rate_flag = DATA_TO_BE_POPULATED;
					  daily_rprt.Dummy_var1 &= 0x00FF;					
						daily_rprt.Dummy_var1 |= (Belt_rate_flag<<8);					
        }
        //log the normal run time count
        //if ((!(Flags_struct.Error_flags & NEGATIVE_RATE_ERR)) && (!(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR)))
			/*	if ((Calculation_struct.Rate < Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate <= 0 && Admin_var.Zero_rate_status == Screen6171_str5))*/
        if ((Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate_for_display <= 0 && Admin_var.Zero_rate_status == Screen6171_str5))

		{
            daily_rprt.Down_time++;
            weekly_rprt.Down_time++;
            monthly_rprt.Down_time++;
        }	
				//else if ((Flags_struct.Error_flags & NEGATIVE_RATE_ERR) && (Belt_rate_flag == START_TIME_STORED))
			/*	else if ((Calculation_struct.Rate > Admin_var.Zero_rate_limit && Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate > 0 && Admin_var.Zero_rate_status == Screen6171_str5))*/
        	else if ((Admin_var.Zero_rate_status == Screen6171_str2)
					|| (Calculation_struct.Rate_for_display > 0 && Admin_var.Zero_rate_status == Screen6171_str5))
		{
          daily_rprt.Run_time++;
          weekly_rprt.Run_time++;
          monthly_rprt.Run_time++;
        }
      break;
  }//switch case

  if(report_type != populate_regular_reports)
  {
    //write the reports to the file
    if (Flags_struct.Connection_flags & USB_CON_FLAG)
    //if(con)
    {
        report_write_status = report_file_write(report_type);
        if (report_write_status == 0)  //if there is no error in writing the file then reset the flag
        {
            switch(report_type)
            {
              /*if the report is calibration report*/
              case calib_report:
                Flags_struct.Report_flags &= ~CAL_RPRT_WR_FLAG;
                break;

              /*if the report is calibration report*/
              case error_report:
                Flags_struct.Report_flags &= ~ERR_RPRT_WR_FLAG;
                break;

							#ifdef SHOW_REPORTS
              /*if the report is weekly report*/
              case daily_report:
                Flags_struct.Report_flags &= ~DLY_RPRT_WR_FLAG;
                daily_rprt.Run_time = daily_rprt.Down_time = 0;
                Belt_rate_flag = Belt_speed_flag = FIRST_TIME_DETECTED;
								daily_rprt.Dummy_var1 = 0x0101;
								daily_rprt.Cnt_zero_speed = 0;
                break;

              /*if the report is weekly report*/
              case weekly_report:
                Flags_struct.Report_flags &= ~WKLY_RPRT_WR_FLAG;
                weekly_rprt.Run_time = weekly_rprt.Down_time = 0;
							  weekly_rprt.Cnt_zero_speed = 0;						
                break;

              /*if the report is monthly report*/
              case monthly_report:
                Flags_struct.Report_flags &= ~MTHLY_RPRT_WR_FLAG;
                monthly_rprt.Run_time = monthly_rprt.Down_time = 0;
								monthly_rprt.Cnt_zero_speed = 0;														
                break;
							#endif
							
							default:
								break;
            }
        }
    }
  }
  return;
}

/*****************************************************************************
* @note       Function name : void rec_Q_init (void)
* @returns    returns       : none
* @param      arg1          : none
* @author                   : Suvrat Joshi
* @date       date created  : 24/8/15
* @brief      Description   : Initializes the record queue (circular buffer) global parameters
* @note       Notes         : None
*****************************************************************************/
void rec_Q_init (void)
{
	g_Qfront = 0;
	g_Qrear = 0;
	//With no element in, make front and rear index value pointing to 0th index of buffer
	g_Q_Status = Q_EMPTY;
	last_sent_mb_record.rec_no = 0;
	last_sent_mb_record.idx_no = (REC_QUEUE_SIZE-1);
	U8GlobalMBCount = 0;
	g_Rec_idx_mb = 0;
}

/*****************************************************************************
* @note       Function name : void rec_Q_add_item (PERIODIC_LOG_STRUCT *periodic_data_item)
* @returns    returns       : none
* @param      arg1          : pointer to periodic record
* @author                   : Suvrat Joshi
* @date       date created  : 24/8/15
* @brief      Description   : Adds new record item to the circular queue/buffer
* @note       Notes         : The buffer is used as a circular queue
*****************************************************************************/
void rec_Q_add_item (PERIODIC_LOG_STRUCT *periodic_data_item)
{
	U8 front_idx = g_Qfront;
	U8 rear_idx = g_Qrear;
	
	if(g_Q_Status == Q_EMPTY)
	{
		//When queue is empty, no need to increment front_idx as with single element as well front=rear
		g_Q_addition_prog = 1;
		memcpy(&periodic_data_mb[front_idx],periodic_data_item,sizeof(PERIODIC_LOG_STRUCT));	//Copy data at front of queue
		g_Q_addition_prog = 0;
		g_Q_Status = Q_NOT_EMPTY;
	}
	else
	{
		front_idx++;
		if(front_idx == REC_QUEUE_SIZE)
		{
			front_idx = 0;			//wrap index number to 0
		}
		if(front_idx == rear_idx)
		{
			rear_idx++;
			if(rear_idx == REC_QUEUE_SIZE)
			{
				rear_idx = 0;
			}
		}
		g_Q_addition_prog = 1;
		memcpy(&periodic_data_mb[front_idx],periodic_data_item,sizeof(PERIODIC_LOG_STRUCT));	//Copy data at front of queue
		g_Q_addition_prog = 0;
		g_Qfront = front_idx;		//Update front and rear global index values
		g_Qrear = rear_idx;
	}
	if(g_restore_prog == 1)
	{
		new_Q_item_added = 1;
	}
}

/*****************************************************************************
* @note       Function name : U8 rec_Q_get_item_mb (void)
* @returns    returns       : record index number to be sent
* @param      arg1          : none
* @author                   : Suvrat Joshi
* @date       date created  : 24/8/15
* @brief      Description   : Gets the index number of the record to be sent from queue to modbus
* @note       Notes         : 
*****************************************************************************/
U8 rec_Q_get_item_mb (void)
{
	U16 records_missed = U8GlobalMBCount;
	U8 record_to_send = 0;
		
	if (records_missed < REC_QUEUE_SIZE)
	{
		record_to_send = last_sent_mb_record.idx_no+1;
		if(record_to_send == REC_QUEUE_SIZE)
		{
			record_to_send = 0;
		}
	}
	else
	{
		record_to_send = g_Qrear;
		U8GlobalMBCount = REC_QUEUE_SIZE;
	}
	return record_to_send;
}

/*****************************************************************************
* @note       Function name : U8 rec_Q_get_item_log (void)
* @returns    returns       : record index number to be sent
* @param      arg1          : none
* @author                   : Suvrat Joshi
* @date       date created  : 24/8/15
* @brief      Description   : Gets the index number of the record to be sent from queue to modbus
* @note       Notes         : 
*****************************************************************************/
U8 rec_Q_get_item_log (void)
{
	U16 records_missed = ferror_cnt;
	U8 record_to_send = 0;
	
	if (records_missed < REC_QUEUE_SIZE)
	{
		record_to_send = g_Qfront;
	}
	else
	{
		record_to_send = g_Qrear;
	}
	return record_to_send;
}

/*****************************************************************************
* @note       Function name : void write_missed_log_recs (void)
* @returns    returns       : none
* @param      arg1          : none
* @author                   : Suvrat Joshi
* @date       date created  : 24/8/15
* @brief      Description   : Writes the missed records to the periodic log file
* @note       Notes         : It uses the circular buffer data records to write into the file
*****************************************************************************/
void write_missed_log_recs (void)
{
	FILE *fp;
	U8 u8File_exists = 0;
	char file_name[FILE_NAME_SIZE]={0};
	U8 records_missed = ferror_cnt;
	U8 rec_to_write = 0;
	
	if (records_missed < REC_QUEUE_SIZE)
	{
		if((records_missed-1) <= g_Qfront)
		{
			rec_to_write = g_Qfront - (records_missed - 1);
		}
		else
		{
			rec_to_write = REC_QUEUE_SIZE + (signed char)((signed char)g_Qfront - (signed char)(records_missed - 1));
		}
	}
	else
	{
		rec_to_write = g_Qrear;
		ferror_cnt = REC_QUEUE_SIZE;
	}

	//write the structure in the file
	/*if the log is a periodic data log*/
	sprintf(&file_name[0],"%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",periodic_data_mb[rec_to_write].Periodic_hdr.Date.Year,
	periodic_data_mb[rec_to_write].Periodic_hdr.Date.Mon,periodic_data_mb[rec_to_write].Periodic_hdr.Date.Year,
	periodic_data_mb[rec_to_write].Periodic_hdr.Date.Mon,periodic_data_mb[rec_to_write].Periodic_hdr.Date.Day);
	
	fp = fopen (file_name, "r");		
	if (fp != NULL)
	{
			u8File_exists = 1;
			fclose(fp);
	}
	else
	{
			u8File_exists = 0;
	}
	
	if(u8File_exists == 0)
	{
		fp = fopen (file_name, "w");
	}
	else
	{
		fp = fopen (file_name, "a");
	}
	
	if(fp == NULL)
	{
		//printf("\n Error in opening periodic log file");
		ferror_cnt--;	
		return;
	}
	else
	{
		if(u8File_exists == 0)
		{
			fprintf(fp, "%s,",fheader.File_hdr.Data_type);
			fprintf(fp, " %d,", fheader.File_hdr.Id);
			fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
																				fheader.File_hdr.Date.Mon,
																				fheader.File_hdr.Date.Day);
			fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
																				fheader.File_hdr.Time.Min,
																				fheader.File_hdr.Time.Sec);
			fprintf(fp, " %s,",Admin_var.Scale_Name);
			fprintf(fp, " %s,",Admin_var.Plant_Name);
			fprintf(fp, " %s\r\n",Admin_var.Product_Name);		
			fprintf(fp,"Function, Record #, Date, Time, Accumulated Weight, Weight Unit, Rate, Rate Unit, Load Percent, Angle, Belt speed, Speed unit,Daily weight, Daily Weight Unit\r\n");
		}
		if(new_Q_item_added)
		{
			g_last_periodic_id++;
			periodic_data_mb[rec_to_write].Periodic_hdr.Id = g_last_periodic_id;
			//new_Q_item_added = 0;
		}
		fprintf(fp, " %s,", periodic_data_mb[rec_to_write].Periodic_hdr.Data_type);
		fprintf(fp, " %d,", periodic_data_mb[rec_to_write].Periodic_hdr.Id);
		fprintf(fp, " %d/%d/%d,", periodic_data_mb[rec_to_write].Periodic_hdr.Date.Year,
																			periodic_data_mb[rec_to_write].Periodic_hdr.Date.Mon,
																			periodic_data_mb[rec_to_write].Periodic_hdr.Date.Day);
		fprintf(fp, " %02d:%02d:%02d,", periodic_data_mb[rec_to_write].Periodic_hdr.Time.Hr,
																			periodic_data_mb[rec_to_write].Periodic_hdr.Time.Min,
																			periodic_data_mb[rec_to_write].Periodic_hdr.Time.Sec);
		fprintf(fp, " %lf,", periodic_data_mb[rec_to_write].Accum_weight);
		fprintf(fp, " %s,", periodic_data_mb[rec_to_write].Accum_weight_unit);
		fprintf(fp, " %f,",periodic_data_mb[rec_to_write].Rate);
		fprintf(fp, " %s/%s,",periodic_data_mb[rec_to_write].Accum_weight_unit,periodic_data_mb[rec_to_write].Rate_unit);
		fprintf(fp, " %f,", periodic_data_mb[rec_to_write].Inst_load_pcnt);
		fprintf(fp, " %f,", periodic_data_mb[rec_to_write].Inst_conv_angle);
		fprintf(fp, " %f,", periodic_data_mb[rec_to_write].Inst_conv_speed);
		fprintf(fp, " %s,", Strings[Units_var.Unit_speed].Text);
		fprintf(fp, " %lf,", periodic_data_mb[rec_to_write].daily_weight);			
		fprintf(fp, " %s,\r\n", periodic_data_mb[rec_to_write].Accum_weight_unit);		
		
		ferror_cnt--;	//Data got written successfully so decrement count
		if(ferror(fp))
		{
			//Again some error occured while writing, so increment to maintain the error count and try again
			ferror_cnt++;
			//printf("\n Periodic log file write error");
		}
		
		flash_para_record_flag(SET_FLAG);
		
		if(0 != fclose(fp))
		{
			//printf ("\nFile could not be closed!\n");
			return;
		}
		fflush(stdout);
	}
}

#endif /*FILE_FS*/
/*****************************************************************************
* End of file
*****************************************************************************/
