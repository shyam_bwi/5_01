/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Conversion_functions.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 12, 2012>
* @date Last Modified  : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __CONVERSION_FUNCTIONS_H
#define __CONVERSION_FUNCTIONS_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "Screen_structure.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define LOADCELL_CAPACITY_45KG           45
#define LOADCELL_CAPACITY_50KG           50
#define LOADCELL_CAPACITY_100KG          100
#define LOADCELL_CAPACITY_150KG          150
#define LOADCELL_CAPACITY_200KG          200
#define LOADCELL_CAPACITY_350KG          350
#define LOADCELL_CAPACITY_500KG          500
#define LOADCELL_CAPACITY_1000KG_LBS     1000

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
//extern float ConvWeightUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, float WeightToConv);
extern double ConvWeightUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double WeightToConv);
extern double ConvDistUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double DistToConv);
extern double ConvRateUnit(unsigned int PresentUnit, unsigned int ConvertedUnit, double RateToConv);
extern unsigned int ConvIPAddr(IP_STRUCT *IP_addr);
extern float Load_cell_capacity_calc (void);
extern void Distance_unit_conversion(void);
extern void Weight_unit_conv_func (void);
extern void Convert_user_values(unsigned prev_dist_unit);
extern void Convert_user_weight_values(unsigned int prev_weight_unit);
extern void Convert_rate_user_values(unsigned int prev_rate_unit);
extern void Convert_totals_weight_values(void);
#endif /*__CONVERSION_FUNCTIONS_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
