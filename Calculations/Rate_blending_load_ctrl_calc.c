/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename       : Rate_blending_loadout_calc.c
 * @brief               : Controller Board
 *
 * @author              : Anagha Basole
 *
 * @date Created        : 14th February Thursday, 2013  <Feb 14, 2013>
 * @date Last Modified  : 21st November Thursday, 2013  <Nov 21, 2013>
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal            :
 * @internal            :
 *
 *****************************************************************************/

/*============================================================================
 * Include Header Files
 *===========================================================================*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "Global_ex.h"
#include "Sensor_board_data_process.h"
#include "Rate_blending_load_ctrl_calc.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "soft_fifo.h"
#include "I2C_driver.h"
/*============================================================================
 * Private Macro Definitions
 *===========================================================================*/
/* Defines Section */
#ifdef RATE_BLEND_LOAD_CTRL_CALC

#define SET                        0
#define RESET                      1
/*============================================================================
 * Private Data Types
 *===========================================================================*/

/*============================================================================
 *Public Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
//U32 WindupCounter =0; 
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
RATE_BLEND_LOAD_STRUCT Rate_blend_load;
/*============================================================================
 * Private Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
static U8 prev_sec;
static U8 u8First;
int PrevISUM = 0;
//static U8 g_FD_action_started_flag = 0;
//float prev_belt_speed = 1.0;
//U8 belt_speed_zero_flag = 1;
//U8 start_calc = 1;
//U8 Feed_timer_status=0;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
 * Private Function Prototypes Declarations

 *===========================================================================*/
static void common_calc_rate_blending_load_ctrl(void);
static unsigned int Decrement_Timer(unsigned int *);
/*============================================================================
 * Function Implementation Section
 *===========================================================================*/

/*****************************************************************************
 * @note       Function name: common_calc_rate_blending_load_ctrl
 * @returns    returns      : None
 * @param      arg1         : None
 * @author                  : R. Venkata Krishna Rao
 * @date       date created : Nov 20, 2013
 * @brief      Description  : Performs common math calculations for rate or blending
 *                          :
 * @note       Notes        : None
 *****************************************************************************/
extern PID_integrator_struct Integrator_limits;
int rc_count =0;
void common_calc_rate_blending_load_ctrl(void) {
	float present_rate = 0.0; // present_rate calculated in Time interval equals to number of packets
	float set_rate = 0.0; // set rate calculated in Time interval equals to number of packets
	static float last_rate = 0.0; // set rate calculated in Time interval equals to number of packets
	present_rate = Rate_blend_load.actual_weight; // This weight is calculated in the duration of 200ms

 	if (Rate_blend_load.Delta_time <= 30) // For belt speed = 0 condition
 			{
 		Rate_blend_load.Delta_time = 200;
 	}

	// if Preload Delay Timer is enabled and Feeder Delay timer is disabled or it is STOPPED
	if (Setup_device_var.Preload_Delay > 0
			&& ((Rate_blend_load.FD_Timer_flag == 0)
					|| (Rate_blend_load.FD_Timer_flag == TIMER_STOP))) {
		// Pop the first value from FIFO and store into Set Rate variable
		Pop_FIFO(&(Rate_blend_load.blend_FIFO), &Rate_blend_load.Set_rate);
		set_rate = Rate_blend_load.Set_rate / RATE_CONV_CURR_200MS; // Conversion of 200mS in
		//calculate the Bias value
		if (Setup_device_var.Feeder_Max_Rate != 0) {
			Rate_blend_load.Bias = Rate_blend_load.Set_rate
					/ Setup_device_var.Feeder_Max_Rate;
		} else {
			Rate_blend_load.Bias = 0;
		}
		if (Rate_blend_load.Bias > 0) {
			// Check Whether Preload Timer is Started OR not
			if (Rate_blend_load.PD_Timer_flag == 0) {
				// Start the Preload Timer
				Rate_blend_load.PD_Timer_flag = TIMER_START;
				//load the preload delay timer with the preload delay value
				Rate_blend_load.Preload_delay_timer =
						Setup_device_var.Preload_Delay;
				prev_sec = LPC_RTC->SEC;
			}
		} else {

			// Clear Preload Timer START and STOP flags
			if (last_rate == 0)
				Rate_blend_load.PD_Timer_flag = 0;
		}
	}
	
	//if preload delay timer is defined and the timer has been started
	if ((Rate_blend_load.Preload_delay_timer > 0)
			&& (Rate_blend_load.PD_Timer_flag == TIMER_START)) {
		/* Decrement the the Preload delay timer value after one second interval
		 and check if Preload Timer is timed out */
		if ((!(Decrement_Timer(&Rate_blend_load.Preload_delay_timer)))) {
			// After feed delay timer times out Stop The Preload delay timer
			Rate_blend_load.PD_Timer_flag = TIMER_STOP;
		} else // if Preload delay              timer is not stoped then Output = Bias
		{
			Rate_blend_load.DAC_count = (int) (Rate_blend_load.Bias
					* (DAC_FULL_SCALE_COUNT + 1));
		}
	} // (if both preload and feeder delay timers are timed out) OR
	  //(if preload timer is not started during feeder delay period) OR
	  //Feeder Delay timer is disabled and Preload Delay Timer is STOPPED or Disabled
	if ((((Rate_blend_load.FD_Timer_flag == TIMER_STOP)
			&& (Rate_blend_load.PD_Timer_flag == TIMER_STOP))
			|| ((Rate_blend_load.FD_Timer_flag == TIMER_START)
					&& (Rate_blend_load.PD_Timer_flag == 0))
			|| ((Rate_blend_load.FD_Timer_flag == 0)
					&& (Rate_blend_load.PD_Timer_flag != TIMER_START)))) {
		// if Preload Delay is disabled and (Feeder delay is not started or disabled or stopped)
		if ((Setup_device_var.Preload_Delay <= 0
				&& Rate_blend_load.FD_Timer_flag != TIMER_START)) {
			//Rate_blend_load.Bias = 0;
			// Pop the first value from FIFO and store into Set Rate variable
			Pop_FIFO(&Rate_blend_load.blend_FIFO, &Rate_blend_load.Set_rate);
			if (Setup_device_var.Feeder_Max_Rate != 0) {
				Rate_blend_load.Bias = Rate_blend_load.Set_rate
						/ Setup_device_var.Feeder_Max_Rate;
			} else {
				Rate_blend_load.Bias = 0;
			}
			set_rate = Rate_blend_load.Set_rate / RATE_CONV_CURR_200MS;
		}
		//Calculate the p_term error
		Rate_blend_load.P_term = Setup_device_var.PID_P_Term
				* (set_rate - present_rate);
		//calculate the i_term error if the windup_flag is not set

		PrevISUM = Rate_blend_load.I_sum;
		if (u8First == 1) {
			if ((Rate_blend_load.Calculation_flags & WINDUP_FLAG) != WINDUP_FLAG) {
				//	Rate_blend_load.I_sum += (set_rate - present_rate)*(200.0/ONE_SEC);
				Rate_blend_load.I_sum += (set_rate - present_rate)
						* (Rate_blend_load.Delta_time / ONE_SEC);
				}
			
			
			//calculate the d_term error                           // current rate               // last rate
			else {
				if (Rate_blend_load.DAC_count >= 0x0FFF) {
					Rate_blend_load.I_sum += (set_rate - present_rate)
							* (Rate_blend_load.Delta_time / ONE_SEC);
					if (Rate_blend_load.I_sum < PrevISUM)
						;
					else
						Rate_blend_load.I_sum = PrevISUM;
				}
				if (Rate_blend_load.DAC_count <= 0) {
					Rate_blend_load.I_sum += (set_rate - present_rate)
							* (Rate_blend_load.Delta_time / ONE_SEC);
					if (Rate_blend_load.I_sum > PrevISUM)
						;
					else
						Rate_blend_load.I_sum = PrevISUM;
				}
			}
				if (Rate_blend_load.I_sum> Integrator_limits.I_sum)
				Rate_blend_load.I_sum = Integrator_limits.I_sum;
				if ((Rate_blend_load.I_sum)< (Integrator_limits.I_sum*(-1.0)))
				Rate_blend_load.I_sum = Integrator_limits.I_sum * (-1.0);

			Rate_blend_load.I_term = Setup_device_var.PID_I_Term
					* Rate_blend_load.I_sum;
			if (Rate_blend_load.I_term> Integrator_limits.I_term){
			Rate_blend_load.I_term = Integrator_limits.I_term;
			Rate_blend_load.I_sum = Integrator_limits.I_sum;	
				Rate_blend_load.I_sum = PrevISUM;
			}
			if ((Rate_blend_load.I_term)< (Integrator_limits.I_term*(-1.0))){
			Rate_blend_load.I_term = Integrator_limits.I_term * (-1.0);
				Rate_blend_load.I_sum = Integrator_limits.I_sum * (-1.0);
				Rate_blend_load.I_sum = PrevISUM;
			}

		}
		Rate_blend_load.D_term = Setup_device_var.PID_D_Term
				* (present_rate - last_rate);
		//store the current rate as the last rate for future calculation
		last_rate = present_rate;
		if(rc_count>= 10){
		//calculate the PID error to be added to the DAC count
		Rate_blend_load.PID_error = Rate_blend_load.P_term
				+ Rate_blend_load.I_term + Rate_blend_load.D_term;
		Rate_blend_load.DAC_count = (int) ((Rate_blend_load.Bias
				* (DAC_FULL_SCALE_COUNT + 1)) + Rate_blend_load.PID_error);
		}
		else rc_count++;
		//Added for fixing issue of Blending control not working in April 2016
		/*--------------------------------------------------------------------------------------------------------*/
		/*if(Rate_blend_load.DAC_count)		//if(Calculation_struct.Belt_speed)
		 {
		 g_FD_action_started_flag = 0;
		 }
		 //if((Rate_blend_load.DAC_count >= DAC_FULL_SCALE_COUNT) && (((prev_belt_speed == 0) && (Calculation_struct.Belt_speed > 0))
		 // ))		//Rate_blend_load.Set_rate		//|| (Calculation_struct.Belt_speed > prev_belt_speed)

		 if((Rate_blend_load.DAC_count >= DAC_FULL_SCALE_COUNT) && ((belt_speed_zero_flag == 1) && (Calculation_struct.Belt_speed > 0)))
		 {
		 //Rate_blend_load.I_term = DAC_FULL_SCALE_COUNT;
		 //start_calc = 1;
		 Rate_blend_load.DAC_count = DAC_FULL_SCALE_COUNT;	// - 1
		 }*/
		/*--------------------------------------------------------------------------------------------------*/

		u8First = 1;

	}
	// During Feed Delay Timer is ON (OR) if (set rate = 0) && (actual rate <= 0), then
	if (((Rate_blend_load.Calculation_flags & FEED_TIMER_START) != 0)
			|| ((set_rate <= 0)))				 // && (present_rate <= 0)))
			{
		Rate_blend_load.DAC_count = 0;
	}
	// If Count is Greater then 4095 or less than Zero
	if ((Rate_blend_load.DAC_count > DAC_FULL_SCALE_COUNT)
			|| (Rate_blend_load.DAC_count <= 0)) {

		Rate_blend_load.Calculation_flags |= WINDUP_FLAG; //set the windup flag
		if (Rate_blend_load.DAC_count <= 0) {
			Rate_blend_load.DAC_count = 0;
		} else {
			Rate_blend_load.DAC_count = DAC_FULL_SCALE_COUNT;
		}
	} else {
		Rate_blend_load.Calculation_flags &= (~WINDUP_FLAG); //reset the windup flag

		//	 belt_speed_zero_flag = 0;   //Added for fixing issue of Blending control not working in April 2016
	}

// 	if (Calculation_struct.Net_total_del_wt < Admin_var.Zero_rate_limit) {
// 			(Rate_blend_load.DAC_count = DAC_FULL_SCALE_COUNT);
// 		
// 	}
	
	//Added for fixing issue of Blending control not working in April 2016
	/*--------------------------------------------------------------------------------------------------------*/
	/* prev_belt_speed = Calculation_struct.Belt_speed;
	 if((Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR) || (prev_belt_speed <= 0))
	 {
	 belt_speed_zero_flag = 1;
	 }*/
	/*--------------------------------------------------------------------------------------------------------*/
	// Rate_blend_load.DAC_count variable is passed to IO processing task where it will map this value to
	// Appropriate current.
// 	  printf("PID Error: %f, P_Error: %f, I_Error: %f, D_Error: %f, DACcount: %d, Bias: %f, A.R: %f\n", Rate_blend_load.PID_error,
// 																																																		 Rate_blend_load.P_term,
// 																																																		 Rate_blend_load.I_term,
// 																																																		 Rate_blend_load.D_term,
// 																																																		 Rate_blend_load.DAC_count,
// 																																																		 (Rate_blend_load.Bias*(DAC_FULL_SCALE_COUNT+1)),
// 																																																		 Calculation_struct.Rate);
}

/*****************************************************************************
 * @note       Function name: rate_control_flow
 * @returns    returns      : None
 * @param      arg1         : None
 * @author                  : R. Venkata Krishna Rao
 * @date       date created : Nov 21, 2013
 * @brief      Description  : Performs rate flow related timer logic
 * @note       Notes        : None
 *****************************************************************************/
void rate_control_flow(void) {
	// Source Point for Set Rate defination is done in scree3.c call back function where Set Point type is set.
	// To display on Blending Run mode screen' Target Rate variable	 
	Setup_device_var.Target_rate = Rate_blend_load.Set_rate;
	// PUSH Setrate in FIFO
	Push_FIFO(&Rate_blend_load.blend_FIFO, Rate_blend_load.Set_rate);
	//Disable Feed Delay Timer in Rate Control
	Rate_blend_load.FD_Timer_flag = 0;
	// to do math related to rate control call below function which is common to both blending and rate control
	common_calc_rate_blending_load_ctrl();
}

/*****************************************************************************
 * @note       Function name: blend_control_flow
 * @returns    returns      : None
 * @param      arg1         : None
 @author                   : R Venkata Krishna Rao
 * @date       date created : Nov 20, 2013
 * @brief      Description  : Performs Blending related timer Logic
 * @note       Notes        : None
 *****************************************************************************/
void blend_control_flow(void) {
	/*#ifdef DEBUG_DATA_SEND_1
	 char chData_buf[50];
	 #endif*/

	// Source Point for Set Rate defination and calculat is done in scree3.c call back function where Set Point type is set.
	// if Feeder Timer is enabled and it not started
	/*	if(Calculation_struct.Belt_speed <=0) //Relaced this by below statement for fixing issue of Blending control not working in April 2016
	 //	if((Calculation_struct.Belt_speed <=0) && (Rate_blend_load.PD_Timer_flag == TIMER_STOP) && \
//		 (Rate_blend_load.Feed_delay_timer == 0) && (g_FD_action_started_flag == 0) && (Rate_blend_load.DAC_count == 0))	
	 {
	 Setup_device_var.Target_rate = Rate_blend_load.Set_rate;
	 Rate_blend_load.DAC_count = 0;
	 Rate_blend_load.P_term = Setup_device_var.PID_P_Term;
	 Rate_blend_load.I_term = Setup_device_var.PID_I_Term;
	 Rate_blend_load.D_term = Setup_device_var.PID_D_Term;
	 Rate_blend_load.I_sum	= 0;
	 Rate_blend_load.PD_Timer_flag = 0;
	 Rate_blend_load.FD_Timer_flag = 0;
	 Rate_blend_load.Feed_delay_timer = 0;
	 Rate_blend_load.Preload_delay_timer = 0;
	 //start_calc = 0;
	 u8First = 0;
	 }
	 else*/
	{
		if ((Setup_device_var.Feed_Delay > 0)
				&& (Rate_blend_load.FD_Timer_flag == 0)) {
			//Load Feeder delay timer
			Rate_blend_load.Feed_delay_timer = Setup_device_var.Feed_Delay;
		}

		//Added for fixing issue of Blending control not working in April 2016
		/* if(Rate_blend_load.FD_Timer_flag == 0)	//Added on 05-Apr-2016
		 {
		 g_FD_action_started_flag = 1;
		 }*/

		// To display on Blending Run mode screen' Target Rate variable
		Setup_device_var.Target_rate = Rate_blend_load.Set_rate;
		// PUSH Setrate in FIFO
		if (Rate_blend_load.Feed_delay_timer > 0) {	//During Feeder delay period scale is not poping values from FIFO,so limited Push operation is done on FIFO.
			if (Rate_blend_load.Set_rate != Rate_blend_load.Last_Set_rate)
				Push_FIFO(&(Rate_blend_load.blend_FIFO),
						Rate_blend_load.Set_rate);
		} else
			//when feeder timer is off popping will be done continuously.
			Push_FIFO(&(Rate_blend_load.blend_FIFO), Rate_blend_load.Set_rate);
		/*if Set Rate > 0 i.e there is some meterial in the master scale and if feeder delay is set by customer in
		 slave scale and if Feeder timer is not started*/
		if (Rate_blend_load.Set_rate > 0 && Setup_device_var.Feed_Delay > 0
				&& (Rate_blend_load.FD_Timer_flag != TIMER_STOP)) {
			// if feed delay timer is not started then start the timer and initialize the timer values
			if (Rate_blend_load.FD_Timer_flag != TIMER_START) {
				// Feed_timer_status = 0;
				Rate_blend_load.FD_Timer_flag = TIMER_START; //start the feeder timer
				prev_sec = LPC_RTC->SEC;
				Rate_blend_load.Last_Set_rate = Rate_blend_load.Set_rate;
			} else { // Decrement the the Feed delay timer value after one second interval
				if (!(Decrement_Timer(&Rate_blend_load.Feed_delay_timer))) {
					// After feed delay timer times out Stop The feeder delay timer
					Rate_blend_load.FD_Timer_flag = TIMER_STOP;
				}
			}
		}

		//Added by DK on 29 April 2016

		/*if(++WindupCounter > 100)
		 {
		 #ifdef DEBUG_DATA_SEND_1
		 sprintf(chData_buf,"\r\n %0.2f,%0.2f,%0.2f,%d,%d",Rate_blend_load.P_term,\
				Rate_blend_load.I_sum, Rate_blend_load.D_term,Rate_blend_load.DAC_count,\
			       (Rate_blend_load.Calculation_flags & WINDUP_FLAG));
		 Uart_i2c_data_Tx_1((uint8_t *)chData_buf, strlen(chData_buf));
		 #endif
		 WindupCounter =0;
		 }*/

		/* if(((Rate_blend_load.Calculation_flags & WINDUP_FLAG) == WINDUP_FLAG) && \
				((Rate_blend_load.DAC_count >= DAC_FULL_SCALE_COUNT) || (Rate_blend_load.DAC_count <= 0)) && \
			   (Rate_blend_load.PD_Timer_flag == TIMER_STOP))*/
		{
			//   Rate_blend_load.Calculation_flags &= (~WINDUP_FLAG); //reset the windup flag
			/* if(++WindupCounter > 100)
			 {
			 WindupCounter =0;
			 //  Rate_blend_load.Calculation_flags &= (~WINDUP_FLAG); //reset the windup flag
			 //Rate_blend_load.P_term = Setup_device_var.PID_P_Term;
			 //	Rate_blend_load.I_term = Setup_device_var.PID_I_Term;
			 //	Rate_blend_load.D_term = Setup_device_var.PID_D_Term;
			 Rate_blend_load.I_sum	= 0;
			 //	Rate_blend_load.PD_Timer_flag = 0;
			 //	Rate_blend_load.FD_Timer_flag = 0;
			 //	Rate_blend_load.Feed_delay_timer = 0;
			 //	Rate_blend_load.Preload_delay_timer = 0;
			 }*/
		}

		// to do math related to blending call below function which is common to both blending and rate control
		common_calc_rate_blending_load_ctrl();

	}

}

/*****************************************************************************
 * @note       Function name: Decrement_Timer
 * @returns    returns      : 0 if timer count becomes zero or timer count
 * @param      arg1         : Timer count variable
 * @author                  : R Venkata Krishna Rao
 * @date       date created : Nov 20, 2013
 * @brief      Description  : Decrements timer count variable at an interval of one second
 * @note       Notes        : None
 *****************************************************************************/
unsigned int Decrement_Timer(unsigned int *delay_timer) {
	if ((LPC_RTC->SEC >= prev_sec + 1)
			|| (prev_sec == 59 && LPC_RTC->SEC == 0)) { // If feed delay timer is not done
		if ((*delay_timer) > 0) {
			(*delay_timer)--;
			prev_sec = LPC_RTC->SEC;
		}
	}
	return (*delay_timer);
}
#endif //#ifdef RATE_BLEND_LOAD_CTRL_CALC
/*****************************************************************************
 * End of file
 *****************************************************************************/
