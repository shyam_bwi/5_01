/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Log_report_data_calculate.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Feb 13th Wednesday, 2013  <Feb 14, 2013>
* @date Last Modified  : Feb 13th Wednesday, 2013  <Feb 14, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __LOG_REPORT_DATA_CALCULATE_H
#define __LOG_REPORT_DATA_CALCULATE_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "File_update.h"
#include "Screen_global_ex.h"

#ifdef FILE_SYS
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define REC_QUEUE_SIZE 				(U8)30
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
extern BOOL File_Write_In_Progress;
/* Character variables section */

/* unsigned integer variables section */
extern U16 ferror_cnt;

extern U8 g_Qfront;
extern U8 g_Qrear;
extern U8 g_Q_Status;
extern U8 g_Q_addition_prog;
extern U16 g_last_periodic_id;
extern U8 new_Q_item_added;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
enum{
	PER_HDR = 0,
	PER_DAT = 1,
	PER_TCW = 2,
	PER_CLR = 3,
	PER_LEN = 4,
	PER_ZER = 5,
	PER_TST = 6,
	PER_MAT = 7,
	PER_DIG = 8,
	PER_FIN = 9,
	PER_CAN = 10,
	PER_IDT = 11,
	PER_FDT = 12,
	PER_DCW = 13,
};

enum{
	Q_EMPTY = 0,
	Q_NOT_EMPTY = 1,
};
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void error_status (void);
extern U8 log_data_populate(U8 log_type);
extern void report_data_populate (U8 report_type);
extern void rec_Q_init (void);
extern void rec_Q_add_item (PERIODIC_LOG_STRUCT *periodic_data_item);

#endif /*FILE_FS*/

#endif /*__LOG_REPORT_DATA_CALCULATE_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
