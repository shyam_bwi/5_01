/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Sensor_board_data_process.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 12, 2012>
* @date Last Modified  : July Thursday, 2012  <Feb 3, 2014>
* @Modified By				 : R.Venkata Krishna Rao
* @internal Change Log : <2014-02-03>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include <RTL.h>
#include "Screen_global_ex.h"
#include "Conversion_functions.h"
#include "Sensor_board_data_process.h"
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "Screen_data_enum.h"
#include "Log_report_data_calculate.h"
#include "Rate_blending_load_ctrl_calc.h"
#include "IOProcessing.h"
#include "I2C_driver.h"
#include "Firmware_upgrade.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifdef CALCULATION
 #define THREE_SEC_TIMEOUT                3000

 #define MILLI_SEC_TO_SEC_CONV_FACTOR     1000
 #define SENSOR_TASK_INTVL        				(10*SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL)
 #define MAX_BUFF_PKTS										100 // for five seconds
 #define ANGLE_SENSOR_LIMIT							  35

#define NO_OF_ELEMENTS 30 //By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
//By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values
#ifdef DEBUG_DATA_SEND_1
	char chData_buf[50];
#endif

	
/* unsigned integer variables section */
//By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values
static U32 speed_buf_index =0;

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */
//By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values
static float speed_buf1[NO_OF_ELEMENTS]={0.0};
static float speed_buf2[NO_OF_ELEMENTS]={0.0};

/* Double variables section */

/* Structure or Union variables section */
Soft_FIFO Rate_disp_FIFO __attribute__((section ("GUI_DATA_RAM"), zero_init));     /*Buffering of Rate for calculating Moving average and Display*/

//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
/*RTCTime log_current_time;
double log_total_weight;
double log_daily_total_weight;*/
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
static BOOL power_on=1;  
/* Character variables section */
U8 u8AngleErrorCount;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
SENSOR_BOARD_PARAM Sens_brd_param;    /*Parameters received from the sensor board*/
CALC_STRUCT Calculation_struct;       /*Parameters used for calculation*/
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
//By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values
int cmpfunc (const void * a, const void * b)
{
  if(*(float*)b > *(float*)a)
  return 1; 
  else if(*(float*)b < *(float*)a)
  return -1;
	else 
	return 0;
	//return ( *(float*)b - *(float*)a );
}

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: void Parse_rcvd_data (void)
* @returns    returns      : none
* @param      arg1         : Pointer to the received data
* @author                  : Anagha Basole
* @date       date created : 31st August 2012
* @brief      Description  : Parse the data received from the sensor board from
*                            the receive buffer, and populate the structure
* @note       Notes        : None
*****************************************************************************/
void Parse_rcvd_data (U8 *rx)
{
  U8 i, rx_head = 0;

  memcpy(&Sens_brd_param.Board_Id, &rx[rx_head], sizeof(Sens_brd_param.Board_Id));
  rx_head += sizeof(Sens_brd_param.Board_Id);

  memcpy(&Sens_brd_param.Fm_Ver, &rx[rx_head], sizeof(Sens_brd_param.Fm_Ver));
  rx_head += sizeof(Sens_brd_param.Fm_Ver);

  memcpy(&Sens_brd_param.Heartbeat, &rx[rx_head], sizeof(Sens_brd_param.Heartbeat));
  rx_head += sizeof(Sens_brd_param.Heartbeat);

  memcpy(&Sens_brd_param.No_of_pulses, &rx[rx_head], sizeof(Sens_brd_param.No_of_pulses));
  rx_head += sizeof(Sens_brd_param.No_of_pulses);

  memcpy(&Sens_brd_param.Time, &rx[rx_head], sizeof(Sens_brd_param.Time));
  rx_head += sizeof(Sens_brd_param.Time);
	if ((Sens_brd_param.Time > FLT_MAX) ||
		   (Sens_brd_param.Time < FLT_MIN))
	{
		 Sens_brd_param.Time = 0;
	}

  for(i=0; i<NO_OF_LOADCELL; i++)
  {
    memcpy(&Sens_brd_param.Accum_LC_Perc[i], &rx[rx_head], sizeof(Sens_brd_param.Accum_LC_Perc[i]));
    rx_head += sizeof(Sens_brd_param.Accum_LC_Perc[i]);
		if ((Sens_brd_param.Accum_LC_Perc[i] > FLT_MAX) ||
		   (Sens_brd_param.Accum_LC_Perc[i] < -(FLT_MAX)))
		{
			 Sens_brd_param.Accum_LC_Perc[i] = 0;
		}
  }
  for(i=0; i<NO_OF_LOADCELL; i++)
  {
    memcpy(&Sens_brd_param.Inst_LC_Perc[i], &rx[rx_head], sizeof(Sens_brd_param.Inst_LC_Perc[i]));
    rx_head += sizeof(Sens_brd_param.Inst_LC_Perc[i]);
		if ((Sens_brd_param.Inst_LC_Perc[i] > FLT_MAX) ||
		   (Sens_brd_param.Inst_LC_Perc[i] < -(FLT_MAX)))
		{
			 Sens_brd_param.Inst_LC_Perc[i] = 0;
		}
  }
  memcpy(&Sens_brd_param.Angle_mv, &rx[rx_head], sizeof(Sens_brd_param.Angle_mv));
  rx_head += sizeof(Sens_brd_param.Angle_mv);
	if ((Sens_brd_param.Angle_mv > FLT_MAX) ||
		   (Sens_brd_param.Angle_mv < -(FLT_MAX)))
	{
		 Sens_brd_param.Angle_mv = 0;
	}

  memcpy(&Sens_brd_param.LC_supply_volt, &rx[rx_head], sizeof(Sens_brd_param.LC_supply_volt));
  rx_head += sizeof(Sens_brd_param.LC_supply_volt);
	if ((Sens_brd_param.LC_supply_volt > FLT_MAX) ||
		   (Sens_brd_param.LC_supply_volt < -(FLT_MAX)))
	{
		 Sens_brd_param.LC_supply_volt = 0;
	}

  memcpy(&Sens_brd_param.Amp_ref_volt, &rx[rx_head], sizeof(Sens_brd_param.Amp_ref_volt));
  rx_head += sizeof(Sens_brd_param.Amp_ref_volt);
	if ((Sens_brd_param.Amp_ref_volt > FLT_MAX) ||
		   (Sens_brd_param.Amp_ref_volt < -(FLT_MAX)))
	{
		 Sens_brd_param.Amp_ref_volt = 0;
	}

  memcpy(&Sens_brd_param.Five_volt_supply, &rx[rx_head], sizeof(Sens_brd_param.Five_volt_supply));
  rx_head += sizeof(Sens_brd_param.Five_volt_supply);
	if ((Sens_brd_param.Five_volt_supply > FLT_MAX) ||
		   (Sens_brd_param.Five_volt_supply < -(FLT_MAX)))
	{
		 Sens_brd_param.Five_volt_supply = 0;
	}

  memcpy(&Sens_brd_param.Rate_input_4_20ma, &rx[rx_head], sizeof(Sens_brd_param.Rate_input_4_20ma));
  rx_head += sizeof(Sens_brd_param.Rate_input_4_20ma);
	if ((Sens_brd_param.Rate_input_4_20ma > FLT_MAX) ||
		   (Sens_brd_param.Rate_input_4_20ma < FLT_MIN))
	{
		 Sens_brd_param.Rate_input_4_20ma = 0;
	}	
  return;
}

/*****************************************************************************
* @note       Function name: float Idler_center_dist_calc (void)
* @returns    returns      : the number of loadcells according to the nos of idlers
* @param      arg1         : none
* @author                  : Anagha Basole
* @date       date created : 8th May 2013
* @brief      Description  : Computes the Idler center distance according to the
*                          : number of idlers selected. Also computes the number of
*                          : loadcells.
* @note       Notes        : None
*****************************************************************************/
float Idler_center_dist_calc (void)
{
   float nos_of_loadcells;

    if (Scale_setup_var.Number_of_idlers == 1)
    {
      nos_of_loadcells = 2.0;
      Calculation_struct.Idler_center_dist = (float)((Scale_setup_var.Idler_distanceA + Scale_setup_var.Idler_distanceB) \
                                              / 2.0);
    }
    else if (Scale_setup_var.Number_of_idlers == 2)
    {
      nos_of_loadcells = 4.0;
      Calculation_struct.Idler_center_dist = (float)((Scale_setup_var.Idler_distanceA + (2.0 * Scale_setup_var.Idler_distanceB) \
                                                + Scale_setup_var.Idler_distanceC) / 2.0);
    }
    else if (Scale_setup_var.Number_of_idlers == 3)
    {
      nos_of_loadcells = 6.0;
      Calculation_struct.Idler_center_dist = (float)((Scale_setup_var.Idler_distanceA + (2.0 * Scale_setup_var.Idler_distanceB) \
                                              + (2.0 * Scale_setup_var.Idler_distanceC) + Scale_setup_var.Idler_distanceD) \
                                                / 2.0);
    }
    else if (Scale_setup_var.Number_of_idlers == 4)
    {
      nos_of_loadcells = 8.0;
      Calculation_struct.Idler_center_dist = (float)((Scale_setup_var.Idler_distanceA + (2.0 * Scale_setup_var.Idler_distanceB) \
                                              + (2.0 * Scale_setup_var.Idler_distanceC) + \
                                              (2.0 * Scale_setup_var.Idler_distanceD) + Scale_setup_var.Idler_distanceE) \
                                               / 2.0);
    }
    Scale_setup_var.Idler_distance = Calculation_struct.Idler_center_dist;

    return (nos_of_loadcells);
}

/*****************************************************************************
* @note       Function name: void Calculation (void)
* @returns    returns      : none
* @param      arg1         : none
* @author                  : Anagha Basole
* @date       date created : 31st August 2012
* @brief      Description  : Complete the calculation using the data rcvd from
*                          : the sensor board and the parameters selected in the
*                          : integrator board.
*                            the receive buffer, and populate the structure
* @note       Notes        : None
*****************************************************************************/
void Calculation (void)
{
    U8 i, load_cell_error = 0;
		static U8 zeroPulseTimes = 0;
    float load_cell_capacity = 1.0, nos_of_loadcells = 1.0, AngleSenADCnt;
    static U16 negative_rate_time = 0, negative_speed_time = 0;
    //static S8 rate_disp_cnt=-1;
	  float rate_avg = 0;

    //By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values
	  float Belt_speed =0.0;
	  double Belt_Speed_Avg=0; 
	
	  U8 front_temp = 0; // local variable for temporary caculation
	
		//char buf[30];
		//static int nooftimes;

    /************************************ UNIT CONVERSIONS CALCULATION *********************************************/
    Distance_unit_conversion();
    Weight_unit_conv_func();
    /************************************ ANGLE CALCULATION *********************************************/
    AngleSenADCnt = Sens_brd_param.Angle_mv * 4096.0/2500.0;
    AngleSenADCnt = AngleSenADCnt * ANGLE_SEN_FACTOR;
	
			//if angle sensor is installed then use the angle from sensor else use the manual angle entery
      if (Scale_setup_var.AngleSensor_Install_status == Screen271_M_str1)
      {
				Calculation_struct.Angle = asin( (((AngleSenADCnt*4.096/4095)-2.048)/2.896309376))*57.29577951;
				/* Added on 30th Nov 2015 for BS-111 fix */
				//If angle is negative then consider its abs value
				Calculation_struct.Angle = fabsf(Calculation_struct.Angle);
		    Sens_brd_param.Angle_mv = Sens_brd_param.Angle_mv/(R130*1.0/(R118+R119+1000+R130));
				Scale_setup_var.Conveyor_angle = Calculation_struct.Angle;
			}
			else
			{
				Calculation_struct.Angle = Scale_setup_var.Conveyor_angle;
				//Sens_brd_param.Angle_mv = 0.00;		//Rajeev for Load Out mode.
			}				

    /************************************ NOS OF LOADCELLS CALC *********************************************/
    nos_of_loadcells = Idler_center_dist_calc();

    /************************************ LOAD CELL CAPACITY CALCULATION ****************************************/
    load_cell_capacity = Load_cell_capacity_calc();

    /************************************ LOAD CELL CAPACITY ASSIGNMENT ****************************************/
    for (i=0; i<nos_of_loadcells; i++)
    {
      Calculation_struct.LC_capacity[i] = load_cell_capacity;
			// Load cell error variable is incremented when any of the load cell is giving abnormal output.
			//if((Sens_brd_param.Inst_LC_Perc[i] > 120) | (Sens_brd_param.Inst_LC_Perc[i] <= 0.1))
			if((Sens_brd_param.Inst_LC_Perc[i] > 120) | (Sens_brd_param.Inst_LC_Perc[i] <= -10))
			{
				load_cell_error++; // Counter for Load cell errors.
			}
      /************************************ REPORT SCREEN PARAM *********************************************/
      switch(i)
      {
        case 0:
          Rprts_diag_var.Load_Cell_1_Output = Sens_brd_param.Inst_LC_Perc[i];
				  
          break;

        case 1:
          Rprts_diag_var.Load_Cell_2_Output = Sens_brd_param.Inst_LC_Perc[i];
          break;

        case 2:
          Rprts_diag_var.Load_Cell_3_Output = Sens_brd_param.Inst_LC_Perc[i];
          break;

        case 3:
          Rprts_diag_var.Load_Cell_4_Output = Sens_brd_param.Inst_LC_Perc[i];
          break;

        case 4:
          Rprts_diag_var.Load_Cell_5_Output = Sens_brd_param.Inst_LC_Perc[i];
          break;

        case 5:
          Rprts_diag_var.Load_Cell_6_Output = Sens_brd_param.Inst_LC_Perc[i];
          break;

        case 6:
          Rprts_diag_var.Load_Cell_7_Output = Sens_brd_param.Inst_LC_Perc[i];
          break;

        case 7:
          Rprts_diag_var.Load_Cell_8_Output = Sens_brd_param.Inst_LC_Perc[i];
          break;
      }
    }
    /************************************ REPORT SCREEN PARAM *********************************************/
    if ((Sens_brd_param.Time != 0) )/*&& (Sens_brd_param.No_of_pulses != 0))*/
    {
       Rprts_diag_var.Speed_Sensor = (Sens_brd_param.No_of_pulses * MILLI_SEC_TO_SEC_CONV_FACTOR) \
                                       / Sens_brd_param.Time;
    }
		else if(Sens_brd_param.No_of_pulses == 0)
		{ 
			 Rprts_diag_var.Speed_Sensor = 0;
		}
    Rprts_diag_var.Speed_In_Hz = Speed_unit;
    Rprts_diag_var.Angle_Sensor = Calculation_struct.Angle;
    Rprts_diag_var.Load_cell_supply = Sens_brd_param.LC_supply_volt;
    Rprts_diag_var.Five_volt_supply = Sens_brd_param.Five_volt_supply;
		Rprts_diag_var.Tolerance_err = ((Calibration_var.Test_weight-Rprts_diag_var.Live_weight_disp)/Rprts_diag_var.Live_weight_disp)*100;
    /************************************ GROSS WEIGHT CALCULATION ****************************************/
    Calculation_struct.Total_gross_weight = 0.0;
    Calculation_struct.Load_cell_perc = 0.0;
    for (i=0; i<nos_of_loadcells; i++)
    {
       Calculation_struct.Gross_weight[i] = ((Sens_brd_param.Accum_LC_Perc[i]* \
                                              Calculation_struct.LC_capacity[i])/ \
                                              cos(RADIANS(Calculation_struct.Angle)));
       Calculation_struct.Total_gross_weight += Calculation_struct.Gross_weight[i];
       Calculation_struct.Load_cell_perc += Sens_brd_param.Accum_LC_Perc[i];
    }

    //If test speed is enabled then use the time as 50ms and calculate the number of pulses depending on 
    //the entered speed and also considering that one pulse is generated when the belt moves 1/8th of an inch
    //
    if (Admin_var.Test_speed_status == Screen431_str2)
    {
        Sens_brd_param.Time = 50;
        Sens_brd_param.No_of_pulses = (unsigned int)(((Admin_var.Test_speed * Calculation_struct.Belt_length_conv_factor)*8)*(Sens_brd_param.Time)/60000.0);
    }
    /********** NET WEIGHT, BELT LOAD, DIST TRAVELLED, NET TOTAL DEL WT, TOTAL WEIGHT CALCULATION ***********/
    Calculation_struct.Net_weight = Calculation_struct.Total_gross_weight - (Calculation_struct.Zero_weight);
		
// 		if  ((Calculation_struct.Total_gross_weight<auto_zero_para1.Zero_wt_tolerance_high)&&(Calculation_struct.Total_gross_weight>auto_zero_para1.Zero_wt_tolerance_low))
// 			Calculation_struct.Net_weight =0;
    if (Admin_var.Test_load_status == Screen431_str2)
    {
        //ensure that the test load weight is in LBS for internal calculation purposes
        Calculation_struct.Net_weight = Admin_var.Test_load * Calculation_struct.Test_weight_conv_factor;
    }
    if (Calculation_struct.Idler_center_dist != 0)
    {
        Calculation_struct.Belt_load = Calculation_struct.Net_weight/ \
                                    (Calculation_struct.Idler_center_dist * Calculation_struct.Idler_dist_conv_factor);
    }

    Calculation_struct.Dist_per_pulse = (Scale_setup_var.Wheel_diameter * M_PI * Calculation_struct.Wheel_diameter_conv_factor) \
                                          / Scale_setup_var.Pulses_Per_Revolution;

    Calculation_struct.Dist_travelled = Sens_brd_param.No_of_pulses * Calculation_struct.Dist_per_pulse;
//SKS->
// 		if(Scale_setup_var.User_Belt_Speed > 0)
// 		{
// 			Calculation_struct.Net_total_del_wt = Calculation_struct.Belt_load * \
// 																						Calculation_struct.Dist_travelled;			
// 		}
// 		else
		//SKS<-
		{
			Calculation_struct.Net_total_del_wt = Calculation_struct.Belt_load * \
																						Calculation_struct.Dist_travelled * \
																						Calculation_struct.Total_trim_factor;
    }
    //Display the live weight in KG or LBS
		if(Scale_setup_var.Distance_unit == Screen24_str5)		//Metric Unit
		{
			Rprts_diag_var.Live_weight_disp = Calculation_struct.Net_weight * LBS_TO_KG_CONV_FACTOR;// * Calculation_struct.Test_weight_conv_factor;
			Rprts_diag_var.Trim_live_weight_disp = Calculation_struct.Net_weight * LBS_TO_KG_CONV_FACTOR * Calculation_struct.Total_trim_factor;
		}
		else
		{
			Rprts_diag_var.Live_weight_disp = Calculation_struct.Net_weight * Calculation_struct.Test_weight_conv_factor;
			Rprts_diag_var.Trim_live_weight_disp = Calculation_struct.Net_weight * Calculation_struct.Test_weight_conv_factor * Calculation_struct.Total_trim_factor;
		}
		
		
    /************************************ RATE & SPEED CALCULATION *********************************************/
    if ((Sens_brd_param.Time != 0) && (Sens_brd_param.No_of_pulses != 0))
    {
					
       //Sens_brd_param.Time is in ms. Convert to min for speed, so divide time by 60 * 1000
				if (Admin_var.Test_speed_status == Screen431_str2)
				{
					 Calculation_struct.Belt_speed = Admin_var.Test_speed;
				}
				else
				{
					//By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values
					Belt_speed = ((Calculation_struct.Dist_travelled * MILLI_SEC_TO_SEC_CONV_FACTOR \
																						 * SEC_TO_MIN_TO_HOUR_CONV_FACTOR * Calculation_struct.Speed_conv_factor) \
																						/ Sens_brd_param.Time);
					
					if(power_on == 1)
					{
          //  power_on =0;
						for (i=0; i <NO_OF_ELEMENTS ;i++)
						{
              speed_buf1[i] = Belt_speed;
						}
          }
					else 
					{	
						if(speed_buf_index < NO_OF_ELEMENTS)
						{
							speed_buf1[speed_buf_index++]=Belt_speed;
						}	
						else 
						{
							speed_buf_index =0;
							speed_buf1[speed_buf_index++]=Belt_speed;
						}
				  }					
					for (i=0; i <NO_OF_ELEMENTS ;i++)
					{
              speed_buf2[i] = speed_buf1[i];
          }
					
					qsort(speed_buf2, NO_OF_ELEMENTS, sizeof(float), cmpfunc);
									
					/*#ifdef DEBUG_DATA_SEND_1
					if(power_on == 1)
					{	
					sprintf(chData_buf,"\r\n %0.2f,%0.2f,%0.2f,%0.2f,%0.2f",speed_buf1[0], speed_buf1[1],speed_buf1[2],speed_buf1[3],speed_buf1[4]);
					Uart_i2c_data_Tx_1((uint8_t *)chData_buf, strlen(chData_buf));
					power_on =0; 
          }
					#endif*/
					
					Belt_Speed_Avg =0.0;
					for(i=0; i < 10; i++)
					{
            Belt_Speed_Avg += speed_buf2[i];
			    }	
					Belt_Speed_Avg = Belt_Speed_Avg/10.0;
					
				 //Commented By DK on 30 April 2016 BS200- Add filtering to belt speed. Buffer 30 values, sort, take the average of the highest 10 values
					/*Calculation_struct.Belt_speed = ((Calculation_struct.Dist_travelled * MILLI_SEC_TO_SEC_CONV_FACTOR \
																						 * SEC_TO_MIN_TO_HOUR_CONV_FACTOR * Calculation_struct.Speed_conv_factor) \
																						/ Sens_brd_param.Time);*/
				 Calculation_struct.Belt_speed = Belt_Speed_Avg;
				 //Calculation_struct.Belt_speed = Belt_speed;
				}
				
				if(2 > Calculation_struct.Belt_speed )
				{
					if(zeroPulseTimes >= 20)
					{
						Calculation_struct.Belt_speed = Calculation_struct.Rate = Calculation_struct.Rate_for_display = 0;
					}
					else
					{

						 //convert time from ms to seconds by dividing time by 1000, then to minute by dividing time by 60 and
						 //to hour if required by dividing time by 60
						 Calculation_struct.Rate = ((Calculation_struct.Net_total_del_wt * MILLI_SEC_TO_SEC_CONV_FACTOR \
																				* SEC_TO_MIN_TO_HOUR_CONV_FACTOR * Calculation_struct.Rate_conv_factor \
																				* Calculation_struct.Weight_conv_factor) \
																			 / Sens_brd_param.Time);
						 // Calculation of Moving average 
             if(power_on == 1)
						 {
							 //for(i=0; i < SOFT_FIFO_SIZE ; i++ )
							 //{
                  Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
              // }	 
								power_on =0;
							 
							  Calculation_struct.Rate_for_display = Calculation_struct.Rate;
						 }	
						
						/*if((0 == Rate_disp_FIFO.count) ||  (50 == Rate_disp_FIFO.count))
						 {
							 Calculation_struct.Rate_for_display = Calculation_struct.Rate;
							 Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
						 }
   					 else */if(MAX_BUFF_PKTS <= (Rate_disp_FIFO.count))
						 { 
								//U8 front_temp = 0; // local variable for temporary caculation
								Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
								Pop_FIFO(&Rate_disp_FIFO,&rate_avg);
								for(i = 0,front_temp = Rate_disp_FIFO.front;i < Rate_disp_FIFO.count;i++,front_temp++)
								{
									Calculation_struct.Rate_for_display += Rate_disp_FIFO.contents[front_temp];
									if(front_temp >= (SOFT_FIFO_SIZE-1))
									{
										 front_temp = 0;
									}
								}
								Calculation_struct.Rate_for_display /= Rate_disp_FIFO.count;
						 }
						 else
						 { 
								Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
						 }							
						
						zeroPulseTimes++;
					}						
				}
				else
				{

				 //convert time from ms to seconds by dividing time by 1000, then to minute by dividing time by 60 and
				 //to hour if required by dividing time by 60
				 Calculation_struct.Rate = ((Calculation_struct.Net_total_del_wt * MILLI_SEC_TO_SEC_CONV_FACTOR \
																		* SEC_TO_MIN_TO_HOUR_CONV_FACTOR * Calculation_struct.Rate_conv_factor \
																		* Calculation_struct.Weight_conv_factor) \
																	 / Sens_brd_param.Time);
				 // Calculation of Moving average 
				 if(power_on == 1)
				 {
					 //for(i=0; i < SOFT_FIFO_SIZE ; i++ )
					 //{
						 Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
					 //}	 
					 power_on =0;
					 Calculation_struct.Rate_for_display = Calculation_struct.Rate;
					}
					/* if((0 == Rate_disp_FIFO.count) ||  (50 == Rate_disp_FIFO.count))
				 {
					 Calculation_struct.Rate_for_display = Calculation_struct.Rate;
					 Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
				 }
				 else*/ if(MAX_BUFF_PKTS <= (Rate_disp_FIFO.count))
				 { 
						U8 front_temp = 0; // local variable for temporary caculation
						Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
						Pop_FIFO(&Rate_disp_FIFO,&rate_avg);
						for(i = 0,front_temp = Rate_disp_FIFO.front;i < Rate_disp_FIFO.count;i++,front_temp++)
						{
							Calculation_struct.Rate_for_display += Rate_disp_FIFO.contents[front_temp];
							if(front_temp >= (SOFT_FIFO_SIZE-1))
							{
								 front_temp = 0;
							}
						}
						
						//sks BS244
						Calculation_struct.Rate_for_display /= (Rate_disp_FIFO.count+1);
				 }
				 else
				 { 
						Push_FIFO(&Rate_disp_FIFO,Calculation_struct.Rate);
				 }								
					zeroPulseTimes = 0;
				}		 
    }
		else if (Sens_brd_param.No_of_pulses == 0 && zeroPulseTimes>=20)
		{
			 Calculation_struct.Belt_speed = Calculation_struct.Rate = Calculation_struct.Rate_for_display = 0;
		}
		else
		{
			if(zeroPulseTimes<20)
				zeroPulseTimes++;
		}
    //if zero rate is enabled and Rate is greater than Zero rate limit or if Zero rate is disabled then accumulate the weight
    //By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152
		if(((Admin_var.Zero_rate_status == Screen6171_str2) && (Calculation_struct.Rate_for_display > Admin_var.Zero_rate_limit)) || 
			  (Admin_var.Zero_rate_status == Screen6171_str5))
    {
			  // Accumulated weight total used for Display purpose
        Calculation_struct.Total_weight_accum += Calculation_struct.Net_total_del_wt;
				  if((11 != IO_board_param.Quadrature_Wave_Setting) && (0 != IO_board_param.Quadrature_Wave_Setting))   //quadrature output
				  {
						  IO_board_param.Delivered_weight_Quad_OP += Calculation_struct.Net_total_del_wt;
					}
					else
					{
						  if(PULSED_OUTPUT_STR == Setup_device_var.Digital_Output_1_Function)
							{	
									IO_board_param.Delivered_weight_OP1 += Calculation_struct.Net_total_del_wt;
							}
							if(PULSED_OUTPUT_STR == Setup_device_var.Digital_Output_2_Function)
							{	
									IO_board_param.Delivered_weight_OP2 += Calculation_struct.Net_total_del_wt;
							}
							if(PULSED_OUTPUT_STR == Setup_device_var.Digital_Output_3_Function)
							{	
									IO_board_param.Delivered_weight_OP3 += Calculation_struct.Net_total_del_wt;
							}
					}	
			  Rate_blend_load.actual_weight += Calculation_struct.Net_total_del_wt;
			  Rate_blend_load.packet_cnt++;
				Rate_blend_load.Delta_time +=Sens_brd_param.Time;
				
					//Commented by DK on 2 May2016 
				//Calculation_struct.Total_weight = ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Calculation_struct.Total_weight_accum); //Added by DK on 14 Jan2016
				//Rprts_diag_var.daily_Total_weight += ((Calculation_struct.Net_total_del_wt)*(Calculation_struct.Weight_conv_factor));
				Totals_var.daily_Total_weight += Calculation_struct.Net_total_del_wt;
				//Rprts_diag_var.daily_Total_weight = ((Totals_var.daily_Total_weight)*(Calculation_struct.Weight_conv_factor)); //Commented by DK on 7 Jan2016
				Rprts_diag_var.daily_Total_weight = ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Totals_var.daily_Total_weight); //Added by DK on 7 Jan2016
				daily_rprt.Total_weight = Rprts_diag_var.daily_Total_weight;
			  //Rprts_diag_var.weekly_Total_weight += ((Calculation_struct.Net_total_del_wt)*(Calculation_struct.Weight_conv_factor));
				Totals_var.weekly_Total_weight += Calculation_struct.Net_total_del_wt;
				//Rprts_diag_var.weekly_Total_weight = ((Totals_var.weekly_Total_weight)*(Calculation_struct.Weight_conv_factor)); //Commented by DK on 8 Jan2016
				Rprts_diag_var.weekly_Total_weight = ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Totals_var.weekly_Total_weight); //Added by DK on 8 Jan2016
				weekly_rprt.Total_weight = Rprts_diag_var.weekly_Total_weight;
			  //Rprts_diag_var.monthly_Total_weight += ((Calculation_struct.Net_total_del_wt)*(Calculation_struct.Weight_conv_factor));
				Totals_var.monthly_Total_weight += Calculation_struct.Net_total_del_wt;
				//Rprts_diag_var.monthly_Total_weight = ((Totals_var.monthly_Total_weight)*(Calculation_struct.Weight_conv_factor)); //Commented by DK on 8 Jan2016
				Rprts_diag_var.monthly_Total_weight = ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Totals_var.monthly_Total_weight); //Added by DK on 8 Jan2016
				monthly_rprt.Total_weight = Rprts_diag_var.monthly_Total_weight;
				//Rprts_diag_var.yearly_Total_weight += ((Calculation_struct.Net_total_del_wt)*(Calculation_struct.Weight_conv_factor));
				Totals_var.yearly_Total_weight += Calculation_struct.Net_total_del_wt;
				//Rprts_diag_var.yearly_Total_weight = ((Totals_var.yearly_Total_weight)*(Calculation_struct.Weight_conv_factor)); //Commented by DK on 8 Jan2016
			  Rprts_diag_var.yearly_Total_weight = ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Totals_var.yearly_Total_weight); //Added by DK on 8 Jan2016
				//Rprts_diag_var.Job_Total +=	((Calculation_struct.Net_total_del_wt)*(Calculation_struct.Weight_conv_factor));
				Totals_var.Job_Total += Calculation_struct.Net_total_del_wt;
				//Rprts_diag_var.Job_Total =	((Totals_var.Job_Total)*(Calculation_struct.Weight_conv_factor));  //Commented by DK on 8 Jan2016
				Rprts_diag_var.Job_Total = ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Totals_var.Job_Total); //Added by DK on 8 Jan2016	
				//Rprts_diag_var.Master_total	+=((Calculation_struct.Net_total_del_wt)*(Calculation_struct.Weight_conv_factor));
				Totals_var.Master_total += Calculation_struct.Net_total_del_wt;
				//Rprts_diag_var.Master_total	=((Totals_var.Master_total)*(Calculation_struct.Weight_conv_factor)); //Commented by DK on 8 Jan2016
		     Rprts_diag_var.Master_total	= ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Totals_var.Master_total); //Added by DK on 8 Jan2016	
    }
		else
		{
		    Calculation_struct.Rate_for_display = 0.0;
	 //Rate_blend_load.actual_weight += Calculation_struct.Net_total_del_wt;
			  Rate_blend_load.packet_cnt++;
				Rate_blend_load.Delta_time +=Sens_brd_param.Time;
  }	

		//Calculation_struct.Total_weight_accum += Calculation_struct.Net_total_del_wt;		
	  if ((Calculation_struct.Total_weight_accum < DBL_MAX) &&
		   (Calculation_struct.Total_weight_accum > -(DBL_MAX)))
	  {
		   Calculation_struct.Total_weight = Calculation_struct.Total_weight_accum * Calculation_struct.Weight_conv_factor;
			 //Calculation_struct.Total_weight = ConvWeightUnit(LBS,Scale_setup_var.Weight_unit , Calculation_struct.Total_weight_accum); //Added by DK on 14 Jan2016
	  
     //Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
			/*log_total_weight = Calculation_struct.Total_weight;
			log_daily_total_weight = Totals_var.daily_Total_weight * Calculation_struct.Weight_conv_factor;
			
			log_current_time.RTC_Year = Current_time.RTC_Year;
			log_current_time.RTC_Mon = Current_time.RTC_Mon;
			log_current_time.RTC_Mday =Current_time.RTC_Mday;
			log_current_time.RTC_Hour =Current_time.RTC_Hour;
			log_current_time.RTC_Min =Current_time.RTC_Min;
			log_current_time.RTC_Sec =Current_time.RTC_Sec;*/
		}

    /************************************ LOAD CELL PERC CALCULATION *********************************************/
    if (nos_of_loadcells != 0)
    {
      Calculation_struct.Load_cell_perc = (Calculation_struct.Load_cell_perc / nos_of_loadcells)*100;
    }

    /************************************ ERROR FLAG GENERATION *********************************************/
    if ((Sens_brd_param.No_of_pulses == 0) && !(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR))
    {
      if (negative_speed_time >= THREE_SEC_TIMEOUT)
      {
          Flags_struct.Error_flags |= ZERO_BELT_SPEED_ERR;
          Flags_struct.Err_Wrn_log_written |= ZERO_BELT_SPEED_ERR_LOGGED;
      }
      else
      {
          negative_speed_time += (SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL * 10);
      }
    }
    else if (Sens_brd_param.No_of_pulses != 0)
    {
       Flags_struct.Error_flags &= ~ZERO_BELT_SPEED_ERR;
       negative_speed_time = 0;
       GUI_data_nav.Error_wrn_msg_flag[ZERO_BELT_SPEED_ERR_TIME] = 0;
    }
   // Setting Error Flags to indicate either Load Cells and Angle sensor failures
    //if any load cell is > 1, or <=0, load_cell_error == nos of load cells in error
    if ((load_cell_error > 0) && !(Flags_struct.Error_flags & LOAD_CELL_DATA_ERR))
    {
       Flags_struct.Error_flags |= LOAD_CELL_DATA_ERR;
       Flags_struct.Err_Wrn_log_written |= LOAD_CELL_DATA_ERR_LOGGED;
    }
    else if (load_cell_error <= 0)
    {
       Flags_struct.Error_flags &= ~LOAD_CELL_DATA_ERR;
       GUI_data_nav.Error_wrn_msg_flag[LOAD_CELL_DATA_ERR_TIME] = 0;
    }
    //if angle is greater than 35 degrees
    if (((Calculation_struct.Angle > ANGLE_SENSOR_LIMIT) || (Calculation_struct.Angle < -ANGLE_SENSOR_LIMIT)) && !(Flags_struct.Error_flags & ANGLE_SENSOR_ERR))
    {
			u8AngleErrorCount++;
			if(u8AngleErrorCount >= 100)
			{
       Flags_struct.Error_flags |= ANGLE_SENSOR_ERR;
       Flags_struct.Err_Wrn_log_written |= ANGLE_SENSOR_ERR_LOGGED;
			}
    }
    else if ((Calculation_struct.Angle >= -ANGLE_SENSOR_LIMIT) && (Calculation_struct.Angle <= ANGLE_SENSOR_LIMIT))
    {
			 u8AngleErrorCount = 0;
       Flags_struct.Error_flags &= ~ANGLE_SENSOR_ERR;
       GUI_data_nav.Error_wrn_msg_flag[ANGLE_SENSOR_ERR_TIME] = 0;
    }
    //if rate is negative
    //By DK on 29 April 2016 for use of Calculation_struct.Rate_for_display instead of Calculation_struct.Rate BS-152
		if ((Calculation_struct.Rate_for_display < Admin_var.Negative_rate_limit) &&
        !(Flags_struct.Error_flags & NEGATIVE_RATE_ERR))
    {
      if (negative_rate_time >= (Admin_var.Negative_rate_time * MILLI_SEC_TO_SEC_CONV_FACTOR))
      {
         Flags_struct.Error_flags |= NEGATIVE_RATE_ERR;
         Flags_struct.Err_Wrn_log_written |= NEGATIVE_RATE_ERR_LOGGED;
      }
      else //if(Calculation_struct.Rate > Admin_var.Negative_rate_limit)
      {
         negative_rate_time += SENSOR_TASK_INTVL;
      }
    }
    else if ((Calculation_struct.Rate_for_display >= Admin_var.Negative_rate_limit || IO_board_param.Error_ack_flag == __TRUE) && 
			       (Flags_struct.Error_flags & NEGATIVE_RATE_ERR))
    {
       negative_rate_time = 0;
       GUI_data_nav.Error_wrn_msg_flag[NEGATIVE_RATE_ERR_TIME] = 0;
			 Flags_struct.Error_flags &= ~NEGATIVE_RATE_ERR;
    }
		if(Admin_var.Test_load_status == Screen431_str2 || Admin_var.Test_speed_status == Screen431_str2)	//TODO
		{
			//TODO
		}
    return;
}
#endif //#ifdef CALCULATION
/*****************************************************************************
* End of file
*****************************************************************************/
