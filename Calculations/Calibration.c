/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Calibration.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : 14th February Thursday, 2013  <Feb 14, 2013>
* @date Last Modified  : 14th February Thursday, 2013  <Feb 14, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Ext_flash_high_level.h"
#include "Log_report_data_calculate.h"
#include "Sensor_board_data_process.h"
#include "Global_ex.h"
#include "Screen_data_enum.h"
#include "Calibration.h"
#include "IOProcessing.h"
#include "Conversion_functions.h"
#include "Plant_connect.h"
#include "EEPROM_high_level.h"
#include "RTOS_main.h"
#include "Modbus_tcp.h"
#include "Firmware_upgrade.h"
#ifdef DEBUG_PORT
#include "I2C_driver.h"
#endif
#include "EMAC_LPC177x_8x.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define STATIC_ZERO_CAL_PKT_CNT 200
#define TEST_CAL_MAX_PULSES     250000
#define AZ_CORRECT_FACT	32
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL Flash_Write_In_progress = 0; //Added on 27 April 2016  
BOOL enable_clr_wt_write_flag =0; //Added on 27 April 2016  
BOOL clr_wt_cmd_recd = 0; //Added on 27 April 2016  

BOOL clear_weight_in_process = 0; //Added by DK on 4 May2016
BOOL Dynamic_zero_cal_done_fg = 0;
/* Character variables section */
U8 enable_clr_wt_write_counter =0; //Added on 27 April 2016
U8 Calib_Cancel_flag = 0;

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */
float Ref_Zero_weight,Old_auto_zero_offset=0,New_auto_zero_offset=0,Zero_wt_tolerance_low,Zero_wt_tolerance_high;
float New_zero_wt = 0,Old_zero_wt=0,Idler_spacing=0,offset1=0,Calc_zero_wt=0;
float LenPulses1=0, CalPulses1=0,ZeroWeight1 = 0;;
/* Double variables section */

/* Structure or Union variables section */


/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
CALIB_STRUCT_DEF Calib_struct;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void test_weight_cal (void);
void material_test_cal (void);
void digital_cal (void);
void dynamic_zero_cal (void);
void static_zero_cal (void);
void clear_weight (void);
void LengthAndZero(void);
void AutoBeltLength(void);
void Restore_Calib_Value_Upon_Cancel_Calibration_Process(void);
void Power_on_auto_zero_cal (void);
void Auto_zero_cycle(void);
int az_cal_on =0;
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: void test_weight_cal (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Viren Dobariya
* @date       date created : April 15, 2013
* @brief      Description  : Performs test weight calibration
* @note       Notes        : Megha Gaikwad modified this function on 4th Nov 2015 
														 for BS-92 fix
*****************************************************************************/
void test_weight_cal (void)
{
    float ActualWeight, CalculatedLoad, CalibDist, CalculatedWt;
    static float StartWt;
    static unsigned int Revolution_pulses;
    static unsigned int CalPulses;
	  // Accumilated weight before starting Calibration
    calib_data_log.Accum_weight = Calculation_struct.Total_weight;
  #ifdef CALCULATION
    Calibration_var.old_span_value = Calculation_struct.Total_trim_factor;
  #endif
    if(!Calib_struct.Start_flag)
    {
			log_data_populate(periodic_tst_wt_log);			
      ActualWeight = 0;
      CalPulses = 0;
      StartWt = 0;
      Calib_struct.Start_flag = 1;
      Revolution_pulses = (unsigned int)(4 * Calculation_struct.Belt_length_calc \
                                          / Calculation_struct.Dist_per_pulse);
      if (Revolution_pulses > TEST_CAL_MAX_PULSES)
      {
         Revolution_pulses = TEST_CAL_MAX_PULSES;
      }
    }
    else
    {
      if(StartWt == 0)
      {
        #ifdef CALCULATION
        StartWt = Calculation_struct.Total_weight_accum;
        #endif
      }
      else
      {
        #ifdef CALCULATION
        CalPulses += Sens_brd_param.No_of_pulses;
        #endif
        if (Revolution_pulses != 0)
        {
            GUI_data_nav.Percent_complete = (CalPulses * 100)/ Revolution_pulses;
        }

        //if(CalPulses >= Scale_setup_var.Pulses_Per_Revolution)
        if(CalPulses >= Revolution_pulses)
        {
           #ifdef CALCULATION
            ActualWeight = Calculation_struct.Total_weight_accum - StartWt;

					  #ifdef MODBUS_TCP
					  Calculation_struct.Weight_during_cal = ActualWeight;
					  #endif

            //convert test weight to pounds and idler distance to inches
            CalculatedLoad = (Calibration_var.Test_weight * Calculation_struct.Test_weight_conv_factor) \
                              / (Calculation_struct.Idler_center_dist * Calculation_struct.Idler_dist_conv_factor);
           #endif
            CalibDist = CalPulses * Calculation_struct.Dist_per_pulse;
            CalculatedWt = CalculatedLoad * CalibDist;

            Calibration_var.new_span_value = (CalculatedWt/ActualWeight)*Calculation_struct.Total_trim_factor;
						/* Added on 4th Nov 2015 for BS-92 fix */
					  if(Calibration_var.new_span_value <= 0)
						{
							Calibration_var.new_span_value = Calibration_var.old_span_value;
						}

            if (Calibration_var.old_span_value != 0)
            {
              Calibration_var.span_diff = (Calibration_var.new_span_value - Calibration_var.old_span_value) * 100.0 \
                                          / Calibration_var.old_span_value;
            }
            else
            {
               Calibration_var.span_diff = Calibration_var.new_span_value;
            }
          #ifdef CALCULATION
            //Rate calculation
            Calibration_var.real_time_rate = Calculation_struct.Rate;
          #endif

          Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
          GUI_data_nav.Percent_complete = 100;
          Calib_struct.Calib_log_flag = CALIB_DATA_LOG_WRITE;
          Calib_struct.Start_flag = 0;
					Calib_struct.Plant_connect_write_flag = TSTWT_PLNT_CNCT_WRITE;
        }
        //set the flag to move the progress bar
        Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
      }
    }
    return;
}
/*****************************************************************************
* @note       Function name: void material_test_cal (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Viren Dobariya
* @date       date created : April 15, 2013
* @brief      Description  : Performs material test calibration
* @note       Notes        : Megha Gaikwad modified this function on 4th Nov 2015 
														 for BS-92 fix
*****************************************************************************/
void material_test_cal (void)
{
    double NewWeight;

	 #ifdef MODBUS_TCP
	  Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight_accum;
   #endif

  #ifdef CALCULATION
    Calibration_var.old_span_value = Calculation_struct.Total_trim_factor;
    NewWeight = ConvWeightUnit(Calibration_var.Cert_scale_unit,
                               Calibration_var.Belt_scale_unit,
                               Calibration_var.Cert_scale_weight);

    Calibration_var.new_span_value = (NewWeight / Calibration_var.Belt_scale_weight) * \
                                       Calculation_struct.Total_trim_factor;
		/* Added on 4th Nov 2015 for BS-92 fix */
		if(Calibration_var.new_span_value <= 0)
		{
			Calibration_var.new_span_value = Calibration_var.old_span_value;
		}
  #endif

    if (Calibration_var.old_span_value != 0)
    {
        Calibration_var.span_diff = ((Calibration_var.new_span_value - Calibration_var.old_span_value) * 100.0) \
                                     /Calibration_var.old_span_value;
    }
    else
    {
       Calibration_var.span_diff = Calibration_var.new_span_value;
    }

    #ifdef CALCULATION
    Calibration_var.real_time_rate = Calculation_struct.Rate;
    #endif

    #ifdef MODBUS_TCP					
		Calculation_struct.Weight_during_cal = (Calculation_struct.Total_weight - Calculation_struct.Weight_during_cal);
		#endif

    GUI_data_nav.Percent_complete = 100;
    //set the flag to move the progress bar
    Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;

    Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
    Calib_struct.Calib_log_flag = CALIB_DATA_LOG_WRITE;
		Calib_struct.Plant_connect_write_flag = MAT_TST_PLNT_CNCT_WRITE;
    return;
}

/*****************************************************************************
* @note       Function name: void digital_cal (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Viren Dobariya
* @date       date created : April 15, 2013
* @brief      Description  : Performs digital calibration
* @note       Notes        : None
*****************************************************************************/
void digital_cal (void)
{
   #ifdef CALCULATION
    Calibration_var.old_span_value = Calculation_struct.Total_trim_factor;
  #endif
    Calibration_var.new_span_value = 1.0;

   #ifdef MODBUS_TCP	
	 Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight;
	 #endif

    if (Calibration_var.old_span_value != 0)
    {
        Calibration_var.span_diff = ((Calibration_var.new_span_value - Calibration_var.old_span_value) * 100.0) \
                                     /Calibration_var.old_span_value;
    }
    else
    {
        Calibration_var.span_diff = Calibration_var.new_span_value;
    }
    #ifdef CALCULATION
    Calibration_var.real_time_rate = Calculation_struct.Rate;
    #endif

    Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;

		#ifdef MODBUS_TCP					
		Calculation_struct.Weight_during_cal = (Calculation_struct.Total_weight - Calculation_struct.Weight_during_cal);
		#endif

    GUI_data_nav.Percent_complete = 100;
    //set the flag to move the progress bar
    Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
    Calib_struct.Calib_log_flag = CALIB_DATA_LOG_WRITE;
		Calib_struct.Plant_connect_write_flag = MAN_CAL_PLNT_CNCT_WRITE;
    return;
}

/*****************************************************************************
* @note       Function name: void static_zero_cal (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Viren Dobariya
* @date       date created : April 15, 2013
* @brief      Description  : Performs static calibration
* @note       Notes        : Megha Gaikwad modified this function on 4th Nov 2015 
														 for BS-92 fix
*****************************************************************************/
void static_zero_cal (void)
{
  static unsigned short SensorBoardPktCnt;
  static float ZeroWeight;
az_cal_on =1;
  if(Sens_brd_param.No_of_pulses == 0)
  {
    if(!Calib_struct.Start_flag)
    {
			log_data_populate(periodic_zer_log);															
      #ifdef CALCULATION
      Calibration_var.Old_zero_value = Calculation_struct.Zero_weight * Calculation_struct.Zero_value_conv_factor;
      #endif
      SensorBoardPktCnt = 0;
      ZeroWeight = 0.0;
      Calib_struct.Start_flag = 1;

			#ifdef MODBUS_TCP
			Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight;
			#endif
    }
    else
    {
      SensorBoardPktCnt++;
      GUI_data_nav.Percent_complete = (SensorBoardPktCnt * 100)/STATIC_ZERO_CAL_PKT_CNT;
      ZeroWeight += Calculation_struct.Total_gross_weight;
      if(SensorBoardPktCnt >= STATIC_ZERO_CAL_PKT_CNT)
      {
        ZeroWeight = ZeroWeight/STATIC_ZERO_CAL_PKT_CNT;
        #ifdef CALCULATION
        Calibration_var.New_zero_value = ZeroWeight * Calculation_struct.Zero_value_conv_factor;
				/* Added on 4th Nov 2015 for BS-92 fix */
				if(Calibration_var.New_zero_value <= 0)
				{
					Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
				}
        #endif
        Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
        Calib_struct.Start_flag = 0;
        Calib_struct.Calib_log_flag = SET_ZERO_DATA_LOG_WRITE;

				#ifdef MODBUS_TCP					
				Calculation_struct.Weight_during_cal = (Calculation_struct.Total_weight - Calculation_struct.Weight_during_cal);
				#endif
      }
    }
    //set the flag to move the progress bar
    Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
  }
  return;
}
/*****************************************************************************
* @note       Function name: void dynamic_zero_cal (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Viren Dobariya
* @date       date created : April 15, 2013
* @brief      Description  : Performs dynamic calibration
* @note       Notes        : Megha Gaikwad modified this function on 4th Nov 2015 
														 for BS-92 fix
*****************************************************************************/

void dynamic_zero_cal (void)
{
  static float LenPulses, CalPulses, Len_pulses_original;
  static float ZeroWeight;
az_cal_on =1;
  if(Sens_brd_param.No_of_pulses != 0)
  {
    if(!Calib_struct.Start_flag)
    {
			log_data_populate(periodic_zer_log);																											
      #ifdef CALCULATION
      Calibration_var.Old_zero_value = Calculation_struct.Zero_weight * \
                                             Calculation_struct.Zero_value_conv_factor;
      #endif
      LenPulses = Calculation_struct.Belt_length_calc / Calculation_struct.Dist_per_pulse;
      Len_pulses_original = LenPulses;
      ZeroWeight = CalPulses = 0;
      Calib_struct.Start_flag = 1;
			
			#ifdef MODBUS_TCP
			Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight;
			#endif
    }
    else
    {
        LenPulses -=  Sens_brd_param.No_of_pulses;
        CalPulses += Sens_brd_param.No_of_pulses;
        #ifdef CALCULATION
        ZeroWeight += (Calculation_struct.Total_gross_weight * Sens_brd_param.No_of_pulses);
        #endif

        if (Len_pulses_original != 0)
        {
          GUI_data_nav.Percent_complete = (CalPulses * 100)/ Len_pulses_original;
        }
        if(LenPulses <= 0)
        {
          #ifdef CALCULATION
          Calibration_var.New_zero_value = (ZeroWeight * Calculation_struct.Zero_value_conv_factor) / CalPulses;
					/* Added on 4th Nov 2015 for BS-92 fix */
					if(Calibration_var.New_zero_value <= 0)
					{
						Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
					}						
          #endif

          if (Calibration_var.Old_zero_value != 0)
          {
            Calibration_var.Zero_diff = ((Calibration_var.New_zero_value - Calibration_var.Old_zero_value) \
                                          /Calibration_var.Old_zero_value) * 100;
          }
          else
          {
            Calibration_var.Zero_diff = Calibration_var.New_zero_value;
          }

          #ifdef CALCULATION
          Calibration_var.real_time_rate = Calculation_struct.Rate;
          #endif
					
          //PVK - Added on 27/4/2016 to updated the zero weight when zero cal initated through Digital input 
					if (( Calib_struct.Cal_status_flag == START_DYNAMICZERO_CAL) && (IO_board_param.Cal_started_flag == __TRUE))
					{
//									Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
//																												 * Calculation_struct.Zero_value_conv_factor;
 							Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
 																												 / Calculation_struct.Zero_value_conv_factor;
					}		
					
          Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
					
          GUI_data_nav.Percent_complete = 100;

          Calib_struct.Start_flag = 0;
          Calib_struct.Calib_log_flag = SET_ZERO_DATA_LOG_WRITE;

          #ifdef MODBUS_TCP					
					Calculation_struct.Weight_during_cal = (Calculation_struct.Total_weight - Calculation_struct.Weight_during_cal);
					#endif
				//	dynamic_cal_on =0;

        }
        //set the flag to move the progress bar
        Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
      }
			 
    } // if the Belt speed is zero LCD will display Screen number: "350000" which asks user to verfy the belt speed.
		
  return;
}

/*****************************************************************************
* @note       Function name: void clear_weight (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : Feb 15, 2013
* @brief      Description  : Logs the clear weight data log and clears the total
*                            accumulated weight till date
* @note       Notes        : None
*****************************************************************************/
void clear_weight (void)
{
	unsigned char i=0;
	
/*	#ifdef DEBUG_PORT
	sprintf(g_buf,"\r\n Entering clear_weight()");
	Uart_i2c_data_send((uint8_t *)g_buf, strlen(g_buf));
	#endif
	*/
	clear_weight_in_process = 1; //Added by DK on 4 May2016  
	
	/*populate the clear weight cal data log and update on the USB drive, if enabled*/
  if (Setup_device_var.Log_clear_wt_data == Screen431_str2)
  {
		log_data_populate(total_clr_wt_log);  
	}
	
	#ifdef MODBUS_TCP

	Calculation_struct.Weight_during_cal = 0;
	plant_connect_struct_write(Calculation_struct.Total_weight, PLANT_CONNECT_NO_CAL_FLAG,
									           PLANT_CONNECT_NO_ERROR_FLAG, PLANT_CONNECT_NO_ERROR_FLAG,
									           PLANT_CONNECT_NO_ERROR_FLAG, PLANT_CONNECT_ZERO_RCRD_FLAG);
	//set the flag for the new record available
  Flags_struct.Plant_connect_record_flag |= NEW_PLANT_CONNECT_RECORD_FLAG;
	#endif
	for(i=6;i<7;i--)
	{
		Rprts_diag_var.LastClearedTotal[i+1]	= Rprts_diag_var.LastClearedTotal[i];
		Rprts_diag_var.Avg_rate_for_mode[i+1]	= Rprts_diag_var.Avg_rate_for_mode[i];
		Rprts_diag_var.RunTime[i+1] 					= Rprts_diag_var.RunTime[i];
		Rprts_diag_var.Clear_Date[i+1].Day  	= Rprts_diag_var.Clear_Date[i].Day;
		Rprts_diag_var.Clear_Date[i+1].Mon  	= Rprts_diag_var.Clear_Date[i].Mon;
		Rprts_diag_var.Clear_Date[i+1].Year 	= Rprts_diag_var.Clear_Date[i].Year;
		Rprts_diag_var.Clear_Time[i+1].Hr   	= Rprts_diag_var.Clear_Time[i].Hr;
		Rprts_diag_var.Clear_Time[i+1].Min  	= Rprts_diag_var.Clear_Time[i].Min;
		Rprts_diag_var.Clear_Time[i+1].Sec  	= Rprts_diag_var.Clear_Time[i].Sec;
		Rprts_diag_var.LastCleared_Weight_unit[i+1] = Rprts_diag_var.LastCleared_Weight_unit[i];
		Rprts_diag_var.LastCleared_Rate_time_unit[i+1] = Rprts_diag_var.LastCleared_Rate_time_unit[i];
	}
	Rprts_diag_var.LastClearedTotal[0]  = Calculation_struct.Total_weight;
	//Added on 17-Nov-2015 to store last cleared weight unit for BS-141 fix
	Rprts_diag_var.LastCleared_Weight_unit[0] = Scale_setup_var.Weight_unit;
		
	if(Calculation_struct.run_time < 0.1)
	{
		Rprts_diag_var.Avg_rate_for_mode[0] = 0;
	}
	else 
	{	
	  //PVK - 11 Jan 2016 removed fixed multiplication factor of 60 and 
		//converted the Avg rate as per selected rate unit 
		//Rprts_diag_var.Avg_rate_for_mode[0] = (Calculation_struct.Total_weight/Calculation_struct.run_time)*60.0;
		if ( Scale_setup_var.Rate_time_unit == Screen26_str2)//hours
		{	 
		   Rprts_diag_var.Avg_rate_for_mode[0] = ((Calculation_struct.Total_weight/Calculation_struct.run_time) * 60.0);
		}
    else//Min
		{	 
		   Rprts_diag_var.Avg_rate_for_mode[0] = (Calculation_struct.Total_weight/Calculation_struct.run_time);
		} 	 
	}	
	
	//Added on 17-Nov-2015 to store last cleared Rate unit for BS-141 fix
	Rprts_diag_var.LastCleared_Rate_time_unit[0] = Scale_setup_var.Rate_time_unit;
	
	Rprts_diag_var.RunTime[0] = Calculation_struct.run_time;
	Rprts_diag_var.Clear_Date[0].Day  = Current_time.RTC_Mday;
	Rprts_diag_var.Clear_Date[0].Mon  = Current_time.RTC_Mon;
	Rprts_diag_var.Clear_Date[0].Year = Current_time.RTC_Year;
	Rprts_diag_var.Clear_Time[0].Hr   = Current_time.RTC_Hour;
	Rprts_diag_var.Clear_Time[0].Min  = Current_time.RTC_Min;
	Rprts_diag_var.Clear_Time[0].Sec  = Current_time.RTC_Sec;
  #ifdef CALCULATION
   Calculation_struct.Total_weight = 0.0;
   Calculation_struct.Total_weight_accum = 0.0;
	 IO_board_param.Delivered_weight_OP1 = 0;
	 IO_board_param.Delivered_weight_OP2 = 0;
	 IO_board_param.Delivered_weight_OP3 = 0;
	 IO_board_param.Delivered_weight_Quad_OP = 0;
	 Calculation_struct.run_time = 0;
	 //PVK
	 //eeprom_weight_backup();
	 /*Store all totals and report parameters in Ext Flash on clear event */
	 
	/* 
	#ifdef DEBUG_PORT
	sprintf(g_buf,"\r\n FlashBackup clear_weight()");
	Uart_i2c_data_send((uint8_t *)g_buf, strlen(g_buf));
	#endif
	*/
	//if(OS_R_OK == os_mut_wait (&FLash_Mutex, FLASH_MUTEX_TIMEOUT))
	{	
		Flash_Write_In_progress = 1; //Added on 27 April 2016 
		
		LPC_EMAC->Command &= (~CR_RX_EN); //Added on 28 April 2016  to disable EMAC receiption 
		LPC_EMAC->Command &= (~CR_TX_EN); //Added on 28 April 2016  to disable EMAC Transmission 
		LPC_EMAC->MAC1     &= ~MAC1_REC_EN;
		LPC_EMAC->IntClear  = 0xFFFF; // Reset all interrupts 

		//	if(enable_clr_wt_write_flag == 1)  //Added on 27 April 2016  
		{	
			//clr_wt_cmd_recd =1 ; //Added on 27 April 2016
			flash_para_backup();
			flash_struct_backup(0);
	//		enable_clr_wt_write_flag = 0; //Added on 27 April 2016  
	//		enable_clr_wt_write_counter =0; //Added on 27 April 2016
		}
		
		//if(clr_wt_cmd_recd == 1 )
		{
		//	LPC_EMAC->IntEnable = INT_RX_DONE | INT_TX_DONE;
			LPC_EMAC->IntClear  = 0xFFFF; // Reset all interrupts 
			LPC_EMAC->Command |= CR_RX_EN; //Added on 28 April 2016
			LPC_EMAC->Command |= CR_TX_EN; //Added on 28 April 2016
			LPC_EMAC->MAC1     |= MAC1_REC_EN;
		}		
		
		Flash_Write_In_progress =0; //Added on 27 April 2016  
		
			
		
		//os_mut_release (&FLash_Mutex);
	}
	// os_mut_release (&FLash_Mutex);
	/* 
	#ifdef DEBUG_PORT
	sprintf(g_buf,"\r\n flash_para_backup() Done");
	Uart_i2c_data_send((uint8_t *)g_buf, strlen(g_buf));
	#endif
	*/
  #endif

  /*populate the periodic data log and update on the USB drive*/
  if (Setup_device_var.Log_run_time_data == Screen431_str2)
  { 
   	log_data_populate(clear_log); 
  }
  //Clear the weight to enter the new load for next batch
  IO_board_param.Batching_Flag = BATCH_START;
/*
	#ifdef DEBUG_PORT
	sprintf(g_buf,"\r\n Exiting clear_weight()");
	Uart_i2c_data_send((uint8_t *)g_buf, strlen(g_buf));
	#endif
*/	
	clear_weight_in_process = 0; //Added by DK on 4 May2016 
  
  return;
}

/*****************************************************************************
* @note       Function name: void LengthAndZero(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Viren Dobariya
* @date       date created : April 15, 2013
* @brief      Description  : Performs length and zero calibration
* @note       Notes        : Megha Gaikwad modified this function on 4th Nov 2015 
														 for BS-92 fix
*****************************************************************************/
extern int first_len_LOG;
void LengthAndZero(void)
{
  static unsigned int LenPulses;
  static float ZeroWeight;
first_len_LOG =0;
	az_cal_on =1;
  if(Sens_brd_param.No_of_pulses != 0)
  {
      if(!Calib_struct.Start_flag)
      {
					log_data_populate(periodic_zer_log);//1st zero																																					
					log_data_populate(periodic_len_log);						
          #ifdef CALCULATION
          Calibration_var.Old_zero_value = Calculation_struct.Zero_weight * \
                                             Calculation_struct.Zero_value_conv_factor;
          Calibration_var.Old_belt_length = Calculation_struct.Belt_length_calc / \
                                             Calculation_struct.Belt_length_conv_factor;
          #endif
          LenPulses = 0;
          ZeroWeight = 0;
          Calib_struct.Start_flag = 1;
          Calib_struct.BeltLenCal_Comp = 0;
				  length_cal_data_log.Prev_nos_pulses = (U16) (Calculation_struct.Belt_length_calc / Calculation_struct.Dist_per_pulse);

				  #ifdef MODBUS_TCP
				  Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight;
				  #endif
      }
      else
      {
          LenPulses +=  Sens_brd_param.No_of_pulses;
          #ifdef CALCULATION
          ZeroWeight += (Calculation_struct.Total_gross_weight * Sens_brd_param.No_of_pulses);
          #endif

          if (Scale_setup_var.Pulses_Per_Revolution != 0)
          {
            GUI_data_nav.Percent_complete = (LenPulses * 100)/ Scale_setup_var.Pulses_Per_Revolution;
          }

          if(Calib_struct.BeltLenCal_Comp == 1)
          {
              Calibration_var.New_belt_length = LenPulses * Calculation_struct.Dist_per_pulse * \
                                                Calculation_struct.Belt_len_conv_fact_display; //in feet or meters
							/* Added on 4th Nov 2015 for BS-92 fix */
							if(Calibration_var.New_belt_length <= 0)
							{
								Calibration_var.New_belt_length = Calibration_var.Old_belt_length;
							}							

              Calibration_var.Belt_length_diff = Calibration_var.New_belt_length - \
                                                    Calibration_var.Old_belt_length; //in feet or meters

              #ifdef CALCULATION
              Calibration_var.New_zero_value = (ZeroWeight * Calculation_struct.Zero_value_conv_factor) \
                                                / LenPulses;
							/* Added on 4th Nov 2015 for BS-92 fix */
							if(Calibration_var.New_zero_value <= 0)
							{
								Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
							}							
              #endif
              if (Calibration_var.Old_zero_value != 0)
              {
                Calibration_var.Zero_diff = ((Calibration_var.New_zero_value - Calibration_var.Old_zero_value)
                                              /Calibration_var.Old_zero_value) * 100;
              }
              else
              {
                Calibration_var.Zero_diff = Calibration_var.New_zero_value;
              }

              #ifdef CALCULATION
                Calibration_var.real_time_rate = Calculation_struct.Rate;
              #endif

							length_cal_data_log.New_nos_pulses = (U16) ((Calibration_var.New_belt_length * \
                                             Calculation_struct.Belt_length_conv_factor) / Calculation_struct.Dist_per_pulse);
              Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
              GUI_data_nav.Percent_complete = 100;

              Calib_struct.Start_flag = 0;
              Calib_struct.BeltLenCal_Comp = 0;
              Calib_struct.Calib_log_flag = LEN_ZER_DATA_LOG_WRITE; //write the length log and set zero log
							
							#ifdef MODBUS_TCP					
					    Calculation_struct.Weight_during_cal = (Calculation_struct.Total_weight - Calculation_struct.Weight_during_cal);
					    #endif
          }
      }
      //set the flag to move the progress bar
      Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
    }

  return;
}

/*****************************************************************************
* @note       Function name: void AutoBeltLength(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Viren Dobariya
* @date       date created : April 15, 2013
* @brief      Description  : Performs auto belt length calibration
* @note       Notes        : Megha Gaikwad modified this function on 4th Nov 2015 
														 for BS-92 fix
*****************************************************************************/
void AutoBeltLength(void)
{
  static unsigned int LenPulses;

  if(Sens_brd_param.No_of_pulses != 0)
  {
      if(!Calib_struct.Start_flag)
      {
        LenPulses = 0;
        Calibration_var.Old_belt_length = Calculation_struct.Belt_length_calc / \
                                             Calculation_struct.Belt_length_conv_factor;
        Calib_struct.Start_flag = 1;
        Calib_struct.BeltLenCal_Comp = 0;
				length_cal_data_log.Prev_nos_pulses = (U16) (Calculation_struct.Belt_length_calc / Calculation_struct.Dist_per_pulse);
				
				#ifdef MODBUS_TCP
				Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight;
				#endif
      }
      else
      {
          LenPulses +=  Sens_brd_param.No_of_pulses;

          if (Calculation_struct.Dist_per_pulse != 0)
          {
              GUI_data_nav.Percent_complete = (LenPulses * 100)/ Scale_setup_var.Pulses_Per_Revolution;
          }
          if(Calib_struct.BeltLenCal_Comp == 1)
          {
              Calibration_var.New_belt_length = LenPulses * Calculation_struct.Dist_per_pulse * \
                                                Calculation_struct.Belt_len_conv_fact_display; //in feet or meters
							/* Added on 4th Nov 2015 for BS-92 fix */
							if(Calibration_var.New_belt_length <= 0)
							{
								Calibration_var.New_belt_length = Calibration_var.Old_belt_length;
							}							

              Calibration_var.Belt_length_diff = Calibration_var.New_belt_length - \
                                                    Calibration_var.Old_belt_length; //in feet or meters

              length_cal_data_log.New_nos_pulses = (U16) ((Calibration_var.New_belt_length * \
                                             Calculation_struct.Belt_length_conv_factor) / Calculation_struct.Dist_per_pulse);
						  Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
              GUI_data_nav.Percent_complete = 100;

              Calib_struct.Start_flag = 0;
              Calib_struct.BeltLenCal_Comp = 0;
              Calib_struct.Calib_log_flag = LENGTH_DATA_LOG_WRITE; //write the length log
						  
						  #ifdef MODBUS_TCP					
					     Calculation_struct.Weight_during_cal = (Calculation_struct.Total_weight - Calculation_struct.Weight_during_cal);
					    #endif
          }
      }
      //set the flag to move the progress bar
      Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
  }

  return;
}

/*****************************************************************************
* @note       Function name  : void Restore_Calib_Value_Upon_Cancel_Calibration_Process(void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Pandurang Khutal
* @date       date created   : 16th May 2016
* @brief      Description    : Restore old cal value when cancel the calibration process
* @note       Notes          :
*****************************************************************************/
void Restore_Calib_Value_Upon_Cancel_Calibration_Process(void)
{
	if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WT_CAL_ORG_NO) == 0)
	{ 
		// Restore the previous span value.
		Calibration_var.new_span_value = Calibration_var.old_span_value; 
		
		if(GUI_data_nav.Percent_complete != 100)
		{
		  Calib_Cancel_flag = 1;
			Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
			GUI_data_nav.Percent_complete = 100;
			Calib_struct.Start_flag = 0;
		}
    else
	  {
      Cal_Reject_Cmd_recd = 1;			
	  }
	}
	else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYNAMIC_ZEROCAL_START) == 0) ||
					 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYN_ZER_CAL_ACPT_ORG_NO) == 0) ||
					 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYNAMIC_ZEROCAL_ACK_ORG_NO) == 0) ||
	         (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, STATIC_ZEROCAL_START) == 0) ||
					 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, STATIC_ZEROCAL_COMPLETE) == 0) ||
					 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_ZER_ORG_NO) == 0)
				  )   
	{
		#ifdef CALCULATION
		Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
		#endif //#ifdef CALCULATION
		
		if(GUI_data_nav.Percent_complete != 100)
		{	
		  Calib_Cancel_flag = 1;
			Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
			GUI_data_nav.Percent_complete = 100;
			Calib_struct.Start_flag = 0;
		}	
		else
		{	
		  Cal_Reject_Cmd_recd = 1;	
		}
			
  }
}

// /*****************************************************************************
// * @note       Function name: void Auto_zero_cycle (void)
// * @returns    returns      : None
// * @param      arg1         : None
// * @author                  : Megha agrawal
// * @date       date created : February, 2017
// * @brief      Description  : Performs  auto zero calibration

// *****************************************************************************/
// void Power_on_auto_zero_cal (void)
void Auto_zero_cycle(void)
{
   float  Len_pulses_original;
   float wt_difference;
	int tmp_cnt;
	
  if((Sens_brd_param.No_of_pulses != 0)&&(az_cal_on==0))//not idle case
  {
    if(!Auto_zero_Start_flag)					//first time auto_zero_cycle
    {
      LenPulses1 = Calculation_struct.Belt_length_calc / Calculation_struct.Dist_per_pulse;			//no. of pulses in a cycle
      ZeroWeight1 = CalPulses1 = 0;			//init to zero
      Auto_zero_Start_flag = 1;					// don't come here again
			New_auto_zero_offset = 0;					//init to zero
			Idler_spacing = (Calculation_struct.Idler_center_dist *	Calculation_struct.Idler_dist_conv_factor);	//calculate idler distance

			Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight;															//weight on which auto_zero needs to be applied
 			Old_zero_wt = Calibration_var.New_zero_value \
			* Calculation_struct.Zero_value_conv_factor;							//read last Zero  Value saved
 		
			// 			Old_zero_wt = Calibration_var.New_zero_value \
// 																								 / Calculation_struct.Zero_value_conv_factor;							//read last Zero  Value saved
// 			
			Zero_wt_tolerance_low = Old_zero_wt*(1 - (Admin_var.Auto_zero_tolerance/100));											//calculate boundary limits low
			Zero_wt_tolerance_high = Old_zero_wt*(1 + (Admin_var.Auto_zero_tolerance/100));											//calculate boundary limits high
			auto_zero_para1.zero_number = Old_zero_wt;																													//copy values to current structure
			auto_zero_para1.Zero_wt_tolerance_low = Zero_wt_tolerance_low;																			//================||===============
			auto_zero_para1.Zero_wt_tolerance_high = Zero_wt_tolerance_high;																		//================||===============
		}
    else
    {
				Idler_spacing = (Calculation_struct.Idler_center_dist *	Calculation_struct.Idler_dist_conv_factor);//calculate idler distance
				Old_auto_zero_offset = New_auto_zero_offset ;																											//
        LenPulses1 -=  Sens_brd_param.No_of_pulses;
        CalPulses1 += Sens_brd_param.No_of_pulses;
        #ifdef CALCULATION

//        ZeroWeight1 += (Calculation_struct.Total_gross_weight * Sens_brd_param.No_of_pulses);
        #endif

        if(LenPulses1 <= 0){	// CYCLE COMPLETED
					LenPulses1 = Calculation_struct.Belt_length_calc / Calculation_struct.Dist_per_pulse;			//no. of pulses in a cycle
						//read previous Zero  number and  Convert values to base unit
						Old_zero_wt =Calibration_var.New_zero_value;// /Calculation_struct.Zero_value_conv_factor;
{
	//					Calibration_var.New_zero_value \
//			* Calculation_struct.Zero_value_conv_factor;							//read last Zero  Value saved
 //				Old_zero_wt = Calibration_var.New_zero_value \
	//			
//																				 / Calculation_struct.Zero_value_conv_factor;	
}
				//stermine low and high tolerance limits for calculations(in base unit)					
					Zero_wt_tolerance_low =(Old_zero_wt)*( 1 - (Admin_var.Auto_zero_tolerance/100));
					Zero_wt_tolerance_high = (Old_zero_wt)*(1 + (Admin_var.Auto_zero_tolerance/100));	
					
					Calc_zero_wt = Calculation_struct.Total_gross_weight*Calculation_struct.Zero_value_conv_factor;;
					// check if zero or less condition holds true
					if((Calc_zero_wt  <= Zero_wt_tolerance_high)&&(Calc_zero_wt  >= Zero_wt_tolerance_low)){
						//calculate deviation
						wt_difference = Calc_zero_wt - (Old_zero_wt);//New_zero_wt;
						
						offset1 = wt_difference / AZ_CORRECT_FACT;			
						//SKS check here
						Calibration_var.New_zero_value = ((Old_zero_wt) + offset1);//New_zero_wt;
						Calculation_struct.Zero_weight =Calibration_var.New_zero_value/Calculation_struct.Zero_value_conv_factor;
						GUI_data_nav.GUI_structure_backup = 1;
						strcpy(auto_zero_para1.status,"TRUE");
							
{//						Calibration_var.New_zero_value \
//																												 * Calculation_struct.Zero_value_conv_factor;
			
// 				Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
// 		
//																										 / Calculation_struct.Zero_value_conv_factor;
}
					}
					else
					{
						strcpy(auto_zero_para1.status,"FALSE");
					}			 
          #ifdef MODBUS_TCP		
					Calculation_struct.Weight_during_cal = Calculation_struct.Total_weight;
					#endif
/* 					auto_zero_para1.zero_number = Old_zero_wt;
// 					auto_zero_para1.Zero_wt_tolerance_low = Zero_wt_tolerance_low;
// 					auto_zero_para1.Zero_wt_tolerance_high = Zero_wt_tolerance_high;				
// 					auto_zero_para1.new_calc_zero_wt = 	Calc_zero_wt;
// 					auto_zero_para1.old_auto_zero_offset = Old_auto_zero_offset;
// 					auto_zero_para1.zero_no_plus_old_azo = Calibration_var.New_zero_value;//New_zero_wt;
// 					auto_zero_para1.zero_wt_difference = wt_difference;
// 					auto_zero_para1.offset1 = offset1;
// 					auto_zero_para1.new_auto_zero_offset = New_auto_zero_offset;
// 					auto_zero_para1.no_of_pulses = LenPulses1;
					*/
        }
      }
			 
    } 
  return;	
	
}

/*****************************************************************************
* End of file
*****************************************************************************/
