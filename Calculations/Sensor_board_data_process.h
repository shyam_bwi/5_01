/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Sensor_board_data_process.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 12, 2012>
* @date Last Modified  : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __SENSOR_BOARD_DATA_PROCESS_H
#define __SENSOR_BOARD_DATA_PROCESS_H

#ifdef CALCULATION
/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "soft_fifo.h"
#include "rtc.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifndef M_PI
  #define M_PI                           3.1415926535
#endif

#define NO_OF_LOADCELL                   8
#define RADIANS( degrees )               ( degrees * M_PI / 180.0 )
#define NOS_PULSES_PER_FEET              96.0

/* For Angle Sensor*/
#define R130                             3400 //3.4K
#define R118                             270  //270R
#define R119                             1000 //1K

#define ANGLE_RANGE                      90
#define MAX_OP_OF_ANGLE_SEN_CHN          4096 //mV
#define ANGLE_SEN_FACTOR                 1.017851

#define SEC_TO_MIN_TO_HOUR_CONV_FACTOR   60.0

#define LBS_TO_SHORT_TON_CONV_FACTOR     0.0005
#define LBS_TO_LONG_TON_CONV_FACTOR      0.000446428571		//0.00044
#define LBS_TO_TONNE_CONV_FACTOR         0.000453592		//0.00045
#define LBS_TO_KG_CONV_FACTOR            0.45359237		//0.4535
#define KG_TO_LBS_CONV_FACTOR            2.204622622		//2.2

#define METER_TO_INCH_CONV_FACTOR        39.3701
#define METER_TO_FEET_CONV_FACTOR        3.2808
#define CENTIMETER_TO_INCH_CONV_FACTOR   0.3937
#define INCH_TO_METER_CONV_FACTOR        0.0254
#define INCH_TO_FEET_CONV_FACTOR         0.0833
#define FEET_TO_INCH_CONV_FACTOR         12
#define INCH_TO_CENTIMETER_CONV_FACTOR   2.54
#define FEET_TO_METER_CONV_FACTOR        0.3048

#define SHORT_TON_TO_TONNE_CONV_FACTOR   0.90718474		//0.907185
#define LONG_TON_TO_TONNE_CONV_FACTOR    1.016046909		//1.01605
#define KG_TO_TONNE_CONV_FACTOR          0.001
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
            {
               U8 Board_Id[8];
               U8 Fm_Ver[9];  //testing on 8/30/13 U8 Fm_Ver[6]
               S8 Heartbeat;
               U16 No_of_pulses;
               float Time;
               float Accum_LC_Perc[NO_OF_LOADCELL];
               float Inst_LC_Perc[NO_OF_LOADCELL];
               float Angle_mv;
               float LC_supply_volt;
               float Amp_ref_volt;
               float Five_volt_supply;
               float Rate_input_4_20ma; //for additional input from the sensor board
            } SENSOR_BOARD_PARAM;

extern SENSOR_BOARD_PARAM Sens_brd_param;

typedef struct
            {
              float LC_capacity[NO_OF_LOADCELL];     /*Capacity of each load cell*/
							
							float Gross_weight[NO_OF_LOADCELL];    /*Gross weight accumulated by each load cell*/
              float Total_gross_weight;              /*Total gross weight of all load cells*/
							
              float Zero_weight;                     /**/
              float Net_weight;                      /**/
              
							float Idler_center_dist;               /*Idler center distance*/
							
							float Belt_load;                       /*In pounds or feet*/
							
              float Dist_travelled;                  /*Distance travelled*/
              float Total_trim_factor;               /*Totalizer trim factor-By trimming load cells
                                                      their outputs are matched to give accurate measurements.*/
              //float Net_total_del_wt;                /*Net total delivered weight*/
							double Net_total_del_wt;                /*Net total delivered weight*/
							// Changed from  float to Double by Venkata Krishna Rao on 31/10/2013
              double Total_weight_accum;              /*Total accumulated weight in pounds used internally*/
							//double Job_Total;                      // An independent total which is relaed to a perticular job
							// Changed from  float to Double by Venkata Krishna Rao on 31/10/2013
              double Total_weight;                    /*Total Accumulated Weight used for display and reports and
                                                       converted according to weight unit*/
							//double Master_total;
              float Belt_speed;
              float Rate;
							
							float Load_cell_perc;                  /*Load cell precentage*/
              
							float Angle;
              float Dist_per_pulse;
              float Belt_length_calc;                /*Belt length in inches used for calculation*/
              //double Rate_setpoint;
              float Target_weight;
              float Cutoff_weight;
              float Rate_from_int_board;
              float Idler_dist_conv_factor;
              //float Weight_conv_factor;
							double Weight_conv_factor;
              float Rate_conv_factor;
              float Wheel_diameter_conv_factor;
              float Speed_conv_factor;
              float Belt_length_conv_factor;
              float Belt_len_conv_fact_display;
              float Test_weight_conv_factor;
              float Zero_value_conv_factor;
							float Weight_during_cal;
							float Rate_for_display; // Rate variable for displaying purpose.
							float run_time;
							float Angle_for_display; // Angle variable for displaying purpose -- added on 27-Nov 2015
            } CALC_STRUCT;
extern Soft_FIFO Rate_disp_FIFO;
						
//Added by DK on 2 May2016 for fixing issue of BS-190 Inaccuracy of Daily Weight in periodic log
/*extern RTCTime log_current_time;		
extern double log_total_weight;		
extern double log_daily_total_weight;*/						
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void Parse_rcvd_data (U8 *rx);
extern void Calculation (void);
extern float Idler_center_dist_calc (void);
#endif      /*#ifdef CALCULATION*/

#endif /*__SENSOR_BOARD_DATA_PROCESS_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
