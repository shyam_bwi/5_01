/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Rate_blending_load_ctrl_calc.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : 18th July Thursday, 2013  <July 18, 2013>
* @date Last Modified  : 18th July Thursday, 2013  <July 18, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __RATE_BLENDING_LOAD_CTRL_CALC_H
#define __RATE_BLENDING_LOAD_CTRL_CALC_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "soft_fifo.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifdef RATE_BLEND_LOAD_CTRL_CALC
 #define WINDUP_FLAG                    0x01
 #define PRELOAD_TIMER_START            0x02
 #define PRELOAD_TIMER_STOP             (~PRELOAD_TIMER_START)
 #define RELOAD_SET_RATE_PARAM_FLAG     0x04
 #define FEED_TIMER_START               0x08
 #define FEED_TIMER_STOP                (~FEED_TIMER_START)
 #define TIMER_START										1
 #define TIMER_STOP											2
 
 #define RATE_CONTROL_SCREEN_FLAG       0x10
 #define BLEND_CONTROL_SCREEN_FLAG      0x20
 #define LOAD_CONTROL_SCREEN_FLAG       0x40
 #define PACKET_INVL										(float)(1000/(10*SENS_TO_INT_BOARD_MODBUS_RTU_COMM_TASK_INTERVAL*Rate_blend_load.packet_cnt)) // In terms of milliseconds
 #define TIMEUNIT_TO_INVL_UNITS					((float)PACKET_INVL*60*Calculation_struct.Rate_conv_factor) 
// #define RATE_CONV_CURR_200MS						((Calculation_struct.Weight_conv_factor)*(Calculation_struct.Rate_conv_factor)*(60.0)*(1000.0/200.0))
#define RATE_CONV_CURR_200MS						((Calculation_struct.Weight_conv_factor)*(Calculation_struct.Rate_conv_factor)*(60.0)*(1000.0/Rate_blend_load.Delta_time))
 #define DELTA_TIME_HOURS								(Calculation_struct.Rate_conv_factor*1000*60)
 #define ONE_SEC												1000  // 1000ms

 #define FIFO_SIZE                      80
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
//extern U8 Feed_timer_status;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
              {
                float Bias;
                float P_term;
                float I_term;
                float D_term;
                float I_sum;
                float PID_error;
                float Set_rate;
								float Delta_time;
								float Last_Set_rate;
								float actual_weight;
								U8 packet_cnt;
								Soft_FIFO blend_FIFO;
                int DAC_count;
                unsigned int Preload_delay_timer;
                unsigned int Feed_delay_timer;
								U8 FD_Timer_flag;
								U8 PD_Timer_flag;
								U8 Calculation_flags;
              }RATE_BLEND_LOAD_STRUCT;

extern RATE_BLEND_LOAD_STRUCT Rate_blend_load;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void rate_control_flow (void);
extern void blend_control_flow (void);
#endif //#ifdef RATE_BLEND_LOAD_CTRL_CALC

#endif /*__RATE_BLENDING_LOADOUT_CALC_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
