/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Blending_rate_load_strl_run_mode_screen.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "Global_ex.h"
#include "Rate_blending_load_ctrl_calc.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define WM_APP_SHOW_TEXT (WM_USER + 0)
#define BLENDING_TEXT_EDIT_SIZE_X    280
#define BLENDING_TEXT_EDIT_SIZE_Y    130
#define BLENDING_TEXT_EDIT_POS_X     5
#define BLENDING_TEXT_EDIT_POS_Y     3
//Variable
#define BLENDING_TEXT_EDIT1_SIZE_X    BLENDING_TEXT_EDIT_SIZE_X
#define BLENDING_TEXT_EDIT1_SIZE_Y    70
#define BLENDING_TEXT_EDIT1_POS_X     BLENDING_TEXT_EDIT_POS_X 
#define BLENDING_TEXT_EDIT1_POS_Y     20
//Unit
#define BLENDING_TEXT_EDIT2_SIZE_X    BLENDING_TEXT_EDIT_SIZE_X
#define BLENDING_TEXT_EDIT2_SIZE_Y    BLENDING_TEXT_EDIT_SIZE_Y
#define BLENDING_TEXT_EDIT2_POS_X     BLENDING_TEXT_EDIT_POS_X
#define BLENDING_TEXT_EDIT2_POS_Y     80

#define RATE_LOAD_VAL_TEXTBOX_Y_OFFSET      35
#define RATE_LOAD_VAL_TEXT_XSIZE_OFFSET     30
#define EDIT_BOX_XSIZE                      45
#define RATE_LOAD_UNIT_TEXT_X						    (LOAD_CTRL_MULTIEDIT_SIZE_X-unit_len-5)
#define RATE_LOAD_UNIT_TEXT_Y								18
#define RATE_LOAD_VAL_TEXT_SIZE 	 		    	(RATE_LOAD_UNIT_TEXT_X-75)
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_scrn_blending_load_rate_ctrl(WM_MESSAGE * pMsg);
//static void _cbEdit1(WM_MESSAGE * pMsg);
//static void _cbEdit2(WM_MESSAGE * pMsg);
//static void _cbEdit3(WM_MESSAGE * pMsg);
//static void Set_edit_wdget_properties(EDIT_Handle * edit_widget, int * variable);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : static void callback_scrn_blending_load_rate_ctrl(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 14th May 2013
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_scrn_blending_load_rate_ctrl(WM_MESSAGE * pMsg)
{
    int Id, NCode, Key_entry, i;
    static BLEND_RATE_LOAD_CTRL_RUNMODE_SCREEN_DATA *data;
    Screen_org_data * new_screen_org;
    static int sel;
		U8 current_level = 0;
		U8 new_level = 0;
    data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_APP_SHOW_TEXT:
          WM_DisableWindow(GUI_data_nav.Edit_widget1);
          WM_DisableWindow(GUI_data_nav.Edit_widget2);
          WM_DisableWindow(GUI_data_nav.Edit_widget3);
          WM_SetFocus(GUI_data_nav.Listbox);
          LISTBOX_SetSel(GUI_data_nav.Listbox, sel);
          break;

        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            break;

                        case WM_NOTIFICATION_SEL_CHANGED:
                            break;
                    }
                 break;
            }
         break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            break;

                        case GUI_KEY_LEFT_CUSTOM:
															current_level = GUI_data_nav.Current_screen_info->Screen_org_no[2];
															if(current_level > '0' && current_level <='8')
																new_level = (current_level - 1)- 0x30;
															else
																new_level = current_level - 0x30;
                              //new_screen_org = GUI_data_nav.Current_screen_info->child;
														  //new_screen_org = Org_760000_children;
                              //new_screen_org = &new_screen_org[new_level-1];												
                              for (i = 0; i < 6; i++)
                              {
                                  GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->Screen_org_no[i];
                              }
															GUI_data_nav.New_screen_num[1] = '6';
															GUI_data_nav.New_screen_num[2] = new_level+0x30;
                              GUI_data_nav.Change = 1;
                              GUI_data_nav.Child_present = 0;														
                            break;

                        case GUI_KEY_RIGHT:
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
															current_level = GUI_data_nav.Current_screen_info->Screen_org_no[2];
															if(current_level >= '0' && current_level <'8')
															{
																new_level = (current_level + 1)- 0x30;
															}
															else if(current_level == '8')
															{
																new_level = 0;
															}
															else
															{
																new_level = current_level - 0x30;
															}
                              //new_screen_org = GUI_data_nav.Current_screen_info->child;
															//new_screen_org = Org_760000_children;
                              //new_screen_org = &new_screen_org[new_level-1];												
                              for (i = 0; i < 6; i++)
                              {
                                  GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->Screen_org_no[i];
                              }
															GUI_data_nav.New_screen_num[1] = '6';															
															GUI_data_nav.New_screen_num[2] = new_level+0x30;
                              GUI_data_nav.Change = 1;
                              GUI_data_nav.Child_present = 0;																
                            break;

                        case GUI_KEY_UP:
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            break;

                        case GUI_KEY_DOWN:
                            break;

                        case GUI_KEY_ENTER:
                            switch (sel)
                            {
                                case 0:
																case 1:
																case 2:
                                  new_screen_org = GUI_data_nav.Current_screen_info->child;
                                  new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                  for (i = 0; i < 6; i++)
                                  {
                                      GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                  }
                                  //GUI_data_nav.New_screen_num[6] = '\0';
                                  GUI_data_nav.Change = 1;
                                  GUI_data_nav.Child_present = 1;																	
																	break;
//                                case 1:
//                                   new_screen_org = GUI_data_nav.Current_screen_info->child;
//                                   new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
//                                   for (i = 0; i < 6; i++)
//                                   {
//                                       GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
//                                   }
//                                   //GUI_data_nav.New_screen_num[6] = '\0';
//                                   GUI_data_nav.Change = 1;
//                                   GUI_data_nav.Child_present = 1;
//                                   WM_SetFocus(GUI_data_nav.Edit_widget1);
//                                   WM_EnableWindow(GUI_data_nav.Edit_widget1);
//                                   EDIT_SetSel(GUI_data_nav.Edit_widget1, 2, 2);
// 																
//                                   break;

//                                 case 2:
//                                   //focus given to P term edit box
//                                   new_screen_org = GUI_data_nav.Current_screen_info->child;
//                                   new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
//                                   for (i = 0; i < 6; i++)
//                                   {
//                                       GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
//                                   }
//                                   //GUI_data_nav.New_screen_num[6] = '\0';
//                                   GUI_data_nav.Change = 1;
//                                   GUI_data_nav.Child_present = 1;																
//                                   WM_SetFocus(GUI_data_nav.Edit_widget1);
//                                   WM_EnableWindow(GUI_data_nav.Edit_widget1);
//                                   EDIT_SetSel(GUI_data_nav.Edit_widget1, 2, 2);
//                                  break;

//                                 case 3:
//                                   //focus given to I term edit box
//                                   WM_SetFocus(GUI_data_nav.Edit_widget2);
//                                   WM_EnableWindow(GUI_data_nav.Edit_widget2);
//                                   EDIT_SetSel(GUI_data_nav.Edit_widget2, 2, 2);
//                                   break;

//                                 case 4:
//                                   //focus given to D term edit box
//                                   WM_SetFocus(GUI_data_nav.Edit_widget3);
//                                   WM_EnableWindow(GUI_data_nav.Edit_widget3);
//                                   EDIT_SetSel(GUI_data_nav.Edit_widget3, 2, 2);
//                                   break;
                            }
                            break;

                        case GUI_KEY_BACK:
                            break;
                    }
                 break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : static void _cbEdit1(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 14th May 2013
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
// static void _cbEdit1(WM_MESSAGE * pMsg)
// {
//   const WM_KEY_INFO * pInfo;
//   WM_MESSAGE          Msg;

//   switch (pMsg->MsgId)
//   {
//       case WM_KEY:
//           pInfo = pMsg->Data.p;
//           if (pInfo->Key == GUI_KEY_ENTER)
//           {
//              Msg.MsgId   = WM_APP_SHOW_TEXT;
//              Msg.hWinSrc = pMsg->hWin;
//              Setup_device_var.Percent_Ingredient = EDIT_GetValue(GUI_data_nav.Edit_widget1);
//              WM_SendMessage(WM_HBKWIN, &Msg);
//              return;
//           }
//           break;
//   }
//   EDIT_Callback(pMsg);
// }

/*****************************************************************************
* @note       Function name  : static void _cbEdit2(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 14th May 2013
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
// static void _cbEdit2(WM_MESSAGE * pMsg)
// {
//   const WM_KEY_INFO * pInfo;
//   WM_MESSAGE          Msg;

//   switch (pMsg->MsgId)
//   {
//       case WM_KEY:
//           pInfo = pMsg->Data.p;
//           if (pInfo->Key == GUI_KEY_ENTER)
//           {
//              Msg.MsgId   = WM_APP_SHOW_TEXT;
//              Msg.hWinSrc = pMsg->hWin;
//              Setup_device_var.PID_I_Term = EDIT_GetValue(GUI_data_nav.Edit_widget2);
//              WM_SendMessage(WM_HBKWIN, &Msg);
//              return;
//           }
//           break;
//   }
//   EDIT_Callback(pMsg);
// }

/*****************************************************************************
* @note       Function name  : static void _cbEdit3(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 14th May 2013
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
// static void _cbEdit3(WM_MESSAGE * pMsg)
// {
//   const WM_KEY_INFO * pInfo;
//   WM_MESSAGE          Msg;

//   switch (pMsg->MsgId)
//   {
//       case WM_KEY:
//           pInfo = pMsg->Data.p;
//           if (pInfo->Key == GUI_KEY_ENTER)
//           {
//              Msg.MsgId   = WM_APP_SHOW_TEXT;
//              Msg.hWinSrc = pMsg->hWin;
//              Setup_device_var.PID_D_Term = EDIT_GetValue(GUI_data_nav.Edit_widget3);
//              WM_SendMessage(WM_HBKWIN, &Msg);
//              return;
//           }
//           break;
//   }
//   EDIT_Callback(pMsg);
// }

/*****************************************************************************
* @note       Function name  : static void Set_edit_wdget_properties(EDIT_Handle
                             :                    * edit_widget, int * variable)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 14th May 2013
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
/*static void Set_edit_wdget_properties(EDIT_Handle * edit_widget, int * variable)
{
    int pxPos = 0, pyPos = 0, y_size = 0;
		U8 unit_len;
	  unit_len = 160;
    MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);
    y_size = GUI_GetYSizeOfFont(&GUI_FontArial_Unicode_MS24_Bold);

    *edit_widget = EDIT_CreateEx(RATE_LOAD_UNIT_TEXT_X, pyPos, EDIT_BOX_XSIZE, y_size, GUI_data_nav.Multi_text,
                                  WM_CF_SHOW, 0, GUI_ID_EDIT0, 3);
    EDIT_SetInsertMode(*edit_widget, 0);
    EDIT_SetBkColor(*edit_widget, EDIT_CI_DISABLED, GUI_BLACK);
    EDIT_SetBkColor(*edit_widget, EDIT_CI_ENABLED, GUI_YELLOW);
    EDIT_SetFont(*edit_widget, &GUI_FontArial_Unicode_MS24_Bold);
    EDIT_SetTextColor(*edit_widget, EDIT_CI_DISABLED, GUI_YELLOW);
    EDIT_SetTextColor(*edit_widget, EDIT_CI_ENABLED, GUI_BLACK);
    EDIT_SetMaxLen(*edit_widget, 6);
    EDIT_SetFocussable(*edit_widget, 1);
    //EDIT_SetFloatMode(*edit_widget, *variable, 0, 999, 3,GUI_EDIT_SUPPRESS_LEADING_ZEROES|GUI_EDIT_NORMAL);
		EDIT_SetDecMode(*edit_widget, *variable, 0, 999, 0,GUI_EDIT_NORMAL);
    EDIT_SetValue(*edit_widget, *variable);
    WM_DisableWindow(*edit_widget);
    EDIT_SetSel(*edit_widget, 0, -1);
    MULTIEDIT_AddText(GUI_data_nav.Multi_text, "        ");
}
*/
/*****************************************************************************
* @note       Function name  : void Create_loadout_screen(int create_update)
* @returns    returns        : None
* @param      arg1           : Flag to create or update the screen
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets.
* @note       Notes          :
*****************************************************************************/
void Create_blending_load_rate_ctrl_screen (int create_update)
{
    int i, pxPos = 0, pyPos = 0, y_size = 0, x_size = 0;
	  U8 unit_len;
    char disp[ARRAY_SIZE];
    BLEND_RATE_LOAD_CTRL_RUNMODE_SCREEN_DATA *data;

    data = GUI_data_nav.Current_screen_info->screen_data;

    if (create_update == CREATE_SCREEN)
    {
        GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LOAD_CTRL_LISTBOX_SIZE_X,
                                                  LISTBOX_SIZE_Y, WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);

        for (i = 0; i < GUI_data_nav.Current_screen_info->size_of_screen_data; i++)
        {
            LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
        }

        LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
        LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
        LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
        LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
        LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
        LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
        LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
        LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

        LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
        LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
        LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
        LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

        WM_SetCallback(WM_HBKWIN, callback_scrn_blending_load_rate_ctrl);
        LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

        GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(LOAD_CTRL_MULTIEDIT_START_X, MULTI_WIN_OFFSET,
                                                     LOAD_CTRL_MULTIEDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                                     WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
        MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
        MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
        MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
        MULTIEDIT_SetAutoScrollV(GUI_data_nav.Multi_text,0);
        MULTIEDIT_SetAutoScrollH(GUI_data_nav.Multi_text,0);
        MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
        MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
        MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);

        MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
        memset(disp, 0, sizeof(disp));
				strcpy(disp, Strings[Scale_setup_var.Weight_unit].Text);
        strcat(disp, " / ");
        strcat(disp, Strings[*data[0].unit].Text);
        unit_len = GUI_GetStringDistX(disp);
        //-------------------First line of text------------------------
        GUI_data_nav.Text_widget = TEXT_CreateEx(BLENDING_TEXT_EDIT_POS_X, BLENDING_TEXT_EDIT_POS_Y,
                                                 BLENDING_TEXT_EDIT_SIZE_X, BLENDING_TEXT_EDIT_SIZE_Y,
                                                 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																								 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
        TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);
			  TEXT_SetText(GUI_data_nav.Text_widget, Strings[data->Text_index3].Text);

        //-------------------First variable-----------------------------
        GUI_data_nav.Text_widget6 = TEXT_CreateEx(BLENDING_TEXT_EDIT1_POS_X, BLENDING_TEXT_EDIT1_POS_Y,
                                                  BLENDING_TEXT_EDIT1_SIZE_X, BLENDING_TEXT_EDIT1_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW,
																									TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget6, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget6, &GUI_FontArialUnicodeMS72_Bold);
				string_of_Weight(disp,data->variable4);
        //sprintf(disp, "%0.1f", *data->variable1);
        TEXT_SetText(GUI_data_nav.Text_widget6, disp);

        //-------------------First variable unit--------------------------
        GUI_data_nav.Text_widget7 = TEXT_CreateEx(BLENDING_TEXT_EDIT2_POS_X, BLENDING_TEXT_EDIT2_POS_Y,
                                                  BLENDING_TEXT_EDIT2_SIZE_X, BLENDING_TEXT_EDIT2_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW,
																									TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget7, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget7, &GUI_FontArial_Unicode_MS24_Bold);
				//if the current screen is weight mode screen
				sprintf(disp, "%s", Strings[*data->variable3_unit].Text);
				TEXT_SetText(GUI_data_nav.Text_widget7, disp);				
				MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);

				
        //------------------------------actual load/rate string------------------------------
				memset(disp, 0, sizeof(disp));
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n\n\n\n\n\n\n");				
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Text_index1].Text);
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, "  ");

        //------------------------------actual load/rate value------------------------------
        MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);
        y_size = GUI_GetYSizeOfFont(&GUI_FontArialUnicodeMS72_Bold);
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%3.1f", *(float *)data[0].variable1);
        x_size = GUI_GetStringDistX(disp);
        GUI_data_nav.Text_widget = TEXT_CreateEx (pxPos, (pyPos - RATE_LOAD_VAL_TEXTBOX_Y_OFFSET),
                                                   RATE_LOAD_VAL_TEXT_SIZE, y_size,
                                                   GUI_data_nav.Multi_text, WM_CF_SHOW, TEXT_CF_HCENTER, GUI_ID_TEXT0, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArialUnicodeMS72_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget, disp);

        //------------------------------actual load/rate unit------------------------------
        MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);
        y_size = GUI_GetYSizeOfFont(&GUI_FontArial_Unicode_MS16_Bold);
        memset(disp, 0, sizeof(disp));
        strcpy(disp, Strings[Scale_setup_var.Weight_unit].Text);
        strcat(disp, " / ");
        strcat(disp, Strings[*data[0].unit].Text);
        x_size = GUI_GetStringDistX(disp);
        GUI_data_nav.Text_widget1 = TEXT_CreateEx(RATE_LOAD_UNIT_TEXT_X, pyPos, x_size, y_size, GUI_data_nav.Multi_text,
                                                WM_CF_SHOW, 0, GUI_ID_TEXT1, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget1, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArial_Unicode_MS16_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget1, disp);
				MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);

        //------------------------------target load/rate string------------------------------
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n\n\n\n");
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Text_index2].Text);
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, "  ");

        //------------------------------target load/rate value------------------------------
        MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);
        y_size = GUI_GetYSizeOfFont(&GUI_FontArialUnicodeMS72_Bold);
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%3.1f", *(float *)data[0].variable2);
        x_size = GUI_GetStringDistX(disp);
        GUI_data_nav.Text_widget2 = TEXT_CreateEx (pxPos, (pyPos - RATE_LOAD_VAL_TEXTBOX_Y_OFFSET),
                                                    RATE_LOAD_VAL_TEXT_SIZE, y_size,
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, TEXT_CF_HCENTER, GUI_ID_TEXT2, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget2, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget2, &GUI_FontArialUnicodeMS72_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget2, disp);


        //------------------------------target load/rate unit------------------------------
        MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);
        y_size = GUI_GetYSizeOfFont(&GUI_FontArial_Unicode_MS16_Bold);
        memset(disp, 0, sizeof(disp));
        strcpy(disp, Strings[Scale_setup_var.Weight_unit].Text);
        strcat(disp, " / ");
        strcat(disp, Strings[*data[0].unit].Text);
        x_size = GUI_GetStringDistX(disp);
        GUI_data_nav.Text_widget3 = TEXT_CreateEx(RATE_LOAD_UNIT_TEXT_X, pyPos, x_size, y_size, GUI_data_nav.Multi_text,
                                                WM_CF_SHOW, 0, GUI_ID_TEXT3, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget3, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget3, &GUI_FontArial_Unicode_MS16_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget3, disp);

         //------------------------------'Ingredient %' string------------------------------
//          MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n\n");
//          MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[Screen453_str5].Text);

//         //------------------------------Ingredient % value------------------------------
//         Set_edit_wdget_properties (&GUI_data_nav.Edit_widget1, data[1].variable3);
//         WM_SetCallback(GUI_data_nav.Edit_widget1, _cbEdit1);
//         MULTIEDIT_AddText(GUI_data_nav.Multi_text, "        ");

//         //------------------------------'I = ' string------------------------------
//         MULTIEDIT_AddText(GUI_data_nav.Multi_text, "    ");
//         MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[Screen73_str12].Text);

//         //------------------------------I term value------------------------------
//         Set_edit_wdget_properties (&GUI_data_nav.Edit_widget2, data[3].variable3);
//         WM_SetCallback(GUI_data_nav.Edit_widget2, _cbEdit2);
//         //MULTIEDIT_AddText(GUI_data_nav.Multi_text, "      ");

//         //------------------------------'D = ' string------------------------------
//         MULTIEDIT_AddText(GUI_data_nav.Multi_text, "    ");
//         MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[Screen73_str13].Text);

//         //------------------------------D term value------------------------------
//         Set_edit_wdget_properties (&GUI_data_nav.Edit_widget3, data[4].variable3);
//         WM_SetCallback(GUI_data_nav.Edit_widget3, _cbEdit3);
//         //MULTIEDIT_AddText(GUI_data_nav.Multi_text, "      ");

//         //------------------------------Additional strings------------------------------
//         MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n\n\n");
//         MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[Screen73_str14].Text);

        WM_SetFocus(GUI_data_nav.Listbox);
				#ifdef RATE_BLEND_LOAD_CTRL_CALC
				//set the flag to reload the set rate value for calculation
				Rate_blend_load.Calculation_flags |= RELOAD_SET_RATE_PARAM_FLAG;
				#endif
    }
    else //update screen
    {
        memset(disp, 0, sizeof(disp));
				string_of_Weight(disp,data->variable4);
			  TEXT_SetText(GUI_data_nav.Text_widget6, disp);						
        //------------------------------actual load/rate value------------------------------
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%3.1f", *(float *)data[0].variable1);
        TEXT_SetText(GUI_data_nav.Text_widget, disp);
        //------------------------------target load/rate value------------------------------
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%3.1f", *(float *)data[0].variable2);
        TEXT_SetText(GUI_data_nav.Text_widget2, disp);
    }
    return;
}

/*****************************************************************************
* End of file
*****************************************************************************/
