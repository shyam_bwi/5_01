/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Weight_rate_run_mode_screen.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdlib.h>
#include "Screen_global_ex.h"
#include "Sensor_board_data_process.h"
#include "Screen_data_enum.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define WM_APP_SHOW_TEXT (WM_USER + 0)
//Acc. weight
#define RUNSCREEN_TEXT_EDIT_SIZE_X    (LISTBOX2_SIZE_X - 2)
#define RUNSCREEN_TEXT_EDIT_SIZE_Y    30
#define RUNSCREEN_TEXT_EDIT_POS_X     5
#define RUNSCREEN_TEXT_EDIT_POS_Y     3
//Variable
#define RUNSCREEN_TEXT_EDIT1_SIZE_X    RUNSCREEN_TEXT_EDIT_SIZE_X
#define RUNSCREEN_TEXT_EDIT1_SIZE_Y    70
#define RUNSCREEN_TEXT_EDIT1_POS_X     RUNSCREEN_TEXT_EDIT_POS_X 
#define RUNSCREEN_TEXT_EDIT1_POS_Y     20
//Unit
#define RUNSCREEN_TEXT_EDIT2_SIZE_X    RUNSCREEN_TEXT_EDIT_SIZE_X
#define RUNSCREEN_TEXT_EDIT2_SIZE_Y    RUNSCREEN_TEXT_EDIT_SIZE_Y
#define RUNSCREEN_TEXT_EDIT2_POS_X     RUNSCREEN_TEXT_EDIT_POS_X
#define RUNSCREEN_TEXT_EDIT2_POS_Y     80
//Rate
#define RUNSCREEN_TEXT_EDIT3_SIZE_X    105//110
#define RUNSCREEN_TEXT_EDIT3_SIZE_Y    RUNSCREEN_TEXT_EDIT_SIZE_Y
#define RUNSCREEN_TEXT_EDIT3_POS_X     0//2
#define RUNSCREEN_TEXT_EDIT3_POS_Y     130
//Variable
#define RUNSCREEN_TEXT_EDIT4_SIZE_X    220//230
#define RUNSCREEN_TEXT_EDIT4_SIZE_Y    RUNSCREEN_TEXT_EDIT1_SIZE_Y
#define RUNSCREEN_TEXT_EDIT4_POS_X     100//110
#define RUNSCREEN_TEXT_EDIT4_POS_Y     RUNSCREEN_TEXT_EDIT3_POS_Y - 30
//Unit
#define RUNSCREEN_TEXT_EDIT5_SIZE_X    235//240
#define RUNSCREEN_TEXT_EDIT5_SIZE_Y    RUNSCREEN_TEXT_EDIT_SIZE_Y
#define RUNSCREEN_TEXT_EDIT5_POS_X     (RUNSCREEN_TEXT_EDIT4_POS_X + RUNSCREEN_TEXT_EDIT4_SIZE_X + 2)
#define RUNSCREEN_TEXT_EDIT5_POS_Y     RUNSCREEN_TEXT_EDIT3_POS_Y
//Speed
#define RUNSCREEN_TEXT_EDIT6_SIZE_X    RUNSCREEN_TEXT_EDIT3_SIZE_X
#define RUNSCREEN_TEXT_EDIT6_SIZE_Y    RUNSCREEN_TEXT_EDIT_SIZE_Y
#define RUNSCREEN_TEXT_EDIT6_POS_X     RUNSCREEN_TEXT_EDIT3_POS_X
#define RUNSCREEN_TEXT_EDIT6_POS_Y     185
//variable
#define RUNSCREEN_TEXT_EDIT7_SIZE_X    RUNSCREEN_TEXT_EDIT4_SIZE_X
#define RUNSCREEN_TEXT_EDIT7_SIZE_Y    RUNSCREEN_TEXT_EDIT1_SIZE_Y
#define RUNSCREEN_TEXT_EDIT7_POS_X     RUNSCREEN_TEXT_EDIT4_POS_X
#define RUNSCREEN_TEXT_EDIT7_POS_Y     RUNSCREEN_TEXT_EDIT6_POS_Y - 30
//unit
#define RUNSCREEN_TEXT_EDIT8_SIZE_X    RUNSCREEN_TEXT_EDIT5_SIZE_X
#define RUNSCREEN_TEXT_EDIT8_SIZE_Y    RUNSCREEN_TEXT_EDIT_SIZE_Y
#define RUNSCREEN_TEXT_EDIT8_POS_X     RUNSCREEN_TEXT_EDIT7_POS_X + (RUNSCREEN_TEXT_EDIT7_SIZE_X + 2)
#define RUNSCREEN_TEXT_EDIT8_POS_Y     RUNSCREEN_TEXT_EDIT6_POS_Y




/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void callback_weight_run_mode_screen(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : static void callback_weight_run_mode_screen(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Rajeev Gadgil
* @date       date created   : 23rd Dec 2014
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
void callback_weight_run_mode_screen(WM_MESSAGE * pMsg)
{
    int Id, NCode, Key_entry, i;
//    static RUNMODE_SCREEN_DATA *data;
//    Screen_org_data * new_screen_org;
//    static int sel;
		U8 current_level = 0;
		U8 new_level = 0;


    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);      /* Id of widget */
            NCode = pMsg->Data.v;                 /* Notification code */
            switch (Id)
            {
                case ID_MULTI_TEXT_1:
                    switch (NCode)
                    {
                        case WM_NOTIFICATION_SEL_CHANGED:
                            /* Change widget text changing the selection */
                            //sel   = ICONVIEW_GetSel(pMsg->hWinSrc);
                            break;
                    }
                    break;
            }
            break;			
        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_MULTI_TEXT_1: // Notifications sent by 'MULTI TEXT'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            break;

                        case GUI_KEY_LEFT_CUSTOM:
															current_level = GUI_data_nav.Current_screen_info->Screen_org_no[2];
															if(current_level > '0' && current_level <='8')
																new_level = (current_level - 1)- 0x30;
															else
																new_level = current_level - 0x30;
                              //new_screen_org = GUI_data_nav.Current_screen_info->child;
														  //new_screen_org = Org_760000_children;
                              //new_screen_org = &new_screen_org[new_level-1];												
                              for (i = 0; i < 6; i++)
                              {
                                  GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->Screen_org_no[i];
                              }
															GUI_data_nav.New_screen_num[1] = '6';
															GUI_data_nav.New_screen_num[2] = new_level+0x30;
                              GUI_data_nav.Change = 1;
                              GUI_data_nav.Child_present = 0;																	
                            break;													

                        case GUI_KEY_RIGHT:									
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
															current_level = GUI_data_nav.Current_screen_info->Screen_org_no[2];
															if(current_level >= '0' && current_level <'8')
															{
																new_level = (current_level + 1)- 0x30;
															}
															else if(current_level == '8')
															{
																new_level = 0;
															}
															else
															{
																new_level = current_level - 0x30;
															}
                              //new_screen_org = GUI_data_nav.Current_screen_info->child;
															//new_screen_org = Org_760000_children;
                              //new_screen_org = &new_screen_org[new_level-1];												
                              for (i = 0; i < 6; i++)
                              {
                                  GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->Screen_org_no[i];
                              }
															GUI_data_nav.New_screen_num[1] = '6';															
															GUI_data_nav.New_screen_num[2] = new_level+0x30;
                              GUI_data_nav.Change = 1;
                              GUI_data_nav.Child_present = 0;																	
                            break;

                        case GUI_KEY_UP:
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            break;

                        case GUI_KEY_DOWN:
                            break;

                        case GUI_KEY_ENTER:
                            break;

                        case GUI_KEY_BACK:
                            break;
                    }
                 break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}
/*****************************************************************************
* @note       Function name    : void Create_Weight_Run_mode_screen(int create_update_weight_screen_flag)
* @returns    returns          : None
* @param      arg1             : create_update_flag
* @author                      : Pandurang Khutal
* @date       date created     : 28th December 2012
* @brief      Description      : Initialize the scan and return lines of the keypad
* @note       Notes            : None
*****************************************************************************/
void Create_Weight_Run_mode_screen(int create_update_weight_screen_flag)
{
    char disp[ARRAY_SIZE];
    RUNMODE_SCREEN_DATA * data;
	
    data = GUI_data_nav.Current_screen_info->screen_data;

	  TEXT_SetDefaultWrapMode(GUI_WRAPMODE_CHAR);

    if (create_update_weight_screen_flag == CREATE_SCREEN)
    {
			
        GUI_SetBkColor(GUI_BLACK);
        GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(LISTBOX_START, MULTI_WIN_OFFSET,
                                                     LISTBOX2_SIZE_X, LISTBOX_SIZE_Y, WM_HBKWIN,
                                                     WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
        MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
        MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
        MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
        MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
        MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
        MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);
				WM_SetFocus(GUI_data_nav.Multi_text);
        WM_SetCallback(WM_HBKWIN, callback_weight_run_mode_screen);

        //-------------------First line of text------------------------
        GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT_POS_X, RUNSCREEN_TEXT_EDIT_POS_Y,
                                                 RUNSCREEN_TEXT_EDIT_SIZE_X, RUNSCREEN_TEXT_EDIT_SIZE_Y,
                                                 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																								 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
        TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS24_Bold);
			  TEXT_SetText(GUI_data_nav.Text_widget, Strings[data->Text_index1].Text);

        //-------------------First variable-----------------------------
        GUI_data_nav.Text_widget1 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT1_POS_X, RUNSCREEN_TEXT_EDIT1_POS_Y,
                                                  RUNSCREEN_TEXT_EDIT1_SIZE_X, RUNSCREEN_TEXT_EDIT1_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW,
																									TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget1, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArialUnicodeMS72_Bold);
				string_of_Weight(disp,data->variable1);
        //sprintf(disp, "%0.1f", *data->variable1);
        TEXT_SetText(GUI_data_nav.Text_widget1, disp);

        //-------------------First variable unit--------------------------
        GUI_data_nav.Text_widget2 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT2_POS_X, RUNSCREEN_TEXT_EDIT2_POS_Y,
                                                  RUNSCREEN_TEXT_EDIT2_SIZE_X, RUNSCREEN_TEXT_EDIT2_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW,
																									TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget2, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget2, &GUI_FontArial_Unicode_MS24_Bold);
				//if the current screen is weight mode screen
				if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_RUN_MODE_SCREEN_ORG_NO_STR) == 0 )||
					(strncmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR,2) == 0) )
				{
					 sprintf(disp, "%s", Strings[*data->variable1_unit].Text);
				}
				else //if it is a rate mode screen
				{
					 sprintf(disp, "%s / %s", Strings[*data->variable2_unit].Text, Strings[*data->variable1_unit].Text);
				}
				TEXT_SetText(GUI_data_nav.Text_widget2, disp);

        //------------------Second line of text----------------------------
					GUI_data_nav.Text_widget3 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT3_POS_X,RUNSCREEN_TEXT_EDIT3_POS_Y,
																										RUNSCREEN_TEXT_EDIT3_SIZE_X, RUNSCREEN_TEXT_EDIT3_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_RIGHT, GUI_ID_TEXT5, NULL);

					TEXT_SetTextColor(GUI_data_nav.Text_widget3, GUI_WHITE);
					TEXT_SetFont(GUI_data_nav.Text_widget3, &GUI_FontArial_Unicode_MS24_Bold);
					TEXT_SetText(GUI_data_nav.Text_widget3, Strings[data->Text_index2].Text);

        //------------------Second variable---------------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) != 0)
				{
					GUI_data_nav.Text_widget4 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT4_POS_X, RUNSCREEN_TEXT_EDIT4_POS_Y,
																										RUNSCREEN_TEXT_EDIT4_SIZE_X, RUNSCREEN_TEXT_EDIT4_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget4, GUI_YELLOW);
					//TEXT_SetFont(GUI_data_nav.Text_widget4, &GUI_FontArial_Unicode_MS24_Bold);
					TEXT_SetFont(GUI_data_nav.Text_widget4, &GUI_FontArialUnicodeMS72_Bold);
					sprintf(disp, "%0.0f", *data->variable2);
					TEXT_SetText(GUI_data_nav.Text_widget4, disp);
				}
				else
				{
					GUI_data_nav.Text_widget4 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT3_POS_X+100, RUNSCREEN_TEXT_EDIT3_POS_Y,
																										RUNSCREEN_TEXT_EDIT4_SIZE_X, RUNSCREEN_TEXT_EDIT4_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget4, GUI_YELLOW);
					TEXT_SetFont(GUI_data_nav.Text_widget4, &GUI_FontArial_Unicode_MS24_Bold);
					sprintf(disp, "%0.0f", *data->variable2);
					TEXT_SetText(GUI_data_nav.Text_widget4, disp);					
				}
        //-------------------Second variable unit----------------------------
        GUI_data_nav.Text_widget5 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT5_POS_X, RUNSCREEN_TEXT_EDIT5_POS_Y,
                                                  RUNSCREEN_TEXT_EDIT5_SIZE_X, RUNSCREEN_TEXT_EDIT5_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW,
																									TEXT_CF_LEFT, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget5, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget5, &GUI_FontArial_Unicode_MS24_Bold);

				//if the current screen is weight mode screen
// 				if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_RUN_MODE_SCREEN_ORG_NO_STR) == 0)||
// 					(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) == 0) )
// 				{
					 sprintf(disp, "%s / %s", Strings[*data->variable1_unit].Text, Strings[*data->variable2_unit].Text);
// 				}
// 				else //if it is a rate mode screen
// 				{
// 					 sprintf(disp, "%s", Strings[*data->variable2_unit].Text);
// 				}
				TEXT_SetText(GUI_data_nav.Text_widget5, disp);

        //--------------------Third line of text-----------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) != 0)
				{
					GUI_data_nav.Text_widget6 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT6_POS_X, RUNSCREEN_TEXT_EDIT6_POS_Y,
																										RUNSCREEN_TEXT_EDIT6_SIZE_X, RUNSCREEN_TEXT_EDIT6_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_RIGHT, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget6, GUI_WHITE);
					TEXT_SetFont(GUI_data_nav.Text_widget6, &GUI_FontArial_Unicode_MS24_Bold);
					TEXT_SetText(GUI_data_nav.Text_widget6, Strings[data->Text_index3].Text);
				}
				else
				{
					GUI_data_nav.Text_widget6 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT6_POS_X, 158,
																										RUNSCREEN_TEXT_EDIT6_SIZE_X, RUNSCREEN_TEXT_EDIT6_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_RIGHT, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget6, GUI_WHITE);
					TEXT_SetFont(GUI_data_nav.Text_widget6, &GUI_FontArial_Unicode_MS24_Bold);
					TEXT_SetText(GUI_data_nav.Text_widget6, Strings[data->Text_index3].Text);					
				}
        //--------------------Third variable---------------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) != 0 && GUI_data_nav.Current_screen_info->Screen_org_no[2]=='0')
				{ 	       
					GUI_data_nav.Text_widget7 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT7_POS_X, RUNSCREEN_TEXT_EDIT7_POS_Y,
																										RUNSCREEN_TEXT_EDIT7_SIZE_X, RUNSCREEN_TEXT_EDIT7_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget7, GUI_YELLOW);
					//TEXT_SetFont(GUI_data_nav.Text_widget7, &GUI_FontArial_Unicode_MS24_Bold);
					TEXT_SetFont(GUI_data_nav.Text_widget7, &GUI_FontArialUnicodeMS72_Bold);
					sprintf(disp, "%0.0f", *data->variable3);
					TEXT_SetText(GUI_data_nav.Text_widget7, disp);
				}
				else if(GUI_data_nav.Current_screen_info->Screen_org_no[2]>'0')
				{
					GUI_data_nav.Text_widget7 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT7_POS_X, RUNSCREEN_TEXT_EDIT7_POS_Y,
																										RUNSCREEN_TEXT_EDIT7_SIZE_X, RUNSCREEN_TEXT_EDIT7_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget7, GUI_YELLOW);
					//TEXT_SetFont(GUI_data_nav.Text_widget7, &GUI_FontArial_Unicode_MS24_Bold);
					TEXT_SetFont(GUI_data_nav.Text_widget7, &GUI_FontArialUnicodeMS72_Bold);
					sprintf(disp, "%d-%02d", (unsigned int)(((*data->variable3))/60.0),((unsigned int)(*data->variable3)%60));					
					TEXT_SetText(GUI_data_nav.Text_widget7, disp);					
				}					
				else
				{
					GUI_data_nav.Text_widget7 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT3_POS_X+100, RUNSCREEN_TEXT_EDIT7_POS_Y,
																										RUNSCREEN_TEXT_EDIT7_SIZE_X, RUNSCREEN_TEXT_EDIT7_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget7, GUI_YELLOW);
					TEXT_SetFont(GUI_data_nav.Text_widget7, &GUI_FontArial_Unicode_MS24_Bold);
					sprintf(disp, "%0.0f", *data->variable3);
					TEXT_SetText(GUI_data_nav.Text_widget7, disp);					
				}
        //---------------------Third variable unit------------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) != 0 && GUI_data_nav.Current_screen_info->Screen_org_no[2]=='0')
				{ 				
					GUI_data_nav.Text_widget8 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT8_POS_X, RUNSCREEN_TEXT_EDIT8_POS_Y,
																										RUNSCREEN_TEXT_EDIT8_SIZE_X, RUNSCREEN_TEXT_EDIT8_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_LEFT, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget8, GUI_YELLOW);
					TEXT_SetFont(GUI_data_nav.Text_widget8, &GUI_FontArial_Unicode_MS24_Bold);
					sprintf(disp, "%s / Min", Strings[*data->variable3_unit].Text);
					//sprintf(disp, "%s / %s", *data->variable3_unit,Strings[data->Text_index1].Text);
					
					TEXT_SetText(GUI_data_nav.Text_widget8, disp);
				}
				else if(GUI_data_nav.Current_screen_info->Screen_org_no[2]>'0')
				{
					GUI_data_nav.Text_widget8 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT8_POS_X, RUNSCREEN_TEXT_EDIT8_POS_Y,
																										RUNSCREEN_TEXT_EDIT8_SIZE_X, RUNSCREEN_TEXT_EDIT8_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_LEFT, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget8, GUI_YELLOW);
					TEXT_SetFont(GUI_data_nav.Text_widget8, &GUI_FontArial_Unicode_MS24_Bold);
					sprintf(disp, "Hours-Mins");
					TEXT_SetText(GUI_data_nav.Text_widget8, disp);					
				}
				else
				{
					GUI_data_nav.Text_widget8 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT5_POS_X, 158,
																										RUNSCREEN_TEXT_EDIT8_SIZE_X, RUNSCREEN_TEXT_EDIT8_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_LEFT, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget8, GUI_YELLOW);
					TEXT_SetFont(GUI_data_nav.Text_widget8, &GUI_FontArial_Unicode_MS24_Bold);
					sprintf(disp, "%s / %s", Strings[*data->variable1_unit].Text, Strings[*data->variable2_unit].Text);					//sprintf(disp, "%s / %s", *data->variable3_unit,Strings[data->Text_index1].Text);
					
					TEXT_SetText(GUI_data_nav.Text_widget8, disp);					
				}
        //---------------------Fourth Line of Text------------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) == 0)
				{					
					GUI_data_nav.Text_widget9 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT6_POS_X, RUNSCREEN_TEXT_EDIT6_POS_Y,
																										RUNSCREEN_TEXT_EDIT6_SIZE_X, RUNSCREEN_TEXT_EDIT6_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_RIGHT, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget9, GUI_WHITE);
					TEXT_SetFont(GUI_data_nav.Text_widget9, &GUI_FontArial_Unicode_MS24_Bold);
					TEXT_SetText(GUI_data_nav.Text_widget9, Strings[data->Text_index4].Text);					
	      //---------------------Fourth variable------------------------------
					GUI_data_nav.Text_widget10 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT6_POS_X+100, RUNSCREEN_TEXT_EDIT6_POS_Y,
																										RUNSCREEN_TEXT_EDIT7_SIZE_X, RUNSCREEN_TEXT_EDIT7_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget10, GUI_YELLOW);
					TEXT_SetFont(GUI_data_nav.Text_widget10, &GUI_FontArial_Unicode_MS24_Bold);
					sprintf(disp, "%d-%02d", (unsigned int)(((*data->variable4))/60.0),((unsigned int)(*data->variable4)%60));					
					TEXT_SetText(GUI_data_nav.Text_widget10, disp);		
	      //---------------------Fourth variable unit------------------------------
					
					GUI_data_nav.Text_widget11 = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT8_POS_X, RUNSCREEN_TEXT_EDIT8_POS_Y,
																										RUNSCREEN_TEXT_EDIT8_SIZE_X, RUNSCREEN_TEXT_EDIT8_SIZE_Y,
																										GUI_data_nav.Multi_text, WM_CF_SHOW,
																										TEXT_CF_LEFT, GUI_ID_TEXT5, NULL);
					TEXT_SetTextColor(GUI_data_nav.Text_widget11, GUI_YELLOW);
					TEXT_SetFont(GUI_data_nav.Text_widget11, &GUI_FontArial_Unicode_MS24_Bold);
					sprintf(disp, "Hours-Mins");
					
					TEXT_SetText(GUI_data_nav.Text_widget11, disp);					
				}
    }
    else //update screen
    {
        //-------------------First variable-----------------------------
        //sprintf(disp, "%0.1f", *data->variable1);
			  string_of_Weight(disp,data->variable1);
			  TEXT_SetText(GUI_data_nav.Text_widget1, disp);
				//-------------------First variable unit------------------------
				//if the current screen is weight mode screen
// 				if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_RUN_MODE_SCREEN_ORG_NO_STR) == 0)||
// 					(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) == 0) )
// 				{
					 sprintf(disp, "%s", Strings[*data->variable1_unit].Text);
// 				}
// 				else //if it is a rate mode screen
// 				{
// 					 sprintf(disp, "%s / %s", Strings[*data->variable2_unit].Text, Strings[*data->variable1_unit].Text);
// 				}
				TEXT_SetText(GUI_data_nav.Text_widget2, disp);			

       //------------------Second variable---------------------------------
        sprintf(disp, "%0.0f",*data->variable2);
        TEXT_SetText(GUI_data_nav.Text_widget4, disp);
			 //------------------Second variable unit----------------------------
				//if the current screen is weight mode screen
			//	if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_RUN_MODE_SCREEN_ORG_NO_STR) == 0)||
			//		(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) == 0) )					
			//	{
					 sprintf(disp, "%s / %s", Strings[*data->variable1_unit].Text, Strings[*data->variable2_unit].Text);
			//	}
			//	else //if it is a rate mode screen
			//	{
			//		 sprintf(disp, "%s", Strings[*data->variable2_unit].Text);
			//	}
				TEXT_SetText(GUI_data_nav.Text_widget5, disp);				
			

        //--------------------Third variable---------------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) != 0 &&  GUI_data_nav.Current_screen_info->Screen_org_no[2]==0)
				{				
        sprintf(disp, "%0.0f",*data->variable3);
        TEXT_SetText(GUI_data_nav.Text_widget7, disp);
				}
				else if(GUI_data_nav.Current_screen_info->Screen_org_no[2]>'0')
				{
					sprintf(disp, "%d-%02d", (unsigned int)(((*data->variable3))/60.0),((unsigned int)(*data->variable3)%60));	
					TEXT_SetText(GUI_data_nav.Text_widget7, disp);					
				}
				else
				{
					sprintf(disp, "%0.0f",*data->variable3);
					TEXT_SetText(GUI_data_nav.Text_widget7, disp);					
				}
				//--------------------Third variable unit----------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) != 0 &&  GUI_data_nav.Current_screen_info->Screen_org_no[2]==0)
				{
					sprintf(disp, "%s / Min", Strings[*data->variable3_unit].Text);
					TEXT_SetText(GUI_data_nav.Text_widget8, disp);
				}
        //--------------------Fourth variable---------------------------------
				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR) == 0)
				{				
					//sprintf(disp, "%0.2f",*data->variable4);
					sprintf(disp, "%d-%02d", (unsigned int)(((*data->variable4))/60.0),((unsigned int)(*data->variable4)%60));					
					TEXT_SetText(GUI_data_nav.Text_widget10, disp);				
				}
    }
    return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
