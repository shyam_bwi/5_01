/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Load_out_run_mode_screen.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Thursday, May 9, 2013
* @date Last Modified  : Thursday, May 9, 2013
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef  LOAD_OUT_RUN_MODE_SCREEN
#define  LOAD_OUT_RUN_MODE_SCREEN

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Sensor_board_data_process.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
__align(4) LOADOUT_RUNMODE_SCREEN_DATA Org_720000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen72_str1, Screen72_str2, Screen72_str3, Screen72_str4, Screen72_str5, &Calculation_struct.Total_weight,
      &Calculation_struct.Target_weight, &Calculation_struct.Rate_for_display, &Calculation_struct.Belt_speed, 0},
    {Screen72_str6, Screen72_str7, 0, 0, 0, NULL, NULL, NULL, NULL, 1},
};
/*============================================================================
* Public Function Declarations
*===========================================================================*/
#endif
/*****************************************************************************
* End of file
*****************************************************************************/
