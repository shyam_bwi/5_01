/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Load_out_run_mode_screen.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "Global_ex.h"
#include "IOProcessing.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

//weight accumulated string
#define LOADOUT_SCREEN_TEXT_EDIT_POS_X        2
#define LOADOUT_SCREEN_TEXT_EDIT_POS_Y        2
#define LOADOUT_SCREEN_TEXT_EDIT_SIZE_X       212
#define LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y       25

//weight unit
#define LOADOUT_SCREEN_TEXT_EDIT2_POS_X       (LOADOUT_SCREEN_TEXT_EDIT_SIZE_X)
#define LOADOUT_SCREEN_TEXT_EDIT2_POS_Y       LOADOUT_SCREEN_TEXT_EDIT_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT2_SIZE_X      100
#define LOADOUT_SCREEN_TEXT_EDIT2_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

//weight value
#define LOADOUT_SCREEN_TEXT_EDIT1_POS_X       LOADOUT_SCREEN_TEXT_EDIT_POS_X
#define LOADOUT_SCREEN_TEXT_EDIT1_POS_Y      (LOADOUT_SCREEN_TEXT_EDIT_POS_Y + LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y)
#define LOADOUT_SCREEN_TEXT_EDIT1_SIZE_X      570
#define LOADOUT_SCREEN_TEXT_EDIT1_SIZE_Y      60

//weight unit
/*#define LOADOUT_SCREEN_TEXT_EDIT2_POS_X       (LOADOUT_SCREEN_TEXT_EDIT1_POS_X + LOADOUT_SCREEN_TEXT_EDIT1_SIZE_X + 10)
#define LOADOUT_SCREEN_TEXT_EDIT2_POS_Y       LOADOUT_SCREEN_TEXT_EDIT1_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT2_SIZE_X      100
#define LOADOUT_SCREEN_TEXT_EDIT2_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT1_SIZE_Y
*/
//---------------------------------------------------------------------------
//current target Wt string
#define LOADOUT_SCREEN_TEXT_EDIT3_POS_X       LOADOUT_SCREEN_TEXT_EDIT_POS_X
#define LOADOUT_SCREEN_TEXT_EDIT3_POS_Y       (LOADOUT_SCREEN_TEXT_EDIT1_POS_Y + LOADOUT_SCREEN_TEXT_EDIT1_SIZE_Y + 30)
#define LOADOUT_SCREEN_TEXT_EDIT3_SIZE_X      twt_strg_size+35
#define LOADOUT_SCREEN_TEXT_EDIT3_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y + 20

//current rate string
#define LOADOUT_SCREEN_TEXT_EDIT6_POS_X       ((LOADOUT_SCREEN_TEXT_EDIT3_POS_X + twt_strg_size) - rate_strg_size)+13//LOADOUT_SCREEN_TEXT_EDIT_POS_X+20 //20
#define LOADOUT_SCREEN_TEXT_EDIT6_POS_Y       (LOADOUT_SCREEN_TEXT_EDIT3_POS_Y + 40)
#define LOADOUT_SCREEN_TEXT_EDIT6_SIZE_X      rate_strg_size+35
#define LOADOUT_SCREEN_TEXT_EDIT6_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

//current speed string
#define LOADOUT_SCREEN_TEXT_EDIT9_POS_X       ((LOADOUT_SCREEN_TEXT_EDIT3_POS_X + twt_strg_size) - speed_strg_size)+10//LOADOUT_SCREEN_TEXT_EDIT_POS_X+15//LOADOUT_SCREEN_TEXT_EDIT6_POS_X
#define LOADOUT_SCREEN_TEXT_EDIT9_POS_Y       (LOADOUT_SCREEN_TEXT_EDIT6_POS_Y + 40)
#define LOADOUT_SCREEN_TEXT_EDIT9_SIZE_X      speed_strg_size+35
#define LOADOUT_SCREEN_TEXT_EDIT9_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

//---------------------------------------------------------------------------
//current setpoint value
#define LOADOUT_SCREEN_TEXT_EDIT4_POS_X       (LOADOUT_SCREEN_TEXT_EDIT3_POS_X + LOADOUT_SCREEN_TEXT_EDIT3_SIZE_X + 0)
#define LOADOUT_SCREEN_TEXT_EDIT4_POS_Y       LOADOUT_SCREEN_TEXT_EDIT3_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT4_SIZE_X      95
#define LOADOUT_SCREEN_TEXT_EDIT4_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

//rate value
#define LOADOUT_SCREEN_TEXT_EDIT7_POS_X       (LOADOUT_SCREEN_TEXT_EDIT6_POS_X + LOADOUT_SCREEN_TEXT_EDIT6_SIZE_X - 9)
#define LOADOUT_SCREEN_TEXT_EDIT7_POS_Y       LOADOUT_SCREEN_TEXT_EDIT6_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT7_SIZE_X      95
#define LOADOUT_SCREEN_TEXT_EDIT7_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

//speed value
#define LOADOUT_SCREEN_TEXT_EDIT10_POS_X      (LOADOUT_SCREEN_TEXT_EDIT9_POS_X + LOADOUT_SCREEN_TEXT_EDIT9_SIZE_X - 7)
#define LOADOUT_SCREEN_TEXT_EDIT10_POS_Y      LOADOUT_SCREEN_TEXT_EDIT9_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT10_SIZE_X     95
#define LOADOUT_SCREEN_TEXT_EDIT10_SIZE_Y     LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

//---------------------------------------------------------------------------
//current weight unit
#define LOADOUT_SCREEN_TEXT_EDIT5_POS_X       (LOADOUT_SCREEN_TEXT_EDIT4_POS_X + LOADOUT_SCREEN_TEXT_EDIT4_SIZE_X+8 )
#define LOADOUT_SCREEN_TEXT_EDIT5_POS_Y       LOADOUT_SCREEN_TEXT_EDIT3_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT5_SIZE_X      100
#define LOADOUT_SCREEN_TEXT_EDIT5_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

//rate unit
#define LOADOUT_SCREEN_TEXT_EDIT8_POS_X       (LOADOUT_SCREEN_TEXT_EDIT7_POS_X + LOADOUT_SCREEN_TEXT_EDIT7_SIZE_X+4)
#define LOADOUT_SCREEN_TEXT_EDIT8_POS_Y       LOADOUT_SCREEN_TEXT_EDIT6_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT8_SIZE_X      LOADOUT_SCREEN_TEXT_EDIT5_SIZE_X + 50
#define LOADOUT_SCREEN_TEXT_EDIT8_SIZE_Y      LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y


//speed unit
#define LOADOUT_SCREEN_TEXT_EDIT11_POS_X      (LOADOUT_SCREEN_TEXT_EDIT10_POS_X + LOADOUT_SCREEN_TEXT_EDIT10_SIZE_X+6)
#define LOADOUT_SCREEN_TEXT_EDIT11_POS_Y      LOADOUT_SCREEN_TEXT_EDIT9_POS_Y
#define LOADOUT_SCREEN_TEXT_EDIT11_SIZE_X     LOADOUT_SCREEN_TEXT_EDIT5_SIZE_X +50
#define LOADOUT_SCREEN_TEXT_EDIT11_SIZE_Y     LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen_loadout(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : static void callback_screen_loadout(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen_loadout(WM_MESSAGE * pMsg)
{

    int Id, NCode, sel, Key_entry, i;
    static LOADOUT_RUNMODE_SCREEN_DATA *data;
    Screen_org_data * new_screen_org;
    char char_data[ARRAY_SIZE];
		U8 current_level = 0;
		U8 new_level = 0;

		sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
		data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            break;

                        case WM_NOTIFICATION_SEL_CHANGED:
                            break;
                    }
                 break;

                default:
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CHILD_DELETED:
                            ;
                            break;
                    }
                 break;
            }
         break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            ;
                           break;

                        case GUI_KEY_LEFT_CUSTOM:
															current_level = GUI_data_nav.Current_screen_info->Screen_org_no[2];
															if(current_level > '0' && current_level <='8')
																new_level = (current_level - 1)- 0x30;
															else
																new_level = current_level - 0x30;
                              //new_screen_org = GUI_data_nav.Current_screen_info->child;
														  //new_screen_org = Org_760000_children;
                              //new_screen_org = &new_screen_org[new_level-1];												
                              for (i = 0; i < 6; i++)
                              {
                                  GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->Screen_org_no[i];
                              }
															GUI_data_nav.New_screen_num[1] = '6';															
															GUI_data_nav.New_screen_num[2] = new_level+0x30;
                              GUI_data_nav.Change = 1;
                              GUI_data_nav.Child_present = 0;																
                            break;

                        case GUI_KEY_RIGHT:
                            ;
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
															current_level = GUI_data_nav.Current_screen_info->Screen_org_no[2];
															if(current_level >= '0' && current_level <'8')
															{
																new_level = (current_level + 1)- 0x30;
															}
															else if(current_level == '8')
															{
																new_level = 0;
															}
															else
															{
																new_level = current_level - 0x30;
															}
                              //new_screen_org = GUI_data_nav.Current_screen_info->child;
															//new_screen_org = Org_760000_children;
                              //new_screen_org = &new_screen_org[new_level-1];												
                              for (i = 0; i < 6; i++)
                              {
                                  GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->Screen_org_no[i];
                              }
															GUI_data_nav.New_screen_num[1] = '6';															
															GUI_data_nav.New_screen_num[2] = new_level+0x30;
                              GUI_data_nav.Change = 1;
                              GUI_data_nav.Child_present = 0;																
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;

                        case GUI_KEY_ENTER:
                            switch (sel)
                            {
                                case 0:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_1);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_1;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_1;
                                    break;

                                case 1:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_2);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_2;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_2;
                                    break;

                                case 2:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_3);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_3;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_3;
                                    break;

                                case 3:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_4);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_4;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_4;
                                    break;

                                case 4:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_5);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_5;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_5;
                                    break;

                                case 5:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_6);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_6;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_6;
                                    break;

                                case 6:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_7);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_7;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_7;
                                    break;

                                case 7:
                                    sprintf(char_data, "%0.3f", Setup_device_var.Load_weight_8);
                                    Calculation_struct.Target_weight = Setup_device_var.Load_weight_8;
                                    Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_8;
                                    break;
                            }
                            if (sel != 8)
                            {
                              TEXT_SetText(GUI_data_nav.Text_widget4, char_data);
                            }
                            else
                            {
                              new_screen_org = GUI_data_nav.Current_screen_info->child;
                              new_screen_org = &new_screen_org[(data[1].screen_an_option - 1)];
                              for (i = 0; i < 6; i++)
                              {
                                  GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                              }
                              //GUI_data_nav.New_screen_num[6] = '\0';
                              GUI_data_nav.Change = 1;
                              GUI_data_nav.Child_present = 1;
                            }
                            break;

                        case GUI_KEY_BACK:
                            ;
                            break;
                    }
                 break;

                case ID_MULTI_TEXT_1:
                    break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void Create_loadout_screen(int create_update)
* @returns    returns        : None
* @param      arg1           : Flag to create or update the screen
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets.
* @note       Notes          :
*****************************************************************************/
void Create_loadout_screen (int create_update)
{
    int i;
    char disp[ARRAY_SIZE];
    LOADOUT_RUNMODE_SCREEN_DATA *data;
    U8 twt_strg_size, rate_strg_size, speed_strg_size;
    data = GUI_data_nav.Current_screen_info->screen_data;

    if (create_update == CREATE_SCREEN)
    {
        GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LOAD_CTRL_LISTBOX_SIZE_X, LISTBOX_SIZE_Y,
                                               WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
        for (i = 1; i < 10; i++)
        {
            memset(disp, 0, sizeof(disp));
            switch (i)
            {

                case 1:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_1);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_1);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_1);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									*/
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_1);
                    break;

                case 2:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_2);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_2);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_2);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									*/
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_2);									
                    break;

                case 3:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_3);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_3);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_3);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									*/
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_3);
                    break;

                case 4:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_4);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_4);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_4);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)*/									
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_4);
                    break;

                case 5:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_5);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_5);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_5);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									*/
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_5);
                    break;

                case 6:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_6);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_6);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_6);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									*/
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_6);
                    break;

                case 7:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_7);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_7);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_7);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									*/
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_7);
                    break;

                case 8:
										/*if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.0f", Setup_device_var.Load_weight_8);
										else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
											sprintf(disp, "%0.1f", Setup_device_var.Load_weight_8);
										else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
											sprintf(disp, "%0.2f", Setup_device_var.Load_weight_8);
										else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									*/
											sprintf(disp, "%0.3f", Setup_device_var.Load_weight_8);
                    break;
            }
            strcat(disp, " ");
            strcat(disp, Strings[Scale_setup_var.Weight_unit].Text);
            if(i == 9)
            {
              memset(disp, 0, sizeof(disp));
              strcpy(disp, Strings[data[1].Text_index1].Text);
            }
            LISTBOX_AddString(GUI_data_nav.Listbox, disp);
        }

        LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
        LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
        LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
        LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
        LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
        LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
        LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
        LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

        LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
        LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
        LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
        LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

        WM_SetCallback(WM_HBKWIN, callback_screen_loadout);
        LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

        GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(LOAD_CTRL_MULTIEDIT_START_X, MULTI_WIN_OFFSET,
                                                     LOAD_CTRL_MULTIEDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                                     WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
        MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
        MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
        MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
        MULTIEDIT_SetAutoScrollV(GUI_data_nav.Multi_text,0);
        MULTIEDIT_SetAutoScrollH(GUI_data_nav.Multi_text,0);
        MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
        MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
        MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);
				twt_strg_size = GUI_GetStringDistX(Strings[data[0].Text_index3].Text);
				rate_strg_size = GUI_GetStringDistX(Strings[data[0].Text_index4].Text);
        speed_strg_size = GUI_GetStringDistX(Strings[data[0].Text_index5].Text);
        //--------------------------------weight accum string---------------------------------
        GUI_data_nav.Text_widget = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT_POS_X, LOADOUT_SCREEN_TEXT_EDIT_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
        TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS24_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget, Strings[data[0].Text_index2].Text);
				
				
				 //--------------------------------weight accum unit---------------------------------
        GUI_data_nav.Text_widget2 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT2_POS_X, LOADOUT_SCREEN_TEXT_EDIT2_POS_Y,
                                                LOADOUT_SCREEN_TEXT_EDIT2_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT2_SIZE_Y,
                              GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget2, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget2, &GUI_FontArial_Unicode_MS24_Bold);
        
				/*memset(disp, 0, sizeof(disp));
        strcpy(disp, "In ");
        strcat(disp, Scale_setup_var.Weight_unit);
				TEXT_SetText(GUI_data_nav.Text_widget2, disp);
				*/
				TEXT_SetText(GUI_data_nav.Text_widget2, Strings[Scale_setup_var.Weight_unit].Text);

        //--------------------------------weight accum value---------------------------------
        GUI_data_nav.Text_widget1 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT1_POS_X, LOADOUT_SCREEN_TEXT_EDIT1_POS_Y,
                                                LOADOUT_SCREEN_TEXT_EDIT1_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT1_SIZE_Y,
                              GUI_data_nav.Multi_text, TEXT_CF_HCENTER, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget1, GUI_YELLOW);
        //TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArial_Unicode_MS24_Bold);
        TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArialUnicodeMS72_Bold);
				memset(disp, 0, sizeof(disp));
				string_of_Weight(disp,data->variable1);
        //sprintf(disp, "%7.1f", *data->variable1);
        TEXT_SetText(GUI_data_nav.Text_widget1, disp);

/*        //--------------------------------weight accum unit---------------------------------
        GUI_data_nav.Text_widget2 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT2_POS_X, LOADOUT_SCREEN_TEXT_EDIT2_POS_Y,
                                                LOADOUT_SCREEN_TEXT_EDIT2_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT2_SIZE_Y,
                              GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget2, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget2, &GUI_FontArial_Unicode_MS24_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget2, Scale_setup_var.Weight_unit);
*/
        //--------------------------------current weight setpoint string---------------------------------
        GUI_data_nav.Text_widget3 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT3_POS_X, LOADOUT_SCREEN_TEXT_EDIT3_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT3_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT3_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget3, GUI_WHITE);
        TEXT_SetFont(GUI_data_nav.Text_widget3, &GUI_FontArial_Unicode_MS24_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget3, Strings[data[0].Text_index3].Text);

        //--------------------------------current weight setpoint value---------------------------------
        GUI_data_nav.Text_widget4 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT4_POS_X, LOADOUT_SCREEN_TEXT_EDIT4_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT4_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT4_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget4, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget4, &GUI_FontArial_Unicode_MS24_Bold);
        memset(disp, 0, sizeof(disp));
				//string_of_Weight(disp,(double *)data->variable2);
				if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
					sprintf(disp, "%0.0f",  *data->variable2);
				else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
					sprintf(disp, "%0.1f",  *data->variable2);
				else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
					sprintf(disp, "%0.2f",  *data->variable2);
				else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									
					sprintf(disp, "%0.3f",  *data->variable2);				
        //sprintf(disp, "%0.3f", *data->variable2);
        TEXT_SetText(GUI_data_nav.Text_widget4, disp);

        //--------------------------------current weight unit---------------------------------
        GUI_data_nav.Text_widget5 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT5_POS_X, LOADOUT_SCREEN_TEXT_EDIT5_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT5_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT5_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget5, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget5, &GUI_FontArial_Unicode_MS24_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget5, Strings[Scale_setup_var.Weight_unit].Text);

        //--------------------------------current rate string---------------------------------
        GUI_data_nav.Text_widget6 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT6_POS_X, LOADOUT_SCREEN_TEXT_EDIT6_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT6_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT6_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget6, GUI_WHITE);
        TEXT_SetFont(GUI_data_nav.Text_widget6, &GUI_FontArial_Unicode_MS24_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget6, Strings[data[0].Text_index4].Text);

        //--------------------------------current rate value---------------------------------
        GUI_data_nav.Text_widget7 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT7_POS_X, LOADOUT_SCREEN_TEXT_EDIT7_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT7_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT7_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget7, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget7, &GUI_FontArial_Unicode_MS24_Bold);
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%0.0f", *data->variable3);
        TEXT_SetText(GUI_data_nav.Text_widget7, disp);

        //--------------------------------current rate unit---------------------------------
        GUI_data_nav.Text_widget8 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT8_POS_X, LOADOUT_SCREEN_TEXT_EDIT8_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT8_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT8_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget8, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget8, &GUI_FontArial_Unicode_MS24_Bold);
        memset(disp, 0, sizeof(disp));
        strcpy(disp, Strings[Units_var.Unit_rate].Text);
//         strcat(disp, " / ");
//         strcat(disp, Strings[Scale_setup_var.Rate_time_unit].Text);
        TEXT_SetText(GUI_data_nav.Text_widget8, disp);

        //--------------------------------current speed string---------------------------------
        GUI_data_nav.Text_widget9 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT9_POS_X, LOADOUT_SCREEN_TEXT_EDIT9_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT9_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT9_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget9, GUI_WHITE);
        TEXT_SetFont(GUI_data_nav.Text_widget9, &GUI_FontArial_Unicode_MS24_Bold);
        TEXT_SetText(GUI_data_nav.Text_widget9, Strings[data[0].Text_index5].Text);

        //--------------------------------current speed value---------------------------------
        GUI_data_nav.Text_widget10 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT10_POS_X, LOADOUT_SCREEN_TEXT_EDIT10_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT10_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT10_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget10, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget10, &GUI_FontArial_Unicode_MS24_Bold);
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%0.0f", *data->variable4);
        TEXT_SetText(GUI_data_nav.Text_widget10, disp);

        //--------------------------------current speed unit---------------------------------
        GUI_data_nav.Text_widget11 = TEXT_CreateEx(LOADOUT_SCREEN_TEXT_EDIT11_POS_X, LOADOUT_SCREEN_TEXT_EDIT11_POS_Y,
                                                  LOADOUT_SCREEN_TEXT_EDIT11_SIZE_X, LOADOUT_SCREEN_TEXT_EDIT11_SIZE_Y,
                                                  GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
        TEXT_SetTextColor(GUI_data_nav.Text_widget11, GUI_YELLOW);
        TEXT_SetFont(GUI_data_nav.Text_widget11, &GUI_FontArial_Unicode_MS24_Bold);
        memset(disp, 0, sizeof(disp));
        strcpy(disp, Strings[Units_var.Unit_speed].Text);
        //strcat(disp, " / Min");
        TEXT_SetText(GUI_data_nav.Text_widget11, disp);

//        Calculation_struct.Target_weight = Setup_device_var.Load_weight_1;
//        Calculation_struct.Target_weight = IO_board_param.Load_from_strt_stop_stn;
//        Calculation_struct.Cutoff_weight = Setup_device_var.Cutoff_1;
        WM_SetFocus(GUI_data_nav.Listbox);
    }
    else //update screen
    {
        //--------------------------------weight accum value---------------------------------
        memset(disp, 0, sizeof(disp));
			  string_of_Weight(disp,data->variable1);
        //sprintf(disp, "%0.1f",*data->variable1);
        TEXT_SetText(GUI_data_nav.Text_widget1, disp);
        //--------------------------------current weight setpoint value-----------------------
        memset(disp, 0, sizeof(disp));
			  //string_of_Weight(disp,(double *)data->variable2);
				if(Scale_setup_var.Decimal_digits == NO_DECIMAL_DIGIT)									
					sprintf(disp, "%0.0f",  *data->variable2);
				else if(Scale_setup_var.Decimal_digits == ONE_DECIMAL_DIGIT)									
					sprintf(disp, "%0.1f",  *data->variable2);
				else if(Scale_setup_var.Decimal_digits == TWO_DECIMAL_DIGIT)									
					sprintf(disp, "%0.2f",  *data->variable2);
				else if(Scale_setup_var.Decimal_digits == THREE_DECIMAL_DIGIT)									
					sprintf(disp, "%0.3f",  *data->variable2);	
        TEXT_SetText(GUI_data_nav.Text_widget4, disp);
        //--------------------------------current rate value----------------------------------
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%0.0f",*data->variable3);
        TEXT_SetText(GUI_data_nav.Text_widget7, disp);
        //--------------------------------current speed value---------------------------------
        memset(disp, 0, sizeof(disp));
        sprintf(disp, "%0.0f", *data->variable4);
        TEXT_SetText(GUI_data_nav.Text_widget10, disp);
    }
    return;
}

/*****************************************************************************
* End of file
*****************************************************************************/
