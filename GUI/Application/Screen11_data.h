/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen11_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN11_DATA
#define  SCREEN11_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
__align(4) SCREEN_TYPE11_DATA Org_313000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
          {Screen316_str1, Screen316_str2, Screen316_str3, 0},
         };

__align(4) SCREEN_TYPE11_DATA Org_324000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
          {Screen316_str1, Screen316_str2, Screen340_str3, 0},
         };

__align(4) SCREEN_TYPE11_DATA Org_331000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
          {Screen332_str1, Screen332_str2, Screen332_str3, 0},
         };

__align(4) SCREEN_TYPE11_DATA Org_351100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
          {Screen3512_str1, Screen3512_str2, Screen3512_str3, 0},
         };

__align(4) SCREEN_TYPE11_DATA Org_352000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
          {Screen3512_str1, Screen3512_str2, Screen352_str1, 1},
         };

__align(4) SCREEN_TYPE11_DATA Org_619120_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
          {Screen61913_str1, Screen61913_str2, Screen61913_str3, 0},
         };

__align(4) SCREEN_TYPE11_DATA Org_619230_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
          {Screen61924_str1, Screen61924_str2, Screen61924_str3, 0},
         };


#endif
/*****************************************************************************
* End of file
*****************************************************************************/
