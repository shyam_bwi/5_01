/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Firmware_upgrade.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Wednesday, April 24, 2013, 10:00:00 AM
* @date Last Modified  : Wednesday, April 24, 2013, 10:00:00 AM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef  FIRMWARE_UPGRADE
#define  FIRMWARE_UPGRADE

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

	typedef struct
{
	float accumulated_zero_wt;
	float new_calc_zero_wt;
	char status[6];
	float old_auto_zero_offset;
	float zero_no_plus_old_azo;			//azo=auto zero offset
	float zero_wt_difference;
	float offset1;
	float new_auto_zero_offset;
	float Zero_wt_tolerance_low;
	float Zero_wt_tolerance_high;
	float zero_number;
	float no_of_pulses;
}AUTO_ZERO_PARAMETERS;

extern AUTO_ZERO_PARAMETERS		auto_zero_para1;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern char FwUpgrade(char *HexFilename, char length);
extern void Write_Debug_Logs_into_file(int MsgNo);

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
