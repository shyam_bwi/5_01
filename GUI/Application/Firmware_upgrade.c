/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename       : Firmware_upgrade.c
 * @brief               : Controller Board
 *
 * @author              : Anagha Basole
 *
 * @date Created        : Wednesday, April 24, 2013, 10:00:00 AM
 * @date Last Modified  : Wednesday, April 24, 2013, 10:00:00 AM
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal            :
 * @internal            :
 *
 *****************************************************************************/

/*============================================================================
 * Include Header Files
 *=============================================================================*/
#include "Global_ex.h"
#include "Main.h"
#include "Firmware_upgrade.h"
#include "EEPROM_high_level.h"
#include "lpc177x_8x_iap.h"
#include "File_update.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "Ext_flash_low_level.h"
#include "Ext_flash_high_level.h"
#include "Printer_high_level.h"
#include "RTOS_main.h"
#include "Log_report_data_calculate.h"
#include "Sensor_board_data_process.h"
#include "wdt.h"
/*============================================================================
 * Private Macro Definitions
 *===========================================================================*/
//#define MATRIX_ARB  (*(volatile U32*)(0x400FC188))
#ifdef REV3_BOARD
#define FLASH_CONFIG_AREA       0x00006000
#define FLASH_CONFIG_SECTOR      6
#else
#define FLASH_CONFIG_AREA       0x0000A000
#define FLASH_CONFIG_SECTOR      10
#endif

//#define EXT_FLASH_LAST_SECTOR_START_ADDR     0x807F0000		//moved to Ext_flash_high_level.h
//#define EXT_FLASH_LAST_SECTOR_END_ADDR       0x807FFFFF		//moved to Ext_flash_high_level.h
/*============================================================================
 * Private Data Types
 *===========================================================================*/

/*============================================================================
 *Public Variablese
 *===========================================================================*/
/* Constants section */
/*const char fixDATAstring[] __attribute__((at(FLASH_CONFIG_AREA))) =
 {
 0xAA, 0x55,0xFF, 0xFF
 };*/

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

__align(4) AUTO_ZERO_PARAMETERS 		auto_zero_para1;
/*============================================================================
 * Private Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
__align(4) unsigned char ConfigBuf[256];
extern U8 Task_section; // testing pupose
extern U8 Error_Task;
extern prop_file_ini prop_file;
unsigned char Struct_backup_fg;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
 * Private Function Prototypes Declarations
 *===========================================================================*/
char FwUpgrade(char *HexFilename, char length);
void Write_Debug_Logs_into_file(int MsgNo);
/*============================================================================
 * Function Implementation Section
 *===========================================================================*/

/*****************************************************************************
 * @note       Function name  : char FwUpgrade(char *HexFilename, char length)
 * @returns    returns        : status of the firmware upgrade
 * @param      arg1           : name of the file to be used for upgrade
 *             arg2           : length of the file name
 * @author                    : Shweta Pimple
 * @date       date created   : 6th March 2013
 * @brief      Description    : Upgrade the firmware of integrator board
 * @note       Notes          :
 *****************************************************************************/

extern void DelayMs(U32 ms);
char FwUpgrade(char *HexFilename, char length)
{
    unsigned char *pFlash;
    IAP_STATUS_CODE status;
    int i = 0;
U16 * read_flash_addr;
union double_union buffer;
U16 * u16Var;
    FILE *fp;
    int Msg_Log_No = 0;

    //__disable_irq();
    //Check for any file writing is not in progress

    do
    {
        ;
    }
    while (File_Write_In_Progress == 1);

    os_tsk_prio_self(MODBUS_SEN_TASK_PRIORITY+2);
    /* Disable UART Rx interrupt */
    UART_IntConfig(_PRINTER_UART, UART_INTCFG_RBR, DISABLE);
    /* Enable UART line status interrupt */
    //UART_IntConfig(_PRINTER_UART, UART_INTCFG_RLS, ENABLE);
    /*
     * Do not enable transmit interrupt here, since it is handled by
     * UART_Send() function, just to reset Tx Interrupt state for the
     * first time
     */
    UART_IntConfig(_PRINTER_UART, UART_INTCFG_THRE, DISABLE);
    //WDT_Un_Init();
    /* Enable Interrupt for UART0 channel */
    NVIC_DisableIRQ(_PRINTER_IRQ);

    // Backup of all GUI structures and Accumilated weight
    Struct_backup_fg = 1;
    eeprom_struct_backup();
    Struct_backup_fg = 0;
    //PVK
    //eeprom_weight_backup();

    /*Store all totals and report parameters in Ext Flash*/
    flash_para_backup();
    //flash_clear_weight_backup();	//Prasad
    flash_struct_backup(0);

	buffer.double_val = Calculation_struct.Total_weight_accum;
	 //Store the accumulated weight into flash at address of flag + 1
	 read_flash_addr = (U16 *)(EXT_FLASH_LAST_SECTOR_START_ADDR + 4);
   u16Var = (U16*)&buffer;
	 //LPC_GPIO2->SET |= TEST_GPIO_P2_31;
	 for (i=0; i<FLASH_SIZEOF(buffer); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //Backup of Job Total
	 buffer.double_val = Rprts_diag_var.Job_Total;
	 read_flash_addr = (U16 *)(EXT_FLASH_LAST_SECTOR_START_ADDR + 12);
   u16Var = (U16*)&buffer;
	 for (i=0; i<FLASH_SIZEOF(buffer); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //Backup of Master Total
	 buffer.double_val = Rprts_diag_var.Master_total;
	 read_flash_addr = (U16 *)(EXT_FLASH_LAST_SECTOR_START_ADDR + 20);
   u16Var = (U16*)&buffer;
	 for (i=0; i<FLASH_SIZEOF(buffer); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //Backup of Daily total
	 buffer.double_val = Rprts_diag_var.daily_Total_weight;
	 read_flash_addr = (U16 *)(EXT_FLASH_LAST_SECTOR_START_ADDR + 28);
   u16Var = (U16*)&buffer;
	 for (i=0; i<FLASH_SIZEOF(buffer); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //Backup of weekly total
	 buffer.double_val = Rprts_diag_var.weekly_Total_weight;
	 read_flash_addr = (U16 *)(EXT_FLASH_LAST_SECTOR_START_ADDR + 36);
   u16Var = (U16*)&buffer;
	 for (i=0; i<FLASH_SIZEOF(buffer); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //Backup of monthly total
	 buffer.double_val = Rprts_diag_var.monthly_Total_weight;
	 read_flash_addr = (U16 *)(EXT_FLASH_LAST_SECTOR_START_ADDR + 44);
   u16Var = (U16*)&buffer;
	 for (i=0; i<FLASH_SIZEOF(buffer); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //Backup of Yearly total
	 buffer.double_val = Rprts_diag_var.yearly_Total_weight;
	 read_flash_addr = (U16 *)(EXT_FLASH_LAST_SECTOR_START_ADDR + 52);
   u16Var = (U16*)&buffer;
	 for (i=0; i<FLASH_SIZEOF(buffer); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 	 //LPC_GPIO2->CLR |= 0x80000000;
	 /********************************************************************/
	 //store the local set point
	 u16Var = (U16*)&PID_Old_Local_Setpoint;
	 for (i=0; i<FLASH_SIZEOF(PID_Old_Local_Setpoint); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //store the current time
	 u16Var = (U16*)&Current_time;
	 for (i=0; i<FLASH_SIZEOF(Current_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 	if(!(Flags_struct.Error_flags & ZERO_BELT_SPEED_ERR) && ((daily_rprt.Dummy_var1&0x0002) == 0x0002))
		{
						if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
						{
								daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
						}
						else if(Admin_var.Current_Time_Format == TWELVE_HR)
						{
							if(Admin_var.AM_PM == AM_TIME_FORMAT)
							{
								if((U8) Current_time.RTC_Hour == 12)
									daily_rprt.End_time.Hr = 0;
								else
									daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour;
								daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
							}
							else
							{
								daily_rprt.End_time.Hr = (U8) Current_time.RTC_Hour+12;
								daily_rprt.End_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_time.Sec = (U8) Current_time.RTC_Sec;
							}
						}
			daily_rprt.Cnt_zero_speed++;
			weekly_rprt.Cnt_zero_speed++;
			monthly_rprt.Cnt_zero_speed++;
			daily_rprt.Dummy_var1 &= 0xFF00;
			daily_rprt.Dummy_var1 |= 0x0004;
		}
	 u16Var = (U16*)&daily_rprt.Cnt_zero_speed;
	 for (i=0; i<FLASH_SIZEOF(daily_rprt.Cnt_zero_speed); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }

	 //store the start time for zero speed
	 u16Var = (U16*)&daily_rprt.Start_time;
	 for (i=0; i<sizeof(daily_rprt.Start_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }

	 //store the end time for zero speed
	 u16Var = (U16*)&daily_rprt.End_time;
	 for (i=0; i<sizeof(daily_rprt.End_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }

   //store the downtime
	 u16Var = (U16*)&daily_rprt.Down_time;
   for (i=0; i<FLASH_SIZEOF(daily_rprt.Down_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }

	 //store the start load time
	 u16Var = (U16*)&daily_rprt.Start_load_time;
	 for (i=0; i<sizeof(daily_rprt.Start_load_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 if(Calculation_struct.Rate >= Admin_var.Zero_rate_limit)
	 {
					if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
					{
							daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour;
							daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
							daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
					}
					else if(Admin_var.Current_Time_Format == TWELVE_HR)
					{
						if(Admin_var.AM_PM == AM_TIME_FORMAT)
						{
							if((U8) Current_time.RTC_Hour == 12)
								daily_rprt.End_load_time.Hr = 0;
							else
								daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour;
							daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
							daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
						}
						else
						{
								daily_rprt.End_load_time.Hr = (U8) Current_time.RTC_Hour+12;
								daily_rprt.End_load_time.Min = (U8) Current_time.RTC_Min;
								daily_rprt.End_load_time.Sec = (U8) Current_time.RTC_Sec;
						}
					}
		 daily_rprt.Dummy_var1 &= 0x0000;
		 daily_rprt.Dummy_var1 |= (0x0304);
	 }
	 u16Var = (U16*)&daily_rprt.End_load_time;
	 for (i=0; i<sizeof(daily_rprt.End_load_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 u16Var = (U16*)&daily_rprt.Run_time;
   for (i=0; i<FLASH_SIZEOF(daily_rprt.Run_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //---------------weekly report------------------------
	 //weekly report zero speed counter
   u16Var = (U16*)&weekly_rprt.Cnt_zero_speed;
	 for (i=0; i<FLASH_SIZEOF(weekly_rprt.Cnt_zero_speed); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //weekly report down time
	 u16Var = (U16*)&weekly_rprt.Down_time;
	 for (i=0; i<FLASH_SIZEOF(weekly_rprt.Down_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }

	 //store weekly report run time
	 u16Var = (U16*)&weekly_rprt.Run_time;
   for (i=0; i<FLASH_SIZEOF(weekly_rprt.Run_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }

	 //----------------------monthly report---------------------------
	 //monthly report count zero speed
   u16Var = (U16*)&monthly_rprt.Cnt_zero_speed;
	 for (i=0; i<FLASH_SIZEOF(monthly_rprt.Cnt_zero_speed); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //monthly report down time
	 u16Var = (U16*)&monthly_rprt.Down_time;
	 for (i=0; i<FLASH_SIZEOF(monthly_rprt.Down_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }

	 //store monthly report run time
	 u16Var = (U16*)&monthly_rprt.Run_time;
	 for (i=0; i<FLASH_SIZEOF(monthly_rprt.Run_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
//
	 //extra write for writing the special id
	 u16Var = (U16*)&monthly_rprt.Run_time;
	 for (i=0; i<FLASH_SIZEOF(monthly_rprt.Run_time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
//
	 //------------------------Report Header Daily Date and Time-----------------
	 u16Var = (U16*)&daily_rprt.freq_rprt_header.Date;
	 for (i=0; i<FLASH_SIZEOF(daily_rprt.freq_rprt_header.Date); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
//
	 u16Var = (U16*)&weekly_rprt.freq_rprt_header.Date;
	 for (i=0; i<FLASH_SIZEOF(weekly_rprt.freq_rprt_header.Date); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	u16Var = (U16*)&monthly_rprt.freq_rprt_header.Date;
	 for (i=0; i<FLASH_SIZEOF(monthly_rprt.freq_rprt_header.Date); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
//
	 u16Var = (U16*)&daily_rprt.freq_rprt_header.Time;
	 for (i=0; i<FLASH_SIZEOF(daily_rprt.freq_rprt_header.Time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //------------------------Report Header Weekly Date and Time-----------------

	 u16Var = (U16*)&weekly_rprt.freq_rprt_header.Time;
	 for (i=0; i<FLASH_SIZEOF(weekly_rprt.freq_rprt_header.Time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
	 //------------------------Report Header Monthly Date and Time-----------------

	 u16Var = (U16*)&monthly_rprt.freq_rprt_header.Time;
	 for (i=0; i<FLASH_SIZEOF(monthly_rprt.freq_rprt_header.Time); i++)
	 {
		   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
	     read_flash_addr++;
	 }
//
	 daily_rprt.Dummy_var1 |= 0x8000;
	 u16Var = (U16*)&daily_rprt.Dummy_var1;
   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
   read_flash_addr++;

	 u16Var = (U16*)&periodic_data.Periodic_hdr.Id;
   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
   read_flash_addr++;
//
	 u16Var = (U16*)&set_zero_data_log.Set_zero_hdr.Id;
   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
   read_flash_addr++;
//
	 u16Var = (U16*)&clear_data_log.Clr_data_hdr.Id;
   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
   read_flash_addr++;
//
	 u16Var = (U16*)&calib_data_log.Cal_data_hdr.Id;
   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
   read_flash_addr++;
//
	 u16Var = (U16*)&length_cal_data_log.Len_data_hdr.Id;
   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
   read_flash_addr++;
//
	 u16Var = (U16*)&error_data_log.Err_data_hdr.Id;
   ExtFlash_writeWord((uint32_t)read_flash_addr, *u16Var++);
   read_flash_addr++;
//
	 read_flash_addr = (U16 *)EXT_FLASH_LAST_SECTOR_START_ADDR;
	 ExtFlash_writeWord((uint32_t)read_flash_addr, BACKUP_PRESENT_FLAG);

    /*Create and initialize the header of the periodic log file*/
    Msg_Log_No = 1;

    Write_Debug_Logs_into_file(Msg_Log_No++);

    if (Flags_struct.Connection_flags & USB_CON_FLAG)
    {
        fp = fopen("Tot_bkup.txt", "w");
        if (fp == NULL)
        {
            ;
        }
        else
        {
            fprintf(fp, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%f\r\n",
                    Calculation_struct.Total_weight_accum,
                    Rprts_diag_var.Job_Total, Rprts_diag_var.Master_total,
                    Rprts_diag_var.daily_Total_weight,
                    Rprts_diag_var.monthly_Total_weight,
                    Rprts_diag_var.yearly_Total_weight,
                    Rprts_diag_var.weekly_Total_weight,
                    Calculation_struct.run_time);
            fclose(fp);
        }

        Write_Debug_Logs_into_file(Msg_Log_No++);

        fp = fopen("ClrWeight_bkup.txt", "w");

        if (fp == NULL)
        {
            ;
        }
        else
        {
            for (i = 0; i < 8; i++)
                fprintf(fp, "%lf,%f,%f,%d,%d,%d,%d,%d,%d,%d,%d\r\n",
                        Rprts_diag_var.LastClearedTotal[i],
                        Rprts_diag_var.Avg_rate_for_mode[i],
                        Rprts_diag_var.RunTime[i],
                        Rprts_diag_var.Clear_Date[i].Day,
                        Rprts_diag_var.Clear_Date[i].Mon,
                        Rprts_diag_var.Clear_Date[i].Year,
                        Rprts_diag_var.Clear_Time[i].Hr,
                        Rprts_diag_var.Clear_Time[i].Min,
                        Rprts_diag_var.Clear_Time[i].Sec,
                        Rprts_diag_var.LastCleared_Weight_unit[i],
                        Rprts_diag_var.LastCleared_Rate_time_unit[i]);
            fclose(fp);
        }
        /*
         fp = fopen ("Miscellaneous.txt", "w");
         if(fp == NULL)
         {
         ;
         }
         else
         {
         fprintf(fp,"%d,%d\r\n",periodic_data.Periodic_hdr.Id,error_data_log.Err_data_hdr.Id);
         fclose(fp);
         }
         */
    }
    i = 1000;
    while (i--)
        ;

    MATRIX_ARB |= 0x00010000;

    //some delay
    i = 10;
    while (i--)
        ;

    memset(ConfigBuf, 0xFF, sizeof(ConfigBuf));
    memcpy(&ConfigBuf[4], HexFilename, length);
    //Store the .hex file name into config area of flash
    pFlash = (unsigned char*) (FLASH_CONFIG_AREA);

    Write_Debug_Logs_into_file(Msg_Log_No++);
    status = EraseSector(FLASH_CONFIG_SECTOR, FLASH_CONFIG_SECTOR);
    Write_Debug_Logs_into_file(Msg_Log_No++);

    if (status != CMD_SUCCESS)
    {
        os_tsk_prio_self(2);
        return _FALSE;
    }

    Write_Debug_Logs_into_file(Msg_Log_No++);
    status = CopyRAM2Flash(pFlash, ConfigBuf, IAP_WRITE_256);
    Write_Debug_Logs_into_file(Msg_Log_No++);

    if (status != CMD_SUCCESS)
    {
        os_tsk_prio_self(2);
        return _FALSE;
    }

    // __enable_irq();

    //Added for firmware upgrade issue i.e. cometimes EMC gets hang as it is not initialized after sofware reset
    Write_Debug_Logs_into_file(Msg_Log_No++);
    funinit("U0:"); 								//uninit file system
		 LPC_SC->PCONP &= ~(0x80000800); /* Disable USB and EMC Interface */
		 
		 DelayMs(1000);
    __disable_irq();
    // funinit("U0:"); 								//uninit file system
    __DSB();                                                     /* Ensure all outstanding memory accesses included
                                                                  buffered write are completed before reset */
    LPC_IOCON->P0_23 &=(~0x3<<3) ;
    LPC_IOCON->P0_23 |=0x01<<3 ;
    LPC_IOCON->P3_30 &=(~0x3<<3) ;
    LPC_IOCON->P3_30 |=0x01<<3 ;

    LPC_GPIO3->DIR |=0x01<<30;
    LPC_GPIO3->SET |=0x01<<30;
    DelayMs(1000);
    LPC_GPIO3->CLR |=0x01<<30;
    DelayMs(1000);
    LPC_GPIO3->SET |=0x01<<30;
    __DSB();                                                     /* Ensure all outstanding memory accesses included
buffered write are completed before reset */
    //reset the system
    LPC_EMC->Control = 0x0000; // EMC disable, Normal memory map
   

    NVIC_SystemReset();

    while (1)
    {
        ;
    }

    //os_tsk_prio_self(3);
    //return _TRUE;
}

/*****************************************************************************
 * @note       Function name : void Write_Debug_Logs_into_file(void)
 * @returns    returns       : None
 *                           :
 * @param      arg1          : None
 * @author                   : Oaces Team
 * @date       date created  : 15/03/2016
 * @brief      Description   : log FW msg to file
 * @note       Notes         : None
 *****************************************************************************/
void Write_Debug_Logs_into_file(int MsgNo)
{
    FILE *fptr;
    char file_name[FILE_NAME_SIZE];
    int size = 0;
    U8 u8File_exists = 0;

    float Temp_g_current_fw_ver;

    if (con == 1)
    {
        //error_data_log.Err_data_hdr.Id++;
        error_data_log.Err_data_hdr.Date.Year = (U16) Current_time.RTC_Year;
        error_data_log.Err_data_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
        error_data_log.Err_data_hdr.Date.Day = (U8) Current_time.RTC_Mday;
        error_data_log.Err_data_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
        error_data_log.Err_data_hdr.Time.Min = (U8) Current_time.RTC_Min;
        error_data_log.Err_data_hdr.Time.Sec = (U8) Current_time.RTC_Sec;
        error_data_log.Report_type = error_log;
        Flags_struct.Log_flags |= ERROR_LOG_WR_FLAG;

        Temp_g_current_fw_ver = g_current_fw_ver;
        //Check for version and open the file as per the version number older version uses
        // file name error_log.txt else System_Log.txt
// 		if (((Temp_g_current_fw_ver >= (float) 1.00)
// 				&& (Temp_g_current_fw_ver <= (float) 4.22))
// 				|| ((Temp_g_current_fw_ver >= (float) 0.90)
// 						&& (Temp_g_current_fw_ver <= (float) 0.99))
// 				|| ((Temp_g_current_fw_ver >= (float) 0.50)
// 						&& (Temp_g_current_fw_ver <= (float) 0.60))) {
// 			strcpy(file_name, "error_log.txt");
// 			fptr = fopen(file_name, "r");
// 		} else {
        {
            strcpy(file_name, "System_Log.txt");
            fptr = fopen(file_name, "r");
            //sks->
            if (fptr != NULL)  						//added 08-11-2016
            {
                //resolved issue: unit resets if blank USB drive is inserted.
                fseek(fptr, -1L, SEEK_END); // Read the last character from file
                size = ftell(fptr); // take a position of file pointer and get size of variable
                if (prop_file.Max_size_int == 0)
                    prop_file.Max_size_int = 6000000;
                if (size > prop_file.Max_size_int)     				//62939136
                {
                    fclose(fptr);
                    fptr = fopen("System_Log.bak", "r");
                    if (fptr != NULL)
                    {
                        fclose(fptr);
                        fdelete("System_Log.bak");
                    }
                    else
                    {
                        if (frename("System_Log.txt", "System_Log.bak") == 0)
                        {
                            strcpy(file_name, "System_Log.txt");
                            fptr = fopen(file_name, "a");
                            fclose(fptr);
                            fptr = fopen(file_name, "r");
                        }
                    }
                }
            }
        }

        //Check for file present
        if (fptr != NULL)
        {
            u8File_exists = 1;
            fclose(fptr);
        }
        else
        {
            u8File_exists = 0;
            //fclose(fp);
        }
        fflush(fptr);

        //Open file in read or write mode
        if (u8File_exists == 0)
        {
            fptr = fopen(file_name, "w");
        }
        else
        {
            fptr = fopen(file_name, "r");
        }

        if (fptr == NULL)
        {
            return;
        }
        else
        {
            if (u8File_exists == 0)
            {
                //Added by DK on 2 Feb 2016
                error_data_log.Err_data_hdr.Id = 1;
                fprintf(fptr,
                        "Record #\t Date\t Time\t  \tType\t \tMessage\t\r\n");
                //fprintf(fp, " %c\t", '0');
                fprintf(fptr, " %d\t", error_data_log.Err_data_hdr.Id);
                fprintf(fptr, " %04d/%02d/%02d\t",
                        error_data_log.Err_data_hdr.Date.Year,
                        error_data_log.Err_data_hdr.Date.Mon,
                        error_data_log.Err_data_hdr.Date.Day);
                fprintf(fptr, " %02d:%02d:%02d\t",
                        error_data_log.Err_data_hdr.Time.Hr,
                        error_data_log.Err_data_hdr.Time.Min,
                        error_data_log.Err_data_hdr.Time.Sec);
                fprintf(fptr, " %s\t", "Scale Info");
                fprintf(fptr,
                        "Scale name =%c%s%c, Plant name =%c%s%c, Product name =%c%s%c, Firmware Version %s\t\r\n",
                        '"', Admin_var.Scale_Name, '"', '"',
                        Admin_var.Plant_Name, '"', '"', Admin_var.Product_Name,
                        '"', Admin_var.Int_firmware_version);
            }
            //Else part is Added by PVK 05 Apr 2016
            else
            {

                //Close the file and open it in append mode
                if (0 != fclose(fptr))
                {
                    //		printf ("\nFile could not be closed!\n");
                }
                fflush(fptr);

                //Open file in append mode to write events
                fptr = fopen(file_name, "a");
                if (fptr == NULL)
                {
                    return;
                }
            }   										//else end

            error_data_log.Err_data_hdr.Id++;
            fprintf(fptr, " %d\t", error_data_log.Err_data_hdr.Id);
            fprintf(fptr, " %04d/%02d/%02d\t",
                    error_data_log.Err_data_hdr.Date.Year,
                    error_data_log.Err_data_hdr.Date.Mon,
                    error_data_log.Err_data_hdr.Date.Day);
            fprintf(fptr, " %02d:%02d:%02d\t",
                    error_data_log.Err_data_hdr.Time.Hr,
                    error_data_log.Err_data_hdr.Time.Min,
                    error_data_log.Err_data_hdr.Time.Sec);
            fprintf(fptr, " %s\t", "Event");

            //	memset(file_name,0,sizeof(file_name));

            switch (MsgNo)
            {
            case 1:
                fprintf(fptr, " %s\r\n",
                        "FW Ugrade Function - Before Opening Tot_bkup.txt");
                break;
            case 2:
                fprintf(fptr, " %s\r\n",
                        "FW Ugrade Function - Before Opening ClrWeight_bkup.tx");
                break;
            case 3:
                fprintf(fptr, " %s\r\n",
                        "FW Ugrade Function - Before Erasing FLASH_CONFIG_SECTOR");
                break;
            case 4:
                fprintf(fptr, " %s\r\n",
                        "FW Ugrade Function - After Erasing FLASH_CONFIG_SECTOR");
                break;
            case 5:
                fprintf(fptr, " %s\r\n",
                        "FW Ugrade Function - Before Writing FLASH_CONFIG_SECTOR");
                break;
            case 6:
                fprintf(fptr, " %s\r\n",
                        "FW Ugrade Function - After Writing FLASH_CONFIG_SECTOR");
                break;
            case 7:
                fprintf(fptr, " %s\r\n",
                        "FW Ugrade Function - Before Disabling USB and EMC Interface");
                break;
            case TCP_MODBUS_SYSLOG_EVENT1:
//				fprintf(fptr, " %s\r\n", "Modbus TCP Instance1 - Connected");
                break;
            case TCP_MODBUS_SYSLOG_EVENT2:
//				fprintf(fptr, " %s\r\n", "Modbus TCP Instance1 - Disconnected");
                break;
            case TCP_MODBUS_SYSLOG_EVENT3:
                //			fprintf(fptr, " %s\r\n", "Modbus TCP Instance2 - Connected");
                break;
            case TCP_MODBUS_SYSLOG_EVENT4:
                //			fprintf(fptr, " %s\r\n", "Modbus TCP Instance2 - Disconnected");
                break;
            case USB_CONNECTED_SYSLOG_EVENT:
                //->sks : BS205 :: prepare a syslog write when USB has been initialised
                fprintf(fptr, " %s\r\n",
                        "USB  Mass storage Device - Connected");
                //<- 26-05-2016
                break;
// 				case POWER_OFF_SYSLOG_EVENT:
// 					//->sks
// 								fprintf(fptr, " %s\r\n","P OFF");
// 				//<- 13-06-2016
// 							break;
//BY MEGHA ON 3/3/2017 FOR AUTO ZERO LOG EVENTS
            //SKS uncomment below block to log auto zero para
            //called from calibration.c line# 1073
            /*			case AUTO_ZERO_LOG_EVENT:

            					fprintf(fptr, " %lf\t", auto_zero_para1.zero_number);
            					fprintf(fptr, " %lf\t", auto_zero_para1.Zero_wt_tolerance_low);
            					fprintf(fptr, " %lf\t", auto_zero_para1.Zero_wt_tolerance_high);
            //					fprintf(fptr, " %lf\t", auto_zero_para1.accumulated_zero_wt);
            					fprintf(fptr, " %lf\t", auto_zero_para1.new_calc_zero_wt);
            					fprintf(fptr, " %s\t", auto_zero_para1.status);
            //					fprintf(fptr, " %lf\t", auto_zero_para1.old_auto_zero_offset);
            					fprintf(fptr, " %lf\t\r\n", auto_zero_para1.zero_no_plus_old_azo);
            //					fprintf(fptr, " %lf\t", auto_zero_para1.zero_wt_difference);
            //					fprintf(fptr, " %lf\t\r\n", auto_zero_para1.offset1);

            //					fprintf(fptr, " %lf\r\n", auto_zero_para1.new_auto_zero_offset);
            // 					fprintf(fptr, " %d\t", Sens_brd_param.No_of_pulses);
            // 					fprintf(fptr, " %lf\t\r\n", auto_zero_para1.no_of_pulses);



            				break;
            		*/


						case MB_DATE_CHANGE_EVENT:
           fprintf(fptr, " DATE CHANGED VIA MODBUS\r\n");
					 break;
					 case MB_TIME_CHANGE_EVENT:
           fprintf(fptr, " TIME CHANGED VIA MODBUS\r\n");
					 break;
					 case FTP_FILE_OPEN_EVENT:
           fprintf(fptr, "OPENING  FILE THROUGH FTP\r\n");
					 break;
					 case FTP_FILE_CLOSE_EVENT:
           fprintf(fptr, " CLOSING FILE THROUGH FTP\r\n");
					 break;
					 case FTP_BROWSE_START_EVENT:
           fprintf(fptr, " BROWSING FILE THROUGH FTP\r\n");
					 break;
					 default:
                break;
            }

            if (ferror(fptr))
            {
                //printf("\n Error log file write error");
            }

            if (0 != fclose(fptr))
            {
                //		printf ("\nFile could not be closed!\n");
            }

            fflush(fptr);
            memset(file_name, 0, sizeof(file_name));
        }
    }
}
/*****************************************************************************
 * End of file
 *****************************************************************************/
