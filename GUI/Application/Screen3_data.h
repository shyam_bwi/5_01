/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen3_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN3_DATA
#define  SCREEN3_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */



__align(4) SCREEN_TYPE3_DATA Org_100000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0,             NULL, no_variable, 0},
    {Screen1_str1,  Screen1_str2,  Screen1_str3,  NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_110000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0,             NULL,  no_variable, 0},
    {Screen1_str4,  Screen1_str5,  Screen1_str6,  NULL,  no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_120000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0,             NULL,  no_variable, 0},
    {Screen1_str7,  Screen1_str8,  Screen1_str9,  NULL,  no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_130000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0,             NULL, no_variable, 0},
    {Screen1_str10, Screen1_str11, Screen1_str12, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_131000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             Screen11_str1,  NULL, no_variable, 0},
    {Screen11_str2, Screen11_str3, Screen11_str4,  NULL, no_variable, 0},
    {Screen11_str5, Screen11_str6, Screen11_str7,  NULL, no_variable, 0},
    {Screen11_str8, Screen11_str9, Screen11_str10, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_140000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0,             NULL, no_variable, 0},
    {Screen1_str13, Screen1_str14, Screen1_str15, NULL, no_variable, 0},
};



__align(4) SCREEN_TYPE3_DATA Org_210000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen21_str1,   NULL,                      no_variable,        0},
    {Screen21_str2,  Screen21_str3,  Screen21_str4,    &Scale_setup_var.Run_mode, unsigned_char_type, 0},
    {Screen21_str8,  Screen21_str9,  Screen21_str10,   &Scale_setup_var.Run_mode, unsigned_char_type, 0},
    #ifdef PID_CONTROL
    {Screen21_str11, Screen21_str12, Screen21_str13,   &Scale_setup_var.Run_mode, unsigned_char_type, 0},
    #ifdef LOAD_CONTROL
    {Screen21_str14, Screen21_str15, Screen21_str16,   &Scale_setup_var.Run_mode, unsigned_char_type, 0},
    #endif
    {Screen21_str17, Screen21_str18, Screen21_str19,   &Scale_setup_var.Run_mode, unsigned_char_type, 0},
    #endif
		{Screen21_str20, Screen21_str21, Screen21_str22,   &Scale_setup_var.Run_mode, unsigned_char_type, 0}
};

__align(4) SCREEN_TYPE3_DATA Org_230000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen23_str1,   NULL,
           no_variable,        0},
    {Screen23_str2,  Screen23_str3,  Screen23_str4,    &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str5,  Screen23_str6,  Screen23_str7,    &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str8,  Screen23_str9,  Screen23_str10,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str11, Screen23_str12, Screen23_str13,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str14, Screen23_str15, Screen23_str16,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str17, Screen23_str18, Screen23_str19,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str20, Screen23_str21, Screen23_str22,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str23, Screen23_str24, Screen23_str25,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str26, Screen23_str27, Screen23_str28,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 0},
    {Screen23_str29, Screen23_str30, Screen23_str31,   &Scale_setup_var.Load_cell_size,
           unsigned_char_type, 1},
};

__align(4) SCREEN_TYPE3_DATA Org_231000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,                    NULL, no_variable, 0},
    {Screen2311_str1, Screen2311_str2, Screen_wizard_string, NULL, no_variable, 1},
    {Screen2312_str1, Screen2312_str2, Screen_wizard_string, NULL, no_variable, 2},
    {Screen2313_str1, Screen2313_str2, Screen_wizard_string, NULL, no_variable, 3},
};

__align(4) SCREEN_TYPE3_DATA Org_231210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen23121_str1, NULL,
           no_variable,        0},
    {Screen23121_str2, Screen23121_str3, Screen23121_str4,  &Scale_setup_var.Custom_LC_unit,
           unsigned_char_type, 0},
    {Screen23121_str5, Screen23121_str6, Screen23121_str7,  &Scale_setup_var.Custom_LC_unit,
           unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_240000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             Screen24_str1, NULL,
           no_variable,        0},
    {Screen24_str2, Screen24_str3, Screen24_str4,  &Scale_setup_var.Distance_unit,
           unsigned_char_type, 0},
    {Screen24_str5, Screen24_str6, Screen24_str7,  &Scale_setup_var.Distance_unit,
           unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_250000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen25_str1,  NULL,
           no_variable,        0},
    {Screen25_str2,  Screen25_str3,  Screen25_str4,   &Scale_setup_var.Weight_unit,
           unsigned_char_type, 0},
    {Screen25_str5,  Screen25_str6,  Screen25_str7,   &Scale_setup_var.Weight_unit,
           unsigned_char_type, 0},
    {Screen25_str8,  0,              0,               &Scale_setup_var.Weight_unit,
           unsigned_char_type, 0},
    {Screen25_str9,  Screen25_str10, Screen25_str11,  &Scale_setup_var.Weight_unit,
           unsigned_char_type, 0},
    {Screen25_str12, 0,              0,               &Scale_setup_var.Weight_unit,
           unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_260000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0, Screen26_str1, NULL,                            no_variable,        0},
    {Screen26_str2, 0, Screen26_str3,  &Scale_setup_var.Rate_time_unit, unsigned_char_type, 0},
    {Screen26_str4, 0, Screen26_str5,  &Scale_setup_var.Rate_time_unit, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_271000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,              NULL,
           no_variable,        0},
    {Screen271_M_str1,  Screen271_M_str2,  Screen271_M_str3,  &Scale_setup_var.AngleSensor_Install_status,
           unsigned_char_type, 0},
    {Screen271_M_str4,  Screen271_M_str5,  Screen271_M_str6,  &Scale_setup_var.AngleSensor_Install_status,
           unsigned_char_type, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_290000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0,             NULL, no_variable, 0},
    {Screen29_str1, Screen29_str2, Screen29_str3, NULL, no_variable, 1},
    {Screen29_str4, Screen29_str5, Screen29_str6, NULL, no_variable, 2},
    {Screen29_str7, Screen29_str8, Screen29_str9, NULL, no_variable, 3},
};

__align(4) SCREEN_TYPE3_DATA Org_293000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,              0,              NULL, no_variable, 0},
    {Screen29_str7, Screen293_str1, Screen293_str2, NULL, no_variable, 1},
};

__align(4) SCREEN_TYPE3_DATA Org_2A0000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                   0,                 0,                  NULL,
        no_variable,        0},
		{Screen2_str32,  Str_NULL, Screen2_str24,   &Scale_setup_var.Decimal_digits,
        unsigned_char_type, 0},
    {Screen2_str21,  Str_NULL, Screen2_str24,   &Scale_setup_var.Decimal_digits,
        unsigned_char_type, 0},
    {Screen2_str22,  Str_NULL, Screen2_str24,   &Scale_setup_var.Decimal_digits,
        unsigned_char_type, 0},
    {Screen2_str23,  Str_NULL, Screen2_str24,   &Scale_setup_var.Decimal_digits,
        unsigned_char_type, 0},
};
__align(4) SCREEN_TYPE3_DATA Org_2B0000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                   0,                 0,                  NULL,
        no_variable,        0},
    {Screen2_str27,  Screen2_str26, Screen2_str30,   &Scale_setup_var.IO_board_install,
        unsigned_char_type, 0},
    {Screen2_str29,  Screen2_str28, Screen2_str31,   &Scale_setup_var.IO_board_install,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_312000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL, no_variable, 0},
    {Screen315_str1, Screen315_str2, Screen315_str3, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_315000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL, no_variable, 0},
    {Screen318_str1, Screen318_str2, Screen318_str3, NULL, no_variable, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_326000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL, no_variable, 0},
    {Screen326_str1, Screen326_str2, Screen326_str3, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_321100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,               0,               NULL,
         no_variable,        0},
    {Screen25_str2, Screen3221_str1, Screen3221_str2,  &Calibration_var.Belt_scale_unit,
         unsigned_char_type, 0},
    {Screen25_str5, Screen3221_str1, Screen3221_str3,  &Calibration_var.Belt_scale_unit,
         unsigned_char_type, 0},
    {Screen25_str8, Screen3221_str1, Screen3221_str4,  &Calibration_var.Belt_scale_unit,
         unsigned_char_type, 0},
    {Screen25_str9, Screen3221_str1, Screen3221_str5,  &Calibration_var.Belt_scale_unit,
         unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_323100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,               0,               NULL,
          no_variable,        0},
    {Screen25_str2,  Screen3241_str1, Screen3241_str2,  &Calibration_var.Cert_scale_unit,
          unsigned_char_type, 0},
    {Screen25_str5,  Screen3241_str1, Screen3241_str3,  &Calibration_var.Cert_scale_unit,
          unsigned_char_type, 0},
    {Screen25_str8,  Screen3241_str1, Screen3241_str4,  &Calibration_var.Cert_scale_unit,
          unsigned_char_type, 0},
    {Screen25_str9,  Screen3241_str1, Screen3241_str5,  &Calibration_var.Cert_scale_unit,
          unsigned_char_type, 0},
    {Screen25_str12, Screen3241_str1, Screen3241_str6,  &Calibration_var.Cert_scale_unit,
          unsigned_char_type, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_330000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,               0,             NULL, no_variable, 0},
    {Screen331_str1, Screen331_str2, Screen331_str3, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_333000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL, no_variable, 0},
    {Screen334_str1, Screen334_str2, Screen334_str3, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_340000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0,             NULL, no_variable, 0},
    {Screen34_str1, Screen34_str2, Screen34_str3, NULL, no_variable, 1},
    {Screen34_str4, Screen34_str5, Screen34_str6, NULL, no_variable, 2},
    {Screen34_str7, Screen34_str8, Screen34_str9, NULL, no_variable, 3},
};

__align(4) SCREEN_TYPE3_DATA Org_350000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             Screen35_str1, NULL,  no_variable, 0},
    {Screen35_str2, Screen35_str3, Screen35_str4, NULL,  no_variable, 1},
    {Screen35_str5, Screen35_str6, Screen35_str7, NULL,  no_variable, 2},
};
__align(4) SCREEN_TYPE3_DATA Org_411000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,              NULL,
           no_variable,        0},
    {Screen411_str1,  Screen411_str2,  Screen411_str3,  &Setup_device_var.Printer_status,
           unsigned_char_type, 0},
    {Screen411_str4,  Screen411_str5,  Screen411_str6,  &Setup_device_var.Printer_status,
           unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_412000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,                    NULL,
          no_variable,        0},
    {Screen4121_str1, Screen4121_str2, Screen4121_str3,       &Setup_device_var.Printer_type,
          unsigned_char_type, 0},
    {Screen4122_str1, Screen4122_str2, Screen_wizard_string,  &Setup_device_var.Printer_type,
          unsigned_char_type, 2},
};
__align(4) SCREEN_TYPE3_DATA Org_414000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,              NULL,
           no_variable,        0},
    {Screen41_str7,  Screen41_str8,  Screen41_str9,  &Setup_device_var.Printer_ticket_num,
           unsigned_int_type, 0},
    {Current_value_string,  Str_NULL,  Screen41_str10,  &Setup_device_var.Printer_ticket_num,
           unsigned_int_type, 0},
};
#if 0
__align(4) SCREEN_TYPE3_DATA Org_412100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL, 0},
    {Screen4121_str1, Screen4121_str2, Screen4121_str3, NULL, 0},
};
#endif

__align(4) SCREEN_TYPE3_DATA Org_412210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                  Screen412211_str1, NULL,
          no_variable,       0},
    {Screen412211_str2, Screen412211_str3,  0,                  &Setup_device_var.Printer_baud,
          unsigned_int_type, 0},
    {Screen412211_str4, Screen412211_str5,  Screen412211_str6,  &Setup_device_var.Printer_baud,
          unsigned_int_type, 0},
    {Screen412211_str7, Screen412211_str8,  Screen412211_str6,  &Setup_device_var.Printer_baud,
          unsigned_int_type, 0},
    {Screen412211_str9, Screen412211_str10, Screen412211_str6,  &Setup_device_var.Printer_baud,
          unsigned_int_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_412220_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                  Screen412212_str1, NULL,
          no_variable,       0},
    {Screen412212_str2, Screen412212_str3,  0,                  &Setup_device_var.Printer_data_bits,
          unsigned_int_type, 0},
    {Screen412212_str4, Screen412212_str5,  0,                  &Setup_device_var.Printer_data_bits,
          unsigned_int_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_412230_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                  Screen412213_str1, NULL,
          no_variable,       0},
    {Screen412213_str2, Screen412213_str3,  0,                  &Setup_device_var.Printer_stop_bits,
          unsigned_int_type, 0},
    {Screen412213_str4, Screen412213_str5,  Screen412213_str6,  &Setup_device_var.Printer_stop_bits,
          unsigned_int_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_412240_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                  Screen412214_str1, NULL,
         no_variable,        0},
    {Screen412214_str2, Screen412214_str3,  0,                  &Setup_device_var.Printer_flow_control,
         unsigned_char_type, 0},
    {Screen412214_str4, Screen412214_str5,  Screen412214_str6,  &Setup_device_var.Printer_flow_control,
         unsigned_char_type, 0},
    {Screen412214_str7, Screen412214_str8,  0,                  &Setup_device_var.Printer_flow_control,
         unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_421000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen421_str1,  NULL,
         no_variable,        0},
    {Screen421_str2, Screen421_str3, Screen421_str4,   &Setup_device_var.Scoreboard_display_data,
         unsigned_char_type, 0},
    {Screen421_str5, Screen421_str6, Screen421_str7,   &Setup_device_var.Scoreboard_display_data,
         unsigned_char_type, 0},
    {Screen421_str8, Screen421_str9, Screen421_str10,  &Setup_device_var.Scoreboard_display_data,
         unsigned_char_type, 1},
};

__align(4) SCREEN_TYPE3_DATA Org_422000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,                    NULL,
          no_variable,        0},
    {Screen4221_str1, Screen4221_str2, Screen4221_str3,       &Setup_device_var.Scoreboard_type,
          unsigned_char_type, 0},
    {Screen4222_str1, Screen4222_str2, Screen_wizard_string,  &Setup_device_var.Scoreboard_type,
          unsigned_char_type, 2},
};

__align(4) SCREEN_TYPE3_DATA Org_423000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,                    NULL,
          no_variable,        0},
    {Screen431_str2, Screen4231_str1, Screen4231_str3,       &Setup_device_var.Scoreboard_TMMode,
          unsigned_char_type, 0},
    {Screen431_str5, Screen4231_str2, Screen4231_str4,  		 &Setup_device_var.Scoreboard_TMMode,
          unsigned_char_type, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_422211_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                  Screen422211_str1, NULL,
          no_variable,       0},
    {Screen422211_str2, Screen422211_str3,  0,                  &Setup_device_var.Scoreboard_baud,
          unsigned_int_type, 0},
    {Screen422211_str4, Screen422211_str5,  Screen422211_str6,  &Setup_device_var.Scoreboard_baud,
          unsigned_int_type, 0},
    {Screen422211_str7, Screen422211_str8,  Screen422211_str6,  &Setup_device_var.Scoreboard_baud,
          unsigned_int_type, 0},
    {Screen422211_str9, Screen422211_str10, Screen422211_str6,  &Setup_device_var.Scoreboard_baud,
          unsigned_int_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_422212_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                  Screen422212_str1, NULL,
          no_variable,       0},
    {Screen422212_str2, Screen422212_str3,  0,                  &Setup_device_var.Scoreboard_data_bits,
          unsigned_int_type, 0},
    {Screen422212_str4, Screen422212_str5,  0,                  &Setup_device_var.Scoreboard_data_bits,
          unsigned_int_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_422213_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                  Screen422213_str1, NULL,
          no_variable,       0},
    {Screen422213_str2, Screen422213_str3,  0,                  &Setup_device_var.Scoreboard_stop_bits,
          unsigned_int_type, 0},
    {Screen422213_str4, Screen422213_str5,  Screen422213_str6,  &Setup_device_var.Scoreboard_stop_bits,
          unsigned_int_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_431000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen431_str1,  NULL,
          no_variable,        0},
    {Screen431_str2, Screen431_str3, Screen431_str4,   &Setup_device_var.Log_cal_data,
          unsigned_char_type, 0},
    {Screen431_str5, Screen431_str6, Screen431_str7,   &Setup_device_var.Log_cal_data,
          unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_432000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen432_str1,  NULL,
          no_variable,        0},
    {Screen431_str2, Screen431_str3, Screen432_str2,   &Setup_device_var.Log_run_time_data,
          unsigned_char_type, 0},
    {Screen431_str5, Screen431_str6, Screen432_str3,   &Setup_device_var.Log_run_time_data,
          unsigned_char_type, 0},
    {Screen432_str4, Screen432_str5, Screen432_str6,  NULL,
          no_variable,        1},
};

__align(4) SCREEN_TYPE3_DATA Org_433000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen433_str1,  NULL,
           no_variable,        0},
    {Screen431_str2, Screen431_str3, Screen433_str2,   &Setup_device_var.Log_err_data,
           unsigned_char_type, 0},
    {Screen431_str5, Screen431_str6, Screen433_str3,   &Setup_device_var.Log_err_data,
           unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_434000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen434_str1,  NULL,
           no_variable,        0},
    {Screen431_str2, Screen431_str3, Screen434_str2,   &Setup_device_var.Log_zero_cal_data,
           unsigned_char_type, 0},
    {Screen431_str5, Screen431_str6, Screen434_str3,   &Setup_device_var.Log_zero_cal_data,
           unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_435000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen435_str1,  NULL,
           no_variable,        0},
    {Screen431_str2, Screen431_str3, Screen435_str2,   &Setup_device_var.Log_clear_wt_data,
           unsigned_char_type, 0},
    {Screen431_str5, Screen431_str6, Screen435_str3,   &Setup_device_var.Log_clear_wt_data,
           unsigned_char_type, 0},
};
__align(4) SCREEN_TYPE3_DATA Org_436000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen436_str0,  NULL,
           no_variable,        0},
    {Screen4361_str0, Screen4361_str0, Screen4361_str1,   NULL,
           no_variable, 0},
    {Screen4362_str0, Screen4362_str0, Screen4362_str1,   NULL,
           no_variable, 0},
					 
};


__align(4) SCREEN_TYPE3_DATA Org_441100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4411_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                0,                 &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
    {Screen4411_str2,  Screen4411_str3,  Screen4411_str4,   &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
    {Screen4411_str5,  Screen4411_str6,  Screen4411_str7,   &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
    {Screen4411_str8,  Screen4411_str9,  Screen4411_str10,  &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
    {Screen4411_str11, Screen4411_str12, Screen4411_str13,  &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
    {Screen4411_str14, Screen4411_str15, Screen4411_str16,  &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
    {Screen4411_str17, Screen4411_str18, Screen4411_str19,  &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
    {Screen4411_str20, Screen4411_str21, Screen4411_str22,  &Setup_device_var.Digital_Input_1_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_441200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4411_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,               &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
    {Screen4411_str2,  Screen4411_str3,  Screen4412_str1,  &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
    {Screen4411_str5,  Screen4411_str6,  Screen4412_str2,  &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
    {Screen4411_str8,  Screen4411_str9,  Screen4412_str3,  &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
    {Screen4411_str11, Screen4411_str12, Screen4412_str4,  &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
    {Screen4411_str14, Screen4411_str15, Screen4412_str5,  &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
    {Screen4411_str17, Screen4411_str18, Screen4412_str6,  &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
    {Screen4411_str20, Screen4411_str21, Screen4412_str7,  &Setup_device_var.Digital_Input_2_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_441300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4411_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,               &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
    {Screen4411_str2,  Screen4411_str3,  Screen4413_str1,  &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
    {Screen4411_str5,  Screen4411_str6,  Screen4413_str2,  &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
    {Screen4411_str8,  Screen4411_str9,  Screen4413_str3,  &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
    {Screen4411_str11, Screen4411_str12, Screen4413_str4,  &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
    {Screen4411_str14, Screen4411_str15, Screen4413_str5,  &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
    {Screen4411_str17, Screen4411_str18, Screen4413_str6,  &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
    {Screen4411_str20, Screen4411_str21, Screen4413_str7,  &Setup_device_var.Digital_Input_3_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_441400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4411_str1, NULL,
        no_variable,        0},
    {Port_string3,     0,                0,                &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
    {Screen4411_str2,  Screen4411_str3,  Screen4414_str1,  &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
    {Screen4411_str5,  Screen4411_str6,  Screen4414_str2,  &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
    {Screen4411_str8,  Screen4411_str9,  Screen4414_str3,  &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
    {Screen4411_str11, Screen4411_str12, Screen4414_str4,  &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
    {Screen4411_str14, Screen4411_str15, Screen4414_str5,  &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
    {Screen4411_str17, Screen4411_str18, Screen4414_str6,  &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
    {Screen4411_str20, Screen4411_str21, Screen4414_str7,  &Setup_device_var.Digital_Input_4_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4421_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,                &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 0},
    {Screen4421_str2,  Screen4421_str3,  Screen4421_str4,   &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 1},
    {Screen4421_str5,  Screen4421_str6,  Screen4421_str7,   &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 0},
    {Screen4421_str8,  Screen4421_str9,  Screen4421_str10,  &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 3},
    {Screen4421_str11, Screen4421_str12, Screen4421_str13,  &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 4},
    {Screen4421_str14, Screen4421_str15, Screen4421_str16,  &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 5},
    {Screen4421_str17, Screen4421_str18, Screen4421_str19,  &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 0},
    {Screen4421_str20, Screen4421_str21, Screen4421_str22,  &Setup_device_var.Digital_Output_1_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442111_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                   0,                 0,                  NULL,
        no_variable,        0},
    {Screen442111_str10,  Screen442111_str2, Screen442111_str11,   &Setup_device_var.Units_Per_Pulse_1,
        unsigned_char_type, 0},				
    {Screen442111_str1,  Screen442111_str2, Screen442111_str3,   &Setup_device_var.Units_Per_Pulse_1,
        unsigned_char_type, 0},
    {Screen442111_str4,  Screen442111_str2, Screen442111_str5,   &Setup_device_var.Units_Per_Pulse_1,
        unsigned_char_type, 0},
    {Screen442111_str6,  Screen442111_str2, Screen442111_str7,   &Setup_device_var.Units_Per_Pulse_1,
        unsigned_char_type, 0},
    {Screen442111_str8,  Screen442111_str2, Screen442111_str9,   &Setup_device_var.Units_Per_Pulse_1,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442130_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen44213_str1,  NULL,
        no_variable,        0},
    {Screen44213_str2,  Screen44213_str3,  Screen44213_str4,   &Setup_device_var.error_alarm_1,
        unsigned_char_type, 0},
    {Screen44213_str5,  Screen44213_str6,  Screen44213_str7,   &Setup_device_var.error_alarm_1,
        unsigned_char_type, 0},
    {Screen44213_str8,  Screen44213_str9,  Screen44213_str10,  &Setup_device_var.error_alarm_1,
        unsigned_char_type, 0},
    {Screen44213_str11, Screen44213_str12, Screen44213_str13,  &Setup_device_var.error_alarm_1,
        unsigned_char_type, 0},
    {Screen44213_str14, Screen44213_str15, Screen44213_str16,  &Setup_device_var.error_alarm_1,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442142_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442142_str1,  Screen442142_str2,  Screen442142_str3,   &Setup_device_var.Speed_Output_Type_1,
        unsigned_char_type, 0},
    {Screen442142_str4,  Screen442142_str5,  Screen442142_str6,   &Setup_device_var.Speed_Output_Type_1,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442152_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442152_str1,  Screen442152_str2,  Screen442152_str3,   &Setup_device_var.Rate_Output_Type_1,
        unsigned_char_type, 0},
    {Screen442152_str4,  Screen442152_str5,  Screen442152_str6,   &Setup_device_var.Rate_Output_Type_1,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4422_str1, NULL,
        no_variable,        0},
    {Port_string3,     0,                0,                &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 0},
    {Screen4421_str2,  Screen4421_str3,  Screen4422_str2,  &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 1},
    {Screen4421_str5,  Screen4421_str6,  Screen4422_str3,  &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 0},
    {Screen4421_str8,  Screen4421_str9,  Screen4422_str4,  &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 3},
    {Screen4421_str11, Screen4421_str12, Screen4422_str5,  &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 4},
    {Screen4421_str14, Screen4421_str15, Screen4422_str6,  &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 5},
    {Screen4421_str17, Screen4421_str18, Screen4422_str7,  &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 0},
    {Screen4421_str20, Screen4421_str21, Screen4422_str8,  &Setup_device_var.Digital_Output_2_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442211_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                   0,                0,                 NULL,
        no_variable,        0},
    {Screen442111_str10, Screen442111_str2, Screen442211_str11,  &Setup_device_var.Units_Per_Pulse_2,
        unsigned_char_type, 0},				
    {Screen442111_str1,  Screen442111_str2, Screen442211_str1,  &Setup_device_var.Units_Per_Pulse_2,
        unsigned_char_type, 0},
    {Screen442111_str4,  Screen442111_str2, Screen442211_str2,  &Setup_device_var.Units_Per_Pulse_2,
        unsigned_char_type, 0},
    {Screen442111_str6,  Screen442111_str2, Screen442211_str3,  &Setup_device_var.Units_Per_Pulse_2,
        unsigned_char_type, 0},
    {Screen442111_str8,  Screen442111_str2, Screen442211_str4,  &Setup_device_var.Units_Per_Pulse_2,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442230_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen44223_str1, NULL,
        no_variable,        0},
    {Screen44213_str2,  Screen44213_str3,  Screen44223_str2,  &Setup_device_var.error_alarm_2,
        unsigned_char_type, 0},
    {Screen44213_str5,  Screen44213_str6,  Screen44223_str3,  &Setup_device_var.error_alarm_2,
        unsigned_char_type, 0},
    {Screen44213_str8,  Screen44213_str9,  Screen44223_str4,  &Setup_device_var.error_alarm_2,
        unsigned_char_type, 0},
    {Screen44213_str11, Screen44213_str12, Screen44223_str5,  &Setup_device_var.error_alarm_2,
        unsigned_char_type, 0},
    {Screen44213_str14, Screen44213_str15, Screen44223_str6,  &Setup_device_var.error_alarm_2,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442242_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442142_str1,  Screen442142_str2,  Screen442242_str1,   &Setup_device_var.Speed_Output_Type_2,
        unsigned_char_type, 0},
    {Screen442142_str4,  Screen442142_str5,  Screen442242_str2,   &Setup_device_var.Speed_Output_Type_2,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442252_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442152_str1,  Screen442152_str2,  Screen442252_str1,   &Setup_device_var.Rate_Output_Type_2,
        unsigned_char_type, 0},
    {Screen442152_str4,  Screen442152_str5,  Screen442252_str2,   &Setup_device_var.Rate_Output_Type_2,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4423_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,              &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 0},
    {Screen4421_str2,  Screen4421_str3,  Screen4423_str2,  &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 1},
    {Screen4421_str5,  Screen4421_str6,  Screen4423_str3,  &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 0},
    {Screen4421_str8,  Screen4421_str9,  Screen4423_str4,  &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 3},
    {Screen4421_str11, Screen4421_str12, Screen4423_str5,  &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 4},
    {Screen4421_str14, Screen4421_str15, Screen4423_str6,  &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 5},
    {Screen4421_str17, Screen4421_str18, Screen4423_str7,  &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 0},
    {Screen4421_str20, Screen4421_str21, Screen4423_str8,  &Setup_device_var.Digital_Output_3_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442311_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                   0,                 0,                  NULL,
        no_variable,        0},
    {Screen442111_str10, Screen442111_str2, Screen442311_str11,  &Setup_device_var.Units_Per_Pulse_3,
        unsigned_char_type, 0},						
    {Screen442111_str1,  Screen442111_str2,  Screen442311_str1,   &Setup_device_var.Units_Per_Pulse_3,
        unsigned_char_type, 0},
    {Screen442111_str4,  Screen442111_str2,  Screen442311_str2,   &Setup_device_var.Units_Per_Pulse_3,
        unsigned_char_type, 0},
    {Screen442111_str6,  Screen442111_str2,  Screen442311_str3,   &Setup_device_var.Units_Per_Pulse_3,
        unsigned_char_type, 0},
    {Screen442111_str8,  Screen442111_str2,  Screen442311_str4,   &Setup_device_var.Units_Per_Pulse_3,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442330_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen44233_str1, NULL,
        no_variable,        0},
    {Screen44213_str2,  Screen44213_str3,  Screen44233_str2,  &Setup_device_var.error_alarm_3,
        unsigned_char_type, 0},
    {Screen44213_str5,  Screen44213_str6,  Screen44233_str3,  &Setup_device_var.error_alarm_3,
        unsigned_char_type, 0},
    {Screen44213_str8,  Screen44213_str9,  Screen44233_str4,  &Setup_device_var.error_alarm_3,
        unsigned_char_type, 0},
    {Screen44213_str11, Screen44213_str12, Screen44233_str5,  &Setup_device_var.error_alarm_3,
        unsigned_char_type, 0},
    {Screen44213_str14, Screen44213_str15, Screen44233_str6,  &Setup_device_var.error_alarm_3,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442342_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442142_str1,  Screen442142_str2,  Screen442342_str1,   &Setup_device_var.Speed_Output_Type_3,
        unsigned_char_type, 0},
    {Screen442142_str4,  Screen442142_str5,  Screen442342_str2,   &Setup_device_var.Speed_Output_Type_3,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_442352_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442152_str1,  Screen442152_str2,  Screen442352_str1,   &Setup_device_var.Rate_Output_Type_3,
        unsigned_char_type, 0},
    {Screen442152_str4,  Screen442152_str5,  Screen442352_str2,   &Setup_device_var.Rate_Output_Type_3,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4431_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,              &Setup_device_var.Relay_Output_1_Function,
        unsigned_char_type, 0},
    {Screen4421_str8,  Screen4421_str9,  Screen4431_str2,  &Setup_device_var.Relay_Output_1_Function,
        unsigned_char_type, 1},
    {Screen4421_str11, Screen4421_str12, Screen4431_str3,  &Setup_device_var.Relay_Output_1_Function,
        unsigned_char_type, 2},
    {Screen4421_str14, Screen4421_str15, Screen4431_str4,  &Setup_device_var.Relay_Output_1_Function,
        unsigned_char_type, 3},
    {Screen4421_str17, Screen4421_str18, Screen4431_str5,  &Setup_device_var.Relay_Output_1_Function,
        unsigned_char_type, 0},
    {Screen4421_str20, Screen4421_str21, Screen4431_str6,  &Setup_device_var.Relay_Output_1_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443110_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen44311_str1, NULL,
        no_variable,        0},
    {Screen44213_str2,  Screen44213_str3,  Screen44311_str2,  &Setup_device_var.error_alarm_4,
        unsigned_char_type, 0},
    {Screen44213_str5,  Screen44213_str6,  Screen44311_str3,  &Setup_device_var.error_alarm_4,
        unsigned_char_type, 0},
    {Screen44213_str8,  Screen44213_str9,  Screen44311_str4,  &Setup_device_var.error_alarm_4,
        unsigned_char_type, 0},
    {Screen44213_str11, Screen44213_str12, Screen44311_str5,  &Setup_device_var.error_alarm_4,
        unsigned_char_type, 0},
    {Screen44213_str14, Screen44213_str15, Screen44311_str6,  &Setup_device_var.error_alarm_4,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443122_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442142_str1,  Screen442142_str2,  Screen443122_str1,   &Setup_device_var.Speed_Output_Type_4,
        unsigned_char_type, 0},
    {Screen442142_str4,  Screen442142_str5,  Screen443122_str2,   &Setup_device_var.Speed_Output_Type_4,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443132_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442152_str1,  Screen442152_str2,  Screen443132_str1,   &Setup_device_var.Rate_Output_Type_4,
        unsigned_char_type, 0},
    {Screen442152_str4,  Screen442152_str5,  Screen443132_str2,   &Setup_device_var.Rate_Output_Type_4,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4432_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,              &Setup_device_var.Relay_Output_2_Function,
        unsigned_char_type, 0},
    {Screen4421_str8,  Screen4421_str9,  Screen4432_str2,  &Setup_device_var.Relay_Output_2_Function,
        unsigned_char_type, 1},
    {Screen4421_str11, Screen4421_str12, Screen4432_str3,  &Setup_device_var.Relay_Output_2_Function,
        unsigned_char_type, 2},
    {Screen4421_str14, Screen4421_str15, Screen4432_str4,  &Setup_device_var.Relay_Output_2_Function,
        unsigned_char_type, 3},
    {Screen4421_str17, Screen4421_str18, Screen4432_str5,  &Setup_device_var.Relay_Output_2_Function,
        unsigned_char_type, 0},
    {Screen4421_str20, Screen4421_str21, Screen4432_str6,  &Setup_device_var.Relay_Output_2_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen44321_str1,  NULL,
        no_variable,        0},
    {Screen44213_str2,  Screen44213_str3,  Screen44321_str2,   &Setup_device_var.error_alarm_5,
        unsigned_char_type, 0},
    {Screen44213_str5,  Screen44213_str6,  Screen44321_str3,   &Setup_device_var.error_alarm_5,
        unsigned_char_type, 0},
    {Screen44213_str8,  Screen44213_str9,  Screen44321_str4,  &Setup_device_var.error_alarm_5,
        unsigned_char_type, 0},
    {Screen44213_str11, Screen44213_str12, Screen44321_str5,  &Setup_device_var.error_alarm_5,
        unsigned_char_type, 0},
    {Screen44213_str14, Screen44213_str15, Screen44321_str6,  &Setup_device_var.error_alarm_5,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443222_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442142_str1,  Screen442142_str2,  Screen443222_str1,   &Setup_device_var.Speed_Output_Type_5,
        unsigned_char_type, 0},
    {Screen442142_str4,  Screen442142_str5,  Screen443222_str2,   &Setup_device_var.Speed_Output_Type_5,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443232_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442152_str1,  Screen442152_str2,  Screen443232_str1,   &Setup_device_var.Rate_Output_Type_5,
        unsigned_char_type, 0},
    {Screen442152_str4,  Screen442152_str5,  Screen443232_str2,   &Setup_device_var.Rate_Output_Type_5,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen4433_str1,  NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,               &Setup_device_var.Relay_Output_3_Function,
        unsigned_char_type, 0},
    {Screen4421_str8,  Screen4421_str9,  Screen4433_str2,   &Setup_device_var.Relay_Output_3_Function,
        unsigned_char_type, 1},
    {Screen4421_str11, Screen4421_str12, Screen4433_str3,   &Setup_device_var.Relay_Output_3_Function,
        unsigned_char_type, 2},
    {Screen4421_str14, Screen4421_str15, Screen4433_str4,  &Setup_device_var.Relay_Output_3_Function,
        unsigned_char_type, 3},
    {Screen4421_str17, Screen4421_str18, Screen4433_str5,  &Setup_device_var.Relay_Output_3_Function,
        unsigned_char_type, 0},
    {Screen4421_str20, Screen4421_str21, Screen4433_str6,  &Setup_device_var.Relay_Output_3_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443310_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen44331_str1,  NULL,
        no_variable,        0},
    {Screen44213_str2,  Screen44213_str3,  Screen44331_str2,   &Setup_device_var.error_alarm_6,
        unsigned_char_type, 0},
    {Screen44213_str5,  Screen44213_str6,  Screen44331_str3,   &Setup_device_var.error_alarm_6,
        unsigned_char_type, 0},
    {Screen44213_str8,  Screen44213_str9,  Screen44331_str4,  &Setup_device_var.error_alarm_6,
        unsigned_char_type, 0},
    {Screen44213_str11, Screen44213_str12, Screen44331_str5,  &Setup_device_var.error_alarm_6,
        unsigned_char_type, 0},
    {Screen44213_str14, Screen44213_str15, Screen44331_str6,  &Setup_device_var.error_alarm_6,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443322_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442142_str1,  Screen442142_str2,  Screen443322_str1,   &Setup_device_var.Speed_Output_Type_6,
        unsigned_char_type, 0},
    {Screen442142_str4,  Screen442142_str5,  Screen443322_str2,   &Setup_device_var.Speed_Output_Type_6,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_443332_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                  0,                  0,                  NULL,
        no_variable,        0},
    {Screen442152_str1,  Screen442152_str2,  Screen443332_str1,   &Setup_device_var.Rate_Output_Type_6,
        unsigned_char_type, 0},
    {Screen442152_str4,  Screen442152_str5,  Screen443332_str2,   &Setup_device_var.Rate_Output_Type_6,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_444000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL,  no_variable, 0},
    {Screen444_str1, Screen444_str2, Screen444_str3, NULL,  no_variable, 1},
    {Screen444_str4, Screen444_str5, Screen444_str6, NULL,  no_variable, 2},
};

__align(4) SCREEN_TYPE3_DATA Org_444110_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen44411_str1, NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,                &Setup_device_var.Analog_Output_1_Function,
        unsigned_char_type, 0},
    {Screen44411_str2, Screen44411_str3, Screen44411_str4,  &Setup_device_var.Analog_Output_1_Function,
        unsigned_char_type, 0},
    {Screen44411_str5, Screen44411_str6, Screen44411_str7,  &Setup_device_var.Analog_Output_1_Function,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_444210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen44421_str1, NULL,
        no_variable,        0},
    {Port_string3,     0,                  0,                &Setup_device_var.Analog_Output_2_Function,
        unsigned_char_type, 0},
    {Screen44421_str2, Screen44421_str3, Screen44421_str4,  &Setup_device_var.Analog_Output_2_Function,
        unsigned_char_type, 0},
    {Screen44421_str5, Screen44421_str6, Screen44421_str7,  &Setup_device_var.Analog_Output_2_Function,
        unsigned_char_type, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_451200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL,
        no_variable,        0},
    {Screen4512_str1, Screen4512_str2, Screen4512_str3,  &Setup_device_var.PID_Action,
        unsigned_char_type, 0},
    {Screen4512_str4, Screen4512_str5, Screen4512_str6,  &Setup_device_var.PID_Action,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_451300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL,
        no_variable,        0},
    {Screen4513_str1, Screen4513_str2, Screen4513_str3,  &Setup_device_var.PID_Setpoint_Type,
        unsigned_char_type, 1},
		{Screen4513_str7, Screen4513_str8, Screen4513_str9,  &Setup_device_var.PID_Setpoint_Type,
        unsigned_char_type, 2},
    {Screen4513_str4, Screen4513_str5, Screen4513_str6,  &Setup_device_var.PID_Setpoint_Type,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_511000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL, no_variable,      0},
    {Screen511_str1, Screen511_str2, Screen511_str3,    NULL, no_variable,      0},
    {Screen511_str4, Screen511_str5, Screen511_str6,    NULL, no_variable,      0},
};

__align(4) SCREEN_TYPE3_DATA Org_614200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                0,                NULL,
        no_variable,        0},
    {Screen6142_str3, Screen6142_str4, Screen6142_str5,  &Admin_var.Current_Date_Format,
        unsigned_char_type, 0},
    {Screen6142_str6, Screen6142_str7, Screen6142_str8,  &Admin_var.Current_Date_Format,
        unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_614300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
	  {0,               0, 0, NULL,                   no_variable,        0},
    {Screen6143_str3, 0, 0,  &Admin_var.Day_of_week, unsigned_char_type, 0},
    {Screen6143_str4, 0, 0,  &Admin_var.Day_of_week, unsigned_char_type, 0},
    {Screen6143_str5, 0, 0,  &Admin_var.Day_of_week, unsigned_char_type, 0},
    {Screen6143_str6, 0, 0,  &Admin_var.Day_of_week, unsigned_char_type, 0},
    {Screen6143_str7, 0, 0,  &Admin_var.Day_of_week, unsigned_char_type, 0},
    {Screen6143_str8, 0, 0,  &Admin_var.Day_of_week, unsigned_char_type, 0},
    {Screen6143_str9, 0, 0,  &Admin_var.Day_of_week, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_615200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL,             no_variable,        0},
    {Screen6152_str3, Screen6152_str4, Screen6152_str5,  &Admin_var.AM_PM, unsigned_char_type, 0},
    {Screen6152_str6, Screen6152_str7, Screen6152_str8,  &Admin_var.AM_PM, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_615300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL,                           no_variable,        0},
    {Screen6153_str3, Screen6153_str4, Screen6153_str5,  &Admin_var.Current_Time_Format, unsigned_char_type, 0},
    {Screen6153_str6, Screen6153_str7, Screen6153_str8,  &Admin_var.Current_Time_Format, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_617000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL, no_variable, 0},
    {Screen617_str1, Screen617_str2, Screen617_str3, NULL, no_variable, 1},
    {Screen617_str4, Screen617_str5, 0,              NULL, no_variable, 2},
};

__align(4) SCREEN_TYPE3_DATA Org_617100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               Screen6171_str1, NULL,
           no_variable,        0},
    {Screen6171_str2, Screen6171_str3, Screen6171_str4,  &Admin_var.Zero_rate_status,
           unsigned_char_type, 0},
    {Screen6171_str5, Screen6171_str6, Screen6171_str7,  &Admin_var.Zero_rate_status,
           unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_618000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen618_str1, NULL, no_variable, 0},
    {Screen618_str2, Screen618_str3, 0,              NULL, no_variable, 1},
    {Screen618_str4, Screen618_str5, 0,              NULL, no_variable, 2},
};

__align(4) SCREEN_TYPE3_DATA Org_619000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0, Screen619_str1,  NULL, no_variable, 0},
    {Screen619_str2, 0, Screen619_str3,  NULL, no_variable, 1},
    {Screen619_str4, 0, Screen619_str5,  NULL, no_variable, 2},
};

__align(4) SCREEN_TYPE3_DATA Org_619100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                0,                 NULL, no_variable, 0},
    {Screen61911_str1, Screen61911_str2, Screen61911_str3,  NULL, no_variable, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_619200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                0,                 NULL, no_variable, 0},
    {Screen61911_str1, Screen61911_str2, Screen61921_str1,  NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_619210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                0,                 NULL, no_variable, 0},
    {Screen61922_str1, Screen61922_str2, Screen61922_str3,  NULL, no_variable, 1},
};

__align(4) SCREEN_TYPE3_DATA Org_631000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL,
          no_variable,        0},
    {Screen631_str1, Screen631_str2, Screen631_str3,  &Admin_var.Setup_Wizard_Locked,
          unsigned_char_type, 0},
    {Screen631_str4, Screen631_str5, Screen631_str6,  &Admin_var.Setup_Wizard_Locked,
          unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_632000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL,
         no_variable,        0},
    {Screen631_str1, Screen631_str2, Screen632_str1,  &Admin_var.Calibration_Locked,
         unsigned_char_type, 0},
    {Screen631_str4, Screen631_str5, Screen632_str2,  &Admin_var.Calibration_Locked,
         unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_633000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL,
          no_variable,        0},
    {Screen631_str1, Screen631_str2, Screen633_str1,  &Admin_var.Zero_Calibration_Locked,
          unsigned_char_type, 0},
    {Screen631_str4, Screen631_str5, Screen633_str2,  &Admin_var.Zero_Calibration_Locked,
          unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_634000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL,
         no_variable,        0},
    {Screen631_str1, Screen634_str1, Screen634_str2,  &Admin_var.Setup_Devices_Locked,
         unsigned_char_type, 0},
    {Screen631_str4, Screen634_str3, Screen634_str4,  &Admin_var.Setup_Devices_Locked,
         unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_635000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL,                    no_variable,        0},
    {Screen631_str1, Screen634_str1, Screen635_str1,  &Admin_var.Admin_Locked, unsigned_char_type, 0},
    {Screen631_str4, Screen634_str3, Screen635_str2,  &Admin_var.Admin_Locked, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_636000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              0,              NULL,
          no_variable,        /*0,*/ 0},
    {Screen631_str1, Screen634_str1, Screen636_str1,  &Admin_var.Clear_Weight_Locked,
          unsigned_char_type, 0},
    {Screen631_str4, Screen634_str3, Screen636_str2,  &Admin_var.Clear_Weight_Locked,
          unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_637100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen63711_str1, NULL,  no_variable, 0},
    {Screen63711_str2, Screen63711_str3, Screen63711_str4, NULL,  no_variable, 1},
    {Screen63711_str5, Screen63711_str6, Screen63711_str7, NULL,  no_variable, 2},
};

__align(4) SCREEN_TYPE3_DATA Org_637310_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                0,                Screen63731_str1, NULL,  no_variable, 0},
    {Screen63731_str2, Screen63731_str3, Screen63731_str4, NULL,  no_variable, 1},
    {Screen63731_str5, Screen63731_str6, Screen63731_str7, NULL,  no_variable, 2},
};
__align(4) SCREEN_TYPE3_DATA Org_641000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL,  no_variable, 0},
    {Screen6412_str1, Screen6412_str2, Screen6412_str3, NULL,  no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_642000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL,  no_variable, 0},
    {Screen6413_str1, Screen6413_str2, Screen6413_str3, NULL,  no_variable, 1},
};

__align(4) SCREEN_TYPE3_DATA Org_644000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
	  {0,                0,                0,                    NULL,  no_variable, 0},
    {Screen6415_str1, Screen6415_str2, Screen6415_str3, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_642110_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0, 0,                 NULL,  no_variable, 0},
    {Screen641311_str0, 0, Screen641311_str1, NULL,  no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_642120_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen641312_str1, NULL,  no_variable, 0},
    {Screen641312_str2, 0,                 Screen641312_str3, NULL,  no_variable, 0},
    {Screen641312_str4, Screen641312_str5, Screen641312_str6, NULL,  no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_650000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             Screen65_str1, NULL,  no_variable, 0},
    {Screen65_str2, Screen65_str3, Screen65_str4, NULL,  no_variable, 1},
    {Screen65_str5, Screen65_str3, Screen65_str6, NULL,  no_variable, 2},
    {Screen65_str7, Screen65_str3, Screen65_str8, NULL,  no_variable, 3},
};

__align(4) SCREEN_TYPE3_DATA Org_661000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,              Screen661_str1, NULL,                   no_variable,        0},
    {Screen661_str2, Screen661_str3, Screen661_str4,  &Admin_var.DHCP_Status, unsigned_char_type, 0},
    {Screen661_str5, Screen661_str6, Screen661_str7,  &Admin_var.DHCP_Status, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_61A100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,                 0,              NULL, no_variable, 0},
    {Screen431_str2, Screen61A1_str1, Screen61A1_str2,  &Admin_var.Test_speed_status, unsigned_char_type, 1},
    {Screen431_str5, Screen61A1_str3, Screen61A1_str4,  &Admin_var.Test_speed_status, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_61A200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,              0,                 0,              NULL, no_variable, 0},
    {Screen431_str2, Screen61A2_str1, Screen61A2_str2,  &Admin_var.Test_load_status, unsigned_char_type, 1},
    {Screen431_str5, Screen61A2_str3, Screen61A2_str4,  &Admin_var.Test_load_status, unsigned_char_type, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_61B000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {0,             0,             0, NULL,
           no_variable,        0},
    {Screen61B1_str0, Screen61B1_str1, Screen61B1_str2,  &Misc_var.Language_select,
           unsigned_char_type, 0},
					 ////SKS 02-11-2016 BS-241
  /*  {Screen61B2_str0, Screen61B2_str1, Screen61B2_str2,  &Misc_var.Language_select,
           unsigned_char_type, 0},*/
};

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
