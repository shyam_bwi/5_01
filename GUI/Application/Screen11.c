/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen11.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"
#include "Calibration.h"
#include "File_update.h"
#include "EEPROM_high_level.h"
#include "Log_report_data_calculate.h"
#include "Calibration.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define PROG_BAR_X_POS    50
#define PROG_BAR_Y_POS    75
#define PROG_BAR_X_SIZE   150
#define PROG_BAR_YSIZE    30
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern int az_cal_on;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen11(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : static void callback_screen11(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen11(WM_MESSAGE * pMsg)
{

    int Id, NCode, sel,Key_entry;
    char file_name_restore[25];
    static SCREEN_TYPE11_DATA *data;
    int i;
    Screen_org_data * new_screen_org;

    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
    data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            break;

                        case WM_NOTIFICATION_SEL_CHANGED:
                            if (data[sel+1].Description_text_index != 0)
                            {
                                MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
                                MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[sel+1].Description_text_index].Text);
                            }
                            WM_SetFocus(GUI_data_nav.Listbox);
                            break;
                    }
                 break;

                default:
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CHILD_DELETED:
                            WM_SetFocus(GUI_data_nav.Listbox);
                            GUI_ClearKeyBuffer();
                            LISTBOX_SetSel(GUI_data_nav.Listbox, 1);
                            break;
                    }
                 break;
            }
            break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            ;
                           break;

                        case GUI_KEY_LEFT_CUSTOM:
                            GUI_data_nav.Cancel_operation = 1;
                            GUI_data_nav.Key_press = 1;
												    
												    //Added by PVK on 2 May 2016
							              if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_START_CAL) == 0))
														
														{ // Restore the previous span value.
															Calibration_var.new_span_value = Calibration_var.old_span_value;
														}
														else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYNAMIC_ZEROCAL_START) == 0)||
															       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, STATIC_ZEROCAL_START) == 0))
														{
															#ifdef CALCULATION
														 //PVK - Added on 27/4/2016 to restore old zero cal value when cancel the zero calibration
															Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
															#endif //#ifdef CALCULATION
														}	
                            az_cal_on =0;
  													log_data_populate(calibration_cancel);

                            break;

                        case GUI_KEY_RIGHT:
                            ;
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;

                        case GUI_KEY_ENTER:
                            if((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) &&
                                (GUI_data_nav.Percent_complete == 0))
                            {
//                                 if (strcmp(TEST_WEIGHT_START_CAL, GUI_data_nav.Current_screen_info->Screen_org_no) == 0)
//                                 {
//                                     Calib_struct.Cal_status_flag = START_TEST_WEIGHT_CAL;
//                                 }
                                if(strcmp(MAT_TST_START_CAL_ORG_NO, GUI_data_nav.Current_screen_info->Screen_org_no) == 0)
                                {
                                    Calib_struct.Cal_status_flag = START_MATERIAL_CAL;
                                }
//                                 else if(strcmp(DIGITAL_START_CAL, GUI_data_nav.Current_screen_info->Screen_org_no) == 0)
//                                 {
//                                     Calib_struct.Cal_status_flag = START_DIGITAL_CAL;
//                                 }
                                else if(strcmp(DYNAMIC_ZEROCAL_START , GUI_data_nav.Current_screen_info->Screen_org_no) == 0)
                                {    
																	  if(Sens_brd_param.No_of_pulses == 0)  
																		{
																				strcpy(GUI_data_nav.New_screen_num, DYNAMIC_ZEROCAL_ACK_ORG_NO);											 
																				GUI_data_nav.Change = 1;
																				GUI_data_nav.Child_present = 0;
																				GUI_data_nav.Wizard_screen_index = 1;
																				Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
																		}
																		else
																		{	
                                       Calib_struct.Cal_status_flag = START_DYNAMICZERO_CAL;
																			Dynamic_zero_cal_done_fg = 0;
																		}
                                }
                                else if (strcmp(STATIC_ZEROCAL_START, GUI_data_nav.Current_screen_info->Screen_org_no) == 0)
                                {
                                  Calib_struct.Cal_status_flag = START_STATICZERO_CAL;
																}	
                            }
                            if ((data[sel].screen_an_option != 0) && (GUI_data_nav.In_wizard == 0))
                            {
                                new_screen_org = GUI_data_nav.Current_screen_info->child;
                                new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                for (i = 0; i < 6; i++)
                                {
                                    GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                }
                                //GUI_data_nav.New_screen_num[6] = '\0';
                                GUI_data_nav.Change = 1;
                                GUI_data_nav.Child_present = 1;
                            }
                            else
                            {
                                GUI_data_nav.Child_present = 0;
                            }
                            if (GUI_data_nav.In_wizard && (Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG))
                            {
                              if (data[sel].screen_an_option != 0)
                              {
                                  new_screen_org = GUI_data_nav.Current_screen_info->child;
                                  new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                  for (i = 0; i < 6; i++)
                                  {
                                      GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                  }
                                  //GUI_data_nav.New_screen_num[6] = '\0';
                                  GUI_data_nav.Change = 1;
                                  GUI_data_nav.Child_present = 1;
                              }
                              else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, BACKUP_USB_FILE_STORE_ORG_NO) == 0) &&
                                  (GUI_data_nav.Percent_complete == 0))
                              {
																 // if USB is connected
																 if(Flags_struct.Connection_flags & USB_CON_FLAG)
	                               {
                                  //store the configuration backup to USB
                                  config_backup_write(Admin_var.Backup_file_name);
                                  GUI_data_nav.Percent_complete = 100;
                                  //set the flag to move the progress bar
                                  Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
																 }
                              }
                              else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_FILE_FROM_USB_ORG_NO) == 0) &&
                                  (GUI_data_nav.Percent_complete == 0))
                              {
                                  //reload the configuration backup from USB
																 if(Flags_struct.Connection_flags & USB_CON_FLAG)
	                               {
                                  memset(file_name_restore, 0, sizeof(file_name_restore));
                                  strcpy(file_name_restore, Admin_var.Reload_file_name);
                                  config_backup_read(Admin_var.Reload_file_name);
                                  strcpy(Admin_var.Reload_file_name, file_name_restore);
                                  GUI_data_nav.Percent_complete = 100;
                                  //set the flag to move the progress bar
                                  Calib_struct.Set_progress_bar = PROGRESS_BAR_MOVE;
																 }
                              }
                              else
                              {
                                  GUI_data_nav.Key_press = 1;
                              }
                            }
                            break;

                        case GUI_KEY_BACK:
													  //Added by PVK on 16 May 2016
													  Restore_Calib_Value_Upon_Cancel_Calibration_Process();
                            go_back_one_level();
//                             if(GUI_data_nav.In_wizard)
//                             {
//                                 GUI_data_nav.Back_one_level = GO_BACK_FLAG;
//                             }
                            break;
                    }
                    break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen11(SCREEN_TYPE11_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen11(SCREEN_TYPE11_DATA *data, int count)
{
  int i;

  GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX_SIZE_X, LISTBOX_SIZE_Y,
                                           WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
  for (i = 0; i < count; i++)
  {
      LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
  }

  LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
  LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
  LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
  LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
  LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
  LISTBOX_SetItemSpacing (GUI_data_nav.Listbox, 30);
  LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
  LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

  LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
  LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
  LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
  LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

  WM_SetCallback(WM_HBKWIN, callback_screen11);
  LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

  GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(MULTI_EDIT_START, MULTI_WIN_OFFSET,
                                               MULTI_EDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                               WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
  MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
  MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
  MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
  MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
  MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
  MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);

  if (data[0].Description_text_index != 0)
  {
      MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
      MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
      MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Description_text_index].Text);
  }

  //* Create the progbar */
  GUI_data_nav.Prog_bar = PROGBAR_CreateEx(PROG_BAR_X_POS, PROG_BAR_Y_POS, PROG_BAR_X_SIZE, PROG_BAR_YSIZE,
                                            GUI_data_nav.Multi_text, WM_CF_SHOW, PROGBAR_CF_HORIZONTAL, 0);

  /* Use memory device (optional, for better looks) */
  PROGBAR_EnableMemdev(GUI_data_nav.Prog_bar);
  PROGBAR_SetFont(GUI_data_nav.Prog_bar, &GUI_Font8x16);
  PROGBAR_SetText(GUI_data_nav.Prog_bar, NULL);
  PROGBAR_SetBarColor(GUI_data_nav.Prog_bar, 0, GUI_LIGHTYELLOW);
  PROGBAR_SetBarColor(GUI_data_nav.Prog_bar, 1, GUI_GRAY);
  PROGBAR_SetTextColor(GUI_data_nav.Prog_bar, 0, GUI_BLACK);
  PROGBAR_SetTextColor(GUI_data_nav.Prog_bar, 1, GUI_BLACK);

  //GUI_data_nav.Percent_complete = 0;

  PROGBAR_SetValue(GUI_data_nav.Prog_bar, GUI_data_nav.Percent_complete);

  WM_SetFocus(GUI_data_nav.Listbox);
  return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
