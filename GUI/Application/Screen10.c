/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen10.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_common_func.h"
#include "Screen_structure.h"
#include "Screen_global_ex.h"
#include "Calibration.h"
#include "Log_report_data_calculate.h"
#include "Calibration.h"
#include <string.h>

/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*======================================================
======================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen10(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : static void callback_screen10(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
extern int az_cal_on ;
static void callback_screen10(WM_MESSAGE * pMsg)
{
    int Id, NCode, sel,Key_entry;
    int i, count = 0, length = 0, counter = 1;
    char *last_ptr, char_data;
    static SCREEN_TYPE10_DATA *data;
    Screen_org_data * new_screen_org;

    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
    data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            break;

                        case WM_NOTIFICATION_SEL_CHANGED:
                            if (data[sel+1].Description_text_index != 0)
                            {
                                count = 4;
                                if (data[sel+1].Variable4 == NULL)
                                {
                                    count = count - 1;
                                }
                                if (data[sel+1].Variable3 == NULL)
                                {
                                    count = count - 1;
                                }
                                if (data[sel+1].Variable2 == NULL)
                                {
                                    count = count - 1;
                                }
                                if (data[sel+1].Variable1 == NULL)
                                {
                                    count = count-1;
                                }

                                MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
                                if (count > 0)
                                {
                                    length = strlen(Strings[data[sel+1].Description_text_index].Text);
                                    for (i = 0; i < length; i++)
                                    {
                                        if(*(Strings[data[sel+1].Description_text_index].Text + i) == 'X')
                                        {
                                            switch(counter)
                                            {
                                                case 1:
                                                    multiedit_insert_txt(data[sel+1].data_type1 ,data[sel+1].Variable1);
																								    GUI_data_nav.Text_widget5 = GUI_data_nav.Text_widget1;
                                                    break;
                                                case 2:
                                                    multiedit_insert_txt(data[sel+1].data_type2 ,data[sel+1].Variable2);
                                                    break;
                                                case 3:
                                                    multiedit_insert_txt(data[sel+1].data_type3 ,data[sel+1].Variable3);
																								    GUI_data_nav.Text_widget3 = GUI_data_nav.Text_widget1;
                                                    break;
                                                case 4:
                                                    multiedit_insert_txt(data[sel+1].data_type4 ,data[sel+1].Variable4);
                                                    break;
                                            }
                                            i = i+4;
                                            counter++;
                                            if (counter == (count+1))
                                            {
                                                break;
                                            }
                                        }
                                        char_data = *(Strings[data[sel+1].Description_text_index].Text+i);
                                        MULTIEDIT_AddText(GUI_data_nav.Multi_text, &char_data);
                                    }
                                    last_ptr = strrchr(Strings[data[sel+1].Description_text_index].Text, 'XXX');
                                    MULTIEDIT_AddText(GUI_data_nav.Multi_text, (last_ptr+2));
                                }
                                else
                                {
                                    MULTIEDIT_AddText(GUI_data_nav.Multi_text, \
                                                      Strings[data[sel+1].Description_text_index].Text);
                                }
                            }
                            WM_SetFocus(GUI_data_nav.Listbox);
                            break;
                    }
                 break;

                default:
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CHILD_DELETED:
                            WM_SetFocus(GUI_data_nav.Listbox);
                            GUI_ClearKeyBuffer();
                            LISTBOX_SetSel(GUI_data_nav.Listbox,1);
                            break;
                    }
                 break;
            }
         break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            ;
                           break;

                        case GUI_KEY_LEFT_CUSTOM:
                            GUI_data_nav.Cancel_operation = 1;
                            GUI_data_nav.Key_press = 1;
												     if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WT_CAL_ORG_NO) == 0)
															   
                             { // Restore the previous span value.
															  Calibration_var.new_span_value = Calibration_var.old_span_value;
														 }
														 else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYN_ZER_CAL_ACPT_ORG_NO) == 0) ||
															        (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYNAMIC_ZEROCAL_ACK_ORG_NO) == 0) ||
														          (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_ZER_ORG_NO) == 0)
														         )   
														 {
																#ifdef CALCULATION
															 //PVK - Added on 27/4/2016 to restore old zero cal value when cancel the zero calibration
																Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
															 
																#endif //#ifdef CALCULATION
														 }
														 	az_cal_on =0;													 
														 log_data_populate(calibration_cancel);
                            break;

                        case GUI_KEY_RIGHT:
                            ;
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
                            if (GUI_data_nav.In_wizard == 1 )
                            {
														    //go to the prev screen for re-calibration and store the current values as the prev ones
                                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WT_CAL_ORG_NO) == 0))
                                {
                                    GUI_data_nav.Up_right_key_pressed = 1;
                                    Calib_struct.Cal_status_flag = START_TEST_WEIGHT_CAL;
                                    #ifdef CALCULATION
                                    Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                                    #endif //#ifdef CALCULATION
                                }
																else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAT_TST_CAL_ACPT_ORG_NO) == 0))
																{
																	  GUI_data_nav.Up_right_key_pressed = 1;
																	  #ifdef CALCULATION
                                    Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                                    #endif //#ifdef CALCULATION
																}
                                else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, AUTO_BELT_LEN_CAL_ACPT_ORG_NO) == 0) ||
                                         (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_LEN_ORG_NO) == 0))
                                {
                                    GUI_data_nav.Up_right_key_pressed = 2;
                                    Calculation_struct.Belt_length_calc = Calibration_var.New_belt_length * \
                                             Calculation_struct.Belt_length_conv_factor; // in inches
                                }
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_ZER_ORG_NO) == 0)
                                {
                                    GUI_data_nav.Up_right_key_pressed = 3;
                                    #ifdef CALCULATION
                                    Calculation_struct.Zero_weight = Calibration_var.New_zero_value / \
                                                                     Calculation_struct.Zero_value_conv_factor; //Calculation_struct.Weight_conv_factor;
                                    #endif //#ifdef CALCULATION
                                }
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYN_ZER_CAL_ACPT_ORG_NO) == 0)
                                {
                                    GUI_data_nav.Up_right_key_pressed = 1;
                                    #ifdef CALCULATION
                                    Calculation_struct.Zero_weight = Calibration_var.New_zero_value / \
                                                                     Calculation_struct.Zero_value_conv_factor;//Calculation_struct.Weight_conv_factor;
                                    #endif //#ifdef CALCULATION
                                }
                                //if screen change is required
                                if (GUI_data_nav.Up_right_key_pressed >= 1)
                                {
                                    GUI_data_nav.Key_press = 1;
                                    GUI_data_nav.GUI_structure_backup = 1;
                                }
                            }
                            if (data[sel].screen_an_option == 0)
                            {
                                GUI_data_nav.Child_present = 0;
                            }
														az_cal_on =0;
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            if (GUI_data_nav.In_wizard == 1)
                            {
															  //go to the prev screen for re-calibration and don't
                                //store the current values as the prev one
                                if( strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WT_CAL_ORG_NO) == 0)
                                {   
                                    GUI_data_nav.Up_right_key_pressed = 1;
																	  Calib_struct.Cal_status_flag = START_TEST_WEIGHT_CAL;
																	  Calibration_var.new_span_value = Calibration_var.old_span_value;
                                }
																else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAT_TST_CAL_ACPT_ORG_NO) == 0))
																{
																	  GUI_data_nav.Up_right_key_pressed = 1;
																}
															  else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYN_ZER_CAL_ACPT_ORG_NO) == 0))
																{
																	  GUI_data_nav.Up_right_key_pressed = 1;
																}
                                else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_LEN_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, AUTO_BELT_LEN_CAL_ACPT_ORG_NO) == 0))
                                {
                                    GUI_data_nav.Up_right_key_pressed = 2;
                                }
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_ZER_ORG_NO) == 0)
                                {
                                    GUI_data_nav.Up_right_key_pressed = 3;
                                }
																if (GUI_data_nav.Up_right_key_pressed >= 1)
                                {
                                    GUI_data_nav.Key_press = 1;
																}
                            }
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;

                        case GUI_KEY_ENTER:
													  if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYNAMIC_ZEROCAL_ACK_ORG_NO) == 0) )
														{
															GUI_data_nav.Change = 1;
															if (GUI_data_nav.Current_screen_info->screens != NULL)
															{
																  for (i = 0; i < 6; i++)
                                  {
                                    GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->screens->Next->number[i];
                                  }
															}
														}
													
                            if (data[sel].screen_an_option != 0)
                            {
                                new_screen_org = GUI_data_nav.Current_screen_info->child;
                                new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                for (i = 0; i < 6; i++)
                                {
                                    GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                }
                                //GUI_data_nav.New_screen_num[6] = '\0';
                                GUI_data_nav.Change = 1;
                                GUI_data_nav.Child_present = 1;
                            }
                            if (GUI_data_nav.In_wizard == 1)
                            {
                                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WT_CAL_ORG_NO) == 0) ||
                                     (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAT_TST_CAL_ACPT_ORG_NO) == 0) ||
                                     (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DIG_CAL_ACPT_ORG_NO) == 0))
                                {
                                    //go to the prev screen for re-calibration and store the current values as the prev ones
                                    #ifdef CALCULATION
                                    Calculation_struct.Total_trim_factor = Calibration_var.new_span_value;
                                    #endif //#ifdef CALCULATION
                                }
                                else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_LEN_ORG_NO) == 0) ||
                                          (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, AUTO_BELT_LEN_CAL_ACPT_ORG_NO) == 0))
                                {
                                    //store the current values
                                    Calculation_struct.Belt_length_calc = Calibration_var.New_belt_length * \
                                             Calculation_struct.Belt_length_conv_factor; //in inches
                                }
                               else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DYN_ZER_CAL_ACPT_ORG_NO) == 0)||
                                        (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LEN_ZER_CAL_ACPT_ZER_ORG_NO) == 0))
                                {
                                    //store the current values
                                    #ifdef CALCULATION
                                    Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
                                                                     / Calculation_struct.Zero_value_conv_factor;//Calculation_struct.Weight_conv_factor;
                                    #endif //#ifdef CALCULATION
																	// by megha for auto zero cycle					
																						Dynamic_zero_cal_done_fg = 1;
																						Auto_zero_Start_flag = 0;																		
                                }
                                GUI_data_nav.GUI_structure_backup = 1;
                            }
                            GUI_data_nav.Key_press = 1;
														az_cal_on =0;
                            break;

                        case GUI_KEY_BACK:
													  //Added by PVK on 16 May 2016
												    Restore_Calib_Value_Upon_Cancel_Calibration_Process();
                            go_back_one_level();
//                             if(GUI_data_nav.In_wizard)
//                             {
//                                 GUI_data_nav.Back_one_level = GO_BACK_FLAG;
//                             }
												az_cal_on =0;
                            break;
                    }
                    break;
            }
            break;

            default:
                WM_DefaultProc(pMsg);
              break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen10(SCREEN_TYPE10_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen10(SCREEN_TYPE10_DATA *data, int count)
{
  int i,j,k, length = 0, counter = 1;
  char *last_ptr, char_data;
	unsigned char  cTmp_string[400];
  GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX_SIZE_X, LISTBOX_SIZE_Y, WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
  for (i = 0; i < count; i++)
  {
      LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
  }

  LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
  LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
  LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
  LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
  LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
  LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
  LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
  LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

  LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
  LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
  LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
  LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

  WM_SetCallback(WM_HBKWIN, callback_screen10);
  LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

  GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(MULTI_EDIT_START, MULTI_WIN_OFFSET,
                                               MULTI_EDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                               WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1,400,NULL);
  MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
  MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
  MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
  MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
  MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
  MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);

  if (data[0].Description_text_index != 0)
  {
    count = 4;
    if (data[0].Variable4 == NULL)
    {
        count = count - 1;
    }
    if (data[0].Variable3 == NULL)
    {
        count = count - 1;
    }
    if (data[0].Variable2 == NULL)
    {
        count = count - 1;
    }
    if (data[0].Variable1 == NULL)
    {
        count = count-1;
    }

//    MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
//    MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
    if (count > 0)
    {
        length = strlen(Strings[data[0].Description_text_index].Text);
				
				for (i = 0; i < length; i++)
        {
					char_data = *(Strings[data[0].Description_text_index].Text+i);
					if(char_data != 'X')
					{
						cTmp_string[k] = char_data;
						k++;
						
					}
					else
					{
						cTmp_string[k] = '\0';
						
						MULTIEDIT_AddText(GUI_data_nav.Multi_text,(cTmp_string )) ;
                switch(counter)
                {
                    case 1:
                        multiedit_insert_txt(data[0].data_type1 ,data[0].Variable1);
										    GUI_data_nav.Text_widget5 = GUI_data_nav.Text_widget1;
                        break;

                    case 2:
                        multiedit_insert_txt(data[0].data_type2 ,data[0].Variable2);
                        break;

                    case 3:
                        multiedit_insert_txt(data[0].data_type3 ,data[0].Variable3);
										    GUI_data_nav.Text_widget3 = GUI_data_nav.Text_widget1;
                        break;

                    case 4:
                        multiedit_insert_txt(data[0].data_type4 ,data[0].Variable4);
                        break;
                }
                i = i+3;
								k = 0;
                counter++;
                if (counter == (count+1))
                {
                  break;
                }						
					}
				}
        last_ptr = strrchr(Strings[data[0].Description_text_index].Text, 'XXX');
				MULTIEDIT_AddText(GUI_data_nav.Multi_text, (last_ptr+2));//(last_ptr+2));				
			}
	

/*				
        for (i = 0; i < length; i++)
        {
										
//            if(*(Strings[data[0].Description_text_index].Text + i) == 'X')
            if(char_data == 'X')

            {
                switch(counter)
                {
                    case 1:
                        multiedit_insert_txt(data[0].data_type1 ,data[0].Variable1);
										    GUI_data_nav.Text_widget5 = GUI_data_nav.Text_widget1;
                        break;

                    case 2:
                        multiedit_insert_txt(data[0].data_type2 ,data[0].Variable2);
                        break;

                    case 3:
                        multiedit_insert_txt(data[0].data_type3 ,data[0].Variable3);
										    GUI_data_nav.Text_widget3 = GUI_data_nav.Text_widget1;
                        break;

                    case 4:
                        multiedit_insert_txt(data[0].data_type4 ,data[0].Variable4);
                        break;
                }
                i = i+4;
                counter++;
                if (counter == (count+1))
                {
                  break;
                }
            }
					}
					
				 char_data = *(Strings[data[0].Description_text_index].Text+i);		
         MULTIEDIT_AddText(GUI_data_nav.Multi_text,&char_data);
        }
 //       last_ptr = strrchr(Strings[data[0].Description_text_index].Text, 'XXX');
//				MULTIEDIT_AddText(GUI_data_nav.Multi_text, (last_ptr+2));//(last_ptr+2));

				
			}
			*/
    else
    {
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Description_text_index].Text);
    }
  }

  WM_SetFocus(GUI_data_nav.Listbox);
  return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
