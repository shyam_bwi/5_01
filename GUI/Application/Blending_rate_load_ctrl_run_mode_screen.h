/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Blending_rate_load_strl_run_mode_screen.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012, 5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef  BLENDING_RATE_LOAD_STRL_RUN_MODE_SCREEN
#define  BLENDING_RATE_LOAD_STRL_RUN_MODE_SCREEN

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Sensor_board_data_process.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/

__align(4) BLEND_RATE_LOAD_CTRL_RUNMODE_SCREEN_DATA Org_730000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen73_str1, Screen73_str2,Screen7_str1, Screen73_str9, Screen73_str10,&Calculation_struct.Total_weight, &Calculation_struct.Rate_for_display,
     &Setup_device_var.Target_rate,             NULL,                         &Scale_setup_var.Rate_time_unit,&Scale_setup_var.Weight_unit, 1},
    {Screen73_str3, Screen73_str4,Screen7_str1, 0, 0,NULL, NULL, NULL, NULL,NULL,NULL,                      2},
    {Screen451_str4, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, NULL, NULL,NULL,                            												3},
//    {Screen73_str7, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, &Setup_device_var.PID_I_Term, NULL,NULL,                            0},
//    {Screen73_str8, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, &Setup_device_var.PID_D_Term, NULL,NULL,                            0},
};

__align(4) BLEND_RATE_LOAD_CTRL_RUNMODE_SCREEN_DATA Org_740000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen73_str1, Screen75_str1,Screen7_str1, Screen75_str3, Screen75_str4,&Calculation_struct.Total_weight, &Calculation_struct.Total_weight,
      &Setup_device_var.Target_Load,                 NULL,                         &Scale_setup_var.Belt_length_unit,&Scale_setup_var.Weight_unit, 1},
    {Screen75_str2, Screen73_str4,Screen7_str1, 0, 0,NULL, NULL, NULL, NULL,  NULL,NULL,																											 2},
    {Screen451_str4, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, NULL, NULL,NULL,                              												 3},
//    {Screen73_str7, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, &Setup_device_var.PID_I_Term, NULL,NULL,                              0},
//    {Screen73_str8, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, &Setup_device_var.PID_D_Term, NULL,NULL,                              0},
};

__align(4) BLEND_RATE_LOAD_CTRL_RUNMODE_SCREEN_DATA Org_750000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen73_str1, Screen74_str1,Screen7_str1, Screen73_str9, Screen73_str10,&Calculation_struct.Total_weight, &Calculation_struct.Rate_for_display,
      &Setup_device_var.Target_rate,             NULL,                         &Scale_setup_var.Rate_time_unit,&Scale_setup_var.Weight_unit, 1},
    {Screen74_str2, Screen73_str4,Screen7_str1, 0, 0,NULL, NULL, NULL, NULL,NULL,NULL,											 2},
    {Screen451_str4, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, NULL, NULL,NULL,                            												 3},
//    {Screen73_str7, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, &Setup_device_var.PID_I_Term, NULL,NULL,                            0},
//    {Screen73_str8, Screen73_str6,Screen7_str1, 0, 0,NULL, NULL, NULL, &Setup_device_var.PID_D_Term, NULL,NULL,                            0},
};


#endif
/*****************************************************************************
* End of file
*****************************************************************************/
