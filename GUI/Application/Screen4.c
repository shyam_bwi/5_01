/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen4.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"
#include "Global_ex.h"
#include "Screen_data_enum.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen4(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : static void callback_screen4(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen4(WM_MESSAGE * pMsg)
{
    int Id, NCode, sel,Key_entry;
    static SCREEN_TYPE4_DATA *data;
    int i;
    Screen_org_data * new_screen_org;

    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
    data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            break;

                        case WM_NOTIFICATION_SEL_CHANGED:
                            if (data[0].Description_text_index != 0)
                            {
                                MULTIEDIT_SetText(GUI_data_nav.Multi_text, " ");
                                MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Description_text_index].Text);
                            }
                            else
                            {
                                MULTIEDIT_SetText(GUI_data_nav.Multi_text, " ");
                            }
                            if (data[sel+1].Description_text_index != 0)
                            {
                                MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
                                MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[sel+1].Description_text_index].Text);
                            }
                            IMAGE_SetBitmap(GUI_data_nav.Image_display, data[sel + 1].pBitmap);
                            WM_SetFocus(GUI_data_nav.Listbox);
                            break;
                    }
                break;

                default:
                    switch(NCode)
                    {
                    case WM_NOTIFICATION_CHILD_DELETED:
                        WM_SetFocus(GUI_data_nav.Listbox);
                        GUI_ClearKeyBuffer();
                        LISTBOX_SetSel(GUI_data_nav.Listbox, 1);
                        break;
                    }
                    break;
            }
            break;

            case WM_KEY:
                Id = WM_GetId(pMsg->hWinSrc);
                Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
                switch(Id)
                {
                    case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                        switch (Key_entry)
                        {
                            case GUI_KEY_LEFT:
                                ;
                               break;

                            case GUI_KEY_LEFT_CUSTOM:
                                ;
                                break;

                            case GUI_KEY_RIGHT:
                                ;
                                break;

                            case GUI_KEY_RIGHT_CUSTOM:
                                ;
                                break;

                            case GUI_KEY_UP:
                                ;
                                break;

                            case GUI_KEY_UP_CUSTOM:
                                ;
                                break;

                            case GUI_KEY_DOWN:
                                ;
                                break;

                            case GUI_KEY_ENTER:
                                if (data[sel+1].screen_an_option != 0)
                                {
                                    new_screen_org = GUI_data_nav.Current_screen_info->child;
                                    new_screen_org = &new_screen_org[(data[sel+1].screen_an_option - 1)];
                                    for (i = 0; i < 6; i++)
                                    {
                                        GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                    }
                                    GUI_data_nav.Change = 1;
                                    GUI_data_nav.Child_present = 1;
                                }
                                else if (!GUI_data_nav.In_wizard)
                                {
                                    go_back_one_level();
                                    GUI_data_nav.Child_present = 0;
                                }
                                else
                                {
                                    GUI_data_nav.Child_present = 0;
                                }
                                sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
                                GUI_data_nav.Key_press = 1;
                                if ((data[sel+1].Variable1 != NULL) &&
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, "343000")))
                                {
                                    if((!security_bit_set()) || (password_struct.password_entered))
                                    {
																			  if(data[sel+1].data_type1 == unsigned_char_type)
																					*data[sel+1].Variable1 = data[sel+1].Main_text_index;
																				else
                                        custom_sscanf(data[sel+1].data_type1, data[sel+1].Variable1, Strings[data[sel+1].Main_text_index].Text);
                                        GUI_data_nav.GUI_structure_backup = 1;
                                    }
                                    else
                                    {
                                        //create the password screen, store the current screen org number to come back to it
                                        //in case the password entered is correct
                                        GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info;
                                        GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
                                        password_struct.password_to_be_entered = 1;
                                    }
                                }
                                break;

                            case GUI_KEY_BACK:
															 go_back_one_level();
                               break;
                        }
                        break;

                    case ID_MULTI_TEXT_1:
                        break;
                }
                break;

            default:
                WM_DefaultProc(pMsg);
                break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen4(SCREEN_TYPE4_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets. Also adds the required
*                            : images on screen.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen4(SCREEN_TYPE4_DATA *data, int count)
{
    int i,k, length = 0, selection = 0;
    char *last_ptr, char_data;
	  char char_data1[ARRAY_SIZE],cTmp_string[400];

    GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX_SIZE_X, LISTBOX_SIZE_Y,
                                             WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);

	  custom_sprintf(data[1].data_type1, Strings[*data[1].Variable1].Text, char_data1, sizeof(char_data1));
    for (i = 1; i < count; i++)
    {
			  LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
			  if (strcmp(char_data1, Strings[data[i].Main_text_index].Text) == 0)
				{
					  selection = (i - 1);
				}
    }

    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
    LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
    LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
    LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
    LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
    LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

    WM_SetCallback(WM_HBKWIN, callback_screen4);
    LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

    GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(MULTI_EDIT_START, MULTI_WIN_OFFSET,
                                                 MULTI_EDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                                 WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 400, NULL);
    MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
    MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
    MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
    MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
    MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
    MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
    if (data[0].Description_text_index != 0)
    {
        MULTIEDIT_SetText(GUI_data_nav.Multi_text, " ");
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Description_text_index].Text);
    }
    else
    {
        MULTIEDIT_SetText(GUI_data_nav.Multi_text, " ");
    }
    if (data[1].Description_text_index != 0)
    {
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
        if (data[1].Variable1 != NULL)
        {
            length = strlen(Strings[data[1].Description_text_index].Text);
						k = 0;
            for (i = 0; i < length; i++)
            {
								char_data = *(Strings[data[1].Description_text_index].Text + i);
								if(char_data != 'X')
								{
									cTmp_string[k] = char_data;
									k++;
									
								}
								else
								{
									cTmp_string[k] = '\0';
									
									MULTIEDIT_AddText(GUI_data_nav.Multi_text,(cTmp_string )) ;
									multiedit_insert_txt(data[1].data_type1 ,Strings[*data[1].Variable1].Text);
									i = i+4;
									k = 0;
									break;						
								}
							
						}
/*				
            for (i = 0; i < length; i++)
            {
                if(*(Strings[data[1].Description_text_index].Text + i) == 'X')
                {
                    multiedit_insert_txt(data[1].data_type1 ,Strings[*data[1].Variable1].Text);
                    i = i+4;
                    break;
                }
                char_data = *(Strings[data[1].Description_text_index].Text+i);
                MULTIEDIT_AddText(GUI_data_nav.Multi_text, &char_data);
            }
*/				
            if(i!= length)
            {
                last_ptr = strrchr(Strings[data[1].Description_text_index].Text, 'XXX');
                MULTIEDIT_AddText(GUI_data_nav.Multi_text, (last_ptr+2));
            }
        }
        else
        {
            MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[1].Description_text_index].Text);
        }
    }
    if ((GUI_data_nav.Current_screen_info->Screen_org_no[0] == '2') &&
        (GUI_data_nav.Current_screen_info->Screen_org_no[1] == '8'))
    {
        GUI_data_nav.Image_display1 = IMAGE_CreateEx(IMAGE_X_POS, 65,  IMAGE_X_SIZE, IMAGE_Y_SIZE, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, 0);
        switch(Scale_setup_var.Number_of_idlers)
        {
            case 1:
                IMAGE_SetBitmap(GUI_data_nav.Image_display1, &bmsingledims);
                break;

            case 2:
                IMAGE_SetBitmap(GUI_data_nav.Image_display1, &bmdualdims);
                break;

            case 3:
                IMAGE_SetBitmap(GUI_data_nav.Image_display1, &bmtripledims);
                break;

            case 4:
                IMAGE_SetBitmap(GUI_data_nav.Image_display1, &bmquaddims);
                break;
        }
    }
    else
    {
        GUI_data_nav.Image_display = IMAGE_CreateEx(IMAGE_X_POS, IMAGE_Y_POS, IMAGE_X_SIZE, IMAGE_Y_SIZE, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, 0);
        IMAGE_SetBitmap(GUI_data_nav.Image_display, data[1].pBitmap);
    }

    WM_SetFocus(GUI_data_nav.Listbox);
		LISTBOX_SetSel(GUI_data_nav.Listbox, selection);
    return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
