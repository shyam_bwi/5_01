/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen4_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012, 5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN4_DATA
#define  SCREEN4_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
__align(4) SCREEN_TYPE4_DATA Org_220000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {NULL,      0,               0,             Screen22_str1,  NULL,
         no_variable,       /*0,*/ 0},
    { &bmSingle, Screen22_str2,  Screen22_str3,  Screen22_str4,   &Scale_setup_var.Number_of_idlers,
        unsigned_int_type, /*0,*/ 0},
    { &bmdual,   Screen22_str5,  Screen22_str6,  Screen22_str7,   &Scale_setup_var.Number_of_idlers,
        unsigned_int_type, /*0,*/ 0},
    { &bmtriple, Screen22_str8,  Screen22_str9,  Screen22_str10,  &Scale_setup_var.Number_of_idlers,
       unsigned_int_type, /*0,*/ 0},
    { &bmquad,   Screen22_str11, Screen22_str12, Screen22_str13,  &Scale_setup_var.Number_of_idlers,
       unsigned_int_type, /*0,*/ 0},
};

// __align(4) SCREEN_TYPE4_DATA Org_270000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {NULL, 0,              0,               0,            NULL, no_variable, /*0,*/ 0},
//     {NULL, Screen2_str13,  Screen27_str1,  Screen27_str2, NULL, no_variable, /*0,*/ 1},
// };

__align(4) SCREEN_TYPE4_DATA Org_280000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {NULL, 0,              0,              0,             NULL, no_variable, /*0,*/ 0},
    {NULL, Screen2_str15,  Screen28_str1,  Screen28_str1, NULL, no_variable, /*0,*/ 1},
};

__align(4) SCREEN_TYPE4_DATA Org_343000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {NULL,        0,                 0,                 0,          NULL,                              
        no_variable,        /*0,*/ 0},
    { &bmlenthcal, Screen343_str0,  Screen343_str1,  Screen343_str2,  &Scale_setup_var.Belt_length_unit,
        unsigned_char_type, /*0,*/ 1},
};

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/

