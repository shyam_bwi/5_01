/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project            : Beltscale Weighing Product - Integrator Board
 * @detail Customer           : Beltway
 *
 * @file Filename             : Display_task.c
 * @brief                     : Controller Board
 *
 * @author                    : Anagha Basole
 *
 * @date Created              : November, 2012
 * @date Last Modified        : November, 2012
 *
 * @internal Change Log       : <YYYY-MM-DD>
 * @internal                  :
 * @internal                  :
 *
 *****************************************************************************/

/*============================================================================
 * Include Header Files
 *===========================================================================*/
#include "stddef.h"
#include "GUI.h"
#include "Global_ex.h"
#include "DIALOG.h"
#include "Display_task.h"
#include "rtc.h"
#include "RTOS_main.h"
#include "Screenshot_bmp.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "Screen_common_func.h"
#include "version.h"
#include "Calibration.h"
#include "Keypad.h"
#include "Log_report_data_calculate.h"
#include "IOProcessing.h"
#include "Plant_connect.h"
#include "Rate_blending_load_ctrl_calc.h"
#include "soft_stack.h"
#include "Ext_flash_high_level.h"
#include "Modbus_tcp.h"
#include <RTL.h>

//#define SUPERIOR		//SKS comment out thisl line if using a normal build
#ifdef GUI
/*============================================================================
 * Private Macro Definitions
 *===========================================================================*/

/*============================================================================
 * Private Data Types
 *===========================================================================*/

/*============================================================================
 *Public Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
 * Private Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
// char *Units[] = {
// 	Units_var.Unit_rate_variable,
// 	Units_var.Unit_speed_variable,
// };
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */
I32 used;
extern int az_cal_on;
/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
 * Private Function Prototypes Declarations
 *===========================================================================*/
__task void update_lcd(void);
static void init_Setup_device_var(void);
static void init_Calibration_var(void);
static void init_Scale_setup_var(void);
static void init_Admin_var(void);
void init_EEP_conf_vars(void);
/*============================================================================
 * Function Implementation Section
 *===========================================================================*/
/*****************************************************************************
 * @note       Function name  : void screen_variables_init(void)
 * @returns    returns        : None
 * @param      arg1           : None
 * @author                    : Anagha Basole
 * @date       date created   : 26th December 2012
 * @brief      Description    : Initializes the GUI variables to their default values.
 * @note       Notes          : None
 *****************************************************************************/

void init_Scale_setup_var(void)
{
    Scale_setup_var.Belt_length_unit = FEET;
    Scale_setup_var.Conveyor_angle = CONVEYOR_ANGLE_DEFAULT;
    Scale_setup_var.AngleSensor_Install_status = Screen271_M_str4; //Not Installed
    Scale_setup_var.Custom_LC_unit = Screen23121_str5;
    Scale_setup_var.Custom_LC_output = 2;
    Scale_setup_var.Custom_LC_capacity = 100;
    Scale_setup_var.Distance_unit = Screen24_str2; //English
    Scale_setup_var.Idler_distanceA = 48;
    Scale_setup_var.Idler_distanceB = 48;
    Scale_setup_var.Idler_distanceC = 48;
    Scale_setup_var.Idler_distanceD = 48;
    Scale_setup_var.Idler_distanceE = 48;
    Scale_setup_var.Idler_unit = INCHES;
    Scale_setup_var.Load_cell_size = Screen23_str8;
    Scale_setup_var.Number_of_idlers = 1;
    Scale_setup_var.Pulses_Per_Revolution = PULSES_PER_REV_BELTWAY_DEF;
    Scale_setup_var.Rate_time_unit = Screen26_str2;
    Scale_setup_var.Run_mode = Screen21_str2;
    Scale_setup_var.Speed_unit = FEET;
    Scale_setup_var.User_Belt_Speed = 0.0;
    Scale_setup_var.Weight_unit = TONS; //Tons
    Scale_setup_var.Wheel_diameter = WHEEL_DIAMETER_BELTWAY_DEF;
    Scale_setup_var.User_Belt_Speeed_WD = WHEEL_DIAMETER_BELTWAY_DEF;
    Scale_setup_var.Wheel_diameter_unit = INCHES;
    Scale_setup_var.Decimal_digits = ONE_DECIMAL_DIGIT;
    Scale_setup_var.IO_board_install = INSTALLED;
    Setup_device_var.Analog_Output1 = 0.0;
    Setup_device_var.Analog_Output2 = 0.0;

    CalcCRC((unsigned char*) &Scale_setup_var,
            (unsigned char*) &Scale_setup_var.CRC_SCALE_SETUP,
            (unsigned int) offsetof(SCALE_SETUP_STRUCT, CRC_SCALE_SETUP));
}

void init_Calibration_var(void)
{
    Calibration_var.Test_weight = 100.00;
    Calibration_var.Belt_scale_weight = 1;
    Calibration_var.Cert_scale_weight = 1;
    Calibration_var.Old_belt_length = 1;
    Calibration_var.New_belt_length = 1;
    Calibration_var.Belt_length_diff = 1;
    Calibration_var.Old_zero_value = 1;
    Calibration_var.New_zero_value = 1;
    Calibration_var.Zero_diff = 1;
    //Calibration_var.Zerocal_real_time_rate = 1;
    Calibration_var.old_span_value = 1;
    Calibration_var.new_span_value = 1;
    Calibration_var.span_diff = 1;
    Calibration_var.real_time_rate = 1;
    Calibration_var.Belt_scale_unit = Calibration_var.Cert_scale_unit = TONS; //Tons
    Calibration_var.Test_zero_weight_unit = Screen25_str8; //pounds

    CalcCRC((unsigned char*) &Calibration_var,
            (unsigned char*) &Calibration_var.CRC_CALIBRATION_STRUCT,
            (unsigned int) offsetof(CALIBRATION_STRUCT,
                                    CRC_CALIBRATION_STRUCT));
}

void init_Setup_device_var(void)
{
    strcpy(Setup_device_var.Addr1, "U.S OPERATIONS");
#ifdef SUPERIOR
    strcpy(Setup_device_var.Addr1, "319 E. STATE HIGHWAY 28");
    strcpy(Setup_device_var.Addr2, "MORRIS, MN 56267");
    strcpy(Setup_device_var.Addr3, "productsupport@superior-ind.com");
#else
    strcpy(Setup_device_var.Addr2, "1 BELTWAY RD");
    strcpy(Setup_device_var.Addr3, "ROCK FALLS,IL 61071");
#endif
    //analog outputs
    Setup_device_var.Analog_Output_1_Function = Screen44411_str5;
    Setup_device_var.Analog_Output_1_Maxrate = 500;
    Setup_device_var.Analog_Output_1_Setpoint = 100;
    Setup_device_var.Analog_Output_1_ZeroCal = 0;
    Setup_device_var.Analog_Output_2_Function = Screen44421_str5;
    Setup_device_var.Analog_Output_2_Maxrate = 500;
    Setup_device_var.Analog_Output_2_Setpoint = 100;
    Setup_device_var.Analog_Output_2_ZeroCal = 0;
    //analog inputs
    Setup_device_var.Analog_Input_Maxrate = 500;
    //company name
#ifdef SUPERIOR
    strcpy(Setup_device_var.Company_name, "SUPERIOR INDUSTRIES");
#else
    strcpy(Setup_device_var.Company_name, "BELT-WAY SCALES, INC");
#endif
    //cutoff weights
    Setup_device_var.Cutoff_1 = 0.000;
    Setup_device_var.Cutoff_2 = 0.000;
    Setup_device_var.Cutoff_3 = 0.000;
    Setup_device_var.Cutoff_4 = 0.000;
    Setup_device_var.Cutoff_5 = 0.000;
    Setup_device_var.Cutoff_6 = 0.000;
    Setup_device_var.Cutoff_7 = 0.000;
    Setup_device_var.Cutoff_8 = 0.000;
    /*Setup_device_var.Cutoff_9 = 0.000;*/
    //digital input function
    Setup_device_var.Digital_Input_1_Function = Port_string3;
    Setup_device_var.Digital_Input_2_Function = Port_string3;
    Setup_device_var.Digital_Input_3_Function = Port_string3;
    Setup_device_var.Digital_Input_4_Function = Port_string3;
    //digital output function
    Setup_device_var.Digital_Output_1_Function = Port_string3;
    Setup_device_var.Digital_Output_2_Function = Port_string3;
    Setup_device_var.Digital_Output_3_Function = Port_string3;
    //empty belt speed
    Setup_device_var.Empty_Belt_Speed = 0;
    //error alarm
    Setup_device_var.error_alarm_1 = LOAD_CELL_ERR_STR;
    Setup_device_var.error_alarm_2 = LOAD_CELL_ERR_STR;
    Setup_device_var.error_alarm_3 = LOAD_CELL_ERR_STR;
    Setup_device_var.error_alarm_4 = LOAD_CELL_ERR_STR;
    Setup_device_var.error_alarm_5 = LOAD_CELL_ERR_STR;
    Setup_device_var.error_alarm_6 = LOAD_CELL_ERR_STR;
    //feeder
    Setup_device_var.Feed_Delay = 0;
    Setup_device_var.Feeder_Max_Rate = 0.0;
    //load weight
    Setup_device_var.Load_weight_1 = 0.000;
    Setup_device_var.Load_weight_2 = 0.000;
    Setup_device_var.Load_weight_3 = 0.000;
    Setup_device_var.Load_weight_4 = 0.000;
    Setup_device_var.Load_weight_5 = 0.000;
    Setup_device_var.Load_weight_6 = 0.000;
    Setup_device_var.Load_weight_7 = 0.000;
    Setup_device_var.Load_weight_8 = 0.000;
    /*Setup_device_var.Load_weight_9 = 115;*/
    //usb report status
    Setup_device_var.Log_cal_data = Screen431_str5;
    Setup_device_var.Log_run_time_data = Screen431_str5;
    Setup_device_var.Log_err_data = Screen431_str5;
    Setup_device_var.Log_zero_cal_data = Screen431_str5;
    Setup_device_var.Log_clear_wt_data = Screen431_str5;
    Setup_device_var.Periodic_log_interval = 10;
    //modbus
    Setup_device_var.Network_Addr = 2;
    //Setup_device_var.Master_nwk_Addr = 1;
    //min belt speeds
    Setup_device_var.Min_Belt_Speed = 0;
    //speed setpoint
    Setup_device_var.MinMax_Speed_Setpoint_1 = 0;
    Setup_device_var.MinMax_Speed_Setpoint_2 = 0;
    Setup_device_var.MinMax_Speed_Setpoint_3 = 0;
    Setup_device_var.MinMax_Speed_Setpoint_4 = 0;
    Setup_device_var.MinMax_Speed_Setpoint_5 = 0;
    Setup_device_var.MinMax_Speed_Setpoint_6 = 0;
    //rate setpoint
    Setup_device_var.MinMax_Rate_Setpoint_1 = 0;
    Setup_device_var.MinMax_Rate_Setpoint_2 = 0;
    Setup_device_var.MinMax_Rate_Setpoint_3 = 0;
    Setup_device_var.MinMax_Rate_Setpoint_4 = 0;
    Setup_device_var.MinMax_Rate_Setpoint_5 = 0;
    Setup_device_var.MinMax_Rate_Setpoint_6 = 0;
    Setup_device_var.Percent_Ingredient = 10.00;
#ifdef SUPERIOR
    strcpy(Setup_device_var.Phone, "320-589-7485");
#else
    strcpy(Setup_device_var.Phone, "8156255573");
#endif

    // strcpy(Setup_device_var.Phone, "8156255573");
    //pid
    Setup_device_var.PID_Action = Screen4512_str1;
    Setup_device_var.PID_Channel = 1;
    Setup_device_var.PID_D_Term = 1;
    Setup_device_var.PID_I_Term = 1;
    Setup_device_var.PID_Local_Setpoint = 100;
    Setup_device_var.PID_P_Term = 1;
    Setup_device_var.PID_Setpoint_Type = Screen4513_str1;
    //preload delay and distance
    Setup_device_var.Preload_Delay = 0.0;
    Setup_device_var.Preload_Distance = 0.0;
    //printer
    Setup_device_var.Printer_ticket_num = 1;
    Setup_device_var.Printer_baud = 9600;
    Setup_device_var.Printer_data_bits = 8;
    Setup_device_var.Printer_stop_bits = 1;
    Setup_device_var.Printer_flow_control = Screen412214_str2;
    Setup_device_var.Printer_status = Screen411_str1;
    Setup_device_var.Printer_type = Screen4121_str1;
    //pulse on time
    Setup_device_var.Pulse_on_time_1 = 0;
    Setup_device_var.Pulse_on_time_2 = 0;
    Setup_device_var.Pulse_on_time_3 = 0;
    //rate output type
    Setup_device_var.Rate_Output_Type_1 = MIN_RATE_OUTPUT_STR;
    Setup_device_var.Rate_Output_Type_2 = MIN_RATE_OUTPUT_STR;
    Setup_device_var.Rate_Output_Type_3 = MIN_RATE_OUTPUT_STR;
    Setup_device_var.Rate_Output_Type_4 = MIN_RATE_OUTPUT_STR;
    Setup_device_var.Rate_Output_Type_5 = MIN_RATE_OUTPUT_STR;
    Setup_device_var.Rate_Output_Type_6 = MIN_RATE_OUTPUT_STR;
    //relay output function
    Setup_device_var.Relay_Output_1_Function = Port_string3;
    Setup_device_var.Relay_Output_2_Function = Port_string3;
    Setup_device_var.Relay_Output_3_Function = Port_string3;
    //scoreboard data
    Setup_device_var.Scoreboard_alt_delay = 0;
    Setup_device_var.Scoreboard_baud = 9600;
    Setup_device_var.Scoreboard_data_bits = 8;
    Setup_device_var.Scoreboard_display_data = Screen421_str5;
    Setup_device_var.Scoreboard_type = Screen4221_str1;
    Setup_device_var.Scoreboard_stop_bits = 1;
    Setup_device_var.Scoreboard_TMMode = Screen431_str5;
    //speed outpt type
    Setup_device_var.Speed_Output_Type_1 = MIN_SPEED_OUTPUT_STR;
    Setup_device_var.Speed_Output_Type_2 = MIN_SPEED_OUTPUT_STR;
    Setup_device_var.Speed_Output_Type_3 = MIN_SPEED_OUTPUT_STR;
    Setup_device_var.Speed_Output_Type_4 = MIN_SPEED_OUTPUT_STR;
    Setup_device_var.Speed_Output_Type_5 = MIN_SPEED_OUTPUT_STR;
    Setup_device_var.Speed_Output_Type_6 = MIN_SPEED_OUTPUT_STR;
    //target load and rate
    Setup_device_var.Target_rate = 0;
    Setup_device_var.Target_Load = 0;
    //unit per pulse
    Setup_device_var.Units_Per_Pulse_1 = FIRST_WT_UNIT_PER_PULSE_STR;
    Setup_device_var.Units_Per_Pulse_2 = FIRST_WT_UNIT_PER_PULSE_STR;
    Setup_device_var.Units_Per_Pulse_3 = FIRST_WT_UNIT_PER_PULSE_STR;

    CalcCRC((unsigned char*) &Setup_device_var,
            (unsigned char*) &Setup_device_var.CRC_SETUP_DEVICES,
            (unsigned int) offsetof(SETUP_DEVICES_STRUCT, CRC_SETUP_DEVICES));
}

void init_Admin_var(void)
{
    Admin_var.Auto_zero_tolerance = 0.50;
    Admin_var.Admin_Locked = Screen631_str4;
    Admin_var.AM_PM = AM_TIME_FORMAT;
    strcpy(Admin_var.Backup_file_name, "123.txt");
    Admin_var.Calibration_Locked = Screen631_str4;
    Admin_var.Clear_Weight_Locked = Screen631_str4;
    Admin_var.Current_Date_Format = MMDDYYYY;
    Admin_var.Current_Time_Format = TWENTYFOUR_HR;
    Admin_var.Config_Date.Year = 2013;
    Admin_var.Config_Date.Mon = 01;
    Admin_var.Config_Date.Day = 01;
    Admin_var.Day_of_week = SUNDAY; //sunday
    Admin_var.Config_Time.Hr = 00;
    Admin_var.Config_Time.Min = 00;
    Admin_var.DHCP_Status = Screen661_str5;
    Admin_var.Gateway.Addr1 = 192;
    Admin_var.Gateway.Addr2 = 168;
    Admin_var.Gateway.Addr3 = 1;
    Admin_var.Gateway.Addr4 = 1;
    strcpy(&Admin_var.Int_firmware_version[0], FirmwareVer);
    strcpy(&Admin_var.Int_update_version[0], FirmwareVer);
    Admin_var.IP_Addr.Addr1 = 192;
    Admin_var.IP_Addr.Addr2 = 168;
    Admin_var.IP_Addr.Addr3 = 1;
    Admin_var.IP_Addr.Addr4 = 100;
    Admin_var.Negative_rate_limit = 0.0;
    Admin_var.Negative_rate_time = 10;
    strcpy(Admin_var.Password, "PASSWORD");
#ifdef SUPERIOR
    strcpy(Admin_var.Plant_Name, "SUPERIOR USA");
    strcpy(Admin_var.Product_Name, "ROCKS");
    strcpy(Admin_var.Scale_Name, "SUPERIOR");
#else
    strcpy(Admin_var.Plant_Name, "BELTWAY USA");
    strcpy(Admin_var.Product_Name, "ROCKS");
    strcpy(Admin_var.Scale_Name, "BELTWAY");
#endif
    strcpy(Admin_var.Reload_file_name, "123.txt");
    Admin_var.Setup_Wizard_Locked = Screen631_str4;
    Admin_var.Setup_Devices_Locked = Screen631_str4;
    //Admin_var.Sensor_Update_Version = 0.04;
    //Admin_var.Sensor_Firmware_Version = 0.05;
    Admin_var.Subnet_Mask.Addr1 = 255;
    Admin_var.Subnet_Mask.Addr2 = 255;
    Admin_var.Subnet_Mask.Addr3 = 255;
    Admin_var.Subnet_Mask.Addr4 = 0;
    Admin_var.Zero_Calibration_Locked = Screen631_str4;
    Admin_var.Zero_rate_limit = -5;
    Admin_var.Zero_rate_status = Screen6171_str5;
    Admin_var.MBS_TCP_slid = 0;
    strcpy(Admin_var.New_Password, "NEW PASSWORD");
    strcpy(Admin_var.Verify_New_Password, "VERIFY PASSWORD");
    memset(Admin_var.Int_update_file_name, 0,
           sizeof(Admin_var.Int_update_file_name));
    //---------added on 11th september 2014
    Admin_var.Test_load = 1.0;
    Admin_var.Test_speed = 1.0;
    Admin_var.Test_load_status = Screen431_str5;
    Admin_var.Test_speed_status = Screen431_str5;
//		Admin_var.Language_select = Screen61B1_str0;
    strcpy(&Admin_var.Int_firmware_version[0], FirmwareVer);
    strcpy(&Admin_var.Int_update_version[0], FirmwareVer);

    CalcCRC((unsigned char*) &Admin_var, (unsigned char*) &Admin_var.CRC_ADMIN,
            (unsigned int) offsetof(ADMIN_STRUCT, CRC_ADMIN));
}

void init_EEP_conf_vars(void)
{
    init_Scale_setup_var();
    init_Calibration_var();
    init_Setup_device_var();
    init_Admin_var();
}

void screen_variables_init(void)
{
    U8 i = 0;

    init_EEP_conf_vars();
    Rprts_diag_var.Load_Cell_1_Output = 0;
    Rprts_diag_var.Load_Cell_2_Output = 0;
    Rprts_diag_var.Load_Cell_3_Output = 0;
    Rprts_diag_var.Load_Cell_4_Output = 0;
    Rprts_diag_var.Load_Cell_5_Output = 0;
    Rprts_diag_var.Load_Cell_6_Output = 0;
    Rprts_diag_var.Load_Cell_7_Output = 0;
    Rprts_diag_var.Load_Cell_8_Output = 0;
    Rprts_diag_var.Speed_Sensor = 0;
    Rprts_diag_var.Angle_Sensor = 0;
    Rprts_diag_var.Digital_Input_1_Status = Port_string2; //off
    Rprts_diag_var.Digital_Input_2_Status = Port_string2; //off
    Rprts_diag_var.Digital_Input_3_Status = Port_string2; //off
    Rprts_diag_var.Digital_Input_4_Status = Port_string2; //off
    Rprts_diag_var.Digital_Output_1_Status = Port_string2; //off
    Rprts_diag_var.Digital_Output_2_Status = Port_string2; //off
    Rprts_diag_var.Digital_Output_3_Status = Port_string2; //off
    Rprts_diag_var.Relay_Output_1_Status = Port_string2; //off
    Rprts_diag_var.Relay_Output_2_Status = Port_string2; //off
    Rprts_diag_var.Relay_Output_3_Status = Port_string2; //off
    Rprts_diag_var.Analog_Output_1_Status = Port_string2; //off
    Rprts_diag_var.Analog_Output_2_Status = Port_string2; //off
    Rprts_diag_var.PID_Set_Rate = Port_string2; //off
    Rprts_diag_var.RS485_1_Status = Port_string1; //on
    Rprts_diag_var.RS485_2_Status = Port_string1; //on
    Rprts_diag_var.RS232_1_Status = Port_string1; //on
    Rprts_diag_var.RS232_2_Status = Port_string1; //on
    Rprts_diag_var.Ethernet_Status = Port_string1; //on
    Rprts_diag_var.SPI_Status = Port_string1; //on
    Rprts_diag_var.Five_volt_supply = 0.0;
    Rprts_diag_var.Load_cell_supply = 0.0;
    Rprts_diag_var.Speed_In_Hz = Speed_unit;

    Units_var.Unit_speed_time_str = min_str;
    Units_var.Unit_pulse_on_time_str = ms_str;
    Units_var.Unit_delay_secs = sec_str;
    Rprts_diag_var.daily_Total_weight = 0.0;
    Rprts_diag_var.weekly_Total_weight = 0.0;
    Rprts_diag_var.monthly_Total_weight = 0.0;
    Rprts_diag_var.Job_Total = 0.0;
    Rprts_diag_var.Master_total = 0.0;
    Rprts_diag_var.yearly_Total_weight = 0.0;
    Idler_center_dist_calc();

    //---------added on 11th september 2014
    Rprts_diag_var.Live_weight_disp = 1.0;
    Rprts_diag_var.Trim_live_weight_disp = 1.0;
    Rprts_diag_var.Calib_err_pct = 1.0;
    //---added on 2nd Jan 2015
    Calculation_struct.run_time = 0;

    for (i = 0; i < 8; i++)
    {
        Rprts_diag_var.LastClearedTotal[i] = 0;
        Rprts_diag_var.Avg_rate_for_mode[i] = 0;
        Rprts_diag_var.RunTime[i] = 0;
        Rprts_diag_var.Clear_Date[i].Day = 0;
        Rprts_diag_var.Clear_Date[i].Mon = 0;
        Rprts_diag_var.Clear_Date[i].Year = 0;
        Rprts_diag_var.Clear_Time[i].Hr = 0;
        Rprts_diag_var.Clear_Time[i].Min = 0;
        Rprts_diag_var.Clear_Time[i].Sec = 0;
        Rprts_diag_var.Clear_Time[i].AM_PM = 0;
        Rprts_diag_var.LastCleared_Weight_unit[i] = Scale_setup_var.Weight_unit;
        Rprts_diag_var.LastCleared_Rate_time_unit[i] =
            Scale_setup_var.Rate_time_unit;
    }
    return;
}

/*****************************************************************************
 * @note       Function name  : __task void update_lcd (void)
 * @returns    returns        : None
 * @param      arg1           : None
 * @author                    : Anagha Basole
 * @date       date created   : 26th December 2012
 * @brief      Description    : Task to update the screen based on user input and
 *                            : if there is any update in the screen data.
 * @note       Notes          : None
 *****************************************************************************/
extern int task_number;
extern int badethernet_flag;
__task void update_lcd(void)
{
    Screen_org_data * current_screen_org_no_main;
    int i = 0, screen_to_be_redrawn = 0, sel = 0, local_var = 0;
    //static short int time_seconds = -1;
    U8 flash_write_status;
    SCREEN_TYPE9_DATA *data9;
    SCREEN_TYPE10_DATA *data10;
    char rcvBuffer[ARRAY_SIZE];
    //int OS_Sleep = 0;
    parent_wizard.w_info = NULL;
    parent_wizard.w_index = 0;
    used = 0;
    GUI_Init();

    MATRIX_ARB = 0 // Set AHB Matrix priorities [0..3] with 3 being highest priority
                 | (1 << 0) // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
                 | (2 << 2)  // PRI_DCODE : D-Code bus priority.
                 | (0 << 4)  // PRI_SYS   : System bus priority.
                 | (0 << 6)  // PRI_GPDMA : General Purpose DMA controller priority.
                 | (0 << 8)  // PRI_ETH   : Ethernet: DMA priority.
                 | (3 << 10)  // PRI_LCD   : LCD DMA priority.
                 | (0 << 12)  // PRI_USB   : USB DMA priority.
                 | (1 << 16)  // ROM_LAT   : ROM latency select.
                 ;

    GUI_UC_SetEncodeUTF8(); /* required only once to activate UTF-8*/
    GUI_Clear();

    GUI_SetBkColor(GUI_BLACK);
    GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);

    WM_SetCreateFlags(WM_CF_MEMDEV_ON_REDRAW); /* Use memory devices on all windows to avoid flicker */
    WM_MULTIBUF_Enable(1);

    GUI_data_nav.Wizard_screen_index = 0;
    GUI_data_nav.Up_right_key_pressed = __FALSE;
    GUI_data_nav.Back_one_level = __FALSE;
    GUI_data_nav.Cancel_operation = __FALSE;
    GUI_data_nav.Child_present = __FALSE;
    GUI_data_nav.In_wizard = __FALSE;
    GUI_data_nav.Change = __FALSE;
    GUI_data_nav.Setup_wizard = __FALSE;
    GUI_data_nav.Key_press = __FALSE;
    memset(GUI_data_nav.Unit_per_pulse_str, 0,
           sizeof(GUI_data_nav.Unit_per_pulse_str));
    for (i = 0; i < sizeof(GUI_data_nav.Error_wrn_msg_flag); i++)
    {
        GUI_data_nav.Error_wrn_msg_flag[i] = 0;
    }
    Units_var.Unit_speed_time_str = min_str;
    Units_var.Unit_pulse_on_time_str = ms_str;
    Units_var.Unit_rate = getRate_unit();
    Units_var.Unit_speed = getSpeed_unit();
    Units_var.Unit_delay_secs = sec_str;
    current_screen_org_no_main = screen_data_create(RUN_MODE_ORG_NO);
    screen_display(current_screen_org_no_main);
    NVIC_EnableIRQ(GPIO_IRQn); // Key pad Interrupt enabling
		//if ((LPC_RTC->ALDOY &0x01)==0x01)
		if (badethernet_flag)
		GUI_DrawBitmap(&bmsmallredethernetactivityicon, WINDOW_FOOTER_ICON1_POS_X0, WINDOW_FOOTER_ICON1_POS_Y0);
		
		
    while (1)
    {

        os_itv_set(LCD_TASK_INTERVAL);
        os_itv_wait();
				LPC_RTC->ALMIN &= ~(0x07<<3);
        LPC_RTC->ALMIN |= (0x01<<3);						//SECTION1
				if (badethernet_flag)
		GUI_DrawBitmap(&bmsmallredethernetactivityicon, WINDOW_FOOTER_ICON1_POS_X0, WINDOW_FOOTER_ICON1_POS_Y0);
		
        switch (Misc_var.Language_select)
        {
        case Screen61B1_str0:
            Strings = Strings_english;
            break;
        case Screen61B2_str0:			//SKS 02-11-2016 BS-241
            Strings = Strings_polish;
            break;

        }

#ifdef SCREENSHOT
        if (Take_Screen_Shot_Flag == __TRUE)
        {
            MainTask();
            Take_Screen_Shot_Flag = __FALSE;
        }
#endif
        Units_var.Unit_rate = getRate_unit();
        Units_var.Unit_speed = getSpeed_unit();
        LPC_RTC->ALMIN &= ~(0x07<<3);
        LPC_RTC->ALMIN |= (0x02<<3);				//SECTION 2
        if ((Keypad_Timeout_Indication == LCD_TURNED_ON_INIT)
                || (Keypad_Timeout_Indication == LCD_ON_TIMEOUT_START))
        {
            //	if(usb_connect_done_fg == 0)		//by megha on 9/11/2016	for restoring flash data to usb screen display
            //	{
            GUI_Exec();
            //	}
            if ((GUI_data_nav.In_wizard == 1)
                    && (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                               MAIN_MENU_ORG_NO) == 0))
            {
                GUI_data_nav.In_wizard = __FALSE;
                GUI_data_nav.Wizard_screen_index = __FALSE;
                GUI_data_nav.Cancel_operation = __FALSE;
                GUI_data_nav.Setup_wizard = __FALSE;
                GUI_data_nav.wizard_screen = NULL;
            }
            if (GUI_data_nav.Change == __TRUE)
            {
                current_screen_org_no_main = screen_data_create(
                                                 GUI_data_nav.New_screen_num);
                screen_to_be_redrawn = __TRUE;
                GUI_data_nav.Change = __FALSE;
                GUI_data_nav.Key_press = __FALSE;
                //the new screen organization number needs to be checked for the static calibration
                //screen since a hybrid screen is not present
                if (strcmp(current_screen_org_no_main->Screen_org_no,
                           STATIC_ZEROCAL_START) == 0)
                {
                    GUI_data_nav.Child_present = __FALSE;
                    GUI_data_nav.Wizard_screen_index = 1;
                    GUI_data_nav.wizard_screen =
                        current_screen_org_no_main->screens;
                }
            }
            if (GUI_data_nav.Current_screen_info->Screen_type != 1
                    && (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                               BACKUP_RESTORE_USB_ORG_NO) != 0))
            {
                if (((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                             TEST_WEIGHT_ENTRY_ORG_NO) == 0)
                        || (strcmp(
                                GUI_data_nav.Current_screen_info->Screen_org_no,
                                MAT_TEST_WIZARD_ORG_NO) == 0)
                        || (strcmp(
                                GUI_data_nav.Current_screen_info->Screen_org_no,
                                DIGI_CAL_WIZARD_ORG_NO) == 0)
                        || (strcmp(
                                GUI_data_nav.Current_screen_info->Screen_org_no,
                                LEN_CAL_WIZARD_ORG_NO) == 0)
                        || ((strcmp(
                                 GUI_data_nav.Current_screen_info->Screen_org_no,
                                 ZERO_CAL_WIZARD_ORG_NO) == 0)
                            && (strcmp(GUI_data_nav.New_screen_num,
                                       STATIC_ZEROCAL_START) == 0))
                        || (strcmp(
                                GUI_data_nav.Current_screen_info->Screen_org_no,
                                DYNAMIC_ZEROCAL_ACK_ORG_NO) == 0)))
                    //  FOR SETUP WIZARD flag will be set in scree3.c call back function.
                {
                    GUI_data_nav.In_wizard = __TRUE;
                }
                else if ((strcmp(
                              GUI_data_nav.Current_screen_info->Screen_org_no,
                              INT_UPDT_WIZARD_ORG_NO) == 0)
                         ||
                         // FOR PASSWORD FLAG Will be set in screen2.c call back function.
                         (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                                 BACKUP_USB_WIZARD_ORG_NO) == 0)
                         || (strcmp(
                                 GUI_data_nav.Current_screen_info->Screen_org_no,
                                 RESTORE_USB_WIZARD_ORG_NO) == 0))
                {
                    GUI_data_nav.In_wizard = __TRUE;
                }
                if (GUI_data_nav.Current_screen_info->screens != NULL)
                {

                    if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                                PASSWORD_SCREEN_ORG_NO) != 0)
                            || (wiz_nav_stack.top == 1))
                    {
                        GUI_data_nav.wizard_screen =
                            GUI_data_nav.Current_screen_info->screens;
                        GUI_data_nav.Child_present = __FALSE;
                        if ((GUI_data_nav.Current_screen_info->Screen_org_no[0]
                                == '3')
                                || (GUI_data_nav.Current_screen_info->Screen_org_no[0]
                                    == '6'))
                        {
                            GUI_data_nav.Wizard_screen_index = 1;
                        }
                        else
                        {
                            GUI_data_nav.Setup_wizard = __TRUE;
                            GUI_data_nav.Wizard_screen_index = 1;
                        }
                    }
                }
            }
            else
            {
                GUI_data_nav.In_wizard = __FALSE;
                memset(wiz_nav_stack.contents, 0,
                       sizeof(wiz_nav_stack.contents));
                wiz_nav_stack.top = 0;
            }
            // if zero calibration is initiated using Zero Key.
            if ((Zero_key_press_flag == 1)
                    && (GUI_data_nav.In_wizard == __FALSE))
            {
                GUI_data_nav.In_wizard = __TRUE;
                GUI_data_nav.wizard_screen = &Org_351000_screen;
                GUI_data_nav.Child_present = __FALSE;
                GUI_data_nav.Wizard_screen_index = 0;
            }
            if (GUI_data_nav.Key_press == __TRUE) //for wizard screens
            {
                if (GUI_data_nav.Child_present != __TRUE)
                {
                    //if the setup wizard end screen is being displayed then on enter go to run mode screen
                    //clear the setup wizard flags
                    if (strcmp(SETUP_WIZARD_COMPLETE_ORG_NO,
                               GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0)
                    {
                        GUI_data_nav.In_wizard = __FALSE;
                        GUI_data_nav.Setup_wizard = __FALSE;
                        current_screen_org_no_main = screen_data_create(
                                                         RUN_MODE_ORG_NO);
                        screen_to_be_redrawn = __TRUE;
                    }
                    if ((GUI_data_nav.In_wizard == __TRUE)
                            && (GUI_data_nav.Cancel_operation == __FALSE))
                    {
                        if ((unsigned int) GUI_data_nav.Wizard_screen_index
                                < GUI_data_nav.wizard_screen->wizard_size)
                        {
                            //for span calibration screen in setup wizard, get the next screen org number depending on the
                            //user selection depending on the org number in the wizard structure
                            if (strcmp(SETUP_WIZARD_SPAN_CAL_CHILD_ORG_NO,
                                       GUI_data_nav.Current_screen_info->Screen_org_no)
                                    == 0)
                            {
                                sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
                                current_screen_org_no_main =
                                    screen_data_create(
                                        GUI_data_nav.wizard_screen->Next[sel
                                                + 1].number);
                            }
                            else
                            {
                                //if user has pressed the button to go to the previous screen then depending on the value
                                // go to the previous screen in the wizard. Also modify the wizard flag.
                                if (GUI_data_nav.Up_right_key_pressed >= 1)
                                {
                                    local_var =
                                        GUI_data_nav.Up_right_key_pressed
                                        + 1;
                                    if ((GUI_data_nav.Wizard_screen_index
                                            - local_var) >= 0)
                                    {
                                        GUI_data_nav.Wizard_screen_index =
                                            GUI_data_nav.Wizard_screen_index
                                            - local_var;		//2;
                                        current_screen_org_no_main =
                                            screen_data_create(
                                                GUI_data_nav.wizard_screen->Next[GUI_data_nav.Wizard_screen_index].number);
                                        GUI_data_nav.Wizard_screen_index++;
                                    }
                                    GUI_data_nav.Up_right_key_pressed = __FALSE;
                                }
                                else
                                {

                                    current_screen_org_no_main =
                                        screen_data_create(
                                            GUI_data_nav.wizard_screen->Next[GUI_data_nav.Wizard_screen_index].number);
                                    GUI_data_nav.Wizard_screen_index++;
                                }
                            }
                            screen_to_be_redrawn = __TRUE;
                        }
                        //if this is the last screen in the wizard then figure out which screen needs to be displayed
                        //depending on wether the setup wizard is in progress or it is a normal wizard
                        else if ((unsigned int) GUI_data_nav.Wizard_screen_index
                                 == GUI_data_nav.wizard_screen->wizard_size)
                        {
                            //if the user is in setup wizard then figure out the next screen organisation number
                            //depending on the structure entry
                            if ((GUI_data_nav.Setup_wizard == __TRUE)
                                    && (password_struct.password_to_be_entered
                                        != __TRUE))
                            {
                                //if length and zero calibration has been performed then directly go to span calibration
                                //else go to zero calibration
                                if (strcmp(LOAD_CELL_SIZE_COMPLETE_SCREEN,
                                           GUI_data_nav.Current_screen_info->Screen_org_no)
                                        == 0)
                                {
                                    current_screen_org_no_main =
                                        screen_data_create(
                                            GUI_data_nav.wizard_screen->setup_wizard_org_no);
                                    GUI_data_nav.wizard_screen =
                                        parent_wizard.w_info;
                                    GUI_data_nav.Wizard_screen_index =
                                        ++parent_wizard.w_index;
                                    parent_wizard.w_info = NULL;
                                    parent_wizard.w_index = 0;
                                }
                                else if (strcmp(LEN_ZER_CAL_COMPLETE_ORG_NO,
                                                GUI_data_nav.Current_screen_info->Screen_org_no)
                                         == 0)
                                {
                                    current_screen_org_no_main =
                                        screen_data_create(
                                            SETUP_WIZARD_SPAN_CAL_ORG_NO);
                                    GUI_data_nav.wizard_screen =
                                        current_screen_org_no_main->screens;
                                    GUI_data_nav.Wizard_screen_index = 0;
                                }
                                //go to the next screen in the setup wizard
                                else
                                {
                                    current_screen_org_no_main =
                                        screen_data_create(
                                            GUI_data_nav.wizard_screen->setup_wizard_org_no);
                                    GUI_data_nav.wizard_screen =
                                        current_screen_org_no_main->screens;
                                    GUI_data_nav.Wizard_screen_index = 0;
                                }
                            }
                            //if user is in the normal wizard then create the end screen and reset the flags
                            else   // If we Initiate Zero calibration through ZERO Key then End is Runmode screen
                            {
                                if ((strncmp(
                                            GUI_data_nav.wizard_screen->Next[0].number,
                                            DYNAMIC_ZEROCAL_START, 6) == 0)
                                        && Zero_key_press_flag == 1)
                                {
                                    current_screen_org_no_main =
                                        screen_data_create(RUN_MODE_ORG_NO);
                                    Zero_key_press_flag = 0;
                                }
                                else
                                {
                                    current_screen_org_no_main =
                                        screen_data_create(
                                            GUI_data_nav.wizard_screen->End);
                                }
                                GUI_data_nav.In_wizard = __FALSE;
                                GUI_data_nav.Wizard_screen_index = 0;
                                GUI_data_nav.wizard_screen = NULL;
                            }
                            screen_to_be_redrawn = __TRUE;
                        }
                        else
                        {
                            GUI_data_nav.In_wizard = __FALSE;
                            GUI_data_nav.Wizard_screen_index = __FALSE;
                        }
                    }  //in_wizard && cancel
                    else if ((GUI_data_nav.In_wizard == __TRUE)
                             && (GUI_data_nav.Cancel_operation == __TRUE))
                    {
                        Init_STACK(wiz_nav_stack);
                        GUI_data_nav.Back_one_level = 0;
                        GUI_data_nav.In_wizard = __FALSE;
                        GUI_data_nav.Wizard_screen_index = __FALSE;
                        GUI_data_nav.Cancel_operation = __FALSE;
                        GUI_data_nav.Setup_wizard = __FALSE;
                        // If we Initiate Zero calibration through ZERO Key then pressing cancel should bring to Runmode screen
                        if ((strncmp(GUI_data_nav.wizard_screen->Next[0].number,
                                     DYNAMIC_ZEROCAL_START, 6) == 0)
                                && Zero_key_press_flag == 1)
                        {
                            current_screen_org_no_main = screen_data_create(
                                                             RUN_MODE_ORG_NO);
                            Zero_key_press_flag = 0;
                        }
                        else
                        {
                            current_screen_org_no_main = screen_data_create(
                                                             GUI_data_nav.wizard_screen->Cancel);
                        }
                        GUI_data_nav.wizard_screen = NULL;
                        screen_to_be_redrawn = __TRUE;
                        Calib_struct.Start_flag = __FALSE;
                        Calib_struct.Cal_status_flag = CLEAR_CAL_FLAG;
                    }
                } //child_present
                else
                {
                    GUI_data_nav.Child_present = __FALSE;
                }
                GUI_data_nav.Key_press = 0;
            } //if (Key_press == __TRUE)

            if (Calib_struct.Set_progress_bar == PROGRESS_BAR_MOVE)
            {
                if (GUI_data_nav.Current_screen_info->Screen_type == 0xB)
                {
                    PROGBAR_SetValue(GUI_data_nav.Prog_bar,
                                     GUI_data_nav.Percent_complete);
                    GUI_Exec();
                }
                if (GUI_data_nav.Percent_complete == 100)
                {
                    if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                                DIGITAL_START_CAL) == 0)
                            || (strcmp(
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    TEST_WEIGHT_START_CAL) == 0))
                    {
                        // For Delay in between one second to two seconds.
                        os_dly_wait(90);
                        GUI_data_nav.Key_press = 1;
                        GUI_data_nav.Percent_complete = 0;
                        GUI_data_nav.Cancel_operation = 0;
                        Calib_struct.Set_progress_bar = PROGRESS_BAR_RESET;
                    }
                    else
                    {
                        Calib_struct.Set_progress_bar = PROGRESS_BAR_RESET;
                    }
                    //display the text "press enter to continue" as an indication for the user to move ahead
                    //So cahng the prev. text
                    if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                                LEN_ZER_CAL_ACPT_LEN_ORG_NO) == 0)
                            || (strcmp(
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    AUTO_BELT_LEN_CAL_ACPT_ORG_NO) == 0))
                    {
                        // Do nothing when these screens are displayed, it is important to have this condition.
                    }
                    else if ((strcmp(
                                  GUI_data_nav.Current_screen_info->Screen_org_no,
                                  BACKUP_USB_FILE_STORE_ORG_NO) == 0))
                    {
                        MULTIEDIT_SetText(GUI_data_nav.Multi_text,
                                          Strings[Str_NULL].Text);
                        MULTIEDIT_AddText(GUI_data_nav.Multi_text,
                                          Strings[Backup_completed_Str].Text);
                    }
                    else if ((strcmp(
                                  GUI_data_nav.Current_screen_info->Screen_org_no,
                                  RESTORE_FILE_FROM_USB_ORG_NO) == 0))
                    {
                        MULTIEDIT_SetText(GUI_data_nav.Multi_text,
                                          Strings[Str_NULL].Text);
                        MULTIEDIT_AddText(GUI_data_nav.Multi_text,
                                          Strings[Restore_completed_Str].Text);
                    }
                    //PVK - 18Jan 2016
                    else if ((strcmp(
                                  GUI_data_nav.Current_screen_info->Screen_org_no,
                                  STATIC_ZEROCAL_START) == 0)
                             || (strcmp(
                                     GUI_data_nav.Current_screen_info->Screen_org_no,
                                     DYNAMIC_ZEROCAL_START) == 0))
                    {
                        MULTIEDIT_SetText(GUI_data_nav.Multi_text,
                                          Strings[Str_NULL].Text);
                        MULTIEDIT_AddText(GUI_data_nav.Multi_text,
                                          Strings[Calibration_completed_Str].Text);
                        MULTIEDIT_AddText(GUI_data_nav.Multi_text,
                                          Strings[Calibration_complete_string].Text);
                    }
                    else
                    {
                    }
                }
                else
                    Calib_struct.Set_progress_bar = PROGRESS_BAR_RESET;
            }
            LPC_RTC->ALMIN &= ~(0x07<<3);
            LPC_RTC->ALMIN |= (0x03<<3);				//SECTION 3
            //Clear weight password screen logic
            if (GUI_data_nav.Clr_wt_pw_to_be_entered == CLR_WT_PW_SCREEN)
            {
                //create the password screen, store the current screen org number to come back to it
                //in case the password entered is correct
                GUI_data_nav.Screen_before_pw_org_num =
                    GUI_data_nav.Current_screen_info;
                password_struct.password_to_be_entered = __TRUE;
                GUI_data_nav.Clr_wt_pw_to_be_entered = CLR_WT_CHK_PW_SCREEN;
            }

            //Password screen creation logic
            if (password_struct.password_to_be_entered == __TRUE)
            {
                current_screen_org_no_main = screen_data_create(
                                                 PASSWORD_SCREEN_ORG_NO);
                screen_to_be_redrawn = __TRUE;
                password_struct.password_to_be_entered = __FALSE;
            }

            //Screen redraw
            if (screen_to_be_redrawn == __TRUE)
            {
                if (GUI_data_nav.Back_one_level != GO_BACK_LOOP_FLAG)
                {
                    if ((GUI_data_nav.In_wizard == __TRUE)
                            && (GUI_data_nav.Back_one_level != GO_BACK_FLAG)
                            && (strncmp(
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    "351", 3) != 0))
                    {
                        Push_STACK(&wiz_nav_stack,
                                   atoi(
                                       GUI_data_nav.Current_screen_info->Screen_org_no));
                    }
                    else
                    {
                        GUI_data_nav.Back_one_level = _FALSE;
                    }
                }
                screen_display(current_screen_org_no_main);
                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                            DYNAMIC_ZEROCAL_START) == 0))
                {
                    GUI_StoreKeyMsg(GUI_KEY_ENTER, 1);
                }
                screen_to_be_redrawn = __FALSE;
            }
            if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
                        STATIC_ZEROCAL_START) == 0)
                    && (Calib_struct.Cal_status_flag == START_STATICZERO_CAL))
            {
                MULTIEDIT_SetText(GUI_data_nav.Multi_text,
                                  Strings[Str_NULL].Text);
                if (Sens_brd_param.No_of_pulses == 0)
                {
                    MULTIEDIT_AddText(GUI_data_nav.Multi_text,
                                      Strings[Screen3512_str3].Text);
                }
                else
                {
                    MULTIEDIT_AddText(GUI_data_nav.Multi_text,
                                      Strings[Static_Calibration_In_Process_Str].Text);
                }
            }
            LPC_RTC->ALMIN &= ~(0x07<<3);
            LPC_RTC->ALMIN |= (0x04<<3);				//SECTION 4
            //calibration is completed, write the report and logs
            if ((Calib_struct.Calib_log_flag == CALIB_DATA_LOG_WRITE)
                    || (Calib_struct.Calib_log_flag == SET_ZERO_DATA_LOG_WRITE)
                    || (Calib_struct.Calib_log_flag == LEN_ZER_DATA_LOG_WRITE)
                    || (Calib_struct.Calib_log_flag == LENGTH_DATA_LOG_WRITE))
            {
                if (Calib_struct.Calib_log_flag == SET_ZERO_DATA_LOG_WRITE)
                {
                    //if logging is enabled
                    if (Setup_device_var.Log_zero_cal_data == Screen431_str2)
                    {
                        /*populate the set zero cal data log and update on the USB drive or flash*/
                        flash_write_status = log_data_populate(set_zero_log);
                    }
                    else
                    {
                        flash_write_status = __TRUE;
                    }

                    report_data_populate(set_zero_test_report);
                    Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    if (flash_write_status == __TRUE)
                    {
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    }
#ifdef MODBUS_TCP
                    plant_connect_struct_write(0, PLANT_CONNECT_NO_CAL_FLAG,
                                               PLANT_CONNECT_NO_ERROR_FLAG, PLANT_CONNECT_NO_ERROR_FLAG,
                                               PLANT_CONNECT_NO_ERROR_FLAG, PLANT_CONNECT_ZERO_RCRD_FLAG);
                    //set the flag for the new record available
                    Flags_struct.Plant_connect_record_flag |=
                        NEW_PLANT_CONNECT_RECORD_FLAG;
#endif
                    /*
                     if(Modbus_Data_Write_Cmd_recd == 1)
                     {
                     Modbus_Data_Write_Cmd_recd = 0;
                     GUI_data_nav.GUI_structure_backup = 1;
                     }
                     */

                }
                else if (Calib_struct.Calib_log_flag == LEN_ZER_DATA_LOG_WRITE)
                {
                    //if logging is enabled
                    LPC_RTC->ALMIN &= ~(0x07<<3);
                    LPC_RTC->ALMIN |= (0x05<<3);				//SECTION 5
                    if ((Setup_device_var.Log_cal_data == Screen431_str2)
                            || (Setup_device_var.Log_zero_cal_data
                                == Screen431_str2))
                    {
                        if (Setup_device_var.Log_cal_data == Screen431_str2)
                        {
                            /*populate the length cal data log and update on the USB drive or flash*/
                            flash_write_status = log_data_populate(length_log);//SKS
                        }
                        if (Setup_device_var.Log_zero_cal_data
                                == Screen431_str2)
                        {
                            /*populate the set zero cal data log and update on the USB drive or flash*/
                            flash_write_status = log_data_populate(
                                                     set_zero_log);
                        }
                    }
                    else
                    {
                        flash_write_status = __TRUE;
                    }
                    report_data_populate(length_cal_report);
                    Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    if (flash_write_status == __TRUE)
                    {
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    }

                    report_data_populate(set_zero_test_report);
                    Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    if (flash_write_status == __TRUE)
                    {
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    }

                    report_data_populate(set_zero_test_report);
                    Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    if (flash_write_status == __TRUE)
                    {
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    }

#ifdef MODBUS_TCP
                    LPC_RTC->ALMIN &= ~(0x07<<3);
                    LPC_RTC->ALMIN |= (0x06<<3);				//SECTION 6
                    plant_connect_struct_write(0, PLANT_CONNECT_NO_CAL_FLAG,
                                               PLANT_CONNECT_NO_ERROR_FLAG, PLANT_CONNECT_NO_ERROR_FLAG,
                                               PLANT_CONNECT_NO_ERROR_FLAG, PLANT_CONNECT_ZERO_RCRD_FLAG);
                    //set the flag for the new record available
                    Flags_struct.Plant_connect_record_flag |=
                        NEW_PLANT_CONNECT_RECORD_FLAG;
#endif
                }
                else if (Calib_struct.Calib_log_flag == LENGTH_DATA_LOG_WRITE)
                {
                    //if logging is enabled
                    if (Setup_device_var.Log_cal_data == Screen431_str2)
                    {
                        /*populate the length cal data log and update on the USB drive or flash*/
                        flash_write_status = log_data_populate(length_log);		//SKS
                    }
                    else
                    {
                        flash_write_status = __TRUE;
                    }
                    report_data_populate(length_cal_report);								//SKS
                    Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    if (flash_write_status == __TRUE)
                    {
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                    }
                }
                else   //if(Calib_struct.Calib_log_flag == CALIB_DATA_LOG_WRITE)
                {

#ifdef MODBUS_TCP
                    if (Calib_struct.Plant_connect_write_flag
                            == TSTWT_PLNT_CNCT_WRITE)
                    {
                        plant_connect_struct_write(0, PLANT_CONNECT_TST_WT_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_CAL_RCRD_FLAG);
                        //set the flag for the new record available
                        Flags_struct.Plant_connect_record_flag |=
                            NEW_PLANT_CONNECT_RECORD_FLAG;
                        report_data_populate(tst_wt_report);
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;

                        if (Setup_device_var.Log_cal_data == Screen431_str2)
                        {
                            /*populate the cal data log and update on the USB drive or flash*/
                            flash_write_status = log_data_populate(tst_wt_log);
                        }
                        else
                        {
                            flash_write_status = __TRUE;
                        }
                    }
                    else if (Calib_struct.Plant_connect_write_flag
                             == MAT_TST_PLNT_CNCT_WRITE)
                    {
                        plant_connect_struct_write(0, PLANT_CONNECT_MAT_WT_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_CAL_RCRD_FLAG);
                        //set the flag for the new record available
                        Flags_struct.Plant_connect_record_flag |=
                            NEW_PLANT_CONNECT_RECORD_FLAG;
                        report_data_populate(material_test_report);
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                        if (Setup_device_var.Log_cal_data == Screen431_str2)
                        {
                            /*populate the cal data log and update on the USB drive or flash*/
                            flash_write_status = log_data_populate(
                                                     mat_test_log);
                        }
                        else
                        {
                            flash_write_status = __TRUE;
                        }
                    }
                    else if (Calib_struct.Plant_connect_write_flag
                             == MAN_CAL_PLNT_CNCT_WRITE)
                    {
                        plant_connect_struct_write(0, PLANT_CONNECT_MANUAL_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_NO_ERROR_FLAG,
                                                   PLANT_CONNECT_CAL_RCRD_FLAG);
                        //set the flag for the new record available
                        Flags_struct.Plant_connect_record_flag |=
                            NEW_PLANT_CONNECT_RECORD_FLAG;
                        //if logging is enabled
                        if (Setup_device_var.Log_cal_data == Screen431_str2)
                        {
                            /*populate the cal data log and update on the USB drive or flash*/
                            flash_write_status = log_data_populate(calib_log);
                        }
                        else
                        {
                            flash_write_status = __TRUE;
                        }
                        report_data_populate(calib_report);
                        Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                        if (flash_write_status == __TRUE)
                        {
                            Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
                        }
                    }
#endif
                }

                if (Calib_Cancel_flag == 0)
                {
                    log_data_populate(calibration_finished);
                }
//               /*populate the material test weight cal report and update on the USB drive*/
//               report_data_populate(calib_report);
// 							Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
//               if (flash_write_status == __TRUE)
//               {
//                   Calib_struct.Calib_log_flag = DATA_LOG_FLAG_RESET;
//               }
            }

            //BS-195 Periodic Log duplicate record numbers, bad data , missing entry
            //Added by PVK on 2 May 2016 populate the cancel calibartion log into period log
            //calibration is completed and cancelled write the report and logs
            if ((Cal_Reject_Cmd_recd == 1) || (Calib_Cancel_flag == 1))
            {
                log_data_populate(calibration_cancel);
                Cal_Reject_Cmd_recd = 0;
                Calib_Cancel_flag = 0;
                az_cal_on =0;
            }

            //Added by PVK 0n 2 may 2016  write modbus data cal data and seetings into flash after writing data into USB
            if (Modbus_Data_Write_Cmd_recd == 1)
            {
                Modbus_Data_Write_Cmd_recd = 0;
                GUI_data_nav.GUI_structure_backup = 1;
            }
            //Calculate used memory for display screen
            //	if(usb_connect_done_fg == 0)					//by megha on 18/11/2016 to avoid conflict for display on USB int
            //		{
            used = GUI_ALLOC_GetNumUsedBytes();
            i++;

            if ((i % 5) == 0)
            {
                Display_Header(1);
                Display_Footer(1);
                //}
                if (strcmp(SCALE_SETUP_ORG_NO,
                           GUI_data_nav.Current_screen_info->Screen_org_no) == 0)
                {
                    Idler_center_dist_calc();
                    WM_Paint(GUI_data_nav.Listbox);
                }
                if (strncmp(RUN_MODE_ORG_NO, /*current_screen_org_no_main*/
                            GUI_data_nav.Current_screen_info->Screen_org_no, 1)
                        == 0)
                {
                    CreateWindow_runmode_screen(UPDATE_SCREEN);
                }
                if ((strncmp(DIAG_SCREEN_LIVE_SENSOR_DATA_ORG_NO,
                             GUI_data_nav.Current_screen_info->Screen_org_no, 3) == 0)
                        || (strncmp(DIAG_SCREEN_LIVE_VOLTAGE_DATA_ORG_NO,
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    3) == 0)
                        || (strncmp(DIAG_SCREEN_LIVE_ANALOG_OPT_ORG_NO,
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    3) == 0)
                        || (strncmp(DIAG_SCREEN_COMM_PORT_STATUS_ORG_NO,
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    3) == 0)
                        || (strncmp(TOTALS_SCREEN_ORG_NO,
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    3) == 0)
                        || (strncmp("550000",
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    2) == 0)
                        || (strncmp(DHCP_CONFIG_ORG_NO,
                                    GUI_data_nav.Current_screen_info->Screen_org_no,
                                    2) == 0))
                {
                    if (GUI_data_nav.Listbox != 0)
                    {
                        WM_Paint(GUI_data_nav.Listbox);
                    }
                }
                //Show the real time rate on accept and complete screens of calibrations
                if (GUI_data_nav.Current_screen_info->Screen_type == 10)
                {
                    data10 = GUI_data_nav.Current_screen_info->screen_data;
                    if ((strcmp(DYN_ZER_CAL_COMPLETE_ORG_NO,
                                GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0))
                    {


                        // For Rate variable
                        Calibration_var.real_time_rate =
                            Calculation_struct.Rate_for_display;
                        memset(rcvBuffer, 0, sizeof(rcvBuffer));
                        custom_sprintf(data10[sel].data_type3,
                                       data10[sel].Variable3, rcvBuffer,
                                       sizeof(rcvBuffer));
                        TEXT_SetText(GUI_data_nav.Text_widget3, "          ");
                        TEXT_SetText(GUI_data_nav.Text_widget3, rcvBuffer);
                        // For speed variable
                        memset(rcvBuffer, 0, sizeof(rcvBuffer));
                        custom_sprintf(data10[sel].data_type1,
                                       data10[sel].Variable1, rcvBuffer,
                                       sizeof(rcvBuffer));
                        TEXT_SetText(GUI_data_nav.Text_widget5, "          ");
                        TEXT_SetText(GUI_data_nav.Text_widget5, rcvBuffer);
                    }
                    else if (data10[sel].Variable4 != NULL
                             && data10[sel].data_type4 == float_type)
                    {
                        Calibration_var.real_time_rate =
                            Calculation_struct.Rate_for_display;
                        if (GUI_data_nav.Text_widget1 != 0)
                        {
                            memset(rcvBuffer, 0, sizeof(rcvBuffer));
                            custom_sprintf(data10[sel].data_type4,
                                           data10[sel].Variable4, rcvBuffer,
                                           sizeof(rcvBuffer));
                            TEXT_SetText(GUI_data_nav.Text_widget1,
                                         "          ");
                            TEXT_SetText(GUI_data_nav.Text_widget1, rcvBuffer);
                        }
                    }

                }
                //Update the speed value
                if ((strcmp(LEN_ZERO_START_BELT_ORG_NO,
                            GUI_data_nav.Current_screen_info->Screen_org_no) == 0)
                        || (strcmp(LEN_ZERO_CAL_START,
                                   GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0)
                        || (strcmp(LEN_ZERO_CAL_STOP,
                                   GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0)
                        || (strcmp(AUTO_BELT_START_BELT_ORG_NO,
                                   GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0)
                        || (strcmp(AUTO_BELT_CAL_START,
                                   GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0)
                        || (strcmp(AUTO_BELT_CAL_STOP,
                                   GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0)
                        || (strcmp(DYNAMIC_ZEROCAL_ACK_ORG_NO,
                                   GUI_data_nav.Current_screen_info->Screen_org_no)
                            == 0))
                {
                    if (GUI_data_nav.Text_widget1 != 0)
                    {
                        memset(rcvBuffer, 0, sizeof(rcvBuffer));
                        if (GUI_data_nav.Current_screen_info->Screen_type
                                == 9)
                        {
                            data9 =
                                GUI_data_nav.Current_screen_info->screen_data;
                            custom_sprintf(data9[sel].data_type,
                                           data9[sel].Variable1, rcvBuffer,
                                           sizeof(rcvBuffer));
                        }
                        if (GUI_data_nav.Current_screen_info->Screen_type
                                == 10)
                        {
                            data10 =
                                GUI_data_nav.Current_screen_info->screen_data;
                            custom_sprintf(data10[sel].data_type1,
                                           data10[sel].Variable1, rcvBuffer,
                                           sizeof(rcvBuffer));
                        }
                        TEXT_SetText(GUI_data_nav.Text_widget1, "          ");
                        TEXT_SetText(GUI_data_nav.Text_widget1, rcvBuffer);
                    }
                }
                i = 0;
            }
        }					//if Keypad_Timeout_Indication
        //Display Live sensor data
        if ((strncmp(DIAG_SCREEN_LIVE_IO_DATA_ORG_NO, /*current_screen_org_no_main*/
                     GUI_data_nav.Current_screen_info->Screen_org_no, 3) == 0)
                || ((GUI_data_nav.Current_screen_info->Screen_org_no[0] == '5')
                    && (GUI_data_nav.Current_screen_info->Screen_org_no[1]
                        == '4')
                    && (GUI_data_nav.Current_screen_info->Screen_org_no[2]
                        == '0')))
        {
            if (GUI_data_nav.Listbox != 0)
            {
                WM_Paint(GUI_data_nav.Listbox);
                IO_board_param.Status_displayed_flag = __TRUE;
            }
        }
        LPC_RTC->ALMIN &= ~(0x07<<3);
        LPC_RTC->ALMIN |= (0x07<<3);		//SECTION 7
        //Clear weight if the flag is set and password entered is correct
        if ((IO_board_param.Clear_weight_input_set == __TRUE)
                || (GUI_data_nav.Clr_wt_pw_to_be_entered == CLEAR_THE_WEIGHT))
        {
            if (Zero_key_press_flag == 2)
            {
                Zero_key_press_flag = 1;
                GUI_data_nav.Key_press = __TRUE;
            }
            //Megha added this for BS-155 fix on 25-Nov-2015 to clear totals when 'Clear weight'
            //status is lock from Admin menu
            else if (Clr_totals_key_press_flag != 0xFF)
            {
                switch (Clr_totals_key_press_flag)
                {
                case 0:
                    Rprts_diag_var.Job_Total = 0.0;
                    Totals_var.Job_Total = 0.0;
                    break;
                case 1:
                    //Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix
                    /*populate the periodic log for Daily weight and update on the USB drive, if enabled*/
                    if (Setup_device_var.Log_clear_wt_data == Screen431_str2)
                    {
                        log_data_populate(daily_wt_clr_log);
                    }
										//BS 282 SKS->
                    //Rprts_diag_var.daily_Total_weight = 0.0;
                    //Totals_var.daily_Total_weight = 0.0;
										//<-
                    //Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix
                    /*populate the periodic log for cleared Daily weight and update on the USB drive, if enabled*/
                    if (Setup_device_var.Log_run_time_data == Screen431_str2)
                    {
                        log_data_populate(clear_log);
                    }
                    break;
                case 2:
								//BS 282 SKS->
                //    Rprts_diag_var.weekly_Total_weight = 0.0;
                //    Totals_var.weekly_Total_weight = 0.0;
										//<-
                    break;
                case 3:
								//BS 282 SKS->
                 //   Rprts_diag_var.monthly_Total_weight = 0.0;
                 //   Totals_var.monthly_Total_weight = 0.0;
										//<-
                    break;
                case 4:
								//BS 282 SKS->
                //    Rprts_diag_var.yearly_Total_weight = 0.0;
                //    Totals_var.yearly_Total_weight = 0.0;
                    break;
										//<-
                case 5:
								//BS 282 SKS->
                 //   Rprts_diag_var.Master_total = 0.0;
                 //   Totals_var.Master_total = 0.0;
								 //<-
                    break;
                default:
                    break;
                }
                Clr_totals_key_press_flag = 0xFF;
                IO_board_param.Clear_weight_input_set = __FALSE;
            }
            else
            {
                if (OS_R_OK == os_mut_wait(&FLash_Mutex, FLASH_MUTEX_TIMEOUT))
                {
                    clear_weight();
                    IO_board_param.Clear_weight_input_set = __FALSE; //Added by DK on 28 April 2016
                    //os_mut_release (&FLash_Mutex);
                }
                os_mut_release(&FLash_Mutex);

                //IO_board_param.Clear_weight_input_set = __FALSE; //Commented by DK on 28 April 2016
            }
            GUI_data_nav.Clr_wt_pw_to_be_entered = CLR_WT_FLAG_RESET;
        }
        LPC_RTC->ALMIN &= ~(0x07<<3);		// SECTION EXIT
    } //while(1)
}
#endif
/*****************************************************************************
 * End of file
 *****************************************************************************/
