
#define NO_OF_BACKUPS					3
#define BACKUP_INSTANCES  				1
#define SECTOR_SIZE						0x20000
#define VARIABLE_STORAGE_BASE_ADDR		0x300000
#define TOTAL_STORAGE_BASE_ADDR			0x400000


uint8_t u8RecursiveWriteBackUpVar = 0;

uint32_t LatestStructureVersion=0x00;
uint32_t OldestStructureVersion=0xFFFFFFFF;

struct sTotalBackupStruct;
struct sVariableStruct;

typedef struct RecHeader
{
	uint16_t u16structHeader;
	uint16_t u16structFirmwareversion;
	uint16_t u16Size;
	uint32_t u32sVersionNo;
}sRecordHeader;

struct
{
	uint16_t u16structHeader;
	uiint16_t u16Size;
	uint32_t u32sVersionNo;
	
}sTotalBackupStruct;


struct 
{
	sRecordHeader sHeader;
	uint64_t u64AccumulatedWeight;
}sVariableStruct;

struct 
{
	sRecordHeader sHeader;
	uint64_t u64AccumulatedWeight;
	
}sVariableStructV1;

enum
{
	enum_totals = 0,
	enum_variables,
	enum_periodic_log,
	enum_system_log
	
}

WriteStructure(unsigned char *u8sptr,unsigned char u8Type ,uint32 u32Address,uint8 u8NoofBackups,uint8 u8BackupInstances)

{
	
	switch(u8Type)
	{
		case enum_totals:
				GetCurrentWriteAddress(u8Type);
			break;
	}
}


uint32 GetCurrentWriteAddress(unsigned char u8Type)
{
	uint32_t u32BaseAddress,max_version=0,min_version=0xFFFFFFFF,temp_version;
	uint32_t u32CurrentAddress,u32OldestAddress,data_valid,size_of_struct =0;
	uint32_t u32CyclicCnt=0,u32CyclicCnt_max,u32NoofBackup =0,u32NoofBackup_max=0;
	u8 * readAddress,*readAddressBase;
	sVariableStruct *sptrVariableStruct;
	sRecordHeader *psHeader;
	switch(u8Type)
	{
		case enum_totals:											//TRAVERSING THE ORIGINALS FOR TOTALS
				u32BaseAddress = TOTAL_STORAGE_BASE_ADDR;			// TOTALS ORIGINALS , SECTOR  STARTS HERE
				readAddress = u32BaseAddress;					// HEAD POINTER FOR TRAVERSING
				psHeader = u32BaseAddress;		
				u32CyclicCnat =0;
				u32NoofBackup =0;
				u32CyclicCnt_max =2;
				u32NoofBackup_max =3;
				readAddressBase = TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE; 
				break;
	}
	do
	{
		
		u16CalculatedCheckSum = CalculateCheckSum(psHeader,psHeader-> u16Size);						//CALCULATE CHECKSUM OF STORED DATA
		u16CheckSum = *(readAddress + psHeader->u16Size-1);													// READ STORED CHECKSUM
		if((psHeader-> u16structHeader == VALID_HEADER) && u16CheckSum == u16CalculatedCheckSum)	// COMPARE CHECKSUM AND HEADER CONSTANT
		{																							// MATCHES WELL -> ALL IS WELL
			temp_version = psHeader->u32sVersionNo;													// GET VERSION NUMBER
			if (temp_version > max_version )														// UPDATE MAX VERSION NUMBER
			{
				max_version = temp_version;
				u32CurrentAddress = readAddress +psHeader -> u16Size;								// IF THE RECORD IS LATEST UPDATE CURRENT READ ADDRESS	
			}
			if (temp_version < min_version_version )												// UPDATE OLDEST VERSION NUMBER
			{
				min_version = temp_version;
				u32OldestAddress = u32CurrentAddress;
			}	
			readAddress=readAddress +psHeader -> u16Size;											// UPDATE STRUCT HEADER POINTER TO THE NEXT LOCATION
			psHeader =readAddress;																	// READ THE NEXT HEADER
	//		if (readAddress > readAddressBase + SECTOR_SIZE )												// data present in the other structure too
	//		{
	//			if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;
	//		else break;
	//		readAddressBase = TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE; 
	//		readAddress = readAddressBase;
	//		}
		}
		else 
		{	
			if ((readAddress+ psHeader->u16Size)> (readAddressBase + SECTOR_SIZE) )
			{
				if(u32CyclicCnt<u32CyclicCnt_max)u32CyclicCnt++;
					else break;
				readAddressBase = TOTAL_STORAGE_BASE_ADDR + (u32CyclicCnt*(u32NoofBackup_max+1))*SECTOR_SIZE; 
				readAddress = readAddressBase;
			}
	//not a valid structure!
			if()
			{
				
			}
			else
			// jump to the backup sector 1
			{
				//jump to backup sector 2
				if()
				{
					else
					{
					// jump to backup sector 3
					if ()
					{
						else
						{
							//die
							while(1);
						}
					}
				}
			}
		}
	} while(readAddress <(u32BaseAddress + (SECTOR_SIZE * (((NO_OF_BACKUPS+1)*(u32CyclicCnt_max))) +1 ));											// CHECK UNTIL END OF SECTOR IS REACHED				
	}
