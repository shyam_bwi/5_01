/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 	     : RTOS_main.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Friday, 2012  <July 13, 2012>
* @date Last Modified  : July Friday, 2012  <July 13, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 		       : 
* @internal 		       :
*
*****************************************************************************/
#ifndef __RTOS_MAIN_H
#define __RTOS_MAIN_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define MODBUS_SEN_TASK_PRIORITY          16
#define MODBUS_INT_TASK_PRIORITY_MASTER   15
#define PRINTER_TASK_PRIORITY             9
#define MODBUS_INT_TASK_PRIORITY_SLAVE    15
#define GUI_TASK_PRIORITY                  5
#define WDT_FEED_TASK_PRIORITY						5


#define FLASH_MUTEX_TIMEOUT 50
#define USB_MUTEX_TIMEOUT 0X10//0xFFFE
#define WDT_MUTEX_TIMEOUT 0X10//0xFFFE 
#define PERIODIC_LOG_UNIT_SEC     999
#define PERIODIC_LOG_SEC_INTERVAL 1
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
//PVK
extern OS_MUT FLash_Mutex;

#ifdef MODBUS_TCP
  extern OS_TID t_modbus_tcp_send;
#endif

#ifdef USB
  extern OS_TID t_usb;
#endif

#ifdef MODBUS_RTU_INTEGRATOR
  extern OS_TID t_modbus_integrator_master_rtu;
  extern OS_TID t_modbus_integrator_slave_rtu;
#endif

#ifdef MODBUS_SENSOR_RTU_MASTER
  extern OS_TID t_modbus_sensor_rtu;
#endif

#ifdef IOBOARD
	extern OS_TID t_ioboard;//, t_weight_monitor;
#endif

#ifdef PRINTER
	extern OS_TID t_printer_data;
#endif

#ifdef KEYPAD
  extern OS_TID t_keypad;
#endif

#ifdef DEBUG_DATA_SEND
extern OS_TID t_Send_Data_On_Debug_Port;
#endif

  extern OS_TID t_flash_log_store;

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern __task void init (void);

#endif /*RTOS_MAIN*/
/*****************************************************************************
* End of file
*****************************************************************************/
