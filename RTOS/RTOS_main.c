/*****************************************************************************
 * @copyright Copyright (c) 2012-2013 Beltway, Inc.
 * @copyright This software is a copyrighted work and/or information
 * @copyright protected as a trade secret. Legal rights of Beltway. In this
 * @copyright software is distinct from ownership of any medium in which
 * @copyright the software is embodied. Copyright or trade secret notices
 * @copyright included must be reproduced in any copies authorized by
 * @copyright Beltway, Inc.
 *
 * @detail Project      : Beltscale Weighing Product - Integrator Board
 * @detail Customer     : Beltway
 *
 * @file Filename       : Rtos_main.c
 * @brief               : Controller Board
 *
 * @author              : Anagha Basole
 *
 * @date Created        : July Thursday, 2012  <July 12, 2012>
 * @date Last Modified  : July Thursday, 2012  <July 12, 2012>
 *
 * @internal Change Log : <YYYY-MM-DD>
 * @internal            :
 * @internal            :
 *
 *****************************************************************************/

/*============================================================================
 * Include Header Files
 *===========================================================================*/
#include "Global_ex.h"
#include "RTOS_main.h"
#include "Display_task.h"
#include "Keypad.h"
#include "USB_main.h"
#include "rtc.h"
#include "SMTP_demo.h"
#include "Modbus_tcp.h"
#include "Modbus_rtu_master.h"
#include "Printer_high_level.h"
#include "Scoreboard.h"
#include "LCDConf.h"
#include "IOboard_App.h"
#include "File_update.h"
#include "Log_report_data_calculate.h"
#include "Ext_flash_high_level.h"
#include "EEPROM_high_level.h"
#include "IOProcessing.h"
#include "Screen_data_enum.h"
#include "Modbus_rtu_slave.h"
#include "Modbus_uart_low_level.h"
#include "Main.h"
#include "EMAC_LPC177x_8x.h"
#include "Screen_global_ex.h"
#include "Ext_flash_low_level.h"
#include "Plant_connect.h"
#include "Calibration.h"
#include "Screenshot_bmp.h"
#include "wdt.h"
#include "RW_FLASH.h"
#include "udp_test_main.h"
#ifdef RTOS
/*============================================================================
 * Private Macro Definitions
 *===========================================================================*/
/* Defines Section */
//#define MODBUS_SEN_TASK_PRIORITY          16 --- moved to rtos_main.h
//#define MODBUS_INT_TASK_PRIORITY_MASTER   15 --- moved to rtos_main.h
#define RTC_TASK_PRIORITY                  13
#define IOBOARD_TASK_PRIORITY              14
#define TCP_TICK_TASK_PRIORITY             12
#define MODBUS_TCP_TASK_PRIORITY           11
#define SCOREBOARD_TASK_PRIORITY           10
//#define PRINTER_TASK_PRIORITY            8 --- moved to rtos_main.h
#define TCP_MAIN_TASK_PRIORITY             8
#define UDP_MAIN_TASK_PRIORITY             6

#define KEYPAD_TASK_PRIORITY               7
//#define WT_MONITOR_TASK_PRIORITY         6
//#define GUI_TASK_PRIORITY                5
//#define MODBUS_INT_TASK_PRIORITY_SLAVE   3
#define TICK_TIMER_TASK_PRIORITY           4
#define EMAIL__TASK_PRIORITY               3
#define USB_TASK_PRIORITY                  13

#ifdef DEBUG_DATA_SEND
#define DEBUG_DATA_SEND_TASK_PRIORITY			 2
#endif

int task_number =0;

/*============================================================================
 * Private Data Types
 *===========================================================================*/
unsigned char Power_on_auto_zero_cal_fg,Auto_zero_Start_flag;
/*============================================================================
 *Public Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */
OS_TID t_Free_Running_Timer_tick;
//PVK
OS_MUT FLash_Mutex;
OS_MUT USB_Mutex;
OS_MUT Debug_uart_Mutex;
OS_MUT WDT_Feed_Mutex;
#ifdef MODBUS_RTU_INTEGRATOR
OS_TID t_modbus_integrator_master_rtu;
OS_TID t_modbus_integrator_slave_rtu;
#endif

/*#ifdef FLASH_LOG_TASK
 OS_TID t_flash_log_store;
 #endif //#ifdef FLASH_LOG_TASK
 */

#ifdef MODBUS_TCP
OS_TID t_modbus_tcp_send;
#endif //#ifdef MODBUS_TCP

#ifdef ETHERNET
OS_TID t_tick;
OS_TID t_tcp_task_main;
OS_TID t_udp_task_main;
OS_TID t_tcp_task_server;
static U64 tcp_stack[2048 / 8];
static U64 udp_stack[2048 / 8];
#endif

#ifdef EMAIL
OS_TID t_email;
#endif

#ifdef KEYPAD
OS_TID t_keypad;
#endif

#ifdef PRINTER
static U64 printer_send_stack[1024 / 8] __attribute__ ((section ("PRINTER_TASK_STACK")));
OS_TID t_printer_data;
#endif

#ifdef SCOREBOARD
OS_TID t_scoreboard_data;
#endif

#ifdef IOBOARD
OS_TID t_ioboard; //, t_weight_monitor;
static U64 Ioboard_stack[1600 / 8];
#endif

#ifdef RTC
OS_TID t_rtc_tick;
static U64 rtc_stack[2048 / 8] __attribute__ ((section ("RTC_TASK_STACK")));
#endif

#ifdef USB
//static U64 usb_stack[1024/8]; //Commented by DK on 3 March 2016
static U64 usb_stack[4096/8] __attribute__ ((section ("USB_TASK_STACK"))); //Added by DK on 3 March 2016
OS_TID t_usb, t_usb_engine;
#endif

#ifdef MODBUS_SENSOR_RTU_MASTER
static U64 modbus_master_stack[1024 / 8];
OS_TID t_modbus_sensor_rtu;
#endif

#ifdef MODBUS_TCP
//static U64 modbus_stack[800/8];
static U64 modbus_stack[2048 / 8] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));
#endif

#ifdef DEBUG_DATA_SEND
static U64 Send_Data_On_Debug_Port_stack[1024] __attribute__ ((section ("DEBUG_DATA_SEND_DATA_RAM")));
OS_TID t_Send_Data_On_Debug_Port;
#endif

#ifdef GUI
//static U64 gui_stack[1600/4];
static U64 gui_stack[4096 / 8] __attribute__ ((section ("GUI_TASK_STACK")));
OS_TID t_lcd_update;
#endif

#ifdef KEYPAD
//static U64 keypad_stack[1024/8];
static U64 keypad_stack[2048 / 8] __attribute__ ((section ("KEYPAD_TASK_STACK")));

#endif

udpPacket_type udpdata;
OS_TID t_WDT_feed;
DATA_LOG_HEADER_STRUCT fdt_hdr;

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
 * Private Variables
 *===========================================================================*/
/* Constants section */

/* Boolean variables section */
static BOOL log_data_pouplate_pending = 0; //Added by DK on 4 May2016

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
 * Private Function Prototypes Declarations
 *===========================================================================*/

/*============================================================================
 * Function Implementation Section
 *===========================================================================*/

/*****************************************************************************
 * @note       Function name:
 * @returns    returns      : None
 * @param      arg1         : None
 * @param      arg2         : None
 * @author                  : Anagha Basole
 * @date       date created :
 * @brief      Description  :
 * @note       Notes        : None
 *****************************************************************************/
__task void Free_Running_Timer_tick(void)
{
    while (1)
    {
        os_itv_set(FREE_RUN_TIMER_TASK_INTERVAL);
        os_itv_wait();
        LPC_RTC->ALSEC |= 0x01;
        //Delay for keypad timeout
        if (!(Keypad_Timeout <= 0))
        {
            //DCR the count
            Keypad_Timeout--;
        }
        //check timeout over if yes then put the LCD backlit in off state
        Keypad_Timeout_LCDOff();
        LPC_RTC->ALSEC &= ~(0x01);
    }
}

#ifdef RTC
/*****************************************************************************
 * @note       Function name: __task void get_time (void)
 * @returns    returns      : None
 * @param      arg1         : None
 * @param      arg2         : None
 * @author                  : Anagha Basole
 * @date       date created :
 * @brief      Description  : Gets the updated time from the RTC, populates all
 *                          : reports every minute, updates the periodic log
 *                          : according to the time specified (if logging is enabled)
 *                          : and also updates the reports depending on the time.
 * @note       Notes        : None
 *****************************************************************************/
int deinit_flag,init_flag,checkUSB;
int check_wdt ;
__task void get_time(void)
{
    static RTCTime Prev_time;
    unsigned int Periodic_log_interval_1Sec;
    unsigned int prev_day, prev_month, prev_year, uninit_count,pinVal,prev_week;
    //  static double prev_weight=0;
    U8 err = 0,err1=0,err2=0;
    U8 u8File_exists = 0;
    FILE *fp,*fpw;
    U16 i,j, read_buffer[30];
    U16* add,u16Var;
    RTCTime last_log_time;
    char file_name1[FILE_NAME_SIZE]= {0};
    unsigned char uCharTime[10];
    int ch;
    //int i=0;
    Current_time = RTCGetTime();
    Prev_time = Current_time;
    //SKS ->new variables added to be saved in
    //Genral purpose registers with power backup (RTC VBAT) <--
    prev_day = LPC_RTC->GPREG3;
    prev_week = LPC_RTC->GPREG4;
    prev_month = LPC_RTC->ALDOM;
    prev_year = LPC_RTC->ALYEAR;
    //prev_day = LPC_RTC->GPREG;

    while (1)
    {
        //while(check_wdt);
        //SKS ->new variables added to be saved in
        //Genral purpose registers with power backup (RTC VBAT) <--
//		prev_day = LPC_RTC->GPREG3;

//		prev_week = LPC_RTC->GPREG4;

        prev_month = LPC_RTC->ALMON;
        prev_year = LPC_RTC->ALYEAR;

        Task_section = 1;
        os_itv_set(RTC_GET_TIME_TASK_INTERVAL);
        os_itv_wait();
        LPC_RTC->ALHOUR &= ~(0x07<<1);
        LPC_RTC->ALHOUR |= (0x01<<1);		//SECTION1
        /* read RTC. */
        Current_time = RTCGetTime();
        //Added by DK on 4 May2016
//         if (log_data_pouplate_pending == 1)
//         {
//             log_data_pouplate_pending = 0;
//             log_data_populate(periodic_log);
//         }

        if (send_debug == 1)
            os_evt_set(PRINTER_EVENT_FLAG, t_printer_data);
        //Added on 21-Oct-2015 for storing last AM_PM setting
        if (Admin_var.AM_PM == AM_TIME_FORMAT)
            daily_rprt.End_time.AM_PM = AM;		//AM = 0
        else
            daily_rprt.End_time.AM_PM = PM;		//PM = 1
        //get current day time
        Admin_var.Day_of_week = Get_Dow_String_Enum(Current_time.RTC_Wday);
        Admin_var.Config_Date.Day = Current_time.RTC_Mday;
        Admin_var.Config_Date.Mon = Current_time.RTC_Mon;
        Admin_var.Config_Date.Year = Current_time.RTC_Year;
        // write missed logs if any
        if (ferror_cnt)
        {
            if (!g_restore_prog)
            {
                write_missed_log_recs();  //sks
            }
        }
        LPC_RTC->ALHOUR &= ~(0x07<<1);
        LPC_RTC->ALHOUR |= (0x02<<1);		//SECTION2
        //SKS check if down key pressed for 3 consecutive sec.. accordinglu uninit FS
        {
            pinVal = LPC_GPIO0->PIN;
            if (uninit_msd_flag==1)
            {
                if ((pinVal&0x01<<26)==0x01<<26)
                {
                    if ((Prev_time.RTC_Sec !=Current_time.RTC_Sec) )
                    {
                        uninit_count++;
                    }
                    if (uninit_count>3)
                    {
                        uninit_msd_flag =1;
                        checkUSB =0;
                    }
                }
                else
                {
                    uninit_count=0;
                    uninit_msd_flag=0;
                }
            }

            if (checkUSB ==0)///*&&(uninit_msd_flag==1)*/)
            {
                if  ((pinVal&0x01<<13)!=0x01<<13);
                else
                {
                    checkUSB =1;
                    uninit_msd_flag =1;
                }
            }
        }
        //populate the reports and store them on the USB according to the settings
        //SKS -> XXX not a fool proof implementation
        if ((Prev_time.RTC_Sec == 59) && (Current_time.RTC_Sec == 0))
        {
            //Increment run time
            Calculation_struct.run_time++;
            /*populate the report data and error data every 1 min since the task gets called every 1 second*/
            // error_status();
            report_data_populate(populate_regular_reports); //update the structures

            //PVK
            /*Store the weight in the EEPROM every 1 min*/
            /*if(prev_weight != Calculation_struct.Total_weight_accum)
             {
             prev_weight = Calculation_struct.Total_weight_accum;
             eeprom_weight_backup();
             }
             */

            /*Store all totals and report parameters in Ext Flash every 1 Min */
            if (OS_R_OK == os_mut_wait(&FLash_Mutex, FLASH_MUTEX_TIMEOUT))
            {
                Flash_Write_In_progress = 1; //Added on 27 April 2016

                flash_para_backup();

                Flash_Write_In_progress = 0; //Added on 27 April 2016

                //os_mut_release (&FLash_Mutex);
            }
            os_mut_release(&FLash_Mutex);
#ifdef MODBUS_TCP
            //set the flag for the new record available
            Flags_struct.Plant_connect_record_flag |=
                NEW_PLANT_CONNECT_RECORD_FLAG;
#endif
        }
        else
        {
            //	return;
        }
        //<--

        LPC_RTC->ALHOUR &= ~(0x07<<1);
        LPC_RTC->ALHOUR |= (0x03<<1);		//SECTION3
        Task_section = 2;
        /*
         * check if a change in GUI / Screen variables has been done.
         * if flag is set then write to eeprom
         * set and release mutex in the proccess
         */
        {
            // Writing GUI structures in EEPROM now is based on GUI varaible change Flag
            if (GUI_data_nav.GUI_structure_backup == 1)
            {
                if (OS_R_OK == os_mut_wait(&FLash_Mutex, FLASH_MUTEX_TIMEOUT))
                {
                    //	if(enable_eeprom_struc_write_flag == 1) //Added on 27 April 2016
                    {
                        eeprom_struct_backup();
                        //os_mut_release (&FLash_Mutex);
                        GUI_data_nav.GUI_structure_backup = 0;
                        //		enable_eeprom_struc_write_flag = 0; //Added on 27 April 2016
                    }
                }
                os_mut_release(&FLash_Mutex);
            }
        }
        //SKS : watchdog implementation to 4_65
        //	if (Prev_time.RTC_Sec != Current_time.RTC_Sec)
        //	WDTFeed();
        LPC_RTC->ALHOUR &= ~(0x07<<1);
        LPC_RTC->ALHOUR |= (0x04<<1);		//SECTION4
        Task_section = 3;
        //Added by PVK 4 May 2016 for 1 sec logging
        //SKS : below routine not used on field yet
        if (PERIODIC_LOG_UNIT_SEC == Setup_device_var.Periodic_log_interval)
        {
            Periodic_log_interval_1Sec = PERIODIC_LOG_SEC_INTERVAL;
            if (((Current_time.RTC_Sec % Periodic_log_interval_1Sec) == 0)
                    && (Prev_time.RTC_Sec != Current_time.RTC_Sec))
            {
                //BS-152 fixed by DK on 27 April 2016
                if ((Admin_var.Zero_rate_status == Screen6171_str2)
                        || (Admin_var.Zero_rate_status == Screen6171_str5))
                {
                    if (clear_weight_in_process == 0) //Added by DK on 4 May2016
                        log_data_populate(periodic_log);
                    else
                        log_data_pouplate_pending = 1; //Added by DK on 4 May2016
                }
                //				}
            }
        }
        else if (((Current_time.RTC_Min
                   % Setup_device_var.Periodic_log_interval) == 0)
                 && (Prev_time.RTC_Min != Current_time.RTC_Min))
        {
            Periodic_log_interval_1Sec = 0;

            //BS-152 fixed by DK on 27 April 2016
            if ((Admin_var.Zero_rate_status == Screen6171_str2)
                    || (Admin_var.Zero_rate_status == Screen6171_str5))
                /*populate the periodic log for user defined minutes and update the file on the USB drive,
                 if logging is enabled*/
                if (Setup_device_var.Log_run_time_data == Screen431_str2)
                {
                    {
                        //if (Flags_struct.Connection_flags & USB_CON_FLAG)
                        if (con == 1)
                        {
                            //if log file exists
                            sprintf(&file_name1[0],"%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",LPC_RTC->YEAR,LPC_RTC->MONTH,
                                    LPC_RTC->YEAR,LPC_RTC->MONTH,LPC_RTC->DOM);
                            fp = fopen (file_name1, "r");
                            if (fp != NULL)
                            {
                                u8File_exists = 1;
                                fclose(fp);
                            }
                            else
                                u8File_exists = 0;
                            if (u8File_exists)
                                log_data_populate(periodic_log);
                            else
                            {
                                sprintf(file_name1,"U0:\\last_file.txt" );
                                //IF FILE EXISTS

                                i=0;
                                fp = fopen (file_name1, "r");
                                if (fp != NULL)	//LAST FILE EXISTS
                                {
                                    ch =fgetc(fp);
                                    while(ch!=EOF)
                                    {
                                        file_name1[i] = ch;

                                        ch =fgetc(fp);
                                        i++;

                                    }
                                    file_name1[i+1] = '\0';
                                    fclose(fp);
                                }
                                fp = fopen (file_name1, "r");
                                if (fp != NULL)	//LAST FILE EXISTS
                                {
                                    //SKS : BS272 	file exists
                                    u8File_exists = 1;
                                    fclose(fp);
                                    strtok(file_name1,"\\");
                                    strtok(NULL,"\\");
                                    last_log_time.RTC_Year = atoi(	strtok(NULL,"-"));
                                    last_log_time.RTC_Mon = atoi(	strtok(NULL,"-"));
                                    last_log_time.RTC_Mday = atoi(	strtok(NULL,"_"));

                                    //SKS : BS270 write FDT here
                                    //SKS : BS272 update FDT header here
                                    fdt_hdr.Date.Year=last_log_time.RTC_Year;
                                    fdt_hdr.Date.Mon = last_log_time.RTC_Mon;
                                    fdt_hdr.Date.Day=last_log_time.RTC_Mday;
                                    fdt_hdr.Time.Hr=23;
                                    fdt_hdr.Time.Min=59;
                                    fdt_hdr.Time.Sec=59;
                                    log_data_populate(periodic_log_stop);	//ok

                                }
                                else u8File_exists = 0;//SKS : BS272 do nothing
                                if(HDR_Written ==0)
                                {
                                    //HDR
//                                    periodic_data.Periodic_hdr.Id = LPC_RTC->GPREG2;
//                                     set_zero_data_log.Set_zero_hdr.Id = LPC_RTC->GPREG2;
//                                     clear_data_log.Clr_data_hdr.Id = LPC_RTC->GPREG2;
//                                     calib_data_log.Cal_data_hdr.Id = LPC_RTC->GPREG2;
//                                     //error_data_log.Err_data_hdr.Id = 0;// //commented by DK on 16 Feb 2016
//                                     length_cal_data_log.Len_data_hdr.Id = LPC_RTC->GPREG2;

                                    strcpy((char *) fheader.File_hdr.Data_type, "HDR");
                                    fheader.File_hdr.Id = 0;
                                    fheader.File_hdr.Date.Year = Current_time.RTC_Year;
                                    fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
                                    fheader.File_hdr.Date.Day = Current_time.RTC_Mday;
                                    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                                        fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
                                    else if (Admin_var.Current_Time_Format == TWELVE_HR)
                                    {
                                        if (Admin_var.AM_PM == AM_TIME_FORMAT)
                                        {
                                            if ((U8) Current_time.RTC_Hour == 12)
                                                fheader.File_hdr.Time.Hr = 0;
                                            else
                                                fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
                                        }
                                        else
                                        {
                                            fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour + 12;
                                        }
                                    }
                                    //fheader.File_hdr.Time.Hr = Admin_var.Config_Time.Hr;
                                    fheader.File_hdr.Time.Min = Current_time.RTC_Min;
                                    fheader.File_hdr.Time.Sec = Current_time.RTC_Sec;
                                    //  if (Flags_struct.Connection_flags & USB_CON_FLAG)
                                    //if (con == 1)
                                    // {
                                    //Check for folder structure
                                    if ((g_old_fw_ver >= (float) 1.00)
                                            && (g_old_fw_ver <= (float) 4.09))
                                    {
                                        sprintf(&file_name1[0], "%04d-%02d-%02d_periodic_log.txt",
                                                fheader.File_hdr.Date.Year,
                                                fheader.File_hdr.Date.Mon,
                                                fheader.File_hdr.Date.Day);

                                    }
                                    else
                                    {
                                        sprintf(&file_name1[0],
                                                "%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",
                                                fheader.File_hdr.Date.Year,
                                                fheader.File_hdr.Date.Mon,
                                                fheader.File_hdr.Date.Year,
                                                fheader.File_hdr.Date.Mon,
                                                fheader.File_hdr.Date.Day);
                                    }

                                    log_file_init(file_name1);
                                    HDR_Written =1;
                                }
                                {
                                    //SKS : BS272 IDT records.
                                    periodic_data.Periodic_hdr.Date.Year = Current_time.RTC_Year;
                                    periodic_data.Periodic_hdr.Date.Mon =Current_time.RTC_Mon;
                                    periodic_data.Periodic_hdr.Date.Day = Current_time.RTC_Mday;
                                    periodic_data.Periodic_hdr.Time.Hr = Current_time.RTC_Hour;
                                    periodic_data.Periodic_hdr.Time.Min =Current_time.RTC_Min;
                                    periodic_data.Periodic_hdr.Time.Sec = Current_time.RTC_Sec;

                                    log_data_populate(periodic_log_start);
                                }

                                Current_time = RTCGetTime();
                                sprintf(&file_name1[0],"%04d\\%02d\\%04d-%02d-%02d_periodic_log.txt",Current_time.RTC_Year,Current_time.RTC_Mon,
                                        Current_time.RTC_Year,Current_time.RTC_Mon, Current_time.RTC_Mday);
                                fp = fopen ("last_file.txt", "w");
                                if (fp != NULL)	//LAST FILE EXISTS
                                {
                                    fprintf(fp,"%s", file_name1);     //ok?    SEEMS ok.. TEST IT
                                    fclose (fp);
                                }
                                //GET CURRENT FILE NAME AND WRITE TO LAST_FILE.TXT
                            }
                        }
                        else
                            log_data_populate(periodic_log);
                    }
                }
        }
        LPC_RTC->ALHOUR &= ~(0x07<<1);
        LPC_RTC->ALHOUR |= (0x05<<1);		//SECTION5
        if ((Current_time.RTC_Hour == 23 && Current_time.RTC_Min == 59
                && Current_time.RTC_Sec == 59 && Prev_time.RTC_Sec == 58
                && Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                || ((Current_time.RTC_Hour == 11)
                    && (Current_time.RTC_Min == 59)
                    && (Current_time.RTC_Sec == 59)
                    && (Prev_time.RTC_Sec == 58)
                    && (Admin_var.Current_Time_Format == TWELVE_HR)
                    && (Admin_var.AM_PM == PM_TIME_FORMAT)))
        {
            //log_data_populate(periodic_log_stop);
            //report_data_populate(daily_report);
            fdt_hdr.Date.Year = (U16) Current_time.RTC_Year;
            fdt_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
            fdt_hdr.Date.Day = (U8) Current_time.RTC_Mday;
            fdt_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
            fdt_hdr.Time.Min = (U8) Current_time.RTC_Min;
            fdt_hdr.Time.Sec = (U8) Current_time.RTC_Sec;

            //Plant_connect_var.Accumulated_weight_before_clr = Rprts_diag_var.daily_Total_weight;
            nooftimes = 0;
            //log_data_populate(periodic_log_stop);
        }
        LPC_RTC->ALHOUR &= ~(0x07<<1);
        LPC_RTC->ALHOUR |= (0x06<<1);		//SECTION6
        Task_section = 4;
#ifdef SCREENSHOT
// 			if(Take_Screen_Shot_Flag == __TRUE)
// 			{
// 				MainTask();
// 				Take_Screen_Shot_Flag =__FALSE;
// 			}
#endif
//SKS end of day routine

        {
            if ((prev_month != Current_time.RTC_Mon)
                    || (Current_time.RTC_Year != prev_year)
                    || (prev_day != Current_time.RTC_Mday))
            {

                if (OS_R_OK == os_mut_wait (&USB_Mutex,USB_MUTEX_TIMEOUT))
                {
                    fdt_hdr.Date.Year = (U16) fdt_hdr.Date.Year;
                    fdt_hdr.Date.Mon = (U8) fdt_hdr.Date.Mon;
                    fdt_hdr.Date.Day = (U8) fdt_hdr.Date.Day;
                    fdt_hdr.Time.Hr = (U8) fdt_hdr.Time.Hr;
                    fdt_hdr.Time.Min = (U8) fdt_hdr.Time.Min;
                    fdt_hdr.Time.Sec = (U8) fdt_hdr.Time.Sec;
                    Plant_connect_var.Accumulated_weight =
                        Calculation_struct.Total_weight;
                    nooftimes = 0;
                    //Added on 12-Oct-2015 for correct FDT entry in 12/24 hr mode
                    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                    {
                        fdt_hdr.Time.Hr = (U8) fdt_hdr.Time.Hr;
                    }
                    else if (Admin_var.Current_Time_Format == TWELVE_HR)
                    {
                        fdt_hdr.Time.Hr = (U8) fdt_hdr.Time.Hr + 12;
                    }
                    //==
                    //              if (con==1)usbh_engine_all();
//                log_data_populate(periodic_log_stop);
//                if (con==1)usbh_engine_all();
                    /*populate the daily report and update on the USB drive*/

                    //tsk_lock();
                    //=====
#ifdef SHOW_REPORTS
                    report_data_populate(daily_report);
#endif
                    periodic_data.Periodic_hdr.Date.Year = (U16) Current_time.RTC_Year;
                    periodic_data.Periodic_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
                    periodic_data.Periodic_hdr.Date.Day = (U8) Current_time.RTC_Mday;
                    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                    {
                        periodic_data.Periodic_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
                    }
                    else if (Admin_var.Current_Time_Format == TWELVE_HR)
                    {
                        if (Admin_var.AM_PM == AM_TIME_FORMAT)
                        {
                            if (Current_time.RTC_Hour == 12)
                                periodic_data.Periodic_hdr.Time.Hr = 0;
                            else
                                periodic_data.Periodic_hdr.Time.Hr =
                                    (U8) Current_time.RTC_Hour;
                        }
                        else
                        {
                            if (Current_time.RTC_Hour == 12)
                                periodic_data.Periodic_hdr.Time.Hr = 12;
                            else
                                periodic_data.Periodic_hdr.Time.Hr =
                                    (U8) Current_time.RTC_Hour + 12;
                        }
                    }
                    periodic_data.Periodic_hdr.Time.Min = (U8) Current_time.RTC_Min;
                    periodic_data.Periodic_hdr.Time.Sec = (U8) Current_time.RTC_Sec;

                    fheader.File_hdr.Date.Year = Current_time.RTC_Year;
                    fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
                    fheader.File_hdr.Date.Day = Current_time.RTC_Mday;
                    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                        fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
                    else if (Admin_var.Current_Time_Format == TWELVE_HR)
                    {
                        if (Admin_var.AM_PM == AM_TIME_FORMAT)
                        {
                            if ((U8) Current_time.RTC_Hour == 12)
                                fheader.File_hdr.Time.Hr = 0;
                            else
                                fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
                        }
                        else
                        {
                            if ((U8) Current_time.RTC_Hour == 12)
                                fheader.File_hdr.Time.Hr = 12;
                            else
                                fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour
                                                           + 12;
                        }
                    }
                    fheader.File_hdr.Time.Min = Current_time.RTC_Min;
                    fheader.File_hdr.Time.Sec = Current_time.RTC_Sec;
                    // SKS : BS272 totals are being cleared here
                    Totals_var.daily_Total_weight = 0;
                    Rprts_diag_var.daily_Total_weight = 0;
                    daily_rprt.Total_weight = 0;
                    LPC_RTC->GPREG3 = Current_time.RTC_Mday;
                    prev_day = LPC_RTC->GPREG3;
                    LPC_RTC->GPREG4 = Current_time.RTC_Wday;
                    //					prev_week = LPC_RTC->GPREG4;
                    /*	 			log_data_populate(periodic_log_stop);
                     report_data_populate(daily_report);		*/

//                     if (Current_time.RTC_Mon != prev_month
//                             || Current_time.RTC_Year != prev_year)
//                     {
//                         periodic_data.Periodic_hdr.Id = LPC_RTC->GPREG2;
//                         periodic_data.Periodic_hdr.Id = 0;			//SKS : The record number should never roll over
//                         clear_data_log.Clr_data_hdr.Id = 0;			//SKS : The record number should never roll over
//                     }

//=========
//                log_data_populate(periodic_log_start);

                    //=============
                    //Rprts_diag_var.daily_Total_weight = Calculation_struct.Total_weight - Plant_connect_var.Accumulated_weight;
                    Totals_var.daily_Total_weight = Calculation_struct.Total_weight
                                                    - Plant_connect_var.Accumulated_weight;
                    Rprts_diag_var.daily_Total_weight = Totals_var.daily_Total_weight;
                    daily_rprt.Total_weight = Rprts_diag_var.daily_Total_weight;
                    //daily_rprt.Total_weight += (3.2/100.0)*(Calculation_struct.Rate_for_display/60.0);
                    /* Now update the date time in header for next daily report */
                    daily_rprt.freq_rprt_header.Date.Year = (U16) Current_time.RTC_Year;
                    daily_rprt.freq_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
                    daily_rprt.freq_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
                    if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                        daily_rprt.freq_rprt_header.Time.Hr =
                            (U8) Current_time.RTC_Hour;
                    else if (Admin_var.Current_Time_Format == TWELVE_HR)
                    {
                        if (Admin_var.AM_PM == AM_TIME_FORMAT)
                        {
                            if ((U8) Current_time.RTC_Hour == 12)
                                daily_rprt.freq_rprt_header.Time.Hr = 0;
                            else
                                daily_rprt.freq_rprt_header.Time.Hr =
                                    (U8) Current_time.RTC_Hour;
                        }
                        else
                        {
                            if ((U8) Current_time.RTC_Hour == 12)
                                daily_rprt.freq_rprt_header.Time.Hr = 12;
                            else
                                daily_rprt.freq_rprt_header.Time.Hr =
                                    (U8) Current_time.RTC_Hour + 12;
                        }
                    }
                    daily_rprt.freq_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
                    daily_rprt.freq_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;
                    //tsk_unlock();
                }
                os_mut_release (&USB_Mutex);
            } //end of day change routine.
        }
        LPC_RTC->ALHOUR &= ~(0x07<<1);
        LPC_RTC->ALHOUR |= (0x07<<1);		//SECTION7
        Task_section = 5;
        /*check for overflow since after 24 hours, the difference between the previous and current weekday
         will indicate an increment in a week*/
        //SKS --> modified on 3rd Nov BS238
        /*	if (((prev_week == 6) && (Current_time.RTC_Wday == 0))
         || (prev_week > Current_time.RTC_Wday)
         || ((Current_time.RTC_Wday == 0)
         && (Current_time.RTC_Wday != prev_week)))*/
        /*if ((prev_week > Current_time.RTC_Wday)||((prev_week!= Current_time.RTC_Wday)&&((Current_time.RTC_Wday==7)||(Current_time.RTC_Wday==0))))*/
        if (((Prev_time.RTC_Wday == 6) && (Current_time.RTC_Wday == 0)) || (Prev_time.RTC_Wday > Current_time.RTC_Wday))
        {
            LPC_RTC->GPREG4 = Current_time.RTC_Wday;
            //		prev_week = LPC_RTC->GPREG4;
            /*populate the weekly report and update on the USB drive*/
#ifdef SHOW_REPORTS
            report_data_populate(weekly_report);
#endif
            Totals_var.weekly_Total_weight = 0;
            Rprts_diag_var.weekly_Total_weight = 0;
            weekly_rprt.Total_weight = 0;
            weekly_rprt.freq_rprt_header.Date.Year =
                (U16) Current_time.RTC_Year;
            weekly_rprt.freq_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
            weekly_rprt.freq_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                weekly_rprt.freq_rprt_header.Time.Hr =
                    (U8) Current_time.RTC_Hour;
            else if (Admin_var.Current_Time_Format == TWELVE_HR)
            {
                if (Admin_var.AM_PM == AM_TIME_FORMAT)
                {
                    if ((U8) Current_time.RTC_Hour == 12)
                        weekly_rprt.freq_rprt_header.Time.Hr = 0;
                    else
                        weekly_rprt.freq_rprt_header.Time.Hr =
                            (U8) Current_time.RTC_Hour;
                }
                else
                {
                    if ((U8) Current_time.RTC_Hour == 12)
                        weekly_rprt.freq_rprt_header.Time.Hr = 12;
                    else
                        weekly_rprt.freq_rprt_header.Time.Hr =
                            (U8) Current_time.RTC_Hour + 12;
                }
            }
            weekly_rprt.freq_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
            weekly_rprt.freq_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;
        }
        Task_section = 6;
        /*check for overflow since after 28/29/30/31 days, the difference between the previous and current month is
         1 which will indicate an increment in month*/
        if ((Current_time.RTC_Mon != prev_month)
                || (Current_time.RTC_Year != Prev_time.RTC_Year))
        {
            LPC_RTC->ALMON = Current_time.RTC_Mon;
            prev_month = LPC_RTC->ALMON;
            /*populate the monthly report and update on the USB drive*/
#ifdef SHOW_REPORTS
            report_data_populate(monthly_report);
#endif
            Totals_var.monthly_Total_weight = 0;
            Rprts_diag_var.monthly_Total_weight = 0;
            monthly_rprt.Total_weight = 0;
            monthly_rprt.freq_rprt_header.Date.Year =
                (U16) Current_time.RTC_Year;
            monthly_rprt.freq_rprt_header.Date.Mon = (U8) Current_time.RTC_Mon;
            monthly_rprt.freq_rprt_header.Date.Day = (U8) Current_time.RTC_Mday;
            if (Admin_var.Current_Time_Format == TWENTYFOUR_HR)
                monthly_rprt.freq_rprt_header.Time.Hr =
                    (U8) Current_time.RTC_Hour;
            else if (Admin_var.Current_Time_Format == TWELVE_HR)
            {
                if (Admin_var.AM_PM == AM_TIME_FORMAT)
                {
                    if ((U8) Current_time.RTC_Hour == 12)
                        monthly_rprt.freq_rprt_header.Time.Hr = 0;
                    else
                        monthly_rprt.freq_rprt_header.Time.Hr =
                            (U8) Current_time.RTC_Hour;
                }
                else
                {
                    if ((U8) Current_time.RTC_Hour == 12)
                        monthly_rprt.freq_rprt_header.Time.Hr = 12;
                    else
                        monthly_rprt.freq_rprt_header.Time.Hr =
                            (U8) Current_time.RTC_Hour + 12;
                }
            }
            monthly_rprt.freq_rprt_header.Time.Min = (U8) Current_time.RTC_Min;
            monthly_rprt.freq_rprt_header.Time.Sec = (U8) Current_time.RTC_Sec;
            //periodic_data.Periodic_hdr.Id = 0;
            //clear_data_log.Clr_data_hdr.Id = 0;
        }

        if (prev_year != Current_time.RTC_Year)
        {
            LPC_RTC->ALYEAR = Prev_time.RTC_Year;
            prev_year = LPC_RTC->ALYEAR;		//SKS --> BS-238 07-11-2016
            Totals_var.yearly_Total_weight = 0;
            Rprts_diag_var.yearly_Total_weight = 0;
        }
        Task_section = 7;
        if ((Flags_struct.Plant_connect_record_flag & REGISTER_ADDR_ENTERED_FLAG)
                == REGISTER_ADDR_ENTERED_FLAG)
        {
            //plant_connect_struct_read();
        }

        //Added on 27 April 2016
        /*	 if((Current_time.RTC_Sec != Prev_time.RTC_Sec) && (enable_eeprom_struc_write_flag == 0))
         {
         if(++enable_eeprom_struc_write_counter > 10)
         {
         enable_eeprom_struc_write_flag = 1;
         enable_eeprom_struc_write_counter = 0;
         }
         }*/

        /*if((Current_time.RTC_Sec != Prev_time.RTC_Sec) && (clr_wt_cmd_recd == 1)) //Added on 27 April 2016
         {
         if(++enable_clr_wt_write_counter > 10)
         {
         if(OS_R_OK == os_mut_wait (&FLash_Mutex, FLASH_MUTEX_TIMEOUT))
         {
         Flash_Write_In_progress = 1; //Added on 27 April 2016
         flash_para_backup();
         flash_struct_backup(0);
         Flash_Write_In_progress = 0; //Added on 27 April 2016
         clr_wt_cmd_recd =0;
         }
         os_mut_release (&FLash_Mutex);
         enable_clr_wt_write_counter =0;
         }
         }*/

        if ((Current_time.RTC_Sec != Prev_time.RTC_Sec)
                && (clr_wt_cmd_recd == 1)) //Added on 27 April 2016
        {
            if (++enable_clr_wt_write_counter > 10)
            {
                clr_wt_cmd_recd = 0;
                enable_clr_wt_write_counter = 0;
            }
        }
//by megha on 22/2/2017 for dynamic auto zero cycle
// 			if(Power_on_auto_zero_cal_fg == 0)
// 			{
// 				Power_on_auto_zero_cal();
//
// 			}
// 			else if(Power_on_auto_zero_cal_fg == 1)
// 			{
        //	if((Dynamic_zero_cal_done_fg == 1)&&(Calibration_var.New_zero_value != 0))
        //SKS email:Wed 22-03-2017 06:17
        if(Admin_var.Auto_zero_tolerance!=0)
        {
            Auto_zero_cycle();
        }

//			}

        //	LPC_RTC->GPREG3 = Current_time.RTC_Mday;
        //	LPC_RTC->ALMON = Current_time.RTC_Mon;
        //	LPC_RTC->GPREG4 = Current_time.RTC_Wday;
        //	LPC_RTC->ALYEAR = Current_time.RTC_Year;
        Prev_time = Current_time;
        LPC_RTC->ALHOUR &= ~(0x07<<1);				//SECTION EXIT
    }
}
#endif

#ifdef ETHERNET

__task void tcp_task_main (void)
{
    U32 regv;
    static U8 link_up = 0;

    dhcp_tout = DHCP_TOUT;
    while (1)
    {
        os_itv_set(ETHERNET_MAIN_TASK_INTERVAL);
        os_itv_wait();
        LPC_RTC->ALSEC |= (0x01<<1);
        main_TcpNet();
// 				if (check_wdt==1)while(1){
// 				if (check_wdt==0)break;
// 				}
        //if DHCP is enabled
        if (Admin_var.DHCP_Status == Screen661_str2)
        {
            dhcp_check();
        }
        regv = read_PHY (PHY_REG_BSR);
        if ((regv & 0x0004) && (link_up == 0))
        {
            /* Link is on. */
            dhcp_chk_enable_disable();
            link_up = 1;
        }
        else if (!(regv & 0x0004)) //link is down
        {
            link_up = 0;
        }
        LPC_RTC->ALSEC &= ~(0x01<<1);
//				modbus_tcp_packet();
    }
}
/*****************************************************************************
 * @note       Function name: __task void tcp_tick (void)
 * @returns    returns      : None
 * @param      arg1         : None
 * @param      arg2         : None
 * @author                  : Anagha Basole
 * @date       date created : July 2012
 * @brief      Description  : task generates periodic ticks for RL-TCPnet
 * @note       Notes        : None
 *****************************************************************************/
__task void tcp_tick(void)
{
    while (1)
    {

        os_itv_set(ETHERNET_TCP_TICK_TASK_INTERVAL);
        os_itv_wait();
        LPC_RTC->ALSEC |= (0X01<<0);
        /* Timer tick every xxx ms */
        timer_tick();					//Generates periodic events for RL-TCPnet.
        LPC_RTC->ALSEC &= ~(0x01<<0);
    }
}
#endif
U8 udp_soc;
U16 bindex;
BOOL wait_ack;
U8 rem_IP[4] = {192,168,1,100};
U8 *sendbuf;
void send_data (void)
{
// static const U8 rem_IP[4] = {192,168,0,100};
    

    if (wait_ack == __TRUE)
    {
        return;
    }
    if (bindex < 128)
    {
        sendbuf = udp_get_buf (512);
//     for (i = 0; i < 512; i += 2) {
//       sendbuf[i]   = bcount >> 8;
//       sendbuf[i+1] = bcount & 0xFF;
//     }
        udp_send (udp_soc, rem_IP, 1000, sendbuf, 512);
    }
}

//U8 *sendbuf;

//int UDP_session_Authenticated =0;

U16 udp_callback (U8 socket, U8 *remip, U16 port, U8 *buf, U16 len)
{
    /* This function is called when UDP data has been received. */
    memcpy(udpdata.rem_IP,remip,4);
    udpdata.port =port;
    udpdata.socket = socket;
    memcpy(udpdata.buf,buf,len);
    //udpdata.data_count++;
	(udpdata.session.open =1);
  //  if (udpdata.session.open ==0)
 //   {
        if (!strncmp((char*)udpdata.buf,"HELLO_BELTSCALE",13))
        {
            sendbuf = udp_get_buf (6);
            strcpy((char*)sendbuf, "PWD");
            udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
            udpdata.session.open =1;
					udpdata.session.Authenticated =0;
        }
  //  }
    else if ((udpdata.session.open ==1)&&(udpdata.session.Authenticated ==0))
    {
        if ((!strncmp((char*)(udpdata.buf),"BW310",5)))
        {
            udpdata.session.Authenticated =1;
					sendbuf = udp_get_buf (300);

        if (Admin_var.DHCP_Status == Screen661_str2)
        {
            sprintf((char*)sendbuf, "%s\t%s\t%s\t%d:%d:%d:%d:%d:%d\t%d.%d.%d.%d\tDHCP_ENABLED\r\n", Admin_var.Scale_Name,Admin_var.Plant_Name,Admin_var.Product_Name,
                    own_hw_adr[0], own_hw_adr[1], own_hw_adr[2], own_hw_adr[3], own_hw_adr[4], own_hw_adr[5],
                    Admin_var.IP_Addr.Addr1, Admin_var.IP_Addr.Addr2, Admin_var.IP_Addr.Addr3, Admin_var.IP_Addr.Addr4);

        }
        else
        {
            sprintf((char*)sendbuf, "%s\t%s\t%s\t%d:%d:%d:%d:%d:%d\t%d.%d.%d.%d\tDHCP_DISABLED\r\n", Admin_var.Scale_Name,Admin_var.Plant_Name,Admin_var.Product_Name,
                    own_hw_adr[0], own_hw_adr[1], own_hw_adr[2], own_hw_adr[3], own_hw_adr[4], own_hw_adr[5],
                    Admin_var.IP_Addr.Addr1, Admin_var.IP_Addr.Addr2, Admin_var.IP_Addr.Addr3, Admin_var.IP_Addr.Addr4);
        }
        udp_send (udpdata.socket, udpdata.rem_IP, udpdata.port,sendbuf, strlen((char*)sendbuf));
        }
				
    }
    else if ((udpdata.session.open ==1)&&(udpdata.session.Authenticated ==1))
    {
			 if ((!strncmp((char*)(udpdata.buf),"UDP_EXIT:",9)))
			 {
				 sendbuf = udp_get_buf (50);
				 sprintf((char*)sendbuf,"BYE BYE! %s\r\n",Admin_var.Scale_Name);
				 
				  udp_send (udpdata.socket, udpdata.rem_IP,udpdata.port, sendbuf, strlen((char*)sendbuf)); 
				 udpdata.session.Authenticated =0; 
				 udpdata.session.open =0;
			 }
        //parse your commands here!
			else
			{
				UDP_parser(buf);
			}


    }
    return (0);
}




__task void test_flash()
{
    int testcase =0;
    while(1)
    {
        os_itv_set(ETHERNET_MAIN_TASK_INTERVAL*100);
        os_itv_wait();
        switch (testcase)
        {
        case 1:
            // write a total to flash

            break ;
        case 2:
            // traverse and get the latest version no.
            break ;
        case 3:
            // read the latest rev no.
            break;
        default :
            break;

        }
    }
}
__task void udp_server()
{

    static int resend_count;
    udp_soc = udp_get_socket (0, UDP_OPT_SEND_CS | UDP_OPT_CHK_CS, udp_callback);
    udp_open (udp_soc, 50001);
    bindex   = 0;
    wait_ack = __FALSE;
    udpdata.data_count =0;
    while(1)
    {
        os_itv_set(ETHERNET_MAIN_TASK_INTERVAL*100);
        os_itv_wait();
        main_TcpNet();
    }

}

//task  wdtfeed
// __task void WDT_Feed_Task(void)
// {
// int state =0;
// while(1)
//    os_itv_set(WDT_FEED_TASK_INTERVAL);
//         os_itv_wait();
// 	 if (state==0)
// 	 {
// 	 state =1;
// 	 	LPC_GPIO1->DIR |=0x01<<5;
// 		LPC_GPIO1->CLR |=0x01<<5;
// 	 }
// 	 else {
// 	 state =0;
// 	 	LPC_GPIO1->DIR |=0x01<<5;
// 		LPC_GPIO1->SET |=0x01<<5;
// 	 }
// //	 if (dbg_wdt==1)
// //	 Uart_i2c_data_send1("W", 1);
// 	 WDTFeed();
// 	 }
/*****************************************************************************
 * @note       Function name: __task void init (void)
 * @returns    returns      : None
 * @param      arg1         : None
 * @author                  : Anagha Basole
 * @date       date created :
 * @brief      Description  : Creates all the tasks with respective priorities
 * @note       Notes        : None
 *****************************************************************************/
__task void init(void)
{
    Rprts_diag_var.Load_Cell_1_Output = 0;
    Rprts_diag_var.Load_Cell_2_Output = 0;
    Rprts_diag_var.Load_Cell_3_Output = 0;
    Rprts_diag_var.Load_Cell_4_Output = 0;
    Rprts_diag_var.Load_Cell_5_Output = 0;
    Rprts_diag_var.Load_Cell_6_Output = 0;
    Rprts_diag_var.Load_Cell_7_Output = 0;
    Rprts_diag_var.Load_Cell_8_Output = 0;
    Rprts_diag_var.Speed_Sensor = 0;
    Rprts_diag_var.Angle_Sensor = 0;
    Rprts_diag_var.Digital_Input_1_Status = Port_string2; //off
    Rprts_diag_var.Digital_Input_2_Status = Port_string2; //off
    Rprts_diag_var.Digital_Input_3_Status = Port_string2; //off
    Rprts_diag_var.Digital_Input_4_Status = Port_string2; //off
    Rprts_diag_var.Digital_Output_1_Status = Port_string2; //off
    Rprts_diag_var.Digital_Output_2_Status = Port_string2; //off
    Rprts_diag_var.Digital_Output_3_Status = Port_string2; //off
    Rprts_diag_var.Relay_Output_1_Status = Port_string2; //off
    Rprts_diag_var.Relay_Output_2_Status = Port_string2; //off
    Rprts_diag_var.Relay_Output_3_Status = Port_string2; //off
    Rprts_diag_var.Analog_Output_1_Status = Port_string2; //off
    Rprts_diag_var.Analog_Output_2_Status = Port_string2; //off
    Rprts_diag_var.PID_Set_Rate = Port_string2; //off
    Rprts_diag_var.RS485_1_Status = Port_string1; //on
    Rprts_diag_var.RS485_2_Status = Port_string1; //on
    Rprts_diag_var.RS232_1_Status = Port_string1; //on
    Rprts_diag_var.RS232_2_Status = Port_string1; //on
    Rprts_diag_var.Ethernet_Status = Port_string1; //on
    Rprts_diag_var.SPI_Status = Port_string1; //on
    Rprts_diag_var.Five_volt_supply = 0.0;
    Rprts_diag_var.Load_cell_supply = 0.0;
    Rprts_diag_var.Speed_In_Hz = Speed_unit;
    //for auto zero testing on 23/2/2017
    //Calibration_var.New_zero_value = 0;
    Modbus_rtu_var.user_semaphore = SEMAPHORE_RELEASED;

    t_Free_Running_Timer_tick = os_tsk_create(Free_Running_Timer_tick,
                                TICK_TIMER_TASK_PRIORITY);

//PVK


    os_mut_init(&FLash_Mutex);
    os_mut_release (&FLash_Mutex);
    os_mut_init (&USB_Mutex);
    os_mut_release (&USB_Mutex);
    os_mut_init (&Debug_uart_Mutex);
    os_mut_release (&Debug_uart_Mutex);
    os_mut_init (&WDT_Feed_Mutex);
    os_mut_release (&WDT_Feed_Mutex);
#ifdef ETHERNET
    t_tick = os_tsk_create(tcp_tick, TCP_TICK_TASK_PRIORITY);
    t_tcp_task_main = os_tsk_create_user(tcp_task_main, UDP_MAIN_TASK_PRIORITY,
                                         &tcp_stack, sizeof(tcp_stack));

    t_udp_task_main = os_tsk_create_user(udp_server, UDP_MAIN_TASK_PRIORITY,
                                         &udp_stack, sizeof(udp_stack));


#endif

#ifdef EMAIL
    t_email = os_tsk_create (email_task, EMAIL__TASK_PRIORITY);
#endif

#ifdef KEYPAD
    t_keypad = os_tsk_create_user(Keypad_task, KEYPAD_TASK_PRIORITY,
                                  &keypad_stack, sizeof(keypad_stack));
#endif

#ifdef MODBUS_SENSOR_RTU_MASTER
    t_modbus_sensor_rtu = os_tsk_create_user(modbus_sensor_rtu,
                          MODBUS_SEN_TASK_PRIORITY, &modbus_master_stack,
                          sizeof(modbus_master_stack));
#endif

#ifdef DEBUG_DATA_SEND
    t_Send_Data_On_Debug_Port = os_tsk_create_user(Send_Data_On_Debug_Port_task,
                                DEBUG_DATA_SEND_TASK_PRIORITY, &Send_Data_On_Debug_Port_stack,
                                sizeof(Send_Data_On_Debug_Port_stack));
#endif

#ifdef PRINTER
    //  t_printer_data = os_tsk_create (printer_send_task, PRINTER_TASK_PRIORITY);
    t_printer_data = os_tsk_create_user(printer_send_task,
                                        PRINTER_TASK_PRIORITY, &printer_send_stack,
                                        sizeof(printer_send_stack));
    Rprts_diag_var.RS232_1_Status = Port_string1;
#endif

#ifdef SCOREBOARD
    t_scoreboard_data = os_tsk_create(scoreboard_send_task,
                                      SCOREBOARD_TASK_PRIORITY);
    Rprts_diag_var.RS232_2_Status = Port_string1;
#endif

#ifdef IOBOARD
    t_ioboard = os_tsk_create_user(ioboard_task, IOBOARD_TASK_PRIORITY,
                                   &Ioboard_stack, sizeof(Ioboard_stack));
    //t_weight_monitor = os_tsk_create(monitor_weight_task, WT_MONITOR_TASK_PRIORITY);
#endif

#ifdef RTC
    t_rtc_tick = os_tsk_create_user(get_time, RTC_TASK_PRIORITY, &rtc_stack,
                                    sizeof(rtc_stack));
#endif

#ifdef GUI
    t_lcd_update = os_tsk_create_user(update_lcd, GUI_TASK_PRIORITY, &gui_stack,
                                      sizeof(gui_stack));
#endif

#ifdef MODBUS_TCP
    Plant_connect_var.Error_code1 = Plant_connect_var.Error_code2 =
                                        Plant_connect_var.Error_code3 = 0;
    Plant_connect_var.Old_record_request = 0;
    Plant_connect_var.Unique_id = 0;
    t_modbus_tcp_send = os_tsk_create_user(modbus_tcp_packet,
                                           MODBUS_TCP_TASK_PRIORITY, &modbus_stack, sizeof(modbus_stack));
#endif

#ifdef USB
    t_usb = os_tsk_create_user(usb_task, USB_TASK_PRIORITY, &usb_stack,
                               sizeof(usb_stack));
#endif

#ifdef MODBUS_RTU_INTEGRATOR
    // Below Code is related to Integrator to Integrator communication, blending and rate control,
    //Please also check Modbus_rtu_master.c,  Modbus_rtu_slave.c and Screen__common_func.c screenfor dependencies.
    if (Setup_device_var.Network_Addr == 0)
    {
        t_modbus_integrator_master_rtu = os_tsk_create(
                                             modbus_integrator_master_rtu, MODBUS_INT_TASK_PRIORITY_MASTER);
        t_modbus_integrator_slave_rtu = 0;
    }
    else
    {
        t_modbus_integrator_slave_rtu = os_tsk_create(
                                            modbus_integrator_slave_rtu, MODBUS_INT_TASK_PRIORITY_SLAVE);
        t_modbus_integrator_master_rtu = 0;
    }
#endif
    OS_Init_Done_Flag = 1;
    //	t_WDT_feed = os_tsk_create(WDT_Feed_Task,
    //                              WDT_FEED_TASK_PRIORITY);

    //   WDTInit(); //SKS 14-07-2017 called at bootup in main()
    LPC_RTC->ALDOY &=(~0x01);		//SKS 14-07-2017  cleared BAD Ethernet IC flag to allow Ethernet Init at next bootup.
    printf("\n all tasks created");
    os_tsk_delete_self();
}


#endif /*#ifdef RTOS*/
/*****************************************************************************
 * End of file
 *****************************************************************************/
